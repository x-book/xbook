package de.uni_muenchen.vetmed.xbook.implementation.xbook;

import com.jidesoft.plaf.LookAndFeelFactory;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.AbstractSplashScreen;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.DatabaseInitializer;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.H2Server;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.MySqlServer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.io.OutputStream;
import java.io.PrintStream;

public abstract class AbstractStart {

    protected static final Log _log = LogFactory.getLog(AbstractStart.class);

    AbstractController<?, ?> controller;

    public AbstractStart(String[] args) {
        this(args, XBookConfiguration.DatabaseMode.MYSQL, true);
    }

    public AbstractStart(String[] args, XBookConfiguration.DatabaseMode databaseMode, boolean startDatabase) {
        try {
            // Set a custom print stream
            System.setOut(new MyPrintStream(System.out));
            long heapSize = Runtime.getRuntime().totalMemory();
            System.out.println("Heap size: " + heapSize);
            System.out.println("Assigned memory (RAM): " + StringHelper.addThousandsSeparator(Runtime.getRuntime().maxMemory()) + " Bytes");
            String username = null;
            String password = null;
            boolean autoContinue = false;
            try {
                String arguments = "";
                for (String s : args) {
                    arguments += s + " ";
                }
                System.out.println("Arguments: " + arguments);

                initConfiguration();

//            if (args.length > 1) {
                for (int i = 0; i < args.length; i++) {
                    if (args[i].equals("-book") || args[i].equals("-b")) {

                        try {
                            //not in use anymore
                            i++;
                        } catch (NumberFormatException nfe) {
                            System.out.println("ERROR: Invalid parameter.");
                            JOptionPane.showMessageDialog(null, "Invalid parameter.", "Error", JOptionPane.WARNING_MESSAGE);
                            return;
                        }
                    } else if (args[i].equals("-u")) {
                        username = args[i + 1];
                        i++;
                    } else if (args[i].equals("-p")) {
                        password = args[i + 1];
                        i++;
                    } else if (args[i].equals("-skip")) {
                        //only 1 argument
                        autoContinue = true;

                    }
                }

//            } else {
//            if (bookTypeTemp == -1) {
//                if ((Configuration.MODE == Configuration.Mode.DEVELOPMENT || Configuration.MODE == Configuration.Mode.TEST)) {
//                    // check the properties for the book type to load
//                    bookTypeTemp = Configuration.getInt(Configuration.BOOK_TYPE);
//                    // if no book type is saved to the properties load the book type chooser.
//                    if (bookTypeTemp == Configuration.NO_BOOK) {
//                        BookTypeChooser chooser = new BookTypeChooser();
//                        bookTypeTemp = chooser.getBookType();
//
//                        // save the settings to the properties
//                        if (chooser.isDoNotShowItAgainSelected()) {
//                            Configuration.setProperty(Configuration.BOOK_TYPE, bookTypeTemp);
//                        }
//                        Configuration.setLanguage(chooser.getLanguage());
//                    }
//                } else { // if is not dev MODE
//                    JOptionPane.showMessageDialog(null, "No parameter! Please use the xBook launcher to start a Book.", "Error", JOptionPane.WARNING_MESSAGE);
//                    return;
//                }
//            }
//            }
                String databaseName = getDatabaseName();
                XBookConfiguration.setBookType(databaseName);
                initLoc();
                initColors();
                //check if already running
//            SingleProgramInstance.check(XBookConfiguration.getProperty(XBookConfiguration.LOCKPORT));
                SingleProgramInstance2.check();
                // display the splash screen
                getSplashScreen();

                new Runnable() {
                    @Override
                    public void run() {

                    }
                }.run();
                databaseName += XBookConfiguration.EXTENSION;
                XBookConfiguration.setDatabaseMode(databaseMode);
                // run the application

                _log.info(
                        "\n\n"
                                + "********************************************************************\n"
                                + "**************************** xBook Start ***************************\n"
                                + "***  Version: " + XBookConfiguration.PROGRAM_VERSION + "\n"
                                + "***  Database: " + databaseName + "\n"
                                + "********************************************************************\n"
                                + "********************************************************************\n"
                );

                try {
                    UIManager.setLookAndFeel(WindowsLookAndFeel.class.getName());
                    LookAndFeelFactory.installJideExtension();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                if (startDatabase)
                    switch (databaseMode) {
                        case H2:
                            H2Server.init();
                            break;
                        case MYSQL:
                            MySqlServer.init();
                            break;
                        case NONE:
                            break;
                        default:
                            _log.error("No database Selected");
                            return;
                    }
                // Check if the database is our standalone one, and if it has been initialized / personalized.
                if (!DatabaseInitializer.init(databaseName, startDatabase)) {
                    // db not started do something print error or something

                    switch (databaseMode) {
                        case H2:
                            H2Server.stopServer();
                            break;
                        case MYSQL:
                            MySqlServer.stopServer();
                            break;
                        case NONE:
                            break;
                        default:
                            _log.error("No database Selected");
                            break;
                    }
                    AbstractSplashScreen.hide(true);
                    JOptionPane.showMessageDialog(null, Loc.get("ERROR_WHILE_STARTING_THE_DATABASE_DIALOG"), Loc.get("ERROR_WHILE_STARTING_THE_DATABASE"), JOptionPane.ERROR_MESSAGE);
                    System.exit(2);
                }
//            } else {
//                //TODO still check if connection to db can be established without starting or stopping
//            }
//            }
                controller = getController(databaseName);
                controller.setAutoContinue(autoContinue);
                if (username != null && password != null) {
                    controller.login(username, password);
                }

                // Initialise a new GUI Controller. It will start the full function of the GUI.
            } catch (Exception ex) {
                _log.error(ex);
                for (Object s : ex.getStackTrace()) {
                    _log.error(s);
                }
            }
        } finally {
            AbstractSplashScreen.hide(true);
        }
    }

    protected void initConfiguration() {
        XBookConfiguration.init();
    }

    public AbstractController<?, ?> getController() {
        return controller;
    }

    protected abstract AbstractSplashScreen getSplashScreen();

    protected abstract void initColors();

    protected abstract void initLoc();

    protected abstract String getDatabaseName();

    protected abstract AbstractController<?, ?> getController(String databaseName);

    private static class MyPrintStream extends PrintStream {

        public MyPrintStream(OutputStream out) {
            super(out);
        }

        //Alex edit
        @Override
        public void println(String s) {
            if (s.equals(null)) {
                return;
            } else if (s.startsWith("Can't find bundle for base name com.toedter.calendar.jcalendar")) {
                return;
            }
            super.println(s);
        }
    }
}
