package de.uni_muenchen.vetmed.xbook.implementation.xbook.controller;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Group;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rights;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.network.*;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.*;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.export.AbstractExport;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.UpdatePanel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.*;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.CodeTablesSynchronisation;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.DatabaseScheme;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.PlannedSynchronisation;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.Synchroniser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.xml.bind.DatatypeConverter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * @param <T>
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class AbstractSynchronisationController<T extends AbstractQueryManager<?, ?>, E extends AbstractExport> extends AbstractController<T, E> implements Observer {


    private static final Log LOGGER = LogFactory.getLog(AbstractSynchronisationController.class);
    /**
     * The Synchronizer which is used to codeTablesSync all projects between the
     * client and the server.
     */
    protected final Synchroniser sync;

    /**
     * The timed synchroniser, that allows project to be synced automatically
     */
    protected final PlannedSynchronisation plannedSynchronisation;
    /**
     * The client which handles the connection to the server
     */
    protected ClientInterface client;

    /**
     * indicator if the user is logged in locally
     */
    protected boolean isLoggedInGlobal = false;

    protected boolean autoUpdateDB = false;

    private final int LOGIN_SUCCESFULL = 1;
    private final int LOGIN_UNSUCESSFULL = 0;
    private final int LOGIN_FAILED = -1;

    /**
     * Creates a new AbstractController with the given Export and Book version
     *
     * @param export      The Export to use
     * @param bookDatabaseVersion The database Verion of the given books
     */
    protected AbstractSynchronisationController(E export, String bookDatabaseVersion) {
        super(export, bookDatabaseVersion);

        client = new PHPClient(this, getSerialisationFactory(),
                XBookConfiguration.configXBook.get(XBookConfiguration.PROXY_HOST, ""),
                XBookConfiguration.configXBook.getInt(XBookConfiguration.PROXY_PORT, 0),
                XBookConfiguration.configXBook.getBoolean(XBookConfiguration.USE_PROXY, false));
        sync = new Synchroniser(this);

        plannedSynchronisation = new PlannedSynchronisation(this, sync);
    }


    /**
     * Solves the Conflict with the given data.
     *
     * @param entryData
     * @param table
     * @throws NotLoggedInException                                             If the user is not logged in.
     * @throws StatementNotExecutedException
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException
     * @throws java.io.IOException
     */
    public boolean solveConflict(DataSetOld entryData,
                                 IBaseManager table) throws NotLoggedInException, StatementNotExecutedException, NotLoadedException, NotConnectedException, IOException, NoRightException {
        checkWriteRights(loadedProject);
        if (sync.commitEntry(entryData, table, loadedProject)) {
            return false;
        }
        ArrayList<ProjectDataSet> list = new ArrayList<>();
        list.add(loadedProject);
        sync.update(list, false);
        return true;
    }

    public synchronized void startSynchronizer() {
        if (!sync.isAlive()) {
            sync.start();
        }

    }

    @Override
    public void loadProject(ProjectDataSet project,
                            boolean displayInputMask) throws StatementNotExecutedException, NotLoggedInException, NoRightException {
        super.loadProject(project, displayInputMask);

        Footer.setConflictedButtonVisible(XBookConfiguration.isConflicted(project.getProjectKey()));
        ArrayList<ProjectDataSet> list = new ArrayList<>();
        list.add(project);
        sync.update(list, false);
    }

    @Override
    public void saveProject(
            ProjectDataSet project) throws NotLoggedInException, StatementNotExecutedException, NoRightException, EntriesException, MissingInputException {
        super.saveProject(project);
        ArrayList<ProjectDataSet> list = new ArrayList<>();
        list.add(project);

        sync.update(list, false);
    }

    @Override
    public boolean saveEntry(
            EntryDataSet entry) throws EntriesException, NotLoadedException, StatementNotExecutedException, NoRightException, NotLoggedInException {
        boolean bool = super.saveEntry(entry);

        ArrayList<ProjectDataSet> list = new ArrayList<>();
        list.add(loadedProject);
        sync.update(list, false);
        return bool;
    }

    @Override
    public void saveEntries(EntryDataSet entry,
                            ArrayList<Key> keys) throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        super.saveEntries(entry, keys);
        ArrayList<ProjectDataSet> list = new ArrayList<>();
        list.add(loadedProject);
        sync.update(list, false);
    }

    /**
     * Returns the Synchroniser.
     *
     * @return The Synchroniser.
     */
    public Synchroniser getSynchroniser() {
        return sync;
    }

    @Override
    public void deleteProject(
            ProjectDataSet project) throws NotConnectedException, NotLoggedInException, StatementNotExecutedException, NoRightException {
        if (!isServerConnected()) {
            throw new NotConnectedException();
        }

        if (!hasEditRights(project)) {
            throw new NoRightException(NoRightException.Rights.NOPROJECTOWNER);
        }

        if (isProjectLoaded && loadedProject.equals(project)) {
            unloadProject();
        }
        sync.removeProject(project);

        Message ms = sendMessage(Commands.DELETE_PROJECT, project.getProjectKey());
        if (ms.getResult().wasSuccessful()) {
            for (IBaseManager syncint : getLocalManager().getSyncTables()) {
                syncint.deletePermanent(project, AbstractQueryManager.DELETE_ALL);
            }
        } else {
            System.out.println(ms.getResult());
        }

    }

    @Override
    public void deleteProjectLocaly(
            ProjectDataSet project) throws NotLoggedInException, StatementNotExecutedException, NoRightException {

        sync.removeProject(project);
        super.deleteProjectLocaly(project);
    }

    @Override
    public Message changePassword(String passwordOld,
                                  String passwordNew) throws IOException, NotLoggedInException, NotConnectedException {
        if (!client.isRunning()) {
            Footer.displayError(Loc.get("NO_CONNECTION_TO_THE_SERVER"));
            return null;
        }
        ArrayList<Serialisable> data = new ArrayList<>();
        data.add(new SerialisableString(passwordOld));
        data.add(new SerialisableString(passwordNew));
        return client.sendMessage(new Message(Commands.CHANGE_PASSWORD,
                data));//Commands.CHANGE_PASSWORD, new String[]{passwordOld, passwordNew});
    }

    /**
     * Requests a Message from the server with the given command. After command
     * was send, methode waits until the command for recieving the message was
     * recieved
     *
     * @param command The request command send to the server
     * @return The message
     * @throws IOException           If a network error occurred
     * @throws NotConnectedException
     * @throws NotLoggedInException  If the user is not logged in.
     */
    public String sendMessageWithString(
            Commands command) throws IOException, NotConnectedException, NotLoggedInException {
        Message message = client.sendMessage(new Message(command));

        if (message.getResult().wasSuccessful()) {
            ArrayList<Serialisable> data = message.getData();
            if (!data.isEmpty()) {
                SerialisableString string = (SerialisableString) data.get(0);
                return string.getString();
            }
        }
        throw new IOException(message.getResult().getErrorMessage());
    }

    /**
     * Requests a List of Messages from the server with the given command. After
     * command was send, methode waits until the command for recieving the
     * message was recieved
     *
     * @param command The request command send to the server
     * @return The messages
     * @throws IOException           If a network error occurred
     * @throws NotConnectedException
     * @throws NotLoggedInException  If the user is not logged in.
     */
    public ArrayList<String> getMessages(
            Commands command) throws IOException, NotConnectedException, NotLoggedInException {
        Message message = client.sendMessage(new Message(command));
        ArrayList<String> list = new ArrayList<>();
        if (message.getResult().wasSuccessful()) {
            ArrayList<Serialisable> data = message.getData();
            if (!data.isEmpty()) {
                SerialisableString string = (SerialisableString) data.get(0);
                list.add(string.getString());
            }
        }
        return list;

    }

    /**
     * Sends a Message with the given command to the Server. Returns the Message
     * received from the server.
     *
     * @param command The command to be send to the server.
     * @return The Message received from the server.
     * @throws NotConnectedException
     * @throws NotLoggedInException  If the user is not logged in.
     * @throws IOException           If a network error occurred
     */
    public Message sendMessage(Commands command) throws NotConnectedException, NotLoggedInException, IOException {
        return client.sendMessage(new Message(command));
    }

    /**
     * Sends the given Message to the server. And returns the Message received
     * from the server
     *
     * @param message The message to be send to server
     * @return The Message received from the server.
     * @throws NotConnectedException
     * @throws NotLoggedInException  If the user is not logged in.
     * @throws IOException           If a network error occurred
     */
    public Message sendMessage(Message message) throws NotConnectedException, NotLoggedInException, IOException {
        return client.sendMessage(message);
    }

    /**
     * Returns the global programm version.
     *
     * @return The global programm version.
     * @throws IOException           If a network error occurred
     * @throws NotConnectedException
     * @throws NotLoggedInException  If the user is not logged in.
     */
    public String getGlobalProgrammVersion() throws IOException, NotConnectedException, NotLoggedInException {
        return sendMessageWithString(Commands.REQUEST_PROGRAMM_VERSION);
    }

    /**
     * Returns the local database version.
     *
     * @return The local database version.
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NotLoggedInException          If the user is not logged in.
     */
    public String getLocalDatabaseVersion() throws StatementNotExecutedException, NotLoggedInException {
        return getLocalManager().getDatabaseManager().getDatabaseVersion();
    }

    /**
     * Reads a ArrayList of the given Objects with the given command from the
     * Server. !
     *
     * @param command The command given to the server
     * @return
     * @throws IOException           If a network error occurred
     * @throws NotConnectedException
     * @throws NotLoggedInException  If the user is not logged in.
     */
    public ArrayList<Serialisable> getObjects(
            Commands command) throws IOException, NotConnectedException, NotLoggedInException {
        Message returnMessage = client.sendMessage(new Message(command));
        if (returnMessage.getResult().wasSuccessful()) {
            return returnMessage.getData();
        }
        throw new IOException(returnMessage.getResult().getErrorMessage());
    }

    public boolean isServerConnected() {
        return client.isRunning();
    }

    /**
     * Returns the SerialisableFactory.
     *
     * @return The SerialisableFactory.
     */
    protected AbstractSerialisableFactory getSerialisationFactory() {
        return new AbstractSerialisableFactory();
    }

    @Override
    public void login(String name, String password) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                client.resumeClient();
            }
        }).start();
        super.login(name, password);

    }

    @Override
    protected void doLogin() {

        if (client.isRunning()) {

            if (onlyLocal) {
                super.doLogin();
            } else {
                try {
                    final int b = serverLogin();
                    if (b == LOGIN_FAILED) {
                        super.doLogin();
                    }
                } catch (Throwable l) {
                    org.apache.log4j.Logger.getLogger(AbstractSynchronisationController.class).error(l, l);
                }
            }
        } else {
            super.doLogin();
        }
    }

    public int serverLogin() {
        try {
            client.sendMessage(new Message(Commands.LOGOUT));
            ArrayList<Serialisable> data = new ArrayList<>();
            data.add(new SerialisableString(userName));
            data.add(new SerialisableString(password));
            Commands type = getType();
            if (type != null) {
                data.add(new SerialisableCommands(type));
            }
            Message response = client.sendMessage(new Message(Commands.SENDLOGININFORMATION, data));
            if (!response.getResult().wasSuccessful()) {
                Footer.displayError(response.getResult().getErrorMessage());
                if (response.getResult().equals(Result.LIMITED_FUNCTIONALITY)) {
                    return LOGIN_FAILED;
                }
                return LOGIN_UNSUCESSFULL;
            } else {
                if (client instanceof PHPClient) {
                    update(null, Commands.PASSWORD_CORRECT);
                }
                return LOGIN_SUCCESFULL;
            }
        } catch (NotConnectedException ex) {
            LOGGER.error(ex, ex);
            return LOGIN_FAILED;
        }

    }

    @Override
    public Message register(String username, String displayName, String organisation, String password,
                            String email) throws NotConnectedException {
        if (!client.isRunning()) {
            Footer.displayError(Loc.get("NO_CONNECTION_TO_THE_SERVER"));
            return null;
        }
        ArrayList<Serialisable> data = new ArrayList<>();
        data.add(new SerialisableString(username));
        data.add(new SerialisableString(displayName));
        data.add(new SerialisableString(organisation));
        data.add(new SerialisableString(password));
        data.add(new SerialisableString(email));
        return client.sendMessage(new Message(Commands.REGISTER, data));
    }

    @Override
    public Message resetPassword(String username, String email) throws NotConnectedException {
        if (!client.isRunning()) {
            Footer.displayError(Loc.get("NO_CONNECTION_TO_THE_SERVER"));
            return null;
        }
        ArrayList<Serialisable> data = new ArrayList<>();
        data.add(new SerialisableString(username));
        data.add(new SerialisableString(email));
        return client.sendMessage(new Message(Commands.RESET_PASSWORD, data));
    }

    /**
     * Sends a message to the server with the given command and the given data.
     *
     * @param commands The command to be send.
     * @param data     The data to be send
     * @return The Response from the server
     * @throws IOException           If a network error occurred
     * @throws NotConnectedException
     * @throws NotLoggedInException  If the user is not logged in.
     */
    public Message sendMessage(Commands commands, ArrayList<Serialisable> data) throws NotConnectedException {
        return client.sendMessage(new Message(commands, data));
    }

    /**
     * Sends a message to the server with the given command and the given data.
     *
     * @param commands The command to be send.
     * @param data     The data to be send
     * @return The Response from the server
     * @throws IOException           If a network error occurred
     * @throws NotConnectedException
     * @throws NotLoggedInException  If the user is not logged in.
     */
    public Message sendMessage(Commands commands, Serialisable data) throws NotConnectedException {
        ArrayList<Serialisable> datalist = new ArrayList<>();
        datalist.add(data);
        return client.sendMessage(new Message(commands, datalist));
    }

    private void doUpdateCheckAndLogin() {

        Footer.startWorking();
        try {
            isLoggedInGlobal = true;
            String globalProgrammVersion;
            try {
                globalProgrammVersion = getGlobalProgrammVersion();
            } catch (NotConnectedException | NotLoggedInException ex) {
                globalProgrammVersion = "";
            }
            String localProgrammVersion = getLocalProgrammVersion();
            boolean isProgrammUp2Date = true;
            if (!globalProgrammVersion.equals(localProgrammVersion)) {
                isProgrammUp2Date = false;
                LOGGER.error("Programm: " + localProgrammVersion + "|" + globalProgrammVersion);
            }
            boolean loggedInBefore = isLoggedIn;
            if (!isLoggedIn) {
                try {
                    //Programm version up2date now check local mysql
                    localLogin();

                } catch (NotLoggedInException ex) {
                    try {

                        //login failed possibly because of old password so set new
                        updateLocalPassword();
                        //check again witch updated login information
                        localLogin();
                    } catch (NotLoggedInException | StatementNotExecutedException ex1) {
                        try {
                            //something not working
                            sendMessage(Commands.LOGOUT);

                        } catch (NotConnectedException | NotLoggedInException ex2) {
                            LOGGER.error(ex2, ex2);
                        }
                        Footer.displayError(Loc.get("NO_CONNECTION_TO_THE_SERVER"));
                        Footer.stopWorking();
                        return;
                    }
                }
                isLoggedIn = true;
            }

            boolean isDatabaseUp2Date = true;
            try {//check if database versions do match

                String databaseBookVersion = getLocalManager().getDatabaseManager().getDatabaseVersion();
                String databaseGeneralVersion = getLocalManager().getDatabaseManager().getGeneralDatabaseVersion();
                String generalVersion = XBookConfiguration.GENERAL_DATABASE_VERSION;
                if (!XBookConfiguration.sharedUserTable) {
                    generalVersion = databaseBookVersion;
                    databaseGeneralVersion = bookDatabaseVersion;
                }
                System.out.println("Database General: " + generalVersion + " | " + databaseGeneralVersion);
                System.out.println("Database: " + databaseBookVersion + " | " + bookDatabaseVersion);

                if (!generalVersion.equals(databaseGeneralVersion) || !databaseBookVersion.equals(bookDatabaseVersion)
                        || !getLocalManager().getUpdateManager().isDatabaseInitialized() || !getLocalManager().getUpdateManager().existsGeneral()) {
                    isDatabaseUp2Date = false;

                }
            } catch (StatementNotExecutedException | NotLoggedInException ex) {
                isDatabaseUp2Date = false;
            }
            try {
                T qm = getLocalManager();

                int databaseNumber;
                try {
                    databaseNumber = qm.getDatabaseManager().getDatabaseNumber();
                } catch (StatementNotExecutedException ex) {
                    databaseNumber = -1;
                }
                int databaseNumberConfig = XBookConfiguration.getInt(XBookConfiguration.DATABASENUMBER);
                if (databaseNumber == -1 || databaseNumber != databaseNumberConfig) {
                    databaseNumber = databaseNumberConfig;
                    if (databaseNumber == -1) {
                        //Database Number not set yet or wrong. translate new!
                        Message response = sendMessage(Commands.REQUEST_DATABASE_NUMBER);
                        databaseNumber = Integer.parseInt(((SerialisableString) response.getData().get(0)).getString());
                    }
                    XBookConfiguration.setProperty(XBookConfiguration.DATABASENUMBER, databaseNumber);
                }
                qm.updateDatabaseNumber(databaseNumber);
                try {//set footer if error just use the one we used for login... should only be this time and after restart work properly so
                    //no problem if this goes wrong
                    Footer.updateUserLabel(getLocalManager().getUserManager().getCurrentUser(false));
                } catch (NotLoggedInException | StatementNotExecutedException ex) {
                    Footer.updateUserLabel(userName);
                }
            } catch (NotLoggedInException | NotConnectedException | StatementNotExecutedException ex) {
                LOGGER.error(ex, ex);
            }
            boolean areCodeTablesUp2Date = testCodetables;
            try {

                String lastUpdateLocal = getLocalManager().getUpdateManager().getLastUpdate();
                System.out.println(lastUpdateLocal);
                Message response = sendMessage(Commands.REQUEST_LAST_UPDATE);
                //TODO what happens if resp = false?
                String lastUpdateGlobal = ((SerialisableString) response.getData().get(0)).getString();
                System.out.println(lastUpdateGlobal);
                if (lastUpdateLocal.compareToIgnoreCase(lastUpdateGlobal) < 0) {
                    areCodeTablesUp2Date = false;
                    if (autoUpdateDB) {
                        isDatabaseUp2Date = false;
                    }
                }
            } catch (NotLoggedInException | NotConnectedException | StatementNotExecutedException ex) {
                areCodeTablesUp2Date = false;
            }

            if (!isProgrammUp2Date || !isDatabaseUp2Date || !areCodeTablesUp2Date) {
                mainFrame.getNavigation().setEnableAll(false);
                mainFrame.displayUpdateScreen(
                        loggedInBefore ? UpdatePanel.SourceType.MANUAL_CONNECTION : UpdatePanel.SourceType.LOGIN,
                        isProgrammUp2Date, isDatabaseUp2Date, areCodeTablesUp2Date, true, autoContinue);
                // Stop rotating the icons in the footer after login process started it.
                Footer.stopWorking();
                return;
            } else if (autoUpdateDB) {
                try {
                    updateDatabase(null);
                } catch (NotLoggedInException e) {
                    e.printStackTrace();
                } catch (StatementNotExecutedException e) {
                    e.printStackTrace();
                }
            }
            if (XBookConfiguration.getBookBoolean(XBookConfiguration.AUTOSYNC)) {
                Footer.setConnectionStatus(Footer.Connection.CONNECTED_AUTO_SYNC);
            } else {
                Footer.setConnectionStatus(Footer.Connection.CONNECTED_MANUAL_SYNC);
            }
            //finaly display projects
            startSynchronizer();
            if (!loggedInBefore) {
                onLogin();
                mainFrame.displayProjectOverviewScreen();
            }
        } catch (IOException ex) {
            LOGGER.error(ex, ex);
        }
        Footer.stopWorking();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Commands) {
            switch ((Commands) arg) {
                case PASSWORD_CORRECT:
                    doUpdateCheckAndLogin();
                    break;
                case PASSWORD_INCORRECT:
                    Footer.startWorking();
                    Footer.displayError(Loc.get("LOGIN_OR_PASSWORD_INCORRECT"));
                    isLoggedIn = false;
                    isLoggedInGlobal = false;
                    Footer.stopWorking();
                    break;
                case SENDLOGININFORMATION:
                    Footer.startWorking();
                    if (isLoggedIn && !updateSkipped) {
                        try {
                            ArrayList<Serialisable> data = new ArrayList<>();
                            data.add(new SerialisableString(userName));
                            data.add(new SerialisableString(password));

                            Commands type = getType();
                            if (type != null) {
                                data.add(new SerialisableCommands(type));
                            }
                            Message message = client.sendMessage(new Message(Commands.SENDLOGININFORMATION, data));
                            Message response = client.sendMessage(message);
                            if (!response.getResult().wasSuccessful()) {
                                System.out.println("not successfull :" + response.getResult());
                            }
                        } catch (NotConnectedException ex) {
                            LOGGER.error(ex, ex);
                        }
                    }
                    if (!updateSkipped) {
                        if (XBookConfiguration.getBookBoolean(XBookConfiguration.AUTOSYNC)) {
                            Footer.setConnectionStatus(Footer.Connection.CONNECTED_AUTO_SYNC);
                        } else {
                            Footer.setConnectionStatus(Footer.Connection.CONNECTED_MANUAL_SYNC);
                        }
                    }
                    Footer.stopWorking();
                    break;
                default:
                    System.out.println("unknown: " + arg);
            }
        } else if (arg instanceof Event) {

            switch ((Event) arg) {
                case SERVER_CONNECTED:
                    if (XBookConfiguration.getBookBoolean(XBookConfiguration.AUTOSYNC)) {
                        Footer.setConnectionStatus(Footer.Connection.CONNECTED_AUTO_SYNC);
                    } else {
                        Footer.setConnectionStatus(Footer.Connection.CONNECTED_MANUAL_SYNC);
                    }
                    if (isLoggedIn) {
                        mainFrame.getNavigation().setSyncButtonActive(true);
                    }
                    break;
                case SERVER_DISCONNECTED:
                    isLoggedInGlobal = false;
                    Footer.setConnectionStatus(Footer.Connection.DISCONNECTED);
                    mainFrame.getNavigation().setSyncButtonActive(false);
                    break;
                case RECONNECT:
                    client.resumeClient();
                    break;
                default:
                    break;
            }
        }
        if (arg instanceof ResultType) {
            switch ((ResultType) arg) {
                case CONTINUE_TO_PROJECT:
                    if (!mainFrame.getController().isProjectLoaded) {

                        try {
                            //update language and user
                            getLocalManager().getLanguageManager().getCurrentLanguageID();
                            Footer.updateUserLabel(getLocalManager().getUserManager().getCurrentUser(true));
                        } catch (NotLoggedInException | StatementNotExecutedException ex) {
                            LOGGER.error(ex, ex);
                        }
                        mainFrame.displayProjectOverviewScreen();
                        onLogin();
                    } else {
                    }
                    break;
                case BACK_TO_LOGIN:
                    mainFrame.displayLoginScreen();
                    break;
                case CONTINUE_TO_APPLICATION:
                    onLogin();
                    break;
                case CLOSE_APPLICATION:
                    exitApplication();
                    break;
                case SKIP_UPDATING_PROGRAMM:

                    isLoggedInGlobal = false;
                    updateSkipped = true;
                    Footer.setConnectionStatus(Footer.Connection.NONE);
                    mainFrame.getNavigation().setSyncButtonActive(false);
                    try {
                        try {
                            sendMessageWithString(Commands.LOGOUT);
                        } catch (NotConnectedException | NotLoggedInException ex) {
                            LOGGER.error(ex, ex);
                        }

                    } catch (IOException ex) {
                    }

                    break;
            }
        }
    }

    public Group getGroupInformation(
            String groupname) throws NotLoggedInException, StatementNotExecutedException, EntriesException {
        return getLocalManager().getGroupManager().getGroupInformation(groupname);
    }

    public Message leaveGroup(int groupID) throws IOException, NotConnectedException, NotLoggedInException {
        return sendMessage(Commands.LEAVE_GROUP, new SerialisableString(groupID));
    }

    public Message updateUserRank(int groupId, int userId,
                                  int rankId) throws IOException, NotConnectedException, NotLoggedInException {
        ArrayList<Serialisable> list = new ArrayList<>();
        list.add(new SerialisableString(groupId));
        list.add(new SerialisableString(userId));
        list.add(new SerialisableString(rankId));
        return sendMessage(Commands.UPDATE_USER_RANK, list);
    }

    public Message adjustRank(int group, int rank, String name,
                              ArrayList<Rights> rights) throws IOException, NotConnectedException, NotLoggedInException {
        ArrayList<Serialisable> list = new ArrayList<>();
        list.add(new SerialisableString(group));
        list.add(new SerialisableString(name));
        list.add(new SerialisableString(rank));
        list.addAll(rights);
        return sendMessage(Commands.CHANGE_RANK, list);
    }

    public Message createGroup(String groupName) throws IOException, NotConnectedException, NotLoggedInException {
        return sendMessage(Commands.CREATE_GROUP, new SerialisableString(groupName));
    }

    public Message deleteGroup(int groupId) throws IOException, NotConnectedException, NotLoggedInException {
        return sendMessage(Commands.DELETE_GROUP, new SerialisableString(groupId));
    }

    public Message removeUserFromGroup(int userId,
                                       int groupId) throws IOException, NotConnectedException, NotLoggedInException {
        return sendMessage(new Message(Commands.REMOVE_USER_FROM_GROUP, new SerialisableString(groupId),
                new SerialisableString(userId)));
    }

    public Message removeRank(int groupID, int rankID) throws IOException, NotConnectedException, NotLoggedInException {
        return sendMessage(new Message(Commands.REMOVE_GROUP_RANK, new SerialisableString(groupID),
                new SerialisableString(rankID)));
    }

    /**
     * Saves the list of rights information
     *
     * @param data
     * @return
     * @throws NotConnectedException
     * @throws NotLoggedInException
     * @throws IOException
     * @throws NotLoadedException
     * @throws StatementNotExecutedException
     * @throws NoRightException
     */
    public Message saveNewUserRights(
            ArrayList<RightsInformation> data) throws NotConnectedException, NotLoggedInException, IOException, NotLoadedException, StatementNotExecutedException, NoRightException {
        checkEditRights();
        ArrayList<Serialisable> information = new ArrayList<>();
        information.add(loadedProject.getProjectKey());
        information.addAll(data);
        return sendMessage(new Message(Commands.CHANGE_USER_RIGHTS, information));
    }

    /**
     * Saves the list of group rights information
     *
     * @param data
     * @return
     * @throws NotConnectedException
     * @throws NotLoggedInException
     * @throws IOException
     * @throws NotLoadedException
     * @throws StatementNotExecutedException
     * @throws NoRightException
     */
    public Message saveNewGroupRights(
            ArrayList<RightsInformation> data) throws NotConnectedException, NotLoggedInException, IOException, NotLoadedException, StatementNotExecutedException, NoRightException {
        checkEditRights();
        ArrayList<Serialisable> information = new ArrayList<>();
        information.add(loadedProject.getProjectKey());
        information.addAll(data);
        return sendMessage(new Message(Commands.CHANGE_PROJECT_RIGHTS_GROUP, information));
    }

    public Message saveUserProfile(User profile) throws IOException, NotConnectedException, NotLoggedInException {
        return sendMessage(Commands.CHANGE_PROFILE, profile);
    }

    public boolean isLoggedInGlobal() {
        return isLoggedInGlobal;
    }

    public Message sendMessageToProjectOwner(String headline, String messageString,
                                             String messageHtml) throws NotConnectedException, IOException, NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        checkWriteRights();
        ArrayList<Serialisable> data = new ArrayList<>();
        data.add(new SerialisableString("" + loadedProject.getProjectOwnerId()));
        data.add(new SerialisableString(headline));
        SerialisableArrayList<SerialisableString> list = SerialisableArrayList.makeListFromString(messageString);

        data.add(list);
        data.add(SerialisableArrayList.makeListFromString(messageHtml));
        Message m = new Message(Commands.SEND_MAIL_TO_USER, data);
        return client.sendMessage(m);
    }

    /**
     * Updates the data of the given table
     *
     * @param tableName The name of the Table to be updated
     * @throws NotLoggedInException
     * @throws StatementNotExecutedException
     * @throws NotConnectedException
     * @throws IOException
     */
    public void updateTable(
            String tableName) throws NotLoggedInException, StatementNotExecutedException, NotConnectedException, IOException {
        AbstractQueryManager localManager = getLocalManager();

        ArrayList<Serialisable> data = new ArrayList<>();
        data.add(new SerialisableString(tableName));
        data.add(new SerialisableString(localManager.getDefinitionManager().getLastSynchronisation(tableName)));
        Message message = sendMessage(new Message(Commands.REQUEST_TABLE_DATA_NEW, data));
        if (message.getResult().wasSuccessful()) {

            for (Serialisable hash : message.getData()) {
                DataTable dataSet = (DataTable) hash;

                localManager.changeDefinitionsNew(tableName, dataSet);
            }
        } else {
            Footer.displayError(message.getResult().getErrorMessage());
        }

    }

    /**
     * Updates the project right table with the server.
     *
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws NotConnectedException
     * @throws IOException                   If a network error occurred
     * @throws StatementNotExecutedException If a sql error occurred
     */
    public void updateProjectRights() throws NotLoggedInException, NotConnectedException, IOException, StatementNotExecutedException {
        String table = getDbName() + "." + ProjectRightManager.TABLENAME_PROJECTRIGHT;
        updateTable(table);
    }

    /**
     * Updates the project right group table with the server.
     *
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws NotConnectedException
     * @throws IOException                   If a network error occurred
     * @throws StatementNotExecutedException If a sql error occurred
     */
    public void updateProjectRightsGroup() throws NotLoggedInException, NotConnectedException, IOException, StatementNotExecutedException {
        String table = getDbName() + "." + GroupManager.TABLENAME_PROJECT_RIGHT_GROUP;
        updateTable(table);

    }

    /**
     * Updates the project right group table with the server.
     *
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws NotConnectedException
     * @throws IOException                   If a network error occurred
     * @throws StatementNotExecutedException If a sql error occurred
     */
    public void updateGroup() throws NotLoggedInException, NotConnectedException, IOException, StatementNotExecutedException {
        String table = getDbName() + "." + GroupManager.TABLENAME_GROUP;
        updateTable(table);

    }

    /**
     * Updates the project right group table with the server.
     *
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws NotConnectedException
     * @throws IOException                   If a network error occurred
     * @throws StatementNotExecutedException If a sql error occurred
     */
    public void updateGroupRights() throws NotLoggedInException, NotConnectedException, IOException, StatementNotExecutedException {
        String table = getDbName() + "." + GroupManager.TABLENAME_GROUP_RIGHTS;
        updateTable(table);
    }

    /**
     * Updates the project right group table with the server.
     *
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws NotConnectedException
     * @throws IOException                   If a network error occurred
     * @throws StatementNotExecutedException If a sql error occurred
     */
    public void updateGroupUser() throws NotLoggedInException, NotConnectedException, IOException, StatementNotExecutedException {

        String table = getDbName() + "." + GroupManager.TABLENAME_GROUP_USER;
        updateTable(table);

    }

    public void updateUserTables() throws NotLoggedInException, StatementNotExecutedException, NotConnectedException, IOException {

        String table = UserManager.TABLENAME_USER;
        updateTable(table);
    }

    public void updateLanguageTables() throws NotLoggedInException, StatementNotExecutedException, NotConnectedException, IOException {

        String table = LanguageManager.TABLENAME_LANGUAGE;

        updateTable(table);
    }

    /**
     * Tries to updateExtention CodeTables.
     *
     * @param listener
     * @param all
     */
    public void updateEntries(PropertyChangeListener listener, boolean all) {
        CodeTablesSynchronisation codeTablesSync = new CodeTablesSynchronisation(this, all);
        codeTablesSync.addPropertyChangeListener(listener);
        codeTablesSync.execute();
    }

    /**
     * Tries to translate the latest database updates and applies them on the
     * local database.
     *
     * @param listener
     * @return
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     */
    public ResultType updateDatabase(PropertyChangeListener listener)
            throws NotLoggedInException, StatementNotExecutedException {

        //check if general tables are there
        if (AbstractConfiguration.sharedUserTable) {
            if (getLocalManager().getUpdateManager().existsGeneral()) {
                String dbVersion = getLocalManager().getDatabaseManager().getGeneralDatabaseVersion();
                System.out.println("general: " + dbVersion);
                System.out.println("program: " + XBookConfiguration.GENERAL_DATABASE_VERSION);
                if (!dbVersion.equals(XBookConfiguration.GENERAL_DATABASE_VERSION)) {
                    try {
                        do {
                            ArrayList<String> updateCommand = new ArrayList<>();
                            Message message = client.sendMessage(
                                    new Message(Commands.REQUEST_GENERAL_UPDATE, dbVersion));//
                            for (Serialisable data : message.getData()) {
                                updateCommand.add(((SerialisableString) data).getString());
                            }
                            getLocalManager().getUpdateManager().applyUpdate(updateCommand,
                                    IStandardColumnTypes.DATABASE_NAME_GENERAL);
                            dbVersion = getLocalManager().getDatabaseManager().getGeneralDatabaseVersion();

                        } while (!dbVersion.equals(XBookConfiguration.GENERAL_DATABASE_VERSION));
                    } catch (NotLoggedInException | NotConnectedException ex) {
                        LOGGER.error(ex.getLocalizedMessage(), ex);
                        return ResultType.UPDATE_FAILED;
                    }
                }
            } else {
                getLocalManager().createDBUpdateLocalManager().grantRights(userName, password);
            }
        }

        if (getLocalManager().getUpdateManager().isDatabaseInitialized()) {//Database initiallized updateExtention

            // global = queryManager.createSyncManager();
            String dbVersion = getLocalManager().getDatabaseManager().getDatabaseVersion();

            if (!dbVersion.equals(bookDatabaseVersion)) {
                try {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            if (!XBookConfiguration.configXBook.getBoolean(XBookConfiguration.FIRST_DB_UPDATE_DONE,
                                    true)) {
                                displayFirstDbUpdateMessage();
                            }
                        }
                    });

                    do {
                        ArrayList<String> updateCommand = new ArrayList<>();
                        Message message = client.sendMessage(new Message(Commands.REQUEST_UPDATE, dbVersion));//
                        for (Serialisable data : message.getData()) {
                            updateCommand.add(((SerialisableString) data).getString());
                        }
                        if (updateCommand.isEmpty()) {
                            System.out.println("update empty");
                            return ResultType.UPDATE_FAILED;
                        }
                        getLocalManager().getUpdateManager().applyUpdate(updateCommand, getDbName());
                        dbVersion = getLocalManager().getDatabaseManager().getDatabaseVersion();
                    } while (!dbVersion.equals(bookDatabaseVersion));
                    XBookConfiguration.configXBook.putBoolean(XBookConfiguration.FIRST_DB_UPDATE_DONE, true);
                } catch (NotLoggedInException | NotConnectedException ex) {
                    LOGGER.error(ex.getLocalizedMessage(), ex);
                    return ResultType.UPDATE_FAILED;
                }
            } else {
                System.out.println("dbversion: " + dbVersion);
                System.out.println("dbversionProgr: " + bookDatabaseVersion);
            }
        }
        //get complete scheme anyways
        DatabaseScheme scheme = getDatabaseScheme();
        scheme.addPropertyChangeListener(listener);
        if (XBookConfiguration.sharedUserTable) {
            scheme.setSyncGeneral(DatabaseScheme.UpdateType.ALL);
        } else {
            scheme.setSyncGeneral(DatabaseScheme.UpdateType.SPECIFIC);
        }
        scheme.execute();
        return ResultType.UPDATE_LATER;
    }

    private void displayFirstDbUpdateMessage() {
        final JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        f.setSize(new Dimension(480, 360));
        f.setLocationRelativeTo(null);
        f.setAlwaysOnTop(true);
        f.getContentPane().setLayout(new BorderLayout());
        f.getContentPane().setBackground(Colors.CONTENT_BACKGROUND);
        MultiLineTextLabel text = new MultiLineTextLabel(Loc.get("THIS_IS_THE_FIRST_INIT_OF_THE_DB"));
        text.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        text.setBackground(Colors.CONTENT_BACKGROUND);
        f.getContentPane().add(BorderLayout.CENTER, text);

        XButton okButton = new XButton(Loc.get("OK"));
        okButton.setPreferredSize(new Dimension(Sizes.BUTTON_WIDTH, Sizes.BUTTON_HEIGHT));
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.setVisible(false);
            }
        });
        JPanel wrapper = new JPanel(new FlowLayout());
        wrapper.setBackground(Colors.CONTENT_BACKGROUND);
        wrapper.add(okButton);
        f.getContentPane().add(BorderLayout.SOUTH, wrapper);

        f.setVisible(true);
    }

    @Override
    public void logout() {
        try {
            System.out.println("trying to logout on server");
            if (isLoggedInGlobal) {
                System.out.println("sending logout to server");
                sendMessageWithString(Commands.LOGOUT);
            }
            System.out.println("trying to logout on server");

        } catch (NotLoggedInException | NotConnectedException | IOException ex) {
        }
        isLoggedInGlobal = false;
        super.logout();

        Footer.setConflictedButtonVisible(false);
    }

    /**
     * @param o
     * @param arg0
     * @param arg1
     */
    public void updateExtention(Observable o, Object arg0, Object arg1) {
        update(o, arg0);
        if (arg0 instanceof ResultType) {
            if (arg1 instanceof AbstractContent) {
                switch ((ResultType) arg0) {
                    case CONTINUE_TO_APPLICATION: {
                        try {
                            //update language and user
                            getLocalManager().getLanguageManager().getCurrentLanguageID();
                            Footer.updateUserLabel(getLocalManager().getUserManager().getCurrentUser(true));
                        } catch (NotLoggedInException | StatementNotExecutedException ex) {
                            LOGGER.error(ex, ex);
                        }
                    }

                    Content.setContent((AbstractContent) arg1);
                    break;
                }
            }
        }
    }

    /**
     * Returns the Database Scheme for the Database. Must be overriden to ensure
     * that the Scheme is correct for this "Book"
     *
     * @return
     */
    protected DatabaseScheme getDatabaseScheme() {
        return new DatabaseScheme(this);
    }

    public ArrayList<String> getEntryDataTables() {
        return getDatabaseScheme().getDefinitionTableNames();
    }

    public final boolean hasReadRights() throws NotLoggedInException, NotLoadedException, StatementNotExecutedException {
        if (loadedProject == null) {
            throw new NotLoadedException();
        }
        return hasReadRights(loadedProject);

    }

    @Override
    public boolean hasReadRights(ProjectDataSet project) throws NotLoggedInException, StatementNotExecutedException {
        AbstractQueryManager m = getLocalManager();
        return (m.getProjectRightManager().hasReadRights(project.getProjectKey())
                || m.getGroupManager().hasReadRights(project.getProjectKey()) || isProjectOwner(project));
    }

    @Override
    public boolean hasWriteRights(
            boolean ignoreNotLoadedException) throws NotLoggedInException, NotLoadedException, StatementNotExecutedException {
        if (!ignoreNotLoadedException && loadedProject == null) {
            throw new NotLoadedException();
        }
        return hasWriteRights(loadedProject);
    }

    @Override
    public boolean hasWriteRights(ProjectDataSet project) throws NotLoggedInException, StatementNotExecutedException {
        AbstractQueryManager m = getLocalManager();
        return (m.getProjectRightManager().hasWriteRights(project.getProjectKey())
                || m.getGroupManager().hasWriteRights(project.getProjectKey()) || isProjectOwner(project));
    }

    @Override
    public final boolean hasEditRights() throws NotLoggedInException, NotLoadedException, StatementNotExecutedException {
        if (loadedProject == null) {
            throw new NotLoadedException();
        }
        return hasEditRights(loadedProject);
    }

    @Override
    public boolean hasEditRights(ProjectDataSet project) throws NotLoggedInException, StatementNotExecutedException {
        T m = getLocalManager();
        return (m.getProjectRightManager().hasEditProjectRights(project.getProjectKey())
                || m.getGroupManager().hasEditProjectRights(project.getProjectKey()) || isProjectOwner(project));
    }

    public boolean hasEditGroupRights(
            int groupId) throws NotLoggedInException, NotLoadedException, StatementNotExecutedException {
        return (getLocalManager().getGroupManager().hasEditGroupRights(groupId));
    }

    /**
     * Checks if a project is loaded, if there is a connection and if the user
     * has the right to read the current project
     *
     * @throws NotLoggedInException          If the user is not logged in. If there is no
     *                                       connection to a database
     * @throws NotLoadedException            If no Project is loaded If no project was
     *                                       loaded
     * @throws StatementNotExecutedException If a sql error occurred If a SQL
     *                                       error appeared
     * @throws NoRightException              If the user has no right to read the current
     *                                       AbstractProjectOverviewScreen
     */
    protected final void checkReadRights()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {

        if (loadedProject == null) {
            throw new NotLoadedException();
        }
        checkReadRights(loadedProject.getProjectKey());
    }

    /**
     * Checks if a project is loaded, if there is a connection and if the user
     * has the right to read the current project
     *
     * @param projectkey The project to check the status for
     * @throws NotLoggedInException          If the user is not logged in. If there is no
     *                                       connection to a database
     * @throws StatementNotExecutedException If a sql error occurred If a SQL
     *                                       error appeared
     * @throws NoRightException              If the user has no right to read the current
     *                                       AbstractProjectOverviewScreen
     */
    protected void checkReadRights(Key projectkey)
            throws NotLoggedInException, StatementNotExecutedException, NoRightException {
        if (getLocalManager() == null) {
            throw new NotLoggedInException();
        }
        if (!getLocalManager().getProjectRightManager().hasReadRights(
                projectkey) && !getLocalManager().getGroupManager().hasReadRights(projectkey) && !isProjectOwner(
                projectkey)) {
            throw new NoRightException(NoRightException.Rights.NOREAD);
        }
    }

    /**
     * Checks if a project is loaded, if there is a connection and if the user
     * has the right to read the current project
     *
     * @param project The project to check the status for
     * @throws NotLoggedInException          If the user is not logged in. If there is no
     *                                       connection to a database
     * @throws StatementNotExecutedException If a sql error occurred If a SQL
     *                                       error appeared
     * @throws NoRightException              If the user has no right to read the current
     *                                       AbstractProjectOverviewScreen
     */
    protected final void checkReadRights(ProjectDataSet project)
            throws NotLoggedInException, StatementNotExecutedException, NoRightException {
        if (getLocalManager() == null) {
            throw new NotLoggedInException();
        }
        checkReadRights(project.getProjectKey());
    }

    /**
     * Checks if a project is loaded, if there is a connection and if the user
     * has the right to read the current project
     *
     * @throws NotLoggedInException          If the user is not logged in. If there is no
     *                                       connection to a database
     * @throws NotLoadedException            If no Project is loaded If no project was
     *                                       loaded
     * @throws StatementNotExecutedException If a sql error occurred If a SQL
     *                                       error appeared
     * @throws NoRightException              If the user has no right to read the current
     *                                       AbstractProjectOverviewScreen
     */
    protected final void checkEditRights()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {

        if (loadedProject == null) {
            throw new NotLoadedException();
        }
        checkEditRights(loadedProject);
    }

    /**
     * Checks if a project is loaded, if there is a connection and if the user
     * has the right to read the current project
     *
     * @param project The project to check the status for
     * @throws NotLoggedInException          If the user is not logged in. If there is no
     *                                       connection to a database
     * @throws StatementNotExecutedException If a sql error occurred If a SQL
     *                                       error appeared
     * @throws NoRightException              If the user has no right to read the current
     *                                       AbstractProjectOverviewScreen
     */
    protected void checkEditRights(ProjectDataSet project)
            throws NotLoggedInException, StatementNotExecutedException, NoRightException {
        if (getLocalManager() == null) {
            throw new NotLoggedInException();
        }
        if (!getLocalManager().getProjectRightManager().hasEditProjectRights(
                project.getProjectKey()) && !getLocalManager().getGroupManager().hasEditProjectRights(
                project.getProjectKey()) && !isProjectOwner(project)) {
            throw new NoRightException(NoRightException.Rights.NOPROJECTOWNER);
        }
    }

    /**
     * Checks if a project is loaded, if there is a connection and if the user
     * has the right to write in the current project
     *
     * @throws NotLoggedInException          If the user is not logged in. If there is no
     *                                       connection to a database
     * @throws NotLoadedException            If no Project is loaded If no project was
     *                                       loaded
     * @throws StatementNotExecutedException If a sql error occurred If a SQL
     *                                       error appeared
     * @throws NoRightException              If the user has no right to read the current
     *                                       AbstractProjectOverviewScreen
     */
    protected final void checkWriteRights()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        if (loadedProject == null) {
            throw new NotLoadedException();
        }
        checkWriteRights(loadedProject);
    }

    protected void checkWriteRights(ProjectDataSet project)
            throws NotLoggedInException, StatementNotExecutedException, NoRightException {
        T m = getLocalManager();
        if (m == null) {
            throw new NotLoggedInException();
        }

        if (!m.getProjectRightManager().hasWriteRights(project.getProjectKey()) && !m.getGroupManager().hasWriteRights(
                project.getProjectKey()) && !isProjectOwner(project)) {
            throw new NoRightException(NoRightException.Rights.NOREAD);
        }
    }

    @Override
    public ArrayList<RightsInformation> getNewUserRights() throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        checkEditRights();
        return getLocalManager().getProjectRightManager().getUserRights(loadedProject.getProjectKey());
    }

    @Override
    public ArrayList<RightsInformation> getNewGroupRights() throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        checkEditRights();
        return getLocalManager().getGroupManager().getGroupRights(loadedProject.getProjectKey());
    }

    public ArrayList<String> getPrimaryColumns(
            String tableName) throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getSchemeManager().getPrimaryKeys(tableName);
    }

    public ArrayList<ArrayList<String>> getCodeTableData(String tableName) throws NotLoggedInException {
        return getLocalManager().getCodeTableManager().getTableContents(tableName);
    }

    public PlannedSynchronisation getPlannedSynchronisation() {
        return plannedSynchronisation;
    }

    public File downloadFile(
            String filename) throws NotLoadedException, NotLoggedInException, StatementNotExecutedException, NoRightException, NotConnectedException, IOException {
        if (loadedProject == null) {
            throw new NotLoadedException();
        }
       return downloadFile(filename, loadedProject.getProjectKey());
    }

    public void downloadAndOpenFile(
            String filename) throws NotLoadedException, NotLoggedInException, StatementNotExecutedException, NoRightException, NotConnectedException, IOException {
        if (loadedProject == null) {
            throw new NotLoadedException();
        }
        downloadAndOpenFile(filename, loadedProject.getProjectKey());
    }

    /**
     * Downloads the specified file from the server to a temporary directory and opens it. If the file is already downloaded
     * the user is asked if he wants to download the file again.
     *
     * @param filename   The name of the file to download
     * @param projectKey The Project key specifying the project the file belongs to
     * @throws NotLoggedInException
     * @throws NoRightException
     * @throws StatementNotExecutedException
     * @throws NotConnectedException
     * @throws IOException
     */
    public File downloadFile(String filename,
                             Key projectKey) throws NotLoggedInException, NoRightException, StatementNotExecutedException, NotConnectedException, IOException {
        checkReadRights(projectKey);

        final File file = new File(
                XBookConfiguration.TMP_DOWNLOAD + "/" + projectKey.getID() + "-" + projectKey.getDBID() + "-" + filename);

        boolean downloadFile = true;
        if (file.exists()) {

            //TODO aks if override file
            int dialogResult = JOptionPane.showConfirmDialog(mainFrame, Loc.get("FILE_DOWNLOAD_FILE_EXISTS"),
                    Loc.get("FILE_DOWNLOAD_FILE_EXISTS_HEADER"), JOptionPane.YES_NO_OPTION);
            if (dialogResult != JOptionPane.YES_OPTION) {
                downloadFile = false;
            }

        }
        if (downloadFile) {
            final Message message = new Message(Commands.DOWNLOAD_FILE);
            message.getData().add(projectKey);
            message.getData().add(new SerialisableString(filename));
            final Message response = sendMessage(message);
            if (response.wasSuccessfull()) {

                final SerialisableString serialisable = (SerialisableString) response.getData().get(0);
                final String string = serialisable.getString();
                final byte[] bytes = DatatypeConverter.parseBase64Binary(string);
                Files.write(file.toPath(), bytes);


            } else {
                Footer.displayError(response.getResult().getErrorMessage());
                return null;
            }
        }
        return file;
    }

    public void downloadAndOpenFile(String filename, Key projectKey) throws NotLoggedInException,
            NoRightException, StatementNotExecutedException, NotConnectedException, IOException {

        File file = downloadFile(filename, projectKey);
        if(file == null){
            return;
        }
        LOGGER.debug(file.toPath());
        Desktop desktop = Desktop.getDesktop();
        if (file.exists()) {
            desktop.open(file);
        }

    }

    /**
     * @throws NotLoggedInException
     * @throws NoRightException
     * @throws StatementNotExecutedException
     * @throws NotConnectedException
     * @throws IOException
     */
    public void archiveFiles(
            ) throws NotLoggedInException, NoRightException, StatementNotExecutedException, NotConnectedException, IOException {

        checkReadRights(loadedProject);


        final Message message = new Message(Commands.ARCHIVE_FILES);
        message.getData().add(loadedProject.getProjectKey());
        final Message response = sendMessage(message);
        if (response.wasSuccessfull()) {

            final SerialisableString serialisable = (SerialisableString) response.getData().get(0);
            final String string = serialisable.getString();
            try {

                Desktop.getDesktop().browse(
                        new URI(XBookConfiguration.getWebAppURL() + "download.php?identifier=" + string));
            } catch (URISyntaxException e) {
                LOGGER.error(e, e);
                Footer.displayError(Loc.get("ERROR_WHILE_ARCHIVING_DATA"));

            }

        } else {
            Footer.displayError(response.getResult().getErrorMessage());
            return;
        }

    }
}
