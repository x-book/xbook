package de.uni_muenchen.vetmed.xbook.implementation.xbook.controller;

import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.plugin.IPlugin;
import de.uni_muenchen.vetmed.xbook.api.plugin.PluginDatamanager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */
public class PluginController {
    protected ProjectDataSet project;
    protected HashMap<String, IPlugin> plugins;
    protected ArrayList<PluginDatamanager> datamanagers = new ArrayList<>();
    
     /**
     * Adds a Data Manager to the List of Data Managers
     *
     * @param manager
     */
    public void addDatamanager(PluginDatamanager manager) {
        datamanagers.add(manager);
    }
    
    /**
     * Removes the given Data Manager from the list of Data Managers
     *
     * @param manager
     */
    public void removeDatamanager(PluginDatamanager manager) {
        datamanagers.remove(manager);
    }
}
