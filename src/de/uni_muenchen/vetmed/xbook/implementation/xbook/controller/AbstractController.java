package de.uni_muenchen.vetmed.xbook.implementation.xbook.controller;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.AbstractStaticData;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.ISynchronisationManager;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Group;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rank;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rights;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.event.CodeTableListener;
import de.uni_muenchen.vetmed.xbook.api.event.LoginListener;
import de.uni_muenchen.vetmed.xbook.api.event.ProjectEvent;
import de.uni_muenchen.vetmed.xbook.api.event.ProjectListener;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.framework.PDFWriter;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.helper.DataManagementHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.DatabaseType;
import de.uni_muenchen.vetmed.xbook.api.helper.EntryModeHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.api.plugin.IPlugin;
import de.uni_muenchen.vetmed.xbook.api.plugin.PluginDatamanager;
import de.uni_muenchen.vetmed.xbook.api.plugin.PluginInformation;
import de.uni_muenchen.vetmed.xbook.api.plugin.PluginLoader;
import de.uni_muenchen.vetmed.xbook.implementation.FeatureConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.*;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.export.AbstractExport;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.Mail;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import javax.mail.MessagingException;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * The interface for important methods of the controller which are neccessary from other parts of the application than
 * the graphical user interface (GUI).
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 * @author Alex
 */
public abstract class AbstractController<T extends AbstractQueryManager<?, ?>, E extends AbstractExport> implements
        ApiControllerAccess, ProjectListener {

    private static final Log LOGGER = LogFactory.getLog(AbstractController.class);
    /**
     * Set this to false to force code table update
     */
    boolean testCodetables = true;
    protected boolean onlyLocal = false;

    protected AbstractQueryManager createDatabaseLocalManager() throws NotLoggedInException {
        return createLocalManager("root", "");
    }

    public void startProjectExport(List<Key> keys) {
        try {
            export.exportProjects(getLocalManager().getProjectManager(), keys);
            Footer.displayConfirmation(Loc.get("EXPORT_SUCCESSFUL"));


        } catch (SQLException | IOException | NotLoggedInException | NotLoadedException | NoRightException | InvalidFormatException exception) {
            log.error("CSVEXPORT", exception);
            Footer.displayError(Loc.get("EXPORT_FAILED"));
        } finally {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Footer.hideProgressBar();
                }
            });
        }


    }


    /**
     * The result Type to indicate further action.
     */
    public enum ResultType {

        /**
         * The event when the update was successful
         */
        UPDATE_SUCCESS,
        /**
         * The event that the update failed
         */
        UPDATE_FAILED,
        /**
         * The event that the update takes place later
         */
        UPDATE_LATER,
        /**
         * The event to continue to project overview
         */
        CONTINUE_TO_PROJECT,
        /**
         * The event to continue back to the Login screen
         */
        BACK_TO_LOGIN,
        /**
         * The event to continue to the application
         */
        CONTINUE_TO_APPLICATION,
        /**
         * The event to close the application
         */
        CLOSE_APPLICATION,
        /**
         * The event to skip updating
         */
        SKIP_UPDATING_PROGRAMM
    }

    protected T queryManager;
    /**
     * The database version of the book. This is independent for all books.
     */
    protected String bookDatabaseVersion;
    /**
     * The project which is currently load. Null if no project is load.
     */
    protected ProjectDataSet loadedProject;
    /**
     * A Map of plugins mapped to their command
     */
    protected HashMap<String, IPlugin> plugins;
    /**
     * A list of all PluginDatamangers
     */
    protected ArrayList<PluginDatamanager> datamanagers = new ArrayList<>();

    /**
     * The export to export the data into .xls or .csv
     */
    protected E export;
    /**
     * The mainframe which is the central GUI hub and manages all display related methodes
     */
    protected AbstractMainFrame<?> mainFrame;
    protected static final Log log = LogFactory.getLog(AbstractController.class);

    /**
     * The 'Controller for the Plugins. Currently not in use.
     */
    private PluginController pluginController = null;
    /**
     * Status if a project is load or not.
     */
    protected boolean isProjectLoaded = false;
    /**
     * status if user is currently logged in
     */
    protected boolean isLoggedIn = false;
    /**
     * The username of the currently signed in user
     */
    protected String userName;
    /**
     * The entered password of the user
     */
    protected String password;

    protected boolean autoContinue;
    /**
     * Holds the key of the entry that is currently loaded. <code>null</code> if no entry is loaded.
     */
    protected HashMap<BaseEntryManager, EntryDataSet> loadedEntries;

    protected String databaseName;
    protected boolean updateSkipped;
    private ArrayList<CodeTableListener> codeTableListener = new ArrayList<>();
    private ArrayList<ProjectListener> projectListener = new ArrayList<>();
    private ArrayList<LoginListener> connectionListener = new ArrayList<>();
    protected FeatureConfiguration featureConfiguration;

    /**
     * Creates a new AbstractController with the given Export and Book version
     *
     * @param export              The Export to use
     * @param bookDatabaseVersion The database Verion of the given books
     */
    protected AbstractController(E export, String bookDatabaseVersion) {
        this.export = export;
        this.bookDatabaseVersion = bookDatabaseVersion;
        loadedEntries = new HashMap<>();

        plugins = new HashMap<>();
        export.setController(this);
        AbstractStaticData.setController(this);
        DataManagementHelper.setController(this);
        addCodeTableEventListener(DataManagementHelper.getCodeTableListener());
    }

    //**********event stuff*********

    /**
     * Adds the listener to the list of codeTableEvent Listeners
     *
     * @param listener The CodeTableListener
     */
    @Override
    public void addCodeTableEventListener(CodeTableListener listener) {
        codeTableListener.add(listener);
    }

    /**
     * Removes the listener from the list of CodeTableEventListener
     *
     * @param listener The CodeTableListener
     */
    public void removeCodeTableEventListener(CodeTableListener listener) {
        codeTableListener.remove(listener);
    }

    public void setAutoContinue(boolean autoContinue) {
        this.autoContinue = autoContinue;
    }

    /**
     * Called if the code tables where updated
     */
    public void codeTablesChanged() {
        for (CodeTableListener l : codeTableListener) {
            l.onCodeTablesUpdated();
        }
    }

    /**
     * Adds the listener to the list of Project listeners
     *
     * @param listener The ProjectListener
     */
    @Override
    public void addProjectListener(ProjectListener listener) {
        projectListener.add(listener);
    }

    /**
     * Removes the listener from the list of Project listeners
     *
     * @param listener The ProjectListener
     */
    public void removeProjectListener(ProjectListener listener) {
        projectListener.remove(listener);
    }

    /**
     * Called if the project is loaded, to notify listeners with the given event.
     */
    public void onProjectLoaded(ProjectEvent evt) {
        for (ProjectListener l : projectListener) {
            l.onProjectLoaded(evt);
        }
    }


    @Override
    public void onProjectAdded(ProjectEvent evt) {
        for (ProjectListener l : projectListener) {
            l.onProjectAdded(evt);
        }
    }

    @Override
    public void onProjectChanged(ProjectEvent evt) {
        for (ProjectListener l : projectListener) {
            l.onProjectChanged(evt);
        }
    }

    /**
     * Called if the project is unloaded, to notify listener with the given event.
     *
     * @param evt the event
     */
    public void onProjectUnloaded(ProjectEvent evt) {
        for (ProjectListener l : projectListener) {
            l.onProjectUnloaded(evt);
        }
    }

    public void onProjectDeleted(ProjectEvent evt) {
        for (ProjectListener l : projectListener) {
            l.onProjectDeleted(evt);
        }
    }

    /**
     * Adds the listener to the list of ConnectionListeners
     */
    @Override
    public void addLoginListener(LoginListener listener) {
        connectionListener.add(listener);
    }

    /**
     * Removes the listener from the list of connection Listeners
     *
     * @param listener The listener to be removed
     */
    public void removeLoginListener(LoginListener listener) {
        connectionListener.remove(listener);
    }

    /**
     * Called to notify listener of the login
     */
    public void onLogin() {
        for (LoginListener l : connectionListener) {
            l.onLogin();
        }
    }

    /**
     * Called to notify listener of the logout
     */
    public void onLogout() {
        for (LoginListener l : connectionListener) {
            l.onLogout();
        }
    }
    //**********end event stuff **********

    /**
     * Adds a Data Manager to the List of Data Managers
     */
    public void addDatamanager(PluginDatamanager manager) {
        datamanagers.add(manager);
    }

    /**
     * Changes the password for the currently logged in user from the old password to the new password
     *
     * @param passwordOld The old password, used to verify the user knows the pw
     * @param passwordNew The new password
     */
    public Message changePassword(String passwordOld,
                                  String passwordNew) throws IOException, NotLoggedInException, NotConnectedException {
        //todo
        return null;
    }

    /**
     * Checks whether the Sync Indicator is set. Can be used for blocking codeTablesSync or forcing updateExtention
     *
     * @return <code>true</code> if the sync indicator is set,
     * <code>false</code>if nots
     * @throws NotLoggedInException If the user is not logged in.
     */
    public boolean checkSyncIndicator() throws StatementNotExecutedException, NotLoggedInException {
        return getLocalManager().checkSyncIndicator();
    }

    /**
     * Creates and returns a new Local Manager with the given name and Password. This can be used for temporal Managers
     *
     * @throws NotLoggedInException If the user is not logged in.
     */
    protected abstract T createLocalManager(String name, String password) throws NotLoggedInException;

    /**
     * Creates a new local manager with the already entered name and password
     *
     * @throws NotLoggedInException If the user is not logged in.
     */
    protected final void createLocalManager() throws NotLoggedInException {
        queryManager = createLocalManager(userName, password);
        queryManager.getProjectManager().addProjectListener(this);
    }

    /**
     * Checks if there is a connection and if the user has the right to read the
     * given project
     *
     * @param project The project to check the status for
     * @throws NotLoggedInException If the user is not logged in. If there is no
     * connection to a database
     * @throws StatementNotExecutedException If a sql error occurred If a SQL
     * error appeared
     * @throws NoRightException If the user has no right to read the current
     * AbstractProjectOverviewScreen
     */
    /**
     * Returns the Database Version of the current book.
     *
     * @return The Database Version of the current book.
     */
    public String getBookDatabaseVersion() {
        return bookDatabaseVersion;
    }

    /**
     * Deactivates a plugin with the given command.
     *
     * @param command The command of the plugin to be deactivated
     */
    public void deactivatePlugin(String command) {
        plugins.get(command).deactivate();
    }

    /**
     * Delete an entry with a specific ID out of the current project.
     *
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     */
    @Override
    public void deleteEntry(BaseEntryManager manager, Key key)
            throws NotLoggedInException, NoRightException, StatementNotExecutedException, EntriesException,
            NotLoadedException, EntryInUseException {
        //TODO check who has delete rights
        checkWriteRights();
        checkUsageOfEntry(manager, key);
        deleteCrossLinksOfEntry(manager, key);
        manager.deleteValue(key, loadedProject.getProjectKey());
        mainFrame.getListingScreen().addUpdatedEntry(manager);
    }

    /**
     * Delete all crosslinks of the current entry.
     */
    protected void deleteCrossLinksOfEntry(BaseEntryManager manager,
                                           Key key) throws NotLoggedInException, StatementNotExecutedException {

    }

    /**
     * Check if this entry is in use and cannot be deleted, without other entries first having to change the
     * references.
     */
    protected void checkUsageOfEntry(BaseEntryManager manager,
                                     Key key) throws NotLoggedInException, StatementNotExecutedException,
            EntryInUseException {
    }

    protected void checkWriteRights(ProjectDataSet project)
            throws NotLoggedInException, StatementNotExecutedException, NoRightException {
        T m = getLocalManager();
        if (m == null) {
            throw new NotLoggedInException();
        }
    }

    /**
     * Deletes the local entry with the given Key. The entry is deleted only locally.
     *
     * @param key The Entry Key of the Entry to be deleted
     * @throws NoRightException              If the user has no rights to delete the entry
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NotLoadedException            If no Project is loaded If no Project is loaded
     */
    public void deleteLocalEntry(AbstractBaseEntryManager manager,
                                 Key key)
            throws NoRightException, NotLoggedInException, StatementNotExecutedException, NotLoadedException {
        checkWriteRights();
        manager.deletePermanent(loadedProject.getProjectKey(), key, AbstractQueryManager.DELETE_ALL);
    }

    /**
     * Deletes the given project
     *
     * @param project The AbstractProjectOverviewScreen to be deleted
     * @throws NoRightException              IF the user has no rights to delete the entry
     * @throws NotLoggedInException          If the user is not logged in. If there is no connection
     * @throws StatementNotExecutedException If a sql error occurred
     */
    public void deleteProject(ProjectDataSet project)
            throws NotConnectedException, NotLoggedInException, StatementNotExecutedException, NoRightException {
        //TODO
    }

    /**
     * Delets the given AbstractProjectOverviewScreen completly from the Local Database
     *
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     */
    public void deleteProjectLocaly(
            ProjectDataSet project)
            throws NotLoggedInException, StatementNotExecutedException, NoRightException {

        if (isProjectLoaded && loadedProject.equals(project)) {
            unloadProject();
        }
        for (IBaseManager syncint : getLocalManager().getSyncTables()) {
            syncint.deletePermanent(project, AbstractQueryManager.DELETE_ALL);
        }
    }

    /**
     * Returns all column names which can be exported.
     *
     * @return All the column names which can be exported
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NotLoggedInException          If the user is not logged in.
     */
    @Override
    public HashMap<IBaseManager, TreeSet<ColumnType>> getAvailableExportEntries()
            throws StatementNotExecutedException, NotLoggedInException {
        HashMap<IBaseManager, TreeSet<ColumnType>> map = new HashMap<>();
        for (IBaseManager inter : getLocalManager().getSyncTables()) {
            if (!(inter instanceof AbstractBaseEntryManager || inter instanceof AbstractProjectManager)) {
                continue;
            }
            TreeSet<ColumnType> list = new TreeSet<>(ColumnType.comp);
            //   AbstractBaseEntryManager entry = (AbstractBaseEntryManager) inter;
            list.addAll(inter.getDataColumns());
            for (ISynchronisationManager manager : inter.getManagers()) {
                if (!(manager instanceof AbstractExtendedEntryManager)) {
                    if (manager instanceof ExportableManager) {
                        list.addAll(((ExportableManager) manager).getExportableColumns());
                    }
                    continue;
                }
                AbstractExtendedEntryManager extendedEntryManager = (AbstractExtendedEntryManager) manager;
                list.addAll(extendedEntryManager.getDataColumns());
            }

            map.put(inter, list);
        }
        return map;

    }

    /**
     * Returns the connection to the database for custom database.
     *
     * @return The connection to the database for custom database.
     * @throws NotLoggedInException If the user is not logged in.
     */
    public Connection getConnection() throws NotLoggedInException {

        return getLocalManager().getUnderlyingConnection();
    }

//    /**
//     * Returns the rights for the current user in the current project.
//     *
//     * @return The rights for the current user in the current project.
//     * @throws StatementNotExecutedException If a sql error occurred
//     * @throws NotLoggedInException If the user is not logged in.
//     * @throws NoRightException
//     * @throws NotLoadedException If no Project is loaded
//     * @throws EntriesException
//     */
//    public UserRight getCurrentUserRights()
//            throws StatementNotExecutedException, NotLoggedInException, NoRightException, NotLoadedException,
//            EntriesException {
//        checkReadRights();
//        return getLocalManager().getUserManager().getUserRight(loadedProject);
//    }
//    /**
//     * Returns the rights for the current user on the given project.
//     *
//     * @param p
//     * @return The rights for the given user.
//     * @throws StatementNotExecutedException If a sql error occurred
//     * @throws NotLoggedInException If the user is not logged in.
//     * @throws EntriesException
//     */
//    public UserRight getCurrentUserRights(AbstractProjectOverviewScreen p)
//            throws StatementNotExecutedException, NotLoggedInException,
//            EntriesException {
//
//        return getLocalManager().getUserManager().getUserRight(p);
//    }

    /**
     * Returns all data from Plugins
     *
     * @return Retu
     */
    protected ArrayList<String> getData(String table) {
        ArrayList<String> data = new ArrayList<>();
        for (PluginDatamanager datamanager : datamanagers) {
            data.addAll(datamanager.getData(table));
        }
        return data;
    }

    @Override
    public String getDbName() throws NotLoggedInException {
        return getLocalManager().getDbName();
    }

    @Override
    public CodeTableHashMap getHashedCodeTableEntries(
            ColumnType columnType) throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getCodeTableManager().getAllEntrys(columnType);
    }

    /**
     * Returns the list of all values, mapped to the value of the id.
     */
    public CodeTableHashMap getHashedCodeTableEntriesByValue(
            ColumnType columnType) throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getCodeTableManager().getAllEntrysForValue(columnType);
    }

    /**
     * Returns the Language Manager
     *
     * @return the Language Manager
     * @throws NotLoggedInException If the user is not logged in.
     */
    public LanguageManager getLanguageManager() throws NotLoggedInException {
        return getLocalManager().getLanguageManager();
    }

    /**
     * Returns the Local Manager
     *
     * @return The Local Manager
     * @throws NotLoggedInException If the user is not logged in.
     */
    public final T getLocalManager() throws NotLoggedInException {
        if (queryManager == null) {
            throw new NotLoggedInException(false);
        }
        return queryManager;
    }

    /**
     * Returns the local prgogramm version.
     *
     * @return The local prgogramm version.
     */
    public String getLocalProgrammVersion() {
        return XBookConfiguration.PROGRAM_VERSION;
    }

    /**
     * Returns the MainFrame
     *
     * @return The MainFrame
     */
    public AbstractMainFrame getMainFrame() {
        return mainFrame;
    }

    @Override
    public int getNumberOfConflictedEntries(ProjectDataSet project)
            throws NotLoggedInException, StatementNotExecutedException, NoRightException, EntriesException {
        checkReadRights(project);
        int entryCount = 0;
        for (IBaseManager baseManager : getLocalManager().getSyncTables()) {
            entryCount += baseManager.getNumberOfConflictedEntries(project.getProjectKey());
        }
        return entryCount;
    }

    /**
     * Returns a List of Plugin Informations.
     */
    public ArrayList<PluginInformation> getPluginInformation() {

        ArrayList<PluginInformation> al = new ArrayList<>();
        File dir = new File(
                System.getProperty("user.dir") + System.getProperty("file.separator") + "plugins");

        if (!dir.exists()) {
            return al;
        }
        for (File file : dir.listFiles()) {
            try {
                if (file.isDirectory()) {
                    continue;
                }

                Class<IPlugin> clasz = PluginLoader.loadPlugin(file.getName());
                IPlugin plugin = clasz.newInstance();
                PluginInformation info = plugin.getPluginInformation();
                if (!isPluginValidForBook(info.getBook())) {
                    continue;
                }
                String command = info.getName();
                while (plugins.containsKey(command)) {
                    command += "1";
                }
                info.setActionCommand(command);
                plugins.put(command, plugin);
                al.add(info);

            } catch (InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(AbstractController.class
                        .getName()).log(Level.SEVERE, null, ex);
            }

        }
        return al;
    }

    /**
     * Returns the name of the project with the given ID and database number.
     *
     * @return The name of the project with the given ID and database number
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     */
    public String getProjectName(
            Key projectKey) throws NotLoggedInException, StatementNotExecutedException, EntriesException {
        return getLocalManager().getProjectManager().getProjectName(projectKey);
    }

    @Override
    public ArrayList<ProjectDataSet> getProjects()
            throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getProjectManager().getProjects();
    }

    public ArrayList<ProjectDataSet> getProjects(
            boolean showHidden) throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getProjectManager().getProjects(showHidden);
    }

    @Override
    public void hideProject(Key projectKey, boolean hide)
            throws NotLoggedInException, StatementNotExecutedException {
        getLocalManager().getProjectManager().setProjectHidden(projectKey, hide);
    }

    @Override
    public int getUserID() throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getUserManager().getCurrentUserId();
    }

    /**
     * Returns the name of the user who is logged in.
     *
     * @return The name of the user who is logged in.
     * @throws NotLoggedInException If the user is not logged in.
     */
    public String getUserName() throws NotLoggedInException {
        return getLocalManager().getUsername();
    }

    /**
     * Returns the name of a user with a specific id.
     *
     * @param userId The ID of the user.
     * @return The name of the user who is logged in.
     * @throws NotLoggedInException If the user is not logged in.
     */
    public String getUserName(int userId) throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getUserManager().getDisplayName(userId);
    }

    @Override
    public String getDisplayName(int id) throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getUserManager().getDisplayName(id);
    }

    /**
     * Returns a list of all Display names saved in the database.
     *
     * @return A list of all user names saved in the database.
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NotLoggedInException          If the user is not logged in.
     */
    public ArrayList<String> getDisplayNames()
            throws StatementNotExecutedException, NotLoggedInException {
        return getLocalManager().getUserManager().getDisplayNames();
    }

    @Override
    public boolean isAdmin() throws StatementNotExecutedException, NotLoggedInException {
        return getLocalManager().getUserManager().isAdmin();
    }

    @Override
    public boolean isDeveloper() throws StatementNotExecutedException, NotLoggedInException {
        return getLocalManager().getUserManager().isDeveloper();
    }

    /**
     * Returns whether there is a connection to the database or not.
     *
     * @return Whether there is a connection to the database or not.
     */
    public boolean isConnectedToDatabase() {
        try {
            return (getLocalManager() != null);
        } catch (NotLoggedInException ex) {
            return false;
        }
    }

    @Override
    public boolean isCurrentProjectConflicted()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        checkWriteRights();
        boolean isConflicted = false;
        for (IBaseManager syncInt : getLocalManager().getSyncTables()) {
            isConflicted |= syncInt.isConflicted(loadedProject.getProjectKey());
        }
        if (isConflicted) {
            XBookConfiguration.setConflict(loadedProject.getProjectKey(), true);
            Footer.setConflictedButtonVisible(true);
        }
        System.out.println("isConflicted: " + isConflicted);
        return isConflicted;
    }

    /**
     * Returns whether any entry is loaded
     *
     * @return whether any entry is loaded
     */
    public boolean isEntryLoaded() {
        return !loadedEntries.isEmpty();
    }

    /**
     * Returns whether a specific entry is loaded
     *
     * @return whether a specific entry is loaded
     */
    public boolean isEntryLoaded(BaseEntryManager manager, EntryDataSet entryDataSet) {
        return isProjectLoaded() && isEntryLoaded() && loadedEntries.get(
                manager) != null && entryDataSet.getEntryKey().equals(
                loadedEntries.get(manager).getEntryKey()) && entryDataSet.getProjectKey().equals(
                loadedProject.getProjectKey());
    }

    /**
     * Returns whether the local database is Initialized
     *
     * @return whether the local database is Initialized
     */
    public boolean isInitialized() {
        return false;
    }

    @Override
    public boolean isProjectLoaded() {
        return isProjectLoaded;
    }

    /**
     * Returns if the current user is owner of the current project or not.
     *
     * @return <code>true</code> if the current user is owner of the * project,
     * <code>false</code> else.
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     */
    public boolean isProjectOwner(ProjectDataSet p)
            throws NotLoggedInException, StatementNotExecutedException {
        return p.getProjectOwnerId() == getUserID();
    }

    /**
     * Returns if the current user is owner of the current project or not.
     *
     * @return <code>true</code> if the current user is owner of the * project,
     * <code>false</code> else.
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     */
    public boolean isProjectOwner(Key projectKey)
            throws NotLoggedInException, StatementNotExecutedException {
        try {
            return getLocalManager().getProjectManager().getProjectOwner(projectKey) == getUserID();
        } catch (EntriesException e) {
            return false;
        }
    }

    @Override
    public boolean isProjectOwner() throws NotLoggedInException, StatementNotExecutedException {
        if (loadedProject == null) {
            throw new NotLoggedInException();
        }
        return loadedProject.getProjectOwnerId() == getUserID();

    }

    /**
     * Loads the data of the given entry of the loaded project.
     *
     * @return The data and column names as an String Array
     * @throws NotLoggedInException          If the user is not logged in. If there is no Connection
     * @throws NoRightException              If The user has no right to read
     * @throws StatementNotExecutedException If a sql error occurred If there is a Error with the connection
     * @throws NotLoadedException            If no Project is loaded If no Projekt was loaded
     */
    public EntryDataSet loadEntry(AbstractBaseEntryManager manager,
                                  Key entryKey)
            throws NotLoggedInException, NoRightException, StatementNotExecutedException, NotLoadedException,
            EntriesException {
        // necessary for correct sidebar information for input fields
        EntryModeHelper.setToSingleEntryMode();

        checkWriteRights(loadedProject);
        EntryDataSet data = new EntryDataSet(entryKey, loadedProject.getProjectKey(), getDbName(),
                manager.tableName);
        manager.load(data);
        return data;
    }

    /**
     * Loads the data of the given entry of the loaded project.
     *
     * @return The data and column names as an String Array
     * @throws NotLoggedInException          If the user is not logged in. If there is no Connection
     * @throws NoRightException              If The user has no right to read
     * @throws StatementNotExecutedException If a sql error occurred If there is a Error with the connection
     * @throws NotLoadedException            If no Project is loaded If no Projekt was loaded
     */
    public EntryDataSet loadEntryBase(AbstractBaseEntryManager manager, Key entryKey)
            throws NotLoggedInException,
            NoRightException, StatementNotExecutedException, NotLoadedException, EntriesException {
        // necessary for correct sidebar information for input fields
        EntryModeHelper.setToSingleEntryMode();

        checkWriteRights(loadedProject);
        EntryDataSet data = new EntryDataSet(entryKey, loadedProject.getProjectKey(), getDbName(),
                manager.tableName);
        manager.loadBase(data);
        return data;
    }

    @Override
    public void loadAndDisplayEntry(BaseEntryManager manager, Key entryKey, boolean isEditMode)
            throws NotLoggedInException, NoRightException, StatementNotExecutedException, NotLoadedException,
            EntriesException {
        loadAndDisplayEntry(manager, entryKey, loadedProject, isEditMode);
    }

    @Override
    public void loadAndDisplayEntry(BaseEntryManager manager, Key entryKey, ProjectDataSet project,
                                    boolean isEditMode)
            throws NotLoggedInException, NoRightException, StatementNotExecutedException, NotLoadedException,
            EntriesException {
        if (isEditMode) {
            checkWriteRights(project);
        } else {
            checkReadRights(project);
        }

        // necessary for correct sidebar information for input fields
        EntryModeHelper.setToSingleEntryMode();

        EntryDataSet data = new EntryDataSet(entryKey, project.getProjectKey(), getDbName(),
                manager.getTableName());
        loadedEntries.put(manager, data);
        manager.loadBase(data);

        AbstractEntry entry = getEntryForManager(GeneralInputMaskMode.SINGLE, manager);
        mainFrame.displayEntryScreenInputUnit(entry, data, isEditMode);
    }

    @Override
    public void loadProject(final ProjectDataSet project,
                            boolean displayInputMask)
            throws StatementNotExecutedException, NotLoggedInException, NoRightException {
        checkReadRights(project);
        try {
            unloadProject();
            mainFrame.unloadListing();
        } catch (Throwable what) {
            what.printStackTrace();
        }
        isProjectLoaded = true;

        if (loadedProject != null) {
            onProjectUnloaded(new ProjectEvent(loadedProject));
        }
        this.loadedProject = null;

        this.loadedProject = project;

        try {
            if (hasWriteRights(project)) {
                isCurrentProjectConflicted();
            }
        } catch (NotLoadedException e) {
            //no freakin way...
        }

        Footer.updateProjectLabel(project);
        if (displayInputMask) {
            loadScreenAfterProjectLoad();
            Footer.displayConfirmation(Loc.get("PROJECT_LOADED", project.getProjectName()));
        }

        // unloadEntry();

//        mainFrame.clearEntryFields(true);
        onProjectLoaded(new ProjectEvent(loadedProject));
        mainFrame.getNavigation().reloadNavigation();
    }

    public ProjectDataSet loadProject(Key projectKey, boolean displayInputMask)
            throws NotLoggedInException,
            NoRightException,
            StatementNotExecutedException {
        checkReadRights(projectKey);
        ProjectDataSet project = getLocalManager().getProjectManager().getProject(projectKey);
        loadProject(project, displayInputMask);

        return project;

    }

    protected void loadScreenAfterProjectLoad() {
        mainFrame.displayEntryScreenInputUnit(true, true);
    }

    /**
     * log in to the local Database, this is used after an connection with the server is established
     *
     * @throws NotLoggedInException If the user is not logged in.
     */
    protected final void localLogin() throws NotLoggedInException {
        createLocalManager();
    }

    /**
     * Use to establish a Connection to the Database with the given name, password and type and returns
     * <code>true</code> if a updateExtention is required!
     *
     * @param name     The name of the user.
     * @param password The password of the user.
     */
    public void login(String name, String password) {

        this.userName = name;
        this.password = password;
        doLogin();

    }

    protected void doLogin() {
        doLoginLocal();
    }

    private void doLoginLocal() {
        Footer.startWorking();
        try {
            //no connection to server just start local version
            System.out.println("local");
            createLocalManager();
            T localManager = getLocalManager();
            //check if current user exists if not error!
            int currentUserId = localManager.getUserManager().getCurrentUserId();
            if (currentUserId == -1) {
                throw new NotLoggedInException(NotLoggedInException.ErrorType.AccessDenied);
            }
            try {//check if database versions do match

                String databaseBookVersion = localManager.getDatabaseManager()
                        .getDatabaseVersion(); //get from database
                String databaseGeneralVersion = localManager.getDatabaseManager()
                        .getGeneralDatabaseVersion();//get from database. is the same as book if no shared users table
                String generalVersion = (AbstractConfiguration.sharedUserTable
                        ? XBookConfiguration.GENERAL_DATABASE_VERSION
                        : bookDatabaseVersion);//if shared use book version
                System.out.println("Database General: " + generalVersion + " | " + databaseGeneralVersion);
                System.out.println("Database: " + databaseBookVersion + " | " + bookDatabaseVersion);
                if (!generalVersion.equals(databaseGeneralVersion) || !databaseBookVersion
                        .equals(bookDatabaseVersion)
                        || !localManager.getUpdateManager().isDatabaseInitialized() || !localManager
                        .getUpdateManager().existsGeneral()) {
                    System.out.println("version mismatch");
                    System.out.println(!generalVersion.equals(databaseGeneralVersion));
                    System.out.println(!databaseBookVersion.equals(bookDatabaseVersion));
                    System.out.println(!localManager.getUpdateManager().isDatabaseInitialized());
                    System.out.println(!localManager.getUpdateManager().existsGeneral());
                    Footer.stopWorking();

                    Footer.displayError(Loc.get("ERROR_MESSAGE>ERROR"));
                    //todo new screen
//                    mainFrame.displayUpdateScreen(UpdatePanel.SourceType.LOGIN, false, false, false, false,
//                    autoContinue);
                    return;
                }
            } catch (StatementNotExecutedException ex) {
                Footer.stopWorking();
                ex.printStackTrace();

                Footer.displayError(Loc.get("ERROR_MESSAGE>ERROR"));
                //TODO new screen
//                mainFrame.displayUpdateScreen(UpdatePanel.SourceType.LOGIN, false, false, false, false, autoContinue);
                return;
            }
            //finaly display projects
//                checkVersion();
            //finaly display projects
//                checkVersion();

            try {
                // set footer if error just use the one we used for login...
                // should only be this time and after restart work properly so
                // no problem if this goes wrong
                Footer.updateUserLabel(localManager.getUserManager().getCurrentUser(false));
            } catch (StatementNotExecutedException ex) {
                Footer.updateUserLabel(userName);

            }
            isLoggedIn = true;

            onLogin();
            //finaly display projects
            mainFrame.displayProjectOverviewScreen();
            Footer.stopWorking();
        } catch (NotLoggedInException ex) {
            if (ex.getErrorType() == NotLoggedInException.ErrorType.DBNotInitialized) {
                Footer.displayError(Loc.get("LOGIN_FAILED_DB_NOT_INITIALISED"));
            } else if (ex.getErrorType() == NotLoggedInException.ErrorType.AccessDenied) {
                Footer.displayError(
                        Loc.get("LOGIN_OR_PASSWORD_INCORRECT") + " " + Loc
                                .get("LOGIN_OR_PASSWORD_INCORRECT_OFFLINE"));
            } else {
                Footer.displayError(Loc.get("ERROR_MESSAGE>ERROR"));
            }

            Footer.stopWorking();
        } catch (StatementNotExecutedException e) {
            Footer.displayError(Loc.get("ERROR_MESSAGE>ERROR"));
            Footer.stopWorking();
        }
    }

    @Override
    public String getDisplayName() throws NotLoggedInException, StatementNotExecutedException {
        return getDisplayName(false);
    }

    /**
     * Returns the display name of the current user
     *
     * @param force Force an update of the saved current user in the user manager.
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     */
    public String getDisplayName(boolean force)
            throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getUserManager().getCurrentUser(force);
    }

    @Override
    public void logout() {
        removeLocalManager();
        loadedProject = null;
        isProjectLoaded = false;
        isLoggedIn = false;

        Footer.updateUserLabel("");
        Footer.updateProjectLabel(null);
        onLogout();
        mainFrame.displayLoginScreen();
    }

    public void newProject(
            DataSetOld project)
            throws StatementNotExecutedException, NotLoggedInException, MissingInputException {
        newProject(project, false);
    }

    /**
     * Creates a new project with the given name.
     *
     * @return A AbstractProjectOverviewScreen object holding the information of the new project.
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NotLoggedInException          If the user is not logged in.
     */
    public void newProject(DataSetOld project, boolean silent)
            throws StatementNotExecutedException, MissingInputException, NotLoggedInException {
        String name = project.getOrCreateDataTable(AbstractProjectManager.TABLENAME_PROJECT).get(0).get(
                AbstractProjectManager.PROJECT_PROJECTNAME);
        if (name.length() > 0) {
            getLocalManager().newProject(project);
            if (!silent) {
                Footer.displayConfirmation(Loc.get("PROJECT_CREATED_SUCCESSFULLY", name));
            }
        } else {
            throw new MissingInputException();
        }

    }

    /**
     * Registers a new User with the given username, display name, password and email Adress
     *
     * @param username     The name the user uses to login
     * @param displayName  The name which is displayed for other users
     * @param organisation The name of the organisation.
     * @param password     The password used to login
     * @param email        The email adress under which the user can recieve emails
     * @throws IOException          If a network error occurred
     * @throws NotLoggedInException If the user is not logged in.
     */
    public Message register(String username, String displayName, String organisation, String password,
                            String email) throws IOException, NotConnectedException, NotLoggedInException {
        AbstractQueryManager localManager = createDatabaseLocalManager();
        try {
            final Message register = localManager.getUserManager().register(username, displayName);
            localManager.updatePassword(username, password);
            return register;
        } catch (StatementNotExecutedException e) {
            return new Message();
        }

    }

    @Override
    public void removeDatamanager(PluginDatamanager manager) {
        datamanagers.remove(manager);
    }

    /**
     * After logout, also remove the local manager, to avoid problems
     */
    protected final void removeLocalManager() {
        queryManager = null;
    }

    /**
     * Retests the password for the given username and email adress
     *
     * @param username The username for which the password shall be reseted
     * @param email    The emal adress associated with this account
     * @return The message recieved from the server
     */
    public Message resetPassword(String username,
                                 String email) throws NotConnectedException, NotLoggedInException, IOException {
        //TODO
        return null;
    }

    /**
     * Saves the entries to the database. If the entry has no EntryKey a new key is generated. The project key is
     * overwritten, so only entries for the current project can be saved
     *
     * @return <code>true</code> if the data was saved * * * * * succesfully,
     * <code>false</code> else.
     * @throws NotLoadedException            If no Project is loaded
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NotLoggedInException          If the user is not logged in.
     */
    @Override
    public boolean saveEntry(EntryDataSet entry)
            throws EntriesException, NotLoadedException, StatementNotExecutedException, NoRightException,
            NotLoggedInException {

        checkWriteRights();

        entry.setProjectKey(loadedProject.getProjectKey());
        ArrayList<String> tableNames = entry.getTableNames();
        for (IBaseManager table : getLocalManager().getSyncTables()) {
            if (!tableNames.contains(table.getTableName())) {
                continue;
            }
            if (!(table instanceof BaseEntryManager)) {
                continue;
            }
            AbstractBaseEntryManager entrydata = (AbstractBaseEntryManager) table;

            entrydata.save(entry);
            mainFrame.getListingScreen().addUpdatedEntry(entrydata);
        }

        ArrayList<ProjectDataSet> list = new ArrayList<>();
        list.add(loadedProject);

        // this causes problems when saving a InputIdDbidField (e.g. Location in
        // EB Feature) because when returning the original (feature) entry is
        // cleared. if this is somehow required think of something else...
        // unloadEntry();
        return true;
    }

    /**
     * Saves an existing project.
     *
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     */
    @Override
    public void saveProject(ProjectDataSet project, boolean notifyable)
            throws NotLoggedInException, StatementNotExecutedException, NoRightException, EntriesException,
            MissingInputException {
        // consider that some books with individual user rights have own implementations of this method!
        // e.g. limited oroject edit rights in ExcaBook

        if (!hasSaveProjectRights(project)) {
            throw new NoRightException(NoRightException.Rights.NOPROJECTOWNER);
        }
        getLocalManager().getProjectManager().save(project);
        if (project.equals(loadedProject)) {
            loadedProject = project;
            Footer.updateProjectLabel(project);
        }
        if (notifyable) {
            Footer.displayConfirmation(Loc.get("PROJECT_EDITED_SUCCESSFULLY", project.getProjectName()));
        }
    }


    /**
     * Checks if the user has Save ProjectRights
     *
     * @param projectDataSet
     * @return
     * @throws NotLoggedInException
     * @throws StatementNotExecutedException
     */
    protected boolean hasSaveProjectRights(ProjectDataSet projectDataSet)
            throws NotLoggedInException, StatementNotExecutedException {
        return hasEditRights(projectDataSet);

    }

    @Override
    public void saveProject(
            ProjectDataSet projectData)
            throws NotLoggedInException, StatementNotExecutedException, NoRightException, EntriesException,
            MissingInputException {
        saveProject(projectData, true);
    }

    protected abstract ArrayList<String> getAccountableMail();

    /**
     * Send a error message.
     *
     * @param message The message text
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NotLoggedInException          If the user is not logged in.
     */
    public void sendErrorMessage(
            String message)
            throws EntriesException, StatementNotExecutedException, NotLoggedInException, MessagingException,
            IOException {

        ArrayList<String> recipients = getAccountableMail();

        //list.add(rs.getString(MAIL));
        recipients.add("lohrer@dbs.ifi.lmu.de");
        recipients.add("kaltenthaler@dbs.ifi.lmu.de");

        Mail mail = new Mail(recipients);

        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(
                XBookConfiguration.DIR_PREFIX
                        + "log/xbooklog.zip")); FileInputStream fis = new FileInputStream(
                XBookConfiguration.DIR_PREFIX + "log/xbook.log")) {
            ZipEntry entry = new ZipEntry("xbook.log");
            zos.putNextEntry(entry);
            byte[] buffer = new byte[8192];
            int read;
            while ((read = fis.read(buffer, 0, 1024)) != -1) {
                zos.write(buffer, 0, read);
            }
            zos.closeEntry();
        }

        mail.sendErrorMessage(message, new File(XBookConfiguration.DIR_PREFIX + "log/xbooklog.zip"),
                getDbName(),
                getDisplayName() + " (" + getUserName() + ")");
//		zos.write( buffer, 0, length );

    }

    /**
     * Used for testing of sending messages.
     */
    public void sendTestMessage() {
//		FileWriter fw = null;
//		try {
//			Mail m = new Mail(new ArrayList<String> ().{"lordjoda@googlemail.com", "dakatotal@googlemail.com"},
//			"testproject");
//			File f = new File("testfile.txt");
//			fw = new FileWriter(f);
//			BufferedWriter bw = new BufferedWriter(fw);
//			bw.write("test");
//			bw.close();
//			fw.close();
//			m.sendConflictMail(f);
//		} catch (MessagingException ex) {
//			Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
//		} catch (IOException ex) {
//			Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
//		} finally {
//			try {
//				fw.close();
//			} catch (IOException ex) {
//				Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
//			}
//		}
    }

    @Override
    public void startExport(HashMap<IBaseManager, ArrayList<ColumnType>> list, ArrayList<Key> keys)
            throws NotLoggedInException, NotLoadedException, NoRightException, StatementNotExecutedException {
        try {
            if (export.exportData(loadedProject, list, keys)) {
                Footer.displayConfirmation(Loc.get("EXPORT_SUCCESSFUL"));
            }

        } catch (SQLException | IOException | NotLoggedInException | NotLoadedException | NoRightException | InvalidFormatException exception) {
            log.error("CSVEXPORT", exception);
            Footer.displayError(Loc.get("EXPORT_FAILED"));
        } finally {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Footer.hideProgressBar();
                }
            });
        }
    }

    /**
     * Starts the updater with the given file name and parameter.
     */
    public void startUpdater() {
        try {
            log.info("Updating Updater");
            URI fileUrl = new URI("http://xbook.vetmed.uni-muenchen.de/download/");
            Desktop.getDesktop().browse(fileUrl);
            exitApplication();
        } catch (IOException | URISyntaxException ex) {
            log.error(ex, ex);
        }
    }

    /**
     * Unloads the current project
     */
    public void unloadProject() {
        isProjectLoaded = false;
        ProjectDataSet tmp = loadedProject;

        // clear all input masks
        for (AbstractEntry entries : mainFrame.getAllAvailableEntryPanels()) {
            entries.clearFields(false);
        }

        loadedProject = null;
        if (tmp != null) {
            onProjectUnloaded(new ProjectEvent(tmp));
        }

        loadedEntries.clear();
        Footer.updateProjectLabel(null);
        unloadEntry();
        onProjectUnloaded(new ProjectEvent(tmp));
    }

    /**
     * Unloads the current entry.
     */
    public void unloadEntry() {
        for (AbstractEntry entryPanel : mainFrame.getAllAvailableEntryPanels()) {
            entryPanel.unsetEntryData();
        }
        loadedEntries.clear();
    }

    @Override
    public void unloadEntry(BaseEntryManager manager) {
        loadedEntries.remove(manager);
    }

    /**
     * Updates the password for the current user.
     *
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NotLoggedInException          If the user is not logged in.
     */
    protected void updateLocalPassword() throws StatementNotExecutedException, NotLoggedInException {
        AbstractQueryManager localManager = createLocalManager("root", "");
        localManager.updatePassword(userName, password);
    }

    @Override
    public ProjectDataSet getCurrentProject() throws NotLoadedException {
        if (loadedProject == null) {
            throw new NotLoadedException();
        }
        return loadedProject;
    }

    @Override
    public ArrayList<String> getInputUnitInformation(String columnName)
            throws NotLoggedInException, StatementNotExecutedException, NotLoadedException {
        if (isProjectLoaded) {
            return getLocalManager().getCodeTableManager()
                    .getInformation(columnName, loadedProject.getProjectKey());
        } else {
            throw new NotLoadedException();
        }
    }

    @Override
    public ArrayList<String> getInputUnitInformation(ColumnType columnType, Key projectKey)
            throws NotLoggedInException, StatementNotExecutedException, NotLoadedException {
        if (projectKey == null) {
            return new ArrayList<>();
        } else {
            return getLocalManager().getCodeTableManager().getInformation(columnType, projectKey);
        }
    }

    /**
     * Returns the Programmtype
     *
     * @return the Programmtype
     */
    protected abstract Commands getType();

    @Override
    public ArrayList<DataColumn> getMultiComboBoxData(String tableName, String id,
                                                      ColumnType.SortedBy sortedBy,
                                                      boolean isLanguage)
            throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getCodeTableManager()
                .getMultiComboBoxData(tableName, id, sortedBy, isLanguage);
    }

    @Override
    public ArrayList<String> getMultiComboBoxDataParents(String tableName, String id)
            throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getCodeTableManager().getMultiboxComboParents(tableName, id);
    }

    /**
     * Exits the Application
     */
    public void exitApplication() {
        System.exit(0);
    }

    @Override
    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void doActionOnSyncUpdatingCurrentEntry(BaseEntryManager manager,
                                                   EntryDataSet syncedEntry) throws NotLoggedInException {

        if (loadedEntries.get(manager) != null && !syncedEntry
                .isDatasetEqual(loadedEntries.get(manager), manager)) {
            mainFrame.displayAutoSynchronisationMessageScreen(Content.getCurrentContent(),
                    loadedEntries.get(manager).getEntryKey());
        }
    }

    @Override
    public abstract String getBookName();

    public abstract ArrayList<ColumnType> getImportColumnLabels()
            throws StatementNotExecutedException, NotLoggedInException;

    public void printRank(HashMap<String, ArrayList<DataColumn>> data, String parentID, String indent,
                          StringBuilder sb,
                          boolean printID) {
        ArrayList<DataColumn> list = data.get(parentID);
        if (list == null) {
            return;
        }
        for (DataColumn entry : list) {
            if (printID) {
                sb.append(indent).append(entry.getValue()).append(" (").append(entry.getColumnName())
                        .append(
                                ")").append("\n");

            } else {
                sb.append(indent).append(entry.getValue()).append("\n");

            }
            printRank(data, entry.getColumnName(), "---" + indent, sb, printID);

        }
    }

    @Override
    public void createPdfOfCodeTableValues(ColumnType column,
                                           boolean printID) throws NotLoggedInException, StatementNotExecutedException {

        Footer.startWorking();

        StringBuilder sb = new StringBuilder();

        switch (column.getType()) {
            case ID:
            case VALUE: {
                CodeTableHashMap data = getHashedCodeTableEntries(column);

                for (Entry<String, String> entry : data.entrySet()) {
                    if (printID) {
                        sb.append(entry.getValue()).append(" (").append(entry.getKey()).append(")")
                                .append("\n");
                    } else {
                        sb.append(entry.getValue()).append("\n");
                    }
                }
                break;
            }

            case HIERARCHIC: {
                HashMap<String, ArrayList<DataColumn>> data = getLocalManager().getCodeTableManager()
                        .getHashedMultiComboBoxData(
                                column);

                String parentID = "0";
                printRank(data, parentID, " ", sb, printID);
                break;
            }
            default: {
                System.out.println(column.getType());
            }
        }
        PDFWriter writer = new PDFWriter();
        try {
            writer.writeHeadline(column.getDisplayName());
            writer.writeText(sb.toString());

            // create the documents folder if it does not exist
            new File(XBookConfiguration.DOCUMENTS_DIR).mkdir();

            String filePath =
                    XBookConfiguration.DOCUMENTS_DIR + "/" + column.getDisplayName().replaceAll("/",
                            "") + ".pdf";
            writer.save(filePath);
            Desktop.getDesktop().open(new File(filePath));

        } catch (IOException | COSVisitorException ex) {
            Logger.getLogger(AbstractController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Footer.stopWorking();

    }

    @Override
    public ArrayList<ProjectDataSet> getProjectSearchResult(ArrayList<String> tableNames,
                                                            String where)
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        final ArrayList<ProjectDataSet> projectSearchResult = getLocalManager().getProjectManager()
                .getProjectSearchResult(
                        tableNames, where);
        final ArrayList<ProjectDataSet> projects = new ArrayList<>();
        for (ProjectDataSet projectDataSet : projectSearchResult) {
            if (hasReadRights(projectDataSet)) {
                projects.add(projectDataSet);
            }
        }
        return projects;
    }

    @Override
    public ArrayList<ColumnType> getAvailableRights() throws NotLoggedInException {
        return getLocalManager().getProjectRightManager().getRights();
    }

    @Override
    public ArrayList<ColumnType> getAvailableRightsGroup() throws NotLoggedInException {
        return getLocalManager().getGroupManager().getRights();
    }

    @Override
    public ArrayList<ColumnType> getAvailableGroupRights() throws NotLoggedInException {
        return getLocalManager().getGroupManager().getGroupRights();
    }

    @Override
    public ArrayList<User> getVisibleUsers()
            throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getUserManager().getVisibleUsers();
    }

    @Override
    public User getUserInformation(
            String username)
            throws NotLoggedInException, StatementNotExecutedException, EntriesException {
        return getLocalManager().getUserManager().getUserInformation(username);
    }

    @Override
    public ArrayList<Group> getGroups() throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getGroupManager().getGroups();
    }

    public ArrayList<Group> getGroupsOfCurrentUser()
            throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getGroupManager().getGroupsOfCurrentUser();
    }

    public ArrayList<User> getUsersOfGroup(int groupId)
            throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getGroupManager().getUsersOfGroup(groupId);
    }

    public Rank getRankOfUser(int userId,
                              int group) throws NotLoggedInException, StatementNotExecutedException, EntriesException {
        return getLocalManager().getGroupManager().getUserRank(group, userId);
    }

    @Override
    public ArrayList<Rank> getRankOfGroup(int groupId)
            throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getGroupManager().getRanks(groupId);
    }

    public ArrayList<Rights> getRankRights(int groupId,
                                           int rankId) throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getGroupManager().getRankRights(groupId, rankId);
    }

    public abstract AbstractEntry getEntryForManager(GeneralInputMaskMode m,
                                                     BaseEntryManager manager);

    /**
     * Returns all users that have any right (either through group or directly) on this project
     *
     * @return All users that have right in this project
     */
    public ArrayList<User> getUsersForProject()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        checkWriteRights();
        UniqueArrayList<User> list = new UniqueArrayList<>();
        list.addAll(getLocalManager().getGroupManager().getGroupUsersForProject(loadedProject));
        list.addAll(getLocalManager().getProjectRightManager().getUsersForProject(loadedProject));
        return list;
    }

    public User getUserProfile()
            throws NotLoggedInException, StatementNotExecutedException, EntriesException {
        return getLocalManager().getUserManager().getUserProfile();
    }

    public void addUpdatedEntry(DataSetOld project, IBaseManager manager) {
        if (project.equals(loadedProject)) {
            if (manager instanceof BaseEntryManager) {
                mainFrame.getListingScreen().addUpdatedEntry((BaseEntryManager) manager);
            }
        }
    }

    public String getValueOfCodeTableId(ColumnType columnType, String id)
            throws NotLoggedInException, EntriesException, StatementNotExecutedException {
        return getLocalManager().getCodeTableManager().getValueForID(columnType, id);
    }

    @Override
    public HashMap<Key, ArrayList<String>> getUsersWithProjectRights(Collection<Key> thisProject)
            throws NotLoggedInException, EntriesException, StatementNotExecutedException, NoRightException {
//        checkReadRights(thisProject);
        return getLocalManager().getProjectRightManager().getUsersWithProjectRights(thisProject);

    }

    @Override
    public HashMap<Key, ArrayList<String>> getGroupsWithProjectRights(Collection<Key> thisProject)
            throws NotLoggedInException, EntriesException, StatementNotExecutedException, NoRightException {
//        checkReadRights(thisProject);
        return getLocalManager().getGroupManager().getGroupsWithProjectRights(thisProject);

    }

    @Override
    public EntryDataSet getLoadedEntry(BaseEntryManager manager) {
        return loadedEntries.get(manager);
    }

    @Override
    public int getEntryCount(ProjectDataSet thisProject)
            throws NotLoggedInException, EntriesException, StatementNotExecutedException, NoRightException {
        checkReadRights(thisProject);
        int entryCount = 0;
        for (IBaseManager baseManager : getLocalManager().getSyncTables()) {
            int countManager = baseManager.getNumberofEntrys(thisProject.getProjectKey());

            entryCount += countManager;
        }

        // remove project (is no "entry" for the user)
        entryCount--;

        return entryCount;
    }

    @Override
    public HashMap<Key, Integer> getNumberOfEntries()
            throws NotLoggedInException, StatementNotExecutedException {
        HashMap<Key, Integer> keyIntegerHashMap = new HashMap<>();
        for (IBaseManager baseManager : getLocalManager().getSyncTables()) {
            HashMap<Key, Integer> entryCount = baseManager.getNumberofEntrysForAllProject();
            addHashMaps(keyIntegerHashMap, entryCount);

        }

        return keyIntegerHashMap;
    }

    @Override
    public HashMap<Key, Integer> getNumberOfUnsyncedEntries()
            throws NotLoggedInException, StatementNotExecutedException {
        HashMap<Key, Integer> keyIntegerHashMap = new HashMap<>();
        for (IBaseManager baseManager : getLocalManager().getSyncTables()) {
            HashMap<Key, Integer> entryCount = baseManager.getNumberofUnsynchronizedEntrysForAllProject();
            addHashMaps(keyIntegerHashMap, entryCount);

        }

        return keyIntegerHashMap;
    }

    private void addHashMaps(HashMap<Key, Integer> keyIntegerHashMap,
                             HashMap<Key, Integer> entryCount) {
        for (Entry<Key, Integer> keyIntegerEntry : entryCount.entrySet()) {
            Integer integer1 = keyIntegerHashMap.get(keyIntegerEntry.getKey());
            int integer = 0;
            if (integer1 != null) {
                integer = integer1;
            }

            keyIntegerHashMap.put(keyIntegerEntry.getKey(), keyIntegerEntry.getValue() + integer);
        }
    }

    @Override
    public HashMap<Key, Integer> getNumberOfConflictedEntries()
            throws NotLoggedInException, StatementNotExecutedException {
        HashMap<Key, Integer> keyIntegerHashMap = new HashMap<>();
        for (IBaseManager baseManager : getLocalManager().getSyncTables()) {
            HashMap<Key, Integer> entryCount = baseManager.getNumberofConflictedEntrysForAllProject();
            addHashMaps(keyIntegerHashMap, entryCount);

        }

        return keyIntegerHashMap;
    }

    @Override
    public boolean isValidCodeID(ColumnType columnType, String idToCheck)
            throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getCodeTableManager().isIDValid(columnType, idToCheck);
    }


    @Override
    public int getNumberOfUncommittedEntries(ProjectDataSet thisProject)
            throws NotLoggedInException, EntriesException, StatementNotExecutedException, NoRightException {
        checkReadRights(thisProject);
        int entryCount = 0;
        for (IBaseManager baseManager : getLocalManager().getSyncTables()) {
            int count = baseManager.getNumberOfUncommittedEntries(thisProject.getProjectKey());
            ;
            if (XBookConfiguration.DISPLAY_SOUTS && count > 0) {
                System.out.println(baseManager.getTableName() + ": " + count);
            }
            entryCount += count;

        }

        return entryCount;
    }

    @Override
    public Preferences getPreferences() {
        return AbstractConfiguration.bookConfig;
    }

    /**
     * Checks if the current input for a specific column type is already existing in the local database.
     *
     * @param columnType The column type for that the value should be checked.
     * @param value      The value to check.
     * @return <code>true</code> if the value is already saved in the database,
     * <code>false</code> else.
     */
    public boolean isValueDuplicated(ColumnType columnType, String value,
                                     BaseEntryManager managerOfLoadedEntry)
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        if (value == null || value.isEmpty()) {
            return false;
        }
        checkReadRights();
        ArrayList<Key> entries = managerOfLoadedEntry.getExistingEntriesForValue(columnType, value,
                loadedProject.getProjectKey());
        if (entries.isEmpty()) {
            return false;
        }
        if (entries.size() > 1) {
            return true;
        }
        // one entry
        if (loadedEntries.get(managerOfLoadedEntry) != null) {
            // check if it's the same entry. If so return false to indicate this is valid otherwise true
            return !loadedEntries.get(managerOfLoadedEntry).getEntryKey().equals(entries.get(0));
        }
        // no loaded entry therfore error
        return true;
    }

    //**** API Stuff *****************//////
    @Override
    public xResultSet getStuff(ArrayList<ColumnType> columns, ArrayList<Key> projects,
                               ArrayList<DataColumn> condition) {
        return null;
    }

    /**
     * checks if the plugin is valid for the current book
     *
     * @param book The book setting of the plugin
     */
    protected abstract boolean isPluginValidForBook(PluginInformation.Book book);

    @Override
//    public ExportResult getDataForColumns(List<Key> projectKeys, List<ColumnType> columns, String table)
    public ExportResult getDataForColumns(String managerName)
            throws NotLoggedInException, NoRightException, StatementNotExecutedException, NotLoadedException,
            EntriesException {
//        for (Key key : projectKeys) {
//            try {
//                checkReadRights(key);
//            } catch (EntriesException e) {
//                throw new NoRightException(NoRightException.Rights.NOREAD);
//            }
//        }
//        return getLocalManager().getManagerForName(table).getDataForColumns(projectKeys, columns);

        // New:
        ArrayList<ColumnType> list = new ArrayList<>();
        int offset = 0;
        //final int ENTRIES_PER_PAGE = 50000;
        final int ENTRIES_PER_PAGE = 0;//we need all. Possible loop if this brings improvements
        DataSetOld pds = loadedProject;
        AbstractBaseEntryManager abeManager = (AbstractBaseEntryManager) getLocalManager()
                .getManagerForName(
                        managerName);
        return abeManager
                .getEntries(pds, list, ColumnType.ExportType.GENERAL, false, offset, ENTRIES_PER_PAGE, null,
                        true);

    }

    @Override
    public ExportResult getSpecificDataForColumns(String managerName)
            throws NotLoggedInException, NoRightException, StatementNotExecutedException, NotLoadedException,
            EntriesException {
        ArrayList<ColumnType> list = new ArrayList<>();
        int offset = 0;
        final int ENTRIES_PER_PAGE = 600;
        DataSetOld pds = loadedProject;
        AbstractBaseEntryManager abeManager = (AbstractBaseEntryManager) getLocalManager()
                .getManagerForName(
                        managerName);
        return abeManager
                .getEntries(pds, list, ColumnType.ExportType.SPECIFIC, false, offset, ENTRIES_PER_PAGE,
                        null,
                        true);
    }

    @Override
    public List<ColumnType> getColumnsForTable(String managerName) throws NotLoggedInException {
        System.out.println("TODO remove status columns");
        return getLocalManager().getManagerForName(managerName).getDataColumns();
    }

    public List<ColumnType> getKeysForTable(String managerName) throws NotLoggedInException {
        System.out.println("TODO!!");
        return new ArrayList<>();
        //return getLocalManager().getManagerForName(managerName).getDataColumns();
    }

    @Override
    public List<String> getSynchronisationTableNames() throws NotLoggedInException {
        ArrayList<String> tables = new ArrayList<>();
        tables.addAll(getLocalManager().getImportantManagerNames());
        return tables;
    }

    // Alex edit:
    public Set<String> getAllTables() throws NotLoggedInException {
        return getLocalManager().getManagerNamesKeySet();
//        return getLocalManager().getManagerNamesEntrySet();
    }

    public boolean hasReadRights()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException {
        AbstractQueryManager m = getLocalManager();

        if (loadedProject == null) {
            throw new NotLoadedException();
        }
        return true;

    }

    @Override
    public boolean hasReadRights(ProjectDataSet project)
            throws NotLoggedInException, StatementNotExecutedException {
        return true;
    }

    @Override
    public final boolean hasWriteRights()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException {
        return hasWriteRights(false);
    }

    @Override
    public boolean canEditEntry()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException {
        return hasWriteRights();
    }

    @Override
    public boolean hasWriteRights(
            boolean ignoreNotLoadedException)
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException {
        AbstractQueryManager m = getLocalManager();

        if (!ignoreNotLoadedException && loadedProject == null) {
            throw new NotLoadedException();
        }
        return true;
    }

    @Override
    public boolean hasWriteRights(ProjectDataSet project)
            throws NotLoggedInException, StatementNotExecutedException {
        return true;
    }

    @Override
    public boolean hasEditRights()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException {
        AbstractQueryManager m = getLocalManager();

        if (loadedProject == null) {
            throw new NotLoadedException();
        }
        return true;
    }

    @Override
    public boolean hasEditRights(ProjectDataSet project)
            throws NotLoggedInException, StatementNotExecutedException {
        AbstractQueryManager m = getLocalManager();
        return true;
    }

    /**
     * Checks if a project is loaded, if there is a connection and if the user has the right to read the current
     * project
     *
     * @throws NotLoggedInException          If the user is not logged in. If there is no connection to a database
     * @throws NotLoadedException            If no Project is loaded If no project was loaded
     * @throws StatementNotExecutedException If a sql error occurred If a SQL error appeared
     * @throws NoRightException              If the user has no right to read the current AbstractProjectOverviewScreen
     */
    protected void checkReadRights()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        AbstractQueryManager m = getLocalManager();
        if (m == null) {
            throw new NotLoggedInException();
        }
        if (loadedProject == null) {
            throw new NotLoadedException();
        }
    }

    /**
     * Checks if a project is loaded, if there is a connection and if the user has the right to read the current
     * project
     *
     * @param projectkey The project to check the status for
     * @throws NotLoggedInException          If the user is not logged in. If there is no connection to a database
     * @throws StatementNotExecutedException If a sql error occurred If a SQL error appeared
     * @throws NoRightException              If the user has no right to read the current AbstractProjectOverviewScreen
     */
    protected void checkReadRights(Key projectkey)
            throws NotLoggedInException, StatementNotExecutedException, NoRightException {
        if (getLocalManager() == null) {
            throw new NotLoggedInException();
        }
//        if (project == null) {
//            throw new NotLoadedException();
//        }

    }

    public boolean hasEditGroupRights(
            int groupId) throws NotLoggedInException, NotLoadedException, StatementNotExecutedException {
        return true;
    }

    /**
     * Checks if a project is loaded, if there is a connection and if the user has the right to read the current
     * project
     *
     * @param project The project to check the status for
     * @throws NotLoggedInException          If the user is not logged in. If there is no connection to a database
     * @throws StatementNotExecutedException If a sql error occurred If a SQL error appeared
     * @throws NoRightException              If the user has no right to read the current AbstractProjectOverviewScreen
     */
    protected void checkReadRights(ProjectDataSet project)
            throws NotLoggedInException, StatementNotExecutedException, NoRightException {
        if (getLocalManager() == null) {
            throw new NotLoggedInException();
        }
//        if (project == null) {
//            throw new NotLoadedException();
//        }

    }

    /**
     * Checks if a project is loaded, if there is a connection and if the user has the right to read the current
     * project
     *
     * @throws NotLoggedInException          If the user is not logged in. If there is no connection to a database
     * @throws NotLoadedException            If no Project is loaded If no project was loaded
     * @throws StatementNotExecutedException If a sql error occurred If a SQL error appeared
     * @throws NoRightException              If the user has no right to read the current AbstractProjectOverviewScreen
     */
    protected void checkEditRights()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        AbstractQueryManager m = getLocalManager();
        if (m == null) {
            throw new NotLoggedInException();
        }
        if (loadedProject == null) {
            throw new NotLoadedException();
        }

    }

    /**
     * Checks if a project is loaded, if there is a connection and if the user has the right to read the current
     * project
     *
     * @param project The project to check the status for
     * @throws NotLoggedInException          If the user is not logged in. If there is no connection to a database
     * @throws StatementNotExecutedException If a sql error occurred If a SQL error appeared
     * @throws NoRightException              If the user has no right to read the current AbstractProjectOverviewScreen
     */
    protected void checkEditRights(ProjectDataSet project)
            throws NotLoggedInException, StatementNotExecutedException, NoRightException {
        if (getLocalManager() == null) {
            throw new NotLoggedInException();
        }
//        if (project == null) {
//            throw new NotLoadedException();
//        }

    }

    /**
     * Checks if a project is loaded, if there is a connection and if the user has the right to read the current
     * project
     *
     * @throws NotLoggedInException          If the user is not logged in. If there is no connection to a database
     * @throws NotLoadedException            If no Project is loaded If no project was loaded
     * @throws StatementNotExecutedException If a sql error occurred If a SQL error appeared
     * @throws NoRightException              If the user has no right to read the current AbstractProjectOverviewScreen
     */
    protected void checkWriteRights()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        AbstractQueryManager m = getLocalManager();
        if (m == null) {
            throw new NotLoggedInException();
        }
        if (loadedProject == null) {
            throw new NotLoadedException();
        }

    }

    @Override
    public ArrayList<RightsInformation> getNewUserRights()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        checkEditRights();
        return getLocalManager().getProjectRightManager().getUserRights(loadedProject.getProjectKey());
    }

    @Override
    public ArrayList<RightsInformation> getNewGroupRights()
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        checkEditRights();
        return getLocalManager().getGroupManager().getGroupRights(loadedProject.getProjectKey());
    }

    @Override
    public HashMap<String, HierarchicData> getHierarchicData(ColumnType multiTable)
            throws NotLoggedInException {
        return getLocalManager().getCodeTableManager().getHierarchicData(multiTable);
    }

    @Override
    public void loadMultiEntries(BaseEntryManager manager,
                                 ArrayList<Key> keys)
            throws NotLoggedInException, NoRightException, StatementNotExecutedException {
        checkReadRights(loadedProject);

        // necessary for correct sidebar information for input fields
        EntryModeHelper.setToMultiEntryMode();

        ArrayList<EntryDataSet> list = new ArrayList<>();

        Footer.setProgressBarValue(0);
        Footer.showProgressBar();
        DataSetOld ref = null;
        int size = keys.size();
        try {
            int i = 0;
            for (Key entryKey : keys) {
                EntryDataSet data = new EntryDataSet(entryKey, loadedProject.getProjectKey(), getDbName(),
                        manager.getTableName());
                String[] tables = null;
                if (ref != null) {
                    tables = ref.getTableNames().toArray(new String[0]);
                }
                manager.loadBase(data, tables);
                list.add(data);
                if (ref == null) {
                    ref = data;
                }
                ref = DataSetOld.intersect(ref, data);
                if (!ref.hasEntries()) {
                    break;
                }
                Footer.setProgressBarValue(i++ * 100.0 / size);

            }
        } finally {
            Footer.hideProgressBar();
        }

        AbstractEntry multiEntry = getEntryForManager(GeneralInputMaskMode.MULTI_EDIT, manager);
        try {
            multiEntry.loadMultiEditEntries(ref);
            multiEntry.setNumberOfLoadedEntries(size);
            multiEntry.setLoadedKeys(keys);
        } catch (WrongModeException ex) {
            Logger.getLogger(AbstractController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Content.setContent(multiEntry);

    }

    @Override
    public int getNumberOfHiddenProjects()
            throws NotLoggedInException, StatementNotExecutedException, EntriesException {
        return getLocalManager().getProjectManager().getNumberOfHiddenProjects();
    }

    @Override
    public int getNumberOfProjects() throws NotLoggedInException, StatementNotExecutedException {
        return getLocalManager().getProjectManager().getNumberOfProjects();
    }

    @Override
    public void saveEntries(EntryDataSet entry,
                            ArrayList<Key> keys)
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        checkWriteRights();

        entry.setProjectKey(loadedProject.getProjectKey());
        for (IBaseManager table : getLocalManager().getSyncTables()) {

            if (!(table instanceof BaseEntryManager)) {
                continue;
            }
            AbstractBaseEntryManager entrydata = (AbstractBaseEntryManager) table;

            entrydata.save(entry, keys);
            mainFrame.getListingScreen().addUpdatedEntry(entrydata, keys.size());
        }
    }

    @Override
    public abstract DatabaseType getDatabaseType();

    public String copyFileToLocalDirectory(Path lastLoadedFile,
                                           String fileExtension) throws IOException {
        final UUID uuid = UUID.randomUUID();
        final boolean mkdirs = new File(XBookConfiguration.TMP_UPLOAD_DIRECTORY).mkdirs();

        final File file = new File(
                XBookConfiguration.TMP_UPLOAD_DIRECTORY + "/" + uuid.toString() + "." + fileExtension);
        final Path copy = Files.copy(lastLoadedFile, file.toPath());
        return uuid.toString() + "." + fileExtension;
    }


    public File openNotUploadedFile(
            String filename)
            throws NotLoggedInException, NoRightException, StatementNotExecutedException, NotConnectedException,
            IOException, NotLoadedException {

        final File file = new File(XBookConfiguration.TMP_UPLOAD_DIRECTORY + "/" + filename);
        LOGGER.debug(file.toPath());
        Desktop desktop = Desktop.getDesktop();
        if (file.exists()) {
            desktop.open(file);
            return file;
        } else {
            throw new IOException("File Not Found");
        }

    }

    /**
     * Returns the feature configuration object.
     *
     * @return The feature configuration object.
     */
    public FeatureConfiguration getFeatureConfiguration() {
        if (featureConfiguration == null) {
            featureConfiguration = new FeatureConfiguration();
        }
        return featureConfiguration;
    }
}
