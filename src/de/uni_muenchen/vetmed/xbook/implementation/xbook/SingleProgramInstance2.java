package de.uni_muenchen.vetmed.xbook.implementation.xbook;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.net.ServerSocketFactory;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

/**
 * Class used to make sure there's only one instance of the program using it at
 * a time.
 *
 * @author fnuecke
 */
public final class SingleProgramInstance2 {

    /**
     * Logger to log errors (e.g. if the program is already running).
     */
    private static final Log log = LogFactory.getLog(SingleProgramInstance2.class);
    private static File f;
    private static FileChannel channel;
    private static FileLock lock;

    /**
     * No need for instances of this class.
     */
    private SingleProgramInstance2() {
    }
    public static void check(){
        try {
            f = new File("xbook.lock");
            // Check if the lock exist
            if (f.exists()) {
                // if exist try to delete it
                f.delete();
            }
            // Try to get the lock
            channel = new RandomAccessFile(f, "rw").getChannel();
            lock = channel.tryLock();
            if(lock == null)
            {
                // File is lock by other application
                channel.close();
                JOptionPane.showMessageDialog(null, Loc.get("XBOOK_ALREADY_RUNNING_MESSAGE"), Loc.get("XBOOK_ALREADY_RUNNING"), JOptionPane.ERROR_MESSAGE);
                System.exit(2);
            }
            // Add shutdown hook to release lock when application shutdown
            ShutdownHook shutdownHook = new ShutdownHook();
            Runtime.getRuntime().addShutdownHook(shutdownHook);



        }
        catch(IOException e)
        {
            JOptionPane.showMessageDialog(null, Loc.get("XBOOK_ALREADY_RUNNING_MESSAGE"), Loc.get("XBOOK_ALREADY_RUNNING"), JOptionPane.ERROR_MESSAGE);
            System.exit(2);
        }
    }

    public static void unlockFile() {
        // release and delete file lock
        try {
            if(lock != null) {
                lock.release();
                channel.close();
                f.delete();
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    static class ShutdownHook extends Thread {

        public void run() {
            unlockFile();
        }
    }
}
