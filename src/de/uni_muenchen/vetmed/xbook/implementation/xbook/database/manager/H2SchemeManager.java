package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.datatype.HashListMap;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.Column;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.Index;

import java.sql.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 30.05.2017.
 */
public class H2SchemeManager extends SchemeManager {

    public static final int ENUM_SIZE = 2;
    public H2SchemeManager(Connection connection, String databaseName) {
        super(connection, databaseName);
    }


    public ArrayList<String> getTables() throws StatementNotExecutedException {
        ArrayList<String> tableNames = new ArrayList<>();
        try {
            ResultSet resultSet = connection.createStatement().executeQuery("SHOW TABLES FROM " + databaseName);
            while (resultSet.next()) {
                tableNames.add(resultSet.getString(2) + "." + resultSet.getString(1));
            }


//            DatabaseMetaData database = connection.getMetaData();
//
//            String[] tblTypes = {"TABLE"};
//            ResultSet rs = database.getTables( null,
//                    null,"%", tblTypes);
//            while (rs.next()) {
//                tableNames.add(databaseName + "." + rs.getString("TABLE_NAME"));
//            }
        } catch (SQLException sql) {
            throw new StatementNotExecutedException("getTables()", sql);
        }

        return tableNames;
    }

    @Override
    protected String columnOptions(String columnName, String type, String size, String nullable, String defaultValues, String autoincrement) {
        if (!type.equals("ENUM"))
            return super.columnOptions(columnName, type, size, nullable, defaultValues, autoincrement);
        String query = "`" + columnName + "` VARCHAR ("+ENUM_SIZE+") NOT NULL DEFAULT '" + defaultValues + "'";
        return query;


    }

    @Override
    public void changeColumn(String tableName, String columnName, String type, String size, String nullable, String defaultValues, String autoincrement) throws StatementNotExecutedException {
        String alterQuery = "ALTER TABLE " + tableName + " ALTER COLUMN "
                + columnOptions(columnName, type, size, nullable,
                defaultValues, autoincrement) + ";";

        try {
            executeQuery(alterQuery);
        } catch (SQLException e) {
            if (e.getErrorCode() == 1265) {
                String defaultString = defaultValues;
                if (defaultString == null) {
                    defaultString = "";
                    if (type.contains("INT") || type.contains("TINYINT")
                            || type.contains("SMALLINT")
                            || type.contains("MEDIUMINT")
                            || type.contains("BIGINT") || type.contains("FLOAT")
                            || type.contains("DECIMAL") || type.contains("DOUBLE")) {
                        defaultString = "-1";
                    }
                }
                String query2 = "UPDATE " + tableName + " SET " + columnName + "='" + defaultString
                        + "' WHERE `" + columnName + "` is NULL";
                try {
                    executeQuery(query2);
                    executeQuery(alterQuery);
                } catch (SQLException ex) {
                    Logger.getLogger(H2SchemeManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                throw new StatementNotExecutedException(alterQuery, e);
            }
        }
    }

    @Override
    protected String getIndexString(String tableName, ArrayList<Index> indices, String query) {
        for (Index index : indices) {
            query += ", INDEX `" + tableName + "." + index.getIndexName() + "` (";
            final ArrayList<String> columns = index.getColumns();
            for (int i = 0; i < columns.size(); i++) {
                query += "`" + columns.get(i) + "`";
                if (!(i == columns.size() - 1)) {
                    query += ", ";
                } else {
                    query += ") ";
                }
            }
        }
        return query;
    }

    public ArrayList<Column> getTableDescription(String tableName)
            throws StatementNotExecutedException {

        ArrayList<Column> result = new ArrayList<>();
        try {
            DatabaseMetaData database = connection.getMetaData();
            ResultSet rs = database.getColumns("XBOOK", getDatabaseName(tableName).toUpperCase(), ColumnHelper.removeDatabaseName(tableName).toUpperCase(), "%");
            String[] elements;
            //doll
            // rs.getString("IS_AUTOINCREMENT"); just works since younger
            // database version
            // or with extra patch
            String query = "SHOW COLUMNS FROM " + tableName + ";";
            ResultSet rsHelp = executeSelect(query);

            String extra;
            while (rs.next()) {
                rsHelp.next();

                elements = new String[6];
                String name = rs.getString("COLUMN_NAME");
                if (name == null) {
                    System.out.println("woot??");
                }
                String type = rs.getString("TYPE_NAME").toUpperCase();
                String size;
                String defaultValue;
                String nullable;
                String autoincrement;
                switch (type) {

                    case "ENUM":
                        extra = rsHelp.getString("Type");
                        size = extra.substring(5, extra.length() - 1);
                        break;
                    case "DATE"://somehow we get wrong values..
                        size = "10";
                        break;
                    default:
                        size = rs.getString("COLUMN_SIZE");
                }
                if (size == null) {
                    System.out.println("?");
                }
                defaultValue = rs.getString("COLUMN_DEF");
                if (defaultValue == null) {
                    defaultValue = "";
                } else if (defaultValue.contains("NEXT VALUE FOR")) {
                    defaultValue = "";
                } else {
                    defaultValue = defaultValue.replaceAll("'", "");
                }
                nullable = rs.getString("IS_NULLABLE");

                autoincrement = rs.getString("IS_AUTOINCREMENT");

                // elements[5]=rs.getString("IS_AUTOINCREMENT");
                result.add(new Column(name, type, size, defaultValue, nullable, autoincrement));
            }
        } catch (SQLException sql) {
            throw new StatementNotExecutedException("getTableDescription()", sql);
        }

        return result;
    }


    @Override
    public ArrayList<Index> getIndices(String tableName) {

        ArrayList<Index> list = new ArrayList<>();
        HashListMap<String, String> indices = new HashListMap<>();
        try {
            ResultSet rs = connection.getMetaData().getIndexInfo("XBOOK", ColumnHelper.getTableName(tableName), ColumnHelper.removeDatabaseName(tableName), false, false);
            while (rs.next()) {
                String indexName = rs.getString("INDEX_NAME");
                if (!indexName.contains("primary_key")) {
                    indices.add(indexName, rs.getString("COLUMN_NAME"));
                }
            }

            for (Map.Entry<String, ArrayList<String>> stringArrayListEntry : indices.entrySet()) {
                list.add(new Index(stringArrayListEntry.getKey(), stringArrayListEntry.getValue()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<String> getPrimaryKeys(String tableName)
            throws StatementNotExecutedException {
        ArrayList<String> result = new ArrayList<>();
        try {
            DatabaseMetaData database = connection.getMetaData();
            ResultSet rs = database.getPrimaryKeys("XBOOK", getDatabaseName(tableName), ColumnHelper.removeDatabaseName(tableName));
            while (rs.next()) {
                result.add(rs.getString("COLUMN_NAME"));
            }
        } catch (SQLException sql) {
            throw new StatementNotExecutedException("getPrimaryKeys()", sql);
        }
        return result;
    }
}
