package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableArrayList;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import org.apache.commons.logging.LogFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * manages database database
 *
 * @author j.lamprecht
 * @author fnuecke
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class TableManager implements IStandardColumnTypes {

    protected final Connection connection;
    protected final String databaseName;
    protected final String AND = " AND ";
    protected final String WHERE  = " WHERE ";
    protected final String FROM = " FROM ";

    /**
     * @param connection that has been established
     */
    public TableManager(Connection connection, String databaseName) {
        this.connection = connection;
        this.databaseName = databaseName;
    }

    public String getDatabaseName(String columnName) {
        int lastIndex = columnName.lastIndexOf(".");
        if (lastIndex == -1) {
            return databaseName;
        }
        return columnName.substring(0, lastIndex);
    }

    public String getDatabaseName() {
        return databaseName;
    }

    /**
     * Executes a statement on the database. Don't use this for insert or update
     * statements!!
     *
     * @param query : select, insert, update or delete statement
     * @throws StatementNotExecutedException
     */
    protected void executeQuery(String query) throws SQLException {
        try {


            Statement statement = connection.createStatement();
            statement.execute(query);
            statement.close();
        } catch (SQLException ex) {

            Statement statement = connection.createStatement();
            statement.execute(query);
            statement.close();
        }
    }

    protected void executeQueryPrepared(String query, String... values) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(query);
        try {

            for (int i = 0; i < values.length; i++) {
                statement.setString(i + 1, values[i]);
            }
            statement.execute();
        } finally {
            if (statement != null) {
                statement.close();
            }
        }


    }

    protected void executeQueryPrepared(String query, List<String> values) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(query);
        try {

            for (int i = 0; i < values.size(); i++) {
                statement.setString(i + 1, values.get(i));
            }
            statement.execute();
        } finally {
            if (statement != null) {
                statement.close();
            }
        }


    }

    /**
     * select statement on database shall be done
     *
     * @param query : select Statement
     * @return
     * @throws SQLException
     */
    public xResultSet executeSelect(String query, boolean bigfile) throws SQLException {
        Statement s = connection.createStatement();
        if (bigfile) {
            s.setFetchSize(10000);
        }
        try {

            return new xResultSet(s.executeQuery(query));
        } catch (SQLException ex) {
            try {
                System.out.println("execute failed but try again!");
                return new xResultSet(s.executeQuery(query));
            } catch (SQLException ex2) {
                throw ex2;
            }
        } finally {
//            s.close();
        }
    }

    public xResultSet executeSelectPrepared(String query, final String... values) throws SQLException {
        return executeSelectPrepared(query, false, values);
    }

    /**
     * select statement on database shall be done
     *
     * @param query : select Statement
     * @return
     * @throws SQLException
     */
    public xResultSet executeSelectPrepared(String query, boolean bigfile, final String... values) throws SQLException {
        PreparedStatement s = connection.prepareStatement(query);
        for (int i = 0; i < values.length; i++) {

            s.setString(i + 1, values[i]);
        }
        if (bigfile) {
            s.setFetchSize(10000);
        }
        try {

            return new xResultSet(s.executeQuery());
        } catch (SQLException ex) {
            try {
                System.out.println("execute failed but try again!");

                return new xResultSet(s.executeQuery());
            } catch (SQLException ex2) {
                throw ex2;
            }
        }
    }

    /**
     * select statement on database shall be done
     *
     * @param query : select Statement
     * @return
     * @throws SQLException
     */
    public xResultSet executeSelect(String query) throws SQLException {
        return executeSelect(query, false);
    }

    public int updateDataWithKey(ArrayList<DataColumn> data, String tableName, ArrayList<DataColumn> key) throws StatementNotExecutedException {

        String query = "UPDATE " + tableName + " SET ";
        PreparedStatement st = null;
        try {
            String where = "";
            ArrayList<String> values = new ArrayList<>();
            for (DataColumn entry : data) {

                query += "`" + entry.getColumnName() + "`" + "=? , ";
                values.add(entry.getValue());
            }
            for (DataColumn keyEntry : key) {
                where += "`" + keyEntry.getColumnName() + "`" + " = ? AND ";
                values.add(keyEntry.getValue());
            }

            where = StringHelper.cutString(where, " AND ");
            //where = where.substring(0, where.length() - 4);
            query = StringHelper.cutString(query, ", ");
//            query = query.substring(0, query.length() - 2);
            query += " WHERE " + where;
            try {
                st = connection.prepareStatement(query);
            } catch (SQLException e) {
                //try again
                st = connection.prepareStatement(query);
            }
            for (int i = 0; i < values.size(); i++) {
                String value = values.get(i);
                if (value == null) {
                    st.setNull(i + 1, Types.VARCHAR);
                } else {
                    st.setString(i + 1, value);
                }
            }
            try {
                return st.executeUpdate();
            } catch (SQLException e) {
                //try again
                return st.executeUpdate();
            }


        } catch (SQLException ex) {
            if (st != null) {
                throw new StatementNotExecutedException(st.toString(), ex);
            } else {
                throw new StatementNotExecutedException(query, ex);
            }
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    throw new StatementNotExecutedException(st.toString(), ex);
                }
            }
        }
    }

    protected int updateData(DataRow data, String tableName) throws StatementNotExecutedException {
        return updateData(data, tableName, new ArrayList<String>());
    }

    /**
     * Updates the Record with the given data and the given key for the given
     * table
     *
     * @param data      The data to be updated
     * @param tableName The name of the table to be updated
     * @param key       The key of the entry
     * @return The number of rows affected
     * @throws StatementNotExecutedException
     */
    protected int updateData(DataRow data, String tableName,
                             ArrayList<String> key)
            throws StatementNotExecutedException {

//
//        String query = "INSERT" + " INTO " + tableName + " (";
//        PreparedStatement s = null;
//        try {
//            String columns = "";
//            String values = "";
//            String updateQuery = " ON DUPLICATE KEY UPDATE ";
//            for (DataColumn d : data.iterator()) {
//
//                columns += "`" + d.getColumnName() + "`" + ",";
//                values += "?, ";
//
//                if (!keyList.contains(ColumnHelper.removeDatabaseName(tableName) + "." + d.getColumnName()) && !keyList.contains(d.getColumnName())) {
//                    if (!d.getColumnName().equals(ColumnHelper.removeDatabaseName(AbstractInputUnitManager.USER_ID_INPUTUNIT.getColumnName()))) {
//                        updateQuery += "`" + d.getColumnName() + "`" + " = VALUES(`" + d.getColumnName() + "`),";
//                    }
//                }
//            }
//            columns += "`" + ID + "`" + "," + "`" + DATABASE_ID + "`";
//            values += "?, ?";
//            query += columns + ") VALUES ";
//            for (Key key1 : keyList) {
//                query += "(" + values + "),";
//            }
//            query = StringHelper.cutString(query, ",");
//            query += StringHelper.cutString(updateQuery, ",");
//            System.out.println(query);
//            try {
//                s = connection.prepareStatement(query);
//            } catch (SQLException e) {
//                //try again
//                s = connection.prepareStatement(query);
//            }
//            try (PreparedStatement preparedStatement = s) {
//                int i = 1;
//                for (Key key1 : keyList) {
//
//
//                    for (String value : data.values()) {
//
//                        if (value == null) {
//                            s.setNull(i++, Types.VARCHAR);
//                        } else {
//                            s.setString(i++, value);
//                        }
//
//                    }
//                    s.setInt(i++, key1.getID());
//                    s.setInt(i++, key1.getDBID());
//                }
//                System.out.println(preparedStatement);
//                preparedStatement.execute();
//            }
//        } catch (SQLException e) {
//            throw new StatementNotExecutedException(query, e);
//        }
//
//
//
//
//    }

        String query = "UPDATE " + tableName + " SET ";
        PreparedStatement st = null;
        try {
            String where = "";
            ArrayList<String> values = new ArrayList<>();
            for (DataColumn entry : data.iterator()) {
                boolean prim = false;
                if (key.contains(ColumnHelper.removeDatabaseName(tableName) + "." + entry.getColumnName()) || key.contains(entry.getColumnName()) || key.contains(entry.getColumnName().toLowerCase())) {
                    where += "`" + entry.getColumnName() + "`" + " = '" + entry.getValue() + "' AND ";
                    prim = true;
                } else {
                    for (int j = 0; j < key.size(); j++) {

                        if ((entry.getColumnName()).equals(tableName + "." + key.get(j))) {
                            where += "`" + entry.getColumnName() + "`" + " = '" + entry.getValue() + "' AND ";
                            prim = true;
                            break;

                        }
                    }
                }

                if (prim) {
                    continue;
                }
                query += "`" + entry.getColumnName() + "`" + "=? , ";
                values.add(entry.getValue());
            }
            if (where.endsWith("AND ")) {
                where = where.substring(0, where.length() - 4);
            }
            query = query.substring(0, query.length() - 2);
            if (!where.isEmpty()) {
                query += " WHERE " + where;
            }
            try {
                st = connection.prepareStatement(query);
            } catch (SQLException e) {
                //try again
                System.out.println("execute failed but try again!");

                st = connection.prepareStatement(query);
            }
            for (int i = 0; i < values.size(); i++) {
                String value = values.get(i);
                if (value == null) {
                    st.setNull(i + 1, Types.VARCHAR);
                } else {
                    st.setString(i + 1, value);
                }
            }
            return st.executeUpdate();

        } catch (SQLException ex) {
            if (st != null) {
                throw new StatementNotExecutedException(st.toString(), ex);
            } else {
                throw new StatementNotExecutedException(query, ex);
            }
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    throw new StatementNotExecutedException(st.toString(), ex);
                }
            }
        }

//      
    }

    /**
     * Inserts or updates entries. This only works for entries in entry tables (not project)
     *
     * @param data
     * @param tableName
     * @param key
     * @param keyList
     * @throws StatementNotExecutedException
     */
    protected void insertOrUpdate(DataRow data, String tableName,
                                  ArrayList<String> key, ArrayList<Key> keyList)
            throws StatementNotExecutedException {


        String query = "INSERT" + " INTO " + tableName + " (";
        PreparedStatement s = null;
        try {
            String columns = "";
            String values = "";
            String updateQuery = " ON DUPLICATE KEY UPDATE ";
            for (DataColumn d : data.iterator()) {

                columns += "`" + d.getColumnName() + "`" + ",";
                values += "?, ";

                if (!key.contains(ColumnHelper.removeDatabaseName(tableName) + "." + d.getColumnName()) && !key.contains(d.getColumnName())) {
                    if (!d.getColumnName().equals(ColumnHelper.removeDatabaseName(AbstractInputUnitManager.USER_ID_INPUTUNIT.getColumnName()))) {
                        updateQuery += "`" + d.getColumnName() + "`" + " = VALUES(`" + d.getColumnName() + "`),";
                    }
                }
            }
            columns += "`" + ID + "`" + "," + "`" + DATABASE_ID + "`";
            values += "?, ?";
            query += columns + ") VALUES ";
            for (Key key1 : keyList) {
                query += "(" + values + "),";
            }
            query = StringHelper.cutString(query, ",");
            query += StringHelper.cutString(updateQuery, ",");
            LogFactory.getLog(this.getClass()).debug(query);
            try {
                s = connection.prepareStatement(query);
            } catch (SQLException e) {
                //try again
                s = connection.prepareStatement(query);
            }
            try (PreparedStatement preparedStatement = s) {
                int i = 1;
                for (Key key1 : keyList) {


                    for (String value : data.values()) {

                        if (value == null) {
                            s.setNull(i++, Types.VARCHAR);
                        } else {
                            s.setString(i++, value);
                        }

                    }
                    s.setInt(i++, key1.getID());
                    s.setInt(i++, key1.getDBID());
                }
                LogFactory.getLog(this.getClass()).debug(preparedStatement);
                preparedStatement.execute();
            }
        } catch (SQLException e) {
            //Make sure values are nullable
            throw new StatementNotExecutedException(query, e);
        }

    }


    /**
     * inserts a new record into the given table
     *
     * @param data      - values of the record
     * @param tableName
     * @throws StatementNotExecutedException
     */

    protected void insertData(DataRow data, String tableName)
            throws StatementNotExecutedException {
        insertData(data, tableName, false);
    }


    /**
     * inserts a new record into the given table
     *
     * @param data      - values of the record
     * @param tableName
     * @param ignore
     * @throws StatementNotExecutedException
     */
    protected void insertData(DataRow data, String tableName, boolean ignore)
            throws StatementNotExecutedException {
        String query = "INSERT" + (ignore ? " IGNORE" : "") + " INTO " + tableName + " (";
        if (XBookConfiguration.getDatabaseMode() == XBookConfiguration.DatabaseMode.H2) {
            if (ignore) {
                query = "INSERT" + " INTO " + tableName + " (";
            }
        }
        PreparedStatement s = null;
        try {
            String columns = "";
            String values = "";
            for (DataColumn d : data.iterator()) {
                columns += "`" + d.getColumnName() + "`" + ",";
                values += "? ,";
            }
            columns = columns.substring(0, columns.length() - 1);
            values = values.substring(0, values.length() - 1);
            query += columns + ") VALUES (" + values + ")";
            try {
                s = connection.prepareStatement(query);
            } catch (SQLException e) {
                //try again
                s = connection.prepareStatement(query);
            }
            int i = 0;
            for (String value : data.values()) {

                if (value == null) {
                    s.setNull(i + 1, Types.VARCHAR);
                } else {
                    s.setString(i + 1, value);
                }
                i++;
            }
            s.executeUpdate();

        } catch (SQLException e) {
            if (ignore) {
                return;
            }
            if (s != null) {
                throw new StatementNotExecutedException(s.toString(), e);
            } else {
                throw new StatementNotExecutedException(query, e);
            }
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException ex) {
                    throw new StatementNotExecutedException(s.toString(), ex);
                }
            }
        }
    }

    protected String getProjectKeyQuery(Collection<Key> keys, ColumnType id, ColumnType dbID) {
        String projectQuery = "(";
        for (Key key : keys) {
            projectQuery += "(" + id + "=" + key.getID()
                    + " AND " + dbID + "=" + key.getDBID() + ") OR ";

        }
        return StringHelper.cutString(projectQuery, " OR ") + ")";
    }

    SerialisableArrayList getEntries(ArrayList<DataColumn> columns, ArrayList<String> tableNames, String condition) throws StatementNotExecutedException {

        String query = "SELECT ";
        for (int i = 0; i < columns.size(); i++) {
            query = query + "`" + columns.get(i).getColumnName() + "`";
            if (i < columns.size() - 1) {
                query = query + ", ";
            } else {
                query = query + " FROM ";
            }
        }
        for (int i = 0; i < tableNames.size(); i++) {
            query = query + tableNames.get(i);
            if (i < tableNames.size() - 1) {
                query = query + ", ";
            } else {
                query = query + condition;
            }
        }

        SerialisableArrayList result = new SerialisableArrayList();
        try {
            xResultSet rs = executeSelect(query);

            while (rs.next()) {
                SerialisableArrayList row = new SerialisableArrayList();
                for (int i = 0; i < columns.size(); i++) {

                    row.add(new DataColumn(columns.get(i).getColumnName(), rs.getString(i + 1)));
                }
                result.add(row);
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        return result;
    }

    /**
     * get last time when table has been changed
     *
     * @param tableName
     * @return
     * @throws StatementNotExecutedException
     */
    public String getLastSynchronisation(String tableName) throws StatementNotExecutedException {
        String query = String.format("SELECT MAX(" + IStandardColumnTypes.STATUS + ") FROM %s ", tableName);
        try {
            xResultSet rs;
            try {
                rs = executeSelect(query);
            } catch (SQLException e) {
                rs = executeSelect(query);
            }
            if (rs.next()) {
                String max = rs.getString(1);
                return (max != null) ? max : "0";
            }
            return "0";
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    /**
     * Returns the String representation for the value with the given id for the
     * given Column The return value is localized if the value is multi language
     * and if no entry is found an empty string is returned
     *
     * @param column The column to get the value for
     * @param id     The id of the value.
     * @return The String Representation of the id. If no entry is found an
     * empty String is returned.
     * @throws StatementNotExecutedException
     * @throws EntriesException
     */
    public String getStringRepresentation(ColumnType column, String id) throws StatementNotExecutedException, EntriesException {
        String query;
        switch (column.getType()) {

            case ID:
                String languageQ = "";
                if (column.isLanguage()) {
                    languageQ = " AND "
                            + LanguageManager.getLanguageQuery(column.getConnectedTableName());
//                            + "(" + AbstractCodeTableManager.LANGUAGE + "=" + LanguageManager.languageID
//                            + " OR " + AbstractCodeTableManager.LANGUAGE + "= 0)";
                }
                query = "SELECT " + AbstractSynchronisationManager.VALUE + " FROM " + column.getConnectedTableName()
                        + " WHERE " + column.getConnectedTableName() + "." + AbstractSynchronisationManager.ID + "='" + id + "'"
                        + languageQ;
                break;
            case BOOLEAN:
                query = "SELECT '" + (id.equals("1") ? Loc.get("YES") : "") + "' AS '" + AbstractSynchronisationManager.VALUE + "'";
                break;
            case YES_NO_NONE:
                query = "SELECT '" + (id.equals("1") ? Loc.get("YES") : (id.equals("0") ? Loc.get("NO") : "")) + "' AS '" + AbstractSynchronisationManager.VALUE + "'";
                break;
            case HIERARCHIC:
                int currentID = Integer.valueOf(id);
                query = "";
                String values = "";
                try {
                    if (currentID < 0) {
                        return "";
                    }
                    while (currentID != 0) {
                        query = "SELECT " + AbstractCodeTableManager.PARENTID + "," + AbstractCodeTableManager.VALUE
                                + " FROM " + column.getConnectedTableName()
                                + " WHERE " + AbstractCodeTableManager.ID + "=" + currentID
                                + " AND " + IStandardColumnTypes.DELETED + "='" + IStandardColumnTypes.DELETED_NO + "'";
                        xResultSet rs;
                        try {
                            rs = executeSelect(query);
                        } catch (SQLException e) {
                            rs = executeSelect(query);
                        }
                        if (rs.next()) {
                            currentID = rs.getInt(AbstractCodeTableManager.PARENTID);
                            values = rs.getString(AbstractCodeTableManager.VALUE) + " / " + values;
                            continue;
                        }
                        return id;
                        //todo maybe anounce that enry was not found?
                    }
                    if (values.endsWith(" / ")) {
                        values = values.substring(0, values.length() - " / ".length());
                    }
                    return values;
                } catch (SQLException ex) {
                    throw new StatementNotExecutedException(query, ex);
                }
            case DATE:
            case TIME:

                if (id.isEmpty()) {
                    return column.getDefaultValue();
                }
            case VALUE:
            case VALUE_THESAURUS:
            default:
                return id;
        }
        try {
            xResultSet rs;
            try {
                rs = executeSelect(query);
            } catch (SQLException e) {
                rs = executeSelect(query);
            }
            if (rs.next()) {
                return rs.getString(AbstractSynchronisationManager.VALUE);
            }
            return "";
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

    public int getNumberOfEntries(String tableName)
            throws StatementNotExecutedException {
        String query = "SELECT COUNT(*) FROM " + tableName + " WHERE " + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "'";
        try {
            xResultSet rs;
            try {
                rs = executeSelect(query);
            } catch (SQLException e) {
                rs = executeSelect(query);
            }
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }


    protected String getAsString(ColumnType columnType) {
        return getAsString(columnType.getColumnName());
    }

    protected String getAsString(String columnType) {
        if (XBookConfiguration.getDatabaseMode() == XBookConfiguration.DatabaseMode.H2) {
            return "\"" + columnType + "\"";
        } else {
            return "'" + columnType + "'";
        }
    }
}
