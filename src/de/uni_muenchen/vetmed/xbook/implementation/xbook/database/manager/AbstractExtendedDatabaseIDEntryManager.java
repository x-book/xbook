package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

import java.sql.Connection;

/**
 * This class is the base class for all managers that have a additional ID column to distinguish
 * different entries with the same value
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 06.12.2017.
 */
public abstract class AbstractExtendedDatabaseIDEntryManager extends  AbstractExtendedEntryManager{
    public static ColumnType KEY_ID = new ColumnType("KeyID", ColumnType.Type.VALUE, ColumnType.ExportType.NONE);
    public static ColumnType KEY_DATABASE_ID = new ColumnType("KeyDatabaseNumber", ColumnType.Type.VALUE, ColumnType.ExportType.NONE);
    public AbstractExtendedDatabaseIDEntryManager(String tableName, String localisedTableName, int dbNumber, Connection connection, String databaseName) {
        super(tableName, localisedTableName, dbNumber, connection, databaseName);
        ColumnType keyID = new ColumnType(KEY_ID, tableName);
        ColumnType keyDatabaseID = new ColumnType(KEY_DATABASE_ID, tableName);
        dataColumns.add(keyID);
        dataColumns.add(keyDatabaseID);
        primaryColumns.add(keyID.getColumnName());
        primaryColumns.add(keyDatabaseID.getColumnName());

    }
}
