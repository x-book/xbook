package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.Loc;

import java.sql.Connection;

import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rights;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.datatype.User;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class ProjectRightManager extends TableManager {

    public final static String TABLENAME_PROJECTRIGHT = "projectright";

    public static final String PROJECT_RIGHT_PROJECT_ID = TABLENAME_PROJECTRIGHT + "." + AbstractSynchronisationManager.PROJECT_ID;
    public static final String PROJECT_RIGHT_PROJECT_DATABASENUMBER = TABLENAME_PROJECTRIGHT + "." + AbstractSynchronisationManager.PROJECT_DATABASE_ID;
    public static final String PROJECTRIGHT_USERID = TABLENAME_PROJECTRIGHT + ".UID";
    public static final String PROJECTRIGHT_RIGHT = TABLENAME_PROJECTRIGHT + ".Userright";
    public static final ColumnType PROJECT_RIGHTS_READ = new ColumnType(TABLENAME_PROJECTRIGHT + ".Read",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("RIGHT_READ"));
    public static final ColumnType PROJECT_RIGHTS_WRITE = new ColumnType(TABLENAME_PROJECTRIGHT + ".Write",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("RIGHT_WRITE"));

    public static final ColumnType PROJECT_RIGHTS_EDIT_PROJECT = new ColumnType(TABLENAME_PROJECTRIGHT + ".EditProject",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("RIGHT_EDIT_PROJECT"));

    protected UserManager userManager;
    protected ArrayList<ColumnType> rights = new ArrayList<>();

    public ProjectRightManager(UserManager user, Connection connection, String databaseName) {
        super(connection, databaseName);
        this.userManager = user;
        rights.add(PROJECT_RIGHTS_READ);
        rights.add(PROJECT_RIGHTS_WRITE);
        rights.add(PROJECT_RIGHTS_EDIT_PROJECT);
    }

    public boolean hasReadRights(Key projectKey) throws StatementNotExecutedException {
        String query = "SELECT " + PROJECT_RIGHTS_READ
                + " FROM " + TABLENAME_PROJECTRIGHT
                + " WHERE " + PROJECT_RIGHT_PROJECT_ID + "=" + projectKey.getID()
                + " AND " + PROJECT_RIGHT_PROJECT_DATABASENUMBER + "=" + projectKey.getDBID()
                + " AND " + PROJECTRIGHT_USERID + " = " + userManager.getCurrentUserId()
                + " AND " + TABLENAME_PROJECTRIGHT + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " + ";";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                if (rs.getBoolean(1)) {
                    return true;
                }
            }
            return false;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

    public boolean hasWriteRights(Key projectKey) throws StatementNotExecutedException {
        String query = "SELECT " + PROJECT_RIGHTS_WRITE
                + " FROM " + TABLENAME_PROJECTRIGHT
                + " WHERE " + PROJECT_RIGHT_PROJECT_ID + "=" + projectKey.getID()
                + " AND " + PROJECT_RIGHT_PROJECT_DATABASENUMBER + "=" + projectKey.getDBID()
                + " AND " + PROJECTRIGHT_USERID + " = " + userManager.getCurrentUserId()
                + " AND " + TABLENAME_PROJECTRIGHT + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " + ";";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                if (rs.getBoolean(1)) {
                    return true;
                }
            }
            return false;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

    public boolean hasEditProjectRights(Key projectKey) throws StatementNotExecutedException {
        String query = "SELECT " + PROJECT_RIGHTS_EDIT_PROJECT
                + " FROM " + TABLENAME_PROJECTRIGHT
                + " WHERE " + PROJECT_RIGHT_PROJECT_ID + "=" + projectKey.getID()
                + " AND " + PROJECT_RIGHT_PROJECT_DATABASENUMBER + "=" + projectKey.getDBID()
                + " AND " + PROJECTRIGHT_USERID + " = " + userManager.getCurrentUserId()
                + " AND " + TABLENAME_PROJECTRIGHT + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " + ";";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                if (rs.getBoolean(1)) {
                    return true;
                }
            }
            return false;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

    public ArrayList<RightsInformation> getUserRights(Key projKey) throws StatementNotExecutedException {
        ArrayList<RightsInformation> list = new ArrayList<>();
        String select = "";
        for (ColumnType right : rights) {
            select += right + ", ";
        }

        String query = "SELECT " + select + PROJECTRIGHT_USERID + "," + UserManager.DISPLAYNAME
                + " FROM " + TABLENAME_PROJECTRIGHT + "," + UserManager.TABLENAME_USER
                + " WHERE " + PROJECT_RIGHT_PROJECT_ID + "=" + projKey.getID()
                + " AND " + PROJECT_RIGHT_PROJECT_DATABASENUMBER + "=" + projKey.getDBID()
                + " AND " + UserManager.USER_UID + "=" + PROJECTRIGHT_USERID
                + " AND " + TABLENAME_PROJECTRIGHT + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " + ";";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                ArrayList<Rights> rightList = new ArrayList<>();
                for (ColumnType right : rights) {
                    rightList.add(new Rights(right, rs.getBoolean(right)));
                }
                list.add(new RightsInformation(rs.getString(UserManager.DISPLAYNAME), rs.getInt(PROJECTRIGHT_USERID), rightList));
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        return list;
    }

    public ArrayList<User> getUsersForProject(ProjectDataSet loadedProject) throws StatementNotExecutedException {
        UniqueArrayList<User> list = new UniqueArrayList<>();
        String query = "SELECT DISTINCT " + UserManager.USER_UID + "," + UserManager.USERNAME + "," + UserManager.DISPLAYNAME + "," + UserManager.SEARCHABLE
                + " FROM " + UserManager.TABLENAME_USER + "," + TABLENAME_PROJECTRIGHT
                + " WHERE " + PROJECT_RIGHT_PROJECT_ID + "=" + loadedProject.getProjectId()
                + " AND " + PROJECT_RIGHT_PROJECT_DATABASENUMBER + "=" + loadedProject.getProjectDatabaseId()
                + " AND (" + PROJECT_RIGHTS_READ + "=1 "
                + " OR " + PROJECT_RIGHTS_WRITE + "=1 "
                + " OR " + PROJECT_RIGHTS_EDIT_PROJECT + "=1) "
                + " AND " + UserManager.USER_UID + "=" + PROJECTRIGHT_USERID
                + " AND " + UserManager.TABLENAME_USER + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + TABLENAME_PROJECTRIGHT + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' ";
        if (XBookConfiguration.DISPLAY_SOUTS) {
            System.out.println(query);
        }
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                list.add(new User(rs.getInt(UserManager.USER_UID), rs.getString(UserManager.USERNAME), rs.getString(UserManager.DISPLAYNAME), rs.getString(UserManager.ORGANISATION), rs.getBoolean(UserManager.SEARCHABLE)));
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }

        return list;
    }

    public ArrayList<ColumnType> getRights() {
        return rights;
    }

    public HashMap<Key, ArrayList<String>> getUsersWithProjectRights(Collection<Key> thisProject) throws StatementNotExecutedException {
        HashMap<Key, ArrayList<String>> hashMap = new HashMap<>();
        String projectQuery = "(";
        for (Key key : thisProject) {
            projectQuery += "(" + PROJECT_RIGHT_PROJECT_ID + "=" + key.getID()
                    + " AND " + PROJECT_RIGHT_PROJECT_DATABASENUMBER + "=" + key.getDBID() + ") OR ";

        }
        projectQuery = StringHelper.cutString(projectQuery, " OR ") + ")";

        StringBuilder st = new StringBuilder("SELECT ").append(UserManager.DISPLAYNAME).append(", " + PROJECT_RIGHT_PROJECT_ID + ", " + PROJECT_RIGHT_PROJECT_DATABASENUMBER).append(" FROM ").append(UserManager.TABLENAME_USER).append(",").append(TABLENAME_PROJECTRIGHT)
                .append(" WHERE ").append(UserManager.USER_UID).append("=").append(PROJECTRIGHT_USERID)
                .append(" AND ").append(projectQuery).append(" AND ").append(PROJECT_RIGHTS_READ).append("= 1");
        String query = st.toString();
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                Key key = new Key(rs.getInt(PROJECT_RIGHT_PROJECT_ID), rs.getInt(PROJECT_RIGHT_PROJECT_DATABASENUMBER));

                ArrayList<String> list = hashMap.get(key);
                if (list == null) {
                    list = new ArrayList<>();
                    hashMap.put(key, list);
                }
                list.add(rs.getString(UserManager.DISPLAYNAME));
            }

        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        return hashMap;
    }
}
