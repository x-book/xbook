package de.uni_muenchen.vetmed.xbook.implementation.xbook.database;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class that checks if the client has been "isInstalled" on the local
 * machine by connecting to the local database (if the default one is used,
 * otherwise the user is responsible for the database setup herself).
 * <p/>
 * <p>
 * If the connection succeeds, we look for a row in the .version table. If no
 * such row exists, we ask the user to the path of the "setup SQL file",
 * containing the database necessary to personalize a template database. </p>
 *
 * @author fnuecke
 */
public final class DatabaseInitializer {

    /**
     * Indicates if database is running
     */
    public static boolean isDatabaseRunning;
    /**
     * Indicates if database was freshly installed
     */
    public static boolean wasInstalled = false;
    /**
     * Indicates if tha database is initialized
     */
    public static boolean isInitialized = false;
    /**
     * Logging...
     */
    private static final Log log = LogFactory.getLog(DatabaseInitializer.class);


    /**
     * No instantiation needed.
     */
    private DatabaseInitializer() {
    }

    /**
     * Initializes and executes the installer if necessary.
     * <p/>
     * <p/>
     * Creates a connection to the local database and checks if there is an
     * entry in the <code>version</code> table.
     *
     * @param tableName
     * @return
     */
    public static boolean init(String tableName, boolean autoConfigurePort) {
        isDatabaseRunning = true;
        log.info("autoconfigure: "+autoConfigurePort);
        int tryNumber = 0;

        while (++tryNumber < 5) {
            if (retry(tableName, autoConfigurePort)) {
                return true;
            }

            synchronized (DatabaseInitializer.class) {
                try {
                    DatabaseInitializer.class.wait(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(DatabaseInitializer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        log.warn(Loc.get("DBINIT.VERIFY_FAILED"));


        isDatabaseRunning = false;

        return false;

    }

    private static boolean retry(String databaseName, boolean autoConfigurePort) {
        Connection connection = null;
        try {

            // Make connection to local database.
            DatabaseConnection databaseConnection = new DatabaseConnection("root", null, "", autoConfigurePort);

            // Get actual connection to perform queryManager.
            connection = databaseConnection.getUnderlyingConnection();

            if (XBookConfiguration.getDatabaseMode() == XBookConfiguration.DatabaseMode.MYSQL) {
                try {
                    connection.createStatement().execute("SET GLOBAL max_allowed_packet = 1024*1024*" + XBookConfiguration.PACKETSIZE);//set number to represent max number of mb

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            Statement stmt = connection.createStatement();
            String databaseCheckQuery = "CREATE SCHEMA IF NOT EXISTS " + databaseName + (XBookConfiguration.getDatabaseMode() == XBookConfiguration.DatabaseMode.MYSQL ? " CHARACTER SET utf8" : "");
            stmt.execute(databaseCheckQuery);
            if (AbstractConfiguration.sharedUserTable) {
                databaseCheckQuery = "CREATE SCHEMA IF NOT EXISTS " + IStandardColumnTypes.DATABASE_NAME_GENERAL + (XBookConfiguration.getDatabaseMode() == XBookConfiguration.DatabaseMode.MYSQL ? " CHARACTER SET utf8" : "");
                stmt.execute(databaseCheckQuery);

                checkTable(connection, IStandardColumnTypes.DATABASE_NAME_GENERAL);
            }
            checkTable(connection, databaseName);


            return true;
        } catch (SQLException | NotLoggedInException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e2) {
                    log.warn(Loc.get("DBINIT.ERROR_CLOSING_CONNECTION"));
                }
            }
        }
    }

    private static void checkTable(Connection connection, String databaseName) throws SQLException {
        Statement stmt = connection.createStatement();

        DatabaseMetaData database = connection.getMetaData();
        String[] tblTypes = {"TABLE"};
        ResultSet rs = database.getTables(databaseName, null, "%", tblTypes);
        while (rs.next()) {
            String query = "CHECK TABLE " + databaseName + "." + rs.getString("TABLE_NAME");
            final ResultSet resultSet = stmt.executeQuery(query);

            while (resultSet.next()) {
                if (!resultSet.getString(4).equals("OK")) {
                    query = "REPAIR TABLE " + databaseName + "." + rs.getString("TABLE_NAME");
                    System.out.println(query);
                    System.out.println();
                    connection.createStatement().executeQuery(query);
                }
            }
        }
    }

}
