package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType.ExportType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnTypeList;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDisplay;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryKey;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportResult;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.SearchEntryInfo;
import de.uni_muenchen.vetmed.xbook.api.event.ValueEventSender;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLine;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLineGroup;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class AbstractBaseEntryManager extends AbstractEntryManager implements
    ValueEventSender, BaseEntryManager {

  protected static final Log _log = LogFactory.getLog(AbstractBaseEntryManager.class);
  protected UserManager user;
  protected AbstractSynchronisationManager[] managers;
  protected final String DISPLAY_SEPERATOR = ", ";

  public AbstractBaseEntryManager(String tableName, String localisedTableName, int dbNumber,
      Connection connection,
      String databaseName, UserManager user, AbstractSynchronisationManager... manager) {
    super(tableName, localisedTableName, dbNumber, connection, databaseName);
    this.user = user;
    dataColumns = new ColumnTypeList(tableName);
    dataColumns.add(new ColumnType(ID, tableName).setSectionProperty(SECTION_SYSTEM));
    dataColumns
        .add(new ColumnType(DATABASE_ID, tableName).setSectionProperty(SECTION_SYSTEM));
    //.setDataType(            DataType.Integer).setScale(IntervalScale.class));
    dataColumns
        .add(new ColumnType(PROJECT_ID, tableName).setSectionProperty(SECTION_SYSTEM));
    //.setDataType(            DataType.Integer).setScale(IntervalScale.class));
    dataColumns.add(
        new ColumnType(PROJECT_DATABASE_ID, tableName).setSectionProperty(SECTION_SYSTEM));
    //.setDataType(                DataType.Integer).setScale(IntervalScale.class));
    dataColumns.add(new ColumnType(MESSAGE_NUMBER, tableName).setSectionProperty(SECTION_SYSTEM));
    dataColumns.add(new ColumnType(STATUS, tableName).setSectionProperty(SECTION_SYSTEM));
    dataColumns.add(new ColumnType(DELETED, tableName).setSectionProperty(SECTION_SYSTEM));
    primaryColumns = new ArrayList<>();
    primaryColumns.add("" + ID);
    primaryColumns.add("" + DATABASE_ID);
    primaryColumns.add("" + PROJECT_ID);
    primaryColumns.add("" + PROJECT_DATABASE_ID);
    managers = manager;

  }

  private boolean contains(String[] array, String value) {
    for (String s : array) {
      if (s.equals(value)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void loadBase(DataSetOld data) throws StatementNotExecutedException {
    loadBase(data, null);
  }

  @Override
  public void loadBase(DataSetOld entryData, String[] tableFilter)
      throws StatementNotExecutedException {
    if (tableFilter == null || contains(tableFilter, tableName)) {
      super.load(entryData);
    }
    for (AbstractSynchronisationManager table : managers) {
      if (tableFilter == null || contains(tableFilter, table.tableName)) {
        table.load(entryData);
      }
    }
  }

  public void loadConflicts(DataSetOld entryData) throws StatementNotExecutedException {
    super.load(entryData);
    for (AbstractSynchronisationManager table : managers) {
      //only load tables that have no own conflict solving algorithm
      if (!(table instanceof IBaseManager)) {
        table.load(entryData);
      }
    }
  }

  @Override
  public void solveConflict(DataSetOld data) throws StatementNotExecutedException {
    super.solveConflict(data);
    for (AbstractSynchronisationManager table : managers) {
      if (table instanceof AbstractExtendedEntryManager) {
        ((AbstractExtendedEntryManager) table).solveConflict(data);
      }

    }
  }

  @Override
  public void deleteValue(Key entryKey, Key projectKey) throws StatementNotExecutedException {
    super.deleteValue(entryKey, projectKey);
    for (AbstractSynchronisationManager table : managers) {

      if (table instanceof AbstractCrossLinkedManager) {
        AbstractCrossLinkedManager cross = (AbstractCrossLinkedManager) table;
        cross.deleteCrossLinksForEntry(entryKey, projectKey, tableName);
      } else {
        table.deleteValue(entryKey, projectKey);
      }
    }
    notifyUpdate();
  }

  @Override
  public void deletePermanent(ProjectDataSet project, int event)
      throws StatementNotExecutedException {
    super.deletePermanent(project, event);
    for (AbstractSynchronisationManager table : managers) {
      table.deletePermanent(project, event);
    }

  }

  @Override
  public void deletePermanent(Key project, Key entry, int event)
      throws StatementNotExecutedException {
    super.deletePermanent(project, entry, event);
    for (AbstractSynchronisationManager table : managers) {
      if (table instanceof AbstractCrossLinkedManager) {
        AbstractCrossLinkedManager cross = (AbstractCrossLinkedManager) table;
        cross.deletePermanent(project, entry, event, tableName);
      } else {
        table.deletePermanent(project, entry, event);
      }
    }
  }

  @Override
  public void updateEntries(EntryDataSet data) throws StatementNotExecutedException {
    super.updateEntries(data);
    for (AbstractSynchronisationManager table : managers) {
      table.updateEntries(data);
    }

    notifyUpdate();
  }

  @Override
  public void save(DataSetOld data) throws StatementNotExecutedException {
    if (!(data instanceof EntryDataSet)) {
      Logger.getLogger(AbstractEntryManager.class.getName())
          .log(Level.SEVERE, "DataSet not EntryDataSet!");

      return;
    }
    save((EntryDataSet) data);
  }

  @Override
  public void save(EntryDataSet data) throws StatementNotExecutedException {

    if (data.getEntryKey() != null && data.getEntryKey().isValidKey()) {//update
      super.save(data);
    } else {
      DataRow dataRow = data.getDataRowForTable(tableName);

      int id;
      String highestID = "SELECT MAX(" + ID + ") FROM " + databaseName + "." + tableName
          + " WHERE " + PROJECT_ID + "=" + data.getProjectKey().getID()
          + " AND " + PROJECT_DATABASE_ID + "=" + data.getProjectKey().getDBID()
          + " AND " + DATABASE_ID + "=" + dbId;

      ResultSet rs;
      try {
        rs = executeSelect(highestID);
        if (rs.next()) {
          id = rs.getInt("MAX(" + ID + ")") + 1;
        } else {
          id = 0;
        }
      } catch (SQLException e) {
        throw new StatementNotExecutedException(highestID, e);
      }

//                ArrayList<DataColumn> dataList = new ArrayList<>();
      DataRow dataList = new DataRow(tableName);
      dataList.add(new DataColumn(id, ID));
      dataList.add(new DataColumn(dbId, DATABASE_ID));
      dataList.add(new DataColumn(data.getProjectKey().getID(), PROJECT_ID));
      dataList.add(new DataColumn(data.getProjectKey().getDBID(), PROJECT_DATABASE_ID));
      DataColumn userData = getUserInformation();
      if (userData != null) {
        dataList.add(userData);
      }
      for (DataColumn row : dataRow.iterator()) {
        dataList.add(row);
      }
      dataList.add(new DataColumn(1, MESSAGE_NUMBER));
      dataList.add(new DataColumn(IStandardColumnTypes.DELETED_NO, DELETED));

      insertData(dataList, databaseName + "." + tableName);
      data.setEntryKey(new Key(id, dbId));
    }
    for (AbstractSynchronisationManager table : managers) {
      table.save(data);
    }

    notifyUpdate();
  }

  public void save(EntryDataSet data, ArrayList<Key> list) throws StatementNotExecutedException {
    if (!data.hasDataTable(tableName)) {
      saveBase(data, list);
    } else {
      super.save(data, list);
    }

    for (AbstractSynchronisationManager table : managers) {
      table.save(data, list);
    }

    notifyUpdate();
  }

  protected void saveBase(EntryDataSet data, ArrayList<Key> keys)
      throws StatementNotExecutedException {

    String keyQuery = "(";
    for (Key key : keys) {
      keyQuery += "(" + ID + "=" + key.getID()
          + " AND " + DATABASE_ID + "=" + key.getDBID() + ") OR ";
    }
    if (keyQuery.endsWith(" OR ")) {
      keyQuery = keyQuery.substring(0, keyQuery.length() - " OR ".length()) + ")";
    }
    String query = "UPDATE " + databaseName + "." + tableName
        + " SET " + MESSAGE_NUMBER + "=" + 1
        + " WHERE " + keyQuery
        + " AND " + PROJECT_ID + "=" + data.getProjectKey().getID()
        + " AND " + PROJECT_DATABASE_ID + "=" + data.getProjectKey().getDBID();
    try {
      executeQuery(query);
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }


  }

//    public void saveBase(DataSetOld data) throws StatementNotExecutedException {
//
//    }

  protected abstract DataColumn getUserInformation() throws StatementNotExecutedException;

  /**
   * Returns the next Entry that is not yet committed and is not conflicted
   *
   * @param project The project to get the Entry for
   * @return The Entry.
   * @throws NotLoggedInException If the user is not logged in.
   * @throws StatementNotExecutedException If a sql error occurred
   * @throws NoRightException If the user has no rights to write in the Project
   */
  @Override
  public EntryDataSet getNextUncomittedEntry(
      DataSetOld project)
      throws StatementNotExecutedException, IOException {

    EntryDataSet data = new EntryDataSet(null, project.getProjectKey(), databaseName, tableName);

    EntryKey key = new EntryKey(null, project.getProjectKey());
    data.addDataTable(getNextUncommitedEntry(key));

    if (data.hasEntries()) {
      data.setEntryKey(key.getEntryKey());
      for (AbstractSynchronisationManager table : managers) {
        if (table instanceof IBaseManager) {
          continue;
        }
        if (table instanceof AbstractEntryManager) {
          AbstractEntryManager entryManager = ((AbstractEntryManager) table);
          data.addDataTable(entryManager.getNextUncommitedEntry(key));

        } else {
          _log.debug("getNextUncomittedEntry not declared for " + table.tableName);
        }

      }
    } else {
      for (AbstractSynchronisationManager table : managers) {
        if (table instanceof IBaseManager) {
          continue;
        }
        if (table.getNumberOfUncommittedEntries(project.getProjectKey()) > 0) {
          AbstractEntryManager entryManager = ((AbstractEntryManager) table);
          data.addDataTable(entryManager.getNextUncommitedEntry(key));
          data.setEntryKey(key.getEntryKey());
          return data;
        }
      }
    }
    return data;
  }


  /**
   * Returns the number of uncommitted Entries for the given Project
   *
   * @param project The Project to get the number of entrys from
   * @return The number of uncommitted Entries for the given Project.
   * @throws NotLoggedInException If the user is not logged in.
   * @throws StatementNotExecutedException If a sql error occurred
   * @throws NoRightException If the user has no right to write to Project
   */
  public int getNumberOfUncommittedEntries(
      DataSetOld project)
      throws NotLoggedInException, StatementNotExecutedException, NoRightException {

    return getNumberOfUncommittedEntries(project.getProjectKey());
  }

  public ExportResult getEntriesForListing(DataSetOld project, ArrayList<ColumnType> list,
      ExportType type,
      boolean conflict, int offset, int count,
      HashMap<ColumnType, SearchEntryInfo> filter)
      throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
    return getEntries(project, list, type, conflict, offset, count, null, filter, true);
  }

  /**
   * Returns the given amount of entries with the given offset.
   *
   * @param offset The offset of the entrys returned.
   * @param count The amount of entries to be returned.
   * @return The given amount of entries with the given offset.
   * @throws NotLoggedInException If the user is not logged in.
   * @throws NotLoadedException If no Project is loaded
   * @throws StatementNotExecutedException If a sql error occurred
   */
  public ExportResult getEntries(DataSetOld project, ArrayList<ColumnType> list,
      ExportType type,
      boolean conflict, int offset, int count, HashMap<ColumnType, SearchEntryInfo> filter,
      boolean hideNonDefaultFields)
      throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
    return getEntries(project, list, type, conflict, offset, count, null, filter,
        hideNonDefaultFields);
  }

  public ExportResult getEntries(DataSetOld project, ArrayList<ColumnType> list,
      ExportType type,
      boolean conflict, int offset, int count, ArrayList<Key> entries,
      HashMap<ColumnType, SearchEntryInfo> filter, boolean hideNonDefaultFields)
      throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
    ExportResult result = new ExportResult();
    ArrayList<Key> projects = new ArrayList<>();
    projects.add(project.getProjectKey());
    result = getEntryData(result, projects, list,
        type == ExportType.CROSS_LINKED ? ExportType.GENERAL : type, conflict, count, offset,
        entries, filter,
        hideNonDefaultFields);

    ArrayList<Key> keys = result.getKeyList();
    if (!keys.isEmpty()) {
      for (AbstractSynchronisationManager table : managers) {
        if (!(table instanceof AbstractExtendedEntryManager)) {
          if (table instanceof ExportableManager) {
            ExportableManager crossLinkedManager = ((ExportableManager) table);
            result = crossLinkedManager
                .getEntryData(result, project.getProjectKey(), tableName, list, type,
                    conflict, keys, filter, hideNonDefaultFields);
            continue;
          }
          _log.info("table " + table.getTableName() + " skipped for getEntries");
          continue;
        }
        result = ((AbstractExtendedEntryManager) table)
            .getEntryData(result, projects, list,
                type == ExportType.CROSS_LINKED ? ExportType.GENERAL : type, keys, filter,
                hideNonDefaultFields);
      }
    }
    return result;
  }

  public ExportResult getFilteredEntriesForListing(ProjectDataSet project, ArrayList<Key> entryKeys,
      ArrayList<ColumnType> list, ExportType type,
      boolean conflict,
      int offset, int count, HashMap<ColumnType, SearchEntryInfo> filter)
      throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
    return getFilteredEntries(project, entryKeys, list, type, conflict, offset, count, filter,
        true);
  }

  public ExportResult getFilteredEntries(ProjectDataSet project, ArrayList<Key> entryKeys,
      ArrayList<ColumnType> list, ExportType type, boolean conflict,
      int offset, int count, HashMap<ColumnType, SearchEntryInfo> filter)
      throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
    return getFilteredEntries(project, entryKeys, list, type, conflict, offset, count, filter,
        false);
  }

  /**
   * Returns the given amount of entries with the given offset.
   *
   * @param offset The offset of the entrys returned.
   * @param count The amount of entries to be returned.
   * @return The given amount of entries with the given offset.
   * @throws NotLoggedInException If the user is not logged in.
   * @throws NotLoadedException If no Project is loaded
   * @throws StatementNotExecutedException If a sql error occurred
   */
  public ExportResult getFilteredEntries(ProjectDataSet project, ArrayList<Key> entryKeys,
      ArrayList<ColumnType> list, ExportType type, boolean conflict,
      int offset, int count, HashMap<ColumnType, SearchEntryInfo> filter,
      boolean forListing)
      throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {

    ExportResult result = new ExportResult();
    ArrayList<Key> projects = new ArrayList<>();
    projects.add(project.getProjectKey());
    getEntryData(result, projects, list, type, conflict, count, offset, entryKeys, filter,
        forListing);

    ArrayList<Key> keys = result.getKeyList();
    for (AbstractSynchronisationManager table : managers) {
      if (!(table instanceof AbstractExtendedEntryManager)) {
        continue;
      }
      ((AbstractExtendedEntryManager) table)
          .getEntryData(result, projects, list, type, keys, filter, false);
    }
    return result;
  }

  @Override
  public ExportResult getEntriesForDisplay(Key projectKey, Key entryKey)
      throws StatementNotExecutedException {
    ExportResult result = new ExportResult();
    ArrayList<Key> projects = new ArrayList<>();
    projects.add(projectKey);
    getEntryData(result, projects, entryKey, false);
    if (result.getKeyList().isEmpty()) {
      return result;
    }
    ArrayList<Key> keys = new ArrayList<>();
    keys.add(entryKey);

    for (AbstractSynchronisationManager table : managers) {
      if (!(table instanceof AbstractExtendedEntryManager)) {
        if (table instanceof AbstractCrossLinkedManager) {
          ExportableManager crossLinkedManager = ((ExportableManager) table);
          crossLinkedManager
              .getEntryData(result, projectKey, tableName, null, ColumnType.ExportType.GENERAL,
                  false, keys, null, false);
          continue;
        }
        continue;
      }
      ((AbstractExtendedEntryManager) table).getEntryData(result, projects, entryKey, true);
    }
    return result;
  }

  public ArrayList<EntryDisplay> getEntryOverview(Key projectKey)
      throws StatementNotExecutedException {
    _log.warn("Called unoverriden getEntryOverview!");
    return new ArrayList<>();
  }

  /**
   * Set the given data to synchronized
   *
   * @param entryData the data to be synchronized
   * @throws StatementNotExecutedException If a sql error occurred
   */
  @Override
  public void setSynchronised(DataSetOld entryData, DataSetOld returnData)
      throws StatementNotExecutedException {

    super.setSynchronised(entryData, returnData);

    for (AbstractSynchronisationManager table : managers) {
      if (table instanceof IBaseManager) {
        continue;
      }
      table.setSynchronised(entryData, returnData);
    }
  }

  /**
   * Sets the given data to conflicted
   *
   * @param entryData the data to be synchronized
   * @throws StatementNotExecutedException If a sql error occurred
   */
  @Override
  public void setConflicted(DataSetOld entryData) throws StatementNotExecutedException {
    super.setConflicted(entryData);
    for (AbstractSynchronisationManager table : managers) {
      if (table instanceof IBaseManager) {
        continue;
      }
      table.setConflicted(entryData);
    }
  }

  @Override
  public void updateUnsyncedEntries(ProjectDataSet project) throws StatementNotExecutedException {
    super.updateUnsyncedEntries(project);
    for (AbstractSynchronisationManager table : managers) {
      if (table instanceof IBaseManager) {
        continue;
      }
      table.updateUnsyncedEntries(project);
    }
  }

  public void setUpdate(EntryDataSet entryData) throws StatementNotExecutedException {
    ArrayList<DataColumn> primList = new ArrayList<>();
    primList.add(
        new DataColumn(entryData.getEntryKey().getID() + "",
            AbstractExtendedEntryManager.ID.getColumnName()));
    primList.add(new DataColumn(entryData.getEntryKey().getDBID() + "",
        AbstractExtendedEntryManager.DATABASE_ID.getColumnName()));
    primList.add(new DataColumn(entryData.getProjectKey().getID() + "",
        AbstractExtendedEntryManager.PROJECT_ID.getColumnName()));
    primList.add(new DataColumn(entryData.getProjectKey().getDBID() + "",
        AbstractExtendedEntryManager.PROJECT_DATABASE_ID.getColumnName()));
    setStatusToSynchronise(primList);
    for (AbstractSynchronisationManager sInt : managers) {
      if (!(sInt instanceof AbstractExtendedEntryManager)) {
        continue;
      }
      sInt.setStatusToSynchronise(primList);
    }
  }

  @Override
  public ArrayList<String> getPrimaryColumnsForTable(String tableName) {
    if (this.tableName.equals(tableName)) {
      return primaryColumns;
    }
    for (AbstractSynchronisationManager manager : managers) {
      if (manager.tableName.equals(tableName)) {
        return manager.primaryColumns;
      }
    }
    return null;
  }

  public AbstractSynchronisationManager[] getManagers() {
    return managers;
  }

  /**
   * Method checks if a combination of given input values already exist in the database.
   *
   * @param projectKey The Key of the AbstractProjectOverviewScreen including ID and DATABASE_ID
   * @param toCheck A list of DataColumns that should be checked for uniqueness.
   * @return <code>true</code> if there already exists an entry with the given
   * data, <code>false</code> else.
   */
  public boolean combinationAlreadyExistsInDatabase(Key projectKey,
      ArrayList<DataColumn> toCheck) throws StatementNotExecutedException {
    return combinationAlreadyExistsInDatabase(projectKey, null, toCheck);
  }

  /**
   * Method checks if a combination of given input values already exist in the database.
   *
   * @param projectKey The Key of the AbstractProjectOverviewScreen including ID and DATABASE_ID
   * @param entryKey The Key of the Entry that should be ingnored while checking. Use
   * <code>null</code> to ignore no entry key.
   * @param toCheck A list of DataColumns that should be checked for uniqueness.
   * @return <code>true</code> if there already exists an entry with the given
   * data, <code>false</code> else.
   */
  public boolean combinationAlreadyExistsInDatabase(Key projectKey, Key entryKey,
      ArrayList<DataColumn> toCheck) throws StatementNotExecutedException {
    String query = "SELECT count(*) FROM " + databaseName + "." + tableName
        + " WHERE " + PROJECT_ID + "='" + projectKey.getID() + "'"
        + " AND " + PROJECT_DATABASE_ID + "='" + projectKey.getDBID() + "'"
        + " AND " + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "'";
    if (entryKey != null) {
      query += " AND NOT (" + ID + "='" + entryKey.getID() + "'"
          + " AND " + DATABASE_ID + "='" + entryKey.getDBID() + "')";
    }
    for (DataColumn col : toCheck) {
      query += " AND " + col.getColumnName() + "='" + col.getValue() + "'";
    }
    query += ";";

    LogFactory.getLog(this.getClass()).debug(query);

    try {
      ResultSet rs = executeSelect(query);
      if (rs.next()) {
        int count = rs.getInt("count(*)");
        return (count > 0);
      }
      return false;
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query, ex);
    }
  }

  @Override
  public SolveConflictLineGroup getLinesForEntry(DataRow localAll,
      DataRow globalRows) throws StatementNotExecutedException, EntriesException {
    String lastSync = "0";
    String deletedGlob = "Y";
    SolveConflictLineGroup lines = new SolveConflictLineGroup(tableName);
    HashMap<Integer, SolveConflictLine> groups = new HashMap<>();
    List<ColumnType> specialColumns = getSepcialColumnsForConflict();
    for (DataColumn local : localAll.iterator()) {//go through local data
      String id = ColumnHelper.removeDatabaseName(local.getColumnName());
      boolean foundServer = false;
      String value = local.getValue();
      for (DataColumn global : globalRows.iterator()) {//go through server data
        String globalValue1 = global.getValue();
        if (ColumnHelper.removeDatabaseName(global.getColumnName()).equals(
            STATUS.getColumnName())) {//remember server status
          lastSync = globalValue1;
        } else if (ColumnHelper.removeDatabaseName(global.getColumnName()).equals(
            DELETED.getColumnName())) {//remember if data is deleted on server
          deletedGlob = globalValue1;
        }
        if (id
            .equals(ColumnHelper.removeDatabaseName(global.getColumnName()))) {//if entrys equal...
          foundServer = true;
          for (ColumnType column : dataColumns) {//go through all data columns
            if (specialColumns.contains(column)) {
              continue;
            }
            if (id.equals(ColumnHelper.removeDatabaseName(column.getColumnName()))) {
              if (column.getType() == ColumnType.Type.DATE) {
                if (value.isEmpty()) {
                  value = column.getDefaultValue();
                }
                if (globalValue1.isEmpty()) {
                  globalValue1 = column.getDefaultValue();
                }
              }
              String left = getStringRepresentation(column, value);//get display for the entry
              String right = getStringRepresentation(column, globalValue1);
              HashMap<ColumnType, String> localValue = new HashMap<>();

              localValue.put(column, value);
              HashMap<ColumnType, String> globalValue = new HashMap<>();
              globalValue.put(column, globalValue1);
              boolean inGroup = false;
              for (int i = 0; i < columnGroup.size(); i++) {//go through all groups
                if (columnGroup.get(i).contains(column)) {//if column is in group
                  inGroup = true;
                  SolveConflictLine line = groups.get(i);
                  if (line == null) {//create new group if there is none
                    line = new SolveConflictLine(localValue, left, globalValue, right,
                        column.getDisplayName(), tableName);
                    groups.put(i, line);
                    lines.add(line);
                  } else {
                    line.addLine(new SolveConflictLine(localValue, left, globalValue, right,
                        column.getDisplayName(), tableName));

                  }
                  break;
                }
              }
              if (!inGroup) {//not in group just add
                lines.add(new SolveConflictLine(localValue, left, globalValue, right,
                    column.getDisplayName(), tableName));
              }
              break;
            }
          }
          break;
        }
      }
      if (globalRows.isEmpty() || !foundServer) {//if no server data is available for this entry
        for (ColumnType column : dataColumns) {
          if (specialColumns.contains(column)) {
            continue;
          }
          if (column.equals(ID) || column.equals(DATABASE_ID) || column.equals(PROJECT_ID) || column
              .equals(
                  PROJECT_DATABASE_ID)) {
            continue;
          }
          if (id.equals(ColumnHelper.removeDatabaseName(column.getColumnName()))) {
            String left = getStringRepresentation(column, value);
            HashMap<ColumnType, String> localValue = new HashMap<>();
            localValue.put(column, value);
            HashMap<ColumnType, String> globalValue = new HashMap<>();
            globalValue.put(column, column.getDefaultValue());

            SolveConflictLine newLine = new SolveConflictLine(localValue, left, globalValue, "",
                column.getDisplayName(), tableName);
            boolean inGroup = false;
            for (int i = 0; i < columnGroup.size(); i++) {
              if (columnGroup.get(i).contains(column)) {
                inGroup = true;
                SolveConflictLine line = groups.get(i);
                if (line == null) {
                  line = newLine;
                  groups.put(i, line);
                  lines.add(newLine);
                } else {
                  line.addLine(newLine);

                }
                break;
              }
            }
            if (!inGroup) {
              lines.add(newLine);
            }
            break;
          }
        }
      }
    }
    if (localAll.isEmpty()) {//only server data is available
      for (DataColumn server : globalRows.iterator()) {
        String id = ColumnHelper.removeDatabaseName(server.getColumnName());
        if (id.equals(STATUS.getColumnName())) {
          lastSync = server.getValue();
        } else if (id.equals(DELETED.getColumnName())) {
          deletedGlob = server.getValue();
        }
        for (ColumnType column : dataColumns) {
          if (specialColumns.contains(column)) {
            continue;
          }
          if (column.equals(ID) || column.equals(DATABASE_ID) || column.equals(PROJECT_ID) || column
              .equals(
                  PROJECT_DATABASE_ID)) {
            continue;
          }

          if (id.equals(ColumnHelper.removeDatabaseName(column.getColumnName()))) {
            String right = getStringRepresentation(column, server.getValue());
            HashMap<ColumnType, String> localValue = new HashMap<>();
            localValue.put(column, column.getDefaultValue());
            HashMap<ColumnType, String> globalValue = new HashMap<>();
            globalValue.put(column, server.getValue());

            SolveConflictLine newLine = new SolveConflictLine(localValue, "", globalValue, right,
                column.getDisplayName(), tableName);
            boolean inGroup = false;
            for (int i = 0; i < columnGroup.size(); i++) {
              if (columnGroup.get(i).contains(column)) {
                inGroup = true;
                SolveConflictLine line = groups.get(i);
                if (line == null) {
                  line = newLine;
                  groups.put(i, line);
                  lines.add(newLine);
                } else {
                  line.addLine(newLine);

                }
                break;
              }
            }
            if (!inGroup) {
              lines.add(newLine);
            }
            break;
          }

        }
      }
    }
    getSpecialLinesForEntry(localAll, globalRows, lines);
    lines.setLastSync(lastSync);
    lines.setDeletedGlobal(deletedGlob.equals(IStandardColumnTypes.DELETED_YES));
    return lines;
  }

//    //this should not be used...
//    @Override
//    protected ArrayList<ColumnType> getConflictableColumns() {
//        return null;
//    }

  public String toString(ExportRow e) {
    StringBuilder sb = new StringBuilder();

    // variables for id, bdid, pid and pdbid
    Map.Entry<ColumnType, ExportColumn> id = null;
    Map.Entry<ColumnType, ExportColumn> dbid = null;
    Map.Entry<ColumnType, ExportColumn> pid = null;
    Map.Entry<ColumnType, ExportColumn> pdbid = null;

    // iterate all elements to display. save id, dbid, pid and pdbid in variales.
    // other elements should be added to the string.
    for (Map.Entry<ColumnType, ExportColumn> s : e.entrySet()) {
      // ID is not working for columntype! that's why equals with getColumnName() WHY???
      //for display of Entries in Conflict screen this seems required. therefore .getColumnName() removed
      if (s.getKey().equals(AbstractInputUnitManager.ID)) {
        id = s;
      } else if (s.getKey().equals(AbstractInputUnitManager.DATABASE_ID)) {
        dbid = s;
      } else if (s.getKey().equals(AbstractInputUnitManager.PROJECT_ID)) {
        pid = s;
      } else if (s.getKey().equals(AbstractInputUnitManager.PROJECT_DATABASE_ID)) {
        pdbid = s;
      } else {
        sb.append(s.getKey().getDisplayName()).append(": ").append(s.getValue())
            .append(DISPLAY_SEPERATOR);
      }
    }
    sb.delete(sb.length() - DISPLAY_SEPERATOR.length(), sb.length());

    // create the string for the first line
    StringBuilder firstLine = new StringBuilder();
    if (id != null) {
      firstLine.append(id.getKey().getDisplayName()).append(": ").append(id.getValue())
          .append(DISPLAY_SEPERATOR);
    }
    if (dbid != null) {
      firstLine.append(dbid.getKey().getDisplayName()).append(": ").append(dbid.getValue()).append(
          DISPLAY_SEPERATOR);
    }
    if (pid != null) {
      firstLine.append(pid.getKey().getDisplayName()).append(": ").append(pid.getValue()).append(
          DISPLAY_SEPERATOR);
    }
    if (pdbid != null) {
      firstLine.append(pdbid.getKey().getDisplayName()).append(": ").append(pdbid.getValue())
          .append(
              DISPLAY_SEPERATOR);
    }
    firstLine.delete(firstLine.length() - DISPLAY_SEPERATOR.length(), firstLine.length());

    return "<b>►&nbsp;&nbsp;&nbsp;" + firstLine.toString() + ":</b><br />" + sb.toString();
  }

  public abstract String toString(Key projectKey, Key entryKey, boolean html)
      throws StatementNotExecutedException;

  /**
   * Returns the columns that shall be displayed as an overview for conflicts.
   *
   * @return the columns that shall be displayed as an overview for conflicts.
   */
  public abstract ArrayList<ColumnType> getConflictDisplayColumns();

  @Override
  public ArrayList<Key> getExistingEntriesForValue(ColumnType columnType, String value,
      Key projectKey) throws StatementNotExecutedException {
    String query = "SELECT " + ID + ", " + DATABASE_ID
        + " FROM " + tableName
        + " WHERE " + PROJECT_ID + "=" + projectKey.getID() + " AND " + PROJECT_DATABASE_ID + "="
        + projectKey.getDBID()
        + " AND " + columnType + "='" + value + "' AND " + DELETED + " = '" + DELETED_NO + "';";
    try {
      xResultSet result = executeSelect(query);
      ArrayList<Key> keys = new ArrayList<>();
      while (result.next()) {
        Key key = new Key(result.getInt(ID), result.getInt(DATABASE_ID));
        keys.add(key);

      }
      return keys;
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query, ex);
    }
  }

  /**
   * Tests if the given key for the given par of ColumnTypes exists for the given project.
   *
   * @param id The ColumnType for the id of the given key.
   * @param databaseID The ColumnType for the databaseID for the dbID of the given key.
   * @param key The key of the entry to search for.
   * @param projectKey The Project key of the project to search for
   * @return true if the entry exists in the current project, false if not
   */
  public boolean checkLinkedEntries(ColumnType id, ColumnType databaseID, Key key,
      Key projectKey) throws StatementNotExecutedException {
    String query = "SELECT " + ID + ", " + DATABASE_ID + " FROM " + tableName
        + " WHERE " + id + "=" + key.getID() + " AND " + databaseID + "=" + key.getDBID()
        + " AND " + PROJECT_ID + "=" + projectKey.getID() + " AND " + PROJECT_DATABASE_ID + "="
        + projectKey.getDBID()
        + " AND " + DELETED + "='" + DELETED_NO + "';";
    try {
      xResultSet rs = executeSelect(query);
      return rs.next();

    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }
  }

  public boolean isEntryValid(Key projectKey, Key key) throws StatementNotExecutedException {
    String query =
        "SELECT 1 "
            + " FROM " + tableName
            + " WHERE " + ID + "='" + key.getID() + "'"
            + " AND " + DATABASE_ID + "='" + key.getDBID() + "'"
            + " AND " + PROJECT_ID + "='" + projectKey.getID() + "'"
            + " AND " + PROJECT_DATABASE_ID + "='" + projectKey.getDBID() + "'"
            + " AND " + DELETED + "='" + DELETED_NO + "';";

    try {
      final xResultSet xResultSet = executeSelect(query);
      return (xResultSet.next());

    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }
  }
}
