package de.uni_muenchen.vetmed.xbook.implementation.xbook.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This Class is the supporting Class for DBConnection. It Provides the default
 * settings for an ossobook Connection to the used MYSQL DB on Local or
 * MainHost.
 *
 * @author ali
 */
public class DatabaseConnection {

    private static final Log LOGGER = LogFactory.getLog(DatabaseConnection.class);
    /**
     * Initialize database driver once.
     */
    static {
        try {
            switch (XBookConfiguration.getDatabaseMode()) {
                case MYSQL: {
                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                    break;
                }
                case H2: {
                    Class.forName("org.h2.Driver").newInstance();
                    break;
                }
                case SQLITE:
                case NONE:
                default:
                    throw new ClassNotFoundException("Database not defined");
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private Connection connection;

    public DatabaseConnection(String username, String password, String database, boolean autoConfigurePort) throws SQLException, NotLoggedInException {
        this("localhost", username, password, database, autoConfigurePort);
    }

    /**
     * Creates a new instance of mySqlConnection using 'ossobookDBConfig.txt'
     * and login info from ossobook
     *
     * @param hostAdress
     * @param username
     * @param password
     * @param database
     * @throws SQLException
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException
     */
    public DatabaseConnection(String hostAdress, String username, String password, String database, boolean autoConfigurePort) throws SQLException, NotLoggedInException {
        String host;
        host = hostAdress + ":" + XBookConfiguration.getDatabasePort(false, autoConfigurePort);

        switch (XBookConfiguration.getDatabaseMode()) {
            case MYSQL: {
                LOGGER.info("jdbc:mysql://" + host + "/" + database);
                connection = DriverManager.getConnection("jdbc:mysql://" + host + (database.equals("") ? "" : "/" + database) + "?zeroDateTimeBehavior=convertToNull&autoReconnect=true&useUnicode=yes&characterEncoding=UTF-8", username, password);
                break;
            }
            case H2: {
                //TODO find out if ZERODATETIMEBEHAVIOR is required
                LOGGER.info("jdbc:h2:./assets/database/xbook;MODE=MySQL;" + (database.equals("") ? "" : "SCHEMA=" + database));

                connection = DriverManager.getConnection("jdbc:h2:./assets/database/xbook;MODE=MySQL;" + (database.equals("") ? "" : "SCHEMA=" + database), username, password);
                printProjects(database);
                break;
            }
            case SQLITE:
            case NONE:
            default:
                throw new SQLException("Database not defined");
        }
    }

    private void printProjects(String database) {
        if (database == null || database.isEmpty())
            return;
        try {
            ResultSet resultSet = connection.createStatement().executeQuery("SELECT * FROM " + database + ".project");
            int columnCount = resultSet.getMetaData().getColumnCount();
            String header = "";
            for (int i = 0; i < columnCount; i++) {
                header += resultSet.getMetaData().getColumnName(i + 1) + " | ";
            }
            header = StringHelper.cutString(header, " | ");
            System.out.println(header);
            while (resultSet.next()) {
                StringBuilder row = new StringBuilder();
                for (int i = 0; i < columnCount; i++) {
                    row.append(resultSet.getString(i + 1)).append(" | ");
                }
                System.out.println(StringHelper.cutString(row.toString(), " | "));

            }

        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    /**
     * Get the database name this connection is set up to connect to.
     *
     * @return the database name for this connection.
     */
    public String getDatabase() {
        try {
            return connection.getCatalog();
        } catch (SQLException e) {
            return null;
        }
    }

    /**
     * Reference to the actual lowlevel connection used.
     *
     * @return actual connection used.
     */
    public Connection getUnderlyingConnection() {
        return connection;
    }
}
