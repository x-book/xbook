package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.manager.xbook.IUserManager;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.User;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Result;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class UserManager extends TableManager implements IUserManager {

    private static final Log LOGGER = LogFactory.getLog(UserManager.class);

    private String currentuser = null;
    private int userid = -1;

    public UserManager(Connection connection, String databaseName) {
        super(connection, databaseName);
    }

    /**
     * @return the users of the database except the admins and the logged in
     * userManager
     * @throws StatementNotExecutedException
     */
    public ArrayList<String> getDisplayNames() throws StatementNotExecutedException {
        ArrayList<String> users = new ArrayList<>();
        String query = "SELECT " + DISPLAYNAME + " FROM "
                + TABLENAME_USER + " WHERE "
                + USERNAME + "!='" + getCurrentUser(false) + "' AND "
                + ADMIN + " !=" + ISADMIN + " ORDER BY " + DISPLAYNAME + ";";
        try {
            ResultSet rs = executeSelect(query);
            while (rs.next()) {
                users.add(rs.getString(1));
            }
            return users;
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

//    /**
//     * Returns all the USers with their rights
//     *
//     * @param project
//     * @return
//     * @throws de.uni_muenchen.vetmed.xbook.xbook.exceptions.StatementNotExecutedException
//     */
//    public ArrayList<UserRight> getUserRights(AbstractProjectOverviewScreen project) throws StatementNotExecutedException {
//        ArrayList<UserRight> users = new ArrayList<>();
//        String queryManager = "SELECT " + DISPLAYNAME + "," + USER_UID + " FROM "
//                + TABLENAME_USER + " ORDER BY " + DISPLAYNAME + ";";
//        try {
//            ResultSet rs = executeSelect(queryManager);
//            while (rs.next()) {
//                users.add(new UserRight(rs.getString(removeDatabaseName(DISPLAYNAME)), rs.getInt(removeDatabaseName(USER_UID)), getRight(rs.getInt(removeDatabaseName(USER_UID)), project)));
//            }
//            return users;
//        } catch (SQLException e) {
//            throw new StatementNotExecutedException(queryManager,e);
//        }
//    }

    /**
     * proofs whether user may do everything
     *
     * @return true if User is administrator, else false
     * @throws StatementNotExecutedException
     */
    public boolean isAdmin() throws StatementNotExecutedException {
        String query = "SELECT " + ADMIN + " FROM "
                + TABLENAME_USER
                + " WHERE " + USER_UID + "='" + getCurrentUserId()
                + "' AND " + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "';";
        try {
            ResultSet rs = executeSelect(query);
            return rs.next() && (((xResultSet) rs).getBoolean(1) || rs.getInt(1) == ISDEVELOPER);
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    /**
     * proofs whether user may do everything
     *
     * @return true if User is administrator, else false
     * @throws StatementNotExecutedException
     */
    public boolean isDeveloper() throws StatementNotExecutedException {
        String query = "SELECT " + ADMIN + " FROM "
                + TABLENAME_USER
                + " WHERE " + USER_UID + "='" + getCurrentUserId()
                + "' AND " + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "';";
        try {
            ResultSet rs = executeSelect(query);
            return rs.next() && rs.getInt(1) == ISDEVELOPER;
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    /**
     * Returns the corresponding ID of the CurrentUser
     *
     * @return The User ID of the given User;
     * @throws StatementNotExecutedException
     */
    public int getCurrentUserId() throws StatementNotExecutedException {
        if (userid != -1) {
            return userid;
        }
        String query = "SELECT " + USER_UID + " FROM " + TABLENAME_USER
                + " WHERE " + USERNAME + "= SUBSTRING_INDEX(USER(),'@',1);";

        if (XBookConfiguration.getDatabaseMode() == XBookConfiguration.DatabaseMode.H2) {
            query = "SELECT " + USER_UID + " FROM " + TABLENAME_USER
                    + " WHERE upper(" + USERNAME + ")= USER();";

            try {
                xResultSet rs2  = executeSelect("SELECT USER()");
                if (rs2.next()) {
                    System.out.println("USERNAME :" + rs2.getString(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        try {
            xResultSet rs = executeSelect(query);
            if (rs.next()) {
                userid = rs.getInt(USER_UID); //
                return userid;
            }
            LOGGER.error("USER NOT FOUND!");
            throw new StatementNotExecutedException(query);
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }

    }

    /**
     * Returns the Current User name
     *
     * @param force Force an update of the saved current user in the user manager.
     * @return The Name of the User currently logged in
     * @throws StatementNotExecutedException
     */
    public String getCurrentUser(boolean force) throws StatementNotExecutedException {
        if (currentuser != null && !force) {
            return currentuser;
        }
        currentuser = getDisplayName(getCurrentUserId());
        return currentuser;
    }

    /**
     * Returns the Username corresponding to the given ID
     *
     * @param id The id of the User
     * @return The Name of the User
     * @throws StatementNotExecutedException
     */
    public String getDisplayName(int id) throws StatementNotExecutedException {
        String query = "SELECT " + DISPLAYNAME + " from " + TABLENAME_USER
                + " where " + USER_UID + "=" + id + ";";
        try {
            ResultSet rs = executeSelect(query);
            if (rs.next()) {
                return rs.getString(1);
            }
            return "Unknown User";

        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    public ArrayList<User> getVisibleUsers() throws StatementNotExecutedException {
        ArrayList<User> list = new ArrayList<>();
        String query = "SELECT " + DISPLAYNAME + "," + USERNAME + "," + ORGANISATION + "," + USER_UID + " FROM "
                + TABLENAME_USER
                + " WHERE " + SEARCHABLE + "=1 ORDER BY " + DISPLAYNAME;
        try {
            ResultSet rs = executeSelect(query);
            while (rs.next()) {
                list.add(new User(rs.getInt(USER_UID), rs.getString(USERNAME), rs.getString(DISPLAYNAME), rs.getString(ORGANISATION), true));
            }
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
        return list;
    }

    public User getUserInformation(String username) throws StatementNotExecutedException, EntriesException {
        String query = "SELECT " + DISPLAYNAME + "," + USERNAME + "," + ORGANISATION + "," + USER_UID + "," + SEARCHABLE + " FROM "
                + TABLENAME_USER
                + " WHERE " + USERNAME + "='" + username + "'";

        try {
            ResultSet rs = executeSelect(query);
            while (rs.next()) {
                return new User(rs.getInt(USER_UID), rs.getString(USERNAME), rs.getString(DISPLAYNAME), rs.getString(ORGANISATION), rs.getBoolean(SEARCHABLE));
            }
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
        throw new EntriesException("USER_NOT_FOUND");
    }

    public User getUserProfile() throws StatementNotExecutedException, EntriesException {
        String query = "SELECT " + DISPLAYNAME + "," + USERNAME + "," + ORGANISATION + "," + USER_UID + "," + SEARCHABLE
                + " FROM " + TABLENAME_USER
                + " WHERE " + USER_UID + "=" + getCurrentUserId() + ";";
        try {
            xResultSet rs = executeSelect(query);
            if (rs.next()) {
                return new User(rs.getInt(USER_UID), rs.getString(USERNAME), rs.getString(DISPLAYNAME), rs.getString(ORGANISATION), rs.getInt(SEARCHABLE) == 1);
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        throw new EntriesException();

    }

    /**
     * Register a new user. This MUST! only be used if no server is used for this book!!!
     *
     * @param username
     * @param displayName
     * @return
     */
    public Message register(String username, String displayName) throws StatementNotExecutedException {

        String userTakenQuery = "SELECT " + USERNAME + " FROM " + TABLENAME_USER + " WHERE " + USERNAME + "= ?";
        try {
            ResultSet rs = executeSelectPrepared(userTakenQuery, username);
            if (rs.next()) {
                final Message message = new Message(Commands.REQUEST_RESPONSE);
                message.setResult(new Result(false, Result.USERNAME_TAKEN));
                return message;
            }
        } catch (SQLException e) {
            throw new StatementNotExecutedException(userTakenQuery, e);
        }


        // Create User
        DataRow newUser = new DataRow(TABLENAME_USER);
        newUser.add(new DataColumn(username, USERNAME));
        newUser.add(new DataColumn(displayName, DISPLAYNAME));
        insertData(newUser, TABLENAME_USER);


        String userID = "SELECT " + USER_UID + " FROM " + TABLENAME_USER + " WHERE " + USERNAME + "= ?";
        String id;
        try {
            ResultSet rs = executeSelectPrepared(userID, username);
            if (rs.next()) {
                id = rs.getString(USER_UID);
            } else {
                final Message message = new Message(Commands.REQUEST_RESPONSE);
                message.setResult(new Result(false, Result.UNKNOWN_USER));
                return message;
            }
        } catch (SQLException e) {
            throw new StatementNotExecutedException(userID, e);
        }
        return new Message(Commands.REQUEST_RESPONSE);
    }
}
