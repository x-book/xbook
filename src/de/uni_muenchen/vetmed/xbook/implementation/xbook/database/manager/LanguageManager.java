package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.TableManager;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class LanguageManager extends TableManager {

    public static final String TABLENAME_LANGUAGE = "language";
    public static final String LANGUAGE_ID = "id";
    public static final String LANGUAGE_NAME = "Language";
    private boolean isInitialised = false;
    public static int languageID;
    public static final int DEFAULT_LANGUAGE = 0;

    public LanguageManager(Connection conn, String dbName) {
        super(conn, dbName);
        try {
            getCurrentLanguageID();
        } catch (StatementNotExecutedException ex) {
            languageID = 2;
        }
    }

    public int getCurrentLanguageID() throws StatementNotExecutedException {
        if (!isInitialised) {

            String country = Locale.getDefault().getLanguage().toUpperCase();
            String query = "Select " + LANGUAGE_ID + " from " + IStandardColumnTypes.DATABASE_NAME_GENERAL + "." + TABLENAME_LANGUAGE
                    + " WHERE " + LANGUAGE_NAME + "='" + country + "';";
            try {
                ResultSet rs = executeSelect(query);
                if (rs.next()) {

                    languageID = rs.getInt(LANGUAGE_ID);
                    isInitialised = true;
                } else {
                    languageID = 2;
                }
            } catch (SQLException e) {
                languageID = 2;

//                throw new StatementNotExecutedException(query, e);
            }

        }
        return languageID;
    }

    public static String getLanguageQuery(String tableName) {
        String query = "((" + tableName + "." + AbstractCodeTableManager.LANGUAGE + "=" + languageID + " AND " + tableName + "." + DELETED + "='" + DELETED_NO + "') OR "
                + "(" + tableName + "." + AbstractCodeTableManager.LANGUAGE + "=" + DEFAULT_LANGUAGE + " AND NOT EXISTS"
                + " (SELECT * FROM " + tableName + " t1 WHERE t1." + AbstractCodeTableManager.LANGUAGE + "=" + languageID
                + " AND t1." + AbstractCodeTableManager.ID + "= " + tableName + "." + AbstractCodeTableManager.ID
                + " AND t1." + DELETED + "='" + DELETED_NO + "'"
                + ")))";
        return query;
    }
}
