package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.DatabaseConnection;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;

/**
 * @param <T>
 * @param <I>
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class AbstractQueryManager<T extends AbstractProjectManager, I extends AbstractInputUnitManager> {

    public static final int ISOLATION_LEVEL = Connection.TRANSACTION_SERIALIZABLE;
    public static final int ALL_GLOBAL = 1;
    public static final int CHANGES_LOCAL = 2;
    public static final int ALL_LOCAL = 4;
    public static final int CHANGES_GLOBAL = 3;
    public static final int DELETE_ALL = 1;
    public static final int DELETE_DELETED = 2;
    protected String name;
    protected String password;
    protected Connection conn;
    protected String dbName;
    protected UpdateManager updateManager;
    protected UserManager userManager;
    protected DatabaseManager databaseManager;
    protected LanguageManager languageManager;
    protected SchemeManager schemeManager;
    protected DefinitionManager definitionManager;
    protected AbstractCodeTableManager codeTableManager;
    protected T projectManager;
    protected ArrayList<IBaseManager> syncTables;
    protected I inputUnitManager;

    protected HashMap<String, AbstractSynchronisationManager> managerMapping;

    public AbstractQueryManager(String name, String password, String databaseName) throws NotLoggedInException {
        this("localhost", name, password, databaseName);
    }

    public AbstractQueryManager(String hostName, String name, String password, String databaseName) throws NotLoggedInException {

        this.name = name;
        this.password = password;
        managerMapping = new HashMap<>();
        // Check which connections are allowed, for the allowed ones check if
        // the login data works / a connection is possible.
        try {
            // Connect, then disconnect again.
            DatabaseConnection connection = new DatabaseConnection(
                    hostName, name, password, databaseName, false);

            this.conn = connection.getUnderlyingConnection();
            dbName = databaseName;
        } catch (SQLException e) {
            // Login failed, disable local database use.
            e.printStackTrace();

            if (e.getErrorCode() == 1045) //Access denied for user
            {
                throw new NotLoggedInException(NotLoggedInException.ErrorType.AccessDenied);
            } else if (e.getErrorCode() == 1044) {
                throw new NotLoggedInException(NotLoggedInException.ErrorType.DBNotInitialized);
            } else {
                throw new NotLoggedInException();
            }
        }
        syncTables = new ArrayList<>();
        languageManager = new LanguageManager(conn, dbName);
        userManager = createUserManager(conn, dbName);
        databaseManager = new DatabaseManager(conn, dbName);

        switch (XBookConfiguration.getDatabaseMode()) {
            case H2:
                schemeManager = new H2SchemeManager(conn, dbName);
                break;
            default:
                schemeManager = new SchemeManager(conn, dbName);

        }
        definitionManager = new DefinitionManager(conn, dbName);
        updateManager = new UpdateManager(conn, dbName);
    }

    protected UserManager createUserManager(Connection conn, String dbName) {
        return new UserManager(conn, dbName);
    }

    public void changeDefinitionsNew(String tableName, List<DataRow> data) throws StatementNotExecutedException {

        ArrayList<String> key = schemeManager.getPrimaryKeys(tableName);

        for (int i = 0; i < data.size(); i++) {
            try {
//TODO insert or update!
                int updated = definitionManager.updateDefinitionData(data.get(i), key, tableName);
                if (updated < 1) {
                    definitionManager.insertDefinitionData(data.get(i), tableName);
                }
            } catch (StatementNotExecutedException e) {
                definitionManager.insertDefinitionData(data.get(i), tableName);
            }
        }
    }

    public boolean checkSyncIndicator() throws StatementNotExecutedException {
        return updateManager.checkSyncIndicator();
    }

    public void closeConnection() {
        try {
            conn.close();
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
    }

    public abstract AbstractQueryManager createDBUpdateLocalManager() throws StatementNotExecutedException, NotLoggedInException;

    public abstract AbstractQueryManager createNewManager(String userName, String password) throws NotLoggedInException, StatementNotExecutedException;

    /**
     * delete all project entries definitely
     *
     * @param project
     * @param event
     * @throws StatementNotExecutedException
     * @precondition method is executed on local database during synchronization
     */
    public void deleteProjectPermanent(ProjectDataSet project, int event)
            throws StatementNotExecutedException {
        for (IBaseManager sync : syncTables) {
            sync.deletePermanent(project, event);
        }
    }

    public void disableAutoCommit() {
        try {
            conn.setAutoCommit(false);
        } catch (SQLException s) {
            s.printStackTrace();
        }
    }

    public void enableAutoCommit() {
        try {
            conn.commit();
            conn.setAutoCommit(true);
        } catch (SQLException s) {
            s.printStackTrace();
        }
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public String getDbName() {
        return dbName;
    }

    public I getAbstractInputUnitManager() {
        return inputUnitManager;
    }

    public DefinitionManager getDefinitionManager() {
        return definitionManager;
    }

    public LanguageManager getLanguageManager() {
        return languageManager;
    }

    public SchemeManager getSchemeManager() {
        return schemeManager;
    }

    public T getProjectManager() {
        return projectManager;
    }

    public Connection getUnderlyingConnection() {
        return conn;
    }

    public UpdateManager getUpdateManager() {
        return updateManager;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public String getUsername() throws NotLoggedInException {
        if (name == null) {
            throw new NotLoggedInException();
        }
        return name;
    }

    public ArrayList<IBaseManager> getSyncTables() {
        return syncTables;
    }

    public void newProject(DataSetOld project)
            throws StatementNotExecutedException {
        projectManager.save(project);
    }

    public int setIsolationLevel(int isolation) {
        try {
            int level = conn.getTransactionIsolation();
            conn.setTransactionIsolation(isolation);
            return level;
        } catch (SQLException s) {
        }
        return -1;
    }

    /**
     * Updates the Database Number to the given database Number. All old entrys
     * must become new DB Number
     *
     * @param newDatabaseNumber
     * @throws StatementNotExecutedException
     */
    public void updateDatabaseNumber(int newDatabaseNumber) throws StatementNotExecutedException {
        databaseManager.setDatabaseNumber(newDatabaseNumber);
        for (IBaseManager sync : syncTables) {
            sync.updateDatabaseNumber(newDatabaseNumber);
        }
    }

    public AbstractCodeTableManager getCodeTableManager() {
        return codeTableManager;
    }

    public void updatePassword(String name, String password) throws StatementNotExecutedException {
        String query;
        switch (XBookConfiguration.getDatabaseMode()) {
            case MYSQL:
                query = "SET PASSWORD FOR '" + name + "'@'localhost' = PASSWORD('" + password + "');";
                break;
            case H2:
            case NONE:
            case SQLITE:
            default:
                query = "ALTER USER  " + name + " SET PASSWORD '" + password + "';";
        }


        try (Statement statement = conn.createStatement()) {

            statement.execute(query);

        } catch (SQLException ex) {
            try {
                try (Statement statement = conn.createStatement()) {
                    switch (XBookConfiguration.getDatabaseMode()) {
                        case MYSQL:
                            statement.execute("GRANT ALL ON *.* to '" + name + "'@'localhost' IDENTIFIED BY '" + password + "';");
                            break;
                        case H2:
                            statement.execute("CREATE USER IF NOT EXISTS " + name + " PASSWORD  '" + password + "'");
                            statement.execute("GRANT ALTER ANY SCHEMA TO " + name + "");
                            break;
                        case NONE:
                        case SQLITE:
                        default:
                            throw new StatementNotExecutedException("Database not defined");

                    }
                }
            } catch (SQLException ex1) {
                throw new StatementNotExecutedException(ex1.getMessage(), ex1);
            }
        }

    }

    /**
     * Grants the user with the given name and given password all rights for the
     * general table
     *
     * @param name     The name of the user
     * @param password The password of the user
     * @throws StatementNotExecutedException
     */
    public void grantRights(String name, String password) throws StatementNotExecutedException {
        String query;
        switch (XBookConfiguration.getDatabaseMode()) {
            case MYSQL:
                query = "GRANT ALL ON " + IStandardColumnTypes.DATABASE_NAME_GENERAL + ".* to '" + name + "'@'localhost' IDENTIFIED BY '" + password + "';";
                break;
            case H2:
                query = "GRANT ALL ON SCHEMA " + IStandardColumnTypes.DATABASE_NAME_GENERAL + " to " + name + ";";
                break;
            case NONE:
            case SQLITE:
            default:
                throw new StatementNotExecutedException("Database not defined");
        }
        try {
            Statement statement = conn.createStatement();
            try {
                statement.execute(query);
            } finally {
                statement.close();
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

    public abstract ProjectRightManager getProjectRightManager();

    public abstract GroupManager getGroupManager();

    public AbstractSynchronisationManager getManagerForName(String name) {
        return managerMapping.get(name);
    }

    public Set<String> getManagerNamesKeySet() {

        // Alex test stuff:

//        Set<Map.Entry<String, AbstractSynchronisationManager>> a = managerMapping.entrySet();

        return managerMapping.keySet();
    }

    public Set<Map.Entry<String, AbstractSynchronisationManager>> getManagerNamesEntrySet() {
        return managerMapping.entrySet();
    }

    public Set<String> getImportantManagerNames() {
        HashSet<String> importantTables = new HashSet<>();
        for (Map.Entry<String, AbstractSynchronisationManager> stringAbstractSynchronisationManagerEntry : managerMapping.entrySet()) {
            if (stringAbstractSynchronisationManagerEntry.getValue() instanceof AbstractBaseEntryManager) {
                importantTables.add(stringAbstractSynchronisationManagerEntry.getKey());
            }
        }
        return importantTables;
//        return managerMapping.keySet();
    }

}
