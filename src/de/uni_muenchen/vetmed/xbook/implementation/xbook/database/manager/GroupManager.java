package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Group;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rank;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rights;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class GroupManager extends TableManager {

    public static final String TABLENAME_GROUP = "groups";
    public static final String TABLENAME_GROUP_RIGHTS = "group_rights";
    public static final String TABLENAME_GROUP_USER = "group_user";
    public static final String TABLENAME_PROJECT_RIGHT_GROUP = "projectright_group";
    public static final ColumnType GROUP_RIGHTS_READ = new ColumnType(TABLENAME_GROUP_RIGHTS + ".Read",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("RIGHT_READ"));
    public static final ColumnType GROUP_RIGHTS_WRITE = new ColumnType(TABLENAME_GROUP_RIGHTS + ".Write",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("RIGHT_WRITE"));
    public static final ColumnType GROUP_RIGHTS_EDIT_PROJECT = new ColumnType(TABLENAME_GROUP_RIGHTS + ".EditProject",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("RIGHT_EDIT_PROJECT"));
    public static final ColumnType GROUP_RIGHTS_EDIT_GROUP = new ColumnType(TABLENAME_GROUP_RIGHTS + ".ChangeGroup",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("RIGHT_EDIT_GROUP"));
    public static final String GROUP_RIGHTS_GROUP_ID = TABLENAME_GROUP_RIGHTS + ".GroupID";
    public static final String GROUP_RIGHTS_RANK = TABLENAME_GROUP_RIGHTS + ".Rank";
    public static final String GROUP_RIGHTS_RANK_DESCRIPTION = TABLENAME_GROUP_RIGHTS + ".Description";
    public static final String GROUP_USER_GROUP_ID = TABLENAME_GROUP_USER + ".ID";
    public static final String GROUP_USER_RANK = TABLENAME_GROUP_USER + ".Rank";
    public static final String GROUP_USER_USER_ID = TABLENAME_GROUP_USER + ".UserID";
    public static final String GROUP_GROUP_ID = TABLENAME_GROUP + ".ID";
    public static final String GROUP_NAME = TABLENAME_GROUP + ".Name";
    public static final String PROJECT_RIGHT_GROUP_GROUP_ID = TABLENAME_PROJECT_RIGHT_GROUP + ".ID";
    public static final String PROJECT_RIGHT_GROUP_PROJECT_ID = TABLENAME_PROJECT_RIGHT_GROUP + ".ProjectID";
    public static final String PROJECT_RIGHT_GROUP_PROJECT_DATABASE_NUMBER = TABLENAME_PROJECT_RIGHT_GROUP + ".ProjectDatabaseNumber";
    public static final ColumnType PROJECT_RIGHT_GROUP_READ = new ColumnType(TABLENAME_PROJECT_RIGHT_GROUP + ".Read",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("RIGHT_READ"));
    public static final ColumnType PROJECT_RIGHT_GROUP_WRITE = new ColumnType(TABLENAME_PROJECT_RIGHT_GROUP + ".Write",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("RIGHT_WRITE"));
    public static final ColumnType PROJECT_RIGHT_GROUP_EDIT_PROJECT = new ColumnType(TABLENAME_PROJECT_RIGHT_GROUP + ".EditProject",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("RIGHT_EDIT_PROJECT"));
    protected UserManager userManager;
    protected ArrayList<ColumnType> rights = new ArrayList<>();
    protected ArrayList<ColumnType> groupRights = new ArrayList<>();

    public GroupManager(UserManager userManager, Connection connection, String databaseName) {
        super(connection, databaseName);
        this.userManager = userManager;
        rights.add(PROJECT_RIGHT_GROUP_READ);
        rights.add(PROJECT_RIGHT_GROUP_WRITE);
        rights.add(PROJECT_RIGHT_GROUP_EDIT_PROJECT);
        groupRights.add(GROUP_RIGHTS_READ);
        groupRights.add(GROUP_RIGHTS_WRITE);
        groupRights.add(GROUP_RIGHTS_EDIT_PROJECT);
        groupRights.add(GROUP_RIGHTS_EDIT_GROUP);
    }

    /**
     * Returns if the current user has read rights on the given project in any group he currently is a member of
     *
     * @param projectKey The key of the project
     * @return true if the user has read rights, false if not
     * @throws StatementNotExecutedException
     */
    public boolean hasReadRights(Key projectKey) throws StatementNotExecutedException {
        String query = "SELECT " + GROUP_RIGHTS_READ + " AND " + PROJECT_RIGHT_GROUP_READ
                + " FROM " + TABLENAME_GROUP_RIGHTS + "," + TABLENAME_GROUP_USER + "," + TABLENAME_PROJECT_RIGHT_GROUP
                + " WHERE " + GROUP_RIGHTS_GROUP_ID + " = " + PROJECT_RIGHT_GROUP_GROUP_ID
                + " AND " + PROJECT_RIGHT_GROUP_PROJECT_ID + "=" + projectKey.getID()
                + " AND " + PROJECT_RIGHT_GROUP_PROJECT_DATABASE_NUMBER + "=" + projectKey.getDBID()
                + " AND " + GROUP_USER_GROUP_ID + " = " + GROUP_RIGHTS_GROUP_ID
                + " AND " + GROUP_USER_RANK + " = " + GROUP_RIGHTS_RANK
                + " AND " + GROUP_USER_USER_ID + " = " + userManager.getCurrentUserId()
                + " AND " + TABLENAME_GROUP_USER + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + TABLENAME_GROUP_RIGHTS + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + TABLENAME_PROJECT_RIGHT_GROUP + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " + ";";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                if (rs.getBoolean(1)) {
                    return true;
                }
            }
            return false;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

    public boolean hasEditGroupRights(int groupId) throws StatementNotExecutedException {
        String query = "SELECT " + GROUP_RIGHTS_EDIT_GROUP
                + " FROM " + TABLENAME_GROUP_RIGHTS + "," + TABLENAME_GROUP_USER
                + " WHERE " + GROUP_RIGHTS_GROUP_ID + " = " + groupId
                + " AND " + GROUP_USER_GROUP_ID + " = " + GROUP_RIGHTS_GROUP_ID
                + " AND " + GROUP_USER_RANK + " = " + GROUP_RIGHTS_RANK
                + " AND " + GROUP_USER_USER_ID + " = " + userManager.getCurrentUserId()
                + " AND " + TABLENAME_GROUP_USER + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + TABLENAME_GROUP_RIGHTS + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " + ";";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                if (rs.getBoolean(1)) {
                    return true;
                }
            }
            return false;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

    public boolean hasWriteRights(Key projectKey) throws StatementNotExecutedException {
        String query = "SELECT " + GROUP_RIGHTS_WRITE + " AND " + PROJECT_RIGHT_GROUP_WRITE
                + " FROM " + TABLENAME_GROUP_RIGHTS + "," + TABLENAME_GROUP_USER + "," + TABLENAME_PROJECT_RIGHT_GROUP
                + " WHERE " + GROUP_RIGHTS_GROUP_ID + " = " + PROJECT_RIGHT_GROUP_GROUP_ID
                + " AND " + PROJECT_RIGHT_GROUP_PROJECT_ID + "=" + projectKey.getID()
                + " AND " + PROJECT_RIGHT_GROUP_PROJECT_DATABASE_NUMBER + "=" + projectKey.getDBID()
                + " AND " + GROUP_USER_GROUP_ID + " = " + GROUP_RIGHTS_GROUP_ID
                + " AND " + GROUP_USER_RANK + " = " + GROUP_RIGHTS_RANK
                + " AND " + GROUP_USER_USER_ID + " = " + userManager.getCurrentUserId()
                + " AND " + TABLENAME_GROUP_USER + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + TABLENAME_GROUP_RIGHTS + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + TABLENAME_PROJECT_RIGHT_GROUP + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " + ";";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                if (rs.getBoolean(1)) {
                    return true;
                }
            }
            return false;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

    public boolean hasEditProjectRights(Key projectKey) throws StatementNotExecutedException {
        String query = "SELECT " + GROUP_RIGHTS_EDIT_PROJECT + " AND " + PROJECT_RIGHT_GROUP_EDIT_PROJECT
                + " FROM " + TABLENAME_GROUP_RIGHTS + "," + TABLENAME_GROUP_USER + "," + TABLENAME_PROJECT_RIGHT_GROUP
                + " WHERE " + GROUP_RIGHTS_GROUP_ID + " = " + PROJECT_RIGHT_GROUP_GROUP_ID
                + " AND " + PROJECT_RIGHT_GROUP_PROJECT_ID + "=" + projectKey.getID()
                + " AND " + PROJECT_RIGHT_GROUP_PROJECT_DATABASE_NUMBER + "=" + projectKey.getDBID()
                + " AND " + GROUP_USER_GROUP_ID + " = " + GROUP_RIGHTS_GROUP_ID
                + " AND " + GROUP_USER_RANK + " = " + GROUP_RIGHTS_RANK
                + " AND " + GROUP_USER_USER_ID + " = " + userManager.getCurrentUserId()
                + " AND " + TABLENAME_GROUP_USER + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + TABLENAME_GROUP_RIGHTS + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + TABLENAME_PROJECT_RIGHT_GROUP + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " + ";";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                if (rs.getBoolean(1)) {
                    return true;
                }
            }
            return false;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

    public boolean hasRight(Key projectKey, String right) throws StatementNotExecutedException {
        String query = "SELECT " + TABLENAME_GROUP_RIGHTS + "." + right + " AND " + TABLENAME_PROJECT_RIGHT_GROUP + "." + right
                + " FROM " + TABLENAME_GROUP_RIGHTS + "," + TABLENAME_GROUP_USER + "," + TABLENAME_PROJECT_RIGHT_GROUP
                + " WHERE " + GROUP_RIGHTS_GROUP_ID + " = " + PROJECT_RIGHT_GROUP_GROUP_ID
                + " AND " + PROJECT_RIGHT_GROUP_PROJECT_ID + "=" + projectKey.getID()
                + " AND " + PROJECT_RIGHT_GROUP_PROJECT_DATABASE_NUMBER + "=" + projectKey.getDBID()
                + " AND " + GROUP_USER_GROUP_ID + " = " + GROUP_RIGHTS_GROUP_ID
                + " AND " + GROUP_USER_RANK + " = " + GROUP_RIGHTS_RANK
                + " AND " + GROUP_USER_USER_ID + " = " + userManager.getCurrentUserId()
                + " AND " + TABLENAME_GROUP_USER + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + TABLENAME_GROUP_RIGHTS + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + TABLENAME_PROJECT_RIGHT_GROUP + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " + ";";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                if (rs.getBoolean(1)) {
                    return true;
                }
            }
            return false;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

    public ArrayList<Group> getGroupsOfCurrentUser() throws StatementNotExecutedException {
        ArrayList<Group> list = new ArrayList<>();
        String query = "SELECT " + GROUP_NAME + "," + GROUP_GROUP_ID
                + " FROM " + TABLENAME_GROUP + "," + TABLENAME_GROUP_USER
                + " WHERE " + GROUP_USER_USER_ID + "=" + userManager.getCurrentUserId()
                + " AND " + TABLENAME_GROUP_USER + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + TABLENAME_GROUP + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + GROUP_GROUP_ID + "=" + GROUP_USER_GROUP_ID;
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                list.add(new Group(rs.getString(GROUP_NAME), rs.getInt(GROUP_GROUP_ID)));
            }

        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        return list;
    }

    public ArrayList<User> getUsersOfGroup(int groupId) throws StatementNotExecutedException {
        ArrayList<User> list = new ArrayList<>();
        String query = "SELECT " + UserManager.USER_UID + "," + UserManager.USERNAME + "," + UserManager.DISPLAYNAME + "," + UserManager.ORGANISATION + "," + UserManager.SEARCHABLE
                + " FROM " + TABLENAME_GROUP_USER + "," + UserManager.TABLENAME_USER
                + " WHERE " + GROUP_USER_GROUP_ID + "=" + groupId
                + " AND " + UserManager.USER_UID + "=" + GROUP_USER_USER_ID
                + " AND " + TABLENAME_GROUP_USER + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + UserManager.TABLENAME_USER + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' ";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                list.add(new User(rs.getInt(UserManager.USER_UID), rs.getString(UserManager.USERNAME), rs.getString(UserManager.DISPLAYNAME), rs.getString(UserManager.ORGANISATION), rs.getBoolean(UserManager.SEARCHABLE)));
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        return list;
    }

    public ArrayList<Group> getGroups() throws StatementNotExecutedException {
        ArrayList<Group> list = new ArrayList<>();
        String query = "SELECT " + GROUP_NAME + "," + GROUP_GROUP_ID
                + " FROM " + TABLENAME_GROUP
                + " WHERE " + TABLENAME_GROUP + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " +
                " ORDER BY "+GROUP_NAME;
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                list.add(new Group(rs.getString(GROUP_NAME), rs.getInt(GROUP_GROUP_ID)));
            }

        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        return list;
    }

    public ArrayList<Rank> getRanks(int groupID) throws StatementNotExecutedException {
        ArrayList<Rank> list = new ArrayList<>();
        String query = "SELECT " + GROUP_RIGHTS_RANK_DESCRIPTION + "," + GROUP_RIGHTS_RANK
                + " FROM " + TABLENAME_GROUP_RIGHTS
                + " WHERE " + GROUP_RIGHTS_GROUP_ID + "=" + groupID
                + " AND " + TABLENAME_GROUP_RIGHTS + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "'";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                list.add(new Rank(rs.getString(GROUP_RIGHTS_RANK_DESCRIPTION), rs.getInt(GROUP_RIGHTS_RANK)));
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        return list;
    }

    public Rank getUserRank(int groupID, int userID) throws StatementNotExecutedException, EntriesException {

        String query = "SELECT " + GROUP_RIGHTS_RANK_DESCRIPTION + "," + GROUP_RIGHTS_RANK
                + " FROM " + TABLENAME_GROUP_RIGHTS + "," + TABLENAME_GROUP_USER
                + " WHERE " + GROUP_RIGHTS_GROUP_ID + "=" + groupID
                + " AND " + GROUP_RIGHTS_RANK + "=" + GROUP_USER_RANK
                + " AND " + GROUP_USER_USER_ID + "=" + userID
                + " AND " + GROUP_USER_GROUP_ID + "=" + GROUP_RIGHTS_GROUP_ID;
        try {
            xResultSet rs = executeSelect(query);
            if (rs.next()) {
                return new Rank(rs.getString(GROUP_RIGHTS_RANK_DESCRIPTION), rs.getInt(GROUP_RIGHTS_RANK));
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        throw new EntriesException(true);
    }

    public ArrayList<RightsInformation> getGroupRights(Key projectKey) throws StatementNotExecutedException {
        ArrayList<RightsInformation> list = new ArrayList<>();
        String select = "";
        for (ColumnType right : rights) {
            select += right + ", ";
        }

        String query = "SELECT " + select + PROJECT_RIGHT_GROUP_GROUP_ID + "," + GROUP_NAME
                + " FROM " + databaseName + "." + TABLENAME_PROJECT_RIGHT_GROUP + "," + databaseName + "." + TABLENAME_GROUP
                + " WHERE " + PROJECT_RIGHT_GROUP_PROJECT_ID + "=" + projectKey.getID()
                + " AND " + PROJECT_RIGHT_GROUP_PROJECT_DATABASE_NUMBER + "=" + projectKey.getDBID()
                + " AND " + GROUP_GROUP_ID + "=" + PROJECT_RIGHT_GROUP_GROUP_ID
                + " AND " + TABLENAME_PROJECT_RIGHT_GROUP + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " + ";";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                ArrayList<Rights> rightList = new ArrayList<>();
                for (ColumnType right : rights) {
                    rightList.add(new Rights(right, rs.getBoolean(right)));
                }
                list.add(new RightsInformation(rs.getString(GROUP_NAME), rs.getInt(PROJECT_RIGHT_GROUP_GROUP_ID), rightList));
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        return list;
    }

    public ArrayList<Rights> getRankRights(int groupID, int rankID) throws StatementNotExecutedException {
        ArrayList<Rights> list = new ArrayList<>();
        String select = "";
        for (ColumnType right : groupRights) {
            select += right + ", ";
        }

        String query = "SELECT " + select + GROUP_RIGHTS_RANK
                + " FROM " + databaseName + "." + TABLENAME_GROUP_RIGHTS
                + " WHERE " + GROUP_RIGHTS_GROUP_ID + "=" + groupID
                + " AND " + GROUP_RIGHTS_RANK + "=" + rankID
                + " AND " + TABLENAME_GROUP_RIGHTS + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " + ";";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {

                for (ColumnType right : groupRights) {
                    list.add(new Rights(right, rs.getBoolean(right)));
                }

            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        return list;
    }

    /**
     * Returns all users that are in the same Group as the current user, and the group has rights for the given project
     *
     * @param loadedProject
     * @return
     * @throws StatementNotExecutedException
     */
    public ArrayList<User> getGroupUsersForProject(ProjectDataSet loadedProject) throws StatementNotExecutedException {
        UniqueArrayList<User> list = new UniqueArrayList<>();
        String query = "SELECT DISTINCT " + UserManager.USER_UID + "," + UserManager.USERNAME + "," + UserManager.DISPLAYNAME + "," + UserManager.SEARCHABLE
                + " FROM " + TABLENAME_GROUP_USER + "," + UserManager.TABLENAME_USER + "," + TABLENAME_PROJECT_RIGHT_GROUP
                + " WHERE " + GROUP_USER_GROUP_ID + "=" + PROJECT_RIGHT_GROUP_GROUP_ID
                + " AND " + PROJECT_RIGHT_GROUP_PROJECT_ID + "=" + loadedProject.getProjectKey().getID()
                + " AND " + PROJECT_RIGHT_GROUP_PROJECT_DATABASE_NUMBER + "=" + loadedProject.getProjectKey().getDBID()
                + " AND EXISTS (Select * from " + TABLENAME_GROUP_USER + " where " + GROUP_USER_GROUP_ID + "=" + PROJECT_RIGHT_GROUP_GROUP_ID
                + " AND " + GROUP_USER_USER_ID + "=" + userManager.getCurrentUserId() + ")"
                + " AND (" + PROJECT_RIGHT_GROUP_READ + "=1 "
                + " OR " + PROJECT_RIGHT_GROUP_WRITE + "=1 "
                + " OR " + PROJECT_RIGHT_GROUP_EDIT_PROJECT + "=1) "
                + " AND " + UserManager.USER_UID + "=" + GROUP_USER_USER_ID
                + " AND " + TABLENAME_GROUP_USER + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + UserManager.TABLENAME_USER + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + TABLENAME_PROJECT_RIGHT_GROUP + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' ";
        if (XBookConfiguration.DISPLAY_SOUTS) {
            System.out.println(query);
        }
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                list.add(new User(rs.getInt(UserManager.USER_UID), rs.getString(UserManager.USERNAME), rs.getString(UserManager.DISPLAYNAME), rs.getString(UserManager.ORGANISATION), rs.getBoolean(UserManager.SEARCHABLE)));
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }

        return list;
    }

    public Group getGroupInformation(String groupname) throws StatementNotExecutedException, EntriesException {
        String query = "SELECT " + GROUP_NAME + "," + GROUP_GROUP_ID
                + " FROM " + TABLENAME_GROUP
                + " WHERE " + TABLENAME_GROUP + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + GROUP_NAME + "='" + groupname + "' ";
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                return new Group(rs.getString(GROUP_NAME), rs.getInt(GROUP_GROUP_ID));
            }

        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        throw new EntriesException();
    }

    public HashMap<Key, ArrayList<String>> getGroupsWithProjectRights(Collection<Key> thisProject) throws StatementNotExecutedException {
        HashMap<Key, ArrayList<String>> hashMap = new HashMap<>();
        String projectQuery = "(";
        for (Key key : thisProject) {
            projectQuery += "(" + PROJECT_RIGHT_GROUP_PROJECT_ID + "=" + key.getID()
                    + " AND " + PROJECT_RIGHT_GROUP_PROJECT_DATABASE_NUMBER + "=" + key.getDBID() + ") OR ";

        }
        projectQuery = StringHelper.cutString(projectQuery, " OR ") + ")";

        StringBuilder sb = new StringBuilder("SELECT ").append(GROUP_NAME).append("," + PROJECT_RIGHT_GROUP_PROJECT_ID + "," + PROJECT_RIGHT_GROUP_PROJECT_DATABASE_NUMBER).append(" FROM ").append(TABLENAME_GROUP).append(",").append(TABLENAME_PROJECT_RIGHT_GROUP)
                .append(" WHERE ").append(GROUP_GROUP_ID).append("=").append(PROJECT_RIGHT_GROUP_GROUP_ID)
                .append(" AND " + projectQuery)
                .append(" AND ").append(PROJECT_RIGHT_GROUP_READ).append("=1");
        try {
            xResultSet rs = executeSelect(sb.toString());
            while (rs.next()) {
                Key key = new Key(rs.getInt(PROJECT_RIGHT_GROUP_PROJECT_ID), rs.getInt(PROJECT_RIGHT_GROUP_PROJECT_DATABASE_NUMBER));

                ArrayList<String> list = hashMap.get(key);
                if (list == null) {
                    list = new ArrayList<>();
                    hashMap.put(key, list);
                }

                list.add(rs.getString(GROUP_NAME));
            }
        } catch (SQLException e) {
            throw new StatementNotExecutedException(sb.toString(), e);
        }

        return hashMap;
    }

    public ArrayList<ColumnType> getRights() {
        return rights;
    }

    public ArrayList<ColumnType> getGroupRights() {
        return groupRights;
    }
}
