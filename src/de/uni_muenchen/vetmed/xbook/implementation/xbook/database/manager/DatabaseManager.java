package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;

/**
 * 
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DatabaseManager extends TableManager {

    protected Integer dbNumer = null;

    public DatabaseManager(Connection connection, String databaseName) {
        super(connection, databaseName);
    }

    // get DB-Version from DB
    public String getGeneralDatabaseVersion() throws StatementNotExecutedException {
        String query = String.format("SELECT " + UpdateManager.VERSION_DBVERSION + " FROM " + UpdateManager.TABLENAME_VERSION_GENERAL);
        try {
            ResultSet rs = executeSelect(query);
            if (rs.next()) {
                return rs.getString(1);
            }
            return "";
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }
    // get DB-Version from DB

    public String getDatabaseVersion() throws StatementNotExecutedException {
        String query = String.format("SELECT " + UpdateManager.VERSION_DBVERSION + " FROM %s." + UpdateManager.TABLENAME_VERSION, databaseName);
        try {
            ResultSet rs = executeSelect(query);
            if (rs.next()) {
                return rs.getString(1);
            }
            return "";
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    public void setDatabaseVersion(String version) throws StatementNotExecutedException {
//        ArrayList<DataColumn> data = new ArrayList<>();
        DataRow data = new DataRow(databaseName + "." + UpdateManager.TABLENAME_VERSION);
        data.add(new DataColumn(version, UpdateManager.VERSION_DBVERSION));
        updateData(data, databaseName + "." + UpdateManager.TABLENAME_VERSION);
    }

    /**
     * get number of local database
     *
     * @return
     * @throws StatementNotExecutedException
     */
    public int getDatabaseNumber() throws StatementNotExecutedException {
        if (dbNumer != null) {
            return dbNumer;
        }
        String query = "SELECT " + UpdateManager.VERSION_DATABASENUMBER + " FROM "
                + UpdateManager.TABLENAME_VERSION_GENERAL + ";";
        System.out.println("QUERY = " + query);
        try {
            ResultSet rs = executeSelect(query);
            if (rs.next()) {
                return dbNumer = rs.getInt(1);
            }
            return -1;
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    //
    /**
     * lock given tables of given database
     *
     * @param tables
     * @throws StatementNotExecutedException
     */
    public void lockDatabase(ArrayList<String> tables)
            throws StatementNotExecutedException {
        String query = "LOCK TABLES ";
        for (int i = 0; i < tables.size(); i++) {
            query = query +  tables.get(i) + " WRITE";
            if (i < tables.size() - 1) {
                query = query + ", ";
            }
        }
        try {
            executeQuery(query);
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    /**
     * unlock database
     *
     * @throws StatementNotExecutedException
     */
    public void unlockTables() throws StatementNotExecutedException {
        String query = "UNLOCK TABLES;";
        try {
            executeQuery(query);
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    public void setDatabaseNumber(int number) throws StatementNotExecutedException {

//        ArrayList<DataColumn> data = new ArrayList<>();
        DataRow data = new DataRow( UpdateManager.TABLENAME_VERSION_GENERAL);
        data.add(new DataColumn(number, UpdateManager.VERSION_DATABASENUMBER));
        updateData(data, UpdateManager.TABLENAME_VERSION_GENERAL);
    }

    public void updateVersionGeneral(int databaseNumner, String databaseVersion) throws StatementNotExecutedException {
//        ArrayList<DataColumn> data = new ArrayList<>();
        DataRow data = new DataRow( UpdateManager.TABLENAME_VERSION_GENERAL);
        data.add(new DataColumn(databaseNumner, UpdateManager.VERSION_DATABASENUMBER));
        data.add(new DataColumn(databaseVersion, UpdateManager.VERSION_DBVERSION));
        int updateCount = updateData(data, UpdateManager.TABLENAME_VERSION_GENERAL);

        if (updateCount < 1) {
            insertData(data, UpdateManager.TABLENAME_VERSION_GENERAL);
        }
    }

    public void updateVersion(int databaseNumner, String databaseVersion) throws StatementNotExecutedException {
//        ArrayList<DataColumn> data = new ArrayList<>();
        DataRow data = new DataRow(databaseName + "." + UpdateManager.TABLENAME_VERSION);
        data.add(new DataColumn(databaseVersion, UpdateManager.VERSION_DBVERSION));

        int updateCount = updateData(data, databaseName + "." + UpdateManager.TABLENAME_VERSION);

        if (updateCount < 1) {
            insertData(data, databaseName + "." + UpdateManager.TABLENAME_VERSION);
        }
    }
}