package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLine;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLineGroup;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.SolveConflictLineUpdateOrDelete;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 17.06.2014.
 */
public abstract class AbstractBaseProjectEntryManager extends AbstractExtendedProjectEntryManager implements IBaseManager {

    AbstractExtendedProjectEntryManager[] managers;

    public AbstractBaseProjectEntryManager(String tableName, String localisedTableName, int dbNumber, Connection connection, String databaseName, AbstractExtendedProjectEntryManager... manager) {
        super(tableName, localisedTableName, dbNumber, connection, databaseName);

        managers = manager;
    }
//should not be used

    @Override
    protected Iterable<? extends ColumnType> getConflictableColumns() {
        return null;
    }

    @Override
    public void deleteValue(Key entryKey, Key projectKey) throws StatementNotExecutedException {
        //do nothing
    }

    @Override
    public void updateDatabaseNumber(int newNumber) throws StatementNotExecutedException {

        dbId = newNumber;
        ArrayList<DataColumn> data = new ArrayList<>();
        data.add(new DataColumn(newNumber, PROJECT_DATABASE_ID));
        ArrayList<DataColumn> key = new ArrayList<>();
        key.add(new DataColumn(-1, PROJECT_DATABASE_ID));
        updateDataWithKey(data, key);

    }

    @Override
    public void setConflicted(DataSetOld dataSet) throws StatementNotExecutedException {
        super.setConflicted(dataSet);
        for (AbstractExtendedProjectEntryManager table : managers) {
            if (table instanceof IBaseManager) {
                continue;
            }
            table.setConflicted(dataSet);
        }
    }

    @Override
    public void updateUnsyncedEntries(ProjectDataSet project) throws StatementNotExecutedException {
        super.updateUnsyncedEntries(project);
        for (AbstractSynchronisationManager table : managers) {
            if (table instanceof IBaseManager) {
                continue;
            }
            table.updateUnsyncedEntries(project);
        }
    }

    @Override
    public void updateEntries(DataSetOld data) throws StatementNotExecutedException {
        super.updateEntries(data);
        for (AbstractExtendedProjectEntryManager table : managers) {
            if (table instanceof IBaseManager) {
                continue;
            }
            table.updateEntries(data);
        }
    }

    @Override
    public void setSynchronised(DataSetOld data, DataSetOld returnData) throws StatementNotExecutedException {
        super.setSynchronised(data, returnData);
        for (AbstractExtendedProjectEntryManager table : managers) {
            if (table instanceof IBaseManager) {
                continue;
            }
            table.setSynchronised(data, returnData);
        }
    }

    @Override
    public void solveConflict(DataSetOld data) throws StatementNotExecutedException {
        super.solveConflict(data);
        for (AbstractExtendedProjectEntryManager table : managers) {
            table.solveConflict(data);
        }
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public DataSetOld getNextUncomittedEntry(DataSetOld dataSet) throws NotLoggedInException, StatementNotExecutedException, NoRightException, IOException {
        DataSetOld result = new DataSetOld(dataSet.getProjectKey(), databaseName,tableName);
        result.addDataTable(getNextUncommitedEntry(dataSet.getProjectKey()));
        if (result.hasEntries()) {
            for (AbstractExtendedProjectEntryManager abstractExtendedProjectEntryManager : managers) {
                result.addDataTable(abstractExtendedProjectEntryManager.getNextUncommitedEntry(dataSet.getProjectKey()));
            }
        }
        return result;
    }

    @Override
    public DataTableOld getNextUncommitedEntry(Key projectKey) throws StatementNotExecutedException, IOException {
        String condition = " WHERE " + PROJECT_ID + "='" + projectKey.getID()
                + "' AND " + PROJECT_DATABASE_ID + "=" + projectKey.getDBID()
                + " AND " + MESSAGE_NUMBER + " = " + 1
                + " LIMIT " + 1;
        return getEntry(condition);
    }

    /**
     * Returns all Conflicts in this manager, for the given project with the
     * given table schema.
     *
     * @param project
     * @return
     * @throws StatementNotExecutedException
     */
    @Override
    public ArrayList<DataSetOld> getConflicts(ProjectDataSet project) throws StatementNotExecutedException {
        ArrayList<DataSetOld> tables = new ArrayList<>();
        String query = "SELECT ";
        for (int i = 0; i < dataColumns.size(); i++) {
            query += dataColumns.get(i);
            if (i < dataColumns.size() - 1) {
                query += ", ";
            } else {
                query += " FROM ";
            }
        }
        query += tableName + " WHERE " + PROJECT_ID + "='" + project.getProjectKey().getID()
                + "' AND " + PROJECT_DATABASE_ID + "=" + project.getProjectKey().getDBID()
                + " AND " + MESSAGE_NUMBER + " = " + -1;
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                DataSetOld set = new DataSetOld(project.getProjectKey(), databaseName,tableName);
                DataTableOld dataTable = new DataTableOld(tableName);
                DataRow hash = new DataRow(tableName);
                for (ColumnType column : dataColumns) {
                    hash.put(column, rs.getString(column));
                }
                DataRow primRow = new DataRow(tableName);
                for (String key : primaryColumns) {
                    primRow.put(ColumnHelper.removeDatabaseName(key), rs.getString(key));
                }
                for (AbstractExtendedProjectEntryManager manager : managers) {
                    set.addDataTable(manager.getConflicts(project.getProjectKey(), primRow));
                }
                dataTable.add(hash);
                set.addDataTable(dataTable);
                tables.add(set);
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        return tables;
    }

    public ArrayList<DataSetOld> getEntry(ProjectDataSet project) throws StatementNotExecutedException {
        ArrayList<DataSetOld> tables = new ArrayList<>();
        String query = "SELECT ";
        for (int i = 0; i < dataColumns.size(); i++) {
            query += dataColumns.get(i);
            if (i < dataColumns.size() - 1) {
                query += ", ";
            } else {
                query += " FROM ";
            }
        }
        query += tableName + " WHERE " + PROJECT_ID + "='" + project.getProjectKey().getID()
                + "' AND " + PROJECT_DATABASE_ID + "=" + project.getProjectKey().getDBID()
        ;
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                DataSetOld set = new DataSetOld(project.getProjectKey(), databaseName,tableName);
                DataTableOld dataTable = new DataTableOld(tableName);
                DataRow hash = new DataRow(tableName);
                for (ColumnType column : dataColumns) {
                    hash.put(column, rs.getString(column));
                }
                DataRow primRow = new DataRow(tableName);
                for (String key : primaryColumns) {
                    primRow.put(ColumnHelper.removeDatabaseName(key), rs.getString(key));
                }
                for (AbstractExtendedProjectEntryManager manager : managers) {
                    set.addDataTable(manager.getConflicts(project.getProjectKey(), primRow));
                }
                dataTable.add(hash);
                set.addDataTable(dataTable);
                tables.add(set);
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
        return tables;
    }

    @Override
    public ArrayList<String> getPrimaryColumnsForTable(String tableName) {
        if (this.tableName.equals(tableName)) {
            return primaryColumns;
        }
        for (AbstractExtendedProjectEntryManager manager : managers) {
            if (manager.tableName.equals(tableName)) {
                return manager.primaryColumns;
            }
        }
        return null;
    }

    @Override
    public AbstractExtendedProjectEntryManager[] getManagers() {
        return managers;
    }

    @Override
    public SolveConflictLineGroup getLinesForEntry(DataRow localAll, DataRow globalRows) throws StatementNotExecutedException, EntriesException {
        String lastSync = "0";
        String deletedGlob = "Y";
        SolveConflictLineGroup lines = new SolveConflictLineGroup(tableName);
        HashMap<Integer, SolveConflictLine> groups = new HashMap<>();
        List<ColumnType> specialColumns = getSepcialColumnsForConflict();

        ArrayList<String> allPrimaries = getPrimaryColumns();
        for (DataColumn local : localAll.iterator()) {//go through local data
            String id = ColumnHelper.removeDatabaseName(local.getColumnName());
            boolean foundServer = false;
            for (DataColumn global : globalRows.iterator()) {//go through server data
                if (ColumnHelper.removeDatabaseName(global.getColumnName()).equals(STATUS.getColumnName())) {//remember server status
                    lastSync = global.getValue();
                } else if (ColumnHelper.removeDatabaseName(global.getColumnName()).equals(DELETED.getColumnName())) {//remember if data is deleted on server
                    deletedGlob = global.getValue();
                }
                if (id.equals(ColumnHelper.removeDatabaseName(global.getColumnName()))) {//if entrys equal...
                    foundServer = true;
                    for (ColumnType column : dataColumns) {//go through all data columns
                        if (specialColumns.contains(column)) {
                            continue;
                        }
                        if (id.equals(ColumnHelper.removeDatabaseName(column.getColumnName()))) {
                            String left = getStringRepresentation(column, local.getValue());//get display for the entry
                            String right = getStringRepresentation(column, global.getValue());
                            HashMap<ColumnType, String> localValue = new HashMap<>();
                            localValue.put(column, local.getValue());
                            HashMap<ColumnType, String> globalValue = new HashMap<>();
                            globalValue.put(column, global.getValue());
                            boolean inGroup = false;
                            for (int i = 0; i < columnGroup.size(); i++) {//go through all groups
                                if (columnGroup.get(i).contains(column)) {//if column is in group
                                    inGroup = true;
                                    SolveConflictLine line = groups.get(i);
                                    if (line == null) {//create new group if there is none

                                        line = new SolveConflictLine(localValue, left, globalValue, right, column.getDisplayName(), tableName);
                                        groups.put(i, line);
                                        lines.add(line);
                                    } else {

                                        line.addLine(new SolveConflictLine(localValue, left, globalValue, right, column.getDisplayName(), tableName));

                                    }
                                    break;
                                }
                            }
                            if (!inGroup) {//not in group just add
                                lines.add(new SolveConflictLine(localValue, left, globalValue, right, column.getDisplayName(), tableName));
                            }
                            break;
                        }
                    }
                    break;
                }
            }
            if (globalRows.isEmpty() || !foundServer) {//if no server data is available for this entry
                for (ColumnType column : dataColumns) {
                    if (specialColumns.contains(column)) {
                        continue;
                    }
                    if (column.equals(ID) || column.equals(DATABASE_ID) || column.equals(PROJECT_ID) || column.equals(PROJECT_DATABASE_ID)) {
                        continue;
                    }
                    if (id.equals(ColumnHelper.removeDatabaseName(column.getColumnName()))) {
                        String left = getStringRepresentation(column, local.getValue());
                        HashMap<ColumnType, String> localValue = new HashMap<>();
                        localValue.put(column, local.getValue());
                        HashMap<ColumnType, String> globalValue = new HashMap<>();

                        SolveConflictLine newLine;
                        if (allPrimaries.contains(id)||allPrimaries.contains(column.getColumnName())) {
                            newLine = new SolveConflictLineUpdateOrDelete(localValue, left, globalValue, Loc.get("DELETE"), column.getDisplayName(), tableName);
                        } else {

                            globalValue.put(column, column.getDefaultValue());
                            newLine = new SolveConflictLine(localValue, left, globalValue, "", column.getDisplayName(), tableName);
                        }

                        boolean inGroup = false;
                        for (int i = 0; i < columnGroup.size(); i++) {
                            if (columnGroup.get(i).contains(column)) {
                                inGroup = true;
                                SolveConflictLine line = groups.get(i);
                                if (line == null) {
                                    line = newLine;
                                    groups.put(i, line);
                                    lines.add(newLine);
                                } else {
                                    line.addLine(newLine);

                                }
                                break;
                            }
                        }
                        if (!inGroup) {
                            lines.add(newLine);
                        }
                        break;
                    }
                }
            }
        }
        if (localAll.isEmpty()) {//only server data is available
            for (DataColumn server : globalRows.iterator()) {
                String id = ColumnHelper.removeDatabaseName(server.getColumnName());
                if (id.equals(STATUS.getColumnName())) {
                    lastSync = server.getValue();
                } else if (id.equals(DELETED.getColumnName())) {
                    deletedGlob = server.getValue();
                }
                for (ColumnType column : dataColumns) {
                    if (specialColumns.contains(column)) {
                        continue;
                    }
                    if (column.equals(ID) || column.equals(DATABASE_ID) || column.equals(PROJECT_ID) || column.equals(PROJECT_DATABASE_ID)) {
                        continue;
                    }

                    if (id.equals(ColumnHelper.removeDatabaseName(column.getColumnName()))) {
                        String right = getStringRepresentation(column, server.getValue());
                        HashMap<ColumnType, String> localValue = new HashMap<>();
                        HashMap<ColumnType, String> globalValue = new HashMap<>();
                        globalValue.put(column, server.getValue());
                        SolveConflictLine newLine;
                        if (allPrimaries.contains(id)||allPrimaries.contains(column.getColumnName())) {
                            newLine = new SolveConflictLineUpdateOrDelete(localValue, Loc.get("DELETE"), globalValue, right, column.getDisplayName(), tableName);
                        } else {

                            localValue.put(column, column.getDefaultValue());
                            newLine = new SolveConflictLine(localValue, "", globalValue, right, column.getDisplayName(), tableName);
                        }
                        boolean inGroup = false;
                        for (int i = 0; i < columnGroup.size(); i++) {
                            if (columnGroup.get(i).contains(column)) {
                                inGroup = true;
                                SolveConflictLine line = groups.get(i);
                                if (line == null) {
                                    line = newLine;
                                    groups.put(i, line);
                                    lines.add(newLine);
                                } else {
                                    line.addLine(newLine);

                                }
                                break;
                            }
                        }
                        if (!inGroup) {
                            lines.add(newLine);
                        }
                        break;
                    }

                }
            }
        }
        getSpecialLinesForEntry(localAll, globalRows, lines);
        lines.setLastSync(lastSync);
        lines.setDeletedGlobal(deletedGlob.equals(IStandardColumnTypes.DELETED_YES));
        return lines;
    }

    public abstract String toString(DataSetOld dataSet) throws StatementNotExecutedException;
}
