package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DefinitionManager extends TableManager {

    public DefinitionManager(Connection connection, String databaseName) {
        super(connection, databaseName);
    }

//    public int updateDefinitionData(ArrayList<DataColumn> data, ArrayList<String> key, String tableName)
//            throws StatementNotExecutedException {
//        return insertOrUpdate(data, tableName, key);
//    }

    public int updateDefinitionData(DataRow data, ArrayList<String> key, String tableName)
            throws StatementNotExecutedException {
        return updateData(data, tableName, key);
    }

//    public void insertDefinitionData(ArrayList<DataColumn> data,
//            String tableName) throws StatementNotExecutedException {
//        insertData(data, tableName);
//    }

    public void insertDefinitionData(DataRow data,
            String tableName) throws StatementNotExecutedException {
        insertData(data, tableName);
    }

    /**
     * definition table records definitely deleted from database
     *
     * @precondition method is executed on local database during synchronization
     * @param tableName
     * @throws StatementNotExecutedException
     */
    public void deleteDefinitionPermanent(String tableName)
            throws StatementNotExecutedException {
        String query = "DELETE FROM " + tableName + " WHERE " + DELETED + "='" + IStandardColumnTypes.DELETED_YES + "'";
        try {
            executeQuery(query);
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    public void truncateDefinition(String tableName) throws StatementNotExecutedException {
        String query = "TRUNCATE TABLE " + tableName;
        try {
            executeQuery(query);
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

}
