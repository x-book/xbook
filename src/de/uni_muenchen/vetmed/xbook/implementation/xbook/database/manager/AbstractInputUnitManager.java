package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class AbstractInputUnitManager extends AbstractBaseEntryManager implements IStandardInputUnitColumnTypes {

    protected LanguageManager language;

    public AbstractInputUnitManager(String localisedTableName, int dbNumber, Connection connection, String databaseName, UserManager user, LanguageManager language, AbstractSynchronisationManager... manager) {
        super(TABLENAME_INPUT_UNIT, localisedTableName, dbNumber, connection, databaseName, user, manager);

        this.language = language;
        dataColumns.add(USER_ID_INPUTUNIT);
    }

    /**
     * Returns the last synchronisation time for the given project
     *
     * @param projectKey The Key of the project
     * @return The highest synchronisation time or 0 if no entry exists
     * @throws StatementNotExecutedException
     */
    @Override
    public String getLastSynchronisation(Key projectKey) throws StatementNotExecutedException {
        String query = "SELECT MAX(" + STATUS + ") FROM " + tableName
                + " WHERE " + PROJECT_ID_INPUTUNIT + "=" + projectKey.getID()
                + " AND " + PROJECT_DATABASE_ID + "=" + projectKey.getDBID();
        try {
            ResultSet rs = executeSelect(query);

            if (rs.next()) {
                String max = rs.getString(1);
                return (max != null) ? max : "0";
            }
            return "0";
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }



    @Override
    protected DataColumn getUserInformation() throws StatementNotExecutedException {
        return new DataColumn(user.getCurrentUserId(), USER_ID_INPUTUNIT);
    }

}
