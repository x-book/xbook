package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;


import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IDefaultSectionAssignments;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.manager.ISynchronisationManager;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType.ExportType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnTypeList;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTableOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryKey;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportResult;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.SearchEntryInfo;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.event.EntryEvent;
import de.uni_muenchen.vetmed.xbook.api.event.EntryListener;
import de.uni_muenchen.vetmed.xbook.api.event.EventRegistry;
import de.uni_muenchen.vetmed.xbook.api.event.ValueEventSender;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLine;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLineGroup;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.DataManagementHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class AbstractSynchronisationManager extends TableManager implements
    ValueEventSender, IStandardColumnTypes, ISynchronisationManager {

  private static final Log LOGGER = LogFactory.getLog(AbstractSynchronisationManager.class);
  private final String FLAG = "■ ■ ■";
  private ArrayList<EntryListener> listenerList = new ArrayList<>();
  public String tableName;
  public String localisedTableName;
  public int dbId;
  protected ColumnTypeList dataColumns;
  protected ArrayList<String> primaryColumns;
  protected ArrayList<ArrayList<ColumnType>> columnGroup;

  protected ColumnType image_column;

  public AbstractSynchronisationManager(String tableName, String localisedTableName, int dbId,
      Connection connection,
      String databaseName) {
    super(connection, databaseName);
    this.tableName = tableName;
    this.localisedTableName = localisedTableName;
    this.dbId = dbId;
    columnGroup = new ArrayList<>();
    dataColumns = new ColumnTypeList(tableName);

    ColumnType projectID = new ColumnType(PROJECT_ID, tableName)
        .setSectionProperty(IDefaultSectionAssignments.SECTION_SYSTEM);

    projectID.setHiddenInListing(true);
    dataColumns.add(projectID);
    ColumnType projectDatabaseID = new ColumnType(PROJECT_DATABASE_ID, tableName)
        .setSectionProperty(IDefaultSectionAssignments.SECTION_SYSTEM)
        .setHiddenInListing(true);
    dataColumns.add(projectDatabaseID);

    dataColumns.add(new ColumnType(MESSAGE_NUMBER, tableName));
    dataColumns.add(new ColumnType(STATUS, tableName));
    dataColumns.add(new ColumnType(DELETED, tableName));
    primaryColumns = new ArrayList<>();
    primaryColumns.add("" + PROJECT_ID);
    primaryColumns.add("" + PROJECT_DATABASE_ID);
  }

  @Override
  public ColumnTypeList getDataColumns() {
    return dataColumns;
  }

  @Override
  public String getTableName() {
    return tableName;
  }

  /**
   * Returns the ExportData for the given Project with the given amount and given offset and given
   * Keys
   *
   * @param projects The project to retrieve the Data from
   * @param count The amount of data to load
   * @param offset The offset of the Data to retrieve
   * @param keys The Keys of the entries to retrieve the data from
   * @return A Hashmap of ExportData mapped to EntryKey of the Entry
   */
  public ExportResult getEntryData(ExportResult result, List<Key> projects, List<ColumnType> list,
      ExportType exportType,
      boolean conflict, int count, int offset, List<Key> keys,
      Map<ColumnType, SearchEntryInfo> filter,
      boolean hideNonDefaultFields) throws StatementNotExecutedException {

    String keyQuery = "";
    if (keys != null && !keys.isEmpty()) {
      keyQuery = " AND (";
      for (Key key : keys) {
        keyQuery += "("
            + tableName + "." + ID + "=" + key.getID() + " AND "
            + tableName + "." + DATABASE_ID + "= " + key.getDBID() + ") OR";
      }
      if (keyQuery.endsWith("OR")) {
        keyQuery = keyQuery.substring(0, keyQuery.length() - 2) + ")";
      }

    } else if (keys != null) {
      return result;
    }

    String limit = "";
    if (count > 0) {
      limit = " ORDER BY " + ID + "," + DATABASE_ID + " LIMIT " + count + " OFFSET " + offset;
    }

    StringBuilder select = new StringBuilder();
    UniqueArrayList<ColumnType> rowNames = new UniqueArrayList<>();
    UniqueArrayList<String> fromTables = new UniqueArrayList<>();
    //only used for filter
    AtomicBoolean wasFilterUsed = new AtomicBoolean(false);
    StringBuilder where = new StringBuilder();
    UniqueArrayList<ColumnType> hierarchicTables = new UniqueArrayList<>();
    if (conflict) {
      where = new StringBuilder(" AND " + MESSAGE_NUMBER + "=" + -1);
    }
    for (ColumnType type : dataColumns) {
      doStuffWithColumnType(type, rowNames, select, where, fromTables, exportType, list, filter,
          wasFilterUsed,
          hierarchicTables);
    }

    if (select.length() == 0) {
      return result;
    }
    String from = "";
    for (String string : fromTables) {
      from += string + ",";
    }
    String projectQuery = "";
    if (projects != null && !projects.isEmpty()) {
      for (Key project : projects) {
        projectQuery += "(" + tableName + "." + PROJECT_ID + "=" + project.getID()
            + " AND " + tableName + "." + PROJECT_DATABASE_ID + "=" + project.getDBID()
            + ")" + " OR ";
      }
      projectQuery = StringHelper.cutString(projectQuery, " OR ") + " AND ";


    }
    String query = "SELECT " + select.substring(0, select.length() - 1)
        + " FROM " + from + tableName
        + " WHERE " + projectQuery + tableName + "." + DELETED + "='"
        + IStandardColumnTypes.DELETED_NO + "' "
        + where + keyQuery + limit;
    try {
      if (XBookConfiguration.DISPLAY_SOUTS) {
        LogFactory.getLog(AbstractSynchronisationManager.class).info(query);
      }
      xResultSet rs = executeSelect(query);

      //if there are no entries in the database still add the names for the columns
      if (!rs.isBeforeFirst()) {
        getColumnHeaders(result, rowNames, exportType, hideNonDefaultFields);
      }
      ExportResult temp = result;
      if (filter != null && !filter.isEmpty() && wasFilterUsed.get()) {
        result = new ExportResult();
      }
      while (rs.next()) {
        getRow(rs, result, rowNames, exportType, hideNonDefaultFields);
      }
      if (filter != null && !filter.isEmpty() && wasFilterUsed.get()) {
        result.filter(filter);
        result = result.concat(temp);
      }
      return result;
    } catch (SQLException ex) {
      LogFactory.getLog(AbstractSynchronisationManager.class).error(ex, ex);

      throw new StatementNotExecutedException(query, ex);
    }

  }


  private void getColumnHeaders(ExportResult result, UniqueArrayList<ColumnType> columnNames,
      ExportType exportType,
      boolean hideNonDefaultFields) {
    for (ColumnType columnName : columnNames) {
      if (!hideNonDefaultFields || !columnName.isNonDefaultField()) {
        if (exportType != ExportType.CROSS_LINKED || columnName.getExportType() == ExportType.ALL) {
          result.addHeadline(columnName);
        }

      }
      for (ColumnType additional : columnName.getAdditionalColumns()) {
        if (additional == null) {
          continue;
        }
        if (additional.getExportType() == exportType
            || additional.getExportType() == ExportType.ALL) {
          if (exportType != ExportType.CROSS_LINKED
              || columnName.getExportType() == ExportType.ALL) {
            result.addHeadline(additional);
          }
        }
      }
    }
  }

  protected void getRow(xResultSet rs, ExportResult data, UniqueArrayList<ColumnType> rowNames,
      ExportType exportType, boolean forListing) throws SQLException {
    try {
      Key project = new Key(rs.getInt(PROJECT_ID), rs.getInt(PROJECT_DATABASE_ID));
      EntryKey entryKey = new EntryKey(null, project);

      ExportRow exportData = data.getExportRow(entryKey);

      for (ColumnType columnName : rowNames) {
        getExportDataFromQuery(exportData, columnName, rs, exportType, forListing);
      }
    } catch (SQLException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }

  /**
   * Retrieves information from the Resultset for the given ColumnType an stores it in the
   * ExportData file.
   *
   * @param exportData The data to store the Entry to
   * @param columnName The Column to retrieve the information from
   * @param rs The Resulset where the sql REsult ist stored
   */
  protected void getExportDataFromQuery(ExportRow exportData, ColumnType columnName, ResultSet rs,
      ExportType exportType, boolean forListing)
      throws SQLException {
    try {
      if (forListing && columnName.isNonDefaultField()) {
        // don't display the column
      } else {
        ExportColumn exportColumn = exportData.getExportColumn(columnName);
        String result = getResultFromColumnType(columnName, rs);
        if ((!forListing && columnName.isShortenedInExport()) || (forListing && columnName
            .isShortenedInListing())) {
          if (!result.isEmpty()) {
            result = FLAG;
          }
        }
        exportColumn.addData(result);
      }
      for (ColumnType additional : columnName.getAdditionalColumns()) {
        if (additional == null) {
          continue;
        }
        if (additional.getExportType() == exportType || exportType == ExportType.ALL
            || additional.getExportType() == ExportType.ALL) {
          getExportDataFromQuery(exportData, additional, rs, exportType, forListing);
        }
      }
    } catch (SQLException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }

  /**
   * records definitely deleted from database for given project
   *
   * @param event - QueryManager.DELETE_DELETED, QueryManager.DELETE_ALL
   * @precondition method is executed on local database during synchronization
   */
  @Override
  public void deletePermanent(ProjectDataSet project, int event)
      throws StatementNotExecutedException {
    String condition = "";
    if (event == AbstractQueryManager.DELETE_DELETED) {
      condition = " AND " + DELETED + "='" + IStandardColumnTypes.DELETED_YES + "' AND "
          + MESSAGE_NUMBER + "=" + IStandardColumnTypes.SYNCHRONIZED;
    }
    String query = "DELETE  FROM " + databaseName + "." + tableName
        + " WHERE " + PROJECT_ID + "='" + project.getProjectKey().getID()
        + "' AND " + PROJECT_DATABASE_ID + "=" + project.getProjectKey().getDBID()
        + condition + ";";
    try {
      executeQuery(query);
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }

  }

  /**
   * Deletes the entry permanently. Does nothing if this is not overriden by a manager...
   */
  @Override
  public void deletePermanent(Key project, Key entry, int event)
      throws StatementNotExecutedException {
    String condition = "";
    if (event == AbstractQueryManager.DELETE_DELETED) {
      condition = " AND " + DELETED + "='" + IStandardColumnTypes.DELETED_YES + "' AND "
          + MESSAGE_NUMBER + "=" + -1;
    }
    String query = "DELETE  FROM " + databaseName + "." + tableName
        + " WHERE " + PROJECT_ID + "='" + project.getID()
        + "' AND " + PROJECT_DATABASE_ID + "=" + project.getDBID()
        + " AND " + ID + "=" + entry.getID()
        + " AND " + DATABASE_ID + "=" + entry.getDBID()
        + condition + ";";
    try {
      executeQuery(query);
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }
  }

  /**
   * Deletes the values for the given Record Key
   */
  @Override
  public void deleteValue(Key entryKey, Key projectKey) throws StatementNotExecutedException {
//        ArrayList<DataColumn> data = new ArrayList<>();
    DataRow data = new DataRow(tableName);
    ArrayList<String> key = new ArrayList<>();
    data.add(new DataColumn(IStandardColumnTypes.DELETED_YES, DELETED));
    data.add(new DataColumn(1, MESSAGE_NUMBER));
    data.add(new DataColumn(entryKey.getID(), ID));
    data.add(new DataColumn(entryKey.getDBID(), DATABASE_ID));
    data.add(new DataColumn(projectKey.getID(), PROJECT_ID));
    data.add(new DataColumn(projectKey.getDBID(), PROJECT_DATABASE_ID));
    key.add(ID.getColumnName());
    key.add(DATABASE_ID.getColumnName());
    key.add(PROJECT_ID.getColumnName());
    key.add(PROJECT_DATABASE_ID.getColumnName());
    updateData(data, key);
    entryDeleted(new EntryEvent(projectKey, entryKey));
    notifyUpdate();
  }

  @Override
  public boolean isSynchronised(ArrayList<DataColumn> entry) throws StatementNotExecutedException {
    String whereQuery = "";
    for (DataColumn hash : entry) {
      for (String column : primaryColumns) {
        if (hash.getColumnName().equals(ColumnHelper.removeDatabaseName(column))) {
          whereQuery += column + "='" + hash.getValue() + "' AND ";
        }
      }
    }
    String query = "SELECT " + MESSAGE_NUMBER + " FROM " + tableName
        + " WHERE " + whereQuery + MESSAGE_NUMBER + "!=0";
    try {
      ResultSet rs = executeSelect(query);
      if (rs.next()) {
        return false;
      }
      return true;
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query, ex);
    }
  }

  @Override
  public boolean isConflicted(Key projectKey) throws StatementNotExecutedException {

    String query = "SELECT " + MESSAGE_NUMBER + " FROM " + tableName
        + " WHERE " + PROJECT_ID + "=" + projectKey.getID() + " AND "
        + PROJECT_DATABASE_ID + "=" + projectKey.getDBID() + " AND " + MESSAGE_NUMBER + "=-1 AND "
        + DELETED
        + " = '" + IStandardColumnTypes.DELETED_NO + "'";
    try {
      ResultSet rs = executeSelect(query);
      if (rs.next()) {
        return true;
      }
      return false;
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query, ex);
    }
  }

  /**
   * Inserts the given data into the current table
   *
   * @param data The data of the record
   * @param ignore whether the insert shall ignore errors
   */
  @Override
  public void insertData(DataRow data, boolean ignore)
      throws StatementNotExecutedException {
    insertData(data, databaseName + "." + tableName, ignore);

  }

  /**
   * Inserts the given data into the current table
   *
   * @param data The data
   */
  @Override
  public void insertData(DataRow data)
      throws StatementNotExecutedException {
    insertData(data, false);

  }

  /**
   * Loads the entry for the current table in the EntryData object, informations about the entry can
   * be found in the object itself. This only works with plain string entrys. More komplex tables
   * must override the methode
   */
  @Override
  public void load(DataSetOld data) throws StatementNotExecutedException {
    String select = "*";
    if (!dataColumns.isEmpty()) {
      select = "";
      for (ColumnType column : dataColumns) {
        select += column.getColumnName() + ",";
      }
      select = select.substring(0, select.length() - 1);
    }
    String query = "SELECT " + select + " FROM " + databaseName + "." + tableName
        + " WHERE " + PROJECT_ID + "='" + data.getProjectKey().getID()
        + "' AND " + PROJECT_DATABASE_ID + "=" + data.getProjectKey().getDBID()
        + " AND " + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "';";
    try {
      xResultSet rs = executeSelect(query);
      DataTableOld result = null;
      ResultSetMetaData metaData = rs.getMetaData();
      while (rs.next()) {
        if (result == null) {
          result = data.getOrCreateDataTable(tableName);
        }
        DataRow row = new DataRow(tableName);

        for (int i = 0; i < dataColumns.size(); i++) {
          ColumnType columnType = dataColumns.get(i);
          try {
            String string = rs.getString(i + 1);
            if (string != null) {
              row.put(columnType, (string));
            }
          } catch (SQLException e) {
            //Date = 0000-00-00
            if (!e.getSQLState().equals("S1009")) {
              throw e;
            } else {
              e.printStackTrace();
            }
          }
        }
        result.add(row);
      }
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }

  }

  /**
   * Updates the data for the current table with the given data and the given key
   *
   * @param data The data to be updated
   * @param key The key of the Entry
   * @return The number of rows affected
   */
  @Override
  public int updateData(DataRow data, ArrayList<String> key)
      throws StatementNotExecutedException {
    return updateData(data, databaseName + "." + tableName, key);
  }

  public void insertOrUpdate(DataRow data, ArrayList<String> key, ArrayList<Key> keys)
      throws StatementNotExecutedException {
    insertOrUpdate(data, databaseName + "." + tableName, key, keys);
  }

  @Override
  public int updateDataWithKey(ArrayList<DataColumn> data,
      ArrayList<DataColumn> key) throws StatementNotExecutedException {
    return updateDataWithKey(data, databaseName + "." + tableName, key);
  }

  /**
   * Updates the database number if (for some reason) no database number was set
   *
   * @param newNumber The new number to be set
   */
  @Override
  public void updateDatabaseNumber(int newNumber) throws StatementNotExecutedException {
    String updateString =
        "UPDATE " + tableName + " SET " + PROJECT_DATABASE_ID + " = " + newNumber + " WHERE "
            + PROJECT_DATABASE_ID + " = " + -1;
    try {
      dbId = newNumber;

      executeQuery(updateString);

    } catch (SQLException ex) {
      throw new StatementNotExecutedException(updateString, ex);
    }
  }

  /**
   * Returns the last synchronisation time for the current table
   */
  @Override
  public String getLastSynchronisation(Key projectKey) throws StatementNotExecutedException {
    String query = "SELECT MAX(" + STATUS + ") FROM " + tableName
        + " WHERE " + PROJECT_ID + "=" + projectKey.getID()
        + " AND " + PROJECT_DATABASE_ID + "=" + projectKey.getDBID();
    try {
      ResultSet rs = executeSelect(query);

      if (rs.next()) {
        String max = rs.getString(1);
        return (max != null) ? max : "0";
      }
      return "0";
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }
  }

  //    /**
//     * Updates the given entrys with the given table scheme
//     *
//     * @param data The data to be updated
//     */
//    public void updateEntries(ArrayList<DataColumn> data) throws StatementNotExecutedException {
//        //check if entry is committed
//        String primary = "";
//        for (int i = 0; i < data.size(); i++) {
//            DataColumn entry = data.get(i);
//            for (String type : primaryColumns) {
//                if (type.equals(entry.getColumnName())) {
//                    primary += entry.getColumnName() + "='" + entry.getValue() + "' AND ";
//                    break;
//                }
//            }
//            if (entry.getColumnName().equals(MESSAGE_NUMBER.getColumnName())) {
//                entry.setValue("0");//make sure data that is updated is always zero
//            }
//        }
//        if (primary.endsWith("AND ")) {
//            primary = primary.substring(0, primary.length() - 4);
//        }
//        String queryManager = "SELECT " + MESSAGE_NUMBER + " FROM " + tableName
//                + " WHERE " + primary;
//        try {
//            xResultSet rs = executeSelect(queryManager);
//            if (rs.next()) {//entry exists update
//                int number = rs.getInt(MESSAGE_NUMBER);
//                if (number == 0) {//only update if not uncommited entry
//                    insertOrUpdate(data, primaryColumns);
//
//                }
//
//            } else {//no entry yet insert
//                insertData(data, tableName);
//            }
//        } catch (SQLException ex) {
//            throw new StatementNotExecutedException(queryManager, ex);
//        }
//    }
  @Override
  public String toString() {
    return localisedTableName;
  }

  /**
   * Sets the given data to synchronised.
   */

  public void setSynchronised(DataSetOld entryData, DataSetOld returnData)
      throws StatementNotExecutedException {

    DataTableOld dataTable = entryData.getOrCreateDataTable(tableName);
    if (dataTable == null) {
      return;
    }
    DataTableOld dataTableReturn = null;
    if (returnData != null) {
      dataTableReturn = returnData.getOrCreateDataTable(tableName);
    }

    for (DataRow data : dataTable) {
      DataRow upData = new DataRow(tableName);

      boolean foundFile = false;
      for (DataColumn entry : data.iterator()) {
        for (String type : primaryColumns) {
          if (type.equals(entry.getColumnName()) || type.equals(
              ColumnHelper.removeDatabaseName(tableName) + "." + entry.getColumnName())) {
            upData.add(entry);
            break;
          }
        }
        if (entry.getColumnName().equals(FILE_NAME.getColumnName())) {
          foundFile = true;

        }

      }
      DataRow row = null;
      if (dataTableReturn != null) {
        row = dataTableReturn.getRow(upData);

      }

      if (foundFile) {
        //get file and delete it!

        String query = "SELECT " + FILE_NAME + " FROM " + tableName;
        String where = " WHERE ";
        ArrayList<String> values = new ArrayList<>();
        for (DataColumn primaryColumn : upData.iterator()) {
          if (primaryColumns.contains(
              ColumnHelper.removeDatabaseName(tableName) + "." + primaryColumn.getColumnName())
              || primaryColumns.contains(primaryColumn.getColumnName())
              || primaryColumns.contains(primaryColumn.getColumnName().toLowerCase())) {
            where += "`" + primaryColumn.getColumnName() + "`" + " = '" + primaryColumn.getValue()
                + "' AND ";
          } else {
            for (int j = 0; j < primaryColumns.size(); j++) {

              if ((primaryColumn.getColumnName()).equals(tableName + "." + primaryColumns.get(j))) {
                where +=
                    "`" + primaryColumn.getColumnName() + "`" + " = '" + primaryColumn.getValue()
                        + "' AND ";
                break;

              }
            }
          }


        }
        try {

          if (where.endsWith("AND ")) {
            where = where.substring(0, where.length() - 4);
          }
          final xResultSet xResultSet = executeSelect(query + where);
          if (xResultSet.next()) {

            String fileName = xResultSet.getString(FILE_NAME);
            final File file = new File(
                XBookConfiguration.TMP_UPLOAD_DIRECTORY + "/" + fileName);
            if (file.exists()) {
              //DELETE FILE
              file.delete();
            }
          }
        } catch (SQLException e) {
          e.printStackTrace();
        }


      }

      if (row != null) {
        upData.putAll(row);
      }
      //TODO find corresponding data row from return data

      upData.put(MESSAGE_NUMBER, "0");
      updateData(upData, primaryColumns);
    }

  }

  /**
   * Sets the given data to conflicted
   */
  @Override
  public void setConflicted(DataSetOld dataSet) throws StatementNotExecutedException {
    DataTableOld dataTable = dataSet.getOrCreateDataTable(tableName);
    if (dataTable == null) {
      return;
    }

    for (DataRow data : dataTable) {
      ArrayList<DataColumn> key = new ArrayList<>();
      for (String type : primaryColumns) {
        for (DataColumn entry : data.iterator()) {
          if (ColumnHelper.removeDatabaseName(type).equals(entry.getColumnName())) {
            key.add(entry);
            break;
          }
        }

      }

      ArrayList<DataColumn> dataList = new ArrayList<>();
      dataList.add(new DataColumn(-1, MESSAGE_NUMBER));

      updateDataWithKey(dataList, key);
    }

  }

  @Override
  public ArrayList<DataSetOld> getConflicts(ProjectDataSet project)
      throws StatementNotExecutedException {
    return null;
  }

  @Override
  public ArrayList<String> getPrimaryColumns() {
    return primaryColumns;
  }

  /**
   * Returns a list of SolveConflictLineGroups for the given Entry. this is done by getting the
   * corresponding database entrys local and global. then a SolveConflictLineGroup is created and
   * finally the Groups are filtered.
   */
  @Override
  public ArrayList<SolveConflictLineGroup> getConflictLines(DataSetOld localData,
      DataSetOld serverData) throws StatementNotExecutedException, EntriesException {
    ArrayList<SolveConflictLineGroup> lines = new ArrayList<>();
    ArrayList<String> allPrimaries = getPrimaryColumns();

    //Create copys for removing
    DataTableOld localDataTable = localData.getOrCreateDataTable(tableName);
    DataTableOld localEntryCopy = localDataTable.copy();
    DataTableOld serverDataTable = serverData.getOrCreateDataTable(tableName);
    DataTableOld serverEntryCopy = serverDataTable.copy();

    //go throug all local entrys
    for (DataRow localAll : localDataTable) {
      ArrayList<DataColumn> primLocal = new ArrayList<>();
      //go throug local data and get all primarys
      for (DataColumn local : localAll.iterator()) {
        if (allPrimaries.contains(tableName + "." + local.getColumnName()) || allPrimaries.contains(
            local.getColumnName())) {
          primLocal.add(new DataColumn(local.getValue(), local.getColumnName()));
        }
      }
      //go though all server data and get primarys
      for (DataRow serverAll : serverDataTable) {
        ArrayList<DataColumn> primServer = new ArrayList<>();
        for (DataColumn serv : serverAll.iterator()) {
          if (allPrimaries.contains(tableName + "." + serv.getColumnName()) || allPrimaries
              .contains(
                  serv.getColumnName())) {
            primServer.add(new DataColumn(serv.getValue(), serv.getColumnName()));
          }
        }
        //if primarys equal get Conflict Lines for this data
        if (primLocal.equals(primServer)) {
          lines.add(getLinesForEntry(localAll, serverAll));

          localEntryCopy.remove(localAll);
          serverEntryCopy.remove(serverAll);
          break;
        }
      }
    }
    //get lines for all entries that have no global partner
    for (DataRow localNoPartner : localEntryCopy) {
      lines.add(getLinesForEntry(localNoPartner, new DataRow(tableName)));
    }
    //go through all entries that don't exist locally
    for (DataRow serverNoPartner : serverEntryCopy) {
      lines.add(getLinesForEntry(new DataRow(tableName), serverNoPartner));
    }
    if ((this instanceof AbstractBaseEntryManager)
        || (this instanceof AbstractExtendedProjectEntryManager)) {
      return filterLines(lines);
    } else {
      return ((AbstractExtendedEntryManager) this).filterLinesExtended(lines);
    }

  }

  /**
   * Filters the List of solve Conflict lines. This can be used to only display Conflicted lines or
   * mandatory lines.
   */
  @Override
  public ArrayList<SolveConflictLineGroup> filterLines(ArrayList<SolveConflictLineGroup> lines) {
    ArrayList<SolveConflictLineGroup> filtered = new ArrayList<>();
    for (SolveConflictLineGroup group : lines) {
      SolveConflictLineGroup newGroup = new SolveConflictLineGroup(tableName);

      for (SolveConflictLine line : group.getLines()) {
        Collection<ColumnType> list = line.getKeys();

        if (list.contains(PROJECT_ID) || list.contains(PROJECT_DATABASE_ID)
            || list.contains(MESSAGE_NUMBER) || list.contains(STATUS)
            || list.contains(DELETED)) {
          continue;
        }
        newGroup.add(line);
      }
      if (!newGroup.getLines().isEmpty()) {
        newGroup.setLastSync(group.getLastSync());
        newGroup.setDeletedGlobal(group.isDeletedGlobal());
        filtered.add(newGroup);

      }
    }
    return filtered;
  }

  protected List<ColumnType> getSepcialColumnsForConflict() {
    return new ArrayList<>();
  }

  protected void getSpecialLinesForEntry(DataRow localAll, DataRow serverAll,
      SolveConflictLineGroup lineGroup) {
  }

  /**
   * Returns the localised table name.
   */
  @Override
  public String getLocalisedTableName() {
    return localisedTableName;
  }

  @Override
  public void save(DataSetOld data) throws StatementNotExecutedException {
    if (data.getOrCreateDataTable(tableName) == null) {
      return;
    }

//        ArrayList<DataColumn> dataList = new ArrayList<>();
    DataRow dataList = new DataRow(tableName);
    dataList.add(new DataColumn(data.getProjectKey().getID(), PROJECT_ID));
    dataList.add(new DataColumn(data.getProjectKey().getDBID(), PROJECT_DATABASE_ID));
    dataList.add(new DataColumn(1, MESSAGE_NUMBER));
    dataList.add(new DataColumn(IStandardColumnTypes.DELETED_YES, DELETED));
    updateData(dataList, primaryColumns);

    for (DataRow entryData : data.getOrCreateDataTable(tableName)) {

      if (entryData.isEmpty()) {
        continue;
      }
//

//            dataList = new ArrayList<>();
      dataList = new DataRow(tableName);
      dataList.add(new DataColumn(data.getProjectKey().getID(), PROJECT_ID));
      dataList.add(new DataColumn(data.getProjectKey().getDBID(), PROJECT_DATABASE_ID));
      for (DataColumn row : entryData.iterator()) {
        dataList.add(row);
      }
      dataList.add(new DataColumn(1, MESSAGE_NUMBER));
      dataList.add(new DataColumn(IStandardColumnTypes.DELETED_NO, DELETED));
      int status = updateData(dataList, primaryColumns);

      if (status == 0) {
        insertData(dataList);
        entryAdded(new EntryEvent(data.getProjectKey(), entryData));
      } else {
        entryUpdated(new EntryEvent(data.getProjectKey(), entryData));
      }
    }
    notifyUpdate();
  }

  protected void save(EntryDataSet data, ArrayList<Key> keys) throws StatementNotExecutedException {
    if (!data.hasDataTable(tableName)) {
      return;
    }
    DataTableOld dataTable = data.getOrCreateDataTable(tableName);

    String keyQuery = "(";
    for (Key key : keys) {
      keyQuery += "(" + ID + "=" + key.getID()
          + " AND " + DATABASE_ID + "=" + key.getDBID() + ") OR ";
    }
    if (keyQuery.endsWith(" OR ")) {
      keyQuery = keyQuery.substring(0, keyQuery.length() - " OR ".length()) + ")";
    }

//        //update where key not in datatable list
//        insertOrUpdate(dataList, primaryColumns);
    ArrayList<DataRow> dataRows = new ArrayList<>();
    for (DataRow entryData : dataTable) {
      DataRow list = new DataRow(tableName);
      for (DataColumn dataColumn : entryData.iterator()) {
        if (primaryColumns.contains(tableName + "." + dataColumn.getColumnName())) {
          list.add(dataColumn);
        }

      }
      if (!list.isEmpty()) {
        dataRows.add(list);
      }

    }
    String filter = "";
    for (DataRow dataRow : dataRows) {
      filter += "(";
      for (DataColumn dataColumn : dataRow.iterator()) {
        filter += dataColumn.getColumnName() + "='" + dataColumn.getValue() + "' AND ";
      }
      if (filter.endsWith(" AND ")) {
        filter = filter.substring(0, filter.length() - " AND ".length()) + ") OR ";
      }
    }
    if (filter.endsWith(" OR ")) {
      filter = filter.substring(0, filter.length() - " OR ".length());
    }
    String query = "UPDATE " + databaseName + "." + tableName
        + " SET " + MESSAGE_NUMBER + "=" + 1 + ", " + DELETED + "='" + DELETED_YES + "'"
        + " WHERE " + keyQuery
        + " AND " + PROJECT_ID + "=" + data.getProjectKey().getID()
        + " AND " + PROJECT_DATABASE_ID + "=" + data.getProjectKey().getDBID();
    boolean skipRemoving = false;
    if (!filter.isEmpty()) {
      query += " AND NOT (" + filter + ")";
    } else {
      //in basemanagers there is only one entry
      if (this instanceof AbstractBaseEntryManager) {
        skipRemoving = dataTable.size() != 0;
      }
    }

    if (!skipRemoving) {
      try {
        executeQuery(query);
      } catch (SQLException e) {
        throw new StatementNotExecutedException(query, e);
      }
    }

    for (DataRow entryData : dataTable) {
      if (entryData.isEmpty()) {
        continue;
      }
      DataRow dataList = new DataRow(tableName);
      dataList.add(new DataColumn(data.getProjectKey().getID(), PROJECT_ID));
      dataList.add(new DataColumn(data.getProjectKey().getDBID(), PROJECT_DATABASE_ID));
      for (DataColumn row : entryData.iterator()) {
        dataList.add(row);
      }

      dataList.add(new DataColumn(1, MESSAGE_NUMBER));
      dataList.add(new DataColumn(IStandardColumnTypes.DELETED_NO, DELETED));

      if (this instanceof AbstractInputUnitManager) {

        DataColumn userInformation = ((AbstractInputUnitManager) this).getUserInformation();
        if (userInformation != null) {
          dataList.add(userInformation);
        }
      }
      insertOrUpdate(dataList, primaryColumns, keys);


    }

  }

  /**
   * Sets the status of the entry given by the given primary keys to synchronise
   */
  @Override
  public void setStatusToSynchronise(ArrayList<DataColumn> primaryKeyValues)
      throws StatementNotExecutedException {
    ArrayList<DataColumn> data = new ArrayList<>();
    data.add(new DataColumn(1, MESSAGE_NUMBER));
    updateDataWithKey(data, primaryKeyValues);
  }

  /**
   * Returns the number of Uncommitted Entries for the given Project
   *
   * @param projectKey The Key of the Project
   * @return The number of entries uncommitted
   */
  @Override
  public int getNumberOfUncommittedEntries(Key projectKey) throws StatementNotExecutedException {
    String query = "SELECT COUNT(*) as count FROM " + databaseName + "." + tableName
        + " WHERE " + MESSAGE_NUMBER + "=" + 1
        + " AND " + PROJECT_ID + "=" + projectKey.getID()
        + " AND " + PROJECT_DATABASE_ID + "=" + projectKey.getDBID();
    try {
      ResultSet rs = executeSelect(query);
      if (rs.next()) {
        return rs.getInt("count");
      }
      return 0;
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query, ex);
    }

  }

  public void addEntryListener(EntryListener listerner) {
    listenerList.add(listerner);
  }

  protected void entryAdded(EntryEvent evt) {
    for (EntryListener listener : listenerList) {
      listener.onEntryAdded(evt);
    }
  }

  protected void entryDeleted(EntryEvent evt) {
    for (EntryListener listener : listenerList) {
      listener.onEntryDeleted(evt);
    }
  }

  protected void entryUpdated(EntryEvent evt) {
    for (EntryListener listener : listenerList) {
      listener.onEntryUpdated(evt);
    }
  }

  /**
   * Returns the number of entries for the given AbstractProjectOverviewScreen in the DB
   *
   * @param projectKey The Key of the AbstractProjectOverviewScreen including Id and DATABASE_ID
   * @return The number of entries
   */
  @Override
  public int getNumberofEntrys(Key projectKey) throws StatementNotExecutedException {
    String query = "SELECT Count(*) FROM " + databaseName + "." + tableName
        + " WHERE " + PROJECT_ID + "='" + projectKey.getID() + "' AND " + PROJECT_DATABASE_ID + "='"
        + projectKey.getDBID() + "' AND " + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "';";
    try {
      ResultSet rs = executeSelect(query);
      if (rs.next()) {
        return rs.getInt("Count(*)");
      }
      return 0;
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }
  }


  @Override
  public void updateUnsyncedEntries(ProjectDataSet project) throws StatementNotExecutedException {
    String query = "UPDATE " + databaseName + "." + tableName
        + " SET " + MESSAGE_NUMBER + "=" + IStandardColumnTypes.CHANGED
        + " WHERE " + STATUS + "='0' "
        + " AND " + PROJECT_ID + "=" + project.getProjectKey().getID()
        + " AND " + PROJECT_DATABASE_ID + "=" + project.getProjectKey().getDBID()
        + " AND " + MESSAGE_NUMBER + "=" + IStandardColumnTypes.SYNCHRONIZED + ";";
    try {
      executeQuery(query);
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }
  }

  /**
   * Returns the number of conflicted Entries for the given Project
   *
   * @param projectKey The Key of the Project
   * @return The number of entries conflicted
   */
  @Override
  public int getNumberOfConflictedEntries(Key projectKey) throws StatementNotExecutedException {
    String query = "SELECT COUNT(*) as count FROM " + databaseName + "." + tableName
        + " WHERE " + MESSAGE_NUMBER + "=" + -1
        + " AND " + PROJECT_ID + "=" + projectKey.getID()
        + " AND " + PROJECT_DATABASE_ID + "=" + projectKey.getDBID();
    try {
      ResultSet rs = executeSelect(query);
      if (rs.next()) {
        return rs.getInt("count");
      }
      return 0;
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query, ex);
    }

  }

  protected String getConflictDisplay(HashMap<ColumnType, String> value) {
    StringBuilder sb = new StringBuilder();
    try {
      for (Map.Entry<ColumnType, String> s : value.entrySet()) {
        sb.append(getStringRepresentation(s.getKey(), s.getValue())).append(" ");
      }
    } catch (StatementNotExecutedException | EntriesException e) {
      e.printStackTrace();
    }
    return sb.toString();
  }

  @Override
  public void notifyUpdate() {
    EventRegistry.notifyListener(tableName);
  }

  protected static String getResultFromColumnType(ColumnType columnType, ResultSet rs)
      throws SQLException {
    String result;
    if (columnType.getType() == ColumnType.Type.BOOLEAN) {
      result = rs.getString(columnType.getColumnName());
      if (result != null && result.equals("1")) {
        result = Loc.get("YES");
      } else {
        result = "";
      }
    } else if (columnType.getType() == ColumnType.Type.YES_NO_NONE) {
      result = rs.getString(columnType.getColumnName());

      if (result != null) {
        if (result.equals("1")) {
          result = Loc.get("YES");
        } else if (result.equals("0")) {
          result = Loc.get("NO");
        } else {
          result = "";
        }

      } else {
        result = "";
      }
    } else if (columnType.getType() == ColumnType.Type.HIERARCHIC
        || columnType.getType() == ColumnType.Type.EXTERN_MULTILAYER) {
      if (columnType.isHierarchyDisplayed()) {
        // the value is the id of the "leave value", convert it to the path from root to leave
        String id = rs.getString(
            columnType + "." + ColumnHelper.removeDatabaseName(columnType.getConnectedTableName()));
        // TODO xxx;
        result = DataManagementHelper.getHierarchicString(columnType, id);
      } else {
        // the value is the "leave value", no further conversions needed
        result = rs.getString(
            columnType + "." + ColumnHelper.removeDatabaseName(columnType.getConnectedTableName()));
      }
    } else if (columnType.getType() == ColumnType.Type.ID) {
      result = rs.getString(
          columnType + "." + ColumnHelper.removeDatabaseName(columnType.getConnectedTableName()));
    } else if (columnType.getType() == ColumnType.Type.ID_SPECIFIC) {
      result = rs.getString(
          ColumnHelper.removeDatabaseName(columnType.getConnectedTableName()) + "." + VALUE);
    } else if (columnType.getType() == ColumnType.Type.DATE) {
//                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      final String columnName = rs.getString(columnType.getColumnName());
      result = columnName;
//                try {
//                    if (columnName != null) {
//                        result = dateFormat.parse(columnName).toString();
//                    }
//
//                } catch (ParseException e) {
//
//                }

    } else if (columnType.getType() == ColumnType.Type.TIME) {
      final String value = rs.getString(columnType.getColumnName());

      if (!value.isEmpty() && value.length() > 5) {
        result = value.substring(0, 5);
      } else {
        result = value;

      }
    } else {
      result = rs.getString(columnType.getColumnName());
    }
    return result;

  }

//  // Alex edited
//  public de.uni_muenchen.vetmed.toolset3.api.helper.DataList getDataForColumns(
//      List<Key> projectKeys,
//      List<ColumnType> columns)
//      throws StatementNotExecutedException, EntriesException {
//    DataList list = new DataList();
//    String keyQuery = "";
//    if (projectKeys != null && !projectKeys.isEmpty()) {
//      keyQuery = " (";
//      for (Key key : projectKeys) {
//        keyQuery += "("
//            + tableName + "." + PROJECT_ID + "=" + key.getID() + " AND "
//            + tableName + "." + PROJECT_DATABASE_ID + "= " + key.getDBID() + ") OR";
//      }
//      if (keyQuery.endsWith("OR")) {
//        keyQuery = keyQuery.substring(0, keyQuery.length() - 2) + ")";
//      }
//
//    } else {
//      return list;
//    }
//
//    HashMap<ColumnType, HashMap<String, HierarchicData>> maps = new HashMap<>();
//    ArrayList<ColumnType> columnTypesList = new ArrayList<>();
//    StringBuilder select = new StringBuilder();
//    StringBuilder where = new StringBuilder();
//    ArrayList<String> fromTables = new ArrayList<>();
//
//    // Alex edit:
////        if (!columns.contains(ID)&&dataColumns.contains(ID)) {
////            columns.add(ID);
////        }
////        if (!columns.contains(DATABASE_ID)&&dataColumns.contains(DATABASE_ID)) {
////            columns.add(DATABASE_ID);
////        }
////        if (!columns.contains(PROJECT_ID)&&dataColumns.contains(PROJECT_ID)) {
////            columns.add(PROJECT_ID);
////        }
////        if (!columns.contains(PROJECT_DATABASE_ID)&&dataColumns.contains(PROJECT_DATABASE_ID)) {
////            columns.add(PROJECT_DATABASE_ID);
////        }
//    for (ColumnType column : columns) {
//      doStuffWithColumnType(column, columnTypesList, select, where, fromTables);
//    }
//    if (select.length() == 0) {
//      return list;
//    }
//    String from = "";
//    for (String string : fromTables) {
//      from += string + ",";
//    }
//    String query = "SELECT " + select.substring(0, select.length() - 1)
//        + " FROM " + from + tableName
//        + " WHERE " + keyQuery
//        + " AND " + tableName + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
//        + where;
//    try {
//      LogFactory.getLog(AbstractSynchronisationManager.class).debug(query);
//      xResultSet rs = executeSelect(query);
//      while (rs.next()) {
//        EntryMap map = new EntryMap();
//        for (ColumnType columnType : columnTypesList) {
//          EntryValues<String> v = new EntryValues<>();
//          v.add(getResultFromColumnType(columnType, rs));
//          map.put(columnType.getColumnHeader(), v);
//        }
//        if (!list.add(map)) {
//          throw new EntriesException("nicht alle werte vorhanden");
//        }
//      }
//    } catch (SQLException ex) {
//      throw new StatementNotExecutedException(query, ex);
//    }
//    return list;
//  }

  protected void doStuffWithColumnType(ColumnType columnType, List<ColumnType> rowNames,
      StringBuilder select,
      StringBuilder where, List<String> fromTables) {
    doStuffWithColumnType(columnType, rowNames, select, where, fromTables,
        ColumnType.ExportType.ALL, null, null,
        new AtomicBoolean(false), null);
  }

  protected void doStuffWithColumnType(ColumnType columnType, List<ColumnType> rowNames,
      StringBuilder select,
      StringBuilder where, List<String> fromTables,
      ExportType exportType, List<ColumnType> list,
      Map<ColumnType, SearchEntryInfo> filter, AtomicBoolean wasfilterUsed,
      UniqueArrayList<ColumnType> hierarchicTables) {
    doStuffWithColumnType(columnType, rowNames, select, where, fromTables, exportType, list, filter,
        wasfilterUsed,
        new StringBuilder(), hierarchicTables);
  }

  protected void doStuffWithColumnType(ColumnType columnType, List<ColumnType> rowNames,
      StringBuilder select,
      StringBuilder where, List<String> fromTables,
      ExportType exportType, List<ColumnType> list,
      Map<ColumnType, SearchEntryInfo> filter, AtomicBoolean wasfilterUsed,
      StringBuilder join, ArrayList<ColumnType> hierarchicTables) {

    if (!columnType.equals(ID) && !columnType.equals(DATABASE_ID) && !columnType.equals(
        PROJECT_ID) && !columnType.equals(PROJECT_DATABASE_ID)) {
      if (!columnType.equals(
          AbstractInputUnitManager.USER_ID_INPUTUNIT) && list != null && !list.isEmpty() && !list
          .contains(
              columnType)) {
        return;
      }
    }
    if (columnType.getExportType() == exportType || exportType == ExportType.ALL
        || columnType.getExportType() == ExportType.ALL) {
      String languageQuery;
      switch (columnType.getType()) {
        case HIERARCHIC:
          // valueTa.put(type.getColumnName(), null);
          rowNames.add(columnType);
          languageQuery = "";
          if (columnType.isLanguage()) {
            languageQuery = " AND (" + LanguageManager.getLanguageQuery(
                columnType.getConnectedTableName()) + ")";
          }
          if (hierarchicTables != null) {
            if (DataManagementHelper.getHierarchicCachedData().containsKey(columnType)) {
              DataManagementHelper.getHierarchicCachedData().remove(columnType);
            }
            hierarchicTables.add(columnType);
          }
          ColumnType multiLayerValueToSelect;
          if (columnType.isHierarchyDisplayed()) {
            multiLayerValueToSelect = ID;
          } else {
            multiLayerValueToSelect = VALUE;
          }

          select.append(
              "(SELECT " + multiLayerValueToSelect + " FROM " + columnType.getConnectedTableName()
                  + " WHERE " + columnType.getConnectedTableName() + "." + ID + "=" + columnType
                  + languageQuery
                  + " AND " + columnType.getConnectedTableName() + "." + DELETED + "='"
                  + IStandardColumnTypes.DELETED_NO + "' LIMIT 1)"
                  + " AS " + getAsString(columnType + "." + ColumnHelper.removeDatabaseName(
                  columnType.getConnectedTableName())) + ",");
          break;
        case ID: {
          rowNames.add(columnType);
          languageQuery = "";
          if (columnType.isLanguage()) {
            languageQuery = " AND (" + LanguageManager.getLanguageQuery(
                columnType.getConnectedTableName()) + ")";
          }
          select.append("(SELECT " + VALUE + " FROM " + columnType.getConnectedTableName()
              + " WHERE " + columnType.getConnectedTableName() + "." + ID + "=" + columnType
              + languageQuery
              + " AND " + columnType.getConnectedTableName() + "." + DELETED + "='"
              + IStandardColumnTypes.DELETED_NO + "' LIMIT 1)"
              + " AS " + getAsString(columnType + "." + ColumnHelper.removeDatabaseName(
              columnType.getConnectedTableName())) + ",");
          break;
        }
        case ID_SPECIFIC: {
          rowNames.add(columnType);
          fromTables.addAll(columnType.getAdditionalTables());
          select.append(columnType.getConnectedTableName() + "." + VALUE + ",");
          where.append(columnType.getSpecificWhere());

          break;
        }

        case SUBQUERY_SPECIFIC: {
          rowNames.add(columnType);

          select
              .append("(" + columnType.getSelectQuery() + ") AS " + getAsString(columnType) + ",");
          break;
        }
        // case ID_LANAGUAGE_NOSORT:
        case EXTERN_VALUE:
          rowNames.add(columnType);

          select.append("(SELECT " + columnType + " FROM " + getDatabaseName(
              columnType.getColumnName()) + " WHERE " + columnType.getWhereQuery() + ") AS "
              + getAsString(
              columnType.getColumnName()) + ",");
          break;

        default: {
          if (ColumnHelper.getTableName(columnType.getColumnName()).equals("")) {
            select.append(tableName + "." + columnType + ",");
          } else {
            select.append(columnType + ",");
          }
          rowNames.add(columnType);

          break;
        }

      }
      for (ColumnType additional : columnType.getAdditionalColumns()) {
        if (additional == null) {
          continue;
        }
        if (additional.getExportType() == exportType || exportType == ExportType.ALL
            || additional.getExportType() == ExportType.ALL) {
          switch (additional.getType()) {
            case EXTERN_VALUE:
              select.append("(SELECT " + additional + " FROM " + getDatabaseName(
                  additional.getColumnName()) + " WHERE " + additional.getWhereQuery() + ") AS "
                  + getAsString(
                  additional.getColumnName()) + ",");
              break;
            case DATE:
            case TIME:
            case VALUE:
            case VALUE_THESAURUS:

              if (ColumnHelper.getTableName(additional.getColumnName()).equals("")) {
                select.append(tableName + "." + additional + ",");
              } else {
                select.append(additional + ",");
              }
              break;
            case ID:
              select.append(
                  "(SELECT " + VALUE + " FROM " + additional.getConnectedTableName() + " WHERE "
                      + additional.getConnectedTableName() + "." + ID + "=" + additional + ") AS "
                      + getAsString(
                      additional.getColumnName()) + ",");
              break;
            case EXTERN_ID: {
              String langugageQuery = "";
              if (additional.isLanguage()) {
                langugageQuery = " AND " + LanguageManager.getLanguageQuery(
                    additional.getConnectedTableName());
              }
              select.append(
                  "(SELECT " + additional.getConnectedTableName() + "." + VALUE + " FROM "
                      + additional.getConnectedTableName() + "," + getDatabaseName(
                      additional.getColumnName())
                      + " WHERE " + additional.getConnectedTableName() + "." + ID + "=" + additional
                      + langugageQuery
                      + " AND " + additional.getWhereQuery()
                      + " AND " + additional.getConnectedTableName() + "." + DELETED + "='"
                      + IStandardColumnTypes.DELETED_NO + "' LIMIT 1) AS " + getAsString(
                      additional.getColumnName()) + ",");

              break;
            }
            case EXTERN_MULTILAYER: {
              // valueTa.put(type.getColumnName(), null);
              languageQuery = "";
              if (columnType.isLanguage()) {
                languageQuery = " AND (" + LanguageManager.getLanguageQuery(
                    additional.getConnectedTableName()) + ")";
              }
              if (hierarchicTables != null) {
                if (DataManagementHelper.getHierarchicCachedData().containsKey(additional)) {
                  DataManagementHelper.getHierarchicCachedData().remove(additional);
                }
                hierarchicTables.add(additional);
              }
              ColumnType multiLayerValueToSelect;
              if (additional.isHierarchyDisplayed()) {
                multiLayerValueToSelect = ID;
              } else {
                multiLayerValueToSelect = VALUE;
              }

              select.append(
                  "(SELECT " + additional.getConnectedTableName() + "." + multiLayerValueToSelect
                      + " FROM " + additional.getConnectedTableName() + "," + getDatabaseName(
                      additional.getColumnName())
                      + " WHERE " + additional.getConnectedTableName() + "." + ID + "=" + additional
                      + languageQuery + " AND " + additional.getWhereQuery()
                      + " AND " + additional.getConnectedTableName() + "." + DELETED + "='"
                      + IStandardColumnTypes.DELETED_NO + "' LIMIT 1)"
                      + " AS " + getAsString(additional + "." + ColumnHelper.removeDatabaseName(
                      additional.getConnectedTableName())) + ",");
              break;
            }
            case ID_SPECIFIC: {
              select.append("(SELECT DISTINCT ").append(additional.getConnectedTableName()).append(
                  ".").append(VALUE).append(" FROM ").append(
                  additional.getConnectedTableName()).append(additional.getSpecificWhere()).append(
                  ") AS ").append(getAsString(additional)).append(",");
            }
          }
        }
      }
    }
    if (filter != null && filter.containsKey(columnType)) {
      SearchEntryInfo searchEntryInfo = filter.get(columnType);
      switch (searchEntryInfo.getCurrentSearchMode()) {
        case EQUALS: {
          where.append(" AND ").append(columnType).append("='").append(searchEntryInfo.getText())
              .append("'");
          break;
        }
        case CONTAINS: {
          where.append(" AND ").append(columnType).append(" LIKE '%")
              .append(searchEntryInfo.getText())
              .append("%'");
          break;
        }
      }

    }
  }

  public HashMap<Key, Integer> getNumberofEntrysForAllProject()
      throws StatementNotExecutedException {

    String query =
        "SELECT Count(*)," + PROJECT_ID + "," + PROJECT_DATABASE_ID + " FROM " + databaseName + "."
            + tableName
            + " WHERE " + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' GROUP BY "
            + PROJECT_ID + "," + PROJECT_DATABASE_ID + ";";
    return sendNumberOfEntriesQuery(query);
  }

  private HashMap<Key, Integer> sendNumberOfEntriesQuery(String query)
      throws StatementNotExecutedException {
    try {
      HashMap<Key, Integer> numberOfEntries = new HashMap<>();

      xResultSet rs = executeSelect(query);
      while (rs.next()) {
        Key key = new Key(rs, PROJECT_ID, PROJECT_DATABASE_ID);
        numberOfEntries.put(key, rs.getInt("Count(*)"));
      }

      return numberOfEntries;
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }
  }

  public HashMap<Key, Integer> getNumberofUnsynchronizedEntrysForAllProject()
      throws StatementNotExecutedException {

    String query =
        "SELECT Count(*)," + PROJECT_ID + "," + PROJECT_DATABASE_ID + " FROM " + databaseName + "."
            + tableName
            + " WHERE " + MESSAGE_NUMBER + "='" + IStandardColumnTypes.CHANGED + "' GROUP BY "
            + PROJECT_ID + "," + PROJECT_DATABASE_ID + ";";
    return sendNumberOfEntriesQuery(query);
  }

  public HashMap<Key, Integer> getNumberofConflictedEntrysForAllProject()
      throws StatementNotExecutedException {

    String query =
        "SELECT Count(*)," + PROJECT_ID + "," + PROJECT_DATABASE_ID + " FROM " + databaseName + "."
            + tableName
            + " WHERE " + MESSAGE_NUMBER + "='" + IStandardColumnTypes.CONFLICTED + "' GROUP BY "
            + PROJECT_ID + "," + PROJECT_DATABASE_ID + ";";
    return sendNumberOfEntriesQuery(query);
  }

  protected String saveFile(boolean fileUpdated, DataRow row, String string)
      throws SQLException, IOException {
    //TODO check if new image was set by user
    LOGGER.debug("file updated: " + fileUpdated);
    if (fileUpdated) {
      //try to load file from disk
      //
      final File file = new File(
          XBookConfiguration.TMP_UPLOAD_DIRECTORY + "/" + string);

      byte[] bytes = Files.readAllBytes(file.toPath());

      String encoded = DatatypeConverter.printBase64Binary(bytes);
      string = encoded;
      row.setImage(true);
    }

    //TODO get path from image column and upload image
    return string;
  }

}
