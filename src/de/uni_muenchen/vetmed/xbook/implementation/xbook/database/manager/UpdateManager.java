package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.util.ArrayList;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class UpdateManager extends TableManager {

    public final static String TABLENAME_UPDATE = "updatecode";
    public final static String TABLENAME_VERSION = "version";
    public final static String TABLENAME_VERSION_GENERAL = IStandardColumnTypes.DATABASE_NAME_GENERAL + ".version";
    public final static String VERSION_DBVERSION = "DBVersion";
    public final static String UPDATE_COMMAND = "Command";
    public final static String UPDATE_VERSION_FROM = "VersionFrom";
    public final static String UPDATE_VERSION_TO = "VersionTo";
    public static String VERSION_DATABASENUMBER = "DatabaseNumber";
    public static String VERSION_MESSAGENUMBER = "MessagesNumber";
    public static String VERSION_USERCOUNT = "Useranzahl";
    public static String VERSION_SYNC_INDICATOR = "Syncindicator";
    public static String VERSION_LASTUPDATE = "LastUpdate";
    private static final Log log = LogFactory.getLog(UpdateManager.class);

    public UpdateManager(Connection connection, String databaseName) {
        super(connection, databaseName);
    }

    public void applyUpdate(ArrayList<String> commando, String db) throws StatementNotExecutedException {
        Savepoint savepoint = null;
        boolean error = false;
        System.out.println("update");
        String errorcommand = "";
        try {
            connection.setAutoCommit(false);
            connection.setCatalog(db);
            savepoint = connection.setSavepoint();
            try {
                if (commando.get(0) != null) {
                    String[] commands = commando.get(0).split(";");

                    for (String command : commands) {
                        String trim = command.trim();
                        if (trim.isEmpty() || trim.equals("\n")) {
                            continue;
                        }
                        errorcommand = command;
                        try {
                            Statement stmt = connection.createStatement();
                            stmt.execute(command);
                            stmt.close();
                        } catch (Exception ex) {
                            log.error("Query: '" + command + "'", ex);
                        }
                    }
                }
//                ArrayList<DataColumn> update = new ArrayList<>();
                DataRow update = new DataRow(TABLENAME_VERSION);
                update.add(new DataColumn(commando.get(1), VERSION_DBVERSION));
                updateData(update, TABLENAME_VERSION);
                connection.commit();
                connection.releaseSavepoint(savepoint);
                savepoint = null;
            } catch (SQLException ex) {
                System.out.println(errorcommand);
                ex.printStackTrace();
                error = true;
            } finally {
                if (savepoint != null) {
                    // Savepoint was not released and nulled -> error.
                    connection.rollback(savepoint);
                    savepoint = null;
                }
                connection.setAutoCommit(true);
                connection.setCatalog(databaseName);
            }
        } catch (SQLException ex) {
            if (savepoint != null) {
                // Savepoint was not released and nulled -> error.
                try {
                    connection.rollback(savepoint);
                } catch (SQLException e) {
                }
            }
        }
        if (error) {
            throw new StatementNotExecutedException(errorcommand);
        }
    }

    public String[] getUpdate(String version) throws StatementNotExecutedException {
        String query = "Select " + UPDATE_COMMAND + "," + UPDATE_VERSION_TO + " FROM " + databaseName + "." + TABLENAME_UPDATE
                + " WHERE " + UPDATE_VERSION_FROM + "='" + version + "';";
        try {
            ResultSet rs = executeSelect(query);
            if (rs.next()) {
                return new String[]{rs.getString(UPDATE_COMMAND), rs.getString(UPDATE_VERSION_TO)};
            }
            return null;

        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query);
        }
    }

    public boolean checkSyncIndicator() throws StatementNotExecutedException {
        String query = "SELECT " + VERSION_SYNC_INDICATOR
                + " FROM " + TABLENAME_VERSION;
        try {
            ResultSet rs = executeSelect(query);
            if (rs.next()) {
                return rs.getInt(VERSION_SYNC_INDICATOR) == 1;
            }
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query);
        }
        return false;
    }

    public boolean existsGeneral() throws StatementNotExecutedException {
        String query = "SELECT COUNT(DISTINCT `table_name`) FROM `information_schema`.`columns` WHERE `table_schema` = '" + IStandardColumnTypes.DATABASE_NAME_GENERAL + "'";
        try {
            ResultSet rs = executeSelect(query);
            if (rs.next()) {
                return rs.getInt(1) > 2;
            }
            return false;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query);
        }
    }

    public boolean isDatabaseInitialized() throws StatementNotExecutedException {
        String query = "SELECT COUNT(DISTINCT `table_name`) FROM `information_schema`.`columns` WHERE `table_schema` = '" + databaseName + "'";
        try {
            ResultSet rs = executeSelect(query);
            if (rs.next()) {
                return rs.getInt(1) > 2;
            }
            return false;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query);
        }

    }

    /**
     * Returns the last update
     *
     * @return
     * @throws StatementNotExecutedException
     */
    public String getLastUpdate() throws StatementNotExecutedException {
        String query = "SELECT " + VERSION_LASTUPDATE + " FROM " + databaseName + "." + TABLENAME_VERSION;
        String lastUpdate = "-1";
        try {
            ResultSet rs = executeSelect(query);
            if (rs.next()) {
                lastUpdate = rs.getString(VERSION_LASTUPDATE);
            }
            query = "SELECT " + VERSION_LASTUPDATE + " FROM " + TABLENAME_VERSION_GENERAL;
            rs = executeSelect(query);
            if (rs.next()) {
                String lastupdateGlobal = rs.getString(VERSION_LASTUPDATE);
                if (lastUpdate.compareToIgnoreCase(lastupdateGlobal) > 0) {
                    lastUpdate = lastupdateGlobal;
                }
            }
            return lastUpdate;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }

    }

    /**
     * Sets the last update to the given value
     *
     * @param lastUpdate The value of the last update
     * @throws StatementNotExecutedException
     */
    public void setLastUpdate(String lastUpdate) throws StatementNotExecutedException {
//        ArrayList<DataColumn> update = new ArrayList<>();
        DataRow update = new DataRow(databaseName + "." + TABLENAME_VERSION);
        update.add(new DataColumn(lastUpdate, VERSION_LASTUPDATE));
        updateData(update, databaseName + "." + TABLENAME_VERSION);

        //update general
        update = new DataRow(TABLENAME_VERSION_GENERAL);
        update.add(new DataColumn(lastUpdate, VERSION_LASTUPDATE));
        updateData(update, TABLENAME_VERSION_GENERAL);


    }
}
