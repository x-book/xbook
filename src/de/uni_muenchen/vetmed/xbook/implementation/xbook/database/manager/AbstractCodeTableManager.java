package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.CodeTableHashMap;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.HierarchicData;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class AbstractCodeTableManager extends TableManager {

  // other stuff
  public final static String VALUE = "Value";
  public final static String ID = "ID";
  public final static String SORT = "sort";
  public final static String LANGUAGE = "Language";
  public final static String PARENTID = "ParentID";
  protected LanguageManager language;
  //public static String LANGUAGESTRING;

  /**
   * Constructor.
   */
  public AbstractCodeTableManager(LanguageManager language, Connection connection,
      String databaseName) {
    super(connection, databaseName);
    this.language = language;
//        try {
//            LANGUAGESTRING = "( " + LANGUAGE + "=" + language.getCurrentLanguageID() + " OR "
//                    + LANGUAGE + "= 0 )";
//        } catch (Exception ex) {
////            Logger.getLogger(AbstractCodeTableManager.class.getName()).log(Level.SEVERE, null, ex);
//        }
  }


  /**
   * Returns all possible entrys that have the given id as parent for the given table name.
   *
   * @param tableName The table name in which the entrys are searched
   * @param id The id of the parent entry
   * @return The list of possible Entrys
   */
  public ArrayList<DataColumn> getMultiComboBoxData(String tableName, String id,
      ColumnType.SortedBy sortedBy, boolean isLanguage) throws StatementNotExecutedException {
    ArrayList<DataColumn> data = new ArrayList<>();
    String languageString = isLanguage
        ? LanguageManager.getLanguageQuery(tableName) + " AND "
        //("(" + LANGUAGE + "=" + language.getCurrentLanguageID() + " OR " + LANGUAGE + "= 0 ) " + " AND ")
        : ("");
    String query = "SELECT " + ID + "," + VALUE + " FROM " + tableName
        + " WHERE  " + languageString + PARENTID + "=" + id + " AND " + tableName + "."
        + IStandardColumnTypes.DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " + (
        sortedBy == ColumnType.SortedBy.ALPHABETICAL ? " order by " + VALUE
            : (sortedBy == ColumnType.SortedBy.ID ? " order by " + ID : ""));
    try {
      ResultSet rs = executeSelect(query);
      while (rs.next()) {
        data.add(new DataColumn(rs.getString(VALUE), rs.getString(ID)));
      }
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query, ex);
    }

    return data;
  }

  /**
   * Returns the list how the given multicombo entry can be build in reversed order
   *
   * @param tableName The name of the table to get the entry from
   * @param id the initial id
   * @return The list of entry ids in the revered order
   */
  public ArrayList<String> getMultiboxComboParents(String tableName, String id)
      throws StatementNotExecutedException {
    ArrayList<String> data = new ArrayList<>();
    String currentID = id;
    String query = "";
    try {
      if (id.equals("-1")) {
        return data;
      }
      while (!(currentID.equals("0"))) {
        data.add(currentID + "");
        query = "SELECT " + PARENTID
            + " FROM " + tableName
            + " WHERE " + ID + "=" + currentID
            + " AND " + IStandardColumnTypes.DELETED + "='" + IStandardColumnTypes.DELETED_NO + "'";
        ResultSet rs = executeSelect(query);
        if (rs.next()) {
          currentID = rs.getString(PARENTID);
          continue;
        }
        throw new StatementNotExecutedException(
            "No entry found for id " + currentID + "\n" + query);
      }
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query, ex);
    }

    return data;
  }

  /**
   * Returns all entries for the table defined for the given column
   *
   * @param column the column for which the predefined values shall be loaded
   * @return The list of entries valid for the given column
   */
  public CodeTableHashMap getAllEntrys(ColumnType column) throws StatementNotExecutedException {
    String languageQuery = "";
    if (column.isLanguage()) {
//            languageQuery = "(" + LANGUAGE + "=" + language.getCurrentLanguageID() + " OR "
//                    + LANGUAGE + "= 0) AND ";
      languageQuery = LanguageManager.getLanguageQuery(column.getConnectedTableName()) + " AND ";
    }
    String query = "SELECT " + ID + "," + VALUE
        + " FROM " + databaseName + "." + column.getConnectedTableName()
        + " WHERE " + languageQuery + IStandardColumnTypes.DELETED + "='"
        + IStandardColumnTypes.DELETED_NO + "' ORDER BY " + (column.isSort() ? (SORT + " ASC;")
        : VALUE) + ";";
    CodeTableHashMap list = new CodeTableHashMap();
    try {
      ResultSet rs = executeSelect(query);
      while (rs.next()) {
        list.put(rs.getString(ID), rs.getString(VALUE));
      }
      return list;
    } catch (SQLException e) {
      throw new StatementNotExecutedException(column + ": " + query, e);
    }
  }

  /**
   * Returns all entries for the table defined for the given column
   *
   * @param column the column for which the predefined values shall be loaded
   * @return The list of entries valid for the given column
   */
  public CodeTableHashMap getAllEntrysForValue(ColumnType column)
      throws StatementNotExecutedException {
    String languageQuery = "";
    if (column.isLanguage()) {
//            languageQuery = "(" + LANGUAGE + "=" + language.getCurrentLanguageID() + " OR "
//                    + LANGUAGE + "= 0) AND ";
      languageQuery = LanguageManager.getLanguageQuery(column.getConnectedTableName()) + " AND ";
    }
    String query = "SELECT " + ID + "," + VALUE
        + " FROM " + databaseName + "." + column.getConnectedTableName()
        + " WHERE " + languageQuery + IStandardColumnTypes.DELETED + "='"
        + IStandardColumnTypes.DELETED_NO + "' ORDER BY " + (column.isSort() ? (SORT + " ASC;")
        : VALUE) + ";";
    CodeTableHashMap list = new CodeTableHashMap();
    try {
      ResultSet rs = executeSelect(query);
      while (rs.next()) {
        list.put(rs.getString(VALUE), rs.getString(ID));
      }
      return list;
    } catch (SQLException e) {
      throw new StatementNotExecutedException(column + ": " + query, e);
    }
  }

  public HashMap<String, ArrayList<DataColumn>> getHashedMultiComboBoxData(ColumnType type)
      throws StatementNotExecutedException {
    return getHashedMultiComboBoxData(type.getConnectedTableName());
  }

  public HashMap<String, ArrayList<DataColumn>> getHashedMultiComboBoxData(String tableName)
      throws StatementNotExecutedException {
    HashMap<String, ArrayList<DataColumn>> data = new HashMap<>();
    String query = "SELECT " + ID + "," + VALUE + "," + PARENTID + " FROM " + tableName
        + " WHERE ( "
        + LanguageManager.getLanguageQuery(tableName)
        //                + LANGUAGE + "=" + language.getCurrentLanguageID() + " OR " + LANGUAGE + "= 0"
        + " ) AND " + DELETED + "='" + DELETED_NO + "'";
    try {
      xResultSet rs = executeSelect(query);

      while (rs.next()) {
        String parentID = rs.getString(PARENTID);
        String id = rs.getString(ID);
        String value = rs.getString(VALUE);
        ArrayList<DataColumn> list = data.get(parentID);
        if (list == null) {
          list = new ArrayList<>();
          data.put(parentID, list);
        }
        list.add(new DataColumn(value, id));
      }
      return data;
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query, ex);
    }
  }

  /**
   * Returns entrys already entered for the given column
   *
   * @param columnName The column from which holds the information to be geathered
   * @return entrys already entered for the given porject and column name
   */
  public UniqueArrayList<String> getInformation(ColumnType columnName, Key project)
      throws StatementNotExecutedException {
    return getInformation(columnName.getColumnName(), project);
  }

  /**
   * Returns entrys already entered for the given column
   *
   * @param columnName The column from which holds the information to be geathered
   * @return entrys already entered for the given porject and column name
   */
  public UniqueArrayList<String> getInformation(String columnName, Key project)
      throws StatementNotExecutedException {
    UniqueArrayList<String> list = new UniqueArrayList<>();
    String tableName = ColumnHelper.getTableName(columnName);
    if (tableName.equals("")) {
      tableName = AbstractInputUnitManager.TABLENAME_INPUT_UNIT;
    }
    String query = "SELECT " + columnName + " FROM " + tableName
        + " WHERE " + AbstractBaseEntryManager.PROJECT_ID + "=" + project.getID() + " AND "
        + AbstractBaseEntryManager.PROJECT_DATABASE_ID + "=" + project.getDBID()
        + " AND " + IStandardColumnTypes.DELETED + "='" + IStandardColumnTypes.DELETED_NO
        + "' GROUP BY " + columnName + ";";
    try {
      ResultSet rs = executeSelect(query);
      while (rs.next()) {
        if (rs.getString(columnName) != null && !rs.getString(columnName).equals("")) {
          list.add(rs.getString(columnName));
        }
      }
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }
    return list;
  }

  public String getValueForID(ColumnType column, String id)
      throws StatementNotExecutedException, EntriesException {
    StringBuilder query = new StringBuilder("SELECT ").append(VALUE).append(" FROM ")
        .append(column.getConnectedTableName()).append(" WHERE ")
        .append(ID).append("=").append(id);

    if (column.isLanguage()) {
      query.append(" AND ")
          .append(LanguageManager.getLanguageQuery(column.getConnectedTableName()));

//                    append("( " + LANGUAGE + "=").append(language.getCurrentLanguageID()).append(" OR " + LANGUAGE
//                    + "= 0 ) ");
    }
    try {
      xResultSet rs = executeSelect(query.toString());
      if (rs.next()) {
        return rs.getString(VALUE);
      }

    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query.toString(), ex);
    }
    throw new EntriesException("Entry not found for " + id);
  }

  public ArrayList<ArrayList<String>> getTableContents(String tablename) {
    String query = "SELECT * FROM " + databaseName + "." + tablename;
    ArrayList<ArrayList<String>> returnList = new ArrayList<>();
    try {
      ResultSet rs = executeSelect(query);
      ResultSetMetaData meta = rs.getMetaData();
      while (rs.next()) {
        ArrayList<String> row = new ArrayList<>();
        for (int i = 1; i <= meta.getColumnCount(); i++) {
          row.add(rs.getString(i));
        }
        returnList.add(row);

      }

    } catch (SQLException ex) {
      Logger.getLogger(AbstractCodeTableManager.class.getName()).log(Level.SEVERE, null, ex);
    }
    return returnList;
  }

  public HashMap<String, HierarchicData> getHierarchicData(ColumnType multiTable) {
    String hierarchic = ""
        + "SELECT " + AbstractCodeTableManager.ID + ", " + AbstractCodeTableManager.VALUE + ", "
        + AbstractCodeTableManager.PARENTID + " "
        + "FROM " + multiTable.getConnectedTableName() + " "
        + "WHERE " + DELETED + "='" + DELETED_NO + "' ";
    if (multiTable.isLanguage()) {
      hierarchic += "AND " + LanguageManager.getLanguageQuery(multiTable.getConnectedTableName());
    }
    xResultSet rsMulti;
    HashMap<String, HierarchicData> hd = new HashMap<>();
    // retrieve all hierarchic data from database
    try {
      rsMulti = executeSelect(hierarchic);
      while (rsMulti.next()) {
        hd.put(rsMulti.getString(AbstractCodeTableManager.ID),
            new HierarchicData(rsMulti.getString(AbstractCodeTableManager.VALUE),
                rsMulti.getString(AbstractCodeTableManager.PARENTID)));
      }
    } catch (SQLException ex) {
      Logger.getLogger(AbstractSynchronisationManager.class.getName()).log(Level.SEVERE, null, ex);
    }
    return hd;
  }

  /**
   * Check the database which values are still saved in the database even if their ID (that means
   * the row) has been deleted, and therefore is not available anymore.
   *
   * @param projectKey The Project Key of the entry to check.
   * @param columnType The ColumnType to check.
   * @return A hashmap of values as strings, mapped on a list of Entry Keys of the entries that have
   * been deleted corresponding for this entry.
   */
  public HashMap<String, ArrayList<Key>> getEntriesUsingDeletedValues(
      Key projectKey, ColumnType columnType) throws StatementNotExecutedException {
    final HashMap<String, ArrayList<Key>> wrongValues = new HashMap<>();
    final String tableName = ColumnHelper.getTableName(columnType.getColumnName());
    final String connectedTableName = columnType.getConnectedTableName();
    String query =
        "SELECT " + tableName + "." + ID + ", " + tableName + "." + DATABASE_ID + "," + VALUE
            + " FROM " + tableName
            + " JOIN " + connectedTableName
            + " ON " + connectedTableName + "." + ID + "=" + columnType
            + " WHERE " + connectedTableName + "." + DELETED + "='" + DELETED_YES + "'"
            + " AND " + PROJECT_ID + "=" + projectKey.getID()
            + " AND " + PROJECT_DATABASE_ID + "=" + projectKey.getDBID()
            + " AND " + tableName + "." + DELETED + "='" + DELETED_NO + "'" + ";";

    try {
      final xResultSet rs = executeSelect(query);
      while (rs.next()) {
        Key key = new Key(rs.getInt(ID), rs.getInt(DATABASE_ID));
        String value = rs.getString(VALUE);
        ArrayList<Key> keys = wrongValues.get(value);
        if (keys == null) {
          keys = new UniqueArrayList<>();
          wrongValues.put(value, keys);
        }
        keys.add(key);
      }
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }

    return wrongValues;
  }

  /**
   * Check the database if the specific value of a column key is still valid in the database or if
   * its value (that means the row) has been deleted, and therefore is not available anymore.
   *
   * @param projectKey The Project Key of the entry to check.
   * @param entryKey The Entry Key of the entry to check.
   * @param columnType The ColumnType to check.
   * @return A list of values as a string that have been deleted corresponding for this entry..
   */
  public ArrayList<String> getListOfDeletedValues(
      Key projectKey, Key entryKey, ColumnType columnType) throws StatementNotExecutedException {
    final ArrayList<String> wrongValues = new ArrayList<>();
    final String tableName = ColumnHelper.getTableName(columnType.getColumnName());
    final String connectedTableName = columnType.getConnectedTableName();
    String query =
        "SELECT DISTINCT" + VALUE
            + " FROM " + tableName
            + " JOIN " + connectedTableName
            + " ON " + connectedTableName + "." + ID + "=" + columnType
            + " WHERE " + connectedTableName + "." + DELETED + "='" + DELETED_YES + "'"
            + " AND " + tableName + "." + ID + "=" + entryKey.getID()
            + " AND " + tableName + "." + DATABASE_ID + "=" + entryKey.getDBID()
            + " AND " + PROJECT_ID + "=" + projectKey.getID()
            + " AND " + PROJECT_DATABASE_ID + "=" + projectKey.getDBID()
            + " AND " + tableName + "." + DELETED + "='" + DELETED_NO + "'" + ";";

    try {
      final xResultSet rs = executeSelect(query);
      while (rs.next()) {
        String value = rs.getString(VALUE);

        wrongValues.add(value);
      }
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }

    return wrongValues;
  }

  public boolean isIDValid(ColumnType columnType, String idToCheck)
      throws StatementNotExecutedException {
    final String connectedTableName = columnType.getConnectedTableName();
    String query =
        "SELECT 1 "
            + " FROM " + connectedTableName
            + " WHERE " + connectedTableName + "." + ID + "='" + idToCheck + "'"
            + " AND " + DELETED + "='" + DELETED_NO + "';";

    try {
      final xResultSet xResultSet = executeSelect(query);
      return (xResultSet.next());

    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }
  }
}
