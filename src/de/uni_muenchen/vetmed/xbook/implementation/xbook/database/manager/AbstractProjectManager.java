package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardProjectColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.event.ProjectEvent;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class AbstractProjectManager extends AbstractBaseProjectEntryManager implements IStandardProjectColumnTypes {


    public static final String PROJECT_HIDDEN = TABLENAME_PROJECT + ".Hidden";
    protected UserManager userManager;

    protected boolean isHiddable = false;

    public AbstractProjectManager(String tableName, String localisedTableName, int dbNumber, Connection connection, String databaseName, UserManager user, AbstractExtendedProjectEntryManager... manager) {
        super(tableName, localisedTableName, dbNumber, connection, databaseName, manager);
        this.userManager = user;
//        dataColumns = new ColumnTypeList(tableName);
//        dataColumns.add(PROJECT_ID_PROJECT);
//        dataColumns.add(PROJECT_DATABASENUMBER);
        dataColumns.add(PROJECT_PROJECTNAME);
        dataColumns.add(PROJECT_PROJECTOWNER);

    }

    public boolean projectExists(ProjectDataSet project)
            throws StatementNotExecutedException {
        return projectExists(project, false);
    }

    /**
     * check project already exists in database
     *
     * @param project
     * @return
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException
     */
    public boolean projectExists(ProjectDataSet project, boolean all)
            throws StatementNotExecutedException {

        String condition = "";
        if (!all) {
            condition = " AND " + DELETED + "= '" + IStandardColumnTypes.DELETED_NO + "'";
        }
        String query = "SELECT COUNT(*) FROM " + databaseName
                + "." + TABLENAME_PROJECT + " WHERE " + PROJECT_ID_PROJECT + "=" + project.getProjectId()
                + " AND " + PROJECT_DATABASENUMBER + "=" + project.getProjectDatabaseId() + condition + ";";
        ResultSet rs;
        try {
            rs = executeSelect(query);
            if (rs.next()) {
                int count = rs.getInt(1);
                return count > 0;
            }
            return true;
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }
//

    /* (non-Javadoc)
     * @see ossobook2010.database.AbstractProjectManager#getProjects()
     */
    public void setProjectHidden(Key projectKey, boolean hide) throws StatementNotExecutedException {
        String query = "UPDATE " + TABLENAME_PROJECT + " SET " + PROJECT_HIDDEN + "=" + (hide ? 1 : 0) + " WHERE " + PROJECT_ID_PROJECT + "=" + projectKey.getID()
                + " AND " + PROJECT_DATABASE_ID + "=" + projectKey.getDBID();
        try {
            executeQuery(query);
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    public ArrayList<ProjectDataSet> getProjects() throws StatementNotExecutedException {
        return getProjects(true);
    }

    public ProjectDataSet getProject(Key projectKey) throws StatementNotExecutedException {

        String query = "SELECT " + PROJECT_ID_PROJECT.getColumnName() + ", " + PROJECT_DATABASENUMBER + ", " + PROJECT_PROJECTNAME + ", " + PROJECT_PROJECTOWNER + (isHiddable ? ", " + PROJECT_HIDDEN : "")
                + " FROM " + databaseName + "." + TABLENAME_PROJECT
                + " WHERE " + PROJECT_ID_PROJECT + "=" + projectKey.getID()
                + " AND " + PROJECT_DATABASENUMBER + "=" + projectKey.getDBID()
                + " AND " + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "'" + getRightQuery();
        try {
            xResultSet rs = executeSelect(query);
            if (rs.next()) {
                int id = rs.getInt(PROJECT_ID_PROJECT.getColumnName());
                int dbNumber = rs.getInt(PROJECT_DATABASENUMBER.getColumnName());

                ProjectDataSet data = getEmptyProject(id, dbNumber, rs);
                load(data);
                return data;
            }

            return null;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

    public ArrayList<ProjectDataSet> getProjects(boolean showHidden) throws StatementNotExecutedException {
        ArrayList<ProjectDataSet> list = new ArrayList<>();
        String hidden = "";
        if (!showHidden) {
            hidden = " AND " + PROJECT_HIDDEN + "= 0 ";
        }
        String query = "SELECT " + PROJECT_ID_PROJECT.getColumnName() + ", " + PROJECT_DATABASENUMBER + ", " + PROJECT_PROJECTNAME + ", " + PROJECT_PROJECTOWNER + (isHiddable ? ", " + PROJECT_HIDDEN : "")
                + " FROM " + databaseName + "." + TABLENAME_PROJECT
                + " WHERE " + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "'" + hidden + getRightQuery()
                +" ORDER BY "+PROJECT_PROJECTNAME;
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                int id = rs.getInt(PROJECT_ID_PROJECT.getColumnName());
                int dbNumber = rs.getInt(PROJECT_DATABASENUMBER.getColumnName());

                ProjectDataSet data = getEmptyProject(id, dbNumber, rs);
                load(data);
                list.add(data);
            }

            return list;
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    protected ProjectDataSet getEmptyProject(int id, int dbNumber, xResultSet rs) throws SQLException {
        return new ProjectDataSet(new Key(id, dbNumber), databaseName, rs.getString(PROJECT_PROJECTNAME), rs.getInt(PROJECT_PROJECTOWNER), (isHiddable ? rs.getBoolean(PROJECT_HIDDEN) : false));
    }

    /**
     * @param project
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException
     */
    @Override
    public void deletePermanent(ProjectDataSet project, int event)
            throws StatementNotExecutedException {
        for (AbstractExtendedProjectEntryManager manager : managers) {
            manager.deletePermanent(project, event);
        }
        ArrayList<DataColumn> data = new ArrayList<>();
        data.add(new DataColumn(IStandardColumnTypes.DELETED_LOCAL, DELETED));
        data.add(new DataColumn(0, MESSAGE_NUMBER));

        ArrayList<DataColumn> key = new ArrayList<>();
        key.add(new DataColumn(project.getProjectId(), PROJECT_ID_PROJECT));
        key.add(new DataColumn(project.getProjectDatabaseId(), PROJECT_DATABASE_ID));
        if (event == AbstractQueryManager.DELETE_DELETED) {

            key.add(new DataColumn(IStandardColumnTypes.DELETED_YES, DELETED));
            key.add(new DataColumn(0, MESSAGE_NUMBER));
        }
        updateDataWithKey(data, databaseName + "." + tableName, key);

    }

    protected String getRightQuery() throws StatementNotExecutedException {
        String query = " AND (" + PROJECT_PROJECTOWNER + "="
                + userManager.getCurrentUserId() + " OR  "
                + " (SELECT " + ProjectRightManager.PROJECT_RIGHTS_READ + "" + " FROM " + ProjectRightManager.TABLENAME_PROJECTRIGHT
                + " WHERE " + ProjectRightManager.PROJECT_RIGHT_PROJECT_ID + "=" + PROJECT_ID_PROJECT
                + " AND " + ProjectRightManager.PROJECT_RIGHT_PROJECT_DATABASENUMBER + "=" + PROJECT_DATABASENUMBER
                + " AND " + ProjectRightManager.PROJECTRIGHT_USERID + " = " + userManager.getCurrentUserId()
                + " AND " + ProjectRightManager.TABLENAME_PROJECTRIGHT + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' ) = 1"
                + " OR " + " EXISTS ( SELECT * "
                + " FROM " + GroupManager.TABLENAME_GROUP_RIGHTS + "," + GroupManager.TABLENAME_GROUP_USER
                + "," + GroupManager.TABLENAME_PROJECT_RIGHT_GROUP
                + " WHERE " + GroupManager.GROUP_RIGHTS_GROUP_ID + " = " + GroupManager.PROJECT_RIGHT_GROUP_GROUP_ID
                + " AND " + GroupManager.PROJECT_RIGHT_GROUP_PROJECT_ID + "=" + PROJECT_ID_PROJECT
                + " AND " + GroupManager.PROJECT_RIGHT_GROUP_PROJECT_DATABASE_NUMBER + "=" + PROJECT_DATABASENUMBER
                + " AND " + GroupManager.GROUP_USER_GROUP_ID + " = " + GroupManager.GROUP_RIGHTS_GROUP_ID
                + " AND " + GroupManager.GROUP_USER_RANK + " = " + GroupManager.GROUP_RIGHTS_RANK
                + " AND " + GroupManager.GROUP_USER_USER_ID + " = " + userManager.getCurrentUserId()
                + " AND " + GroupManager.TABLENAME_GROUP_USER + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + GroupManager.TABLENAME_GROUP_RIGHTS + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' "
                + " AND " + GroupManager.TABLENAME_PROJECT_RIGHT_GROUP + "." + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "' " +
                " AND (" + GroupManager.GROUP_RIGHTS_READ + " AND " + GroupManager.PROJECT_RIGHT_GROUP_READ + ") = 1)"
                + ")";

        return query;
    }
//


    /* (non-Javadoc)
     * @see ossobook2010.database.AbstractProjectManager#newProject(java.lang.String)
     */
    private ProjectDataSet newProject(ProjectDataSet project) throws StatementNotExecutedException {
        String query = "(SELECT COALESCE(pid+1,1) as temp FROM (SELECT MAX(" + PROJECT_ID_PROJECT + ") as pid FROM " + TABLENAME_PROJECT + " WHERE " + PROJECT_DATABASENUMBER + "=" + dbId + ") as temp )";

        int id;
        try {
            xResultSet rs = executeSelect(query);
            rs.next();

            id = rs.getInt("temp");
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
//        ArrayList<DataColumn> data = new ArrayList<>();
        DataRow data = new DataRow(tableName);
        data.add(new DataColumn(id, PROJECT_ID_PROJECT));
        data.add(new DataColumn(dbId, PROJECT_DATABASENUMBER));
        project.setProjectKey(new Key(id, dbId));
        data.add(new DataColumn(userManager.getCurrentUserId(), PROJECT_PROJECTOWNER));
        project.setProjectOwnerId(userManager.getCurrentUserId());
        DataTableOld table = project.getOrCreateDataTable(tableName);
        for (DataRow dataRow : table) {
            for (DataColumn entry : dataRow.iterator()) {
                data.add(entry);
            }
            dataRow.add(new DataColumn(id, PROJECT_ID_PROJECT));
            dataRow.add(new DataColumn(dbId, PROJECT_DATABASENUMBER));
            dataRow.add(new DataColumn(userManager.getCurrentUserId(), PROJECT_PROJECTOWNER));
        }

        data.add(new DataColumn(1, MESSAGE_NUMBER));
        data.add(new DataColumn(IStandardColumnTypes.DELETED_NO, PROJECT_DELETED));

        insertData(data);

        return project;
    }

    //
    /* (non-Javadoc)
     * @see ossobook2010.database.AbstractProjectManager#getProjectName(ossobook2010.helpers.metainfo.AbstractProjectOverviewScreen)
     */
    public String getProjectName(DataSetOld project)
            throws StatementNotExecutedException {

        String query = "SELECT " + PROJECT_PROJECTNAME + " FROM " + databaseName
                + "." + TABLENAME_PROJECT + " WHERE " + PROJECT_ID_PROJECT + "=" + project.getProjectKey().getID()
                + " AND " + PROJECT_DATABASENUMBER + "=" + project.getProjectKey().getDBID() + ";";
        try {
            ResultSet rs = executeSelect(query);
            rs.next();
            return rs.getString(1);
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    public ArrayList<ProjectDataSet> getProjectSearchResult(ArrayList<String> tableNames, String where) throws StatementNotExecutedException {
        String from = ", ";
        for (String tableName : tableNames) {
            from += databaseName + "." + tableName + ", ";
        }
        from = from.substring(0, from.length() - 2);

        String query
                = "SELECT DISTINCT " + PROJECT_ID_PROJECT + ", " + PROJECT_DATABASENUMBER + ", "
                + PROJECT_PROJECTNAME + ", " + PROJECT_PROJECTOWNER + (isHiddable ? (", " + PROJECT_HIDDEN) : "")
                + " FROM " + databaseName + "." + TABLENAME_PROJECT
                + from
                + where;

        ArrayList<ProjectDataSet> list = new ArrayList<>();
        try {
            System.out.println(query);
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                int id = rs.getInt(PROJECT_ID_PROJECT.getColumnName());
                int dbNumber = rs.getInt(PROJECT_DATABASENUMBER.getColumnName());
                // String projectName = rs.getString(PROJECT_PROJECTNAME.getColumnName());
                // String projectOwner = rs.getString(PROJECT_PROJECTOWNER.getColumnName());

                ProjectDataSet data = getEmptyProject(id, dbNumber, rs);
                load(data);
                list.add(data);
            }
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
        return list;
    }

    public void load(DataSetOld data) throws StatementNotExecutedException {
        super.load(data);
        for (AbstractExtendedProjectEntryManager table : managers) {

            table.load(data);
        }
    }

    @Override
    public void save(DataSetOld entry) throws StatementNotExecutedException {
        if (entry.getProjectKey() == null || !entry.getProjectKey().isValidKey()) {//new
            newProject((ProjectDataSet) entry);
            projectAdded(new ProjectEvent(entry));
        } else {
            super.save(entry);
            projectUpdated(new ProjectEvent(entry));
        }
        for (AbstractExtendedProjectEntryManager table : managers) {
            table.save(entry);
        }
    }

    /**
     * set last time of synchronization for project after synchronization of
     * project is finished
     *
     * @param time
     * @param project
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException
     */
    public void setLastSynchronizeTime(String time, DataSetOld project)
            throws StatementNotExecutedException {
        ArrayList<DataColumn> data = new ArrayList<>();
        data.add(new DataColumn(time, PROJECT_LAST_SYNCRONISATION));
        ArrayList<DataColumn> key = new ArrayList<>();
        data.add(new DataColumn(project.getProjectKey().getID(), PROJECT_ID_PROJECT));
        data.add(new DataColumn(project.getProjectKey().getDBID(), PROJECT_DATABASENUMBER));
        updateDataWithKey(data, databaseName + "." + TABLENAME_PROJECT, key);
    }

    /**
     * Returns the last synchronisation time for the current table
     *
     * @return
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException
     */
    @Override
    public String getLastSynchronisation(Key projectKey) throws StatementNotExecutedException {
        String query = "SELECT MAX(" + STATUS + ") FROM " + tableName
                + " WHERE " + PROJECT_ID_PROJECT + "=" + projectKey.getID()
                + " AND " + PROJECT_DATABASENUMBER + "=" + projectKey.getDBID();
        try {
            ResultSet rs = executeSelect(query);

            if (rs.next()) {
                String max = rs.getString(1);
                return (max != null) ? max : "0";
            }
            return "0";
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    /* public DataSetOld getEntry(Key projectKey) throws StatementNotExecutedException {
     return getNextUncommitedEntry(projectKey);
     }
     */

    /**
     * translate last time of synchronization of project
     *
     * @param project
     * @return
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException
     */
    public String getLastSynchronization(DataSetOld project)
            throws StatementNotExecutedException {
        String query = "SELECT " + PROJECT_LAST_SYNCRONISATION + " FROM " + databaseName
                + "." + TABLENAME_PROJECT + " WHERE " + PROJECT_ID_PROJECT + "=" + project.getProjectKey().getID()
                + " AND " + PROJECT_DATABASENUMBER + "=" + project.getProjectKey().getDBID() + ";";
        try {
            ResultSet rs = executeSelect(query);
            if (rs.next()) {
                return rs.getString(1);
            }
            return null;
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    public String getProjectName(Key projectKey) throws StatementNotExecutedException, EntriesException {
        String query = "SELECT " + PROJECT_PROJECTNAME + " FROM " + databaseName
                + "." + TABLENAME_PROJECT + " WHERE " + PROJECT_ID_PROJECT + "=" + projectKey.getID()
                + " AND " + PROJECT_DATABASENUMBER + "=" + projectKey.getDBID() + ";";

        ResultSet rs;
        try {
            rs = executeSelect(query);
            if (rs.next()) {
                return rs.getString(PROJECT_PROJECTNAME.getColumnName());
            }
            throw new EntriesException(query);
        } catch (SQLException ex) {
            Logger.getLogger(AbstractProjectManager.class.getName()).log(Level.SEVERE, null, ex);
            throw new StatementNotExecutedException(query);
        }

    }

    public void setActive(DataSetOld project) throws StatementNotExecutedException {
        ArrayList<DataColumn> data = new ArrayList<>();
        data.add(new DataColumn(IStandardColumnTypes.DELETED_NO, DELETED));
        ArrayList<DataColumn> key = new ArrayList<>();
        key.add(new DataColumn(project.getProjectKey().getID(), PROJECT_ID_PROJECT));
        key.add(new DataColumn(project.getProjectKey().getDBID(), PROJECT_DATABASENUMBER));
        updateDataWithKey(data, tableName, key);
    }

    @Override
    public boolean isSynchronised(DataSetOld entry) throws StatementNotExecutedException {
        String query = "SELECT " + MESSAGE_NUMBER + " FROM " + databaseName + "." + tableName
                + " WHERE " + PROJECT_ID_PROJECT + "=" + entry.getProjectKey().getID()
                + " AND " + PROJECT_DATABASE_ID + "=" + entry.getProjectKey().getDBID()
                + " AND " + MESSAGE_NUMBER + "!=0";
        try {
            ResultSet rs = executeSelect(query);
            return !rs.next();
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

    public int getProjectOwner(Key projectKey) throws StatementNotExecutedException, EntriesException {
        String query = "SELECT " + PROJECT_PROJECTOWNER + " FROM " + databaseName + "." + tableName
                + " WHERE " + PROJECT_ID_PROJECT + "=" + projectKey.getID()
                + " AND " + PROJECT_DATABASE_ID + "=" + projectKey.getDBID();
        try {
            xResultSet rs = executeSelect(query);
            if (rs.next()) {
                return rs.getInt(PROJECT_PROJECTOWNER);
            }
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
        throw new EntriesException(Loc.get("ERROR_MESSAGE>ENTRY_NOT_FOUND"));
    }

    public int getNumberOfHiddenProjects() throws StatementNotExecutedException, EntriesException {
        String query = "SELECT COUNT(*)"
                + " FROM " + databaseName + "." + tableName
                + " WHERE " + PROJECT_HIDDEN + " = 1"
                + " AND " + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "'";

        try {
            xResultSet rs = executeSelect(query);
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
        throw new EntriesException(Loc.get("ERROR_MESSAGE>ENTRY_NOT_FOUND"));
    }

    /**
     * Returns a list of entries to be displayed
     *
     * @param offset
     * @param entriesPerPage
     * @return
     * @throws StatementNotExecutedException
     */
    public ExportResult getEntryData(int offset, int entriesPerPage) throws StatementNotExecutedException {
        return getEntryData(offset, entriesPerPage, null);
    }

    public ExportResult getEntryData(int offset, int entriesPerPage, List<Key> projectKeys) throws StatementNotExecutedException {
        ExportResult result = new ExportResult();
        if (projectKeys == null)
            projectKeys = getProjectKeys();
        getEntryData(result, projectKeys,null , ColumnType.ExportType.GENERAL, false, offset, entriesPerPage, null, null, true);
        ArrayList<Key> keys = result.getProjectKeyList();
        for (AbstractExtendedProjectEntryManager abstractExtendedProjectEntryManager : getManagers()) {
            abstractExtendedProjectEntryManager.getEntryData(result, keys, null, ColumnType.ExportType.GENERAL, false, -1, -1, null, null, true);

        }
        return result;
    }

    /**
     * Returns the list of Keys for all entries, the user has the right to view.
     *
     * @return
     * @throws StatementNotExecutedException
     */
    ArrayList<Key> getProjectKeys() throws StatementNotExecutedException {
        String query = "SELECT " + PROJECT_ID_PROJECT + ", " + PROJECT_DATABASENUMBER + " FROM " + tableName + " WHERE " + DELETED + "='" + DELETED_NO + "'" + getRightQuery() + " ;";
        ArrayList<Key> keys = new ArrayList<>();
        try {
            xResultSet rs = executeSelect(query);
            while (rs.next()) {
                Key project = new Key(rs.getInt(PROJECT_ID_PROJECT), rs.getInt(PROJECT_DATABASENUMBER));
                keys.add(project);
            }
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
        return keys;
    }

    public int getNumberOfProjects() throws StatementNotExecutedException {
        return getProjectKeys().size();
    }

    /**
     * Method checks if a combination of given input values already exist in the
     * database.
     *
     * @param projectKey The Key of the current project.
     * @param toCheck    A list of DataColumns that should be checked for
     *                   uniqueness.
     * @return <code>true</code> if there already exists an entry with the given
     * data, <code>false</code> else.
     * @throws StatementNotExecutedException
     */
    public boolean combinationAlreadyExistsInDatabase(Key projectKey, ArrayList<DataColumn> toCheck) throws StatementNotExecutedException {
        String projectQuery = "";
        if (projectKey != null) {
            projectQuery = " NOT (" + PROJECT_ID + "='" + projectKey.getID() + "'"
                    + " AND " + PROJECT_DATABASE_ID + "='" + projectKey.getDBID() + "')"
                    + " AND ";
        }
        String query = "SELECT count(*) FROM " + databaseName + "." + tableName
                + " WHERE " + projectQuery + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "'";

        for (DataColumn col : toCheck) {
            query += " AND " + col.getColumnName() + "='" + col.getValue() + "'";
        }
        query += ";";

        if (XBookConfiguration.DISPLAY_SOUTS) {
            System.out.println(query);
        }

        try {
            ResultSet rs = executeSelect(query);
            if (rs.next()) {
                int count = rs.getInt("count(*)");
                return (count > 0);
            }
            return false;
        } catch (SQLException ex) {
            throw new StatementNotExecutedException(query, ex);
        }
    }

}
