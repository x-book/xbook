package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLineGroup;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.SolveConflictLineUpdateOrDelete;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 * @author Alex
 */
public abstract class AbstractExtendedEntryManager extends AbstractEntryManager {

    protected ArrayList<ColumnType> conflictableColumns;

    public AbstractExtendedEntryManager(String tableName, String localisedTableName, int dbNumber, Connection connection, String databaseName) {
        super(tableName, localisedTableName, dbNumber, connection, databaseName);
        conflictableColumns = getConflictableColumns();

    }

    protected abstract ArrayList<ColumnType> getConflictableColumns();


    @Override
    public SolveConflictLineGroup getLinesForEntry(DataRow localAll, DataRow globalRows) throws EntriesException, StatementNotExecutedException {
        String lastSync = "0";
        SolveConflictLineGroup lines = new SolveConflictLineGroup(tableName);

        String displayL = "";
        HashMap<ColumnType, String> valueL = new HashMap<>();
        String displayR = "";
        HashMap<ColumnType, String> valueR = new HashMap<>();
        boolean deletedLocal = true;
        boolean deletedGlobal = true;

        for (ColumnType column : conflictableColumns) {
            String localColumn = localAll.get(column);
            if (localColumn != null) {
                valueL.put(column, localColumn);

            }
            String globalColumn = globalRows.get(column);
            if (globalColumn != null) {

                valueR.put(column, globalColumn);
                deletedGlobal = false;
            }
        }
        if (globalRows.get(tableName + "." + STATUS) != null) {
            lastSync = globalRows.get(tableName + "." + STATUS);
        }
        if (globalRows.get(tableName + "." + DELETED) != null) {
            deletedGlobal = globalRows.get(tableName + "." + DELETED).equals(IStandardColumnTypes.DELETED_YES);
        }
        if (localAll.get(tableName + "." + DELETED) != null) {
            deletedLocal = localAll.get(tableName + "." + DELETED).equals(IStandardColumnTypes.DELETED_YES);
        }
        displayR = getConflictDisplay(valueR);
        displayL = getConflictDisplay(valueL);

        lines.setLastSync(lastSync);
        if (deletedGlobal) {
            displayR = Loc.get("DELETE");
        }
        if (deletedLocal) {
            displayL = Loc.get("DELETE");
        }
        if (deletedGlobal && deletedLocal) {
            return lines;
        }

        lines.add(new SolveConflictLineUpdateOrDelete(valueL, displayL, valueR, displayR, localisedTableName, tableName));

        return lines;

    }



}
