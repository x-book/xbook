package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnTypeList;
import de.uni_muenchen.vetmed.xbook.api.datatype.CrossLinkedEntry;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTableOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryKey;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportResult;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.SearchEntryInfo;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLineGroup;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.SolveConflictLineUpdateOrDelete;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractCrossLinkedManager extends AbstractBaseProjectEntryManager implements
    ExportableManager {

  private static final Log LOGGER = LogFactory.getLog(AbstractCrossLinkedManager.class);

  public final static String TABLE_LINKED_TO = "linked_to";
  private AbstractBaseEntryManager managerOne;
  private AbstractBaseEntryManager managerTwo;

  public ColumnType idOne;
  public ColumnType dbOne;
  public ColumnType idTwo;
  public ColumnType dbTwo;
  protected ArrayList<ColumnType> conflicted;

  /**
   * Constructor.
   */
  public AbstractCrossLinkedManager(String tableName, String localisedTableName, int dbId,
      Connection connection,
      String databaseName,
      ColumnType idOne, ColumnType dbOne, ColumnType idTwo, ColumnType dbTwo) {
    super(tableName, localisedTableName, dbId, connection, databaseName);
    dataColumns = new ColumnTypeList(tableName);
    dataColumns.add(new ColumnType(PROJECT_ID, tableName).setForListing(false));
    dataColumns.add(new ColumnType(PROJECT_DATABASE_ID, tableName).setForListing(false));
    dataColumns.add(new ColumnType(MESSAGE_NUMBER, tableName));
    dataColumns.add(new ColumnType(STATUS, tableName));
    dataColumns.add(new ColumnType(DELETED, tableName));
    dataColumns.add(idOne);
    dataColumns.add(dbOne);
    dataColumns.add(idTwo);
    dataColumns.add(dbTwo);
    primaryColumns = new ArrayList<>();
    primaryColumns.add("" + PROJECT_ID);
    primaryColumns.add("" + PROJECT_DATABASE_ID);
    primaryColumns.add((idOne.getColumnName()));
    primaryColumns.add((dbOne.getColumnName()));
    primaryColumns.add((idTwo.getColumnName()));
    primaryColumns.add((dbTwo.getColumnName()));
    this.idOne = idOne;
    this.dbOne = dbOne;
    this.idTwo = idTwo;
    this.dbTwo = dbTwo;
    conflicted = new ArrayList<>();
    conflicted.add(idOne);
    conflicted.add(dbOne);
    conflicted.add(this.idTwo);
    conflicted.add(dbTwo);
  }

  @Override
  public Collection<? extends ColumnType> getExportableColumns() {
    return new ArrayList<>();
  }

  /**
   *
   */
  protected void setManagerOne(AbstractBaseEntryManager managerOne) {
    this.managerOne = managerOne;
  }

  /**
   *
   */
  protected void setManagerTwo(AbstractBaseEntryManager managerTwo) {
    this.managerTwo = managerTwo;

    if (getManager(managerTwo.getTableName()) != managerTwo) {
      throw new RuntimeException("Wrong Order: " + managerTwo.getTableName());
    }
  }

  @Override
  public void save(DataSetOld data) throws StatementNotExecutedException {
    if (!(data instanceof EntryDataSet)) {
      return;
    }
    EntryDataSet entryData = (EntryDataSet) data;
    DataTableOld table = entryData.getOrCreateDataTable(tableName);

    String tableLinkedFrom = entryData.getBaseTableName();

//        ArrayList<DataColumn> insertOrUpdate = new ArrayList<>();
    DataRow updateData = new DataRow(tableName);
    updateData.add(
        new DataColumn(entryData.getEntryKey().getID(), getId(entryData.getBaseTableName(), true)));
    updateData.add(new DataColumn(entryData.getEntryKey().getDBID(),
        getDbid(entryData.getBaseTableName(), true)));
    updateData.add(new DataColumn(entryData.getProjectKey().getID(), PROJECT_ID));
    updateData.add(new DataColumn(entryData.getProjectKey().getDBID(), PROJECT_DATABASE_ID));
    updateData.add(new DataColumn(IStandardColumnTypes.DELETED_YES, DELETED));
    updateData.add(new DataColumn(IStandardColumnTypes.CHANGED, MESSAGE_NUMBER));
    updateData(updateData, primaryColumns);

    for (DataRow row : table) {

      String otherEntryID = row.get(getId(tableLinkedFrom, false));
      String otherEntryDB = row.get(getDbid(tableLinkedFrom, false));

//            ArrayList<DataColumn> rowData = new ArrayList<>();
      DataRow rowData = new DataRow(tableName);
      rowData.add(new DataColumn(entryData.getEntryKey().getID(), getId(tableLinkedFrom, true)));
      rowData
          .add(new DataColumn(entryData.getEntryKey().getDBID(), getDbid(tableLinkedFrom, true)));
      rowData.add(new DataColumn(otherEntryID, getId(tableLinkedFrom, false)));
      rowData.add(new DataColumn(otherEntryDB, getDbid(tableLinkedFrom, false)));
      rowData.add(new DataColumn(entryData.getProjectKey().getID(), PROJECT_ID));
      rowData.add(new DataColumn(entryData.getProjectKey().getDBID(), PROJECT_DATABASE_ID));
      rowData.add(new DataColumn(IStandardColumnTypes.DELETED_NO, DELETED));
      rowData.add(new DataColumn(IStandardColumnTypes.CHANGED, MESSAGE_NUMBER));

      if (updateData(rowData, primaryColumns) == 0) {
        insertData(rowData);
      }

    }
  }

  @Override
  public void load(DataSetOld data) throws StatementNotExecutedException {
    // do not load
  }

  /**
   *
   */
  public void updateDatabaseId(int newNumber, String firstTableName)
      throws StatementNotExecutedException {
    ArrayList<DataColumn> data = new ArrayList<>();
    data.add(new DataColumn(newNumber, PROJECT_DATABASE_ID));
    ArrayList<DataColumn> key = new ArrayList<>();
    key.add(new DataColumn(-1, PROJECT_DATABASE_ID));
    updateDataWithKey(data, key);

    data = new ArrayList<>();
    data.add(new DataColumn(newNumber, getDbid(firstTableName)));
    key = new ArrayList<>();
    key.add(new DataColumn(-1, getDbid(firstTableName)));
    updateDataWithKey(data, key);

    data = new ArrayList<>();
    data.add(new DataColumn(newNumber, getDbid(firstTableName, false)));
    key = new ArrayList<>();
    key.add(new DataColumn(-1, getDbid(firstTableName, false)));
    updateDataWithKey(data, key);
    dbId = newNumber;
  }

  /**
   * Returns the ColumnType of the linked table name. This method always returns the corresponding
   * ColumnType of the ID belonging to the table.
   *
   * @param linkedTableName The name of the linked table.
   * @return The corresponding ColumnType of the ID belonging to the table.
   */
  public ColumnType getId(String linkedTableName) {
    return getId(linkedTableName, true);
  }

  /**
   * Returns the ColumnType of the linked table name.
   *
   * @param linkedTableName The name of the linked table.
   * @param getThisLinkedTable <code>true</code> to return the corresponding ColumnType of the ID
   * belonging to the table, <code>false</code> to return the other one.
   * @return The corresponding or other ColumnType of the ID belonging to the table.
   */


  public ColumnType getId(String linkedTableName, boolean getThisLinkedTable) {
    boolean b = isFirst(linkedTableName);
    if (!getThisLinkedTable) {
      b = !b;
    }
    return b ? idOne : idTwo;
  }


  /**
   * Returns the ColumnType of the linked table name. This method always returns the corresponding
   * ColumnType of the DATABASE_ID belonging to the table.
   *
   * @param linkedTableName The name of the linked table.
   * @return The corresponding ColumnType of the DATABASE_ID belonging to the table.
   */
  public ColumnType getDbid(String linkedTableName) {
    return getDbid(linkedTableName, true);
  }

  /**
   *
   */
  public AbstractBaseEntryManager getManager(String tableToCheck, boolean getThisLinkedTable) {
    boolean b = isFirst(tableToCheck);
    if (!getThisLinkedTable) {
      b = !b;
    }
    return b ? managerOne : managerTwo;
  }

  /**
   *
   */
  protected AbstractBaseEntryManager getManager(String tableToCheck) {
    return getManager(tableToCheck, true);
  }

  /**
   * Returns the ColumnType of the linked table name.
   *
   * @param linkedTableName The name of the linked table.
   * @param getThisLinkedTable <code>true</code> to return the corresponding ColumnType of the
   * DATABASE_ID belonging to the table, <code>false</code> to return the other one.
   * @return The corresponding or other ColumnType of the DATABASE_ID belonging to the table.
   */
  public ColumnType getDbid(String linkedTableName, boolean getThisLinkedTable) {
    boolean b = isFirst(linkedTableName);
    if (!getThisLinkedTable) {
      b = !b;
    }
    return b ? dbOne : dbTwo;
  }

  /**
   * Loads all entries of the table specified to load. The entryKey is the entry of the other table
   */
  public ArrayList<Key> load(Key projectKey, Key entryKey,
      String requestingTableName) throws StatementNotExecutedException {

    ArrayList<Key> list = new ArrayList<>();
    String query = "SELECT " + getId(requestingTableName) + "," + getDbid(requestingTableName) +
        " FROM " + tableName +
        " WHERE " +
        getId(requestingTableName, false) + "=" + entryKey.getID() +
        " AND " + getDbid(requestingTableName, false) + "=" + entryKey.getDBID() +
        " AND " + PROJECT_ID + "=" + projectKey.getID() +
        " AND " + PROJECT_DATABASE_ID + "=" + projectKey.getDBID() +
        " AND " + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "'";

    if (XBookConfiguration.DISPLAY_SOUTS) {
      LOGGER.info(query);
    }
    try {
      xResultSet rs = executeSelect(query);
      while (rs.next()) {
        list.add(new Key(rs.getInt(getId(requestingTableName)),
            rs.getInt(getDbid(requestingTableName))));
      }
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query, ex);
    }
    return list;
  }

  /**
   *
   */
  public void deletePermanent(Key project, Key entry, int event, String requestingTableName)
      throws StatementNotExecutedException {
    String condition = "";
    if (event == AbstractQueryManager.DELETE_DELETED) {
      condition = " AND " + DELETED + "='" + IStandardColumnTypes.DELETED_YES + "' AND "
          + MESSAGE_NUMBER + "=" + -1;
    }
    String query = "DELETE  FROM " + databaseName + "." + tableName
        + " WHERE " + PROJECT_ID + "='" + project.getID()
        + "' AND " + PROJECT_DATABASE_ID + "=" + project.getDBID()
        + " AND " + getId(requestingTableName) + "=" + entry.getID()
        + " AND " + getDbid(requestingTableName) + "=" + entry.getDBID()
        + condition + ";";
    try {
      executeQuery(query);
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }
  }

  /**
   *
   */
  public boolean isFirst(String tableName) {
    return tableName.equals(managerOne.tableName);

  }

  /**
   *
   */
  public void deleteCrossLinksForEntry(Key entryKey, Key projectKey,
      BaseEntryManager manager) throws StatementNotExecutedException {
    deleteCrossLinksForEntry(entryKey, projectKey, manager.getTableName());
  }

  //
  public void deleteCrossLinksForEntry(Key entryKey, Key projectKey,
      String requestingTableName) throws StatementNotExecutedException {
//        ArrayList<DataColumn> data = new ArrayList<>();
    ArrayList<String> key = new ArrayList<>();
    DataRow data = new DataRow(tableName);
    data.add(new DataColumn(IStandardColumnTypes.DELETED_YES, DELETED));
    data.add(new DataColumn(1, MESSAGE_NUMBER));
    data.add(new DataColumn(entryKey.getID(), getId(requestingTableName)));
    data.add(new DataColumn(entryKey.getDBID(), getDbid(requestingTableName)));
    data.add(new DataColumn(projectKey.getID(), PROJECT_ID));
    data.add(new DataColumn(projectKey.getDBID(), PROJECT_DATABASE_ID));
    key.add(getId(requestingTableName).getColumnName());
    key.add(getDbid(requestingTableName).getColumnName());
    key.add(PROJECT_ID.getColumnName());
    key.add(PROJECT_DATABASE_ID.getColumnName());
    updateData(data, key);
  }

  public void deleteEntry(Key entryKey, Key mappedKey, Key projectKey,
      BaseEntryManager manager) throws StatementNotExecutedException {
    deleteEntry(entryKey, mappedKey, projectKey, manager.getTableName());
  }

  //
  public void deleteEntry(Key entryKey, Key mappedKey, Key projectKey,
      String requestingTableName) throws StatementNotExecutedException {
//        ArrayList<DataColumn> data = new ArrayList<>();
    ArrayList<String> key = new ArrayList<>();
    DataRow data = new DataRow(tableName);
    data.add(new DataColumn(IStandardColumnTypes.DELETED_YES, DELETED));
    data.add(new DataColumn(1, MESSAGE_NUMBER));
    data.add(new DataColumn(entryKey.getID(), getId(requestingTableName)));
    data.add(new DataColumn(entryKey.getDBID(), getDbid(requestingTableName)));
    data.add(new DataColumn(mappedKey.getID(), getId(requestingTableName, false)));
    data.add(new DataColumn(mappedKey.getDBID(), getDbid(requestingTableName, false)));
    data.add(new DataColumn(projectKey.getID(), PROJECT_ID));
    data.add(new DataColumn(projectKey.getDBID(), PROJECT_DATABASE_ID));
    key.add(getId(requestingTableName).getColumnName());
    key.add(getDbid(requestingTableName).getColumnName());
    key.add(getId(requestingTableName, false).getColumnName());
    key.add(getDbid(requestingTableName, false).getColumnName());
    key.add(PROJECT_ID.getColumnName());
    key.add(PROJECT_DATABASE_ID.getColumnName());
    updateData(data, key);
  }

  @Override
  public ArrayList<String> getPrimaryColumnsForTable(String tableName) {
    if (this.tableName.equals(tableName)) {
      return primaryColumns;
    }

    return null;
  }

  /**
   *
   */
  @Override
  public ExportResult getEntryData(ExportResult data, Key project, String tableToCheck,
      ArrayList<ColumnType> list,
      ColumnType.ExportType exportType,
      boolean conflict, ArrayList<Key> keys, HashMap<ColumnType, SearchEntryInfo> filter,
      boolean hideNonDefaultFields) throws StatementNotExecutedException {
    ColumnType columnType = getId(tableToCheck, false);
    String keyQuery = "";
    if (keys != null && !keys.isEmpty()) {
      keyQuery = " AND (";
      for (Key key : keys) {
        keyQuery += "("
            + getId(tableToCheck) + "=" + key.getID() + " AND "
            + getDbid(tableToCheck) + "= " + key.getDBID() + ") OR";
      }
      if (keyQuery.endsWith("OR")) {
        keyQuery = keyQuery.substring(0, keyQuery.length() - 2) + ")";
      }

    }

    String filterQuery = "";
    if (filter != null) {
      for (ColumnType dataColumn : dataColumns) {
        if (filter.containsKey(dataColumn)) {
          filterQuery += " AND " + dataColumn + "=" + filter.get(dataColumn);
        }
      }
    }

    boolean shorten =
        (hideNonDefaultFields && columnType.isShortenedInListing()) || (!hideNonDefaultFields
            && columnType.isShortenedInExport());
    String query =
        "SELECT " + getId(tableToCheck) + ", " + getDbid(tableToCheck) + "," + getId(tableToCheck,
            false) + ", " + getDbid(tableToCheck, false) +
            (shorten ? (", Count(DISTINCT " + getId(tableToCheck, false) + ", " + getDbid(
                tableToCheck,
                false) + ") AS amount") : "")
            + " FROM " + databaseName + "." + tableName
            + " WHERE " + PROJECT_ID + " = " + project.getID()
            + " AND " + PROJECT_DATABASE_ID + " = " + project.getDBID()
            + filterQuery
            + " AND " + DELETED + " = '" + IStandardColumnTypes.DELETED_NO + "' "
            + keyQuery;

    if (shorten) {
      query += " GROUP BY " + getId(tableToCheck) + ", " + getDbid(tableToCheck) + "; ";
    }
    LogFactory.getLog(AbstractCrossLinkedManager.class).debug(query);

    try {
      xResultSet rs = executeSelect(query);
      while (rs.next()) {
        Key key = new Key(rs.getInt(getId(tableToCheck)),
            rs.getInt(getDbid(tableToCheck)));//get key for current entry
        EntryKey entryKey = new EntryKey(key, project);
        ExportRow exportData = data.getExportRow(entryKey);

        ExportColumn column = exportData.getExportColumn(columnType);
        column.addData(getDataString(project, tableToCheck, rs, columnType, hideNonDefaultFields));

        addAdditionalColumnsForResult(tableToCheck, rs, conflict, exportData, key, project,
            hideNonDefaultFields);
      }
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }

    return data;
  }

  protected void addAdditionalColumnsForResult(String tableToCheck, xResultSet rs, boolean conflict,
      ExportRow exportData, Key key,
      Key project, boolean hideNonDefaultFields) throws SQLException {
    if (!hideNonDefaultFields && !conflict) {
      ColumnType columnType = getDbid(tableToCheck, false);
      ExportColumn column = exportData.getExportColumn(columnType);
      column.addData(rs.getString(columnType));
    }
  }

  /**
   * Converts the data from the result set to the String that should be displayed.
   */
  protected String getDataString(Key project, String tableToCheck, xResultSet rs,
      ColumnType columnType,
      boolean forListing) throws SQLException {
    if ((forListing && columnType.isShortenedInListing()) || (!forListing && columnType
        .isShortenedInExport())) {
      return "[ #" + rs.getString("amount") + " ]";
    } else {
      int otherId = rs.getInt(getId(tableToCheck, false));//get connected id
      int otherDbId = rs.getInt(getDbid(tableToCheck, false));//get connected dbid
      Key otherKey = new Key(otherId, otherDbId);
      AbstractBaseEntryManager manager = getManager(tableToCheck, false);
      return manager.toString(project, otherKey, false);
    }
  }

  @Override
  public boolean isSynchronised(DataSetOld entry) throws StatementNotExecutedException {
    DataTableOld table = entry.getOrCreateDataTable(tableName);
    assert table.size() == 1;
    DataRow row = table.get(0);
    String query = "SELECT " + MESSAGE_NUMBER + " FROM " + databaseName + "." + tableName
        + " WHERE " + PROJECT_ID + "=" + entry.getProjectKey().getID()
        + " AND " + PROJECT_DATABASE_ID + "=" + entry.getProjectKey().getDBID()
        + " AND " + idOne + "='" + row.get(idOne) + "'"
        + " AND " + dbOne + "='" + row.get(dbOne) + "'"
        + " AND " + idTwo + "='" + row.get(idTwo) + "'"
        + " AND " + dbTwo + "='" + row.get(dbTwo) + "'"
        + " AND " + MESSAGE_NUMBER + "!=0";
    try {
      ResultSet rs = executeSelect(query);
      return !rs.next();
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query, ex);
    }
  }

  @Override
  public SolveConflictLineGroup getLinesForEntry(DataRow localAll,
      DataRow globalRows) throws EntriesException,
      StatementNotExecutedException {
    String lastSync = "0";
    SolveConflictLineGroup lines = new SolveConflictLineGroup(tableName);

    String displayL = "";
    HashMap<ColumnType, String> valueL = new HashMap<>();
    String displayR = "";
    HashMap<ColumnType, String> valueR = new HashMap<>();
    boolean deletedLocal = true;
    boolean deletedGlobal = true;

    if (!localAll.isEmpty()) {

      if (localAll.get(tableName + "." + DELETED) != null) {
        deletedLocal = localAll.get(tableName + "." + DELETED)
            .equals(IStandardColumnTypes.DELETED_YES);
      } else if (localAll.get(DELETED) != null) {
        deletedLocal = localAll.get(DELETED).equals(IStandardColumnTypes.DELETED_YES);
      }
      if (!deletedLocal) {

        Key projectKey = new Key(localAll.get(tableName + "." + PROJECT_ID),
            localAll.get(tableName + "." + PROJECT_DATABASE_ID));

        valueL.put(idOne, localAll.get(idOne));
        valueL.put(dbOne, localAll.get(dbOne));
        valueL.put(idTwo, localAll.get(idTwo));
        valueL.put(dbTwo, localAll.get(dbTwo));
        Key keyOne = new Key(localAll.get(idOne), localAll.get(dbOne));
        String disp1 = managerOne.toString(projectKey, keyOne, true);
        Key keyTow = new Key(localAll.get(idTwo), localAll.get(dbTwo));
        String disp2 = managerTwo.toString(projectKey, keyTow, true);
        displayL =
            "<html><b>" + managerOne.getLocalisedTableName() + ":</b><br>" + disp1 + "<br><b>"
                + managerTwo.getLocalisedTableName() + ":</b><br>" + disp2;
      }
    }
    if (!globalRows.isEmpty()) {

      if (globalRows.get(DELETED) != null) {
        deletedGlobal = globalRows.get(DELETED).equals(IStandardColumnTypes.DELETED_YES);
      } else if (globalRows.get(tableName + "." + DELETED) != null) {
        deletedGlobal = globalRows.get(tableName + "." + DELETED)
            .equals(IStandardColumnTypes.DELETED_YES);
      }
      if (!deletedGlobal) {
        Key projectKey = new Key(globalRows.get(tableName + "." + PROJECT_ID),
            globalRows.get(tableName + "." + PROJECT_DATABASE_ID));

        valueR.put(idOne, globalRows.get(idOne));
        valueR.put(dbOne, globalRows.get(dbOne));
        valueR.put(idTwo, globalRows.get(idTwo));
        valueR.put(dbTwo, globalRows.get(dbTwo));
        Key keyOne = new Key(globalRows.get(idOne), globalRows.get(dbOne));
        String disp1 = managerOne.toString(projectKey, keyOne, true);
        Key keyTow = new Key(globalRows.get(idTwo), globalRows.get(dbTwo));
        String disp2 = managerTwo.toString(projectKey, keyTow, true);
        displayR =
            "<html><b>" + managerOne.getLocalisedTableName() + ":</b><br>" + disp1 + "<br><b>"
                + managerTwo.getLocalisedTableName() + ":</b><br>" + disp2;
      }

    }
    if (globalRows.get(STATUS) != null) {
      lastSync = globalRows.get(STATUS);
    }
    if (globalRows.get(tableName + "." + STATUS) != null) {
      lastSync = globalRows.get(tableName + "." + STATUS);
    }

    lines.setLastSync(lastSync);
    if (deletedGlobal) {
      displayR = Loc.get("DELETE");
    }
    if (deletedLocal) {
      displayL = Loc.get("DELETE");
    }
    if (deletedGlobal && deletedLocal) {
      return lines;
    }

    lines.add(
        new SolveConflictLineUpdateOrDelete(valueL, displayL, valueR, displayR, localisedTableName,
            tableName));

    return lines;

  }

  @Override
  public String toString(DataSetOld dataSet) throws StatementNotExecutedException {
    DataTableOld entryTable = dataSet.getOrCreateDataTable(tableName);
    for (DataRow d : entryTable) {
      Key projectKey = new Key(d.get(tableName + "." + PROJECT_ID),
          d.get(tableName + "." + PROJECT_DATABASE_ID));

      Key keyOne = new Key(d.get(idOne), d.get(dbOne));
      String disp1 = managerOne.toString(projectKey, keyOne, true);
      Key keyTow = new Key(d.get(idTwo), d.get(dbTwo));
      String disp2 = managerTwo.toString(projectKey, keyTow, true);
      disp1 = disp1.replaceAll("<br>", ", ");
      disp2 = disp2.replaceAll("<br>", ", ");
      return "<html><b>" + managerOne.getLocalisedTableName() + ":</b> " + disp1 + "<br><b>"
          + managerTwo.getLocalisedTableName() + ":</b> " + disp2 + "</html>";
    }
    return "Not found??";
  }

  /**
   *
   */
  public boolean hasManager(BaseEntryManager manager) {
    return managerOne.equals(manager) || managerTwo.equals(manager);
  }

  public ArrayList<CrossLinkedEntry> checkForCrossLinksNotWorkingManagerOne(Key projectKey)
      throws StatementNotExecutedException {
    return getDeletedMappings(projectKey, managerOne, managerTwo);
  }

  public ArrayList<CrossLinkedEntry> checkForCrossLinksNotWorkingManagerTwo(Key projectKey)
      throws StatementNotExecutedException {
    return getDeletedMappings(projectKey, managerTwo, managerOne);
  }

  private ArrayList<CrossLinkedEntry> getDeletedMappings(Key projectKey,
      AbstractBaseEntryManager managerOne,
      AbstractBaseEntryManager managerTwo)
      throws StatementNotExecutedException {

    ArrayList<CrossLinkedEntry> crossLinkedEntries = new ArrayList<>();
    ColumnType idOne = getId(managerOne.tableName);
    ColumnType idTwo = getId(managerTwo.tableName);
    ColumnType dbOne = getDbid(managerOne.tableName);
    ColumnType dbTwo = getDbid(managerTwo.tableName);

    String query = "SELECT " + managerOne.tableName + "." + ID + ", " + managerOne.tableName + "."
        + DATABASE_ID + "," + managerTwo.tableName + "." + ID + ", " + managerTwo.tableName + "."
        + DATABASE_ID + " FROM " + tableName
        + " JOIN " + managerOne.tableName
        + " ON " + idOne + "=" + managerOne.tableName + "." + ID
        + " AND " + dbOne + "=" + managerOne.tableName + "." + DATABASE_ID
        + " AND " + tableName + "." + PROJECT_ID + "=" + managerOne.tableName + "." + PROJECT_ID
        + " AND " + tableName + "." + PROJECT_DATABASE_ID + "=" + managerOne.tableName + "."
        + PROJECT_DATABASE_ID
        + " JOIN " + managerTwo.tableName
        + " ON " + idTwo + "=" + managerTwo.tableName + "." + ID
        + " AND " + dbTwo + "=" + managerTwo.tableName + "." + DATABASE_ID
        + " AND " + tableName + "." + PROJECT_ID + "=" + managerTwo.tableName + "." + PROJECT_ID
        + " AND " + tableName + "." + PROJECT_DATABASE_ID + "=" + managerTwo.tableName + "."
        + PROJECT_DATABASE_ID
        + " WHERE " + tableName + "." + DELETED + "='" + DELETED_NO + "'"
        + AND + managerOne.tableName + "." + DELETED + "='" + DELETED_NO + "'"
        + AND + managerTwo.tableName + "." + DELETED + "='" + DELETED_YES + "' "
        + AND + tableName + "." + PROJECT_ID + "=" + projectKey.getID() + " "
        + AND + tableName + "." + PROJECT_DATABASE_ID + "=" + projectKey.getDBID() + " ";
    xResultSet rs = null;
    try {
      rs = executeSelect(query);
      while (rs.next()) {
        Key keyOne = new Key(rs.getInt(managerOne.tableName + "." + ID),
            rs.getInt(managerOne.tableName + "." + DATABASE_ID));
        Key keyTwo = new Key(rs.getInt(managerTwo.tableName + "." + ID),
            rs.getInt(managerTwo.tableName + "." + DATABASE_ID));
        String displayOriginal = managerOne.toString(projectKey, keyOne, false);
        String displayDeleted = managerTwo.toString(projectKey, keyTwo, false);
        crossLinkedEntries.add(new CrossLinkedEntry(keyOne, keyTwo, projectKey, managerOne, this,
            displayOriginal, displayDeleted));

      }
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }

    return crossLinkedEntries;

  }


  public boolean isValidCrossLinkedValue(Key projectKey, Key key,
      BaseEntryManager manager) throws StatementNotExecutedException {
    return getManager(manager.getTableName(), false).isEntryValid(projectKey, key);
  }
}
