package de.uni_muenchen.vetmed.xbook.implementation.xbook.database;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Utility class used to start and stop the standalone MySQL server that comes
 * with the client.
 *
 * @author fnuecke
 */
public final class MySqlServer {

    /**
     * Logging...
     */
    private static final Log log = LogFactory.getLog(MySqlServer.class);

    /**
     * No need for an instance.
     */
    private MySqlServer() {
    }

    static boolean shutdownWanted = false;

    /**
     * Start the MySQL server if possible.
     */
    public static void init() {
        try {
            killRunningDatabases();
            startServer();
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    shutdownWanted = true;
                    stopServer();
                }
            }));
        } catch (IOException | InterruptedException e) {
            log.error(Loc.get("MYSQLSERVER>FAILED_START", e.getMessage()));
            e.printStackTrace();
            // Set local connections to disabled.
            log.info(Loc.get("MYSQLSERVER>DB_NOT_STARTED"));
            JOptionPane.showMessageDialog(null, Loc.get("MYSQLSERVER>DB_NOT_STARTED"), Loc.get("ERROR"), JOptionPane.ERROR_MESSAGE);
            System.exit(2);
        }
    }

    /**
     * Run the stop command with a timeout of 5 seconds.
     */
    public static void stopServer() {

        try {
            String[] stopCmd = {"", "", "", ""};
            final String osName = System.getProperty("os.name").toLowerCase();
            if (osName.contains("win")) {
                stopCmd = new String[]{XBookConfiguration.DIR_PREFIX + "assets/mysql/bin/mysqladmin.exe", "-uroot", "-P" + XBookConfiguration.getDatabasePort(), "shutdown"};
            } else if (osName.contains("linux") || osName.contains("mac")) {
                String strCommandstop = XBookConfiguration.DIR_PREFIX + "/assets/mysql/bin/" + System.getProperty("sun.arch.data.model") + "/mysqladmin";
                stopCmd = new String[]{strCommandstop, "--user=root", "--password=", "shutdown", "--port=" + XBookConfiguration.getDatabasePort()};
            }
            log.info(Loc.get("MYSQLSERVER>STOP_DATABASE", XBookConfiguration.cmdToString(stopCmd)));
            ProcessBuilder builder = new ProcessBuilder(stopCmd);
            builder.redirectErrorStream(true);
            Process p = builder.start();
            final BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String line;

                        while ((line = in.readLine()) != null) {
                            System.out.println(line);
                            log.info(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            log.info(Loc.get("MYSQLSERVER>SUCCEEDED"));
        } catch (IOException ex) {
            log.error(Loc.get("MYSQLSERVER>FAILED_STOPPING_DATABASE", ex.getMessage()));
        }
    }

    public static void startServer() throws IOException, InterruptedException {
        String[] CMD_SERVER_START = {"", ""};
        File f = new File(XBookConfiguration.DIR_PREFIX);
        ;
        final String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("win")) {
            CMD_SERVER_START = new String[]{XBookConfiguration.DIR_PREFIX + "assets/mysql/bin/mysqld.exe", "-P" + XBookConfiguration.getDatabasePort(true,true), "--thread_stack=190000"};
        } else if (osName.contains("linux") || osName.contains("mac")) {

            String strCommandtest = XBookConfiguration.DIR_PREFIX + "assets/mysql/bin/" + System.getProperty("sun.arch.data.model") + "/mysqld";
            System.out.println(f.exists());
            System.out.println(f.canExecute());
            System.out.println(f.canRead());
            // remove --port from my.cnf file for [mysqld] block
            CMD_SERVER_START = new String[]{strCommandtest, "--defaults-file=" + XBookConfiguration.DIR_PREFIX + "assets/mysql/my.cnf", "--port=" + XBookConfiguration.getDatabasePort(true,true), "--thread_stack=192000"};
        }

        log.info(Loc.get("MYSQLSERVER>START_DATABASE", XBookConfiguration.cmdToString(CMD_SERVER_START)));
        ProcessBuilder builder = new ProcessBuilder(CMD_SERVER_START);
        // builder.directory(f);
        builder.redirectErrorStream(true);
        final Process p = builder.start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                final int i;
                try {
                    i = p.waitFor();

                    System.out.println("process terminated with: " + i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).start();
        final BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String line;

                    while ((line = in.readLine()) != null) {

                        log.info(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("??");
                }
            }
        }).start();

        log.info(Loc.get("MYSQLSERVER>SUCCEEDED"));
    }

    private static void killRunningDatabases() throws IOException {
        //only works for windows!
        if (System.getProperty("os.name").toLowerCase().equals("linux")) {
            stopServer();
        }
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            String cmd = System.getenv("windir") + "\\system32\\" + "tasklist.exe";
            //try this if above generates problems
            //String cmd = "wmic process get Caption,Commandline,Processid";
            try {
                Process p = Runtime.getRuntime().exec(cmd);
                InputStreamReader isr = new InputStreamReader(p.getInputStream());
                BufferedReader input = new BufferedReader(isr);
                try {
                    String line;

                    while ((line = input.readLine()) != null) {

                        int exe = line.indexOf(".exe");
                        if (exe > 0) {
                            String prozess = line.substring(0, exe + 4);
                            if (prozess.equals("mysqld.exe")) {
                                String[] lines = line.split(" ");
                                String[] real = new String[6];
                                int index = 0;
                                for (String entry : lines) {
                                    if (!entry.isEmpty()) {
                                        real[index++] = entry;
                                    }

                                }
                                String origin = real[2];
                                String pID = real[1];
                                log.info(line);
                                if (origin.equals("Console")) {
                                    log.info("trying to kill");
                                    String killCommand = "Taskkill /PID " + pID + " /F";
                                    log.info(killCommand);
                                    Runtime.getRuntime().exec(killCommand);
                                }
                            }
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
