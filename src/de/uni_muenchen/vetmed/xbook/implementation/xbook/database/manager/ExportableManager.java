package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportResult;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.SearchEntryInfo;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface ExportableManager {
    ExportResult getEntryData(ExportResult data, Key project, String tableToCheck, ArrayList<ColumnType> list, ColumnType.ExportType exportType,
                              boolean conflict, ArrayList<Key> keys, HashMap<ColumnType, SearchEntryInfo> filter, boolean forListing) throws StatementNotExecutedException;

    Collection<? extends ColumnType> getExportableColumns();
}
