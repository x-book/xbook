package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Result;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class LocalUserManager extends UserManager {

    public static final String TABLENAME_SYSTEM_USER = IStandardColumnTypes.DATABASE_NAME_GENERAL + ".systemuser";
    public static final String SYSTEM_USER_EMAIL = IStandardColumnTypes.DATABASE_NAME_GENERAL + ".Mail";
    public static final String SYSTEM_USER_UID = IStandardColumnTypes.DATABASE_NAME_GENERAL + ".UID";
    public static final String SYSTEM_USER_PASSWORD = IStandardColumnTypes.DATABASE_NAME_GENERAL + ".Password";

    public LocalUserManager(Connection connection, String databaseName) {
        super(connection, databaseName);
    }


}
