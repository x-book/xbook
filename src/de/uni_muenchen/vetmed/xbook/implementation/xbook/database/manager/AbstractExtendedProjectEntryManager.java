package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTableOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.event.ProjectEvent;
import de.uni_muenchen.vetmed.xbook.api.event.ProjectListener;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLineGroup;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.SolveConflictLineUpdateOrDelete;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 17.06.2014.
 */
public abstract class AbstractExtendedProjectEntryManager extends AbstractSynchronisationManager {

  private static final Log LOGGER = LogFactory.getLog(AbstractExtendedProjectEntryManager.class);

  private ArrayList<ProjectListener> listenerList = new ArrayList<>();
  protected Iterable<? extends ColumnType> conflictableColumns;

  public AbstractExtendedProjectEntryManager(String tableName, String localisedTableName,
      int dbNumber,
      Connection connection, String databaseName) {
    super(tableName, localisedTableName, dbNumber, connection, databaseName);
    conflictableColumns = getConflictableColumns();
  }

  protected abstract Iterable<? extends ColumnType> getConflictableColumns();

  public void addProjectListener(ProjectListener listerner) {
    listenerList.add(listerner);
  }

  protected void projectAdded(ProjectEvent evt) {
    for (ProjectListener listener : listenerList) {
      listener.onProjectAdded(evt);
    }
  }

  protected void projectDeleted(ProjectEvent evt) {
    for (ProjectListener listener : listenerList) {
      listener.onProjectDeleted(evt);
    }
  }

  protected void projectUpdated(ProjectEvent evt) {
    for (ProjectListener listener : listenerList) {
      listener.onProjectChanged(evt);
    }
  }

  protected DataTableOld getEntry(String condition)
      throws StatementNotExecutedException, IOException {

    DataTableOld dataTable = new DataTableOld(tableName);
    String query = "SELECT ";
    for (int i = 0; i < dataColumns.size(); i++) {
      query += dataColumns.get(i);
      if (i < dataColumns.size() - 1) {
        query += ", ";
      } else {
        query += " FROM ";
      }
    }
    query += tableName + condition;
    xResultSet rs = null;
    int cumulativeImageFileSize = 0;

    try {
      rs = executeSelect(query, true);
      boolean addMoreRows = true;

      boolean wasAnythingLoaded = false;
      while (rs.next() && addMoreRows) {
        DataRow row = new DataRow(tableName);
        for (ColumnType column : dataColumns) {
          String string = rs.getString(column);
          //we have an image... now lets solve this...
          if (column.getType() == ColumnType.Type.FILE) {
            // check if new image was set by user
            final boolean fileUpdated = rs.getBoolean(FILE_MUST_BE_UPDATED);
            //get path from image column and upload image

            saveFile(fileUpdated, row, string);
            cumulativeImageFileSize += string.length();
            if (cumulativeImageFileSize > AbstractConfiguration.MAX_UPLOAD_BATCH_SIZE) {
              addMoreRows = false;
              dataTable.setWhereAllEntriesLoaded(false);
            }
          }

          row.put(column, string);


        }
        //load first entry anyway...
        if (addMoreRows || !wasAnythingLoaded) {
          dataTable.add(row);
        }
        wasAnythingLoaded = true;

      }

    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException ex) {
          Logger.getLogger(AbstractEntryManager.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return dataTable;
  }

  public DataTableOld getNextUncommitedEntry(Key projectKey)
      throws StatementNotExecutedException, IOException {
    String condition = " WHERE " + PROJECT_ID + "='" + projectKey.getID()
        + "' AND " + PROJECT_DATABASE_ID + "=" + projectKey.getDBID();

    return getEntry(condition);
  }

  public void updateEntries(DataSetOld data) throws StatementNotExecutedException {
    String query = "";
    try {
      DataTableOld table = data.getOrCreateDataTable(tableName);
      if (table == null) {
        return;
      }
      for (DataRow entryData : table) {
        if (entryData.isEmpty()) {
          continue;
        }
//                ArrayList<DataColumn> dataList = new ArrayList<>();
        DataRow dataList = new DataRow(tableName);
        for (DataColumn row : entryData.iterator()) {
          dataList.add(row);
        }
        int status = updateData(dataList, primaryColumns);
        if (status == 0) {
          insertData(dataList);
          projectAdded(new ProjectEvent(data));
        } else {
          projectUpdated(new ProjectEvent(data));
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
      throw new StatementNotExecutedException(query, e);
    } catch (NullPointerException ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Solves the Conflict by updating the given data
   *
   * @param entryData The data of the entry
   */
  public void solveConflict(DataSetOld entryData) throws StatementNotExecutedException {
    DataTableOld table = entryData.getOrCreateDataTable(tableName);
    if (table == null) {
      return;
    }
    for (DataRow row : table) {
      if (row.isEmpty()) {
        continue;
      }
//            ArrayList<DataColumn> dataList = new ArrayList<>();
      DataRow dataList = new DataRow(tableName);
      dataList.add(new DataColumn(entryData.getProjectKey().getID(), PROJECT_ID));
      dataList.add(new DataColumn(entryData.getProjectKey().getDBID(), PROJECT_DATABASE_ID));
      dataList.add(new DataColumn(1, MESSAGE_NUMBER));

      for (DataColumn data : row.iterator()) {
        dataList.add(data);
      }
      int status = updateData(dataList, primaryColumns);
      if (status == 0) {
        insertData(dataList);
      }

    }
  }

  DataTableOld getConflicts(Key projectKey, DataRow primRow) throws StatementNotExecutedException {
    StringBuilder sb = new StringBuilder();
    sb.append("SELECT ");
    for (ColumnType type : dataColumns) {
      sb.append(type.getColumnName()).append(", ");
    }
    sb.delete(sb.length() - ", ".length(), sb.length());
    sb.append(" FROM ").append(tableName);
    sb.append(" WHERE ");
    for (DataColumn column : primRow.iterator()) {
      sb.append(column.getColumnName()).append("=").append(column.getValue()).append(" AND ");
    }
    sb.delete(sb.length() - " AND ".length(), sb.length());
    try {
      xResultSet rs = executeSelect(sb.toString());
      DataTableOld table = new DataTableOld(tableName);
      while (rs.next()) {
        DataRow row = new DataRow(tableName);
        for (ColumnType type : dataColumns) {
          row.put(type, rs.getString(type));
        }
        table.add(row);
      }
      return table;
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(sb.toString(), ex);
    }
  }

  @Override
  public SolveConflictLineGroup getLinesForEntry(DataRow localAll,
      DataRow globalRows) throws EntriesException, StatementNotExecutedException {
    return getProjectEntryLines(localAll, globalRows);

  }

  protected SolveConflictLineGroup getProjectEntryLines(DataRow localAll, DataRow globalRows) {
    String lastSync = "0";
    SolveConflictLineGroup lines = new SolveConflictLineGroup(tableName);

    String displayL = "";
    HashMap<ColumnType, String> valueL = new HashMap<>();
    String displayR = "";
    HashMap<ColumnType, String> valueR = new HashMap<>();
    boolean deletedLocal = true;
    boolean deletedGlobal = true;

    for (ColumnType column : conflictableColumns) {
      String localColumn = localAll.get(column);
      if (localColumn != null) {
        valueL.put(column, localColumn);

      }
      String globalColumn = globalRows.get(column);
      if (globalColumn != null) {

        valueR.put(column, globalColumn);
        deletedGlobal = false;
      }
    }
    if (globalRows.get(STATUS) != null) {
      lastSync = globalRows.get(STATUS);
    }
    if (globalRows.get(tableName + "." + STATUS) != null) {
      lastSync = globalRows.get(tableName + "." + STATUS);
    }
    if (globalRows.get(DELETED) != null) {
      deletedGlobal = globalRows.get(DELETED).equals(IStandardColumnTypes.DELETED_YES);
    }

    if (globalRows.get(tableName + "." + DELETED) != null) {
      deletedGlobal = globalRows.get(tableName + "." + DELETED)
          .equals(IStandardColumnTypes.DELETED_YES);
    }
    if (localAll.get(tableName + "." + DELETED) != null) {
      deletedLocal = localAll.get(tableName + "." + DELETED)
          .equals(IStandardColumnTypes.DELETED_YES);
    }

    if (localAll.get(DELETED) != null) {
      deletedLocal = localAll.get(DELETED).equals(IStandardColumnTypes.DELETED_YES);
    }
    displayR = getConflictDisplay(valueR);
    displayL = getConflictDisplay(valueL);

    lines.setLastSync(lastSync);
    if (deletedGlobal) {
      displayR = Loc.get("DELETE");
    }
    if (deletedLocal) {
      displayL = Loc.get("DELETE");
    }
    if (deletedGlobal && deletedLocal) {
      return lines;
    }

    lines.add(
        new SolveConflictLineUpdateOrDelete(valueL, displayL, valueR, displayR, localisedTableName,
            tableName));

    return lines;
  }

}
