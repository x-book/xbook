package de.uni_muenchen.vetmed.xbook.implementation.xbook.database;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.tools.Server;

import java.sql.SQLException;

public class SQLiteServer {
    private static Server server;
    private static final Log log = LogFactory.getLog(SQLiteServer.class);

    public static void init() {
        try {
            startServer();
        } catch (SQLException e) {
            log.error(Loc.get("H2SERVER>FAILED_START", e.getMessage()));
        }
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                stopServer();
            }
        }));
    }

    private static void startServer() throws SQLException {
        server = Server.createTcpServer("-tcpPort", "53309", "-tcpAllowOthers").start();
    }


    private static void stopServer() {
        if (server != null) {
            server.stop();
        }
    }
}
