package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.HashListMap;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.Column;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.Index;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class SchemeManager extends TableManager {

    private static final Log _log = LogFactory.getLog(SchemeManager.class);

    public SchemeManager(Connection connection, String databaseName) {
        super(connection, databaseName);
    }

    /**
     * gets all table names of database
     *
     * @return
     */
    public ArrayList<String> getTables() throws StatementNotExecutedException {
        ArrayList<String> tableNames = new ArrayList<>();
        try {
            DatabaseMetaData database = connection.getMetaData();

            String[] tblTypes = {"TABLE"};
            ResultSet rs = database.getTables(null, connection.getCatalog(),
                    "%", tblTypes);
            while (rs.next()) {
                tableNames.add(databaseName + "." + rs.getString("TABLE_NAME"));
            }
        } catch (SQLException sql) {
            throw new StatementNotExecutedException("getTables()", sql);
        }

        return tableNames;
    }

    public ArrayList<String> getTablesGeneral() throws StatementNotExecutedException {
        ArrayList<String> tableNames = new ArrayList<>();
        try {
            DatabaseMetaData database = connection.getMetaData();

            String[] tblTypes = {"TABLE"};
            ResultSet rs = database.getTables(IStandardColumnTypes.DATABASE_NAME_GENERAL, null, "%", tblTypes);
            while (rs.next()) {
                String tableName = IStandardColumnTypes.DATABASE_NAME_GENERAL + "." + rs.getString("TABLE_NAME");
                System.out.println(tableName);
                tableNames.add(tableName);
            }
        } catch (SQLException sql) {
            throw new StatementNotExecutedException("getTables()", sql);
        }

        return tableNames;
    }

    /**
     * gets the description of the given table - consisting of: columns: name,
     * type, default value, is nullable and autoincrement
     *
     * @param tableName
     * @return
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException
     */
    public ArrayList<Column> getTableDescription(String tableName)
            throws StatementNotExecutedException {

        ArrayList<Column> result = new ArrayList<>();
        try {
            DatabaseMetaData database = connection.getMetaData();
            ResultSet rs = database.getColumns(getDatabaseName(tableName), null, ColumnHelper.removeDatabaseName(tableName), "%");
            String[] elements;
            //doll
            // rs.getString("IS_AUTOINCREMENT"); just works since younger
            // database version
            // or with extra patch
            String query = "Explain " + tableName + ";";
            ResultSet rsHelp = executeSelect(query);
            String extra;
            while (rs.next()) {
                rsHelp.next();

                elements = new String[6];
                String name = rs.getString("COLUMN_NAME");
                if (name == null) {
                    System.out.println("woot??");
                }
                String type = rs.getString("TYPE_NAME").toUpperCase();
                String size;
                String defaultValue;
                String nullable;
                String autoincrement;
                switch (type) {

                    case "ENUM":
                        extra = rsHelp.getString("Type");
                        size = extra.substring(5, extra.length() - 1);
                        break;
                    case "DATE"://somehow we get wrong values..
                        size = "10";
                        break;
                    default:
                        size = rs.getString("COLUMN_SIZE");
                }
                if (size == null) {
                    System.out.println("?");
                }
                defaultValue = rs.getString("COLUMN_DEF");
                if (defaultValue == null) {
                    defaultValue = "";
                }
                nullable = rs.getString("IS_NULLABLE");

                extra = rsHelp.getString("Extra");
                if (extra.contains("auto_increment")) {
                    autoincrement = "YES";
                } else {
                    autoincrement = "NO";
                }
                // elements[5]=rs.getString("IS_AUTOINCREMENT");
                result.add(new Column(name, type, size, defaultValue, nullable, autoincrement));
            }
        } catch (SQLException sql) {
            throw new StatementNotExecutedException("getTableDescription()", sql);
        }

        return result;
    }

    /**
     * gets the primary key fields of the given table
     *
     * @param tableName
     * @return
     * @throws StatementNotExecutedException
     */
    public ArrayList<String> getPrimaryKeys(String tableName)
            throws StatementNotExecutedException {
        ArrayList<String> result = new ArrayList<>();
        try {
            DatabaseMetaData database = connection.getMetaData();
            ResultSet rs = database.getPrimaryKeys(getDatabaseName(tableName), null, ColumnHelper.removeDatabaseName(tableName));
            while (rs.next()) {
                result.add(rs.getString("COLUMN_NAME"));
            }
        } catch (SQLException sql) {
            throw new StatementNotExecutedException("getPrimaryKeys()", sql);
        }
        return result;
    }

    /**
     * creates a new table with the given column attributes and primary key
     *
     * @param tableName
     * @param columnNames
     * @param types
     * @param sizes
     * @param nullable      - column can be null
     * @param defaultValues - column has a default value
     * @param autoincrement - column is autoincrement
     * @param primaryKey
     * @throws StatementNotExecutedException
     */
    public void createTable(String tableName, String[] columnNames,
                            String[] types, String[] sizes, String[] nullable,
                            String[] defaultValues, String[] autoincrement, String[] primaryKey, ArrayList<Index> indices)
            throws StatementNotExecutedException {
        String query = "CREATE TABLE " + tableName + " (";
        for (int i = 0; i < columnNames.length; i++) {
            query = query
                    + columnOptions(columnNames[i], types[i], sizes[i],
                    nullable[i], defaultValues[i], autoincrement[i]);
            query = query + ", ";
        }
        query = query + "PRIMARY KEY (";
        for (int i = 0; i < primaryKey.length; i++) {
            query = query + primaryKey[i];
            if (!(i == primaryKey.length - 1)) {
                query = query + ", ";
            } else {
                query = query + ") ";
            }
        }
        query = getIndexString(tableName, indices, query);

        query += ");";
        try {
            executeQuery(query);
        } catch (SQLException e) {

            throw new StatementNotExecutedException(query, e);
        }
    }

    protected String getIndexString(String tableName, ArrayList<Index> indices, String query) {
        for (Index index : indices) {
            query += ", INDEX `" + index.getIndexName() + "` (";
            final ArrayList<String> columns = index.getColumns();
            for (int i = 0; i < columns.size(); i++) {
                query += "`" + columns.get(i) + "`";
                if (!(i == columns.size() - 1)) {
                    query += ", ";
                } else {
                    query += ") ";
                }
            }
        }
        return query;
    }

    /**
     * changes the definition of the given column
     *
     * @param tableName
     * @param columnName
     * @param type
     * @param size
     * @param nullable      - can be null
     * @param defaultValues - has a default value
     * @param autoincrement - is autoincrement
     * @throws StatementNotExecutedException
     */
    public void changeColumn(String tableName, String columnName, String type,
                             String size, String nullable, String defaultValues,
                             String autoincrement) throws StatementNotExecutedException {
        String query = "ALTER TABLE "
                + tableName
                + " CHANGE COLUMN "
                + "`" + columnName + "` "
                + columnOptions(columnName, type, size, nullable,
                defaultValues, autoincrement) + ";";

        try {
            executeQuery(query);
        } catch (SQLException e) {
            if (e.getErrorCode() == 1265) {
                String defaultString = defaultValues;
                if (defaultString == null) {
                    defaultString = "";
                    if (type.contains("INT") || type.contains("TINYINT")
                            || type.contains("SMALLINT")
                            || type.contains("MEDIUMINT")
                            || type.contains("BIGINT") || type.contains("FLOAT")
                            || type.contains("DECIMAL") || type.contains("DOUBLE")) {
                        defaultString = "-1";
                    }
                }
                String query2 = "UPDATE " + tableName + " SET " + columnName + "='" + defaultString
                        + "' WHERE `" + columnName + "` is NULL";
                try {
                    executeQuery(query2);
                    executeQuery(query);
                } catch (SQLException ex) {
                    Logger.getLogger(SchemeManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                throw new StatementNotExecutedException(query, e);
            }
        }

    }

    /**
     * adds a column to the given table
     *
     * @param tableName
     * @param columnName
     * @param type
     * @param size
     * @param nullable      - can be null
     * @param defaultValues - has a default value
     * @param autoincrement - is autoincrement
     * @throws StatementNotExecutedException
     */
    public void addColumn(String tableName, String columnName, String type,
                          String size, String nullable, String defaultValues,
                          String autoincrement, String after) throws StatementNotExecutedException {
        String query = "ALTER TABLE "
                + tableName
                + " ADD "
                + columnOptions(columnName, type, size, nullable, defaultValues, autoincrement)
                + (after != null ? (" AFTER `" + after + "`") : "");

        try {
            executeQuery(query);
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    public void removeColumn(String tableName, String columnName) throws StatementNotExecutedException {
        String query = "ALTER TABLE "
                + tableName
                + " DROP COLUMN `"
                + columnName + "`";

        try {
            executeQuery(query);
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    /**
     * adds a primary key to the given table
     *
     * @param tableName
     * @param columns   - new primary key
     * @throws StatementNotExecutedException
     */
    public void addPrimaryKey(String tableName, String[] columns)
            throws StatementNotExecutedException {
        String query = "ALTER TABLE " + tableName + " ADD PRIMARY KEY (`";
        for (int i = 0; i < columns.length - 1; i++) {
            query = query + columns[i] + "`, `";
        }
        query = query + columns[columns.length - 1] + "`);";

        try {
            executeQuery(query);
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    /**
     * adapts primary key of given table
     *
     * @param tableName
     * @param columns   - new primary key
     * @throws StatementNotExecutedException
     */
    public void changePrimaryKey(String tableName, String[] columns)
            throws StatementNotExecutedException {
        String query = "ALTER TABLE " + tableName + " DROP PRIMARY KEY ";
        try {


            executeQuery(query);
            query = "ALTER TABLE " + tableName + " ADD PRIMARY KEY (`";
            for (int i = 0; i < columns.length - 1; i++) {
                query = query + columns[i] + "`, `";
            }
            query = query + columns[columns.length - 1] + "`);";

            try {
                executeQuery(query);
            } catch (SQLException e) {
                throw new StatementNotExecutedException(query, e);
            }
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }



    /**
     * gets the description of the default value
     *
     * @param defaultValue
     * @param type
     * @return
     */
    private static String getDefaultValueString(String defaultValue, String type, String nullable) {
        if (type.equals("TEXT") || type.equals("MEDIUMTEXT") || type.equals("LONGTEXT")) {//   || type.equals("enum")){
            return "";
        }
        if (type.equals("DATE") && defaultValue.equals("")) {
            return "";
        }
        if (type.equals("TIME") && defaultValue.equals("")) {
            return "";
        }
        if (defaultValue == null) {
            return "";
        } else if (defaultValue.equals("") && type.equals("ENUM")) {//||type.equals("VARCHAR")){
//            if(nullable.equals("YES")){
//                return "DEFAULT NULL";
//            }
//            else{
            return "";
//            }
        } else if (defaultValue.equals("")
                && // Using contains in the following to also cover unsigned
                (type.contains("INT") || type.contains("TINYINT")
                        || type.contains("SMALLINT")
                        || type.contains("MEDIUMINT")
                        || type.contains("BIGINT") || type.contains("FLOAT")
                        || type.contains("DECIMAL") || type.contains("DOUBLE"))) {
            return "";
        } else if (!defaultValue.equals("") && type.contains("TIMESTAMP")) {
            return "DEFAULT " + defaultValue + " ";
        } else if (!defaultValue.equals("")) {
            return "DEFAULT '" + defaultValue + "' ";
        } else {
            return "DEFAULT '' ";
        }
    }

    /**
     * builds the queryManager part for the column definition
     *
     * @param columnName
     * @param type
     * @param size
     * @param nullable
     * @param defaultValues
     * @param autoincrement
     * @return
     */
    protected String columnOptions(String columnName, String type, String size,
                                   String nullable, String defaultValues, String autoincrement) {
        String[] parts = type.split(" ");
        String query = "`" + columnName + "` ";
        if (size.equals("0") || type.equals("DOUBLE")) {
            size = "";
        } else {
            size = " (" + size + ") ";
        }
        if (type.contains("UNSIGNED") && parts.length == 2
                && parts[1].equals("UNSIGNED")) {
            query += parts[0].toUpperCase() + size
                    + parts[1].toUpperCase() + " ";
        } else if (type.equals("TEXT") || type.equals("LONGTEXT") || type.equals("DATE") || type.equals("TIME")) {
            query += type.toUpperCase();
        } else {
            query += type.toUpperCase() + size;
        }
        if (nullable.equals("NO")) {
            query = query + " NOT NULL ";
        }
        query = query + " " + getDefaultValueString(defaultValues, type, nullable);
        if (autoincrement.equals("YES")) {
            query = query + " AUTO_INCREMENT ";
        }
        return query;
    }

    public void changeIndices(String tableName, ArrayList<Index> indices, ArrayList<Index> oldIndices) throws StatementNotExecutedException {
        String query = "ALTER TABLE " + tableName + " ";
        for (Index oldIndex : oldIndices) {
            query += " DROP INDEX `" + oldIndex.getIndexName() + "`,";
        }

        for (Index index : indices) {
            query += " ADD INDEX `" + index.getIndexName() + "` (";
            final ArrayList<String> columns = index.getColumns();
            for (int i = 0; i < columns.size(); i++) {
                query += "`" + columns.get(i) + "`";
                if (!(i == columns.size() - 1)) {
                    query += ", ";
                } else {
                    query += "), ";
                }
            }
        }
        query = query.substring(0, query.length() - ", ".length()) + ";";
        try {
            executeQuery(query);
        } catch (SQLException e) {
            throw new StatementNotExecutedException(query, e);
        }
    }

    public ArrayList<Index> getIndices(String tableName) {
        String query = "SHOW INDEX FROM  " + tableName + ";";
        ArrayList<Index> list = new ArrayList<>();
        HashListMap<String, String> indices = new HashListMap<>();
        try {
            ResultSet rs = executeSelect(query);
            while (rs.next()) {
                String indexName = rs.getString("Key_name");
                if (!indexName.equals("PRIMARY")) {
                    indices.add(indexName, rs.getString("Column_name"));
                }
            }

            for (Map.Entry<String, ArrayList<String>> stringArrayListEntry : indices.entrySet()) {
                list.add(new Index(stringArrayListEntry.getKey(), stringArrayListEntry.getValue()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
