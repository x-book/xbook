package de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IDefaultSectionAssignments;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnTypeList;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTableOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryKey;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportResult;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.SearchEntryInfo;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.event.EntryEvent;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLine;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLineGroup;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 17.06.2014.
 */
public abstract class AbstractEntryManager extends AbstractSynchronisationManager implements
    IDefaultSectionAssignments {

  private static final Log LOGGER = LogFactory.getLog(AbstractEntryManager.class);

  public static int PRIORITY = 20;

  public AbstractEntryManager(String tableName, String localisedTableName, int dbId,
      Connection connection,
      String databaseName) {
    super(tableName, localisedTableName, dbId, connection, databaseName);
    dataColumns = new ColumnTypeList(tableName);

    dataColumns.add(
        new ColumnType(ID, tableName).setSectionProperty(SECTION_SYSTEM).setHiddenInListing(true));
    //.setDataType(                        DataType.Integer).setScale(IntervalScale.class));
    dataColumns.add(new ColumnType(DATABASE_ID, tableName).setSectionProperty(SECTION_SYSTEM)
        .setHiddenInListing(
            true));//.setDataType(DataType.Integer).setScale(IntervalScale.class));
    dataColumns.add(
        new ColumnType(PROJECT_ID, tableName).setSectionProperty(SECTION_SYSTEM).setHiddenInListing(
            true));//.setDataType(DataType.Integer).setScale(IntervalScale.class));
    dataColumns.add(
        new ColumnType(PROJECT_DATABASE_ID, tableName).setSectionProperty(SECTION_SYSTEM)
            .setHiddenInListing(
                true));//.setDataType(DataType.Integer).setScale(IntervalScale.class));
    dataColumns.add(new ColumnType(MESSAGE_NUMBER, tableName).setSectionProperty(SECTION_SYSTEM));
    dataColumns.add(new ColumnType(STATUS, tableName).setSectionProperty(SECTION_SYSTEM));
    dataColumns.add(new ColumnType(DELETED, tableName).setSectionProperty(SECTION_SYSTEM));
    primaryColumns = new ArrayList<>();
    primaryColumns.add("" + ID);
    primaryColumns.add("" + DATABASE_ID);
    primaryColumns.add("" + PROJECT_ID);
    primaryColumns.add("" + PROJECT_DATABASE_ID);

  }

  /**
   * Loads the entry for the current table in the EntryData object, informations about the entry can
   * be found in the object itself. This only works with plain string entrys. More komplex tables
   * must override the methode
   */
  @Override
  public void load(DataSetOld data) throws StatementNotExecutedException {
    if (!(data instanceof EntryDataSet)) {
      return;
    }
    load((EntryDataSet) data);//forward this as we only need entry data...
  }

  public DataTableOld getNextUncommitedEntry(EntryKey entry)
      throws StatementNotExecutedException, IOException {
    String condition = " WHERE ";
    String limit = "";
    if (entry.getEntryKey() != null && entry.getEntryKey().isValidKey()) {
      condition += ID + "='" + entry.getEntryKey().getID()
          + "' AND " + DATABASE_ID + "=" + entry.getEntryKey().getDBID() + " AND ";
    } else {
      limit = " LIMIT " + 1;
    }
    condition += PROJECT_ID + "='" + entry.getProjectKey().getID()
        + "' AND " + PROJECT_DATABASE_ID + "=" + entry.getProjectKey().getDBID()
        + " AND " + MESSAGE_NUMBER + " = 1";
    condition = condition + limit;

    boolean needsId = (entry.getEntryKey() == null || !entry.getEntryKey().isValidKey());
    DataTableOld dataTable = new DataTableOld(tableName);
    String query = "SELECT ";
    for (int i = 0; i < dataColumns.size(); i++) {
      ColumnType query1 = dataColumns.get(i);
      if (query1.getType() != ColumnType.Type.EXTERN_VALUE
          && query1.getType() != ColumnType.Type.EXTERN_ID) {
        query += query1;

        if (i < dataColumns.size() - 1) {
          query += ", ";
        } else {
          query += " FROM ";
        }
      }
    }
    int id = -1;
    int dbnumber = -1;
    query += tableName + condition;
    int counter = 0;

    int cumulativeImageFileSize = 0;

    xResultSet rs = null;
    try {
      rs = executeSelect(query, true);
      boolean addMoreRows = true;

      boolean wasAnythingLoaded = false;
      while (rs.next() && addMoreRows) {
        counter++;

        DataRow row = new DataRow(tableName);
        for (ColumnType column : dataColumns) {
          if (column.getType() != ColumnType.Type.EXTERN_VALUE
              && column.getType() != ColumnType.Type.EXTERN_ID) {

            String string = rs.getString(column);
            if (needsId) {
              if (column.equals(ID)) {
                id = rs.getInt(ID);
              } else if (column.equals(DATABASE_ID)) {
                dbnumber = rs.getInt(DATABASE_ID);
              }
            }
            //TODO if image add image to row!

            //we have an image... now lets solve this...
            if (column.getType() == ColumnType.Type.FILE) {
              string = saveFile(rs.getBoolean(FILE_MUST_BE_UPDATED), row, string);
              cumulativeImageFileSize += string.length();
              if (cumulativeImageFileSize > AbstractConfiguration.MAX_UPLOAD_BATCH_SIZE) {
                addMoreRows = false;
                dataTable.setWhereAllEntriesLoaded(false);
              }
            }
            row.put(column, string);

          }
        }
        //load first entry anyway...
        if (addMoreRows || !wasAnythingLoaded) {
          dataTable.add(row);
        }
        wasAnythingLoaded = true;
      }
      if (needsId) {
        entry.setEntryKey(new Key(id, dbnumber));
      }
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException ex) {
          Logger.getLogger(AbstractEntryManager.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return dataTable;
  }


  public void deletePermanent(Key project, Key entry, int event)
      throws StatementNotExecutedException {
    String condition = "";
    if (event == AbstractQueryManager.DELETE_DELETED) {
      condition = " AND " + DELETED + "='" + IStandardColumnTypes.DELETED_YES + "' AND "
          + MESSAGE_NUMBER + "=" + -1;
    }
    String query = "DELETE  FROM " + databaseName + "." + tableName
        + " WHERE " + PROJECT_ID + "='" + project.getID()
        + "' AND " + PROJECT_DATABASE_ID + "=" + project.getDBID()
        + " AND " + ID + "=" + entry.getID()
        + " AND " + DATABASE_ID + "=" + entry.getDBID()
        + condition + ";";
    try {
      executeQuery(query);
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    }
  }

  public void updateEntries(EntryDataSet data) throws StatementNotExecutedException {
    String query = "";
    try {
      DataTableOld table = data.getOrCreateDataTable(tableName);
      if (table == null) {
        return;
      }
      for (DataRow entryData : table) {
        if (entryData.isEmpty()) {
          continue;
        }
//                ArrayList<DataColumn> dataList = new ArrayList<>();
        DataRow dataList = new DataRow(tableName);
        for (DataColumn row : entryData.iterator()) {

          dataList.add(row);
        }
        int status = updateData(dataList, primaryColumns);
        if (status == 0) {
          insertData(dataList);
          entryAdded(new EntryEvent(data.getProjectKey(), entryData));
        } else {
          entryUpdated(new EntryEvent(data.getProjectKey(), entryData));
        }
      }
    } catch (SQLException e) {
      throw new StatementNotExecutedException(query, e);
    } catch (NullPointerException ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Solves the Conflict by updating the given data
   *
   * @param entryData The data of the entry
   */
  public void solveConflict(EntryDataSet entryData) throws StatementNotExecutedException {
    DataTableOld table = entryData.getOrCreateDataTable(tableName);
    if (table == null) {
      return;
    }
    for (DataRow row : table) {
      if (row.isEmpty()) {
        continue;
      }
//            ArrayList<DataColumn> dataList = new ArrayList<>();
      DataRow dataList = new DataRow(tableName);
      dataList.add(new DataColumn(entryData.getProjectKey().getID(), PROJECT_ID));
      dataList.add(new DataColumn(entryData.getProjectKey().getDBID(), PROJECT_DATABASE_ID));
      dataList.add(new DataColumn(0, MESSAGE_NUMBER));
      if (entryData.getEntryKey() != null) {
        dataList.add(new DataColumn(entryData.getEntryKey().getID(), ID));
        dataList.add(new DataColumn(entryData.getEntryKey().getDBID(), DATABASE_ID));
      }
      for (DataColumn data : row.iterator()) {
        if (data.getColumnName().equals(MESSAGE_NUMBER.getColumnName())) {
          continue;
        }
        dataList.add(data);
      }
      int status = updateData(dataList, primaryColumns);
      if (status == 0) {
        insertData(dataList);
      }

    }
  }

  /**
   * Checks if the the given Entry is synchronized.
   *
   * @param entry The Entry to be checked
   * @return True if the entry is synchronized or does not exist. False otherwise
   */
  public boolean isSynchronised(EntryDataSet entry) throws StatementNotExecutedException {
    String query = "SELECT " + MESSAGE_NUMBER + " FROM " + databaseName + "." + tableName
        + " WHERE " + ID + "=" + entry.getEntryKey().getID()
        + " AND " + DATABASE_ID + "=" + entry.getEntryKey().getDBID()
        + " AND " + PROJECT_ID + "=" + entry.getProjectKey().getID()
        + " AND " + PROJECT_DATABASE_ID + "=" + entry.getProjectKey().getDBID()
        + " AND " + MESSAGE_NUMBER + "!=0";
    try {
      ResultSet rs = executeSelect(query);
      return !rs.next();
    } catch (SQLException ex) {
      throw new StatementNotExecutedException(query, ex);
    }
  }

  public void updateEntries(DataSetOld data) throws StatementNotExecutedException {
    updateEntries((EntryDataSet) data);
  }

  public boolean isSynchronised(DataSetOld entry) throws StatementNotExecutedException {
    return isSynchronised((EntryDataSet) entry);
  }

  public void solveConflict(DataSetOld entryData) throws StatementNotExecutedException {

    solveConflict((EntryDataSet) entryData);
  }

  /*************************************/
  public void load(EntryDataSet data) throws StatementNotExecutedException {

    EntryDataSet entryData = (EntryDataSet) data;
    String select = "*";
    List<ColumnType> columnList = new ArrayList<>();
    if (!dataColumns.isEmpty()) {
      select = "";
      for (ColumnType column : dataColumns) {
        if (column.getType() == ColumnType.Type.EXTERN_VALUE) {
          continue;
        }
        select += column.getColumnName() + ",";
        columnList.add(column);
      }
      select = select.substring(0, select.length() - 1);
    }
    String query = "SELECT " + select + " FROM " + databaseName + "." + tableName
        + " WHERE " + ID + "='" + entryData.getEntryKey().getID() + "'"
        + " AND " + DATABASE_ID + "=" + entryData.getEntryKey().getDBID()
        + " AND " + PROJECT_ID + "='" + entryData.getProjectKey().getID() + "'"
        + " AND " + PROJECT_DATABASE_ID + "=" + entryData.getProjectKey().getDBID()
        + " AND " + DELETED + "='" + IStandardColumnTypes.DELETED_NO + "';";
    try {
      xResultSet rs = executeSelect(query);
      ResultSetMetaData rsmd = rs.getMetaData();
      //System.out.println(queryManager);
      while (rs.next()) {
        DataTableOld result = entryData.getOrCreateDataTable(tableName);

        DataRow row = new DataRow(tableName);

        for (int i = 0; i < columnList.size(); i++) {
          ColumnType dataColumn = columnList.get(i);
          try {
            if (rs.getString(dataColumn) != null) {
              String value = rs.getString(dataColumn);
              if (java.sql.Types.TIMESTAMP == rsmd.getColumnType(i + 1)) {
                value = value.substring(0, value.length() - 2);
              }

              row.put(dataColumn, value);
            } else {
              row.put(dataColumn, "");
            }
          } catch (SQLException e) {
            //Date = 0000-00-00
            if (!e.getSQLState().equals("S1009")) {
              throw e;
            } else {
              e.printStackTrace();
            }
          }
        }
        //Originally had problems with h2 do to upper case
//                int columnCount = rsmd.getColumnCount();
//
//                for (int i = 0; i < columnCount; i++) {
//                    try {
//                        if (rs.getString(i + 1) != null) {
//                            String value = rs.getString(i + 1);
//                            if (java.sql.Types.TIMESTAMP == rsmd.getColumnType(i + 1)) {
//                                value = value.substring(0, value.length() - 2);
//                            }
//
//                            row.add(new DataColumn(value, tableName + "." + rsmd.getColumnName(i + 1)));
//                        } else {
//                            row.add(new DataColumn("", tableName + "." + rsmd.getColumnName(i + 1)));
//                        }
//                    } catch (SQLException e) {
//                        //Date = 0000-00-00
//                        if (!e.getSQLState().equals("S1009")) {
//                            throw e;
//                        } else {
//                            e.printStackTrace();
//                        }
//                    }
//                }
        result.add(row);
      }

    } catch (SQLException e) {

      throw new StatementNotExecutedException(query, e);
    }

  }

  /**
   * Saves the entry, this only works if the data type is String or Integer, otherwise this methode
   * should be overwritten
   */
  @Override
  public void save(DataSetOld data) throws StatementNotExecutedException {
    if (!(data instanceof EntryDataSet)) {
      Logger.getLogger(AbstractEntryManager.class.getName())
          .log(Level.SEVERE, "DataSet not EntryDataSet!");

      return;
    }
    save((EntryDataSet) data);
  }

  protected void save(EntryDataSet data) throws StatementNotExecutedException {
    DataTableOld dataTable = data.getOrCreateDataTable(tableName);
    if (dataTable == null) {
      return;
    }

    // update where key not in datatable list
    // insertOrUpdate(dataList, primaryColumns);
    ArrayList<DataRow> dataRows = new ArrayList<>();
    for (DataRow entryData : dataTable) {
      DataRow list = new DataRow(tableName);
      for (DataColumn dataColumn : entryData.iterator()) {
        if (primaryColumns.contains(tableName + "." + dataColumn.getColumnName())) {
          list.add(dataColumn);
        }
      }
      if (!list.isEmpty()) {
        dataRows.add(list);
      }
    }
    String filter = "";
    for (DataRow dataRow : dataRows) {
      filter += "(";
      for (DataColumn dataColumn : dataRow.iterator()) {
        filter += dataColumn.getColumnName() + "='" + dataColumn.getValue() + "' AND ";
      }
      if (filter.endsWith(" AND ")) {
        filter = filter.substring(0, filter.length() - " AND ".length()) + ") OR ";
      }
    }
    if (filter.endsWith(" OR ")) {
      filter = filter.substring(0, filter.length() - " OR ".length());
    }
    String query = "UPDATE " + databaseName + "." + tableName
        + " SET " + MESSAGE_NUMBER + "=" + 1 + ", " + DELETED + "='" + DELETED_YES + "'"
        + " WHERE " + ID + "=" + data.getEntryKey().getID()
        + " AND " + DATABASE_ID + "=" + data.getEntryKey().getDBID()
        + " AND " + PROJECT_ID + "=" + data.getProjectKey().getID()
        + " AND " + PROJECT_DATABASE_ID + "=" + data.getProjectKey().getDBID();
    boolean skipRemoving = false;
    if (!filter.isEmpty()) {
      query += " AND NOT (" + filter + ")";
    } else {
      //in basemanagers there is only one entry
      if (this instanceof AbstractBaseEntryManager) {
        skipRemoving = dataTable.size() != 0;
      }
    }

    if (!skipRemoving) {
      try {
        executeQuery(query);
      } catch (SQLException e) {
        throw new StatementNotExecutedException(query, e);
      }
    }

    for (DataRow entryData : dataTable) {
      if (entryData.isEmpty()) {
        continue;
      }
      DataRow dataList = new DataRow(tableName);
      dataList.add(new DataColumn(data.getEntryKey().getID(), ID));
      dataList.add(new DataColumn(data.getEntryKey().getDBID(), DATABASE_ID));
      dataList.add(new DataColumn(data.getProjectKey().getID(), PROJECT_ID));
      dataList.add(new DataColumn(data.getProjectKey().getDBID(), PROJECT_DATABASE_ID));
      for (DataColumn row : entryData.iterator()) {
        dataList.add(row);
      }
      dataList.add(new DataColumn(1, MESSAGE_NUMBER));
      dataList.add(new DataColumn(IStandardColumnTypes.DELETED_NO, DELETED));
      int status = updateData(dataList, primaryColumns);

      if (status == 0) {
        insertData(dataList);
        entryAdded(new EntryEvent(data.getProjectKey(), entryData));
      } else {
        entryUpdated(new EntryEvent(data.getProjectKey(), entryData));
      }
    }
  }


  /**
   * Returns the ExportData for the given Project with the given amount and given offset and given
   * Keys
   *
   * @param projects The project to retrieve the Data from
   * @return A Hashmap of ExportData mapped to EntryKey of the Entry
   */
  public ExportResult getEntryData(ExportResult result, ArrayList<Key> projects, Key entryKey,
      boolean forListing) throws StatementNotExecutedException {
    ArrayList<Key> entryList = new ArrayList<>();
    entryList.add(entryKey);
    return getEntryData(result, projects, null, ColumnType.ExportType.ALL, false, 0, 0, entryList,
        null,
        forListing);
  }

  /**
   * Returns the ExportData for the given Project with the given amount and given offset and given
   * Keys
   *
   * @param projects The project to retrieve the Data from
   * @return A Hashmap of ExportData mapped to EntryKey of the Entry
   */
  public ExportResult getEntryData(ExportResult result, ArrayList<Key> projects,
      ColumnType.ExportType type,
      boolean conflict, ArrayList<ColumnType> list,
      boolean forListing) throws StatementNotExecutedException {
    return getEntryData(result, projects, list, type, conflict, 0, 0, null, null, forListing);
  }

  /**
   * Returns the ExportData for the given Project and the given EntryKeys
   *
   * @param projects The Project to retrieve the data from
   * @param keys The Keys of the entries to retrieve the data from
   * @return A Hashmap of ExportData mapped to EntryKey of the Entry
   */
  public ExportResult getEntryData(ExportResult result, ArrayList<Key> projects,
      ArrayList<ColumnType> list,
      ColumnType.ExportType type, ArrayList<Key> keys,
      HashMap<ColumnType, SearchEntryInfo> filter,
      boolean forListing) throws StatementNotExecutedException {
    return getEntryData(result, projects, list, type, false, -1, -1, keys, filter, forListing);
  }

  /**
   * Returns the ExportData for the given Project with the given amount and given offset
   *
   * @param projects The project to retrieve the Data from
   * @param count The amount of data to load
   * @param offset The offset of the Data to retrieve
   * @return A Hashmap of ExportData mapped to EntryKey of the Entry
   */
  public ExportResult getEntryData(ExportResult result, ArrayList<Key> projects,
      ArrayList<ColumnType> list,
      ColumnType.ExportType type, boolean conflict, int count, int offset,
      HashMap<ColumnType, SearchEntryInfo> filter,
      boolean forListing) throws StatementNotExecutedException {
    return getEntryData(result, projects, list, type, conflict, count, offset, null, filter,
        forListing);
  }

  @Override
  protected void getRow(xResultSet rs, ExportResult data, UniqueArrayList<ColumnType> rowNames,
      ColumnType.ExportType exportType, boolean forListing) throws SQLException {
    try {
      int id = rs.getInt(ID);
      int db = rs.getInt(DATABASE_ID);

      Key entry = new Key(id, db);
      Key project = new Key(rs.getInt(PROJECT_ID), rs.getInt(PROJECT_DATABASE_ID));
      EntryKey entryKey = new EntryKey(entry, project);

      ExportRow exportData = data.getExportRow(entryKey);

      for (ColumnType columnName : rowNames) {
        getExportDataFromQuery(exportData, columnName, rs, exportType, forListing);
      }
    } catch (SQLException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }

  /**
   * Displays each row as onw column. Uses value of the base column as identifier
   *
   * @param data The export result where there data is stored
   * @param rs The result set
   * @param base The base column. This is used as identifier.
   * @param newGroupForRow If true, for each row a new group will be created. This should be used if
   * the result of the base may not be distinct enough.
   * @param types The different columns to be displayed. The name is extended with the value of the
   * base column
   */
  static protected void displayRowAsOwnColumn(ExportResult data, xResultSet rs, ColumnType base,
      ColumnType priorityColumn, boolean newGroupForRow,
      ColumnType... types) throws SQLException {
    int id = rs.getInt(ID);
    int db = rs.getInt(DATABASE_ID);

    Key entry = new Key(id, db);
    Key project = new Key(rs.getInt(PROJECT_ID), rs.getInt(PROJECT_DATABASE_ID));
    EntryKey entryKey = new EntryKey(entry, project);
    ExportRow exportData = data.getExportRow(entryKey);
    String baseValue = getResultFromColumnType(base, rs);
    if (baseValue == null) {
      baseValue = "";
    }
    int prio = 0;
    if (priorityColumn != null) {
      prio = Integer.valueOf(getResultFromColumnType(priorityColumn, rs));
    } else {
      prio = baseValue.hashCode();
    }
    String value = baseValue + ": ";
    int i = 0;
    int groupNumber = 0;
    boolean valueSet = false;
    for (ColumnType type : types) {
      String body = getResultFromColumnType(type, rs);
      int basePriority = type.getPriority();
      int inSubgroupPriority = i++;

      ColumnType columnName = new ColumnType(value + type.getColumnName(), type.getType(),
          type.getExportType())
          .setDisplayName(
              (newGroupForRow ? ((groupNumber + 1) + ". ") : "") + value + type.getDisplayName())
          .setPriority(base.getPriority(), prio, basePriority + groupNumber, inSubgroupPriority);

      //use same groupID for all values in this group.
      if (newGroupForRow && !valueSet) {
        while (exportData.existsExportColumn(columnName)) {

          int groupNumber1 = groupNumber++;
          columnName
              .setPriority(base.getPriority(), prio, basePriority + groupNumber, inSubgroupPriority)
              .setDisplayName((groupNumber + 1) + ". " + value + type.getDisplayName());

        }
        valueSet = true;
      }
      ExportColumn columnBody = exportData.getExportColumn(columnName);

      columnBody.addData(body);

    }

  }

  /**
   * Displays each row as onw column. Uses value of the base column as identifier
   *
   * @param data The export result where there data is stored
   * @param rs The result set
   * @param baseColumns The base column. This is used as identifier.
   * @param newGroupForRow If true, for each row a new group will be created. This should be used if
   * the result of the base may not be distinct enough.
   * @param types The different columns to be displayed. The name is extended with the value of the
   * base column
   */
  static protected void displayRowAsOwnColumn(ExportResult data, xResultSet rs,
      ArrayList<ColumnType> baseColumns,
      ColumnType prioColumn, boolean newGroupForRow,
      ColumnType... types) throws SQLException {
    int id = rs.getInt(ID);
    int db = rs.getInt(DATABASE_ID);

    Key entry = new Key(id, db);
    Key project = new Key(rs.getInt(PROJECT_ID), rs.getInt(PROJECT_DATABASE_ID));
    EntryKey entryKey = new EntryKey(entry, project);
    ExportRow exportData = data.getExportRow(entryKey);
    String baseValue = "";
    for (ColumnType baseColumn : baseColumns) {
      String resultFromColumnType = getResultFromColumnType(baseColumn, rs);
      if (resultFromColumnType != null) {
        baseValue += resultFromColumnType + " ";
      }
    }
    int prio = Integer.valueOf(getResultFromColumnType(prioColumn, rs));
    String value = ": " + baseValue;
    int i = 0;
    int groupNumber = 0;
    boolean valueSet = false;
    for (ColumnType type : types) {
      String body = getResultFromColumnType(type, rs);
      int basePriority = type.getPriority();
      int inSubgroupPriority = i++;

      ColumnType columnName = new ColumnType(value + type.getColumnName(), type.getType(),
          type.getExportType())
          .setDisplayName(type.getDisplayName() + value)
          .setPriority(basePriority, prio, groupNumber, inSubgroupPriority);

      //use same groupID for all values in this group.
      if (newGroupForRow && !valueSet) {
        while (exportData.existsExportColumn(columnName)) {

          int groupNumber1 = groupNumber++;
          columnName.setPriority(basePriority, prio, groupNumber1, i);

        }
        valueSet = true;
      }
      ExportColumn columnBody = exportData.getExportColumn(columnName);

      columnBody.addData(body);

    }

  }

  /**
   * Updates the database number if (for some reason) no database number was set
   *
   * @param newNumber The new number to be set
   */
  @Override
  public void updateDatabaseNumber(int newNumber) throws StatementNotExecutedException {

    ArrayList<DataColumn> data = new ArrayList<>();
    data.add(new DataColumn(newNumber, PROJECT_DATABASE_ID));
    ArrayList<DataColumn> key = new ArrayList<>();
    key.add(new DataColumn(-1, PROJECT_DATABASE_ID));
    updateDataWithKey(data, key);

    data = new ArrayList<>();
    data.add(new DataColumn(newNumber, DATABASE_ID));
    key = new ArrayList<>();
    key.add(new DataColumn(-1, DATABASE_ID));
    updateDataWithKey(data, key);
    dbId = newNumber;

  }

  public ArrayList<SolveConflictLineGroup> filterLinesExtended(
      ArrayList<SolveConflictLineGroup> lines) {
    ArrayList<SolveConflictLineGroup> filtered = new ArrayList<>();
    boolean isConflicted = false;
    for (SolveConflictLineGroup group : lines) {
      if (!group.getLines().isEmpty()) {
        SolveConflictLineGroup newGroup = new SolveConflictLineGroup(group.getTableName());
        newGroup.setLastSync(group.getLastSync());
        for (SolveConflictLine line : group.getLines()) {
          Collection<ColumnType> list = line.getKeys();
          if (list.contains(PROJECT_ID) || list.contains(PROJECT_DATABASE_ID)
              || list.contains(MESSAGE_NUMBER) || list.contains(STATUS)
              || list.contains(DELETED)) {
            continue;
          }
          if (!line.isSameValue()) {
            isConflicted = true;
          }
          newGroup.add(line);
        }
        filtered.add(newGroup);
      }
    }
    if (isConflicted) {
      return filtered;
    } else {
      return new ArrayList<>();
    }
  }

  @Override
  public final ArrayList<SolveConflictLineGroup> filterLines(
      ArrayList<SolveConflictLineGroup> lines) {
    ArrayList<SolveConflictLineGroup> filtered = new ArrayList<>();
    for (SolveConflictLineGroup group : lines) {
      SolveConflictLineGroup newGroup = new SolveConflictLineGroup(tableName);

      for (SolveConflictLine line : group.getLines()) {
        Collection<ColumnType> list = line.getKeys();

        if (list.contains(PROJECT_ID) || list.contains(PROJECT_DATABASE_ID)
            || list.contains(MESSAGE_NUMBER) || list.contains(STATUS)
            || list.contains(DELETED)) {
          continue;
        }
        if (continueForCustomValue(line.getFirstLocalValue(), line.getFirstGlobalValue())) {
          continue;
        }
        for (ColumnType column : dataColumns) {
          if (!list.contains(column)) {
            continue;
          }
          //only display conflicted and mandatory columns. Potentially add boolean to check if column shall
          // be displayed in conflict
          if (this instanceof AbstractBaseEntryManager) {
            AbstractBaseEntryManager abstractBaseEntryManager = (AbstractBaseEntryManager) this;
            ArrayList<ColumnType> conflictDisplayColumns = abstractBaseEntryManager
                .getConflictDisplayColumns();
            if (conflictDisplayColumns != null && conflictDisplayColumns.contains(column)) {
              newGroup.add(line);
              break;
            }
          }
          if (!line.isSameValue() || column.isMandatory()) {
            newGroup.add(line);
            break;

          }
        }
      }
      if (!newGroup.getLines().isEmpty()) {
        newGroup.setLastSync(group.getLastSync());
        newGroup.setDeletedGlobal(group.isDeletedGlobal());
        filtered.add(newGroup);

      }
    }
    return filtered;
  }


  protected boolean continueForCustomValue(HashMap<ColumnType, String> firstLocalValue,
      HashMap<ColumnType, String> firstGlobalValue) {
    return false;
  }


}
