package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper;


/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 13.01.2017.
 */
public abstract class AbstractSheet {
    public abstract String getName();

    public abstract AbstractCell[] getRow(int i);

    public abstract int getRows();

    public abstract AbstractCell getCell(int columnNumber, int j);
}
