//package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.xls;
//
//import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.AbstractCell;
//import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.AbstractSheet;
//import jxl.Sheet;
//
///**
// * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 13.01.2017.
// */
//public class ExcelSheet extends AbstractSheet{
//    private Sheet sheet;
//
//    public ExcelSheet(Sheet sheet) {
//
//        this.sheet = sheet;
//    }
//
//    @Override
//    public String getName() {
//        return sheet.getName();
//    }
//
//    @Override
//    public AbstractCell[] getRow(int i) {
//        return ExcelCell.getRowFromRow(sheet.getRow(i));
//    }
//
//    @Override
//    public int getRows() {
//        return sheet.getRows();
//    }
//
//    @Override
//    public AbstractCell getCell(int columnNumber, int j) {
//        return new ExcelCell(sheet.getCell(columnNumber,j));
//    }
//}
