//package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.xls;
//
//import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.AbstractSheet;
//import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.AbstractWorkbook;
//import jxl.Sheet;
//import jxl.Workbook;
//import jxl.WorkbookSettings;
//import jxl.read.biff.BiffException;
//
//import java.io.File;
//import java.io.IOException;
//
///**
// * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 13.01.2017.
// */
//public class ExcelWorkbook extends AbstractWorkbook {
//    private final Workbook workbook;
//
//    public ExcelWorkbook(File selectedFile) throws IOException {
//
//        WorkbookSettings wbs = new WorkbookSettings();
//        wbs.setEncoding("Cp1252");
//        try {
//            workbook = Workbook.getWorkbook(selectedFile, wbs);
//        } catch (BiffException e) {
//            throw new IOException(e.getMessage(),e.getCause());
//        }
//    }
//
//    @Override
//    public int getNumberOfSheets() {
//        return workbook.getNumberOfSheets();
//    }
//
//    @Override
//    public AbstractSheet getSheet(String sheetName) {
//        return new ExcelSheet(workbook.getSheet(sheetName));
//    }
//
//    @Override
//    public String[] getSheetNames() {
//        return workbook.getSheetNames();
//    }
//
//    public AbstractSheet[] getSheets(){
//        AbstractSheet[] sheets = new AbstractSheet[workbook.getNumberOfSheets()];
//        Sheet[] sheets1 = workbook.getSheets();
//        for (int i = 0; i < sheets1.length; i++) {
//            sheets[i] = new ExcelSheet(sheets1[i]);
//        }
//        return sheets;
//    }
//
//
//}
