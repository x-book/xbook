//package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.xls;
//
//import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.AbstractCell;
//import jxl.Cell;
//
///**
// * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 13.01.2017.
// */
//public class ExcelCell extends AbstractCell {
//    private Cell cell;
//
//    public ExcelCell(Cell cell) {
//
//        this.cell = cell;
//    }
//
//    public static AbstractCell[] getRowFromRow(Cell[] row) {
//        AbstractCell[] cells = new AbstractCell[row.length];
//        for (int i = 0; i < row.length; i++) {
//            cells[i] = new ExcelCell(row[i]);
//        }
//        return cells;
//    }
//
//    @Override
//    public String getContents() {
//        return cell.getContents();
//    }
//}
