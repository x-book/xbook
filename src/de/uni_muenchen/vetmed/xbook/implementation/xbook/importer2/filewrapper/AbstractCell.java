package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 13.01.2017.
 */
public abstract class AbstractCell {
    public abstract String getContents();
}
