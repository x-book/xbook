package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 13.01.2017.
 */
public abstract class AbstractWorkbook {

    public abstract int getNumberOfSheets();

    public abstract AbstractSheet getSheet(String sheetName);

    public static AbstractWorkbook getWorkbookForFile(File selectedFile) throws IOException {


//        return new ExcelWorkbook(selectedFile);
        return null;
    }

    public abstract String[] getSheetNames();
}
