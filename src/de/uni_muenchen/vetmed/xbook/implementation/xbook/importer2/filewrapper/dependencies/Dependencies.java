package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.dependencies;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 13.01.2017.
 */
public abstract class Dependencies {

    HashMap<String, ArrayList<Dependency>> dependsOn;
    HashMap<String, ArrayList<Dependency>> isDependentFor;

    public Dependencies() {
        for (Dependency dependency : getDependencies()) {
            addDependency(dependency);
        }
    }

    protected abstract ArrayList<Dependency> getDependencies();

    private void addDependency(Dependency dependency) {
        ArrayList<Dependency.Dep> deps = dependency.getDependentOn();
        for (Dependency.Dep dep : deps) {
            ArrayList<Dependency> dependencies = isDependentFor.get(dep.dependsOn);
            if (dependencies == null) {
                dependencies = new ArrayList<>();
                isDependentFor.put(dep.dependsOn, dependencies);
            }
            dependencies.add(dependency);
        }
        String depends = dependency.getDependentFor();

        ArrayList<Dependency> dependencies = dependsOn.get(depends);
        if (dependencies == null) {
            dependencies = new ArrayList<>();
            dependsOn.put(depends, dependencies);
        }
        dependencies.add(dependency);

    }

    public ArrayList<Dependency> getDependenciesForSheet(String sheetName) {
        return dependsOn.get(sheetName);
    }

    public ArrayList<Dependency> getDependenciesDependantOnSheet(String sheetName) {
        return isDependentFor.get(sheetName);
    }

    public ArrayList<Dependency> getDependencies(String sheetName) {
        ArrayList<Dependency> dependencies = new ArrayList<>();
        dependencies.addAll(dependsOn.get(sheetName));
        dependencies.addAll(isDependentFor.get(sheetName));
        return dependencies;
    }
}
