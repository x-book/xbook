package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.dependencies;

import java.util.ArrayList;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 13.01.2017.
 */
public class Dependency {

    public Dependency(ArrayList<Dep> dependentOn, String dependentFor) {
        this.dependentOn = dependentOn;
        this.dependentFor = dependentFor;
    }

    private ArrayList<Dep> dependentOn;

    private String dependentFor;

    public ArrayList<Dep> getDependentOn() {
        return dependentOn;
    }

    public String getDependentFor() {
        return dependentFor;
    }

    public static class Dep {
        String dependsOn;
        String fieldIDOn;
        String fieldIDTo;

        public String getDependsOn() {
            return dependsOn;
        }

        public String getFieldIDOn() {
            return fieldIDOn;
        }

        public String getFieldIDTo() {
            return fieldIDTo;
        }
    }
}
