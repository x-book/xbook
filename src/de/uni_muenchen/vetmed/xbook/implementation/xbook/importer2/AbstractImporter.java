package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2;

import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.CodeTableHashMap;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.AbstractCell;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.AbstractSheet;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.AbstractWorkbook;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer2.filewrapper.dependencies.Dependencies;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lohrer on 14.10.2016.
 */
public abstract class AbstractImporter extends AbstractContent {

    /* TODO Import vorgang sollte komplett hier abgebildet sein.
     * TODO Verknüpfung der tabellen sollte definiert werden.(wie ?)
     * TODO: Unterscheidung zwischen import in projekten und neuen Projekten
     * TODO: Alle werte in tabellen holen
     * TODO: Mapping der Spalten zu benötigten spalten.
     * TODO: Mapping der Werte in den einzelnen spalten zu Bekannten werten.
     * TODO: Optionen für andere Eingaben als Excel (Wrapper?)
    */
    private final AbstractController<?,?> controller;

    private final HashMap<String, HashMap<String, ColumnInformation>> sheets;

    private HashMap<String, String> sheetMapping = new HashMap<>();
    public HashMap<AbstractSheet, HashMap<String, CodeTableHashMap>> ids = new HashMap<>();

    protected final SimpleDateFormat inputDateFormat = new SimpleDateFormat("dd.MM.yyyy");
    protected final DateFormat outputDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    protected AbstractWorkbook workbook;
    protected Dependencies dependencies;

    public AbstractImporter(AbstractController controller) {
        this.controller = controller;
        sheets = getSheets();
        dependencies = getDependencies();
        init();
    }

    protected abstract Dependencies getDependencies();

    protected void saveEntry(EntryDataSet entry) throws StatementNotExecutedException, NotLoggedInException {
        ArrayList<String> tableNames = entry.getTableNames();
        for (IBaseManager table : controller.getLocalManager().getSyncTables()) {
            if (!tableNames.contains(table.getTableName())) {
                continue;
            }
            if (!(table instanceof BaseEntryManager)) {
                continue;
            }
            AbstractBaseEntryManager entrydata = (AbstractBaseEntryManager) table;

            entrydata.save(entry);

        }
    }

    private void loadFile() {
        //open file

        JFileChooser c = new JFileChooser();
        c.setAcceptAllFileFilterUsed(false);
        c.addChoosableFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extension = null;
                String s = f.getName();
                int i = s.lastIndexOf('.');

                if (i > 0 && i < s.length() - 1) {
                    extension = s.substring(i + 1).toLowerCase();
                }

                return extension != null && extension.equals("xls");
            }

            @Override
            public String getDescription() {
                return "Excel Files";
            }
        });

        int i = c.showOpenDialog(controller.getMainFrame());
        if (i != JFileChooser.APPROVE_OPTION) {
            return;
        }
        System.out.println(c.getSelectedFile());
        try {
            workbook = AbstractWorkbook.getWorkbookForFile(c.getSelectedFile());

        } catch (IOException e) {
            e.printStackTrace();
            Footer.displayError("Error Opening File", 10000);
            return;
        }
        for (String s : workbook.getSheetNames()) {
            System.out.println(s);
        }

//        if (workbook.getNumberOfSheets() != 3) {
//            Footer.displayError("Invalid number of Sheets required: 3, found " + workbook.getNumberOfSheets(), 10000);
//        }

        //TODO get name of sheets and contents and iterate
        //project check
        //TODO user has to map.

        //TODO check


    }

    /**
     * Checks if all column values are correct.
     * @return true if no error occurred and false if an error occurred
     */
    private boolean check() {
        for (Map.Entry<String, HashMap<String, ColumnInformation>> stringHashMapEntry : sheets.entrySet()) {
            AbstractSheet sheet = workbook.getSheet(sheetMapping.get(stringHashMapEntry.getKey()));
            HashMap<String, ColumnInformation> value = stringHashMapEntry.getValue();
            AbstractCell[] headlines = sheet.getRow(0);
            checkAndCreateMapping(sheet.getName(), value, headlines);
            if (checkSheet(sheet, value)) {
                return false;
            }
        }
        return true;
    }


    protected abstract HashMap<String, HashMap<String, ColumnInformation>> getSheets();


    protected boolean checkSheet(AbstractSheet sheet, HashMap<String, ColumnInformation> mapping) {

        //TODO let user map values?

        HashMap<String, CodeTableHashMap> value1 = new HashMap<>();
        ids.put(sheet, value1);
        for (Map.Entry<String, ColumnInformation> s : mapping.entrySet()) {
            ColumnType columnType = s.getValue().getColumnType();
            if (columnType == null) {
                continue;
            }
            if (columnType.getType() == ColumnType.Type.ID) {
                try {
                    ColumnInformation columnInformation = s.getValue();
                    if (columnInformation == null) {
                        Footer.displayError("no column Info for " + columnType, 10000);
                        return false;
                    }
                    value1.put(s.getKey(), controller.getHashedCodeTableEntriesByValue(columnInformation.columnType));
                } catch (NotLoggedInException | StatementNotExecutedException e) {
                    Footer.displayError(e.getMessage(), 10000);
                    return false;
                }
            }

//            System.out.println("Number of rows: " + sheet.getRows());
            for (int j = 1; j < sheet.getRows(); j++) {
                AbstractCell cell = sheet.getCell(mapping.get(s.getKey()).getColumnNumber(), j);
                String contents1 = cell.getContents();
                switch (columnType.getType()) {
                    case YES_NO_NONE:
                        String value = contents1.trim();
                        if (!value.isEmpty() && !value.toLowerCase().equals("ja") && !value.toLowerCase().equals("nein")) {
                            Footer.displayError("Invalid value: '" + contents1 + "' in field " + s.getKey() + "  Row " + (j + 1) + " in " + sheet.getName(), 10000);
                            return false;
                        }
                        break;
                    case DATE:
                        //check if can parse date
                        if (!contents1.trim().isEmpty()) {
                            try {
                                //check if input can be parsed
                                inputDateFormat.parse(contents1);
                            } catch (ParseException e) {
                                //if not check if only year
                                try {
                                    inputDateFormat.parse("01.01." + contents1);
                                } catch (ParseException ex) {
                                    Footer.displayError("Invalid value: '" + contents1 + "' in field " + s.getKey() + "  Row " + (j + 1) + " in " + sheet.getName());
                                    return false;
                                }
                            }
                        }

                        break;
                    case VALUE:
                        //TODO check numbers?

                        int length = contents1.length();
                        Integer allowedLength = columnType.getMaxInputLength();
                        if (allowedLength != null && length > allowedLength) {
                            Footer.displayError("Value too long (" + length + "/" + allowedLength + "): "
                                    + "Table: " + sheet.getName() + ", "
                                    + "Column: '" + s.getKey() + "', "
                                    + "Value: '" + contents1 + "', "
                                    + "Row: " + (j + 1), 10000);
                            return false;
                        }
                        break;
                    case ID:
                        if (!contents1.isEmpty()) {
                            String[] split = contents1.split("\\|");
                            for (String s1 : split) {
                                s1 = s1.trim();

                                if (!(value1.get(s.getKey())).containsKey(s1)) {
                                    Footer.displayError("No mapping for Columns: " + s.getKey() + " with value: " + s1 + " in Row " + (j + 1) + " in " + sheet.getName(), 10000);
                                    return false;
                                }
                            }
                        }
                        break;

                    default:
                        Footer.displayError("No Mapping for type: " + columnType.getType() + " with value: " + contents1 + " in field " + s + "  Row " + (j + 1) + " in " + sheet.getName());
                        return false;

                }

                if (columnType.isMandatory() && contents1.isEmpty() && !isValidValueForColumn(contents1, s.getKey())) {
                    Footer.displayError("Mandatory Field: " + s.getKey() + " not found in Row " + (j + 1) + " in " + sheet.getName(), 10000);
                    return false;
                }
            }
        }
        return true;
    }


//    private boolean checkNumber(Sheet sheet, HashMap<String, Integer> mapping) {
//        for (int j = 1; j < sheet.getRows(); j++) {
//            for (String s : yesNoIds) {
//
//                Cell cell = sheet.getCell(mapping.get(s), j);
//
//                if (!cell.getContents().isEmpty()) {
//                    try {
//                        Integer.parseInt(cell.getContents());
//                    } catch (NumberFormatException ex) {
//                        Footer.displayError("Invalid value: " + cell.getContents() + " in field " + s + "  Row " + (j + 1) + " in " + sheet.getName(), 10000);
//                        return true;
//                    }
//                }
//            }
//        }
//        return false;
//    }

//    protected boolean checkIds(Sheet sheet, HashMap<String, ColumnInformation> mapping, ArrayList<String> projectIDS, HashMap<String, CodeTableHashMap> ids) {
//        //get possible IDs
//
//        for (String columnType : projectIDS) {
//            try {
//                ColumnInformation columnInformation = mapping.get(columnType);
//                if (columnInformation == null) {
//                    Footer.displayError("no column Info for " + columnType, 10000);
//                    return true;
//                }
//                ids.put(columnType, controller.getHashedCodeTableEntriesByValue(columnInformation.columnType));
//            } catch (NotLoggedInException | StatementNotExecutedException e) {
//                Footer.displayError(e.getMessage(), 10000);
//                return true;
//            }
//        }
//        for (int j = 1; j < sheet.getRows(); j++) {
//            for (String s : projectIDS) {
//
//                Cell cell = sheet.getCell(mapping.get(s).getColumnNumber(), j);
//                String contents = cell.getContents();
//                if (!contents.isEmpty()) {
//                    String[] split = contents.split("\\|");
//                    for (String value : split) {
//                        value = value.trim();
//
//                        if (!ids.get(s).containsKey(value)) {
//                            Footer.displayError("No mapping for Columns: " + s + " with value: " + value + " in Row " + (j + 1) + " in " + sheet.getName(), 10000);
//                            return true;
//                        }
//                    }
//                }
//            }
//        }
//        return false;
//    }



    protected abstract boolean isValidValueForColumn(String contents, String s);

    protected boolean checkAndCreateMapping(String sheetName, HashMap<String, ColumnInformation> columns, AbstractCell[] headlines) {
        for (Map.Entry<String, ColumnInformation> column : columns.entrySet()) {
            boolean found = false;
            for (int j = 0; j < headlines.length; j++) {
                if (column.getKey().equals(headlines[j].getContents())) {
                    column.getValue().setColumnNumber(j);
                    found = true;
                    break;
                }

            }
            if (!found) {
                Footer.displayError("Required Column: " + column.getKey() + " not found in sheet name " + sheetName, 10000);
                return true;
            }
        }
        return false;
    }

    /****** GUI STUFF *****/
    @Override
    protected JPanel getContent() {
        JPanel jPanel = new JPanel();
        JButton load = new JButton("Load");
        load.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadFile();
            }
        });
        jPanel.add(load);
        return jPanel;
    }

    @Override
    public ButtonPanel getButtonBar() {
        return null;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    /************************/
    public static class ColumnInformation {

        private ColumnType columnType;
        private int columnNumber;

        public ColumnInformation(int columnNumber) {
            this.columnNumber = columnNumber;
        }

        public ColumnInformation(ColumnType columnType) {
            this.columnType = columnType;
        }

        public void setColumnType(ColumnType columnType) {
            this.columnType = columnType;
        }

        public void setColumnNumber(int columnNumber) {
            this.columnNumber = columnNumber;
        }

        public ColumnType getColumnType() {
            return columnType;
        }

        public int getColumnNumber() {
            return columnNumber;
        }
    }

}
