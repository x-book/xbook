package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 * An input field which is only used by the measurement input screen.
 *
 * @author Daniel Kaltenthaler
 */
public class NumericValueField extends JPanel {

    /**
     * The text field where to enter the values.
     */
    private JTextField textField;

    /**
     * Constructor of the MeasurementInputField class.
     *
     * @param title The title which is displayed above of the input field.
     * @param id The ID of the measurement field.
     */
    public NumericValueField(String title, String id) {
        setBorder(new LineBorder(new Color(190, 190, 190), 1));
        setBackground(Colors.INPUT_FIELD_BACKGROUND);
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setPreferredSize(new Dimension(Sizes.INPUT_GRID_SIZE.width, Sizes.INPUT_GRID_SIZE.height - 30));

        JPanel contentPanel = new JPanel(new BorderLayout());
        contentPanel.setBorder(new LineBorder(new Color(230, 230, 230), 1));
        textField = new JTextField();
        textField.setBorder(new EmptyBorder(3, 3, 3, 3));
        textField.setName(id);
        textField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent event) {
                if (!(textField.getText().isEmpty())) {
                    checkValue();
                }
                textField.setBackground(Color.WHITE);
            }

            @Override
            public void focusGained(FocusEvent e) {
                textField.setBorder(new JTextField().getBorder());
            }
        });
        contentPanel.add(BorderLayout.CENTER, textField);

        JLabel mmLabel = new JLabel("mm");
        mmLabel.setBackground(Colors.INPUT_FIELD_BACKGROUND);
        mmLabel.setBorder(new EmptyBorder(0, 6, 0, 0));

        // put all items together
        JLabel labelTitle = new JLabel(title);
        labelTitle.setBackground(Colors.BUTTON_PANEL_BACKGROUND);
        mainPanel.add(BorderLayout.NORTH, labelTitle);
        mainPanel.add(BorderLayout.CENTER, contentPanel);
        mainPanel.add(BorderLayout.EAST, mmLabel);
        add(mainPanel);
    }

    /**
     * Returns the textfield object.
     *
     * @return The textfield object.
     */
    public JTextField getTextField() {
        return textField;
    }

    /**
     * Get the data as a float value.
     *
     * @return The Float value.
     */
    public String getValue() {
        return checkValue();
    }

    /**
     * Checks if the current input is a valid Float value. Replaces commata by dots. If it is no valid value, clear the
     * input field and return null. Return the float value if it is a valid value.
     *
     * @return The Float value of the input.
     */
    private String checkValue() {
        String value = textField.getText().replace(",", ".");
        try {
            // check if it is a valid float value ...
            Float.parseFloat(value);
            // ... remove the value if it is zero ...
            if (value.equals("0") || value.equals("0.0")) {
                textField.setText("");
            }
            // ... and return the value as string to avoid tranformations of the value.
            return value;
        } catch (NumberFormatException e) {
            textField.setText("");
            textField.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
            Footer.displayError(Loc.get("NO_VALID_INPUT_ONLY_FLOAT_VALUES"));
        }
        return null;
    }

    @Override
    public boolean requestFocusInWindow() {
        return textField.requestFocusInWindow();
    }

    @Override
    public String toString() {
        if (textField != null) {
            return getTextField().getName() + ":" + textField.getText();
        }
        return super.toString();
    }

}
