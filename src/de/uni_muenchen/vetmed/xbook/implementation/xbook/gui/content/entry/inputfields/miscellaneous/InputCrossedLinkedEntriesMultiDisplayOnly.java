package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractCrossLinkedManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class InputCrossedLinkedEntriesMultiDisplayOnly extends InputCrossedLinkedEntriesMulti {

    private static Log LOGGER = LogFactory.getLog(InputCrossedLinkedEntriesMultiDisplayOnly.class);

    /**
     * Constructor.
     *
     * @param entry          The corresponding Entry object.
     * @param manager        The corresponding manager.
     * @param mappingManager The corresponding cross linked manager.
     * @param isMandatory    Whether the input field is a mandatory field or not.
     */
    public InputCrossedLinkedEntriesMultiDisplayOnly(AbstractEntry entry, AbstractBaseEntryManager manager,
                                                     AbstractCrossLinkedManager mappingManager, boolean isMandatory) {
        super(entry, manager, mappingManager, isMandatory);
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();
        setRememberValueFieldDisabled();
        getAddButton().setVisible(false);
        getRemoveButton().setVisible(false);
        getListSelectedItems().setEnabled(true);
    }
}
