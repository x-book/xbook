package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public enum RightMode {

    USER_TO_PROJECT, GROUP_TO_PROJECT, RANK_TO_GROUP, USER_TO_GROUP,GENERAL_RIGHTS
}
