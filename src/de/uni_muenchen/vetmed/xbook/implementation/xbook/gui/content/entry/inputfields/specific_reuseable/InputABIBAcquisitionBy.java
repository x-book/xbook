package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.specific_reuseable;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.HierarchicComboBox;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An input field that allows entering the "acquisition by" information in
 * ArchaeoBook.
 * <p/>
 * In general there are three modes available: Entering information of
 * governmental institution, of other institutions and of private persons.
 * <p/>
 * Entering governmental institution makes possible to add further information
 * in a single textfield. Entering other institutions or provate persons makes
 * possible to add the name, adress, contact information and other informations
 * in several textfields.
 *
 * @author Daniel Kaltenthaler
 * <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputABIBAcquisitionBy extends AbstractInputElement {

    private final static int NONE = 0;
    private final static int STATE_INSTITUTION = 1;
    private final static int OTHER_INSTITUTION = 2;
    private final static int PRIVATE_PERSON = 3;
    /**
     * The default height for the text fields.
     */
    private final int HEIGHT_TEXTFIELDS = Sizes.INPUT_FIELD_HEIGHT - 5;
    /**
     * The textfield for the name.
     */
    private JTextField textfieldName;
    /**
     * The textfield for the first line of the adress.
     */
    private JTextField textfieldAddress1;
    /**
     * The textfield for the second line of the adress.
     */
    private JTextField textfieldAddress2;
    /**
     * The textfield for mail adress.
     */
    private JTextField textfieldEmail;
    /**
     * The textfield for "other information".
     */
    private JTextField textfieldOthers;
    /**
     * The label for the name.
     */
    private JLabel labelName;
    /**
     * The label for the institution.
     */
    private JLabel labelInstitution;
    /**
     * The label for the adress.
     */
    private JLabel labelAdress;
    /**
     * The label for the adress2 (REGION).
     */
    private JLabel labelAdress2;
    /**
     * The label for the email adress.
     */
    private JLabel labelMail;
    /**
     * The label for the "other information".
     */
    private JLabel labelOthers;
    /**
     * The outter wrapper of the input field.
     */
    private JPanel outerWrapper;
    /**
     * The combo box object.
     */
    private JComboBox combo;
    private HierarchicComboBox comboInstitution;
    private CodeTableHashMap dataFirstCombo;

    private final String tableName;
    private final ColumnType columnTypeAcquiredFrom;
    private final ColumnType columnTypeAcquiredFromName;
    private final ColumnType columnTypeAcquiredFromAdress1;
    private final ColumnType columnTypeAcquiredFromAdress2;
    private final ColumnType columnTypeAcquiredFromEmail;
    private final ColumnType columnTypeAcquiredFromOtherInformation;
    private final ColumnType columnTypeAcquiredFromInstitutionID;

    /**
     * Constructor.
     *
     * @param tableName
     * @param columnTypeAcquiredFrom
     * @param columnTypeAcquiredFromName
     * @param columnTypeAcquiredFromAdress1
     * @param columnTypeAcquiredFromOtherInformation
     * @param columnTypeAcquiredFromAdress2
     * @param columnTypeAcquiredFromEmail
     * @param columnTypeAcquiredFromInstitutionID
     */
    public InputABIBAcquisitionBy(String tableName, ColumnType columnTypeAcquiredFrom, ColumnType columnTypeAcquiredFromName, ColumnType columnTypeAcquiredFromAdress1, ColumnType columnTypeAcquiredFromAdress2, ColumnType columnTypeAcquiredFromEmail, ColumnType columnTypeAcquiredFromOtherInformation, ColumnType columnTypeAcquiredFromInstitutionID) {
        super(columnTypeAcquiredFrom);

        this.columnTypeAcquiredFrom = columnTypeAcquiredFrom;
        this.columnTypeAcquiredFromName = columnTypeAcquiredFromName;
        this.columnTypeAcquiredFromAdress1 = columnTypeAcquiredFromAdress1;
        this.columnTypeAcquiredFromAdress2 = columnTypeAcquiredFromAdress2;
        this.columnTypeAcquiredFromEmail = columnTypeAcquiredFromEmail;
        this.columnTypeAcquiredFromOtherInformation = columnTypeAcquiredFromOtherInformation;
        this.columnTypeAcquiredFromInstitutionID = columnTypeAcquiredFromInstitutionID;

        this.tableName = tableName;

        setGridX(2);
        setGridY(4);
        setSidebar(new SidebarEntryField(columnTypeAcquiredFrom, Loc.get("SIDEBAR_ENTRY>ACQUIRED_FROM")));
    }

    /**
     * Set all labels and all textfields visible or not.
     *
     * @param state <code>true</code> to set them visible, <code>false</code>
     * else.
     */
    private void setAllVisible(boolean state) {

        labelInstitution.setVisible(state);
        comboInstitution.setVisible(state);
        labelName.setVisible(state);
        textfieldName.setVisible(state);
        labelAdress.setVisible(state);
        textfieldAddress1.setVisible(state);
        labelAdress2.setVisible(false); // Hack
        textfieldAddress2.setVisible(state);
        labelMail.setVisible(state);
        textfieldEmail.setVisible(state);
        labelOthers.setVisible(state);
        textfieldOthers.setVisible(state);
    }

    private void setRestrictedVisible(boolean state) {

        //displayLabel(labelAdress2 = new JLabel(Loc.get("REGION")));            
        labelInstitution.setVisible(state);
        comboInstitution.setVisible(state);
        labelName.setVisible(state);
        textfieldName.setVisible(state);

        labelAdress2.setVisible(state);
        textfieldAddress2.setVisible(state);
        state = false;
        labelAdress.setVisible(state);
        textfieldAddress1.setVisible(state);
        labelMail.setVisible(state);
        textfieldEmail.setVisible(state);
        labelOthers.setVisible(state);
        textfieldOthers.setVisible(state);
    }

    /**
     * Format and display a text label for the description of the textfields.
     *
     * @param label The JLabel object that should be displayed.
     */
    private void displayLabel(JLabel label) {
        label.setBorder(BorderFactory.createEmptyBorder(6, 0, 0, 0));
        outerWrapper.add(label);
    }

    /**
     * Format and display a text field.
     *
     * @param textField The JTextField object that should be displayed.
     */
    private void displayTextField(JTextField textField) {
        displayTextField(textField, false);
    }

    /**
     * Format and display a text field.
     *
     * @param textField The JTextField object that should be displayed.
     * @param hasButtomBorder <code>true</code> to add a small empty border to
     * the buttom, <code>false</code> else.
     */
    private void displayTextField(final JTextField textField, boolean hasButtomBorder) {
        JPanel innerWrapper = new JPanel(new BorderLayout());
        if (hasButtomBorder) {
            innerWrapper.setBorder(BorderFactory.createEmptyBorder(0, 0, 6, 0));
        }
        textField.setSize(new Dimension(1, HEIGHT_TEXTFIELDS));
        textField.setPreferredSize(new Dimension(1, HEIGHT_TEXTFIELDS));
        textField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (textField.getText().isEmpty()) {
                    textField.setToolTipText(null);
                } else {
                    textField.setToolTipText(textField.getText());
                }
            }
        });
        innerWrapper.add(BorderLayout.CENTER, textField);
        outerWrapper.add(innerWrapper);
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(tableName);
        String mode = null;
        String name = null;
        String address1 = null;
        String address2 = null;
        String email = null;
        String others = null;

        mode = list.get(columnTypeAcquiredFrom);
        name = list.get(columnTypeAcquiredFromName);
        address1 = list.get(columnTypeAcquiredFromAdress1);
        address2 = list.get(columnTypeAcquiredFromAdress2);
        email = list.get(columnTypeAcquiredFromEmail);
        others = list.get(columnTypeAcquiredFromOtherInformation);

        try {
            clearAll();
            Integer modeId = Integer.parseInt(mode);
            combo.setSelectedItem(dataFirstCombo.convertIdAsStringToString(mode));
            setElementsVisible(modeId);
            if (modeId == STATE_INSTITUTION) {
                comboInstitution.load(data);
                textfieldOthers.setText(others);
            } else if (modeId == OTHER_INSTITUTION || modeId == PRIVATE_PERSON) {
                textfieldName.setText(name);
                textfieldAddress1.setText(address1);
                textfieldAddress2.setText(address2);
                textfieldEmail.setText(email);
                textfieldOthers.setText(others);
            }
        } catch (NumberFormatException nfe) {
            setElementsVisible(NONE);
        }

    }

    /**
     * Clear all text fields.
     */
    private void clearAll() {
        comboInstitution.clear();
        textfieldName.setText("");
        textfieldAddress1.setText("");
        textfieldAddress2.setText("");
        textfieldEmail.setText("");
        textfieldOthers.setText("");
    }

    @Override
    public void clear() {
        combo.setSelectedIndex(NONE);
        setAllVisible(false);
        clearAll();
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(tableName);

        Integer modeId = dataFirstCombo.convertStringToIdAsInteger((String) combo.getSelectedItem());
        if (modeId == null) {
            modeId = NONE;
        }
        if (modeId == STATE_INSTITUTION) {
            list.add(new DataColumn(modeId + "", columnTypeAcquiredFrom.getColumnName()));
            comboInstitution.save(data);
            list.add(new DataColumn("", columnTypeAcquiredFromName.getColumnName()));
            list.add(new DataColumn("", columnTypeAcquiredFromAdress1.getColumnName()));
            list.add(new DataColumn("", columnTypeAcquiredFromAdress2.getColumnName()));
            list.add(new DataColumn("", columnTypeAcquiredFromEmail.getColumnName()));
            list.add(new DataColumn(textfieldOthers.getText(), columnTypeAcquiredFromOtherInformation.getColumnName()));
        } else if (modeId == OTHER_INSTITUTION || modeId == PRIVATE_PERSON) {
            list.add(new DataColumn(modeId + "", columnTypeAcquiredFrom.getColumnName()));
            list.add(new DataColumn("-1", columnTypeAcquiredFromInstitutionID.getColumnName()));
            list.add(new DataColumn(textfieldName.getText(), columnTypeAcquiredFromName.getColumnName()));
            list.add(new DataColumn(textfieldAddress1.getText(), columnTypeAcquiredFromAdress1.getColumnName()));
            list.add(new DataColumn(textfieldAddress2.getText(), columnTypeAcquiredFromAdress2.getColumnName()));
            list.add(new DataColumn(textfieldEmail.getText(), columnTypeAcquiredFromEmail.getColumnName()));
            list.add(new DataColumn(textfieldOthers.getText(), columnTypeAcquiredFromOtherInformation.getColumnName()));
        } else /* if (modeId == NONE) */ {
            list.add(new DataColumn("-1", columnTypeAcquiredFrom.getColumnName()));
            list.add(new DataColumn("-1", columnTypeAcquiredFromInstitutionID.getColumnName()));
            list.add(new DataColumn("", columnTypeAcquiredFromName.getColumnName()));
            list.add(new DataColumn("", columnTypeAcquiredFromAdress1.getColumnName()));
            list.add(new DataColumn("", columnTypeAcquiredFromAdress2.getColumnName()));
            list.add(new DataColumn("", columnTypeAcquiredFromEmail.getColumnName()));
            list.add(new DataColumn("", columnTypeAcquiredFromOtherInformation.getColumnName()));
        }
    }

    @Override
    public String getStringRepresentation() {
        String comboInput = (String) combo.getSelectedItem();
        if (comboInput.isEmpty()) {
            return "";
        }
        Integer modeId = dataFirstCombo.convertStringToIdAsInteger(comboInput);
        if (modeId == STATE_INSTITUTION) {
            return comboInput + " / " + comboInstitution.inputToString() + " / " + textfieldOthers.getText();
        } else if (modeId == OTHER_INSTITUTION || modeId == PRIVATE_PERSON) {
            return comboInput + " / " + combo.getSelectedItem() + " / " + textfieldName.getText() + " / " + textfieldAddress1.getText() + " / " + textfieldAddress2.getText() + " / " + textfieldEmail.getText() + " / " + textfieldOthers.getText();
        }
        return "";
    }

    private void setElementsVisible(Integer modeId) {
        if (modeId == null) {
            modeId = NONE;
        }
        if (modeId == STATE_INSTITUTION) {
            setAllVisible(false);
            labelInstitution.setVisible(true);
            comboInstitution.setVisible(true);
            labelOthers.setVisible(true);
            textfieldOthers.setVisible(true); // 
            clearAll();
        } else if (modeId == OTHER_INSTITUTION) {
            setAllVisible(true);
            labelInstitution.setVisible(false);
            comboInstitution.setVisible(false);
            labelName.setText(Loc.get("INSTITUTION_NAME"));
            clearAll();
        } else if (modeId == PRIVATE_PERSON) {
            try {
                if (apiControllerAccess.isProjectOwner()) {
                    setAllVisible(true);
                } else {
                    setRestrictedVisible(true);
                }
            } catch (NotLoggedInException ex) {
                Logger.getLogger(InputABIBAcquisitionBy.class.getName()).log(Level.SEVERE, null, ex);
            } catch (StatementNotExecutedException ex) {
                Logger.getLogger(InputABIBAcquisitionBy.class.getName()).log(Level.SEVERE, null, ex);
            }
            labelInstitution.setVisible(false);
            comboInstitution.setVisible(false);
            labelName.setText(Loc.get("FIRST_LAST_NAME"));
            clearAll();
        } else { // if (modeId == NONE) {
            setAllVisible(false);
        }
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(comboInstitution);
        comp.add(combo);
        comp.add(textfieldName);
        comp.add(textfieldAddress1);
        comp.add(textfieldAddress2);
        comp.add(textfieldEmail);
        comp.add(textfieldOthers);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return comboInstitution.getSelectedInput() != null;
    }

    @Override
    protected void createFieldInput() {
        outerWrapper = new JPanel(new StackLayout());
        outerWrapper.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        try {
            dataFirstCombo = apiControllerAccess.getHashedCodeTableEntries(columnTypeAcquiredFrom);
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            dataFirstCombo = new CodeTableHashMap();
            Logger.getLogger(InputABIBAcquisitionBy.class.getName()).log(Level.SEVERE, null, ex);
        }
        combo = new JComboBox(dataFirstCombo.getValuesAsArray(true));
        combo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    Integer modeId = dataFirstCombo.convertStringToIdAsInteger((String) combo.getSelectedItem());
                    setElementsVisible(modeId);
                    clearAll();
                }
            }
        });
        combo.setSize(new Dimension(1, Sizes.INPUT_FIELD_HEIGHT));
        combo.setPreferredSize(new Dimension(1, Sizes.INPUT_FIELD_HEIGHT));
        combo.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (combo.getSelectedItem().toString().isEmpty()) {
                    combo.setToolTipText(null);
                } else {
                    combo.setToolTipText(combo.getSelectedItem().toString());
                }
            }
        });
        outerWrapper.add(combo);

        displayLabel(labelInstitution = new JLabel(Loc.get("INSTITUTION_NAME")));
        outerWrapper.add(comboInstitution = new HierarchicComboBox(apiControllerAccess, columnTypeAcquiredFromInstitutionID));

        displayLabel(labelName = new JLabel(Loc.get("FIRST_LAST_NAME")));
        displayTextField(textfieldName = new JTextField());

        displayLabel(labelAdress = new JLabel(Loc.get("ADRESS")));
        displayTextField(textfieldAddress1 = new JTextField(), true);

        displayLabel(labelAdress2 = new JLabel(Loc.get("REGION")));
        displayTextField(textfieldAddress2 = new JTextField());

        displayLabel(labelMail = new JLabel(Loc.get("EMAIL")));
        displayTextField(textfieldEmail = new JTextField());

        displayLabel(labelOthers = new JLabel(Loc.get("OTHER")));
        displayTextField(textfieldOthers = new JTextField());
        //
        setAllVisible(false);
        setContent(outerWrapper);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }



}
