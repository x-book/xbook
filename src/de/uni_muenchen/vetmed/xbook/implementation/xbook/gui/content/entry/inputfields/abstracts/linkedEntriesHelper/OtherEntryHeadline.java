package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.linkedEntriesHelper;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class OtherEntryHeadline extends JPanel {

    /**
     * The default height of one line.
     */
    private static final int LINE_HEIGHT = 30;

    public OtherEntryHeadline(ArrayList<VisualColumnHelper> headlines) {
        setLayout(new GridLayout(1, headlines.size() - 6));
        setPreferredSize(new Dimension(1, LINE_HEIGHT));

        JLabel spacer1 = new JLabel("");
        spacer1.setPreferredSize(new Dimension(40, spacer1.getMinimumSize().height));
        add(spacer1);
        JLabel spacer2 = new JLabel("");
        spacer2.setPreferredSize(new Dimension(40, spacer2.getMinimumSize().height));
        add(spacer2);
        for (VisualColumnHelper b : headlines) {
            JComponent comp = b.getElement();
            comp.setPreferredSize(new Dimension(b.getWidth(), comp.getMinimumSize().height));
            add(comp);
        }
    }
}
