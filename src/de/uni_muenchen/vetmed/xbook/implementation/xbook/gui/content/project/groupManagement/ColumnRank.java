package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.groupManagement;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rank;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rights;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.GroupManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Result;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ColumnRank extends AbstractGroupManagementColumn {

    protected final GroupManagement groupManagement;
    private JPanel panel;
    private JComboBox comboRankSelection;
    private ButtonPanel rankPanel;
    private JPanel selectRankPanel;
    private JPanel input;
    private final ArrayList<JCheckBox> allCheckBoxes;
    private ArrayList<Rank> ranks;
    private final JTextField textfieldRankName;
    private boolean hasEditGroupRights = false;

    private JCheckBox checkReadRight;
    private JCheckBox checkWriteRight;
    private JCheckBox checkProjectEditRight;

    public ColumnRank(GroupManagement groupManagement, AbstractMainFrame mainFrame) {
        super(mainFrame);
        this.groupManagement = groupManagement;
        allCheckBoxes = new ArrayList<>();
        textfieldRankName = new JTextField();
        textfieldRankName.setPreferredSize(new Dimension(1, Sizes.BUTTON_HEIGHT));

        createCustomContent();
        update();
        input.setVisible(false);

    }

    @Override
    protected String getTitle() {
        return Loc.get("EDIT_RANK");
    }

    @Override
    protected JScrollPane getContent() {
        panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        JScrollPane scroll = new JScrollPane();
        scroll.setViewportView(ComponentHelper.wrapComponent(panel, Colors.CONTENT_BACKGROUND, 0, 10, 0, 10));
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        return scroll;
    }

    @Override
    protected ButtonPanel getButtonBar() {
        rankPanel = new ButtonPanel();

        XImageButton button = new XImageButton(Loc.get("CREATE_RANK"));
        button.setWidth(200);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionOnCreateRank();
            }
        });
        rankPanel.addTo(ButtonPanel.Position.SOUTH_EAST, button);

        return rankPanel;
    }

    protected void actionOnCreateRank() {
        mainFrame.displayProjectUserRightsCreateRankScreen(groupManagement);
    }

    protected void displayRightManagementScreen() {
        mainFrame.displayGroupManagementScreen(groupManagement.getSelectedGroup());

    }

    private void createCustomContent() {
        selectRankPanel = new JPanel(new StackLayout());
        selectRankPanel.setBackground(Colors.CONTENT_BACKGROUND);

        selectRankPanel.add(new JLabel(Loc.get("SELECT_RANK_TO_EDIT") + ":"));
        comboRankSelection = new JComboBox();
        comboRankSelection.setPreferredSize(new Dimension(1, Sizes.BUTTON_HEIGHT));
        comboRankSelection.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (ranks != null) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        try {
                            textfieldRankName.setText("");
                            if (comboRankSelection.getSelectedItem().equals("")) {
                                input.setVisible(false);
                            } else {
                                input.setVisible(true);
                            }
                            if (getSelectedRank() != null) {
                                textfieldRankName.setText(getSelectedRank().getName());

                                ArrayList<Rights> rights = mainFrame.getController().getRankRights(groupManagement.getSelectedGroup().getId(), getSelectedRank().getId());
                                for (JCheckBox check : allCheckBoxes) {
                                    for (Rights right : rights) {
                                        if (right.getColumnType().getColumnName().equals(check.getName())) {
                                            check.setSelected(right.hasRight());
                                            break;
                                        }
                                    }

                                }
                            }
                        } catch (NotLoggedInException | StatementNotExecutedException ex) {
                            Logger.getLogger(ColumnRank.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        });
        JPanel comboWrapper = new JPanel(new BorderLayout());
        comboWrapper.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 0));
        comboWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        comboWrapper.add(BorderLayout.CENTER, comboRankSelection);
        selectRankPanel.add(comboWrapper);

        input = new JPanel(new StackLayout());
        input.setBackground(Colors.CONTENT_BACKGROUND);
        selectRankPanel.add(input);

        panel.add(selectRankPanel);

    }

    private Rank getSelectedRank() {
        for (Rank rank : ranks) {
            if (rank.getName().equals(comboRankSelection.getSelectedItem())) {
                return rank;
            }
        }
        ;
        return null;
    }

    private ArrayList<Rights> getSelectedRights() {
        ArrayList<Rights> rights = new ArrayList<>();
        for (JCheckBox check : allCheckBoxes) {
            ColumnType col = new ColumnType(check.getName(), ColumnType.Type.VALUE, ColumnType.ExportType.NONE);
            col.setDisplayName(check.getText());
            rights.add(new Rights(col, check.isSelected()));
        }
        return rights;
    }

    protected void update() {
        try {
            if (groupManagement.getSelectedGroup() == null) {
                hasEditGroupRights = false;
            } else {
                hasEditGroupRights = mainFrame.getController().hasEditGroupRights(groupManagement.getSelectedGroup().getId());
            }
        } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException ex) {
            Logger.getLogger(ColumnUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        comboRankSelection.removeAllItems();
        input.removeAll();
        allCheckBoxes.clear();
        textfieldRankName.setText("");
        if (!hasEditGroupRights || groupManagement.getSelectedGroup() == null) {
            rankPanel.setVisible(false);
            selectRankPanel.setVisible(false);
        } else {
            rankPanel.setVisible(true);
            selectRankPanel.setVisible(true);
            try {
                comboRankSelection.addItem("");
                comboRankSelection.setSelectedItem("");
                ranks = mainFrame.getController().getRankOfGroup(groupManagement.getSelectedGroup().getId());
                for (Rank rank : ranks) {
                    comboRankSelection.addItem(rank.getName());
                }
                input.add(new JLabel(Loc.get("RANK_NAME") + ":"));
                input.add(textfieldRankName);
                JLabel labelRankRights = new JLabel(Loc.get("SET_RANK_RIGHTS") + ":");
                labelRankRights.setBorder(BorderFactory.createEmptyBorder(12, 0, 6, 0));
                input.add(labelRankRights);
                for (final ColumnType right : mainFrame.getController().getAvailableGroupRights()) {
                    JCheckBox check = new JCheckBox(right.getDisplayName());
                    check.setBackground(Colors.CONTENT_BACKGROUND);
                    check.setName(right.getColumnName());

                    // save the general rights check boxes to the variables
                    if (right.equals(GroupManager.GROUP_RIGHTS_READ)) {
                        check.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                if (!checkReadRight.isSelected()) {
                                    checkWriteRight.setSelected(false);
                                    checkProjectEditRight.setSelected(false);
                                }
                            }
                        });
                        checkReadRight = check;
                    } else if (right.equals(GroupManager.GROUP_RIGHTS_WRITE)) {
                        check.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                if (checkWriteRight.isSelected()) {
                                    checkReadRight.setSelected(true);
                                } else {
                                    checkProjectEditRight.setSelected(false);
                                }
                            }
                        });
                        checkWriteRight = check;
                    } else if (right.equals(GroupManager.GROUP_RIGHTS_EDIT_PROJECT)) {
                        check.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                if (checkProjectEditRight.isSelected()) {
                                    checkReadRight.setSelected(true);
                                    checkWriteRight.setSelected(true);
                                }
                            }
                        });
                        checkProjectEditRight = check;
                    }

                    input.add(check);
                    allCheckBoxes.add(check);
                }

                JPanel buttons = new JPanel(new GridLayout(1, 2));
                buttons.setBackground(Colors.CONTENT_BACKGROUND);
                XButton deleteButton = new XButton(Loc.get("DELETE"));
                deleteButton.setStyle(XButton.Style.DARKER);
                deleteButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object[] options = {"<html><b>" + Loc.get("DELETE") + "</b></html>", Loc.get("CANCEL")};
                        int n = JOptionPane.showOptionDialog(mainFrame,
                                "<html>" + Loc.get("DO_YOU_REALLY_WANT_TO_DELETE_THE_RANK", comboRankSelection.getSelectedItem(), groupManagement.getSelectedGroup().getName()) + "</html>",
                                "", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                                null, options, options[0]);
                        if (n == JOptionPane.YES_OPTION) {
                            try {
                                Message ms = ((AbstractSynchronisationController) mainFrame.getController()).removeRank(groupManagement.getSelectedGroup().getId(), getSelectedRank().getId());
                                if (ms.getResult().wasSuccessful()) {
                                    Footer.displayConfirmation(Loc.get("RANK_SUCCESSFULLY_DELETED", getSelectedRank().getName()));
                                    displayRightManagementScreen();
                                } else if (ms.getResult().getStatus() == Result.GROUP_NOT_EMPTY) {
                                    Footer.displayError(Loc.get("RANK_CANNOT_BE_DELETED_RANK_STILL_IN_USE", getSelectedRank().getName()));
                                } else {
                                    Footer.displayError(Loc.get("ERROR_WHILE_DELETING_THE_RANK", getSelectedRank().getName()) + " : " + ms.getResult().getErrorMessage());
                                }
                            } catch (IOException | NotConnectedException | NotLoggedInException ex) {
                                Logger.getLogger(ColumnRank.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                });
                buttons.add(deleteButton);
                XButton saveButton = new XButton(Loc.get("SAVE"));
                saveButton.setStyle(XButton.Style.DARKER);
                saveButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            Message ms = ((AbstractSynchronisationController) mainFrame.getController()).adjustRank(groupManagement.getSelectedGroup().getId(), getSelectedRank().getId(), textfieldRankName.getText(), getSelectedRights());
                            if (ms.getResult().wasSuccessful()) {
                                displayRightManagementScreen();
                                Footer.displayConfirmation(Loc.get("RANK_INFORMATION_UPDATED"));
                            } else {
                                Footer.displayError(ms.getResult().getErrorMessage());
                            }
                        } catch (IOException | NotConnectedException | NotLoggedInException ex) {
                            Logger.getLogger(ColumnRank.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
                buttons.add(saveButton);
                input.add(buttons);
            } catch (NotLoggedInException | StatementNotExecutedException ex) {
                Logger.getLogger(ColumnRank.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    protected JComponent getDescription() {
        MultiLineTextLabel text = new MultiLineTextLabel(Loc.get("GROUP_MANAGEMENT_RANK_DESCRIPTION"));
        text.setBackground(Colors.CONTENT_BACKGROUND);
        return ComponentHelper.wrapComponent(text, Colors.CONTENT_BACKGROUND, 0, 0, 0, 0);
    }
}
