package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.dependable;

import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.SwitchToNewModeImpossibleException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;

import java.util.ArrayList;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 23.06.2017.
 */
public abstract class AbstractExtendedDependableEntry extends AbstractDependableEntry {

    public final AbstractBaseDependableEntry baseDependableEntry;

    public AbstractExtendedDependableEntry(GeneralInputMaskMode mode, AbstractBaseEntryManager thisEntry, ArrayList<AbstractBaseEntryManager> allEntries, AbstractBaseDependableEntry baseDependableEntry) {
        super(mode, thisEntry, allEntries);
        this.baseDependableEntry = baseDependableEntry;
    }

    public AbstractExtendedDependableEntry(GeneralInputMaskMode mode, boolean doInit, AbstractBaseEntryManager thisEntry, ArrayList<AbstractBaseEntryManager> allEntries, AbstractBaseDependableEntry baseDependableEntry) {
        super(mode, doInit, thisEntry, allEntries);
        this.baseDependableEntry = baseDependableEntry;
    }

    public AbstractExtendedDependableEntry(GeneralInputMaskMode mode, EntryDataSet data, AbstractBaseEntryManager thisEntry, ArrayList<AbstractBaseEntryManager> allEntries, AbstractBaseDependableEntry baseDependableEntry) {
        super(mode, data, thisEntry, allEntries);
        this.baseDependableEntry = baseDependableEntry;
    }


    @Override
    public void init() {
        super.init();
    }

    @Override
    public void setToHasWritingRightsMode(boolean b) {
        super.setToHasWritingRightsMode(b);
    }

    @Override
    public void actionOnSave(boolean displayNewEntryAfterSaving) {
        super.actionOnSave(displayNewEntryAfterSaving);

        if (displayNewEntryAfterSaving) {
            try {
                toNewEntryMode(true);
            } catch (SwitchToNewModeImpossibleException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void toNewEntryMode(boolean ignoreRememberValues) throws SwitchToNewModeImpossibleException {
        Content.setContent(baseDependableEntry);
        baseDependableEntry.toNewEntryMode(false);
    }
}
