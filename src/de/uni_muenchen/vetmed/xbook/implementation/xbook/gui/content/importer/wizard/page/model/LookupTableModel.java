package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.page.model;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import java.util.Map.Entry;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.LookupTableBuilerTableModel;

public class LookupTableModel extends LookupTableBuilerTableModel<String, Entry<String, String>, String> {

	private String label = null;
	private IMainFrame mainFrame;

	public LookupTableModel(IMainFrame mainFrame, String label) {
		super();
		this.label = label;
		this.mainFrame = mainFrame;
	}

	@Override
	public String convertValue(Entry<String, String> c) {
		return c.getValue();
	}

	@Override
	public String convertKey(Entry<String, String> c) {
		return c.getKey();
	}

	@Override
	public Object[] getRow(String d, Entry<String, String> c) {
		if (c == null) {
			return new Object[]{d, ""};
		}
		return new Object[]{d, c.getValue()};
	}

	@Override
	public int getEditorColumnIndex() {
		return 1;
	}

	@Override
	public String[] getColumnNames() {
		return new String[]{
			Loc.get("COLUMN_NAME_IN", mainFrame.getController().getBookName())};
	}
}
