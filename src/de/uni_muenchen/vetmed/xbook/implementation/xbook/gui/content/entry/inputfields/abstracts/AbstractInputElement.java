package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.StatusLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.StatusLabel.Status;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XLabel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.UpdateableEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElementWrapper;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An abstract class that provides the basic functions and abstract methods for an input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractInputElement extends JPanel implements IInputElement, IInputElementWrapper {

    public enum Mode {
        INPUT_FIELD, STACK, BLANK
    }

    protected static final Log log = LogFactory.getLog(AbstractInputElement.class);
    public static final String YES = "1";
    public static final String NO = "0";
    /**
     * The size of the empty border around the input element.
     */
    public static final int BORDER_AROUND = 3;
    /**
     * An object that holds all specific data for the input field.
     */
    protected ColumnType columnType;
    /**
     * The panel wrapper for the input field content.
     */
    protected JPanel multiPanel;
    /**
     * The number of horizontal grid elements that are used for this input element.
     */
    private int gridX = 1;
    /**
     * The number of vertical grid elements that are used for this input element.
     */
    private int gridY = 1;
    /**
     * An outter wrapper panel that holds all elements of the input element.
     */
    protected JPanel outerWrapper;
    /**
     * A JCheckBox that set the input field invisible when clicking on it.
     */
    private JLabel invisibleLabel;
    /**
     * A JLabel that set the input field deleted in a multi edit field when clicking on it.
     */
    private JLabel deleteLabel;
    /**
     * A JCheckBox to check when the input should not be removed from the input field when saving.
     */
    protected JCheckBox checkRememberValue;
    /**
     * An inner wrapper panel that holds all elements of the input element.
     */
    protected JPanel innerWrapper;
    /**
     * The basic Controller object.
     */
    protected static ApiControllerAccess apiControllerAccess;
    protected static AbstractMainFrame mainFrame;
    /**
     * The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
     */
    protected SidebarPanel sidebar;
    /**
     * The correspondenting entry object.
     */
    protected UpdateableEntry entry;
    /**
     * The element of the input element that is currently focussed.
     */
    protected Component currentFocused;
    /**
     * The panel that holds the first line of each input field (title, checkbox, etc...)
     */
    protected JPanel firstLine;
    /**
     * The panel that holds the the status in multi-edit-mode
     */
    protected JPanel statusLine;
    /**
     * Holds if the field shoud be disabled or enabled because of right issues.
     */
    protected boolean hasWritingRights = true;
    /**
     * Holds if the field is currently enabled or not.
     */
    protected boolean isEnabled = true;
    protected XLabel firstLineLabel;
    protected StatusLabel statusLineLabel;
    protected String titleString;
    private boolean isCreated = false;
    protected Mode mode;
    protected boolean isMultiEdit = false;
    protected String valueAfterLoading;
    protected Status statusAfterLoading;
    protected Status currentStatus;
    //    private boolean isMultiEditEnabled = true;

    /**
     * Should be set to true if the input field was loaded, but the value is not valid anymore (e.g. thesauri).
     */
    protected boolean invalidValueLoaded = false;

    public AbstractInputElement(ColumnType columnType) {
        this(Mode.INPUT_FIELD, columnType);
    }

    public AbstractInputElement(Mode style, ColumnType columnType) {
        this.columnType = columnType;
        this.mode = style;
        setSidebar("");
    }

    /**
     * Finishes the input field.
     *
     * @return The input element object.
     */
    public final AbstractInputElement create() {
        if (isCreated) {
            return this;
        }
        if (mode != Mode.BLANK) {
            createFieldWrapper();
        } else {
            createFieldWrapper2();
        }
        createFieldInput();
        isCreated = true;
        return this;
    }

    /**
     * Creates the input field specific input elements.
     */
    protected abstract void createFieldInput();

    /**
     * Creates the basic wrapper and functionality of the input field (in general).
     */
    protected void createFieldWrapper() {
        setLayout(new BorderLayout());
        setBorder(new EmptyBorder(BORDER_AROUND, BORDER_AROUND, BORDER_AROUND, BORDER_AROUND));
        setBackground(Colors.CONTENT_BACKGROUND);

        // outer wrapper
        outerWrapper = new JPanel(new BorderLayout());
        outerWrapper.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_BORDER, 1));
        outerWrapper.setBackground(Colors.INPUT_FIELD_BACKGROUND);

        // inner wrapper
        innerWrapper = new JPanel(new BorderLayout());
        innerWrapper.setBackground(Colors.INPUT_FIELD_BACKGROUND);

        setMode(mode);

        colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);

        if (columnType.isMandatory()) {
            setMandatoryStyle(false);
        } else {
            setDefaultStyle(false);
        }
    }

    protected void createFieldWrapper2() {
        setLayout(new BorderLayout());
        setBorder(new EmptyBorder(0, 0, 0, 0));
        setBackground(Color.MAGENTA);

        // outer wrapper
        outerWrapper = new JPanel(new BorderLayout());
        outerWrapper.setBorder(new EmptyBorder(0, 0, 0, 0));
        outerWrapper.setBackground(Color.YELLOW);

        // inner wrapper
        innerWrapper = new JPanel(new BorderLayout());
        innerWrapper.setBackground(Color.CYAN);

        setMode(mode);

        outerWrapper.add(BorderLayout.CENTER, innerWrapper);
        add(BorderLayout.CENTER, outerWrapper);
    }

    public void setToMultiEditStyle() {
        isMultiEdit = true;
        createStatusLine();

        setRememberValueFieldDisabled();
        hideHideButton();
        setDeleteButtonVisible(true);

        setVisible(true);
    }

    protected void createFirstLineWithCheckBox() {
        JPanel crossWrapper = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
        crossWrapper.setBackground(Colors.INPUT_FIELD_BACKGROUND);
        crossWrapper.setBorder(new EmptyBorder(9, 0, 0, 0));

        if (firstLine == null) {
            firstLine = new JPanel(new BorderLayout());
            firstLine.setBackground(Colors.INPUT_FIELD_BACKGROUND);
            firstLine.setPreferredSize(Sizes.STACK_LABEL_SIZE_DEFAULT);

            // remember checkbox
            checkRememberValue = new JCheckBox("");
            checkRememberValue.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        AbstractConfiguration.setInputFieldCheckboxProperty(apiControllerAccess.getPreferences(),
                                apiControllerAccess.getCurrentProject().getProjectKey(), columnType.getColumnName(),
                                checkRememberValue.isSelected());
                    } catch (NotLoadedException ex) {
                        Logger.getLogger(AbstractInputElement.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            checkRememberValue.setBackground(Colors.INPUT_FIELD_BACKGROUND);
            checkRememberValue.setFocusable(false);

            // label
            if (columnType == null) {
                firstLineLabel = new XLabel("");
                titleString = "";
            } else if (columnType.getHtmlDisplayNameGeneral() != null) {
                firstLineLabel = new XLabel(columnType.getHtmlDisplayNameGeneral());
                titleString = columnType.getHtmlDisplayNameGeneral();
            } else {
                firstLineLabel = new XLabel(titleString);
            }

            // set invisible button/label
            invisibleLabel = new JLabel();
            invisibleLabel.setBackground(Colors.INPUT_FIELD_BACKGROUND);
            invisibleLabel.setIcon(Images.DELETE_CROSS_GRAY);
            invisibleLabel.setBorder(new EmptyBorder(0, 0, 0, 4));
            invisibleLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    setVisibleAndSaveToProperties(false);
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    invisibleLabel.setIcon(Images.DELETE_CROSS_BLACK);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    invisibleLabel.setIcon(Images.DELETE_CROSS_GRAY);
                }
            });
            crossWrapper.add(invisibleLabel);
            if (columnType != null && columnType.isMandatory()) {
                invisibleLabel.setVisible(false);
            }

            // set delete button/label for multi edit
            deleteLabel = new JLabel();
            deleteLabel.setBackground(Colors.INPUT_FIELD_BACKGROUND);
            deleteLabel.setIcon(Images.DELETE_CIRCLE_RED);
            deleteLabel.setBorder(new EmptyBorder(0, 0, 0, 4));
            deleteLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    clear();
                    setStatus(Status.VALUE_DELETED);
                    entry.updateUpdateableInputFields();
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    deleteLabel.setIcon(Images.DELETE_CIRCLE_GRAY);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    deleteLabel.setIcon(Images.DELETE_CIRCLE_RED);
                }
            });
            crossWrapper.add(deleteLabel);
            setDeleteButtonVisible(false);
            if (columnType != null && columnType.isMandatory()) {
                deleteLabel.setVisible(false);
            }
        } else {
            firstLine.removeAll();
        }

        switch (mode) {
            case STACK:
                firstLine.add(BorderLayout.CENTER, firstLineLabel);
                break;
            case INPUT_FIELD:
            default:
                firstLine.add(BorderLayout.WEST, checkRememberValue);
                firstLine.add(BorderLayout.CENTER, firstLineLabel);
                firstLine.add(BorderLayout.EAST, crossWrapper);
                break;
        }
    }

    protected void createStatusLine() {
        if (statusLine == null) {
            statusLine = new JPanel(new BorderLayout());
            statusLine.setBackground(Colors.INPUT_FIELD_BACKGROUND);
            statusLine.setPreferredSize(Sizes.STACK_LABEL_SIZE_DEFAULT);
            statusLine.setBorder(new EmptyBorder(0, 0, 0, 0));

            // label
            statusLineLabel = new StatusLabel();
        } else {
            statusLine.removeAll();
        }
        statusLine.add(BorderLayout.CENTER, statusLineLabel);
        outerWrapper.add(BorderLayout.SOUTH, statusLine);
    }

    public void setMode(Mode mode) {
        this.mode = mode;
        if (outerWrapper == null) {
            return;
        }
        createFirstLineWithCheckBox();

        // TODO Really?
        calculateSize();

        // restructure outer wrapper
        outerWrapper.removeAll();
        switch (mode) {
            case STACK:
                outerWrapper.add(BorderLayout.WEST, ComponentHelper.wrapComponent(firstLine, 0, 4, 0, 4));
                innerWrapper.setBorder(new EmptyBorder(4, 3, 4, 3));
                break;
            case BLANK:
                innerWrapper.setBorder(new EmptyBorder(0, 3, 4, 3));
                break;
            case INPUT_FIELD:
            default:
                outerWrapper.add(BorderLayout.NORTH, firstLine);
                innerWrapper.setBorder(new EmptyBorder(0, 3, 4, 3));
                break;
        }
        outerWrapper.add(BorderLayout.CENTER, innerWrapper);

        // insert outer wrapper into general wrapper
        removeAll();
        add(BorderLayout.CENTER, outerWrapper);

    }

    /**
     * Set the input field to mandatory style.
     *
     * @param isFocused <code>true</code> to set the field to focued style.
     */
    public void setMandatoryStyle(boolean isFocused) {
        setMandatoryStyle(true, isFocused);
    }

    /**
     * Set the input field to mandatory or default style.
     *
     * @param mandatory <code>true</code> to set the field to mandatory style.
     *                  <code>false</code> to set the field to default style.
     * @param isFocused <code>true</code> to set the field to focued style.
     */
    public void setMandatoryStyle(boolean mandatory, boolean isFocused) {
        int borderStrength;
        Color borderColor, backgroundColor;
        if (isMultiEdit) {
            if (mandatory) {
                if (isFocused) {
                    borderColor = Colors.INPUT_FIELD_FOCUSED_BORDER;
                } else {
                    borderColor = Colors.INPUT_FIELD_BORDER;
                }
                borderStrength = 2;
                backgroundColor = statusLineLabel.getBackground();
                firstLineLabel.setText(titleString);
            } else {
                if (isFocused) {
                    borderColor = Colors.INPUT_FIELD_FOCUSED_BORDER;
                } else {
                    borderColor = Colors.INPUT_FIELD_BORDER;
                }
                borderStrength = 1;
                backgroundColor = statusLineLabel.getBackground();
                firstLineLabel.setText("<html><b>" + firstLineLabel.getText() + "</b></html>");
            }
        } else {
            if (mandatory) {
                if (isFocused) {
                    backgroundColor = Colors.INPUT_FIELD_FOCUSED_MANDATORY_BACKGROUND;
                    borderColor = Colors.INPUT_FIELD_FOCUSED_BORDER;
                } else {
                    backgroundColor = Colors.INPUT_FIELD_MANDATORY_BACKGROUND;
                    borderColor = Colors.INPUT_FIELD_BORDER;
                }
                borderStrength = 1;
                firstLineLabel.setText("<html><b>" + firstLineLabel.getText() + "</b></html>");
            } else {
                if (isFocused) {
                    backgroundColor = Colors.INPUT_FIELD_FOCUSED_BACKGROUND;
                    borderColor = Colors.INPUT_FIELD_FOCUSED_BORDER;
                } else {
                    backgroundColor = Colors.INPUT_FIELD_BACKGROUND;
                    borderColor = Colors.INPUT_FIELD_BORDER;
                }
                borderStrength = 1;
                firstLineLabel.setText(titleString);
            }
        }
        outerWrapper.setBorder(BorderFactory.createLineBorder(borderColor, borderStrength));
        colorizeBackground(backgroundColor);
    }

    @Override
    public void setErrorStyle() {
        if (!isMultiEdit) {
            ComponentHelper.colorAllChildren(this, Colors.INPUT_FIELD_ERROR_BACKGROUND);
        } else {
            statusLineLabel.setStatus(StatusLabel.Status.ERROR);
        }
    }

    @Override
    public void setDefaultStyle(boolean isFocused) {
        int borderStrength;
        Color borderColor, backgroundColor;
        if (isMultiEdit) {
            if (isMandatory()) {
                backgroundColor = statusLineLabel.getBackground();
                borderColor = Colors.INPUT_FIELD_FOCUSED_BORDER;
                borderStrength = 1;
            } else {
                backgroundColor = statusLineLabel.getBackground();
                if (isFocused) {
                    borderColor = Colors.INPUT_FIELD_FOCUSED_BORDER;
                } else {
                    borderColor = Colors.INPUT_FIELD_BORDER;
                }
                borderStrength = 1;
            }
        } else {
            if (isMandatory()) {
                if (isFocused) {
                    backgroundColor = Colors.INPUT_FIELD_MANDATORY_BACKGROUND;
                    borderColor = Colors.INPUT_FIELD_FOCUSED_BORDER;
                } else {
                    backgroundColor = Colors.INPUT_FIELD_FOCUSED_MANDATORY_BACKGROUND;
                    borderColor = Colors.INPUT_FIELD_BORDER;
                }
                borderStrength = 1;
            } else {
                if (isFocused) {
                    backgroundColor = Colors.INPUT_FIELD_BACKGROUND;
                    borderColor = Colors.INPUT_FIELD_FOCUSED_BORDER;
                } else {
                    backgroundColor = Colors.INPUT_FIELD_FOCUSED_BACKGROUND;
                    borderColor = Colors.INPUT_FIELD_BORDER;
                }
                borderStrength = 1;
            }
        }
        outerWrapper.setBorder(BorderFactory.createLineBorder(borderColor, borderStrength));
        colorizeBackground(backgroundColor);
    }

    /**
     * Sets the mainframe object.
     *
     * @param mainFrame The mainframe object.
     */
    public static void setMainFrame(AbstractMainFrame mainFrame) {
        AbstractInputElement.mainFrame = mainFrame;
    }

    protected void doThingsAfterEntryObjectIsAvailable() {
        if (mode == Mode.INPUT_FIELD && checkRememberValue != null) {
            try {
                checkRememberValue.setSelected(AbstractConfiguration.getInputFieldCheckboxProperty(
                        apiControllerAccess.getPreferences(), apiControllerAccess.getCurrentProject().getProjectKey(),
                        columnType.getColumnName()));
            } catch (NullPointerException ex) {
                Logger.getLogger(AbstractInputElement.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NotLoadedException ex) {
                Logger.getLogger(AbstractInputElement.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Set an content object as content for this AbstractInputField.
     *
     * @param content The content which is set for this AbstractInputField object.
     */
    public void setContent(JComponent content) {
        innerWrapper.removeAll();
        innerWrapper.add(BorderLayout.CENTER, content);
        innerWrapper.validate();
        innerWrapper.repaint();
    }

    /**
     * Calculates the size of the input field.
     * <p>
     * Useres the horizontal and vertical number of grids and calculates the real pixel size of the input field.
     */
    protected void calculateSize() {
        if (outerWrapper != null) {
            int x, y;
            switch (mode) {
                case INPUT_FIELD:
                    x = (Sizes.INPUT_GRID_SIZE.width - 10) * gridX;
                    if (isMultiEdit) {
                        y = (Sizes.INPUT_GRID_SIZE.height - 10) * gridY;
                    } else {
                        y = (Sizes.INPUT_GRID_SIZE.height - 10) * gridY;
                    }
                    break;
                case STACK:
                default:
                    x = Sizes.STACK_DEFAULT_WIDTH;
                    y = Sizes.STACK_DEFAULT_HEIGHT_NEW * gridY;
                    break;
            }
            outerWrapper.setPreferredSize(new Dimension(x, y));
        }
    }

    @Override
    public String toString() {
        String txt = getStringRepresentation();
        if (txt == null) {
            return "[InputField: " + columnType.getColumnName() + " | Type: " + this.getClass().getSimpleName() + " |" +
                    " ERROR: inputField is null]";
        } else {
            return "[InputField: " + columnType.getColumnName() + " | Type: " + this.getClass().getSimpleName() + " |" +
                    " Value: " + txt + "]";
        }
    }

    @Override
    public abstract String getStringRepresentation();

    /**
     * A static method to set the basic mainframe object.
     *
     * @param apiControllerAccess The static basic mainframe object.
     */
    public static void setApiControllerAccess(ApiControllerAccess apiControllerAccess) {
        AbstractInputElement.apiControllerAccess = apiControllerAccess;
    }

    @Override
    public void setEntry(UpdateableEntry entry) {
        this.entry = entry;
        doThingsAfterEntryObjectIsAvailable();
    }

    public UpdateableEntry getEntry() {
        return entry;
    }

    /**
     * Returns a list of all components that are focusable. Input fields should override this method to define all
     * specific components.
     * <p/>
     * This method SHOULD NOT BE CALLED from other methods than getMyFocusableComponents() !
     *
     * @return A list of all focusable components.
     */
    @Override
    public abstract ArrayList<Component> getMyFocusableComponents();

    /**
     * Returns a list of all components that are focusable. This includes the input field itself as well as the input
     * element. Input fields should override this method to define all specific components.
     * <p/>
     * This method SHOULD NOT BE CALLED from other methods than getMyFocusableComponents() !
     *
     * @return A list of all focusable components.
     */
    public final ArrayList<Component> getFocusableComponents() {
        ArrayList<Component> list = new ArrayList<>();
        if (sidebar != null) {
            list.addAll(sidebar.getFocusableItems());
        } else {
            System.out.println("no sidebar found " + columnType);
        }
        list.addAll(getMyFocusableComponents());
        return list;
    }

    @Override
    public void setFocus() {
        ArrayList<Component> comp = getMyFocusableComponents();
        if (currentFocused == null) {
            if (comp.isEmpty()) {
            } else {
                comp.get(0).requestFocusInWindow();
            }
        } else {
            currentFocused.requestFocusInWindow();
        }
    }

    /**
     * Returns a focus listener that can be added to the focusable elements.
     *
     * @return The focus listener.
     */
    public FocusListener getFocusFunction() {
        return new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (columnType == ColumnType.ITTABLE) {
                    return;
                }
                ArrayList<Component> comp = getMyFocusableComponents();
                currentFocused = e.getComponent();
                if (entry != null) {
                    entry.setInputFieldFocused(AbstractInputElement.this);
                }
                if (!comp.contains(e.getOppositeComponent())) {
                    if (sidebar != null) {
                        Sidebar.setSidebarContent(sidebar);
                    }
                }
                setDefaultStyle(true);
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (columnType == ColumnType.ITTABLE) {
                    return;
                }
                ArrayList<Component> comp = getMyFocusableComponents();
                if (!comp.contains(sidebar)) {
                    comp.add(sidebar);
                    comp.addAll(sidebar.getFocusableItems());
                }
                if (!comp.contains(e.getOppositeComponent())) {
                    currentFocused = null;
                    Sidebar.setSidebarContent(entry.getSideBar());
                }
                setDefaultStyle(false);
            }
        };
    }

    /**
     * Set this element visible or invisible. Also saves the settings in the properties.
     *
     * @param flag Whether the input field should be set visible or not.
     */
    public void setVisibleAndSaveToProperties(boolean flag) {
        try {
            setVisible(flag);
            AbstractConfiguration.setInputFieldProperty(
                    apiControllerAccess.getPreferences(),
                    apiControllerAccess.getCurrentProject().getProjectKey(),
                    columnType.getColumnName(), flag);
            if (!flag) {
                checkRememberValue.setSelected(false);
                clear();
            }
        } catch (NotLoadedException ex) {
            Logger.getLogger(AbstractInputElement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ColumnType getColumnType() {
        return columnType;
    }

    @Override
    public void colorizeBackground(Color color) {
        ComponentHelper.colorAllChildren(this, color);
        firstLine.setBackground(color);
        checkRememberValue.setBackground(color);
        invisibleLabel.setBackground(color);
        deleteLabel.setBackground(color);
        innerWrapper.setBackground(color);
    }

    @Override
    public boolean isMandatory() {
        if (columnType == null) {
            return false;
        }
        return columnType.isMandatory();
    }

    @Override
    public boolean isRememberValueSelected() {
        return checkRememberValue.isSelected();
    }

    /**
     * Action that is called when and entry was saved.
     *
     * @param displayNewEntryAfterSaving
     */
    public void actionOnSave(boolean displayNewEntryAfterSaving) {
    }

    /**
     * Method is run when the input element gains the focus.
     */
    @Override
    public void actionOnFocusGain() {
        if (isMultiEdit
                && (statusLineLabel.getCurrentStatus() == StatusLabel.Status.ERROR
                || statusLineLabel.getCurrentStatus() == StatusLabel.Status.MANDATORY_VALUE_MISSING)) {
            setStatus(StatusLabel.Status.NEW_VALUE_ENTERED);
        }
        setDefaultStyle(true);
        invalidValueLoaded = false;
    }

    /**
     * Method is run when the input element loses the focus.
     */
    @Override
    public void actionOnFocusLost() {
        if (valueAfterLoading == null) {
            return;
        } else {
            if (valueAfterLoading.equals(getStringRepresentation())) {
                setStatus(statusAfterLoading);
            } else { // if value was changed
                if (isMandatory() && !isValidInput()) {
                    setStatus(StatusLabel.Status.MANDATORY_VALUE_MISSING);
                } else {
                    setStatus(StatusLabel.Status.NEW_VALUE_ENTERED);
                }
            }
        }
    }

    /**
     * Checks for multi edit mode if the input field has dependencies and its value has been updated.
     * <p>
     * If yes, displays a JOptionPane to inform the user and to take action.
     * <p>
     * Use this as the root call.
     *
     * @return <code>true</code> if the input field has dependencies and an
     * updated value.
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException
     */
    public boolean areMultiEditDependenciesValid() throws IsAMandatoryFieldException {
        return areMultiEditDependenciesValid(true);
    }

    /**
     * Checks for multi edit mode if the input field has dependencies and its value has been updated.
     * <p>
     * If yes, displays a JOptionPane to inform the user and to take action.
     *
     * @param firstCall defines that it is the root call of the method. If firstCall is false, there will no dialog be
     *                  opened to the user.
     * @return <code>true</code> if the input field has dependencies and an
     * updated value.
     */
    private boolean areMultiEditDependenciesValid(boolean firstCall) throws IsAMandatoryFieldException {

        if (isMultiEdit()) {
            boolean isOk = true;
            if (getCurrentStatus() == Status.MANDATORY_VALUE_MISSING) {
                throw new IsAMandatoryFieldException(getColumnType());
            }
            if (getCurrentStatus() == Status.NEW_VALUE_ENTERED || getCurrentStatus() == Status.VALUE_DELETED) {
                if (getAllNextInputFieldsThatAreUpdateable().isEmpty()) {
                    isOk = true;
                } else {
                    for (AbstractInputElement i : getAllNextInputFieldsThatAreUpdateable()) {
                        if (i.areMultiEditDependenciesValid(false)) {
                            isOk = false;
                            break;
                        }
                    }
                }
            }

            // return and tell that everything is fine
            if (!firstCall) {
                return isOk;
            }

            //////
            ////// else let the user decide if everything is fine or not.
            //////
            if (!isOk) {
                // get a list of all next input fields
                ArrayList<AbstractInputElement> needCheck = new ArrayList<>();
                for (AbstractInputElement i : getAllNextInputFieldsThatAreUpdateable()) {
                    Status s = i.getCurrentStatus();
                    if (s == Status.DIFFERENT_VALUES_FOUND
                            || s == Status.NEW_VALUE_ENTERED
                            || s == Status.SAME_VALUES_FOUND
                            || s == Status.NOT_SUPPORTED) {
                        needCheck.add(i);
                    }
                }

                // create a readable string representation for these fields
                String list = "";
                for (int i = 0; i < needCheck.size(); i++) {
                    if (i != 0 && i % 3 == 0) {
                        list += "<br />";
                    }
                    list += "<b>" + needCheck.get(i).getColumnType().getDisplayName() + "</b>, ";
                }
                list = StringHelper.cutString(list, ", ");

                // display the option pane
                Object[] options = {
                        Loc.get("SAVE"),
                        "<html><b>" + Loc.get("CANCEL") + "</b></html>"};
                int n = JOptionPane.showOptionDialog(mainFrame,
                        "<html>" + Loc.get("YOU_HAVE_EDITED_AN_INPUTFIELD_WITH_DEPENDENCIES") + "<br /><br />"
                                + Loc.get("INPUT_FIELD_HAS_DEPENDENCIES", columnType.getDisplayName(), list) + "<br " +
                                "/><br />"
                                + "<b>" + Loc.get("IMPORTANT").toUpperCase() + "</b><br />"
                                + Loc.get("VALUES_OF_EDITED_INPUTFIELDS_REMAIN_UNCHANGED") + "<br />"
                                + Loc.get("PLEASE_CHECK_IF_VALUES_OF_DEPENCENT_INPUT_FIELDS_ARE_STILL_VALID") + "<br " +
                                "/><br />"
                                + Loc.get("HOW_DO_YOU_WANT_TO_PROCEED_FURTHER") + "</html>",
                        Loc.get("DEPENDENCIES_FOUND"),
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
                if (n == JOptionPane.NO_OPTION) {
                    return false;
                } else if (n == JOptionPane.YES_OPTION) {
                    return true;
                } else if (n == JOptionPane.CLOSED_OPTION) {
                    return false;
                }
            }
        }

        return true;

    }

    @Override
    public void setEnabled(boolean b) {
        isEnabled = hasWritingRights && b;
        for (Component c : getMyFocusableComponents()) {
            c.setEnabled(isEnabled);
            colorizeEnableSpecificItems(c, isEnabled);
        }
        setDeleteButtonVisible(isMultiEdit && b);
    }

    public void setEnabledForRights(boolean b) {
        hasWritingRights = b;

        // Necessary keep input fields deactivated that should not always be
        // activated (e.g. measurements (OB) / photo fields (EB).
        if (!b) {
            for (Component c : getMyFocusableComponents()) {
                c.setEnabled(false);
                colorizeEnableSpecificItems(c, false);
            }
        } else {
            for (Component c : getMyFocusableComponents()) {
                c.setEnabled(isEnabled);
                colorizeEnableSpecificItems(c, isEnabled);
            }
        }
    }

    protected void colorizeEnableSpecificItems(Component c, boolean isEnabled) {
        Component toColorize = c;
        if (c instanceof JTextField || c instanceof JTextArea) {
            toColorize = c;
        } else {
            return;
        }
        if (isEnabled) {
            toColorize.setBackground(Color.WHITE);
        } else {
            toColorize.setBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

    /**
     * Method that is run when the entry screen is reloaded or when a data set is saved.
     *
     * @deprecated Should now be realised via event listeners.
     */
    @Deprecated
    public void reload() {
    }

    /**
     * Returns the table name of the input element.
     *
     * @return The table name.
     */
    public String getTableName() {
        String tableName = ColumnHelper.getTableName(columnType.getColumnName());
        if (tableName.isEmpty()) {
            // default table name
            tableName = IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT;
        }
        return tableName;
    }

    /**
     * Returns the table name of the input element.
     *
     * @param colTy
     * @return The table name.
     */
    protected static String getTableName(ColumnType colTy) {
        String tableName = ColumnHelper.getTableName(colTy.getColumnName());
        if (tableName.isEmpty()) {
            // default table name
            tableName = IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT;
        }
        return tableName;
    }

    @Override
    public void setRememberValueFieldDisabled() {
        checkRememberValue.setSelected(false);
        checkRememberValue.setIcon(Images.CHECKBOX_DEFAULT_ICON);
        checkRememberValue.setDisabledIcon(Images.CHECKBOX_DISABLED);
        checkRememberValue.setDisabledSelectedIcon(Images.CHECKBOX_DISABLED_SELECTED);
        checkRememberValue.setEnabled(false);
    }

    @Override
    public void hideHideButton() {
        invisibleLabel.setVisible(false);
        invisibleLabel.setEnabled(false);
    }

    @Override
    public void setDeleteButtonVisible(boolean b) {
        deleteLabel.setVisible(b);
        deleteLabel.setEnabled(b);
    }


    // ========================================================================

    /**
     * Get the number of horizontal grid elements used for this input element.
     *
     * @return The width of the input field (in grid elements).
     */
    @Override
    public int getGridX() {
        return gridX;
    }

    /**
     * Get the number of vertical grid elements used for this input element.
     *
     * @return The height of the input field (in grid elements).
     */
    @Override
    public int getGridY() {
        return gridY;
    }

    public boolean isCreated() {
        return isCreated;
    }

    // ========================================================================
    public void setColumnType(ColumnType columnType) {
        this.columnType = columnType;
    }

    @Override
    public void setGridX(int gridX) {
        this.gridX = gridX;
        calculateSize();
    }

    @Override
    public void setGridY(int gridY) {
        this.gridY = gridY;
        calculateSize();
    }

    /**
     * Set the titel of the input field.
     *
     * @param title The titel of the input field.
     */
    public void setTitle(String title) {
        this.titleString = title;
        if (firstLineLabel != null) {
            firstLineLabel.setText(title);
        }
    }

    @Override
    public void setSidebar(SidebarPanel sidebar) {
        this.sidebar = sidebar;
    }

    @Override
    public void setSidebar(String sidebarText) {
        if (columnType != null) {
            this.sidebar = new SidebarEntryField(columnType, sidebarText);
        }
    }

    @Override
    public void loadMultiEdit(DataSetOld data) {
        clearMultiEdit();
        load(data);
        valueAfterLoading = getStringRepresentation();
        updateStatusLabel(data);
        statusAfterLoading = statusLineLabel.getCurrentStatus();
        //entry.updateUpdateableInputFields();
    }

    public void updateStatusLabel(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        String s = list.get(columnType);
        if (s == null) {
            setStatus(StatusLabel.Status.DIFFERENT_VALUES_FOUND);
        } else if (s.isEmpty() || s.equals("-1") || s.equals("-1.0")) {
            setStatus(StatusLabel.Status.NO_VALUE_FOUND);
        } else {
            setStatus(StatusLabel.Status.SAME_VALUES_FOUND);
        }
    }

    /**
     * Set the status of the status line to a new value.
     *
     * @param status
     */
    public void setStatus(StatusLabel.Status status) {
        // check if mutli edit is enabled for this input field
        if (!columnType.isMultiEditAllowed()) {
            // if not, check if it is currently multi edit mode
            if (isMultiEdit) {
                status = StatusLabel.Status.NOT_SUPPORTED;
                setDefaultStyle(false);
            } else {
                entry.updateUpdateableInputFields();
            }

        }
        // update the status label
        if (statusLineLabel != null) {
            currentStatus = status;
            statusLineLabel.setStatus(status);
            colorizeBackground(status.getBgColor());
        }
        // enable/disable the input field
        boolean enabled = (status == Status.NOT_SUPPORTED);
        setEnabled(!enabled);
    }

    public void clearMultiEdit() {
        clear();
        valueAfterLoading = null;
        setStatus(StatusLabel.Status.NOT_CHECKED);
    }

    //    public void ssetMultiEditEnabled(boolean b) {
//        isMultiEditEnabled = b;
//    }
//
//    public boolean isMultiEditEnabled() {
//        return isMultiEditEnabled;
//    }
    public boolean isMultiEdit() {
        return isMultiEdit;
    }

    public Status getStatus() {
        return currentStatus;
    }

    /**
     * Checks for multi edit mode if this input element has a valid input, dependant on the status label.
     *
     * @return <code>true</code> if the field has a valid input,
     * <code>false</code> else.
     */
    public boolean isValidInputDependentOnStatus() {
        Status status = getStatus();
        if (status == StatusLabel.Status.ERROR
                || status == StatusLabel.Status.MANDATORY_VALUE_MISSING
                || status == StatusLabel.Status.NOT_CHECKED
                || status == StatusLabel.Status.NOT_SUPPORTED
                || status == StatusLabel.Status.NO_VALUE_FOUND
                || status == StatusLabel.Status.VALUE_DELETED) {
            return false;
        }
        return true;
    }

    /**
     * Returns wether the input field is updateable, or not.
     *
     * @return If the input field is updateable.
     */
    public boolean isUpdateableInputField() {
        return (this instanceof InterfaceUpdateableInputField);
    }

    public Status getCurrentStatus() {
        return currentStatus;
    }

    public Status getStatusAfterLoading() {
        return statusAfterLoading;
    }

    public ArrayList<AbstractInputElement> getDirectDependInputFields() {
        return new ArrayList<>();
    }

    public ArrayList<AbstractInputElement> getAllNextInputFieldsThatAreUpdateable() {
        UniqueArrayList<AbstractInputElement> all = new UniqueArrayList<>();
        all.addAll(getDirectDependInputFields());
        for (AbstractInputElement e : getDirectDependInputFields()) {
            all.addAll(e.getAllNextInputFieldsThatAreUpdateable());
        }
        return all;
    }

    public boolean invalidValueLoaded() {
        return invalidValueLoaded;
    }
}
