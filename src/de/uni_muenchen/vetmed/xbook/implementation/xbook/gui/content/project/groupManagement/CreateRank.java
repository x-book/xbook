package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.groupManagement;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights.AbstractProjectRightsAdd;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights.AddUserRights;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rank;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rights;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights.RightMode;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class CreateRank extends AbstractProjectRightsAdd<Rank> {

    private ArrayList<Rank> allRanks;
    private final GroupManagement groupManagement;

    public CreateRank(GroupManagement groupManagement) {
        super(RightMode.RANK_TO_GROUP);
        this.groupManagement = groupManagement;
    }

    @Override
    protected ArrayList<Rank> getComboData() {
        return new ArrayList<>();
    }

    @Override
    protected ArrayList<ColumnType> getAvailableRights() {
        try {
            return mainFrame.getController().getAvailableGroupRights();
        } catch (NotLoggedInException ex) {
            Logger.getLogger(AddUserRights.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }

    @Override
    protected void actionAfterSaving() {
        mainFrame.displayGroupManagementScreen(groupManagement.getSelectedGroup());
    }

    @Override
    protected void save() {
        String rankName = textField.getText();
        if (rankName.isEmpty()) {
            Footer.displayError(Loc.get("PLEASE_ENTER_A_RANK_NAME"));
            return;
        } else {
            ArrayList<Rights> rights = new ArrayList<>();
            for (JCheckBox check : allCheckBoxes) {
                rights.add(new Rights(new ColumnType(check.getName(),ColumnType.Type.VALUE,ColumnType.ExportType.NONE), check.isSelected()));
            }
            if (RightsInformation.hasAnyRight(rights) /* || rights.isEmpty() */) {
                try {
                    Message ms = ((AbstractSynchronisationController) mainFrame.getController()).adjustRank(groupManagement.getSelectedGroup().getId(), -1, rankName, rights);
                    if (ms.getResult().wasSuccessful()) {
                        actionAfterSaving();
                        Footer.displayConfirmation(Loc.get("RANK_SUCCESSFULLY_SAVED", rankName));
                    } else {
                        Footer.displayError(Loc.get("ERROR_WHILE_SAVING_THE_RANK", rankName));
                    }
                } catch (IOException | NotConnectedException | NotLoggedInException ex) {
                    LogFactory.getLog(CreateRank.class).error(ex);
                    Footer.displayError(Loc.get("ERROR_WHILE_SAVING_THE_RANK", rankName));
                }
            } else {
                Footer.displayWarning(Loc.get("NO_RIGHTS_ASSIGNED"));
            }
        }
    }

    @Override
    protected void fillComboData() {
        allRanks = getComboData();
        for (Rank r : allRanks) {
            combo.addItem(r.getName() + ")");
        }
    }
}
