package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTitle;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel.Position;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarProjectEdit;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarProjectEditField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractProjectManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractFocusableEntryContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputTextField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A input panel to create a new project or to edit an existing one.
 * <p/>
 * The panel holds several input possibilities and possibilities to save/update
 * and reset the data.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractProjectEdit extends AbstractFocusableEntryContent  {

    /**
     * The delete button.
     */
    protected XImageButton deleteGlobalButton;
    /**
     * The clear or reset button.
     */
    protected XImageButton resetOrClearButton;
    /**
     * The save and back to project overview button.
     */
    protected XImageButton saveAndBackButton;
    /**
     * The save and continue editing button.
     */
    protected XImageButton saveButton;

    protected ProjectDataSet projectToEdit;

    protected InputTextField projectName;
    private InputTextField projectOwner;
    /**
     * Holds all stack input fields that are displayed and necessary for saving
     * and loading.
     */
    protected ArrayList<AbstractInputElement> fields = new ArrayList<>();

    /**
     * Constructor that should be used when creating a new project.
     */
    public AbstractProjectEdit() {
        super();
        init();
    }

    /**
     * Constructor that should be used when editing an existing project.
     *
     * @param project The project data to edit.
     */
    public AbstractProjectEdit(ProjectDataSet project) {
        super();
        this.projectToEdit = project;
        init();
    }

    @Override
    protected JPanel getContent() {
        JPanel wrapperPanel = new JPanel();
        wrapperPanel.setBackground(Colors.CONTENT_BACKGROUND);

        JPanel panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        // initialise the input fields
        // project title
        if (isEditMode()) {
            panel.add(new StackTitle(Loc.get("EDIT_PROJECT")));
        } else {
            panel.add(new StackTitle(Loc.get("NEW_PROJECT")));
        }

        // add the custom contents (top)
        ArrayList<AbstractInputElement> abstractStackElementsTop = addCustomContentTop();
        if (abstractStackElementsTop != null) {
            for (AbstractInputElement stack : abstractStackElementsTop) {
                stack.setMode(AbstractInputElement.Mode.STACK);
                stack.create();
                fields.add(stack);
                panel.add(stack);
            }
        }

        // project name
        projectName = getProjectNameField();
        projectName.setMode(AbstractInputElement.Mode.STACK);
        projectName.create();
        fields.add(projectName);
        panel.add(projectName);

        // add the custom contents (center)
        ArrayList<AbstractInputElement> abstractStackElementsCenter = addCustomContentCenter();
        if (abstractStackElementsCenter != null) {
            for (AbstractInputElement stack : abstractStackElementsCenter) {
                stack.setMode(AbstractInputElement.Mode.STACK);
                stack.create();
                fields.add(stack);
                panel.add(stack);
            }
        }

        // project owner input field (only display on edit mode)
        projectOwner = new InputTextField(AbstractProjectManager.PROJECT_PROJECTOWNER);
        projectOwner.setSidebar(Loc.get("SIDEBAR_EDIT_PROJECT>PROJECT_OWNER"));
        if (projectToEdit != null) {
            projectOwner.setMode(AbstractInputElement.Mode.STACK);
            projectOwner.create();
            projectOwner.setEnabled(false);
            if (showProjectOwnerRow()) {
                panel.add(projectOwner);
            }
        }

        // add the custom contents (below)
        ArrayList<AbstractInputElement> abstractStackElementsBelow = addCustomContentBelow();
        if (abstractStackElementsBelow != null) {
            for (AbstractInputElement stack : abstractStackElementsBelow) {
                stack.setMode(AbstractInputElement.Mode.STACK);
                stack.create();
                fields.add(stack);
                panel.add(stack);
            }
        }

        // if editing mode: fill the input fields with existing project data
        if (projectToEdit != null) {
            resetFields();

            applyUserRights();
        }
        setEntryObject();
        addFocusFunction();
        wrapperPanel.add(panel);
        return wrapperPanel;
    }

    /**
     * Returns the status whether to show or hide the project owner row. Default: true.
     * Override this method to set it to false.
     *
     * @return The status whether to show or hide the project owner row.
     */
    protected boolean showProjectOwnerRow() {
        return true;
    }

    /**
     * Disable the input fields if the user has no rights to edit the project
     * information check if the current user has rights to edit the project
     * information
     */
    protected void applyUserRights() {
        boolean userHasEditRights = false;

        try {
            userHasEditRights = mainFrame.getController().hasEditRights(projectToEdit);
        } catch (StatementNotExecutedException | NotLoggedInException ex) {
            Logger.getLogger(AbstractProjectEdit.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (!userHasEditRights) {
            disableInputFields();
            saveAndBackButton.setVisible(false);
            saveButton.setVisible(false);
            deleteGlobalButton.setVisible(false);
            resetOrClearButton.setVisible(false);
        }
    }

    /**
     * Set the entry object to all input field objects.
     */
    protected void setEntryObject() {

        for (AbstractInputElement abstractInputElement : fields) {
            abstractInputElement.setEntry(this);
        }

    }

    /**
     * Adds the general focus function (that is defined in AbstractInputElement) to
     * all available focusable elements of all input fields.
     */
    private void addFocusFunction() {
        for (AbstractInputElement input : fields) {
            for (Component comp : input.getMyFocusableComponents()) {
                comp.addFocusListener(input.getFocusFunction());
            }
        }
    }

    protected InputTextField getProjectNameField() {
        InputTextField name = new InputTextField(AbstractProjectManager.PROJECT_PROJECTNAME);
        name.setSidebar(new SidebarProjectEditField(Loc.get("PROJECT_NAME"), Loc.get("SIDEBAR_EDIT_PROJECT>PROJECT_NAME"), isEditMode()));
        return name;
    }

    /**
     * This method adds custom content elements ON TOP OF the project name and
     * project owner field.
     *
     * @return An ArrayList of the elements that should be added.
     */
    protected ArrayList<AbstractInputElement> addCustomContentTop() {
        return null;
    }

    /**
     * This method adds custom content elements BETWEEN the project name and
     * project owner field.
     *
     * @return An ArrayList of the elements that should be added.
     */
    protected ArrayList<AbstractInputElement> addCustomContentCenter() {
        return null;
    }

    /**
     * This method adds custom content elements BELOW the project name and
     * project owner field.
     *
     * @return An ArrayList of the elements that should be added.
     */
    protected abstract ArrayList<AbstractInputElement> addCustomContentBelow();

    /**
     * Method that is called before saving the entry. Can be used to do
     * individual checks, e.g. checking if a similar entry already exists in the
     * database.
     * <p>
     * Returning <code>false</code> will not automatically cause displaying an
     * error message in the footer. So please do it here.
     * <p>
     * Notice that the correctness of the input of the input fields is already
     * checked in Method isValidInput() of class AbstractInputElement.
     *
     * @return <code>true</code> if saving should be continued,
     * <code>false</code> else
     */
    protected boolean checkBeforeSaving() {
        return true;
    }

    /**
     * Save a new project to the database or updateExtention the existing
     * project information.
     *
     * @param returnToOverview Value if the project overview screen should be
     *                         loaded after saving, or not.
     * @return
     */
    public ProjectDataSet saveProject(boolean returnToOverview) {
        try {
            ProjectDataSet dataToSave;
            if (!checkBeforeSaving()) {
                return null;
            }
            // check if is valid project name ...
            if (!projectName.isValidInput()) {
                projectName.setErrorStyle();
                throw new IsAMandatoryFieldException(projectName.getColumnType());
            }
            // ... and save!
            if (projectToEdit == null) {
                dataToSave = new ProjectDataSet(null, mainFrame.getController().getDbName(), projectName.getText(), -1, false);
            } else {
                dataToSave = new ProjectDataSet(projectToEdit.getProjectKey(), mainFrame.getController().getDbName(), projectName.getText(), projectToEdit.getProjectOwnerId(), false);
            }

            for (AbstractInputElement input : fields) {
                // check if is valid field value ...
                if (input.isMandatory() && !input.isValidInput()) {
                    input.setErrorStyle();
                    throw new IsAMandatoryFieldException(input.getColumnType());
                }
                // ... and save!
                input.save(dataToSave);
            }
            if (projectToEdit == null) {
                ((AbstractController) mainFrame.getController()).newProject(dataToSave);
            } else {
                mainFrame.getController().saveProject(dataToSave);
            }
            if (returnToOverview) {
                mainFrame.displayProjectOverviewScreen();
            } else {
                mainFrame.displayProjectEditScreen(dataToSave);
            }
            return dataToSave;
        } catch (IsAMandatoryFieldException ex) {
            Footer.displayWarning(Loc.get("ENTER_A_VALUE_FOR_THE_MANDATORY_FIELD", ex.getColumnType().getDisplayName()));
        } catch (NumberFormatException | NotLoggedInException | StatementNotExecutedException | NoRightException | EntriesException | MissingInputException ex) {
            Footer.displayError(Loc.get("ERROR_WHILE_SAVING_PROJECT"));
            ex.printStackTrace();
            Logger.getLogger(AbstractProjectEdit.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }



    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();

        panel.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_BACK, Loc.get("BACK"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.displayProjectOverviewScreen();
            }
        });

        String resetOrClear = Loc.get("RESET");
        resetOrClearButton = panel.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_REVERT, resetOrClear, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetFields();
            }
        });

        if (projectToEdit != null) {
            panel.addImageButton(Position.NORTH_CENTER, Images.BUTTONPANEL_DELETE_LOCAL, Loc.get("LOCAL_DELETE"), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int n = JOptionPane.showConfirmDialog(AbstractProjectEdit.this, "<html>" + Loc.get("SURE_TO_DELETE_PROJECT_LOCAL", projectToEdit.getProjectName()) + "</html>", Loc.get("WARNING"), JOptionPane.YES_NO_OPTION);
                    if (n == JOptionPane.YES_OPTION) {
                        try {
                            ((AbstractController) mainFrame.getController()).deleteProjectLocaly(projectToEdit);
                            mainFrame.displayProjectOverviewScreen();
                        } catch (NotLoggedInException | StatementNotExecutedException | NoRightException ex) {
                            Logger.getLogger(AbstractProjectEdit.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });

            deleteGlobalButton = panel.addImageButton(Position.NORTH_CENTER, Images.BUTTONPANEL_DELETE_GLOBAL, Loc.get("DELETE_COMPLETELY"), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int n = JOptionPane.showConfirmDialog(AbstractProjectEdit.this, "<html>" + Loc.get("SURE_TO_DELETE_PROJECT_GLOBAL", projectToEdit.getProjectName()) + "</html>", Loc.get("WARNING"), JOptionPane.YES_NO_OPTION);
                    if (n == JOptionPane.YES_OPTION) {
                        try {
                            ((AbstractController) mainFrame.getController()).deleteProject(projectToEdit);
                            mainFrame.displayProjectOverviewScreen();
                        } catch (NotLoggedInException | NoRightException | NotConnectedException | StatementNotExecutedException ex) {
                            Logger.getLogger(AbstractProjectEdit.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
        }

        String saveOrAdd = Loc.get("SAVE");
        ImageIcon saveOrAddIcon = Images.BUTTONPANEL_SAVE;

        String saveOrAddAndBack = Loc.get("SAVE_AND_BACK");
        ImageIcon saveOrAddAndBackIcon = Images.BUTTONPANEL_SAVE_NEXT;

        if (projectToEdit == null) {
            saveOrAdd = Loc.get("CREATE");
            saveOrAddAndBack = Loc.get("CREATE_AND_BACK");
        }
        saveAndBackButton = panel.addImageButton(Position.NORTH_EAST, saveOrAddAndBackIcon, saveOrAddAndBack, Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveProject(true);
            }
        });
        saveButton = panel.addImageButton(Position.NORTH_EAST, saveOrAddIcon, saveOrAdd, Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveProject(false);
            }
        });

        return panel;
    }

    /**
     * Check if edit mode or new mode is set.
     *
     * @return
     */
    protected boolean isEditMode() {
        return projectToEdit != null;
    }

    /**
     * Reset all fields.
     */
    protected void resetFields() {
        projectName.load(projectToEdit);
        try {
            projectOwner.setText(mainFrame.getController().getDisplayName(projectToEdit.getProjectOwnerId()));
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(AbstractProjectEdit.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (AbstractInputElement stack : fields) {
            stack.load(projectToEdit);
        }
    }

    /**
     * Reset all fields.
     */
    protected void disableInputFields() {
        projectName.setEnabled(false);
        projectOwner.setEnabled(false);

        for (AbstractInputElement stack : fields) {
            stack.setEnabled(false);
        }
    }

    @Override
    public SidebarPanel getSideBar() {
        return new SidebarProjectEdit(isEditMode());
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }



    @Override
    public void updateInputFieldVisibility() {
        System.out.println("updateInputFieldVisibility() not in use for now");
    }

    @Override
    public void updateUpdateableInputFields() {
        System.out.println("updateUpdateableInputFields() not in use for now");
    }

    @Override
    public void updateUpdateableInputFieldsSingle() {
        System.out.println("updateUpdateableInputFieldsSingle() not in use for now");
    }

    @Override
    public void updateUpdateableInputFieldsMultiEdit() {
        System.out.println("updateUpdateableInputFieldsMultiEdit() not in use for now");
    }

    @Override
    public void revertMultiEditInputField(AbstractInputElement fieldToReload) throws WrongModeException {
        // do nothing
    }
}
