package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.combosearch;

/**
 *
 * @author ciaran.harrington@extern.lrz-muenchen.de
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.combosearch.Searchable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;

public class Demonstration{
 //       public abstract class searchable implements Searchable{};
        public static void main(String[] args) throws Exception {

        List myWords = new ArrayList();
        myWords.add("bike");
        myWords.add("car");
        myWords.add("cap");
        myWords.add("cape");
        myWords.add("canadian");
        myWords.add("caprecious");
        myWords.add("catepult");
        StringSearchable searchable = new StringSearchable(myWords);
        AutocompleteJComboBox combo = new AutocompleteJComboBox(searchable);
        JFrame frame = new JFrame();
        frame.add(combo);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }			
}
