package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rights;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.exception.InvalidSelectionException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotSavedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.modernCheckbox.ModernCheckbox;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTextBlock;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.rights.RightsCheckBox;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.GroupManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.ProjectRightManager;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractRights extends AbstractContent {

    /**
     * The default height of one project line.
     */
    protected static final int LINE_HEIGHT = 42;
    /**
     * The default Dimension of a button.
     */
    protected static final Dimension BUTTON_DIMENSION = new Dimension(45, 36);
    /**
     * The save button object.
     */
    private Color moduloLineColor = new Color(240, 240, 240);
    /**
     * The save button that stores the changes in the database.
     */
    private XButton saveButton;
    /**
     * The content where the right information are displayed.
     */
    private JPanel contentRights;
    /**
     * Holds all available right lines.
     */
    private final ArrayList<RightsLine> allRightLines;
    /**
     * Holds all available right information objects.
     */
    private final ArrayList<RightsInformation> allRights;
    /**
     * A static index that counts the created right lines.
     */
    public static int index = 0;
    /**
     * Defines the selected mode that is used in the right selection.
     */
    private final RightMode mode;
    /**
     * The filter line object.
     */
    private FilterLine filterLine;

    /**
     * The loading panel that should be set visible
     * while the content is being loaded.
     */
    protected JPanel loadingPanel;
    /**
     * The panel where the content is stored.
     */
    protected JPanel contentPanel;

    /**
     * Constructor.
     *
     * @param mode The selected mode that is used in the right selection.
     */
    public AbstractRights(RightMode mode) {
        super(true);
        this.mode = mode;

        allRightLines = new ArrayList<>();
        allRights = new ArrayList<>();
        init();
        ComponentHelper.colorAllChildren(this, Colors.CONTENT_BACKGROUND);
    }

    @Override
    protected JPanel getContent() {
        JPanel panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        loadingPanel = new JPanel(new StackLayout());
        loadingPanel.setVisible(false);
        loadingPanel.add(getTitle());
        loadingPanel.add(new XTextBlock(Loc.get("IS_BEING_LOADED")));
        panel.add(loadingPanel);

        contentPanel = new JPanel(new StackLayout());
        createContentOnCondition();

        panel.add(contentPanel);

        return panel;
    }

    protected void createContentOnCondition() {

        if (!((AbstractSynchronisationController) mainFrame.getController()).isServerConnected()) {
            // display information screen if no connection to the server is available
            contentPanel.add(getTitle());
            contentPanel.add(new XTextBlock(Loc.get("NO_CONNECTION_TO_SERVER")));
        } else if (!mainFrame.getController().isProjectLoaded()) {
            // display information screen if no project is loaded
            contentPanel.add(getTitle());
            contentPanel.add(new XTextBlock(Loc.get("NO_PROJECT_IS_LOADED")));
//            saveButton.setVisible(false);
        } else {
            // display the default content
            contentPanel.setLayout(new BorderLayout());
            contentPanel.add(BorderLayout.CENTER, createContent());
        }
    }

    /**
     * Creates the default content.
     *
     * @return A JPanel with the default content to display.
     */
    protected JPanel createContent() {
        JPanel allWrapper = new JPanel(new StackLayout());
        allWrapper.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        //
        // Filter Title
        //
        allWrapper.add(new XTitle(Loc.get("FILTER")));

        //
        // Filter Content
        //
        allWrapper.add(filterLine = new FilterLine());

        JLabel emptyLine = new JLabel(" ");
        emptyLine.setBackground(Colors.CONTENT_BACKGROUND);
        allWrapper.add(emptyLine);

        //
        // Rights Title
        //
        allWrapper.add(getTitle());

        //
        // Rights Content
        //
        JScrollPane pane = new JScrollPane();
        pane.setBackground(Colors.CONTENT_BACKGROUND);
        pane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        JPanel contentRightsWrapper = new JPanel(new StackLayout()) {
            @Override
            public Dimension getPreferredSize() {
                if (!allRightLines.isEmpty()) {
                    return new Dimension(allRightLines.get(0).getPreferredSize().width, allRightLines.size() * LINE_HEIGHT);
                }
                return super.getPreferredSize();
            }
        };
        contentRightsWrapper.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        contentRights = new JPanel(new StackLayout());
        contentRights.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        contentRights.setBackground(Colors.CONTENT_BACKGROUND);
        contentRightsWrapper.add(contentRights);

        pane.setViewportView(contentRightsWrapper);
        pane.setBackground(Colors.CONTENT_BACKGROUND);

        allWrapper.add(pane);

        return allWrapper;
    }

    private synchronized void setLoadingStatus(final boolean isLoading) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                loadingPanel.setVisible(isLoading);
                contentPanel.setVisible(!isLoading);
                if (isLoading) {
                    Footer.startWorking();
                } else {
                    Footer.stopWorking();
                }
            }
        });

    }

    /**
     * Adds a right line.
     *
     * @param rinfo The user and rights information.
     */
    private void addRightsLine(RightsInformation rinfo) {
        if (shouldDisplayLine(rinfo) && (displayEmptyLines() || rinfo.hasAnyRight())) {
            RightsLine rl = new RightsLine(rinfo);
            contentRights.add(rl);
            allRightLines.add(rl);

            allRights.add(rinfo);
            index++;
        }
    }

    /**
     * Set the return value to "true" to also display lines that have not any rights.
     * <p>
     * E.g. necessary for general rights.
     *
     * @return
     */
    protected boolean displayEmptyLines() {
        return false;
    }

    protected abstract boolean shouldDisplayLine(RightsInformation rinfo);


    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();

        panel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_SAVE, Loc.get("SAVE"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    save();
                    reloadThisPanel();
                } catch (NotSavedException e1) {
                    // nothing to do, is only important to avoid that rows are reset to "non-changed"
                }
            }
        });

        panel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_ADD, getAddButtonTooltip(), Sizes.MODIFIER_BIG_BUTTON, getAddButtonListener());

        panel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_REVERT, Loc.get("RESET"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reloadThisPanel();
            }
        });

        return panel;
    }

    /**
     * Load a new instance of the object, basically by mainframe.displayXXXScreen().
     */
    protected abstract void reloadThisPanel();

    /**
     * Returns the current values of all displayed rows.
     *
     * @return The current values of all displayed rows.
     */
    protected ArrayList<RightsInformation> getCurrentValues() {
        // the data that will be returned later
        ArrayList<RightsInformation> u = new ArrayList<>();
        // iterate all right lines in the GUI and let them store their changed data in the list.
        for (int i = 0; i < allRightLines.size(); i++) {
            RightsInformation rinfo = allRights.get(i);
            allRightLines.get(i).retrieveChangedData(u, rinfo.getName(), rinfo.getID());
        }
        // return the retrieved data to be saved
        return u;
    }

    /**
     * Returns the XTitle object with the correct label.
     *
     * @return The XTitle object.
     */
    protected XTitle getTitle() {
        if (mode == RightMode.GENERAL_RIGHTS) {
            return new XTitle(Loc.get("GENERAL_RIGHTS"));
        } else if (mode == RightMode.USER_TO_PROJECT) {
            return new XTitle(Loc.get("USER_RIGHTS"));
        } else {
            return new XTitle(Loc.get("GROUP_RIGHTS"));
        }
    }

    /**
     * Saves the current selected data to the database.
     */
    protected abstract void save() throws NotSavedException;

    /**
     * Loads the data from the database.
     */
    protected abstract ArrayList<RightsInformation> load();

    @Override
    public boolean forceSidebar() {
        return true;
    }

    @Override
    public void setFocus() {
    }

    @Override
    public void actionOnDisconnect() {
    }

    @Override
    public void updateContent() {
        super.updateContent();

        /* Display the information that something is loaded. */
        setLoadingStatus(true);

        if (((AbstractSynchronisationController) mainFrame.getController()).isLoggedInGlobal()) {
            index = 0;

            ArrayList<RightsInformation> rights = load();
            for (RightsInformation u : rights) {
                addRightsLine(u);
            }
            updateNameLabelSize();
        } else {
            content.setVisible(false);
            buttonBar.setVisible(false);
        }
        updateNameLabelSize();

        /* Remove the information that something is loaded. */
        setLoadingStatus(false);

        revalidate();
        repaint();
    }

    /**
     * Set an individual listener to the "add" button.
     *
     * @return The defined listener.
     */
    protected abstract ActionListener getAddButtonListener();

    /**
     * Set the tooltip label of the "add" button.
     *
     * @return The tooltip label.
     */
    protected abstract String getAddButtonTooltip();

    /**
     * Updates the width of the name label of all right rows, so that all name labels have the same width.
     */
    private void updateNameLabelSize() {
        final int MIN_WIDTH = 200;
        final int MAX_WIDTH = 600;

        // calculate the new width
        int width = MIN_WIDTH;
        for (RightsLine rl : allRightLines) {
            int rowWidth = rl.getNameLabel().getPreferredSize().width;
            if (rowWidth > width) {
                width = rowWidth;
            }
        }

        if (width > MAX_WIDTH) {
            width = MAX_WIDTH;
        }

        // apply the new width
        for (RightsLine rl : allRightLines) {
            rl.getNameLabel().setSize(new Dimension(width, LINE_HEIGHT));
            rl.getNameLabel().setPreferredSize(new Dimension(width, LINE_HEIGHT));
        }
        filterLine.getTextFieldFilterName().setSize(new Dimension(width, LINE_HEIGHT));
        filterLine.getTextFieldFilterName().setPreferredSize(new Dimension(width, LINE_HEIGHT));
    }

    /**
     * Applies the filter to the lines.
     */
    private void applyFilter() {

        // fetch the data
        String filteredValueName = filterLine.getTextFieldFilterName().getText();
        ArrayList<RightsCheckBox> filteredValuesCheckboxes = filterLine.getAllCheckboxes();

        // apply the data
        for (RightsLine line : allRightLines) {
            boolean displayRow = false;

            String name = line.nameLabel.getText();
            name = name.replace("<html>", "");
            name = name.replace("</html>", "");
            name = name.replace("<br>", "");
            name = name.replace("<small>", "");
            name = name.replace("</small>", "");
            if (!filteredValueName.trim().isEmpty()) {
                if (name.toLowerCase().trim().contains(filteredValueName.trim().toLowerCase())) {
                    displayRow = true;
                }
            } else {
                // check if all rows are disabled
                boolean anyIsEnabled = false;
                for (RightsCheckBox filteredCheck : filteredValuesCheckboxes) {
                    if (filteredCheck.getCurrentSelection() == ModernCheckbox.Selection.SELECTED || filteredCheck.getCurrentSelection() == ModernCheckbox.Selection.DESELECTED) {
                        anyIsEnabled = true;
                        break;
                    }
                }
                if (anyIsEnabled) {
                    displayRow = true;
                    // if any checkbox is selected, then check for values and filter the rows
                    for (RightsCheckBox filteredCheck : filteredValuesCheckboxes) {
                        if (filteredCheck.getCurrentSelection() == ModernCheckbox.Selection.SELECTED || filteredCheck.getCurrentSelection() == ModernCheckbox.Selection.DESELECTED) {
                            for (RightsCheckBox lineCheck : line.getAllCheckboxes()) {
                                if (filteredCheck.getName().equals(lineCheck.getName())) {
                                    if (!filteredCheck.getCurrentSelection().equals(lineCheck.getCurrentSelection())) {
                                        displayRow = false;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    // of all checkboxes are disabled, then the rows shall be displayed.
                    displayRow = true;
                }
            }

            line.setVisible(displayRow);
        }
    }

    protected abstract ArrayList<ColumnType> getListOfRights() throws NotLoggedInException;

    /**
     * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
     */
    private class RightsLine extends JPanel {

        /**
         * The label displaying the name of the  user/group/etc.
         */
        private JLabel nameLabel;
        /**
         * Holds all right checkboxes for the row.
         */
        ArrayList<RightsCheckBox> allCheckboxes = new ArrayList<>();

        /**
         * The checkbox for the "read" right.
         */
        private RightsCheckBox readCheck = null;
        /**
         * The checkbox for the "write" right.
         */
        private RightsCheckBox writeCheck = null;
        /**
         * The checkbox for the "project edit" right.
         */
        private RightsCheckBox projectEditCheck = null;

        /**
         * Constructor
         *
         * @param rinfo The corresponding rights information object for the row.
         */
        public RightsLine(final RightsInformation rinfo) {
            setLayout(new FlowLayout(FlowLayout.LEADING));
            if (index % 2 == 0) {
                setBackground(moduloLineColor);
            } else {
                setBackground(Colors.CONTENT_BACKGROUND);
            }

            // create the user label
            nameLabel = new JLabel(rinfo.getName());
            nameLabel.setBorder(BorderFactory.createEmptyBorder(0, 6, 0, 12));
            nameLabel.setFont(Fonts.FONT_BIG_BOLD);
            add(nameLabel);

            // create the selection fields for each right
            for (Rights right : rinfo.getRights()) {
                RightsCheckBox c = addCheckbox(right.getColumnType().getColumnName(), right.getColumnType().getDisplayName(), right.hasRight());
                if (right.getColumnType().equals(ProjectRightManager.PROJECT_RIGHTS_READ) || right.getColumnType().equals(GroupManager.PROJECT_RIGHT_GROUP_READ)) {
                    readCheck = c;
                }
                if (right.getColumnType().equals(ProjectRightManager.PROJECT_RIGHTS_WRITE) || right.getColumnType().equals(GroupManager.PROJECT_RIGHT_GROUP_WRITE)) {
                    writeCheck = c;
                }
                if (right.getColumnType().equals(ProjectRightManager.PROJECT_RIGHTS_EDIT_PROJECT) || right.getColumnType().equals(GroupManager.PROJECT_RIGHT_GROUP_EDIT_PROJECT)) {
                    projectEditCheck = c;
                }
            }
            if (readCheck != null && writeCheck != null && projectEditCheck != null) {
                readCheck.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (readCheck.getCurrentSelection() == ModernCheckbox.Selection.DESELECTED) {
                            if (writeCheck.getCurrentSelection() == ModernCheckbox.Selection.SELECTED) {
                                try {
                                    writeCheck.setSelection(ModernCheckbox.Selection.DESELECTED);
                                } catch (InvalidSelectionException e1) {
                                    e1.printStackTrace();
                                }
                            }
                            if (projectEditCheck.getCurrentSelection() == ModernCheckbox.Selection.SELECTED) {
                                try {
                                    projectEditCheck.setSelection(ModernCheckbox.Selection.DESELECTED);
                                } catch (InvalidSelectionException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }
                });
                writeCheck.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (writeCheck.getCurrentSelection() == ModernCheckbox.Selection.SELECTED) {
                            if (readCheck.getCurrentSelection() == ModernCheckbox.Selection.DESELECTED) {
                                try {
                                    readCheck.setSelection(ModernCheckbox.Selection.SELECTED);
                                } catch (InvalidSelectionException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        } else {
                            if (projectEditCheck.getCurrentSelection() == ModernCheckbox.Selection.SELECTED) {
                                try {
                                    projectEditCheck.setSelection(ModernCheckbox.Selection.DESELECTED);
                                } catch (InvalidSelectionException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }
                });
                projectEditCheck.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (projectEditCheck.getCurrentSelection() == ModernCheckbox.Selection.SELECTED) {
                            if (readCheck.getCurrentSelection() == ModernCheckbox.Selection.DESELECTED) {
                                try {
                                    readCheck.setSelection(ModernCheckbox.Selection.SELECTED);
                                } catch (InvalidSelectionException e1) {
                                    e1.printStackTrace();
                                }
                            }
                            if (writeCheck.getCurrentSelection() == ModernCheckbox.Selection.DESELECTED) {
                                try {
                                    writeCheck.setSelection(ModernCheckbox.Selection.SELECTED);
                                } catch (InvalidSelectionException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }
                });
            }
            setPreferredSize(new Dimension(getMinimumSize().width, AbstractRights.LINE_HEIGHT));
        }

        /**
         * Method that retrieves the data that was changed and store it in the sent list.
         *
         * @param rinfo The list where to save the changed data.
         * @param name  The displayed name of the user/group/etc.
         * @param id    The id of the user/group/etc.
         */
        public void retrieveChangedData(List<RightsInformation> rinfo, String name, int id) {
            ArrayList<Rights> rights = new ArrayList<>();

            // iterates all available checkboxes and only store the data in the list,
            // if the value has been updated.
            for (RightsCheckBox checkbox : allCheckboxes) {
                if (checkbox.hasValueChanged()) {
                    boolean selected = checkbox.getCurrentSelection() == ModernCheckbox.Selection.SELECTED ? true : false;
                    rights.add(new Rights(checkbox.getName(), selected));
                }
            }

            // store the changed data, if available only
            if (!rights.isEmpty()) {
                rinfo.add(new RightsInformation(name, id, rights));
            }
        }

        /**
         * Adds a new checkbox for a specific right.
         *
         * @param internName   An intern name for the right to be used to identify the right. In general it is the column name in the database.
         * @param displayLabel The displayed name of the right.
         * @param isSelected   The status if the value is selected or not.
         * @return
         */
        private RightsCheckBox addCheckbox(String internName, String displayLabel, boolean isSelected) {
            RightsCheckBox check = new RightsCheckBox(ModernCheckbox.Mode.YES_NO, displayLabel);
            check.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
            try {
                if (isSelected) {
                    check.setSelection(ModernCheckbox.Selection.SELECTED, true);
                } else {
                    check.setSelection(ModernCheckbox.Selection.DESELECTED, true);
                }
            } catch (InvalidSelectionException e) {
                e.printStackTrace();
            }
            check.setName(internName);
//            if (index % 2 == 0) {
//                check.setBackground(moduloLineColor);
//            } else {
//                check.setBackground(Colors.CONTENT_BACKGROUND);
//            }
            add(check);
            allCheckboxes.add(check);
            return (check);
        }

        /**
         * Returns the current values from the selection of the rights row..
         *
         * @return The current values.
         */
        private ArrayList<Rights> getCurrentValues() {
            ArrayList<Rights> r = new ArrayList<>();
            for (RightsCheckBox check : allCheckboxes) {
                boolean selected = check.getCurrentSelection() == ModernCheckbox.Selection.SELECTED ? true : false;
                r.add(new Rights(new ColumnType(check.getName(), ColumnType.Type.VALUE, ColumnType.ExportType.NONE), selected));
            }
            return r;
        }

        /**
         * Returns all checkboxes of this row.
         *
         * @return All checkboxes of this row.
         */
        private ArrayList<RightsCheckBox> getAllCheckboxes() {
            return allCheckboxes;
        }

        /**
         * Returns the name label object.
         *
         * @return The name label object.
         */
        public JLabel getNameLabel() {
            return nameLabel;
        }
    }

    /**
     * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
     */
    private class FilterLine extends JPanel {

        /**
         * The textfield where to filter for names.
         */
        private JTextField textFieldFilterName;

        /**
         * Holds all right checkboxes for the row.
         */
        ArrayList<RightsCheckBox> allCheckboxes = new ArrayList<>();

        /**
         * Constructor
         */
        public FilterLine() {
            setLayout(new FlowLayout(FlowLayout.LEADING));
            if (index % 2 == 0) {
                setBackground(moduloLineColor);
            } else {
                setBackground(Colors.CONTENT_BACKGROUND);
            }

            // create the user label
            textFieldFilterName = new JTextField();
            textFieldFilterName.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    updateRow();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    updateRow();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    updateRow();
                }

                private void updateRow() {
                    for (RightsCheckBox check : allCheckboxes) {
                        if (textFieldFilterName.getText().trim().isEmpty()) {
                            check.setEnabled(true);
                            check.updateColor();
                        } else {
                            try {
                                check.setSelection(ModernCheckbox.Selection.NONE);
                                check.setEnabled(false);
                                check.updateColor();
                            } catch (InvalidSelectionException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    applyFilter();
                }
            });
            add(ComponentHelper.wrapComponent(textFieldFilterName, 0, 0, 0, 0));

            // create the selection fields for each right
            try {
                ArrayList<ColumnType> generalRights = getListOfRights();
                for (ColumnType right : generalRights) {
                    addCheckbox(right.getColumnName(), right.getDisplayName(), ModernCheckbox.Selection.NONE);
                }
            } catch (NotLoggedInException e) {
                Logger.getLogger(AbstractRights.class).error(e);
            }

            setPreferredSize(new Dimension(getMinimumSize().width, AbstractRights.LINE_HEIGHT));
        }

        /**
         * Adds a new checkbox for a specific right.
         *
         * @param internName   An intern name for the right to be used to identify the right. In general it is the column name in the database.
         * @param displayLabel The displayed name of the right.
         * @param selection    The status which selection of the checkbox shall be set.
         * @return The checkbox object.
         */
        private ModernCheckbox addCheckbox(String internName, String displayLabel, ModernCheckbox.Selection selection) {
            RightsCheckBox check = new RightsCheckBox(ModernCheckbox.Mode.YES_NO_NONE, displayLabel);
            check.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
            try {
                check.setSelection(selection);
            } catch (InvalidSelectionException e) {
                e.printStackTrace();
            }
            check.setName(internName);
            check.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    applyFilter();
                }
            });
            add(check);
            allCheckboxes.add(check);
            return (check);
        }

        /**
         * Returns the textfield where to filter for names.
         *
         * @return The textfield where to filter for names.
         */
        public JTextField getTextFieldFilterName() {
            return textFieldFilterName;
        }

        /**
         * Returns a list of all right checkboxes for the row.
         *
         * @return A list of all right checkboxes for the row.
         */
        public ArrayList<RightsCheckBox> getAllCheckboxes() {
            return allCheckboxes;
        }
    }

}
