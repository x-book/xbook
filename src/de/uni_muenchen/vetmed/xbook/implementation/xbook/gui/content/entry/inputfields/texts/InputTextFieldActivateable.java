package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawTextField;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.ITextField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractActiveableInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputTextFieldActivateable extends AbstractActiveableInputElement implements ITextField {

    /**
     * The text field object.
     */
    protected RawTextField zField;

    public InputTextFieldActivateable(ColumnType columnTypeBoolean, ColumnType columnTypeField) {
        super(columnTypeBoolean, columnTypeField);
    }

    @Override
    public String getText() {
        return zField.getText();
    }

    public JTextField getTextField() {
        return zField.getTextField();
    }

    @Override
    public AbstractInputElementFunctions createActivableInputField() {
        if (zField == null) {
            zField = new RawTextField(columnType);
        }
        return zField;
    }

    @Override
    public void setText(String text) {
        zField.setText(text);
    }

}
