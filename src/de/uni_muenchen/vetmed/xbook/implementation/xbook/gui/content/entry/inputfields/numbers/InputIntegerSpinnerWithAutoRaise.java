package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IIntegerSpinnerWithAutoRaise;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An integer spinner as input element and adds methods to auto raise and decrease.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputIntegerSpinnerWithAutoRaise extends InputIntegerSpinner implements IIntegerSpinnerWithAutoRaise {

	private JRadioButton radioUp;
	private JRadioButton radioDown;
	private final String propertyName;

	/**
	 * Constructor.
	 *
	 * @param columnType The database column data of the correspondenting input field.
	 * @param propertyName The String that should be used as key in the properties.
	 */
	public InputIntegerSpinnerWithAutoRaise(ColumnType columnType, String propertyName) {
		super(columnType);
		this.propertyName = propertyName;
	}

	@Override
	public void actionOnSave(boolean displayNewEntryAfterSaving) {
		if (displayNewEntryAfterSaving) {
			String saveString = null;
			if (!spinner.getValue().equals("")) {
				saveString = String.valueOf(spinner.getValue());
			}
			try {
				int saveInt = Integer.parseInt(saveString);
				if (radioUp.isSelected()) {
					spinner.setValue((saveInt + 1) + "");
				} else { // if (radioDown.isSelected())
					spinner.setValue((saveInt - 1) + "");
				}
			} catch (NumberFormatException nfe) {
				spinner.setValue("");
			}
		}
	}

	@Override
	public ArrayList<Component> getMyFocusableComponents() {
		ArrayList<Component> comp = super.getMyFocusableComponents();
		comp.add(radioUp);
		comp.add(radioDown);
		return comp;
	}

	@Override
	protected void createFieldInput() {
		super.createFieldInput();
		JPanel selectionWrapper = new JPanel(new BorderLayout());

		ButtonGroup buttonGroup = new ButtonGroup();

		radioUp = new JRadioButton("▲");
		radioUp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					AbstractConfiguration.setMiscPropertyBoolean(
							apiControllerAccess.getPreferences(),
							apiControllerAccess.getCurrentProject().getProjectKey(),
							propertyName, true);
				} catch (NotLoadedException ex) {
					Logger.getLogger(InputIntegerSpinnerWithAutoRaise.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});
		buttonGroup.add(radioUp);
		selectionWrapper.add(BorderLayout.WEST, radioUp);

		radioDown = new JRadioButton("▼");
		radioDown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					AbstractConfiguration.setMiscPropertyBoolean(
							apiControllerAccess.getPreferences(),
							apiControllerAccess.getCurrentProject().getProjectKey(),
							propertyName, false);
				} catch (NotLoadedException ex) {
					Logger.getLogger(InputIntegerSpinnerWithAutoRaise.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});
		buttonGroup.add(radioDown);
		selectionWrapper.add(BorderLayout.EAST, radioDown);

		multiPanel.add(BorderLayout.EAST, selectionWrapper);

		try {
			if (AbstractConfiguration.getMiscPropertyBoolean(
					apiControllerAccess.getPreferences(),
					apiControllerAccess.getCurrentProject().getProjectKey(),
					propertyName, true)) {
				radioUp.setSelected(true);
			} else {
				radioDown.setSelected(true);
			}
		} catch (NotLoadedException ex) {
			Logger.getLogger(InputIntegerSpinnerWithAutoRaise.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
