package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.rows;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class ProjectSearchBooleanRow extends AbstractProjectSearchRow {

	private JRadioButton radioYes;
	private JRadioButton radioNo;

	public ProjectSearchBooleanRow(ColumnType columnType) {
		super(columnType, DataType.EQUAL);
		
		buttonContainsEquals.setVisible(false);
	}

	@Override
	protected JComponent getInputElements() {
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));

		radioYes = new JRadioButton(Loc.get("YES"));
		radioYes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (radioYes.isSelected()) {
					radioNo.setSelected(false);
				}
			}
		});
		panel.add(radioYes);

		radioNo = new JRadioButton(Loc.get("NO"));
		radioNo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (radioNo.isSelected()) {
					radioYes.setSelected(false);
				}
			}
		});
		panel.add(radioNo);

		return panel;
	}

	@Override
	protected ArrayList<JComponent> getAllInputElements() {
		ArrayList<JComponent> elements = super.getAllInputElements();
		elements.add(radioNo);
		elements.add(radioYes);
		return elements;
	}

	@Override
	public void clear() {
		radioYes.setSelected(false);
		radioNo.setSelected(false);
	}

	@Override
	public ArrayList<String> getInput() {
		ArrayList<String> value = new ArrayList<>();
		value.add(getRawInput());
		return value;
	}

	@Override
	public String getRawInput() {
		if (radioYes.isSelected()) {
			return "1";
		}
		if (radioNo.isSelected()) {
			return "0";
		}
		return "-1";
	}

    @Override
    public boolean isEmpty() {
        return getRawInput().equals("-1");
    }

}
