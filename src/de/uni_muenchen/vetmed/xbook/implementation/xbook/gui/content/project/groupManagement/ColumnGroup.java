package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.groupManagement;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Group;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ColumnGroup extends AbstractGroupManagementColumn {

    private ArrayList<UserLine> allLines;
    private Group selectedGroup = null;
    private final GroupManagement groupManagement;
    private JLabel deleteLabel;

    public ColumnGroup(GroupManagement rangScreen, AbstractMainFrame mainFrame) {
        super(mainFrame);
        this.groupManagement = rangScreen;
    }

    @Override
    protected String getTitle() {
        return Loc.get("GROUP_OVERVIEW");
    }

    @Override
    protected JScrollPane getContent() {
        allLines = new ArrayList<>();

        JPanel panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        JScrollPane scroll = new JScrollPane();
        scroll.setViewportView(ComponentHelper.wrapComponent(panel, Colors.CONTENT_BACKGROUND, 0,10,0,10));
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));

        try {
            ArrayList<Group> groupsOfCurrentUser = getGroups();
            for (Group group : groupsOfCurrentUser) {
                panel.add(createUserLine(group));
            }
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(ColumnGroup.class.getName()).log(Level.SEVERE, null, ex);
        }
        return scroll;
    }

    protected void actionOnGroupDeleted(){
        mainFrame.displayGroupManagementScreen();
    }
    protected ArrayList<Group> getGroups() throws NotLoggedInException, StatementNotExecutedException {
        return mainFrame.getController().getGroupsOfCurrentUser();
    }

    @Override
    protected ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();
        
		XImageButton button = new XImageButton(Loc.get("CREATE_GROUP"));
		button.setWidth(200);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.displayProjectUserRightsCreateGroupScreen();
            }
        });
        panel.addTo(ButtonPanel.Position.SOUTH_EAST, button);
        
        return panel;
    }

    private UserLine createUserLine(Group group) {
        UserLine line = new UserLine(group);
        allLines.add(line);
        return line;
    }

    public Group getSelectedGroup() {
        return selectedGroup;
    }

    public void setSelectedGroup(Group group) {
        selectedGroup = group;
        for (UserLine line : allLines) {
            if (line.group.getId() == selectedGroup.getId()) {
                line.button.setSelected(true);
                line.labelName.setFont(Fonts.FONT_STANDARD_BOLD);
            }
        }
        groupManagement.updateUserPanel();
        groupManagement.updateRankPanel();
    }

    @Override
    protected JComponent getDescription() {
        MultiLineTextLabel text = new MultiLineTextLabel(Loc.get("GROUP_MANAGEMENT_GROUP_DESCRIPTION"));
        text.setBackground(Colors.CONTENT_BACKGROUND);
        return ComponentHelper.wrapComponent(text, Colors.CONTENT_BACKGROUND, 0, 0, 0, 0);
    }

    private class UserLine extends JPanel {

        private Group group;
        private JPanel test;
        private JPanel left;
        private JRadioButton button;
        private JLabel labelName;

        public UserLine(final Group group) {
            super(new BorderLayout());
            setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

            this.group = group;

            boolean hasEditGroupRights = false;
            try {
                hasEditGroupRights = mainFrame.getController().hasEditGroupRights(group.getId());
            } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException ex) {
                Logger.getLogger(ColumnUser.class.getName()).log(Level.SEVERE, null, ex);
            }
            test = new JPanel(new BorderLayout());

            test.setBackground(Colors.CONTENT_BACKGROUND);
            left = new JPanel(new FlowLayout(FlowLayout.LEADING));

            left.setBackground(Colors.CONTENT_BACKGROUND);
            button = new JRadioButton();

            button.setBackground(Colors.CONTENT_BACKGROUND);

            left.add(button);

            left.add(labelName = new JLabel(group.getName()));
            labelName.setFont(Fonts.FONT_STANDARD_PLAIN);


            try {
                left.add(new JLabel("(" + mainFrame.getController().getUsersOfGroup(group.getId()).size() + " " + Loc.get("USERS") + ")"));
            } catch (NotLoggedInException | StatementNotExecutedException ex) {
                Logger.getLogger(ColumnGroup.class.getName()).log(Level.SEVERE, null, ex);
            }

            test.add(BorderLayout.WEST, left);
            deleteLabel = new JLabel(Images.DELETE_SQUARE);

            deleteLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    deleteLabel.setIcon(Images.DELETE_SQUARE_HOVERED);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    deleteLabel.setIcon(Images.DELETE_SQUARE);
                }
                @Override
                public void mouseClicked(MouseEvent e) {
                    Object[] options = {"<html><b>" + Loc.get("REMOVE") + "</b></html>", Loc.get("CANCEL")};
                    int n = JOptionPane.showOptionDialog(mainFrame,
                            "<html>" + Loc.get("DO_YOU_REALLY_WANT_TO_DELETE_THE_GROUP", group.getName()) + "</html>",
                            "", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                            null, options, options[0]);
                    if (n == JOptionPane.YES_OPTION) {
                        try {
                            Message ms = ((AbstractSynchronisationController) mainFrame.getController()).deleteGroup(group.getId());
                            if (ms.getResult().wasSuccessful()) {
                                Footer.displayConfirmation(Loc.get("GROUP_SUCCESSFULLY_DELETED", group.getName()));
                            } else {
                                Footer.displayError(Loc.get("ERROR_WHILE_DELETING_THE_GROUP", group.getName()));
                            }
                        } catch (IOException | NotConnectedException | NotLoggedInException ex) {
                                Footer.displayError(Loc.get("ERROR_WHILE_DELETING_THE_GROUP", group.getName()));
                        }
             actionOnGroupDeleted();
                       }
                }
            });
            if (hasEditGroupRights) {
                test.add(BorderLayout.EAST, deleteLabel);
            }

            add(BorderLayout.NORTH, test);

            setCustomListeners();
        }

        private void setCustomListeners() {
            deleteLabel.addMouseListener(getCustomMouseListener());
            test.addMouseListener(getCustomMouseListener());
            left.addMouseListener(getCustomMouseListener());
            button.addMouseListener(getCustomMouseListener());
        }

        private MouseListener getCustomMouseListener() {
            return new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    test.setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                    left.setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                    button.setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    test.setBackground(Colors.CONTENT_BACKGROUND);
                    left.setBackground(Colors.CONTENT_BACKGROUND);
                    button.setBackground(Colors.CONTENT_BACKGROUND);
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    for (UserLine l : allLines) {
                        l.button.setSelected(false);
                        l.labelName.setFont(Fonts.FONT_STANDARD_PLAIN);
                    }
                    button.setSelected(true);
                    labelName.setFont(Fonts.FONT_STANDARD_BOLD);
                    selectedGroup = group;
                    groupManagement.updateUserPanel();
                    groupManagement.updateRankPanel();
                }
            };
        }

        public Group getGroup() {
            return group;
        }
    }
}
