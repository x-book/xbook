package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.Unit;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.IButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.IEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.AbstractInputFieldFactory;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboIdBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboIdSearchBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboTextBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboTextBoxWithProjectData;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboTextBoxWithThesaurus;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IHierarchicComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IMultiComboIdBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.ICheckBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IDateChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IIconButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IOpenURLButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IPlaceHolder;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IPlaceholderButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.ITimeChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IUserDisplayField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IYesNoField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IFloatField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IIntegerField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IIntegerSpinner;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IIntegerSpinnerWithAutoRaise;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IIntegerUnitField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.ILabeledFloatField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.ILabeledIntegerField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IMinMaxFloatField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.INumberAsStringField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.INumberAsStringUnitField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.IFileNameChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.IMultiTextField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.ITextArea;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.ITextField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.InputComboBox;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.InputComboIdBox;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.InputComboIdSearchBox;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.InputComboTextBox;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.InputComboTextBoxWithProjectData;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.InputComboTextBoxWithThesaurus;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.InputHierarchicComboBox;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.InputMultiComboIdBox;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputButton;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputCheckBox;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputDateChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputIconButton;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputOpenURLButton;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputPlaceHolder;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputPlaceHolderButton;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputTimeChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputUserDisplayField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputYesNoBox;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers.InputFloatField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers.InputIntegerField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers.InputIntegerSpinner;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers.InputIntegerSpinnerWithAutoRaise;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers.InputIntegerUnitField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers.InputLabeledFloatField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers.InputLabeledIntegerField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers.InputMinMaxFloatField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers.InputNumberAsStringField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers.InputNumberAsStringUnitField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputFileNameChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputMultiTextField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputTextArea;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputTextField;
import java.util.ArrayList;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputFieldFactory extends AbstractInputFieldFactory {

    private InputFieldFactory() {

    }

    public static final void createFactory() {
        if (factory == null) {
            factory = new InputFieldFactory();
        }
    }

    @Override
    public ITextField createTextField(ColumnType columnType) {
        return new InputTextField(columnType);
    }

    @Override
    public ITextArea createTextArea(ColumnType columnType) {
        return new InputTextArea(columnType);
    }

    @Override
    public IMultiTextField createMultiTextField(ColumnType columnType, String table) {
        return new InputMultiTextField(columnType, table);
    }

    @Override
    public IFileNameChooser createFileNameChooser(ColumnType columnType, ArrayList<FileNameExtensionFilter> fileFilter) {
        return new InputFileNameChooser(columnType, fileFilter);
    }

    @Override
    public IFloatField createFloatField(ColumnType columnType) {
        return new InputFloatField(columnType);
    }

    @Override
    public IIntegerField createIntegerField(ColumnType columnType) {
        return new InputIntegerField(columnType);
    }

    @Override
    public IIntegerSpinner createIntegerSpinner(ColumnType columnType) {
        return new InputIntegerSpinner(columnType);
    }

    @Override
    public IIntegerSpinnerWithAutoRaise createIntegerSpinnerWithAutoRaise(ColumnType columnType, String propertyName) {
        return new InputIntegerSpinnerWithAutoRaise(columnType, propertyName);
    }

    @Override
    public IIntegerUnitField createIntegerUnitField(ColumnType columnType, ArrayList<Unit> units) {
        return new InputIntegerUnitField(columnType, units);
    }

    @Override
    public ILabeledFloatField createLabeledFloatField(ColumnType columnType, String labelText) {
        return new InputLabeledFloatField(columnType, labelText);
    }

    @Override
    public ILabeledIntegerField createLabeledIntegerField(ColumnType columnType, String labelText) {
        return new InputLabeledIntegerField(columnType, labelText);
    }

    @Override
    public IMinMaxFloatField createMinMaxFloatField(ColumnType columnTypeMin, ColumnType columnTypeMax, String title, String labelText) {
        return new InputMinMaxFloatField(columnTypeMin, columnTypeMax, title, labelText);
    }

    @Override
    public INumberAsStringField createNumberAsStringField(ColumnType columnType) {
        return new InputNumberAsStringField(columnType);
    }

    @Override
    public INumberAsStringUnitField createNumberAsStringUnitField(ColumnType columnType, ArrayList<Unit> units) {
        return new InputNumberAsStringUnitField(columnType, units);
    }

    @Override
    public IComboBox createComboBox(ColumnType columnType, ArrayList<String> values) {
        return new InputComboBox(columnType, values);
    }

    @Override
    public IComboIdBox createComboIdBox(ColumnType columnType) {
        return new InputComboIdBox(columnType);
    }

    @Override
    public IComboIdSearchBox createComboIdSearchBox(ColumnType columnType) {
        return new InputComboIdSearchBox(columnType);
    }

    @Override
    public IComboTextBox createComboTextBox(ColumnType columnType) {
        return new InputComboTextBox(columnType);
    }

    @Override
    public IComboTextBoxWithThesaurus createComboTextBoxWithThesaurus(ColumnType columnType) {
        return new InputComboTextBoxWithThesaurus(columnType);
    }

    @Override
    public IComboTextBoxWithProjectData createComboTextBoxWithProjectData(ColumnType columnTypeToSave, ColumnType columnTypeToLoad) {
        return new InputComboTextBoxWithProjectData(columnTypeToSave, columnTypeToLoad);
    }

    @Override
    public IHierarchicComboBox createHierarchicComboBox(ColumnType columnType) {
        return new InputHierarchicComboBox(columnType);
    }

    @Override
    public IMultiComboIdBox createMultiComboIdBox(ColumnType columnType, String table) {
        return new InputMultiComboIdBox(columnType, table);
    }

    @Override
    public IButton createButton(ColumnType columnType, IEntry entry) {
        return new InputButton(columnType, entry) {
            @Override
            public String getStringRepresentation() {
                return "n.a.";
            }
        };
    }

    @Override
    public ICheckBox createCheckBox(ColumnType columnType) {
        return new InputCheckBox(columnType);
    }

    @Override
    public IDateChooser createDateChooser(ColumnType columnType) {
        return new InputDateChooser(columnType);
    }

    @Override
    public IIconButton createIconButton(ColumnType columnType, IEntry entry) {
        return new InputIconButton(columnType, entry) {
            @Override
            public String getStringRepresentation() {
                return "n.a.";
            }
        };
    }

    @Override
    public IOpenURLButton createOpenURLButton(ColumnType columnType) {
        return new InputOpenURLButton(columnType);
    }

    @Override
    public IPlaceHolder createPlaceHolder(String title) {
        return new InputPlaceHolder(title);
    }

    @Override
    public IPlaceholderButton createPlaceHolderButton(String title, String buttonLabel) {
        return new InputPlaceHolderButton(title, buttonLabel);
    }

    @Override
    public ITimeChooser createTimeChooser(ColumnType columnType) {
        return new InputTimeChooser(columnType);
    }

    @Override
    public IUserDisplayField createUserDisplayField(ColumnType columnType) {
        return new InputUserDisplayField(columnType);
    }

    @Override
    public IYesNoField createYesNoField(ColumnType columnType) {
        return new InputYesNoBox(columnType);
    }

}
