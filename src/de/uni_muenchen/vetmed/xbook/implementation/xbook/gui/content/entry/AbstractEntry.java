package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry;

import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.IEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import de.uni_muenchen.vetmed.xbook.api.helper.InputMaskModes;
import java.awt.Color;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractEntry extends AbstractEntryRoot implements IEntry {

    /**
     * The button to clear the entry.
     */
    protected XImageButton buttonClear;
    /**
     * The button to print the entry in the console.
     */
    protected XImageButton buttonPrint;
    /**
     * The button to print the entry in the console.
     */
    protected XImageButton buttonInsertDefaultValues;
    /**
     * The button to create a new entry.
     */
    protected XImageButton buttonNewEntry;
    /**
     * The button to reset the entry.
     */
    private XImageButton buttonReset;
    /**
     * The button to delete the entry.
     */
    private XImageButton buttonDelete;
    /**
     * The button to save and clear the fields for a new dataset.
     */
    protected XImageButton buttonSaveAndNewDataset;
    /**
     * The button to save the entry and proceed editing.
     */
    private XImageButton buttonSaveAndProceed;

    /**
     * Constructor. Basic constructor to load an Entry screen (new entry mode).
     *
     * @param mode
     */
    public AbstractEntry(GeneralInputMaskMode mode) {
        super(mode, true);
    }

    /**
     * Constructor. Basic constructor to load an Entry screen (new entry mode).
     *
     * @param mode
     * @param doInit
     */
    public AbstractEntry(GeneralInputMaskMode mode, boolean doInit) {
        super(mode, doInit);
    }

    /**
     * Constructor.
     *
     * Basic constructor to load an Entry screen (edit entry mode).
     *
     * @param mode
     * @param data
     */
    public AbstractEntry(GeneralInputMaskMode mode, EntryDataSet data) {
        super(mode, true, data);
    }

    public void setDeleteButtonEnabled(boolean status) {
        buttonDelete.setVisible(status);
    }

//    public void setClearButtonEnabled(boolean status) {
//         buttonClear.setVisible(status);
//    } 
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    // Overrideable Functions
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    @Override
    public ButtonPanel getButtonBar() {

        ButtonPanel buttonPanel = new ButtonPanel();

        if (generalMode == GeneralInputMaskMode.MULTI_EDIT) {
            buttonPanel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_BACK, Loc.get("BACK"), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            mainFrame.displayListingScreen();
                        }
                    }).start();
                }
            });
        }

        if (generalMode == GeneralInputMaskMode.SINGLE) {
            buttonDelete = buttonPanel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_DELETE, Loc.get("DELETE"), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Object[] options = {Loc.get("YES"), Loc.get("CANCEL")};
                    int n = JOptionPane.showOptionDialog(AbstractEntry.this,
                            Loc.get("ARE_YOU_SURE_YOU_WANT_TO_DELETE_THIS_ENTRY"),
                            Loc.get("DELETE"),
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE,
                            null, options, options[1]);
                    if (n == JOptionPane.YES_OPTION) {
                        try {
                            apiControllerAccess.deleteEntry(getManager(), loadedData.getEntryKey());
                            Footer.displayConfirmation(Loc.get("ENTRY_SUCCESSFUL_DELETED"));
                            try {
                                toNewEntryMode(false);
                            } catch (SwitchToNewModeImpossibleException ex) {
                                Logger.getLogger(AbstractEntry.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } catch (NotLoggedInException | NoRightException | StatementNotExecutedException | EntriesException | NotLoadedException ex) {
                            Logger.getLogger(AbstractEntry.class.getName()).log(Level.SEVERE, null, ex);
                            Footer.displayError(Loc.get("ENTRY_COULD_NOT_BE_DELETED"));
                        } catch (EntryInUseException e1) {
                            e1.printStackTrace();
                            Footer.displayWarning(Loc.get("ENTRY_IN_USE_COULT_NOT_BE_DELETED"));
                        }
                        clearFields();
                    }
                }
            });
        }
        if (generalMode == GeneralInputMaskMode.SINGLE) {
            buttonClear = buttonPanel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_CLEAR, Loc.get("CLEAR"), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Object[] options = {Loc.get("YES"), Loc.get("CANCEL")};
                    int n = JOptionPane.showOptionDialog(AbstractEntry.this,
                            Loc.get("ARE_YOU_SURE_YOU_WANT_TO_CLEAR_THIS_ENTRY"),
                            Loc.get("CLEAR"),
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE,
                            null, options, options[1]);
                    if (n == JOptionPane.YES_OPTION) {
                        clearFields();
                    }
                }
            });
        }

        buttonReset = buttonPanel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_REVERT, Loc.get("RESET"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Footer.startWorking();

                Object[] options = {Loc.get("YES"), Loc.get("CANCEL")};
                int n = JOptionPane.showOptionDialog(AbstractEntry.this,
                        Loc.get("ARE_YOU_SURE_YOU_WANT_TO_RESET_THIS_ENTRY"),
                        Loc.get("CLEAR"),
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE,
                        null, options, options[1]);
                if (n == JOptionPane.YES_OPTION) {
                    try {
                        resetFields();
                    } catch (WrongModeException ex) {
                        Logger.getLogger(AbstractEntry.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                Footer.stopWorking();
            }
        });

        if (generalMode == GeneralInputMaskMode.SINGLE) {
            buttonNewEntry = buttonPanel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_NEWFILE, Loc.get("NEW_ENTRY"), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    doToNewEntry();
                }
            });
        }

        if (XBookConfiguration.MODE == XBookConfiguration.Mode.DEVELOPMENT) { // DEBUG Button for output
            if (generalMode == GeneralInputMaskMode.SINGLE) {
                buttonPrint = buttonPanel.addImageButton(ButtonPanel.Position.NORTH_CENTER, Images.BUTTONPANEL_PLACEHOLDER, Loc.get("PRINT"), new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        printCurrentValues();
                    }
                });
                buttonPrint.setStyle(XImageButton.Style.GRAY);
            }


        }

        if (generalMode == GeneralInputMaskMode.SINGLE) {
            buttonSaveAndNewDataset = buttonPanel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_SAVENEW, "<html><center>" + Loc.get("SAVE_AND_NEW_DATASET") + "</center></html>", Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            saveEntry(true);
                        }
                    }).start();
                }
            });
        }

        if (generalMode == GeneralInputMaskMode.SINGLE) {
            buttonSaveAndProceed = buttonPanel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_SAVE, "<html><center>" + Loc.get("SAVE_AND_PROCCED") + "</center></html>", Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            saveEntry(false);
                        }
                    }).start();
                }
            });
        }

        if (generalMode == GeneralInputMaskMode.MULTI_EDIT) {
            buttonSaveAndProceed = buttonPanel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_MULTISAVE, "<html><center>" + Loc.get("SAVE_AND_PROCCED") + "</center></html>", Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            // if all is ok, start the default saving process
                            if (saveMultiEntry()) {
                                mainFrame.displayListingScreen();
                            }
                        }
                    }).start();
                }
            });
        }

        if (generalMode == GeneralInputMaskMode.MULTI_EDIT) {
            entryCountLabel = new JLabel("");
            entryCountLabel.setOpaque(true);
            // entryCountLabel.setBackground(Colors.CONTENT_BACKGROUND);
            entryCountLabel.setBackground(Color.YELLOW);
            buttonPanel.getButtonsCenterNorth().add(ComponentHelper.wrapComponent(ComponentHelper.wrapComponent(entryCountLabel, Color.YELLOW, 4, 40, 4, 40), Colors.CONTENT_BACKGROUND, 20, 0, 0, 0));
        }

        return buttonPanel;
    }

    private void doToNewEntry() {
        try {
            if (generalMode == GeneralInputMaskMode.SINGLE) {
                toNewEntryMode(false);
            } else if (generalMode == GeneralInputMaskMode.MULTI_EDIT) {
                mainFrame.displayEntryScreenInputUnit(true, false);
            }
        } catch (SwitchToNewModeImpossibleException ex) {
            Logger.getLogger(AbstractEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void toNewEntryMode(boolean ignoreRememberValues) throws SwitchToNewModeImpossibleException {
        super.toNewEntryMode(ignoreRememberValues);

        // set the correct buttons visible
        if (buttonClear != null && buttonDelete != null && buttonReset != null) {
            buttonClear.setVisible(true);
            buttonDelete.setVisible(false);
            buttonReset.setVisible(false);
        }

    }

    @Override
    protected void toUpdateEntryMode() throws SwitchToNewModeImpossibleException {
        super.toUpdateEntryMode();

        // set the correct buttons visible
        buttonClear.setVisible(false);
        buttonReset.setVisible(true);
        try {
            buttonDelete.setVisible(getManager() != null);
        } catch (NotLoggedInException ex) {
            Logger.getLogger(AbstractEntry.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setCrossConnectionEnabled(boolean isCrossConnected) {
        buttonReset.setVisible(isCrossConnected);
        buttonSaveAndNewDataset.setVisible(isCrossConnected);
        buttonSaveAndProceed.setVisible(isCrossConnected);

        try {
            buttonDelete.setVisible(getManager() != null && isCrossConnected);
        } catch (NotLoggedInException ex) {
            Logger.getLogger(AbstractEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public AbstractEntry copy(EntryDataSet data) {
        return null;
    }

    @Override
    public void setToHasWritingRightsMode(boolean b) {
        super.setToHasWritingRightsMode(b);

        if (mode == InputMaskModes.NEW_ENTRY) {
            buttonClear.setVisible(b);
            if (buttonReset != null) {
                buttonReset.setVisible(false);
            }
            if (buttonDelete != null) {
                buttonDelete.setVisible(false);
            }
        } else {
            if (buttonClear != null) {
                buttonClear.setVisible(false);
            }
            if (buttonReset != null) {
                buttonReset.setVisible(b);
            }
            if (buttonDelete != null) {
                try {
                    buttonDelete.setVisible(getManager() != null && b);
                } catch (NotLoggedInException ex) {
                    Logger.getLogger(AbstractEntry.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        if (buttonNewEntry != null) {
            buttonNewEntry.setVisible(b);
        }
        if (buttonSaveAndNewDataset != null) {
            buttonSaveAndNewDataset.setVisible(b);
        }
        if (buttonSaveAndProceed != null) {
            buttonSaveAndProceed.setVisible(b);
        }

    }



    @Override
    public void setButtonsEnabled(boolean status) {
        if (buttonSaveAndNewDataset != null) {
            buttonSaveAndNewDataset.setEnabled(status);
        }
        buttonSaveAndProceed.setEnabled(status);
    }
}
