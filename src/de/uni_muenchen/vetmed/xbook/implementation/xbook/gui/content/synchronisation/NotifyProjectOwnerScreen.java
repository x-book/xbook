package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.helper.ConflictMessageBuilder;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class NotifyProjectOwnerScreen extends AbstractContent {

    private SolveConflict solveConflictPanel;
    private JCheckBox checkPrintData;
    private JTextArea input;
    private XImageButton buttonSend;

    public NotifyProjectOwnerScreen(SolveConflict previousPanel) {
        super();
        init();
        this.solveConflictPanel = previousPanel;
    }

    @Override
    protected JPanel getContent() {
        JPanel wrapper = new JPanel(new StackLayout());
        wrapper.add(new XTitle("<html><center>" + Loc.get("NOTIFY_PROJECT_OWNER") + "</center></html>"));

        wrapper.setBackground(Colors.CONTENT_BACKGROUND);
        final MultiLineTextLabel text1 = new MultiLineTextLabel(Loc.get("NOTIFY_PROJECT_OWNER_LINE1"));
        text1.setBackground(Colors.CONTENT_BACKGROUND);
        text1.setBorder(new EmptyBorder(3, 5, 3, 5));
        wrapper.add(text1);

        JPanel inputPanel = new JPanel(new BorderLayout());
        inputPanel.setBackground(Colors.CONTENT_BACKGROUND);
        inputPanel.setBorder(new EmptyBorder(3, 5, 3, 5));
        input = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(input);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        input.setLineWrap(true);
        input.setWrapStyleWord(true);
        input.setRows(8);

        inputPanel.add(scrollPane);
        wrapper.add(inputPanel);

        MultiLineTextLabel text2 = new MultiLineTextLabel("\n" + Loc.get("NOTIFY_PROJECT_OWNER_LINE2") + "\n");
        text2.setBorder(new EmptyBorder(3, 5, 3, 5));
        text2.setBackground(Colors.CONTENT_BACKGROUND);
        wrapper.add(text2);

        checkPrintData = new JCheckBox(Loc.get("HIGHLIGHT_SELECTION"));
        checkPrintData.setBorder(new EmptyBorder(3, 5, 3, 5));
        checkPrintData.setBackground(Colors.CONTENT_BACKGROUND);
        wrapper.add(checkPrintData);

        MultiLineTextLabel text3 = new MultiLineTextLabel("\n" + Loc.get("NOTIFY_PROJECT_OWNER_LINE3"));
        text3.setBorder(new EmptyBorder(3, 5, 3, 5));
        text3.setBackground(Colors.CONTENT_BACKGROUND);
        wrapper.add(text3);
        return wrapper;
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();
        panel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_BACK, Loc.get("BACK"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Content.setContent(solveConflictPanel);
            }
        });
        buttonSend = panel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_MAIL, Loc.get("SEND_NOTIFICATION"), 1.5, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonSend.setEnabled(false);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        doSendMessage();
                    }
                }).start();
            }
        });
        return panel;
    }

    private void doSendMessage() {
        Footer.startWorking();

        try {

            String bookName = mainFrame.getController().getBookName();
            String projectName = mainFrame.getController().getCurrentProject().getProjectName();
            String userName = mainFrame.getController().getDisplayName();

            String headline = "[" + bookName + "] " + Loc.get("CONFLICT_OCCURED_IN_PROJECT", projectName);

            String messageString = ConflictMessageBuilder.getMessageNotify(projectName, userName, solveConflictPanel.getLines(), checkPrintData.isSelected(), input.getText());
            String messageHtml = ConflictMessageBuilder.getMessageNotifyHtml(projectName, userName, solveConflictPanel.getLines(), checkPrintData.isSelected(), input.getText());
            Message m = ((AbstractSynchronisationController) ((AbstractMainFrame) mainFrame).getController()).sendMessageToProjectOwner(headline, messageString, messageHtml);
            if (m.getResult().wasSuccessful()) {
                solveConflictPanel.selectAllServerValues();
                mainFrame.displayConflictScreen();
                Footer.displayConfirmation(Loc.get("MESSAGE_SUCCESSFULLY_SENT"));
            } else {
                Footer.displayError(Loc.get("ERROR_WHILE_SENDING_THE_MESSAGE") + ": " + m.getResult());
            }

        } catch (NotLoadedException | NotLoggedInException | NotConnectedException | IOException | StatementNotExecutedException | NoRightException ex) {
            Logger.getLogger(NotifyProjectOwnerScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            buttonSend.setEnabled(true);
            Footer.stopWorking();
        }
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }
}
