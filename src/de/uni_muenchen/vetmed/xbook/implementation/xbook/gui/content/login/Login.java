package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.login;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarLogin;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTextBlock;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackClickableLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackPasswordField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.PleaseQuoteBlock;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawCheckBoxSmall;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackCheckBoxSmall;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackImageButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTextBlock;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTextField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTitle;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTwoButtons;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableString;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JCheckBox;
import javax.swing.SwingUtilities;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Login extends AbstractContent {

    String s = "";

    /**
     * The textfield to enter the user name.
     */
    private StackTextField username;
    /**
     * The textfield to enter the password.
     */
    private StackPasswordField password;
    /**
     * The sidebar object what will be updated when a 'message of the day' is
     * available.
     */
    private SidebarLogin sidebar;
    /**
     * The checkbox where to set if the login data should be saved.
     */
    private JCheckBox rememberCheck;
    /**
     * The buttons to login and to reset.
     */
    private StackTwoButtons buttonLoginReset;

    boolean forceSidebar;

    /**
     * Constructor.
     */
    public Login() {
        super();
        init();
    }

    @Override
    protected JPanel getContent() {
        JPanel panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        panel.add(new XTextBlock("")); // use as spacer
        panel.add(new StackTitle(Loc.get("LOGIN")));
        panel.add(new StackTextBlock(Loc.get("LOGIN_INFORMATION")));

        username = new StackTextField(Loc.get("USER"), (Integer) Sizes.STACK_LABEL_WIDTH_SMALL, null);
        username.setValue(XBookConfiguration.getProperty(XBookConfiguration.REMEMBER_LOGIN_USER));
        panel.add(username);

        password = new StackPasswordField(Loc.get("PASSWORD"), Sizes.STACK_LABEL_WIDTH_SMALL);
        password.setValue(XBookConfiguration.getPasswordProperty(XBookConfiguration.REMEMBER_LOGIN_PASSWORD));
        password.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                rememberCheck.setSelected(false);
            }
        });
        panel.add(password);

        StackCheckBoxSmall remember = new StackCheckBoxSmall(new ColumnType("dummy", ColumnType.Type.UNKNOWN, ColumnType.ExportType.NONE), "", (Integer) Sizes.STACK_LABEL_WIDTH_SMALL);
        rememberCheck = ((RawCheckBoxSmall) remember.getElement()).getCheckBox();
        rememberCheck.setText(Loc.get("REMEMBER_PASSWORD?"));
        rememberCheck.setBackground(Colors.CONTENT_BACKGROUND);
        if (!password.getValue().isEmpty()) {
            rememberCheck.setSelected(true);
        }
        panel.add(remember);

//        if (Configuration.MODE == Configuration.Mode.DEVELOPMENT) {
//            username.setValue("ossobook");
//            password.setText("ossobook");
//        }
        panel.add(buttonLoginReset = new StackTwoButtons(null, Loc.get("LOGIN"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        login();
                    }
                });
            }
        }, null, Loc.get("RESET"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                username.clearInput();
                password.setText("");
                rememberCheck.setSelected(false);
            }
        }, Sizes.STACK_LABEL_WIDTH_SMALL));
        buttonLoginReset.setStyleSecondButton(XImageButton.Style.GRAY);

        panel.add(new StackClickableLabel(Loc.get("LOST_PASSWORD") + "?", new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Content.setContent(new LostPassword());
            }
        }));

        panel.add(new StackTitle(Loc.get("REGISTRATION")));
        panel.add(new StackTextBlock(Loc.get("LOGIN_REGISTRATION_INFORMATION")));
        StackImageButton buttonRegistration;
        panel.add(buttonRegistration = new StackImageButton(null, Loc.get("REGISTRATION"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.displayRegisterScreen();
            }
        }, Sizes.STACK_LABEL_WIDTH_SMALL));
        buttonRegistration.setStyle(XImageButton.Style.GRAY);

        setEnterKeyListeners();

        if (XBookConfiguration.MODE == XBookConfiguration.Mode.DEVELOPMENT) {
            addDevElements(panel);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //Sidebar.setSidebarAllowedContentSetting(false);
                    Sidebar.hideSidebar();
                    forceSidebar = false;
                    if (mainFrame.getController() instanceof AbstractSynchronisationController) {
                        Message ms = ((AbstractSynchronisationController) mainFrame.getController()).sendMessage(Commands.GET_MOTD);
                        if (ms.getResult().wasSuccessful()) {
                            String message = ((SerialisableString) ms.getData().get(0)).getString();

                            // wait until the sidebar object side is initialised
                            while (sidebar == null) {
                                try {
                                    Thread.sleep(500);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }

                            // then display the message of the day
                            if (!message.isEmpty()) {
                                sidebar.addHtmlPane(message, false);
                                forceSidebar = true;
                                //Sidebar.setSidebarAllowedContentSetting(true);
                                Sidebar.showSidebar();
                                Sidebar.setSidebarContent(sidebar);
                                sidebar.repaint();
                                sidebar.revalidate();
                            } else {
                                Sidebar.hideSidebar();
                            }
                        } else {
                            System.out.println(ms.getResult());
                        }
                    }
                } catch (NotConnectedException | NotLoggedInException | IOException ex) {
                    ex.printStackTrace();
                    Sidebar.hideSidebar();
                }
            }
        }).start();

        add(BorderLayout.SOUTH, new PleaseQuoteBlock(apiControllerAccess));
        return panel;
    }

    private void addDevElements(JPanel panel) {
    }

    @Override
    public ButtonPanel getButtonBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return forceSidebar;
    }

    @Override
    public SidebarPanel getSideBar() {
        if (sidebar == null) {
            sidebar = new SidebarLogin();
        }
        //don't display sidebar if empty...
        if (!forceSidebar) {
            return null;
        }

        return sidebar;
    }

    /**
     * Do the login.
     */
    private void login() {
        buttonLoginReset.getFirstButton().setEnabled(false);

        // save the user name in the properties in any case 
        try {
            XBookConfiguration.setProperty(XBookConfiguration.REMEMBER_LOGIN_USER, username.getValue());
        } catch (IsAMandatoryFieldException ex) {
            // not neccessary here!
        }

        // if the check box is selected, also save the password in the properties 
        if (rememberCheck.isSelected()) {
            XBookConfiguration.setPasswordProperty(XBookConfiguration.REMEMBER_LOGIN_PASSWORD, password.getValue());
        } else {
            XBookConfiguration.setPasswordProperty(XBookConfiguration.REMEMBER_LOGIN_PASSWORD, "");
        }

        // then do the login
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (username.getString().toUpperCase().equals("ROOT")) {
                        Footer.displayError(Loc.get("YOU_ARE_NOT_ALLOWED_TO_USE_THIS_USERNAME", username.getString()));
                        return;
                    }
                    ((AbstractMainFrame) mainFrame).getController().login(username.getValue(), password.getValue());
                } catch (IsAMandatoryFieldException ex) {
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }
                buttonLoginReset.getFirstButton().setEnabled(true);
            }
        }).start();
    }

    /**
     * Set the key listeners to the elements that should allow login with
     * pressing 'Enter'.
     */
    private void setEnterKeyListeners() {
        KeyAdapter adapter = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {

                if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                    login();
                    e.consume();
                }
            }
        };
        username.addKeyListener(adapter);
        password.addKeyListener(adapter);
    }

    @Override
    public void setFocus() {
        username.requestFocus();
    }
}
