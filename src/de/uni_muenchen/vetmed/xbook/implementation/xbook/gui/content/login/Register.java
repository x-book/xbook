package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.login;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.PleaseQuoteBlock;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.*;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.helper.DatabaseType;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Result;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Register extends AbstractContent {

    /**
     * The textfield to enter the user name.
     */
    private StackTextField userNameInput;
    /**
     * The textfield to enter the first name.
     */
    private StackTextField firstNameInput;
    /**
     * The textfield to enter the last name.
     */
    private StackTextField lastNameInput;
    /**
     * The textfield to enter the organisation.
     */
    private StackTextField organisationInput;
    /**
     * The textfield to enter the password.
     */
    private StackTextField email1Input;
    /**
     * The textfield to enter the password for a second time.
     */
    private StackTextField email2Input;
    /**
     * The textfield to enter the password.
     */
    private StackPasswordField password1Input;
    /**
     * The textfield to enter the password for a second time.
     */
    private StackPasswordField password2Input;

    /**
     * Constructor.
     */
    public Register() {
        this(true);
    }

    /**
     * Constructor.
     *
     * @param emailVisible <code>false</code> to hide the email input fields,
     *                     <code>true</code> else.
     */
    public Register(boolean emailVisible) {
        super();
        init();
        setEmailVisible(emailVisible);
    }

    @Override
    protected JPanel getContent() {
        JPanel panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        panel.add(new StackTitle(Loc.get("REGISTRATION")));
        panel.add(new StackTextBlock(Loc.get("REGISTRATION_INFORMATION")));
        panel.add(userNameInput = new StackTextField(Loc.get("LOGIN_NAME"), 16));

        panel.add(firstNameInput = new StackTextField(Loc.get("FIRST_NAME"), 50));
        panel.add(lastNameInput = new StackTextField(Loc.get("LAST_NAME"), 50));
        panel.add(organisationInput = new StackTextField(Loc.get("ORGANISATION"), 100));
        panel.add(email1Input = new StackTextField(Loc.get("EMAIL"), 50));
        panel.add(email2Input = new StackTextField(Loc.get("EMAIL_REPEAT"), 50));
        panel.add(password1Input = new StackPasswordField(Loc.get("PASSWORD", 9999)));
        panel.add(password2Input = new StackPasswordField(Loc.get("PASSWORD_REPEAT", 9999)));

        PleaseQuoteBlock m = new PleaseQuoteBlock(apiControllerAccess);
        m.setBackground(Color.YELLOW);
        panel.add(m);

        ActionListener listenerRegister = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    String userName = userNameInput.getString();
                    String firstName = firstNameInput.getString();
                    String lastName = lastNameInput.getString();
                    String organisation = organisationInput.getString();
                    String email1 = email1Input.getString();
                    String email2 = email2Input.getString();
                    String password1 = password1Input.getString();
                    String password2 = password2Input.getString();

                    if (userName.length() > 16) {
                        // should not happen!
                        Footer.displayError(Loc.get("MAXIMUM_LENGTH_OF_XXX_IS_XXX_CHARS", Loc.get("LOGIN_NAME"), "16"));
                        return;
                    }

                    if (userName.isEmpty()
                            || firstName.isEmpty()
                            || lastName.isEmpty()
                            || organisation.isEmpty()
                            || email1.isEmpty()
                            || email2.isEmpty()
                            || password1.isEmpty()
                            || password2.isEmpty()) {
                        Footer.displayError(Loc.get("AT_LEAST_ONE_INPUT_FIELD_IS_NOT_FILLED"));
                        return;
                    }

                    // check username maximal size
                    if (userName.length() >= 16) {
                        Footer.displayError(Loc.get("USERNAME_LENGTH_MUST_BE_LESS_THAN_16"));
                        return;
                    }

                    // if database H2 is used, dots (.) are not allowed in user name
                    if (apiControllerAccess.getDatabaseType() == DatabaseType.H2) {
                        if (userName.contains(".")) {
                            Footer.displayError(Loc.get("USERNAME_MAY_NOT_CONTAIN_A_DOT"));
                            return;
                        }

                    }

                    // check if email address is a valid one
                    Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
                    Matcher m = p.matcher(email1);
                    boolean matchFound = m.matches();
                    if (!matchFound) {
                        email1Input.clearInput();
                        email2Input.clearInput();
                        email1Input.setErrorStyle();
                        email2Input.setErrorStyle();
                        Footer.displayError(Loc.get("EMAIL_ADDRESS_INVALID"));
                        return;
                    }

                    // check if email addresses are the same ones
                    if (!email1.equals(email2)) {
                        email1Input.clearInput();
                        email2Input.clearInput();
                        email1Input.setErrorStyle();
                        email2Input.setErrorStyle();
                        Footer.displayError(Loc.get("EMAIL_ADDRESSES_ARE_NOT_IDENTICAL"));
                        return;
                    }

                    // check if passwords are the same ones
                    if (!password1.equals(password2)) {
                        password1Input.clearInput();
                        password2Input.clearInput();
                        password1Input.setErrorStyle();
                        password2Input.setErrorStyle();
                        Footer.displayError(Loc.get("PASSWORDS_ARE_NOT_IDENTICAL"));
                        return;
                    }

                    if (password1.length() > 41) {
                        // should not happen!
                        Footer.displayError(Loc.get("MAXIMUM_LENGTH_OF_XXX_IS_XXX_CHARS", Loc.get("PASSWORD"), "41"));
                        return;
                    }

                    // check the length of the password
                    if (password1.length() < 6) {
                        password1Input.clearInput();
                        password2Input.clearInput();
                        password1Input.setErrorStyle();
                        password2Input.setErrorStyle();
                        Footer.displayError(Loc.get("PASSWORD_IS_TOO_SHORT"));
                        return;
                    }

                    // check if the username only contains ASCII chars
                    if (!StringHelper.isPureAscii(userName)) {
                        userNameInput.clearInput();
                        userNameInput.setErrorStyle();
                        Footer.displayError(Loc.get("USERNAME_CONTAINS_INVALID_CHARACTERS", StringHelper.getNonAsciiChars(userName)));
                        return;
                    }

                    // check if the password only contains ASCII chars
                    if (!StringHelper.isPureAscii(password1)) {
                        password1Input.clearInput();
                        password2Input.clearInput();
                        password1Input.setErrorStyle();
                        password2Input.setErrorStyle();
                        Footer.displayError(Loc.get("PASSWORD_CONTAINS_INVALID_CHARACTERS", StringHelper.getNonAsciiChars(password1)));
                        return;
                    }

                    String password = password1;
                    String realName = firstName + " " + lastName;
                    String email = email1Input.getString();
                    try {
                        Message rs = ((AbstractMainFrame) mainFrame).getController().register(userName, realName, organisation, password, email);
                        if (rs == null) {
                            return;
                        } else if (rs.getResult().wasSuccessful()) {
                            Footer.displayConfirmation(Loc.get("REGISTRATION_SUCCEEDED"));
                            mainFrame.displayLoginScreen();
                            JOptionPane.showMessageDialog(((AbstractMainFrame) mainFrame), Loc.get("REGISTRATION_SUCCEEDED_DIALOG"), Loc.get("REGISTRATION_SUCCEEDED"), JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            switch (rs.getResult().getStatus()) {
                                case Result.USERNAME_TAKEN:
                                    Footer.displayError(Loc.get("ERROR_MESSAGE>USERNAME_TAKEN"));
                                    break;
                                case Result.MAIL_TAKEN:
                                    Footer.displayError(Loc.get("ERROR_MESSAGE>MAIL_TAKEN"));
                                    break;
                                default:
                                    Footer.displayError(Loc.get("REGISTRATION_FAILED"));
                                    break;
                            }
                        }
                    } catch (IOException | NotLoggedInException | NotConnectedException ex) {
                        Footer.displayError(Loc.get("REGISTRATION_FAILED"));
                        Logger.getLogger(AbstractController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (IsAMandatoryFieldException ex) {
                    Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };

        ActionListener listenerReset = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userNameInput.clearInput();
                firstNameInput.clearInput();
                lastNameInput.clearInput();
                organisationInput.clearInput();
                email1Input.clearInput();
                email2Input.clearInput();
                password1Input.clearInput();
                password2Input.clearInput();
            }
        };

        panel.add(new StackTwoButtons(Loc.get("REGISTER"), listenerRegister, Loc.get("RESET"), listenerReset));
        return panel;
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();
        panel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_BACK, Loc.get("BACK"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.displayLoginScreen();
            }
        });
        return panel;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    /**
     * Hides the email input fields.
     * <p>
     * Necessary for Books like PalaeoDepot or AnthroDepot that do not save the
     * email adress in the database.
     * <p>
     * If fields are set to invisibile the place holder "not@in.use" is inserted
     * to pass the email validation test.
     *
     * @param status <code>false</code> to hide the email input fields,
     *               <code>true</code> else.
     */
    private void setEmailVisible(boolean status) {
        email1Input.setVisible(status);
        email2Input.setVisible(status);
        if (!status) {
            email1Input.setValue("not@in.use");
            email2Input.setValue("not@in.use");
        }
    }
}
