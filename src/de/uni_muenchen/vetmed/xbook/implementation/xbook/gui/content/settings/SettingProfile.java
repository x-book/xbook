package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.User;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.settingPanel.AbstractSettingPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class SettingProfile extends AbstractSettingPanel {

    /**
     * The basic mainframe object.
     */
    private AbstractMainFrame mainFrame;
    /**
     * The panel that holds the standard content.
     */
    private JPanel userDataWrapper;
    /**
     * The panel that holds the error content.
     */
    private JPanel errorPanel;
    private JTextField userDisplayName;
    private JTextField organisation;
    private de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.fields.TwoRadioButtons allowPublicSearch;
    private JPasswordField passwordOld;
    private JPasswordField passwordNew1;
    private JPasswordField passwordNew2;
    private User profile;

    /**
     * Constructor.
     *
     * @param mainFrame The basic mainframe object.
     */
    public SettingProfile(final AbstractMainFrame mainFrame) {
        this.mainFrame = mainFrame;

        userDataWrapper = new JPanel(new StackLayout());
        errorPanel = new JPanel(new StackLayout());
        try {
            profile = mainFrame.getController().getUserProfile();
        } catch (NotLoggedInException | StatementNotExecutedException | EntriesException ex) {
            profile = new User(-1, "", "", "", true);
        }

        userDataWrapper.setVisible(true);
        errorPanel.setVisible(false);

        // compose error screen
        errorPanel.add(ComponentHelper.wrapComponent(createTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("EDIT_USER_DATA_INFORMATION")), 0, 10, 10, 10));
        errorPanel.add(ComponentHelper.wrapComponent(createTitleBar(Loc.get("ONLY_AVAILABLE_IF_A_USER_IS_LOGGED_IN")), 0, 10, 10, 10));

        // compose user data screen
        userDataWrapper.add(ComponentHelper.wrapComponent(createTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("EDIT_USER_DATA_INFORMATION")), 0, 10, 10, 10));

        userDataWrapper.add(ComponentHelper.wrapComponent(createTitleBar(Loc.get("USER_NAME")), 0, 10, 10, 10));
        userDataWrapper.add(ComponentHelper.wrapComponent(createTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("USER_NAME_SETTING_INFO")), 0, 10, 10, 10));
        String userName = "";
        try {
            userName = mainFrame.getController().getUserName();
        } catch (java.lang.NullPointerException | NotLoggedInException ex) {
        }
        JTextField userNameField = createTextField(userName);
        userNameField.setEnabled(false);
        userNameField.setPreferredSize(new Dimension(300, Sizes.BUTTON_HEIGHT));
        userNameField.setBackground(new Color(222, 222, 222));
        userDataWrapper.add(ComponentHelper.wrapComponent(userNameField, 0, 10, 10, 10));

        userDataWrapper.add(ComponentHelper.wrapComponent(createTitleBar(Loc.get("DISPLAY_NAME")), 0, 10, 10, 10));
        userDataWrapper.add(ComponentHelper.wrapComponent(createTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("DISPLAY_NAME_SETTING_INFO")), 0, 10, 10, 10));
        String displayName = "";
        try {
            displayName = profile.getDisplayName();
        } catch (NullPointerException ex) {
        }
        userDataWrapper.add(ComponentHelper.wrapComponent(userDisplayName = createTextField(displayName), 0, 10, 10, 10));
        userDisplayName.setPreferredSize(new Dimension(300, Sizes.BUTTON_HEIGHT));

        userDataWrapper.add(ComponentHelper.wrapComponent(createTitleBar(Loc.get("ORGANISATION")), 0, 10, 10, 10));
        String organisationName = "";
        try {
            organisationName = profile.getOrganisation();
        } catch (NullPointerException ex) {
        }
        userDataWrapper.add(ComponentHelper.wrapComponent(organisation = createTextField(organisationName), 0, 10, 10, 10));
        organisation.setPreferredSize(new Dimension(300, Sizes.BUTTON_HEIGHT));

        userDataWrapper.add(ComponentHelper.wrapComponent(createTitleBar(Loc.get("ALLOW_PUBLIC_SEARCH")), 0, 10, 10, 10));
        userDataWrapper.add(ComponentHelper.wrapComponent(createTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("ALLOW_PUBLIC_SEARCH_SETTING_INFO")), 0, 10, 10, 10));
        userDataWrapper.add(ComponentHelper.wrapComponent(allowPublicSearch = createTwoRadioButtons(Colors.CONTENT_BACKGROUND, Loc.get("YES"), Loc.get("NO")), 0, 10, 10, 10));
        allowPublicSearch.setYesSelected(profile.isAllowPublicSearch());

        userDataWrapper.add(ComponentHelper.wrapComponent(createTitleBar(Loc.get("CHANGE_PASSWORD")), 10, 10, 10, 10));
        userDataWrapper.add(ComponentHelper.wrapComponent(createTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("ENTER_THE_ORIGIN_PASSWORD_INFORMATION")), 0, 10, 10, 10));

        passwordOld = createPasswordField();
        passwordOld.setPreferredSize(new Dimension(300, 20));
        userDataWrapper.add(wrap(passwordOld));

        userDataWrapper.add(ComponentHelper.wrapComponent(createTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("ENTER_THE_NEW_PASSWORD_INFORMATION")), 0, 10, 10, 10));

        passwordNew1 = createPasswordField();
        passwordNew1.setPreferredSize(new Dimension(300, 20));
        userDataWrapper.add(wrap(passwordNew1));

        passwordNew2 = createPasswordField();
        passwordNew2.setPreferredSize(new Dimension(300, 20));
        userDataWrapper.add(wrap(passwordNew2));

        userDataWrapper.add(ComponentHelper.wrapComponent(createTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("CHANGE_PASSWORD_BUTTON_INFORMATION")), 0, 10, 10, 10));

        XButton buttonSavePassword = new XButton(Loc.get("CHANGE_PASSWORD_NOW"), 2 * XButton.DEFAULT_WIDTH);
        buttonSavePassword.setStyle(XButton.Style.DEFAULT);
        buttonSavePassword.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String inputPasswordOld = String.valueOf(passwordOld.getPassword());
                String inputPasswordNew1 = String.valueOf(passwordNew1.getPassword());
                String inputPasswordNew2 = String.valueOf(passwordNew2.getPassword());

                if (inputPasswordOld.isEmpty()
                        || inputPasswordNew1.isEmpty()
                        || inputPasswordNew2.isEmpty()) {
                    Footer.displayError(Loc.get("AT_LEAST_ONE_INPUT_FIELD_IS_NOT_FILLED"));
                    return;
                }

                // check if new passwords are the same ones
                if (!inputPasswordNew1.equals(inputPasswordNew2)) {
                    passwordNew1.setText("");
                    passwordNew2.setText("");
                    Footer.displayError(Loc.get("PASSWORDS_ARE_NOT_IDENTICAL"));
                    return;
                }

                if (inputPasswordNew1.length() < 6) {
                    passwordNew1.setText("");
                    passwordNew2.setText("");
                    Footer.displayError(Loc.get("PASSWORD_IS_TOO_SHORT"));
                    return;
                }

                try {
                    Message rs = mainFrame.getController().changePassword(inputPasswordOld, inputPasswordNew1);
                    if (rs.getResult().wasSuccessful()) {
                        Footer.displayConfirmation(Loc.get("PASSWORD_CHANGE_SUCCESSFUL"));
                        JOptionPane.showMessageDialog(mainFrame, Loc.get("PASSWORD_CHANGE_SUCCESSFUL_DIALOG"), Loc.get("PASSWORD_CHANGE_SUCCESSFUL"), JOptionPane.INFORMATION_MESSAGE);
                        passwordOld.setText("");
                        passwordNew1.setText("");
                        passwordNew2.setText("");
                    } else {
                        Footer.displayError(Loc.get("PASSWORD_CHANGE_UNSUCCESSFUL"));
                    }
                } catch (IOException | NotLoggedInException | NotConnectedException ex) {
                    Logger.getLogger(SettingProfile.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        userDataWrapper.add(wrap(buttonSavePassword));


        // check if a user is logged in. If not set the error screen visible.
        try {
            mainFrame.getController().getUserName();
        } catch (NotLoggedInException e) {
            userDataWrapper.setVisible(false);
            errorPanel.setVisible(true);
        }

        setLayout(new StackLayout());
        add(errorPanel);
        add(userDataWrapper);
        ComponentHelper.colorAllChildren(this, Colors.CONTENT_BACKGROUND);
    }

    @Override
    public void save() {
        if (userDataWrapper.isVisible()) {
            User newProfileData = new User(profile.getId(), profile.getUserName(), userDisplayName.getText(), organisation.getText(), allowPublicSearch.isYesSelected());
            if (!profile.equals(newProfileData)) {
                try {
                    Message ms = ((AbstractSynchronisationController) mainFrame.getController()).saveUserProfile(newProfileData);
                    if (ms.getResult().wasSuccessful()) {
                        ((AbstractSynchronisationController) mainFrame.getController()).updateUserTables();
                        Footer.updateUserLabel(mainFrame.getController().getDisplayName(true));
                    } else {
                        Footer.displayError(ms.getResult().toString());
                    }
                } catch (IOException | NotConnectedException | NotLoggedInException | StatementNotExecutedException ex) {
                }
            }
        }
    }

    @Override
    public void reset() {
        if (userDataWrapper.isVisible()) {
            allowPublicSearch.setYesSelected(profile.isAllowPublicSearch());
            userDisplayName.setText(profile.getDisplayName());
            organisation.setText(profile.getOrganisation());
        }
    }

    @Override
    public void setDefaultValues() {
        reset();
    }

    private JPanel wrap(JComponent comp) {
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(BorderLayout.WEST, comp);
        return ComponentHelper.wrapComponent(panel, 0, 10, 10, 10);
    }

    /**
     * Check if any input has been done in one or more of the password fields.
     *
     * @return The status if a input has been done.
     */
    public boolean isInputInPasswordFields() {
        return !(passwordOld.getPassword().length == 0 && passwordNew1.getPassword().length == 0 && passwordNew2.getPassword().length == 0);
    }

    /**
     * Clear all password fields (new and old ones).
     */
    public void clearPasswordFields() {
        passwordNew1.setText("");
        passwordNew2.setText("");
        passwordOld.setText("");
    }
}
