/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.combosearch;

/**
 *
 * @author ciaran.harrington@extern.lrz-muenchen.de
 */
import java.util.Collection;
//package de.uni_muenchen.vetmed.xbook.xbook.gui.components.content.project.rights;
/*
 * Interface to search an underlying characteristics of items and return a collection of found items.
 * @author G. Cope
 *
 * @param  The type of items to be found.
 * @param  The type of items to be searched
 */
    public interface Searchable<V> {
	/**
	 * Searches an underlying characteristics of items consisting of type E
	 * @param value A searchable value of type V
	 * @return A Collection of items of type E.
	 */
	public Collection search(V value );
}