package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.page.model;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.LookupTableBuilerTableModel;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import java.util.ArrayList;

public class ColumnNameTableModel extends LookupTableBuilerTableModel<String, ColumnType, String> {

    protected ColumnType[] requiredColumns;
    private IMainFrame mainFrame;

    public ColumnNameTableModel(IMainFrame mainFrame, ColumnType[] names) {
        this.mainFrame = mainFrame;
        requiredColumns = names;
    }

    @Override
    public Object[] getRow(String d, ColumnType c) {
        if (c == null) {
            return new Object[]{d, ""};
        }
        return new Object[]{d, c.getDisplayName()};
    }

    @Override
    public String convertValue(ColumnType c) {
        return c.getDisplayName();
    }

    @Override
    public String convertKey(ColumnType c) {
        if (c == null) {
            return null;
        }
        return c.getColumnName();
    }

    @Override
    public int getEditorColumnIndex() {
        return 1;
    }

    @Override
    public String[] getColumnNames() {
        return new String[]{
            Loc.get("COLUMN_NAME_IN", Loc.get("FILE")),
            Loc.get("COLUMN_NAME_IN", mainFrame.getController().getBookName())};
    }

    @Override
    public boolean isMapComplete() {
        for (ColumnType requiredCol : requiredColumns) {
            if (!getInternalMap().values().contains(requiredCol)) {
                return false;
            }
        }
        return true;
    }

    public ArrayList<ColumnType> getMissingColumns() {
        ArrayList<ColumnType> al = new ArrayList<>();
        for (ColumnType requiredCol : requiredColumns) {
            if (!getInternalMap().values().contains(requiredCol)) {
                al.add(requiredCol);
            }
        }
        return al;
    }

}
