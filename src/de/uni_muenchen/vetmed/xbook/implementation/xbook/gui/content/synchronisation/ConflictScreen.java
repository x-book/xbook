package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractSynchronisationManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractExtendedEntryManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportResult;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportRow;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseProjectEntryManager;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.ColouredLines;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarConflicts;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.ManagerType;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Result;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableArrayList;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableString;

import java.awt.BorderLayout;
import java.util.HashMap;
import javax.swing.BorderFactory;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ConflictScreen extends AbstractContent {

    /**
     * Holds all single entry datas.
     */
    protected HashMap<BaseEntryManager, ArrayList<ExportRow>> entryMaps;
    protected ArrayList<ArrayList<DataSetOld>> dynamicMaps;
    protected final String DISPLAY_SEPERATOR = ", ";

    /**
     * Constructor.
     */
    public ConflictScreen() {
        super();
        init();
    }

    @Override
    protected JPanel getContent() {
        entryMaps = new HashMap<>();
        dynamicMaps = new ArrayList<>();

        JPanel wrapper = new JPanel(new StackLayout());
        wrapper.setBackground(Colors.CONTENT_BACKGROUND);

        boolean existsConflicts = false;

        // ////////////////////////////////////////
        // display the conflicted entries
        // ////////////////////////////////////////
        ArrayList<IBaseManager> syncTables;
        ExportResult res;
        final int LIMIT = 30;
        ProjectDataSet pr;
        try {
            pr = mainFrame.getController().getCurrentProject();
            syncTables = ((AbstractController) mainFrame.getController()).getLocalManager().getSyncTables();
        } catch (NotLoadedException | NotLoggedInException ex) {
            Logger.getLogger(ConflictScreen.class.getName()).log(Level.SEVERE, null, ex);
            wrapper.add(new XTitle(Loc.get("NO_PROJECT_LOADED")));
            return wrapper;
        }

        for (IBaseManager s : syncTables) {
            try {
                // first check if the elements has to be displayed
                if (!(s instanceof BaseEntryManager)) {
                    continue;
                }
                AbstractBaseEntryManager manager = (AbstractBaseEntryManager) s;
                entryMaps.put(manager, new ArrayList<ExportRow>());
                try {
                    res = getConflictableEntries(LIMIT, pr, manager);
                } catch (NotLoggedInException ex) {
                    Logger.getLogger(ConflictScreen.class.getName()).log(Level.SEVERE, null, ex);
                    wrapper.add(new XTitle(Loc.get("NOT_LOGGED_IN")));
                    return wrapper;
                } catch (NoRightException ex) {
                    Logger.getLogger(ConflictScreen.class.getName()).log(Level.SEVERE, null, ex);
                    wrapper.add(new XTitle(Loc.get("NO_RIGHTS_TO_EDIT_CONFLICTS")));
                    return wrapper;
                }
                if (res.getEntries().isEmpty()) {
                    continue;
                }
                existsConflicts = true;

                // if yes, display the items
                wrapper.add(new XTitle(manager.localisedTableName));
                if (res.getEntries().size() >= LIMIT) {
                    JLabel hintLabel = new JLabel("<html><b>" + Loc.get("LIMITED_BUT_MORE_ENTRIES_AVAILABLE", LIMIT) + "</b></html>");
                    hintLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 6, 0));
                    wrapper.add(hintLabel);
                }
                EntryLines colouredLines = new EntryLines(manager);
                int i = 0;
                for (ExportRow e : res.getEntries().values()) {
                    entryMaps.get(manager).add(e);
                    //addLineTo(colouredLines, i++, getEntryLineInformation(e));
                    addLineTo(colouredLines, i++, manager.toString(e));
                }
                wrapper.add(colouredLines);

            } catch (StatementNotExecutedException | NotLoadedException ex) {
                Logger.getLogger(ConflictScreen.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        // ////////////////////////////////////////
        // display other conflicted elements
        // ////////////////////////////////////////
        int counter = 0;
        for (IBaseManager s : syncTables) {
            try {
                // first check if the elements has to be displayed
                if (s instanceof AbstractExtendedEntryManager) {
                    continue;
                }
                if (!(s instanceof AbstractBaseProjectEntryManager)) {
                    continue;
                }
                AbstractBaseProjectEntryManager absync = (AbstractBaseProjectEntryManager) s;
                ArrayList<DataSetOld> con = getProjectConflicts(pr, absync);
                if (con.isEmpty()) {
                    continue;
                }
                existsConflicts = true;

                // if yes, display the items
                dynamicMaps.add(con);
                wrapper.add(new XTitle(absync.getLocalisedTableName()));
                DynamicLines colouredLines = new DynamicLines(counter, absync);
                for (int i = 0; i < con.size(); i++) {
                    //addLineTo(colouredLines, i, "<b>►&nbsp;&nbsp;&nbsp;</b>" + getDynamicLineText(con.get(i)));
                    addLineTo(colouredLines, i, absync.toString(con.get(i)));
                }
                wrapper.add(colouredLines);
                counter++;
            } catch (StatementNotExecutedException | NotLoadedException ex) {
                Logger.getLogger(ConflictScreen.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        if (!existsConflicts) {
            try {
                XBookConfiguration.setConflict(mainFrame.getController().getCurrentProject().getProjectKey(), false);
                Footer.setConflictedButtonVisible(false);
                wrapper.add(new XTitle(Loc.get("NO_FURTHER_CONFLICTS")));
                // check one time more for conflicted data sets
                if (mainFrame.getController().isCurrentProjectConflicted()) {
                    XBookConfiguration.setConflict(mainFrame.getController().getCurrentProject().getProjectKey(), true);
                    Footer.setConflictedButtonVisible(true);
                }
            } catch (NotLoadedException | NoRightException | NotLoggedInException | StatementNotExecutedException ex) {
                Logger.getLogger(ConflictScreen.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return wrapper;
    }

    protected ArrayList<DataSetOld> getProjectConflicts(ProjectDataSet pr, AbstractBaseProjectEntryManager absync) throws StatementNotExecutedException, NotLoadedException {
        return absync.getConflicts(pr);
    }

    /**
     * Displays conflictable entries
     * @param LIMIT
     * @param pr
     * @param manager
     * @return
     * @throws NotLoggedInException
     * @throws NotLoadedException
     * @throws StatementNotExecutedException
     * @throws NoRightException
     */
    protected ExportResult getConflictableEntries(int LIMIT, ProjectDataSet pr, AbstractBaseEntryManager manager) throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        return manager.getEntries(pr, manager.getConflictDisplayColumns(), ColumnType.ExportType.GENERAL, true, 0, LIMIT, null,false);

    }

    @Override
    public ButtonPanel getButtonBar() {
        return null;
    }

    @Override
    public SidebarPanel getSideBar() {
        return new SidebarConflicts();
    }

    @Override
    public boolean forceSidebar() {
        return true;
    }

    private void addLineTo(ColouredLines colouredLines, int index, String lineText) {
        ColouredLines.Line line = colouredLines.addLine(index);
        line.setLayout(new BorderLayout());
        JPanel textWrapper = new JPanel(new BorderLayout());
        MultiLineTextLabel text1 = new MultiLineTextLabel(lineText);
        textWrapper.add(BorderLayout.CENTER, text1);
        JPanel borderedWrapper = ComponentHelper.wrapComponent(textWrapper, 5, 10, 5, 10);
        line.add(BorderLayout.CENTER, borderedWrapper);

        // add custom mouse listeners
        text1.setOpaque(false);
        textWrapper.setOpaque(false);
        borderedWrapper.setOpaque(false);
        text1.addMouseListener(line.getMouseListener());
        textWrapper.addMouseListener(line.getMouseListener());
        borderedWrapper.addMouseListener(line.getMouseListener());
    }

    private class DynamicLines extends ColouredLines {

        private int arrayIndex;
        private AbstractSynchronisationManager syncInterface;

        public DynamicLines(int arrayIndex, AbstractSynchronisationManager syncInterface) {
            super();
            this.arrayIndex = arrayIndex;
            this.syncInterface = syncInterface;
        }

        public DynamicLines(int arrayIndex, int lineHeight, AbstractSynchronisationManager syncInterface) {
            super(lineHeight);
            this.arrayIndex = arrayIndex;
            this.syncInterface = syncInterface;
        }

        @Override
        public void getActionOnDoubleClick(int index) {
            ArrayList<String> prim = syncInterface.getPrimaryColumns();
            SerialisableArrayList<DataColumn> additionalPrimaryKeyValues = new SerialisableArrayList<>();
            for (DataRow d : dynamicMaps.get(arrayIndex).get(index).getOrCreateDataTable(syncInterface.tableName)) {
                for (DataColumn dd : d.iterator()) {
                    if (!dd.getColumnName().equals(AbstractSynchronisationManager.PROJECT_ID.getColumnName())
                            && !dd.getColumnName().equals(AbstractSynchronisationManager.PROJECT_DATABASE_ID.getColumnName())
                            && !dd.getColumnName().equals(syncInterface.tableName + "." + AbstractSynchronisationManager.PROJECT_ID.getColumnName())
                            && !dd.getColumnName().equals(syncInterface.tableName + "." + AbstractSynchronisationManager.PROJECT_DATABASE_ID.getColumnName())
                            && (prim.contains(syncInterface.tableName + "." + dd.getColumnName())) || (prim.contains(dd.getColumnName()))) {
                        additionalPrimaryKeyValues.add(dd);
                    }
                }
            }
            ArrayList<Serialisable> list = new ArrayList<>();
            try {
                list.add(mainFrame.getController().getCurrentProject().getProjectKey());
                list.add(additionalPrimaryKeyValues);
                list.add(new SerialisableString(syncInterface.tableName));
                Message ms = ((AbstractSynchronisationController) mainFrame.getController()).sendMessage(Commands.REQUEST_SPECIFIC_PROJECT_DATA, list);
                if (ms.getResult().wasSuccessful()) {
                    mainFrame.displaySolveConflictScreen(dynamicMaps.get(arrayIndex).get(index), (DataSetOld) ms.getData().get(0), syncInterface, ManagerType.PROJECT);
                } else {
                    if (ms.getResult().getStatus() == Result.ENTRY_NOT_FOUND) {
                        ArrayList<DataColumn> primList = additionalPrimaryKeyValues.getData();
                        primList.add(new DataColumn(mainFrame.getController().getCurrentProject().getProjectKey().getID() + "", IStandardColumnTypes.PROJECT_ID.getColumnName()));
                        primList.add(new DataColumn(mainFrame.getController().getCurrentProject().getProjectKey().getDBID() + "", IStandardColumnTypes.PROJECT_DATABASE_ID.getColumnName()));
                        syncInterface.setStatusToSynchronise(primList);
                        mainFrame.displayConflictScreen();
                        Footer.displayConfirmation(Loc.get("CONFLICT_WAS_RESOLVED"));
                    }
                }
            } catch (NotLoadedException |  NotConnectedException | StatementNotExecutedException ex) {
                Logger.getLogger(ConflictScreen.class.getName()).log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            }
        }

        @Override
        public JPopupMenu getPopupMenu() {
            return null;
        }
    }

    private class EntryLines extends ColouredLines {

        private AbstractBaseEntryManager manager;

        public EntryLines(AbstractBaseEntryManager manager) {
            this.manager = manager;
        }

        @Override
        public void getActionOnDoubleClick(int index) {
            try {
                Key entryKey = new Key(Integer.valueOf(entryMaps.get(manager).get(index).get(AbstractExtendedEntryManager.ID).getStringData()),
                        Integer.valueOf(entryMaps.get(manager).get(index).get(AbstractExtendedEntryManager.DATABASE_ID).getStringData()));
                Key projectKey = new Key(Integer.valueOf(entryMaps.get(manager).get(index).get(AbstractExtendedEntryManager.PROJECT_ID).getStringData()),
                        Integer.valueOf(entryMaps.get(manager).get(index).get(AbstractExtendedEntryManager.PROJECT_DATABASE_ID).getStringData()));

                DataSetOld local = new EntryDataSet(entryKey, projectKey, mainFrame.getController().getDbName(), manager.tableName);
                manager.loadConflicts(local);
                ArrayList<Serialisable> list = new ArrayList<>();
                list.add(projectKey);
                list.add(entryKey);
                list.add(new SerialisableString(manager.tableName));

                Message ms = ((AbstractSynchronisationController) mainFrame.getController()).sendMessage(Commands.REQUEST_SPECIFIC_ENTRY, list);
                if (ms.getResult().wasSuccessful()) {
                    //only one entry expected...
                    EntryDataSet serialisable = (EntryDataSet) ms.getData().get(0);
                    mainFrame.displaySolveConflictScreen(local, serialisable, manager, ManagerType.ENTRY);

                } else {
                    if (ms.getResult().equals(Result.ENTRY_NOT_FOUND)) {
                        manager.solveConflict(local);
                        mainFrame.displayConflictScreen();
                        Footer.displayConfirmation(Loc.get("CONFLICT_WAS_RESOLVED"));
                    }
                }
            } catch (NotConnectedException | NotLoggedInException | StatementNotExecutedException ex) {
                Logger.getLogger(ConflictScreen.class.getName()).log(Level.SEVERE, ex.getLocalizedMessage(), ex);

            }
        }

        @Override
        public JPopupMenu getPopupMenu() {
            return null;
        }
    }
}
