package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawYesNoField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IYesNoField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

/**
 * A checkbox as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputYesNoBox extends AbstractDefaultInputElement implements IYesNoField {

    /**
     * The text field object.
     */
    protected RawYesNoField zField;

    public InputYesNoBox( ColumnType columnType) {
        super(columnType);       
    }

    @Override
    public AbstractInputElementFunctions createInputField() {
        if (zField == null) {
            zField = new RawYesNoField(columnType);
        }
        return zField;
    }

    @Override
    public void setSelectedYes() {
        zField.setSelectedYes();
    }

    @Override
    public void setSelectedNo() {
        zField.setSelectedNo();
    }

    @Override
    public void setSelectedNone() {
        zField.setSelectedNone();
    }

    @Override
    public void setSelected(State state) {
        zField.setSelected(state);
    }

    @Override
    public void setSelected(String id) {
        zField.setSelected(id);
    }

    @Override
    public State getSelectedValue() {
        return zField.getSelectedValue();
    }

    @Override
    public String getSelectedID() {
        return zField.getSelectedID();
    }

    @Override
    public boolean isSelectedYes() {
        return zField.isSelectedYes();
    }

    @Override
    public boolean isSelectedNo() {
        return zField.isSelectedNo();
    }

    @Override
    public boolean isSelectedNone() {
        return zField.isSelectedNone();
    }

}
