package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawTimeField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.ITimeChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputTimeChooser extends AbstractDefaultInputElement implements ITimeChooser {

    protected RawTimeField zField;

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input field.
     */
    public InputTimeChooser(ColumnType columnType) {
        super(columnType);
    }

    @Override
    public AbstractInputElementFunctions createInputField() {
        if (zField == null) {
            zField = new RawTimeField(columnType);
        }
        return zField;
    }

    @Override
    public void load(DataSetOld data) {
        zField.load(data);
    }

    @Override
    public void save(DataSetOld data) {
        zField.save(data);
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();

        zField.getTextField().addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                setDefaultStyle(true);
            }

            @Override
            public void focusLost(FocusEvent e) {
                zField.checkInput();
            }
        });
    }

    // ========================================================================


    @Override
    public void setTime(String time) {
        zField.setTime(time);
    }

    @Override
    public String getTime() {
        return zField.getTime();
    }

}
