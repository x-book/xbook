package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.filter;

import de.uni_muenchen.vetmed.xbook.api.Loc;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class SyncFilterRowYesNo extends AbstractSyncFilterRow<SyncFilterRowYesNo.YesNoInput> {

    public SyncFilterRowYesNo(String labelText) {
        super(labelText);
        buttonContainsEquals.setVisible(false);
    }

    @Override
    protected YesNoInput createInput() {
        return new YesNoInput();
    }

    @Override
    public SyncFilterRowData getCurrentData() {
        ArrayList<String> list = new ArrayList<>();
        if (input.yes.isSelected()) {
            list.add("1");
        }
        if (input.no.isSelected()) {
            list.add("-1");
            list.add("0");
        }

        return new SyncFilterRowData(SyncFilterRowMode.IS, list);
    }

    @Override
    public void reset() {
        input.yes.setSelected(false);
        input.no.setSelected(false);
    }

    protected class YesNoInput extends JPanel {

        private JCheckBox yes;
        private JCheckBox no;

        public YesNoInput() {
            super(new FlowLayout(FlowLayout.LEFT));

            add(yes = new JCheckBox(Loc.get("YES")));
            add(no = new JCheckBox(Loc.get("NO")));
        }
    }

}