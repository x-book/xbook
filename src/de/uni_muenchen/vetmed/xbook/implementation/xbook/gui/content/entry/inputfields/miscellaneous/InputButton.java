package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.IEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IButton;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.AbstractExtendedInputContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectEdit;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * A button as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class InputButton extends AbstractInputElement implements IButton<AbstractExtendedInputContent> {

    /**
     * The button object.
     */
    protected XButton button;
    /**
     * The entry screen.
     */
    protected IEntry entry;
    /**
     * The project edit screen.
     */
    protected AbstractProjectEdit projectEdit;
    protected AbstractExtendedInputContent contentToOpen;

    /**
     * Constructor.
     *
     * @param columnType The database column data of the corresponding input field.
     */
    public InputButton(ColumnType columnType) {
        super(columnType);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the corresponding input field.
     * @param entry      The corresponding Abstract entry object.
     */
    public InputButton(ColumnType columnType, IEntry entry) {
        super(columnType);
        this.entry = entry;
    }

    /**
     * Constructor.
     *
     * @param columnType  The database column data of the corresponding input field.
     * @param projectEdit The corresponding project edit object.
     */
    public InputButton(ColumnType columnType, AbstractProjectEdit projectEdit) {
        super(columnType);
        this.projectEdit = projectEdit;
    }

    public void addActionListener(ActionListener al) {
        getButton().addActionListener(al);
    }

    /**
     * Set an AbstractExtendedInputContent object that is load when clicking on the button.
     *
     * @param content The contentToOpen panel to load when clicking on the button.
     */
    public void setTargetContent(final AbstractExtendedInputContent content) {
        this.contentToOpen = content;
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Content.setContent(content);
                if (entry != null) {
                    entry.updateUpdateableInputFields();
                }
                if (projectEdit != null) {
                    projectEdit.updateUpdateableInputFields();
                }
                Sidebar.setSidebarContent(content.getSideBar());
            }
        });
    }

    @Override
    public void load(DataSetOld data) {
        // NOT NECESSARY TO IMPLEMENT HERE
    }

    @Override
    public void clear() {
        if (contentToOpen != null) {
            contentToOpen.clear();
        }
    }

    @Override
    public void save(DataSetOld data) {
        if (contentToOpen != null) {
            contentToOpen.save(data);
        }
    }

    @Override
    public boolean isValidInput() {
        if (contentToOpen != null) {
            return contentToOpen.isValidInput();
        } else {
            return false;
        }
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(button);
        comp.add(button.getButton());
        return comp;
    }

    public XButton getButton() {
        return button;
    }


    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        button = new XButton(Loc.get("OPEN"));
        if (contentToOpen != null) {
            setTargetContent(contentToOpen);
        }
        multiPanel.add(BorderLayout.CENTER, button);

        setContent(multiPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

    // ========================================================================
    public void setContentToOpen(AbstractExtendedInputContent contentToOpen) {
        this.contentToOpen = contentToOpen;
    }

    @Override
    public void actionOnFocusGain() {
        super.actionOnFocusGain();
    }

    @Override
    public void actionOnFocusLost() {
        super.actionOnFocusLost();
    }

}
