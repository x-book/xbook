package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry;

import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.UpdateableEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractFocusableEntryContent extends AbstractContent implements UpdateableEntry {

    protected AbstractInputElement currentFocus = null;

    @Override
    public void removeCurrentFocus() {
        setInputFieldFocused(null);
    }

    @Override
    public void setInputFieldFocused(IInputElement input) {
        if (currentFocus != null && !currentFocus.equals(input)) {
            currentFocus.actionOnFocusLost();
        }
        if (input instanceof AbstractInputElement) {
            currentFocus = ((AbstractInputElement) input);
            input.actionOnFocusGain();
        } else {
            currentFocus = null;
        }
    }

}
