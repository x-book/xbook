package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.table.CustomTableModel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.FilterManager;
import static de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing.AbstractFilterRow.SEPARATOR;
import java.util.ArrayList;
import javax.swing.RowFilter;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class FilterRowIsText extends AbstractFilterRow {

    public FilterRowIsText(Filter filterSceen, Listing listingScreen) {
        super(filterSceen, listingScreen);
    }

    @Override
    public FilterManager createFilterManager() {
        return new FilterManager() {
            @Override
            public boolean include(RowFilter.Entry<? extends CustomTableModel, ? extends Integer> entry) {
                int columnId = entry.getModel().findColumn((String) combo.getSelectedItem());
                if (columnId != -1) {
                    String inputRaw = text.getText().toUpperCase();
                    String[] inputTemp = inputRaw.split(SEPARATOR);
                    ArrayList<String> input = new ArrayList<>();
                    for (String s : inputTemp) {
                        input.add(s.trim());
                    }

                    // check if value contains text
                    String value = entry.getStringValue(columnId).toUpperCase();
                    for (String s : input) {
                        if (value.equals(s)) {
                            return true;
                        }
                    }
                }
                return false;
            }
        };
    }

    @Override
    protected String getDescriptionLabel() {
        return "<span style='color: #000000'><b>" + Loc.get("IS").toUpperCase() + "</b></span>";
    }

    @Override
    protected String getGhostText() {
        return Loc.get("TEXT");
    }

}
