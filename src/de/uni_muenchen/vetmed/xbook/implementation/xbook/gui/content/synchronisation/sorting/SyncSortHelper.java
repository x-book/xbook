package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.sorting;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractProjectManager;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class SyncSortHelper {
    private ColumnType columnType = AbstractProjectManager.PROJECT_PROJECTNAME;
    private ColumnType defaultType = AbstractProjectManager.PROJECT_PROJECTNAME;
    private SyncSortStates state = SyncSortStates.DEFAULT;

    public void set(ColumnType columnType) {
        if (this.columnType.equals(columnType)) {
            if (state == SyncSortStates.DEFAULT) {
                state = SyncSortStates.ASC;
            } else if (state == SyncSortStates.ASC) {
                state = SyncSortStates.DESC;
            } else if (state == SyncSortStates.DESC) {
                state = SyncSortStates.DEFAULT;
                this.columnType = defaultType;
            }
        } else {
            this.columnType = columnType;
            this.state = SyncSortStates.ASC;
        }
    }

    public void setColumnType(ColumnType columnType) {
        this.columnType = columnType;
    }

    public ColumnType getColumnType() {
        return columnType;
    }

    public void setState(SyncSortStates state) {
        this.state = state;
    }

    public SyncSortStates getState() {
        return state;
    }
}