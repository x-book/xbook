package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.IEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IPlaceholderButton;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputPlaceHolderButton extends InputButton implements IPlaceholderButton {

    /**
     * The label that is displayed on the button.
     */
    private String buttonLabel;

    /**
     * Constructor.
     *
     * @param title       The title that should be displayed as field description.
     * @param buttonLabel The label that is displayed on the button.
     */
    public InputPlaceHolderButton(String title, String buttonLabel) {
        super(new ColumnType(title, ColumnType.Type.VALUE, ColumnType.ExportType.ALL), (IEntry) null);
        setTitle(title);
        this.buttonLabel = buttonLabel;
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();

        button.setText(buttonLabel);
    }

    @Override
    public String getStringRepresentation() {
        return "n.a. - placeholder only";
    }

}
