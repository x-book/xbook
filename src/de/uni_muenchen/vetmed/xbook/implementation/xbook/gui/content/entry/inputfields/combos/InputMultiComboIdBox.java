package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos;

import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.DynamicComboBoxListId;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IMultiComboIdBox;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Allows entering multiple values that are saved as id.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputMultiComboIdBox extends AbstractInputElement implements IMultiComboIdBox {

    protected DynamicComboBoxListId<String> multiComboBox;
    private CodeTableHashMap allData;
    private String table;
    private boolean addEmptyEntry;

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input field.
     */
    public InputMultiComboIdBox(ColumnType columnType, String table) {
        super(columnType);

        setGridX(2);
        setGridY(3);
        this.table = table;
    }

    @Override
    public void load(DataSetOld data) {
        DataTableOld dataTable = data.getOrCreateDataTable(table);
        for (DataRow list : dataTable) {
            final String s = list.get(columnType);
            if (s != null) {
                multiComboBox.addItemToList(allData.get(s));
            }
        }
    }

    @Override
    public void clear() {
        multiComboBox.clearInput();
        removeAllSelectedItems();
    }

    @Override
    public void save(DataSetOld data) {
        DataTableOld list = data.getOrCreateDataTable(table);
        for (String s : getSelectedData()) {
            if (s != null) {
                DataRow row = new DataRow(table);
                row.add(new DataColumn(s, columnType));
                list.add(row);
            }
        }
    }

    public void reloadInputField() {
        multiComboBox.updateData(true);
    }

    public CodeTableHashMap getData() {
        try {
            CodeTableHashMap data = new CodeTableHashMap();
            if (addEmptyEntry) {
                data.put("", "");
            }
            data.putAll(apiControllerAccess.getHashedCodeTableEntries(columnType));
            return data;
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(InputComboIdBox.class.getName()).log(Level.SEVERE, null, ex);
            return new CodeTableHashMap();
        }
    }

    @Override
    public String getStringRepresentation() {
        String s = "";
        for (Object items : multiComboBox.getListItems()) {
            s += items + ", ";
        }
        if (!s.isEmpty()) {
            s = s.substring(0, s.length() - ", ".length());
        }
        return s;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.addAll(multiComboBox.getFocusableComponents());
        comp.add(multiComboBox.getComboBox());
        comp.add(multiComboBox.getComboBox().getEditor().getEditorComponent());
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !getSelectedData().isEmpty();
    }

    @Override
    public void actionOnFocusLost() {
        multiComboBox.addInputToList();
        super.actionOnFocusLost();
    }

    @Override
    protected void createFieldInput() {
        allData = new CodeTableHashMap();
        for (Entry<String, String> db : getData().entrySet()) {
            allData.put(db.getKey(), db.getValue());
        }

        multiComboBox = new DynamicComboBoxListId<String>(allData, columnType.getDisplayName()) {
            @Override
            public String getEmptyItem() {
                return "";
            }

            @Override
            public Comparator<String> getComparator() {
                return new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return o1.toUpperCase().compareTo(o2.toUpperCase());
                    }
                };
            }

            @Override
            protected CodeTableHashMap reloadDatabaseValues() {
                return getData();
            }
        };
        setContent(multiComboBox);

        multiComboBox.setSelectAllAndRemoveAllButtonVisible(false);
        multiComboBox.getComboBox().setPreferredSize(new Dimension(1, Sizes.INPUT_FIELD_HEIGHT));
        multiComboBox.getComboBox().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        multiComboBox.getListScroller().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        if (multiComboBox.getListScroller().getMouseWheelListeners().length > 0) {
            multiComboBox.getListScroller().removeMouseWheelListener(multiComboBox.getListScroller().getMouseWheelListeners()[0]);
        }
        multiComboBox.getList().setBackground(Color.WHITE);
    }

    @Override
    public void addToSelection(String id) {
        multiComboBox.addItemToList(allData.get(id));
    }

    @Override
    public void addToSelection(List<String> ids) {
        for (String s : ids) {
            multiComboBox.addItemToList(allData.get(s));
        }
    }

    @Override
    public void removeAllSelectedItems() {
        multiComboBox.removeAllItemsFromList();
    }

    @Override
    public void setSelectedData(String id) {
        multiComboBox.addItemToList(allData.get(id));
    }

    @Override
    public void setSelectedData(List<String> ids) {
        for (String id : ids) {
            setSelectedData(id);
        }
    }

    @Override
    public List<String> getSelectedData() {
        return allData.convertStringToId(multiComboBox.getListItems());
    }

    @Override
    public void setMode(Mode mode) {
        super.setMode(mode);
        if (mode == Mode.STACK) {
            setPreferredSize(new Dimension(Sizes.STACK_DEFAULT_WIDTH, 130));
        }
    }

    @Override
    public void setAddEmptyEntry(boolean addEmptyEntry) {
        this.addEmptyEntry = addEmptyEntry;
    }
}
