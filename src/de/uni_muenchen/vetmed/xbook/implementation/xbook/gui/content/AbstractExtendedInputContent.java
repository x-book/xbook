package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.IEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectEdit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractExtendedInputContent extends AbstractContent {

    protected IEntry entry;
    protected AbstractProjectEdit projectEdit;

    public AbstractExtendedInputContent(IEntry entry) {
        super();
        this.entry = entry;
    }

    public AbstractExtendedInputContent(IEntry entry, boolean isScrollable) {
        super(isScrollable);
        this.entry = entry;
    }

    public AbstractExtendedInputContent(AbstractProjectEdit projectEdit) {
        super();
        this.projectEdit = projectEdit;
    }

    public AbstractExtendedInputContent(AbstractProjectEdit projectEdit, boolean isScrollable) {
        super(isScrollable);
        this.projectEdit = projectEdit;
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel buttonPanel = new ButtonPanel();
        buttonPanel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_CONFIRM, Loc.get(
                "APPLY_AND_BACK"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (checkContent()) {
                    if (entry != null) {
                        Content.setContent((AbstractContent) entry);
                    } else if (projectEdit != null) {
                        Content.setContent(projectEdit);
                    }
                    Sidebar.clearSidebarContent();
                    actionWhenClickingBackButton();
                    if (entry != null) {
                        entry.updateUpdateableInputFields();
                    } else if (projectEdit != null) {
                        projectEdit.updateUpdateableInputFields();
                    }
                }
            }
        });
        return buttonPanel;
    }

    @Override
    public abstract SidebarPanel getSideBar();

    @Override
    public boolean forceSidebar() {
        return false;
    }

    public void actionWhenClickingBackButton() {
    }

    public boolean checkContent() {
        return true;
    }

    public abstract void clear();

    public abstract void save(DataSetOld data);

    public abstract boolean isValidInput();

    public abstract String getStringRepresentation();
}
