package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawFileNameChooser;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.IFileNameChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractActiveableInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.ArrayList;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputFileNameChooserActivateable extends AbstractActiveableInputElement implements IFileNameChooser {

    /**
     * The text field object.
     */
    protected RawFileNameChooser zField;
    protected ArrayList<FileNameExtensionFilter> fileFilter;

    public InputFileNameChooserActivateable(ColumnType columnTypeBoolean, ColumnType columnType, ArrayList<FileNameExtensionFilter> fileFilter) {
        super(columnTypeBoolean, columnType);
        this.fileFilter = fileFilter;
    }

    @Override
    public String getText() {
        return zField.getText();
    }

    public JTextField getTextField() {
        return zField.getTextField();
    }

    @Override
    public AbstractInputElementFunctions createActivableInputField() {
        if (zField == null) {
            zField = new RawFileNameChooser(columnType, fileFilter);
        }
        return zField;
    }

    @Override
    public void setText(String text) {
        zField.setText(text);
    }

}
