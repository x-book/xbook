package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.combosearch;

/*
package de.uni_muenchen.vetmed.xbook.xbook.gui.components.content.entry.inputFields.combos.combosearch;
/**
 *
 * @author ciaran.harrington@extern.lrz-muenchen.de
 */
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JComboBox;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

public class AutocompleteJComboBox extends JComboBox {

    private final Searchable searchable;

    public AutocompleteJComboBox(Searchable s) {
        super();
        this.searchable = s;
        setEditable(true);
        Component c = getEditor().getEditorComponent();
        if (c instanceof JTextComponent) {
            final JTextComponent tc = (JTextComponent) c;
            tc.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void changedUpdate(DocumentEvent arg0) {
                }
                @Override
                public void insertUpdate(DocumentEvent arg0) {
                    update();
                }
                @Override
                public void removeUpdate(DocumentEvent arg0) {
                    update();
                }
                public void update() {
                    
                    List<String> founds = new ArrayList(searchable.search(tc.getText()));
                    Set foundSet = new HashSet();
                    for (String s : founds) {
                        foundSet.add(s.toLowerCase());
                    }
                    Collections.sort(founds);//sort alphabetically
                    setEditable(false);
                    removeAllItems();
                    //if founds contains the search text, then only add once.
                    if (!foundSet.contains(tc.getText().toLowerCase())) {
                        addItem(tc.getText());
                    }
                    for (String s : founds) {
                        addItem(s);
                    }
                    setEditable(true);
                    setPopupVisible(true);
                }
            });
            //When the text component changes, focus is gained 
            //and the menu disappears. To account for this, whenever the focus
            //is gained by the JTextComponent and it has searchable values, we show the popup.
            tc.addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent arg0) {
                    if (tc.getText().length() > 0) {
                        setPopupVisible(true);
                    }
                }
                @Override
                public void focusLost(FocusEvent arg0) {
                }
            });
        } else {
            throw new IllegalStateException("Editing component is not a JTextComponent!");
        }
    }
}

//