package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.StatusLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.DynamicGridLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XCollapsibleTitle;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XSubTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarCustomEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarMultiEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import de.uni_muenchen.vetmed.xbook.api.helper.InputMaskModes;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.InterfaceUpdateableInputField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers.InputIntegerSpinnerWithAutoRaise;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractEntryRoot extends AbstractFocusableEntryContent {

    private static final Log logger = LogFactory.getLog(AbstractEntryRoot.class);
    private static final long serialVersionUID = 1L;

    protected GeneralInputMaskMode generalMode;
    protected InputMaskModes mode;
    protected JLabel entryCountLabel;

    /**
     * An arraylist that holds all input elements.
     */
    protected ArrayList<AbstractInputElement> inputElements;
    /**
     * An arraylist that holds all updateable input elements.
     */
    private ArrayList<InterfaceUpdateableInputField> updateableInputFields;
    /**
     * The loaded data that is set to the input fields (on edit or read MODE only).
     */
    protected EntryDataSet loadedData;
    protected DataSetOld loadedDataMulti;
    protected ArrayList<Key> loadedKeys;

    /**
     * The input field that currently holds the focus.
     */
    /**
     * A wrapper panel that wraps everything (you don't say!)
     */
    protected JPanel wrapper;
    private final XSubTitle subTitle = new XSubTitle("");
    protected ArrayList<JPanel> allSections = new ArrayList<>();
    protected ArrayList<ArrayList<AbstractInputElement>> sections;
    private SidebarCustomEntry sidebar;

    /**
     * Constructor.
     *
     * @param m
     * @param doInit
     */
    public AbstractEntryRoot(GeneralInputMaskMode m, boolean doInit) {
        this(m, doInit, null);
    }

    /**
     * Constructor.
     *
     * @param m
     * @param doInit
     * @param data
     */
    public AbstractEntryRoot(GeneralInputMaskMode m, boolean doInit, EntryDataSet data) {
        super();
        this.generalMode = m;

        // setting the mode is necessary for initialisation because later
        // the modes for single and multi edit cannot be changed anymore
        if (m == GeneralInputMaskMode.SINGLE) {
            mode = InputMaskModes.NEW_ENTRY;
        } else /* if (m == GeneralInputMaskMode.MULTI_EDIT) */ {
            mode = InputMaskModes.MULTI_EDIT;
        }
        this.loadedData = data;
        initialise(doInit);
    }

    protected void initialise(boolean doInit) {
        inputElements = new ArrayList<>();
        updateableInputFields = new ArrayList<>();
        sections = new ArrayList<>();
        if (doInit) {
            init();
            updateInputFieldVisibility();
        }
    }

    @Override
    protected JPanel getContent() {
        Footer.startWorking();
        if (wrapper == null) {
            wrapper = new JPanel(getInputElementsWrapperLayout());
            wrapper.setBackground(Colors.CONTENT_BACKGROUND);

            subTitle.setOpaque(false);
            wrapper.add(subTitle);
            allSections = new ArrayList<>();
            inputElements = new ArrayList<>();
            composeSections();
            setEntryObject();
            for (AbstractInputElement inputField : getInputElements()) {
                // set input fields to multi edit layout, if necessary
                if (generalMode == GeneralInputMaskMode.MULTI_EDIT) {
                    inputField.setToMultiEditStyle();
                }

                // check if input field is an updateable input field
                if (inputField instanceof InterfaceUpdateableInputField) {
                    setFieldUpdatedateable((InterfaceUpdateableInputField) inputField);
                }
            }
            addFocusFunction();
        }
        Footer.stopWorking();
        return wrapper;
    }

    protected LayoutManager getInputElementsWrapperLayout() {
        return new StackLayout();
    }

    /**
     * Resets the input mask to "new entry" mode
     */
    public void toNewEntryMode(boolean ignoreRememberValues) throws SwitchToNewModeImpossibleException {
        if (mode == InputMaskModes.MULTI_EDIT) {
            throw new SwitchToNewModeImpossibleException(mode, InputMaskModes.NEW_ENTRY);
        }
        mode = InputMaskModes.NEW_ENTRY;

        // clear all input fields
        clearFields(ignoreRememberValues);
        reloadFields();
        setFocusToFirstInputElement();
        try {
            mainFrame.getController().unloadEntry(getManager());
        } catch (NotLoggedInException e) {
            e.printStackTrace();
        }

        unsetEntryData();
        //loadedData = null; /// ???? correct?
        //updateInputFieldVisibility(); // ??? correct?

        // set the correct buttons visible
        subTitle.setText(Loc.get("NEW_DATASET"));
        try {
            setToHasWritingRightsMode(canEditEntry());
        } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Resets the input mask to "update entry" mode
     */
    protected void toUpdateEntryMode() throws SwitchToNewModeImpossibleException {
        if (mode == InputMaskModes.MULTI_EDIT) {
            throw new SwitchToNewModeImpossibleException(mode, InputMaskModes.EDIT_ENTRY);
        }
        System.out.println("TO UPDATE MODE: AbstractEntry");
        mode = InputMaskModes.EDIT_ENTRY;

        reloadFields();
        subTitle.setText(Loc.get("EDIT_DATASET"));
        try {
            setToHasWritingRightsMode(canEditEntry());
        } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the name of the table of the current entry mask.
     *
     * @return The table name.
     */
    public abstract String getTableName();

    /**
     * Checks for multi edit mode if any input field has dependencies and its value has been updated.
     * <p>
     * If yes, displays a JOptionPane to inform the user and to take action.
     *
     * @return <code>true</code> if any input field has dependencies and an
     * updated value.
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException
     */
    public boolean areMultiEditDependenciesValid() throws IsAMandatoryFieldException {
        for (AbstractInputElement input : getInputElements()) {
            if (!input.areMultiEditDependenciesValid()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Iterates all input fields and return an EntryData object that hold all data.
     * <p>
     * Method considers the current mode. If single edit mode is active, all values are returned. If multi edit mode is
     * active, only the values of the input fields are returned that have been changed.
     *
     * @return An EntryData object that hold all data
     * @throws IsAMandatoryFieldException When the input field is a mandatory field but no valid input was done.
     * @throws NotLoggedInException
     */
    public EntryDataSet getCurrentValues() throws IsAMandatoryFieldException, NotLoggedInException {
        Key key;
        if (loadedData == null) {
            key = null;
        } else {
            key = loadedData.getEntryKey();
        }
        EntryDataSet entry = new EntryDataSet(key, null, mainFrame.getController().getDbName(), getTableName());
        for (AbstractInputElement input : getInputElements()) {
            // only check the values for mandatory fields
            // - if single mod is active (check everything) or
            // - if multi edit mod is active (only check the changed items)
            if (generalMode == GeneralInputMaskMode.SINGLE
                    || (isMultiEditMode()
                    && (input.getStatus() == StatusLabel.Status.NEW_VALUE_ENTERED || input.getStatus() == StatusLabel.Status.VALUE_DELETED))) {
                if (input.isMandatory() && !input.isValidInput()) {
                    input.setErrorStyle();
                    throw new IsAMandatoryFieldException(input.getColumnType());
                }

                if (input.isVisible() || loadedData == null) {
                    input.save(entry);
                }
            }
        }
        return entry;
    }

    protected void printCurrentValues() {
        for (AbstractInputElement input : getInputElements()) {
            logger.debug(input.toString());
        }
    }

    protected ArrayList<AbstractInputElement> getInputElements() {
        return inputElements;
    }

    /**
     * Sets all buttons of the panel to active or deactive.
     *
     * @param status
     */
    public abstract void setButtonsEnabled(boolean status);

    /**
     * Add a new entry to the database.
     *
     * @param displayNewEntryAfterSaving
     * @return
     */
    public EntryDataSet saveEntry(boolean displayNewEntryAfterSaving) {
        removeCurrentFocus();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                setButtonsEnabled(false);
            }
        });
        if (!checkBeforeSaving()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    setButtonsEnabled(true);
                }
            });
            return null;
        }

        Footer.startWorking();

        EntryDataSet data = null;
        try {
            data = getCurrentValues();
            mainFrame.getController().saveEntry(data);
            if (displayNewEntryAfterSaving) {
                if (loadedData == null) {
                    Footer.displayConfirmation(Loc.get("ENTRY_SUCCESSFUL_ADDED"));
                } else {
                    Footer.displayConfirmation(Loc.get("ENTRY_SUCCESSFUL_EDITED"));
                }
                toNewEntryMode(false);
            } else {
                if (loadedData == null) {
                    Footer.displayConfirmation(Loc.get("ENTRY_SUCCESSFUL_ADDED") + " " + Loc.get("ENTRY_REMAINS_OPEN"));
                } else {
                    Footer.displayConfirmation(Loc.get("ENTRY_SUCCESSFUL_EDITED") + " " + Loc.get("ENTRY_REMAINS_OPEN"
                    ));
                }
                mainFrame.getController().loadAndDisplayEntry(getManager(), data.getEntryKey(), true);

                /* moved from a) to here because otherwise loadedData is always
                 reset even in new entry mode and the previous entered entry
                 is overridden. */
                if (data.getEntryKey() != null) {
                    loadedData = data;
                }
            }
            actionOnSave(displayNewEntryAfterSaving);
            /* a) */

        } catch (EntriesException | NotLoadedException | StatementNotExecutedException | NotLoggedInException ex) {
            Logger.getLogger(AbstractEntry.class.getName()).log(Level.SEVERE, null, ex);
            Footer.displayError(Loc.get("ERROR_WHILE_SAVING_THE_ENTRY"));
        } catch (NoRightException ex) {
            Footer.displayError(Loc.get("ERROR_MESSAGE>NO_WRITE_RIGHTS"));
        } catch (IsAMandatoryFieldException e) {
            System.out.println(Loc.get("FIELD_IS_A_MANDATORY_FIELD", e.getColumnType().getDisplayName()));
            Footer.displayWarning(Loc.get("FIELD_IS_A_MANDATORY_FIELD", e.getColumnType().getDisplayName()));
        } catch (SwitchToNewModeImpossibleException ex) {
            Logger.getLogger(AbstractEntryRoot.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    setButtonsEnabled(true);
                }
            });
            Footer.stopWorking();
        }

        return data;
    }

    public boolean saveMultiEntry() {
        removeCurrentFocus();

        try {
            // check for input field dependencies. If yes, interrupt saving process
            if (!areMultiEditDependenciesValid()) {
                Footer.displayWarning(Loc.get("ERROR_MESSAGE>ENTRY_NOT_SAVED"));
                return false;
            }

            // try to save
            Footer.startWorking();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    setButtonsEnabled(false);
                }
            });
            if (!checkBeforeSaving()) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        setButtonsEnabled(true);
                    }
                });
                return false;
            }

            EntryDataSet data = null;
            data = getCurrentValues(); // TODO?
            mainFrame.getController().saveEntries(data, loadedKeys);

            return true;
        } catch (NotLoggedInException | NoRightException | NotLoadedException | StatementNotExecutedException e) {
            Logger.getLogger(AbstractEntry.class.getName()).log(Level.SEVERE, null, e);
            Footer.displayError(Loc.get("ERROR_WHILE_SAVING_THE_ENTRY"));
        } catch (IsAMandatoryFieldException e) {
            System.out.println(Loc.get("FIELD_IS_A_MANDATORY_FIELD", e.getColumnType().getDisplayName()));
            Footer.displayWarning(Loc.get("FIELD_IS_A_MANDATORY_FIELD", e.getColumnType().getDisplayName()));
        } finally {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    setButtonsEnabled(true);
                }
            });
            Footer.stopWorking();
        }
        return false;
    }

    public void loadEntry() throws WrongModeException {
        Footer.startWorking();
        if (generalMode != GeneralInputMaskMode.SINGLE) {
            throw new WrongModeException(GeneralInputMaskMode.SINGLE, generalMode);
        }
        ArrayList<AbstractInputElement> loaded = new ArrayList<>();
        if (loadedData != null) {
            for (AbstractInputElement input : getInputElements()) {
                if (!loaded.contains(input)) {
                    loadInputElement(loaded, input);
                }
            }
        }
        updateUpdateableInputFields();
        Footer.stopWorking();
    }

    protected void loadInputElement(ArrayList<AbstractInputElement> loaded, AbstractInputElement input) {

        try {
            input.clear();
            // update updateable inputfield itself before loading data
            // to avoid existing loaded data is not set correctly
            if (input instanceof InterfaceUpdateableInputField) {
                InterfaceUpdateableInputField uInput = (InterfaceUpdateableInputField) input;
                for (AbstractInputElement ie : uInput.getDependentOnFields()) {
                    if (loaded.contains(ie)) {
                        continue;
                    }
                    loadInputElement(loaded, ie);
                }
                uInput.updateInputFieldSingle();
            }
            input.load(loadedData);
            loaded.add(input);
        } catch (NullPointerException npe) {
            input.setErrorStyle();
            Logger.getLogger(AbstractEntry.class.getName()).log(Level.SEVERE, null, npe);
        }

    }

    public void loadMultiEditEntries(DataSetOld dataToLoad) throws WrongModeException {
        if (generalMode != GeneralInputMaskMode.MULTI_EDIT) {
            throw new WrongModeException(GeneralInputMaskMode.MULTI_EDIT, generalMode);
        }

        Footer.startWorking();
        for (AbstractInputElement input : getInputElements()) {
            try {
                input.clearMultiEdit();
                input.loadMultiEdit(dataToLoad);
                this.loadedDataMulti = dataToLoad;
            } catch (NullPointerException npe) {
                input.setErrorStyle();
                Logger.getLogger(AbstractEntry.class.getName()).log(Level.SEVERE, null, npe);
            }
        }
        updateUpdateableInputFields();
        Footer.stopWorking();
    }

    @Override
    public void revertMultiEditInputField(AbstractInputElement fieldToReload) throws WrongModeException {
        if (isMultiEditMode()) {
            throw new WrongModeException(GeneralInputMaskMode.MULTI_EDIT, generalMode);
        }

        Footer.startWorking();
        try {
            fieldToReload.clearMultiEdit();
            fieldToReload.loadMultiEdit(loadedDataMulti);
        } catch (NullPointerException npe) {
            fieldToReload.setErrorStyle();
            Logger.getLogger(AbstractEntry.class.getName()).log(Level.SEVERE, null, npe);
        }
        Footer.stopWorking();
    }

    public void setEntryData(EntryDataSet data, boolean isEditMode) {
        loadedData = data;
        try {
            loadEntry();
        } catch (WrongModeException ex) {
            Logger.getLogger(AbstractEntryRoot.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            toUpdateEntryMode();
        } catch (SwitchToNewModeImpossibleException ex) {
            Logger.getLogger(AbstractEntryRoot.class.getName()).log(Level.SEVERE, null, ex);
        }
        updateInputFieldVisibility();
    }

    public void unsetEntryData() {
        loadedData = null;
        updateInputFieldVisibility();
    }

    protected void resetFields() throws WrongModeException {
        if (generalMode == GeneralInputMaskMode.SINGLE) {
            EntryDataSet dataBak = loadedData;
            clearFields();
            loadedData = dataBak;
            loadEntry();
        } else {
            loadMultiEditEntries(loadedDataMulti);
        }
    }

    /**
     * Clears all input fields. Does not clear the fields, that are selected as "remember value".
     */
    public void clearFields() {
        clearFields(false);
    }

    /**
     * Clears all input fields.
     *
     * @param ignoreRememberValues <code>false</code> to also clear the fields,
     *                             that are selected as "remember value".
     */
    public void clearFields(boolean ignoreRememberValues) {
        loadedData = null;
        for (AbstractInputElement element : getInputElements()) {
            if (!(element instanceof InputIntegerSpinnerWithAutoRaise)) {
                if (ignoreRememberValues || !element.isRememberValueSelected()) {
                    element.clear();
                }
            }
        }
        updateUpdateableInputFields();
    }

    public void reloadFields() {
        for (AbstractInputElement element : getInputElements()) {
            element.reload();
        }
        try {
            setToHasWritingRightsMode(canEditEntry());
        } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException e) {
            setToHasWritingRightsMode(false);
        }
    }

    protected boolean canEditEntry() throws NotLoggedInException, NotLoadedException, StatementNotExecutedException {
        return mainFrame.getController().canEditEntry();
    }

    public void actionOnSave(boolean displayNewEntryAfterSaving) {
        for (AbstractInputElement element : getInputElements()) {
            element.actionOnSave(displayNewEntryAfterSaving);
        }
    }

    @Override
    public SidebarPanel getSideBar() {
        if (sidebar == null) {
            if (generalMode == GeneralInputMaskMode.MULTI_EDIT) {
                sidebar = new SidebarMultiEntry();
            } else {
                sidebar = new SidebarEntry();
            }
        }
        return sidebar;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    public GeneralInputMaskMode getGeneralMode() {
        return generalMode;
    }

    public boolean isMultiEditMode() {
        return getGeneralMode() == GeneralInputMaskMode.MULTI_EDIT;
    }

    @Override
    public void actionOnDisconnect() {
    }

    // ////////////////////////////////////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////
    // Helper methods for several issues
    // ////////////////////////////////////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////

    /**
     * Method that is called when compose the sections and their input fields.
     * <p>
     * Please use the addSection method to compose them.
     */
    public abstract void composeSections();

    /**
     * Adds a new section with a collapsible title bar and the several input fields.
     *
     * @param sectionTitle         The title of the collapsible title bar.
     * @param inputElements        The input fields.
     * @param enableCollapsibility <code>true</code> to set the elements
     *                             visible, <code>false</code> else.
     * @param setGridHeightToNull
     * @return
     */
    protected CustomCollapsibleTitle addSection(String sectionTitle,
                                                ArrayList<AbstractInputElement> inputElements,
                                                boolean enableCollapsibility, boolean setGridHeightToNull) {
        return addSection(sectionTitle, inputElements, enableCollapsibility, setGridHeightToNull, true);
    }

    /**
     * Adds a new section with a collapsible title bar and the several input fields.
     *
     * @param sectionTitle         The title of the collapsible title bar.
     * @param inputElements        The input fields.
     * @param enableCollapsibility <code>true</code> to set the elements
     *                             visible, <code>false</code> else.
     * @return
     */
    protected CustomCollapsibleTitle addSection(String sectionTitle,
                                                ArrayList<AbstractInputElement> inputElements,
                                                boolean enableCollapsibility) {
        return addSection(sectionTitle, inputElements, enableCollapsibility, false);
    }

    /**
     * Adds a new section with a collapsible title bar and the several input fields.
     *
     * @param sectionTitle  The title of the collapsible title bar.
     * @param inputElements The input fields.
     * @return
     */
    protected CustomCollapsibleTitle addSection(String sectionTitle,
                                                ArrayList<AbstractInputElement> inputElements) {
        return addSection(sectionTitle, inputElements, true, false, true);
    }

    /**
     * Adds a new section with a collapsible title bar and the several input fields.
     *
     * @param sectionTitle         The title of the collapsible title bar.
     * @param inputElements        The input fields.
     * @param enableCollapsibility <code>true</code> to set the elements
     *                             visible, <code>false</code> else.
     * @param displayShowAll       Display or hide the show all button.
     * @return
     */
    protected CustomCollapsibleTitle addSection(String sectionTitle,
                                                ArrayList<AbstractInputElement> inputElements,
                                                boolean enableCollapsibility, boolean setGridHeightToNull,
                                                boolean displayShowAll) {
        sections.add(inputElements);
        // set the input fields
        JPanel panel = initializeDynamicGridPanel(setGridHeightToNull);

        if (inputElements != null) {
            for (AbstractInputElement element : inputElements) {
                addInputElementToPanel(element, panel);
            }
        }

        CustomCollapsibleTitle title = initializeCollapsibleTitle(sectionTitle, inputElements, enableCollapsibility,
                displayShowAll, panel);
        return title;
    }

    protected CustomCollapsibleTitle initializeCollapsibleTitle(String sectionTitle,
                                                                ArrayList<AbstractInputElement> inputElements,
                                                                boolean enableCollapsibility, boolean displayShowAll,
                                                                JPanel panel) {

        // set the collapsible title
        CustomCollapsibleTitle title = new CustomCollapsibleTitle(sectionTitle, inputElements, displayShowAll);
        title.setElementToHide(panel);
        title.enableCollapsibility(enableCollapsibility);

        // build the elements together
        wrapper.add(title);
        wrapper.add(panel);

        allSections.add(panel);
        return title;
    }

    protected void addInputElementToPanel(AbstractInputElement element, JPanel panel) {
        if (!element.isCreated()) {
            element.create();
        }
        panel.add(element);
        getInputElements().add(element);
    }

    protected JPanel initializeDynamicGridPanel(boolean setGridHeightToNull) {

        DynamicGridLayout dynamicGridLayout;
        if (setGridHeightToNull) {
            dynamicGridLayout = new DynamicGridLayout(Sizes.INPUT_GRID_SIZE.width, 0);
        } else {
            switch (generalMode) {
                case SINGLE:
                    dynamicGridLayout = new DynamicGridLayout(Sizes.INPUT_GRID_SIZE.width,
                            Sizes.INPUT_GRID_SIZE.height);
                    break;
                case MULTI_EDIT:
                default:
                    dynamicGridLayout = new DynamicGridLayout(Sizes.INPUT_GRID_SIZE.width,
                            Sizes.INPUT_GRID_SIZE.height + 28);
                    break;
            }
        }

        JPanel panel = new JPanel();
        panel.setBackground(Colors.CONTENT_BACKGROUND);
        panel.setOpaque(true);
        panel.setLayout(dynamicGridLayout);
        return panel;
    }

    /**
     * Adds a new section with a collapsible title bar, labels and the several input fields.
     *
     * @param sectionTitle         The title of the collapsible title bar.
     * @param components           The Components: Labels and input fields.
     * @param enableCollapsibility
     * @param setGridHeightToNull
     * @return section panel with the title
     */
    protected CustomCollapsibleTitle addSectionWithLabels(String sectionTitle,
                                                          ArrayList<Component> components,
                                                          boolean enableCollapsibility, boolean setGridHeightToNull) {

        JPanel panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);
        panel.setOpaque(true);

        // set the input fields
        JPanel dynamicGridPanel = initializeDynamicGridPanel(setGridHeightToNull);

        int count = 0;

        ArrayList<AbstractInputElement> inputElements = new ArrayList<>();
        if (components != null) {
            for (Component component : components) {

                if (component instanceof JLabel) {
                    JPanel labelsPanel = new JPanel();
                    labelsPanel.setLayout(new BorderLayout());
                    labelsPanel.setOpaque(true);
                    labelsPanel.setBackground(Colors.CONTENT_BACKGROUND);
                    component.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 14));
                    ((JLabel) component).setHorizontalAlignment(JLabel.LEFT);
                    labelsPanel.add(component);

                    if (count != 0) {
                        panel.add(dynamicGridPanel);
                        dynamicGridPanel = initializeDynamicGridPanel(setGridHeightToNull);
                        count = 0;
                    }
                    panel.add(labelsPanel);
                }

                if (component instanceof AbstractInputElement) {
                    addInputElementToPanel((AbstractInputElement) component, dynamicGridPanel);
                    inputElements.add((AbstractInputElement) component);
                    count++;
                }
            }
        }

        // set the collapsible title
        CustomCollapsibleTitle title = initializeCollapsibleTitle(sectionTitle, inputElements, enableCollapsibility,
                true, panel);

        return title;
    }

    /**
     * Adds a new section with a collapsible title bar and the several input fields.
     *
     * @param sectionTitle  The title of the collapsible title bar.
     * @param inputElements The input fields.
     * @return section panel with the title
     */
    protected CustomCollapsibleTitle addTablePanelSection(String sectionTitle,
                                                          ArrayList<AbstractInputElement> inputElements) {

        // set the input fields
        JPanel panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);
        panel.setOpaque(true);

        if (inputElements != null) {
            for (AbstractInputElement element : inputElements) {

                addInputElementToPanel(element, panel);

                JLabel label = new JLabel("vgap");
                label.setForeground(Colors.CONTENT_BACKGROUND);
                label.setOpaque(false);
                panel.add(label);

            }
        }
        // set the collapsible title
        CustomCollapsibleTitle title = initializeCollapsibleTitle(sectionTitle, inputElements, true, true, panel);

        return title;
    }

    /**
     * Set the entry object to all input field objects.
     */
    protected void setEntryObject() {
        for (ArrayList<AbstractInputElement> section : sections) {
            for (AbstractInputElement abstractInputElement : section) {
                abstractInputElement.setEntry(this);
            }
        }
    }

    /**
     * Adds the focus function to all input fields.
     */
    private void addFocusFunction() {
        for (AbstractInputElement input : getInputElements()) {
            for (Component comp : input.getMyFocusableComponents()) {
                comp.addFocusListener(input.getFocusFunction());
            }
        }
    }

    @Override
    public void updateInputFieldVisibility() {
        try {
            if (mainFrame.getController().isProjectLoaded()
                    && mainFrame.getController().getCurrentProject().getProjectKey() != null) {
                for (AbstractInputElement input : getInputElements()) {
                    if (input.getColumnType() == null) {
                        continue;
                    }
                    input.setVisible(AbstractConfiguration.getInputFieldProperty(
                            mainFrame.getController().getPreferences(),
                            mainFrame.getController().getCurrentProject().getProjectKey(),
                            input.getColumnType().getColumnName()));
                }
            }
        } catch (NotLoadedException e) {
        }
    }

    @Override
    public void setFocus() {
        if (currentFocus == null) {
            // if there is no current element focused
            setFocusToFirstInputElement();
        } else {
            // set the current element focused
            currentFocus.setFocus();
        }
    }

    /**
     * Set the focus to the first input element.
     */
    public void setFocusToFirstInputElement() {
        ArrayList<AbstractInputElement> inputElements = getInputElements();
        if (!inputElements.isEmpty()) {
            currentFocus = inputElements.get(0);
            currentFocus.setFocus();
        } else {
            currentFocus = null;
        }
        // move the scroll pane to top
        scroll.getVerticalScrollBar().setValue(0);
    }

    /**
     * Set an inputfield as updateable, so the input field will be updated if the method updateUpdateableInputFields()
     * is called.
     *
     * @param input The updateable input field.
     * @return The updateable input field.
     */
    protected InterfaceUpdateableInputField setFieldUpdatedateable(InterfaceUpdateableInputField input) {
        updateableInputFields.add(input);
        return input;
    }

    @Override
    public void updateUpdateableInputFields() {
        if (generalMode == GeneralInputMaskMode.SINGLE) {
            updateUpdateableInputFieldsSingle();
        }
        if (generalMode == GeneralInputMaskMode.MULTI_EDIT) {
            updateUpdateableInputFieldsMultiEdit();
        }
    }

    @Override
    public void updateUpdateableInputFieldsSingle() {
        for (InterfaceUpdateableInputField updateable : updateableInputFields) {
            updateable.updateInputFieldSingle();
        }
    }

    @Override
    public void updateUpdateableInputFieldsMultiEdit() {
        for (InterfaceUpdateableInputField updateable : updateableInputFields) {
            updateable.updateInputFieldMultiEdit();
        }
    }

    /**
     * Set the next input element focused.
     */
    public void setNextElementFocused() {
        if (currentFocus == null) {
            currentFocus = getInputElements().get(0);
        } else {
            currentFocus =
                    getInputElements().get((getInputElements().indexOf(currentFocus) + 1) % getInputElements().size());
        }
        currentFocus.requestFocus();
    }

    @Override
    public void updateContent() {
        super.updateContent();
        reloadFields();
    }

    public String getInputOfField(ColumnType type) {
        for (AbstractInputElement input : getInputElements()) {
            if (input.getColumnType().equals(type)) {
                return input.toString();
            }
        }
        return "";
    }

    /**
     * Returns the manager for this entry
     *
     * @return
     * @throws NotLoggedInException
     */
    protected AbstractBaseEntryManager getManager() throws NotLoggedInException {
        return ((AbstractController) mainFrame.getController()).getLocalManager().getAbstractInputUnitManager();
    }

    /**
     * Checks all input fields if all mandatory fields have valid values. Non-mandatory fields won't influence the
     * result.
     *
     * @return The value if all mandatory fields have valid values.
     */
    public boolean isValidInput() {
        for (AbstractInputElement input : getInputElements()) {
            if (input.isMandatory() && !input.isValidInput()) {
                return false;
            }
        }
        return true;
    }

    //    public void setProxy() {
////        ArrayList<AbstractInputElement> elements = getInputElements();
////        for (int i = elements.size() - 1; i >= 0; i--) {
////            if (elements.get(i) instanceof AbstractInputCrossLinkedEntryButton) {
////                elements.remove(i);
////            }
////        }
////        for (JPanel section : allSections) {
////            Component[] comp = section.getComponents();
////            for (int i = comp.length - 1; i >= 0; i--) {
////                if (comp[i] instanceof AbstractInputCrossLinkedEntryButton) {
////                    section.remove(i);
////                }
////            }
////        }
//    }
    public void setToHasWritingRightsMode(boolean b) {
        for (AbstractInputElement field : getInputElements()) {
            field.setEnabledForRights(b);
        }
    }

    /**
     * Method that is called before saving the entry. Can be used to do individual checks, e.g. checking if a similar
     * entry already exists in the database.
     * <p>
     * Returning <code>false</code> will not automatically cause displaying an error message in the footer. So please do
     * it here.
     * <p>
     * Notice that the correctness of the input of the input fields is already checked in Method isValidInput() of class
     * AbstractInputElement.
     *
     * @return <code>true</code> if saving should be continued,
     * <code>false</code> else
     */
    protected boolean checkBeforeSaving() {

        // check if any invalid value was loaded and was not updated.
        for (AbstractInputElement element : getInputElements()) {
            if (element.invalidValueLoaded()) {
                Footer.displayError(Loc.get("UNPROCESSED_INVALID_VALUES_IN_THE_INPUT_FIELDS"));
                return false;
            }
        }
        return true;

    }

    public void setNumberOfLoadedEntries(int size) {
        entryCountLabel.setText("<html><center>" + Loc.get("EDITING_OF_MULTIPLE_ENTRIES_ACTIVE") + ":<br><b>" + Loc.get("X_ENTRIES_LOADED", size) + "</b></center></html>");

    }

    public void setLoadedKeys(ArrayList<Key> loadedKeys) {
        this.loadedKeys = loadedKeys;
    }

    /* ****************************************************************************************** */

    /**
     * An inner class that extends the CustomCollapsibleTitle class with another button that allows setting all input
     * elements visible.
     */
    public class CustomCollapsibleTitle extends XCollapsibleTitle {

        /**
         * The button which allows setting all input elements visible.
         */
        protected JLabel visibleButton;

        private Color showAllFontColor;

        private ArrayList<AbstractInputElement> inputElements;

        /**
         * Constructor.
         *
         * @param title         The title that is displayed in the bar.
         * @param inputElements The input elements that are assinged to this
         *                      section.
         */
//        public CustomCollapsibleTitle(String title, ArrayList<AbstractInputElement> inputElements) {
//            this(title, inputElements, true);
//        }

        /**
         * Constructor.
         *
         * @param title          The title that is displayed in the bar.
         * @param inputElements  The input elements that are assinged to this section.
         * @param displayShowAll Display or hide the show all button.
         */
        public CustomCollapsibleTitle(String title, ArrayList<AbstractInputElement> inputElements,
                                      final boolean displayShowAll) {
            super(title);

            this.inputElements = inputElements;

            showAllFontColor = Color.LIGHT_GRAY;
            // set the icon
            visibleButton = new JLabel(Loc.get("SHOW_ALL"));
            visibleButton.setOpaque(true);
            visibleButton.setForeground(Color.LIGHT_GRAY);
            visibleButton.setBackground(BG_COLOR);
            visibleButton.setBorder(new EmptyBorder(0, 0, 0, 7));
            if (displayShowAll) {
                labelWrapper.add(BorderLayout.EAST, visibleButton);
            }
            visibleButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    for (AbstractInputElement element : CustomCollapsibleTitle.this.inputElements) {
                        element.setVisibleAndSaveToProperties(true);
                    }
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    visibleButton.setForeground(Color.WHITE);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    visibleButton.setForeground(showAllFontColor);
                }
            });

        }

        @Override
        protected void reset() {
            super.reset();
            if (visibleButton != null) {
                visibleButton.setVisible(!isCollapsed);
            }
        }

        public void addSubCollapsibleTitle(CustomCollapsibleTitle sub) {
            super.addSubCollapsibleTitle(sub);
            Color c = Colors.CONTENT_BACKGROUND;
            Color newColor = new Color(c.getRed() - 15, c.getGreen() - 15,
                    c.getBlue() - 15);
            sub.showAllFontColor = Color.BLACK;
            sub.visibleButton.setForeground(sub.showAllFontColor);
            sub.visibleButton.setBackground(newColor);
        }
    }

    /**
     * Returns the mode of the input mask, e.g. whether it is editable or not.
     *
     * @return The mode of the input mask.
     */
    public InputMaskModes getInputMaskMode() {
        return mode;
    }
}
