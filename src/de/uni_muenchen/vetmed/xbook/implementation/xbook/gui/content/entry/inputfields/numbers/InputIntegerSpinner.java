package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IIntegerSpinner;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.DocumentFilter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * An integer spinner as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputIntegerSpinner extends AbstractInputElement implements IIntegerSpinner {

    /**
     * The spinner object.
     */
    protected JSpinner spinner;
    /**
     * Holds the ID of the mode of this spinner.
     */
    private Modes mode = Modes.STANDARD;
    /**
     * Status if the spinner allows an empty input field
     */
    private boolean isEmptyInputAllowed;

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     */
    public InputIntegerSpinner(ColumnType columnType) {
        this(columnType, false);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     */
    public InputIntegerSpinner(ColumnType columnType, boolean isEmptyInputAllowed) {
        super(columnType);
        setIsEmptyInputAllowed(isEmptyInputAllowed);
    }

    @Deprecated
    public InputIntegerSpinner(ColumnType columnType, int gridX, int gridY) {
        this(columnType, gridX, gridY, Modes.STANDARD, false, (SidebarPanel) null);
    }

    @Deprecated
    public InputIntegerSpinner(ColumnType columnType, int gridX, int gridY, SidebarPanel sidebar) {
        this(columnType, gridX, gridY, Modes.STANDARD, false, sidebar);
    }

    @Deprecated
    public InputIntegerSpinner(ColumnType columnType, int gridX, int gridY, String sidebarText) {
        this(columnType, gridX, gridY, Modes.STANDARD, false, new SidebarEntryField(columnType, sidebarText));
    }

    @Deprecated
    public InputIntegerSpinner(ColumnType columnType, int gridX, int gridY, Modes mode, boolean isEmptyInputAllowed) {
        this(columnType, gridX, gridY, mode, isEmptyInputAllowed, (SidebarPanel) null);
    }

    @Deprecated
    public InputIntegerSpinner(ColumnType columnType, int gridX, int gridY, Modes mode, boolean isEmptyInputAllowed, String sidebarText) {
        this(columnType, gridX, gridY, mode, isEmptyInputAllowed, new SidebarEntryField(columnType, sidebarText));
    }

    @Deprecated
    public InputIntegerSpinner(ColumnType columnType, int gridX, int gridY, Modes mode, boolean isEmptyInputAllowed, SidebarPanel sidebar) {
        super(columnType);
        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);
        setMode(mode);
        setIsEmptyInputAllowed(isEmptyInputAllowed);
    }

    /**
     * Calculates an String[] with the valid Integer values for the spinner
     * dependent on the available mode settings (greater than null, greater
     * equal null, standard) and the boolean value of isEmptyInputAllowed.
     *
     * @return The final String[] holding the valid Integer values.
     */
    private String[] fillSpinnerWithData() {
        String[] dataAkk;
        switch (mode) {
            case GREATER_THAN_NULL:
                if (isEmptyInputAllowed) {
                    dataAkk = new String[10001];
                    dataAkk[0] = "";
                    for (int n = 1; n < 10001; n++) {
                        dataAkk[n] = "" + (n);
                    }
                } else {
                    dataAkk = new String[10000];
                    for (int n = 0; n < 10000; n++) {
                        dataAkk[n] = "" + (n + 1);
                    }
                }
                break;
            case GREATER_EQUAL_NULL:
                if (isEmptyInputAllowed) {
                    dataAkk = new String[10002];
                    dataAkk[0] = "";
                    for (int n = 1; n < 10002; n++) {
                        dataAkk[n] = "" + (n - 1);
                    }
                } else {
                    dataAkk = new String[10001];
                    for (int n = 0; n < 10001; n++) {
                        dataAkk[n] = "" + (n);
                    }
                }
                break;
            case STANDARD:
            default:
                if (isEmptyInputAllowed) {
                    dataAkk = new String[20002];
                    for (int n = 0; n < 10000; n++) {
                        dataAkk[n] = "" + (n - 10000);
                    }
                    dataAkk[1000] = "";
                    for (int n = 10001; n < 20002; n++) {
                        dataAkk[n] = "" + (n - 10001);
                    }
                } else {
                    dataAkk = new String[20001];
                    for (int n = 0; n < 20001; n++) {
                        dataAkk[n] = "" + (n - 10000);
                    }
                }
                break;
        }
        return dataAkk;
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        final String s = list.get(columnType);
        if (s != null && !s.isEmpty()) {

            if (s.equals("-1") || s.equals("0")) {
                if (mode == Modes.GREATER_THAN_NULL) {
                    spinner.setValue("");
                }
            } else {
                spinner.setValue(s);
            }
        }
    }

    @Override
    public void clear() {
        if (isEmptyInputAllowed) {
            spinner.setValue("");
        } else if (mode == Modes.GREATER_EQUAL_NULL || mode == Modes.STANDARD) {
            spinner.setValue("0");
        } else if (mode == Modes.GREATER_THAN_NULL) {
            spinner.setValue("1");
        }
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        String saveValue;
        if (spinner.getValue().equals("")) {
            saveValue = "-1";
        } else {
            saveValue = String.valueOf(spinner.getValue());
        }
        list.add(new DataColumn(saveValue, columnType.getColumnName()));
    }


    @Override
    public String getStringRepresentation() {
        String saveValue;
        if (spinner.getValue().equals("")) {
            saveValue = "-1";
        } else {
            saveValue = String.valueOf(spinner.getValue());
        }
        return saveValue;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(((JSpinner.DefaultEditor) spinner.getEditor()).getTextField());
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !spinner.getValue().equals("");
    }

    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        String[] data = fillSpinnerWithData();
        SpinnerModel sm = new SpinnerListModel(data);
        spinner = new JSpinner(sm);
        ((JSpinner.ListEditor) spinner.getEditor()).getTextField().setFormatterFactory(new DefaultFormatterFactory(new MyListFormatter()));
        spinner.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
        spinner.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        spinner.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (spinner.getValue().toString().isEmpty()) {
                    spinner.setToolTipText(null);
                } else {
                    spinner.setToolTipText(spinner.getValue().toString());
                }
            }
        });
        multiPanel.add(BorderLayout.CENTER, spinner);
        clear();

        setContent(multiPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

    // ========================================================================
    public AbstractInputElement setIsEmptyInputAllowed(boolean isEmptyInputAllowed) {
        this.isEmptyInputAllowed = isEmptyInputAllowed;
//        if (spinner != null) {
//            String[] s = {};
//            SpinnerModel sm = new SpinnerListModel(s);
//            spinner.setModel(sm);
//        }
        return this;
    }

    @Override
    public void setInteger(String number) {
        if (!number.isEmpty()) {
            spinner.setValue(number);
        } else {
            clear();
        }
    }

    @Override
    public void setInteger(Integer number) {
        if (number != null) {
            spinner.setValue("" + number);
        } else {
            clear();
        }
    }

    @Override
    public String getIntegerAsString() {
        return String.valueOf(spinner.getValue());

    }

    @Override
    public Integer getInteger() {
        String value = String.valueOf(spinner.getValue());
        if (value.equals("")) {
            return null;
        } else {
            return Integer.parseInt(value);
        }
    }

    @Override
    public void setMode(Modes mode) {
        this.mode = mode;

    }

    // ========================================================================
    private class MyListFormatter extends JFormattedTextField.AbstractFormatter {

        private DocumentFilter filter;

        @Override
        public String valueToString(Object value) throws ParseException {
            if (value == null) {
                return "";
            }
            return value.toString();
        }

        @Override
        public Object stringToValue(String string) throws ParseException {
            return string;
        }

        @Override
        protected DocumentFilter getDocumentFilter() {
            if (filter == null) {
                filter = new Filter();
            }
            return filter;
        }

        private class Filter extends DocumentFilter {

            @Override
            public void replace(DocumentFilter.FilterBypass fb, int offset, int length,
                    String string, AttributeSet attrs) throws
                    BadLocationException {

                super.replace(fb, offset, length, string, attrs);
            }

            @Override
            public void insertString(DocumentFilter.FilterBypass fb, int offset,
                    String string, AttributeSet attr)
                    throws BadLocationException {
                replace(fb, offset, 0, string, attr);
            }
        }
    }
}
