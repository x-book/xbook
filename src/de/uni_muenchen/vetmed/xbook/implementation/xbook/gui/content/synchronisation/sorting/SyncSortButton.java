package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.sorting;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.Synchronisation;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class SyncSortButton extends JButton {

  private ColumnType columnType;
  private Synchronisation syncPanel;
  private SyncSortHelper syncSortHelper;

  public SyncSortButton(Synchronisation syncPanel, SyncSortHelper syncSortHelper,
      ColumnType columnType) {
    super(columnType.getDisplayName());
    this.syncPanel = syncPanel;
    this.syncSortHelper = syncSortHelper;
    this.columnType = columnType;
    addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        new Thread(new Runnable() {
          @Override
          public void run() {
            SyncSortButton.this.syncPanel.doSort(SyncSortButton.this.columnType);
          }
        }).start();
      }
    });
  }

  public void updateLabel() {
    if (syncSortHelper.getState() == SyncSortStates.DEFAULT) {
      setText(columnType.getDisplayName());
    } else if (syncSortHelper.getState() == SyncSortStates.ASC) {
      setText("<html><b>" + columnType.getDisplayName() + "</b> ▼</html>");
    } else if (syncSortHelper.getState() == SyncSortStates.DESC) {
      setText("<html><b>" + columnType.getDisplayName() + "</b> ▲</html>");
    }
  }

  public void resetLabel() {
    setText(columnType.getDisplayName());
  }

  public ColumnType getColumnType() {
    return columnType;
  }

  public void setColumnType(ColumnType columnType) {
    this.columnType = columnType;
  }
}
