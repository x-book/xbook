package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JTextField;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryCodeTableField;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboIdSearchBox;

/**
 * A general object for the OssoBook input fields which contains a combobox to
 * select something. The return value for saving the items to the database are
 * returned as a Integer value.
 *
 * The difference to the InputComboIdBox class is that the user can search for
 * entries by typing letters into the input field.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputComboIdSearchBox extends InputComboIdBox implements IComboIdSearchBox {

    private boolean isEditable = true;

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     */
    public InputComboIdSearchBox(ColumnType columnType) {
        super(columnType);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     */
    @Deprecated
    public InputComboIdSearchBox(ColumnType columnType, int gridX, int gridY) {
        this(columnType, gridX, gridY, false, true, (SidebarPanel) null);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    @Deprecated
    public InputComboIdSearchBox(ColumnType columnType, int gridX, int gridY, SidebarPanel sidebar) {
        this(columnType, gridX, gridY, false, true, sidebar);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param sidebarText The sidebar text for this input element.
     */
    @Deprecated
    public InputComboIdSearchBox(ColumnType columnType, int gridX, int gridY, String sidebarText) {
        this(columnType, gridX, gridY, false, true, new SidebarEntryCodeTableField(columnType, sidebarText));
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param addEmptyEntry State whether an empty entry should be added to the
     * list or not.
     */
    @Deprecated
    public InputComboIdSearchBox(final ColumnType columnType, int gridX, int gridY, boolean addEmptyEntry) {
        this(columnType, gridX, gridY, addEmptyEntry, true, (SidebarPanel) null);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param addEmptyEntry State whether an empty entry should be added to the
     * list or not.
     * @param isEditable Set the combobox editable or not.
     */
    @Deprecated
    public InputComboIdSearchBox(final ColumnType columnType, int gridX, int gridY, boolean addEmptyEntry, boolean isEditable) {
        this(columnType, gridX, gridY, addEmptyEntry, isEditable, (SidebarPanel) null);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param addEmptyEntry State whether an empty entry should be added to the
     * list or not.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    @Deprecated
    public InputComboIdSearchBox(final ColumnType columnType, int gridX, int gridY, boolean addEmptyEntry, SidebarPanel sidebar) {
        this(columnType, gridX, gridY, addEmptyEntry, true, sidebar);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param addEmptyEntry State whether an empty entry should be added to the
     * list or not.
     * @param sidebarText The sidebar text for this input element.
     */
    @Deprecated
    public InputComboIdSearchBox(final ColumnType columnType, int gridX, int gridY, boolean addEmptyEntry, String sidebarText) {
        this(columnType, gridX, gridY, addEmptyEntry, true, new SidebarEntryCodeTableField(columnType, sidebarText));
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param addEmptyEntry State whether an empty entry should be added to the
     * list or not.
     * @param isEditable Set the combobox editable or not.
     * @param sidebarText The sidebar text for this input element.
     */
    @Deprecated
    public InputComboIdSearchBox(final ColumnType columnType, int gridX, int gridY, boolean addEmptyEntry, boolean isEditable, String sidebarText) {
        this(columnType, gridX, gridY, addEmptyEntry, new SidebarEntryCodeTableField(columnType, sidebarText));
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param addEmptyEntry State whether an empty entry should be added to the
     * list or not.
     * @param isEditable Set the combobox editable or not.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    @Deprecated
    public InputComboIdSearchBox(final ColumnType columnType, int gridX, int gridY, boolean addEmptyEntry, boolean isEditable, SidebarPanel sidebar) {
        super(columnType, gridX, gridY, addEmptyEntry, sidebar);

        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);

        setAddEmptyEntry(addEmptyEntry);
        setIsEditable(isEditable);
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();

        combo.setEditable(isEditable);
        combo.setCustomInputEnabled(false);
        combo.setIntegerInputEnabled(true);

        JTextField tf = (JTextField) combo.getEditor().getEditorComponent();
        tf.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                setDefaultStyle(true);
            }
        });
    }

    // ========================================================================
    @Override
    public void setIsEditable(boolean isEditable) {
        this.isEditable = isEditable;
    }

}
