package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard;

import java.lang.reflect.InvocationTargetException;

import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;

/**
 *
 * @author Eduardo Granado <eduardo.granado@unibas.ch>
 *
 */
public abstract class Wizard {

    private WizardPage currentWizardPage = null;
    private WizardPage firstWizardPage = null;

    public AbstractContent createWizardContent()
            throws InstantiationException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException,
            NoSuchMethodException, SecurityException {
        addPages();
        return getStartingPage().getContent();
    }

    /**
     * 
     * @param wizardPageContent 
     */
    public void addPage(AbstractWizardPageContent wizardPageContent) {
        addPage(new WizardPage(wizardPageContent));
    }

    /**
     * Adds a new Page to the wizard
     * @param page 
     */
    public void addPage(WizardPage page) {
        if (page instanceof WizardPage) {
            if (currentWizardPage == null) {
                firstWizardPage = page;
                page.setPreviousPage(null);
            } else {
                page.setPreviousPage(currentWizardPage);
                currentWizardPage.setNextPage(page);
            }
            page.setWizard(this);
            page.setNextPage(null);
            currentWizardPage = page;
        }
    }

    public WizardPage getNextPage(WizardPage page) {
        return page.getNextPage();
    }

    public WizardPage getPreviousPage(WizardPage page) {
        return page.getPreviousPage();
    }

    public WizardPage getStartingPage() {
        return firstWizardPage;
    }

    public abstract void addPages();

    public abstract boolean canFinish();

    public abstract boolean performFinish();
    // public abstract boolean performCancel();
}
