package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.filter;

import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class SyncFilterRowData {

    private SyncFilterRowMode mode;
    private ArrayList<String> values;

    SyncFilterRowData(SyncFilterRowMode mode, ArrayList<String> values) {
        this.mode = mode;
        this.values = values;
    }

    public ArrayList<String> getValues() {
        return values;
    }

    public SyncFilterRowMode getMode() {
        return mode;
    }

    public static SyncFilterRowData createEmptySet() {
        return new SyncFilterRowData(SyncFilterRowMode.CONTAINS, new ArrayList<String>());
    }
}