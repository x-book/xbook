package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard;

import java.lang.reflect.InvocationTargetException;

import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;

public class WizardPage {

    private Wizard wizard = null;
    private WizardPage nextPage = null;
    private WizardPage previousPage = null;
    private AbstractWizardPageContent wizardPageContent;

    public WizardPage(AbstractWizardPageContent wizardPageContent) {
        super();

        if (wizardPageContent == null) {
            throw new NullPointerException();
        } else {
            this.wizardPageContent = wizardPageContent;
        }
    }

    public AbstractContent getContent()
            throws InstantiationException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException,
            NoSuchMethodException, SecurityException {
        if (wizardPageContent.getWizardPage() == null) {

            wizardPageContent.setPage(this);
        }
        return wizardPageContent;
    }

    public void deleteContentCache() {
        if (wizardPageContent.getWizardPage() == null) {

            wizardPageContent.setPage(this);
        }
        wizardPageContent.reset();
    }

    public boolean isLastPage() {
        return nextPage == null;
    }

    public boolean isFirstPage() {
        return previousPage == null;
    }

    public WizardPage getNextPage() {
        return nextPage;
    }

    public WizardPage getPreviousPage() {
        return previousPage;
    }

    public void setNextPage(WizardPage page) {
        nextPage = page;
    }

    public void setPreviousPage(WizardPage page) {
        previousPage = page;
    }

    public Wizard getWizard() {
        return wizard;
    }

    public void setWizard(Wizard wizard) {
        this.wizard = wizard;
    }
}
