package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.page;

import de.uni_muenchen.vetmed.xbook.api.Loc;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.ImportWizard;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.AbstractOpenCsvFilePanel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.AbstractOpenCsvFilePanel.TableChangedListener;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.AbstractWizardPageContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.WizardPage;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer.UserMainTable;

/**
 *
 *
 * @author Eduardo Granado <eduardo.granado@unibas.ch>
 */
public abstract class AbstractOpenCsvFileWizardPageContent extends AbstractWizardPageContent {

	private AbstractOpenCsvFilePanel openCsvFilePanel = null;
	protected ImportWizard wizard = null;

	public AbstractOpenCsvFileWizardPageContent() {
	}

	@Override
	public void setPage(WizardPage page) {
		super.setPage(page);
		init();
	}

	private AbstractOpenCsvFilePanel createContent() {
		openCsvFilePanel = getOpenCsvFilePanel();
		openCsvFilePanel.setTitle(getTitle());
		wizard = (ImportWizard) getWizardPage().getWizard();
		openCsvFilePanel.setTableChangedListener(new TableChangedListener() {
			@Override
			public void tableChanged(AbstractOpenCsvFilePanel src) {
				String[][] table = src.getTable();

				boolean tableIsValid = src.validateTable(table);

				setPageComplete(tableIsValid);

				if (tableIsValid) {
					wizard.setUserTable(new UserMainTable(table));
				} else {
					wizard.setUserTable(null);
				}

				getWizardPage().getNextPage().deleteContentCache();
			}
		});
		return openCsvFilePanel;
	}

	@Override
	public void reset() {
		openCsvFilePanel.reset();
	}

	@Override
	public javax.swing.JPanel getContent() {
		if (openCsvFilePanel == null) {
			openCsvFilePanel = createContent();
		}
		return openCsvFilePanel;
	}

	@Override
	public String getTitle() {
		return Loc.get("SELECT_TABLE_TO_IMPORT");
	}
	
	protected abstract AbstractOpenCsvFilePanel getOpenCsvFilePanel();
}
