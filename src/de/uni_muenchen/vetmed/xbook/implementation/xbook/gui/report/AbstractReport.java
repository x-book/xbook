package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.report;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.BigButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 14.11.2016.
 */
public abstract class AbstractReport extends AbstractContent {

  final static Log LOG = LogFactory.getLog(AbstractReport.class);

  private static final char[] ILLEGAL_CHARACTERS = {'/', '\n', '\r', '\t', '\0', '\f', '`', '?',
      '*', '\\', '<', '>', '|', '\"', ':'};

  public AbstractReport() {
    super();
  }

  public static void doReport(ArrayList<JasperPrint> printList, String name, Component parent) {
    try {

      JFileChooser chooser = new FileSaver();
      chooser.setFileFilter(
          new FileNameExtensionFilter("<html><body><b>PDF</b> [Adobe PDF]</body><html>", "pdf"));
      chooser.setSelectedFile(new File(name));
      int retVal = chooser.showSaveDialog(parent);
      if (retVal != JFileChooser.APPROVE_OPTION) {
        return;
      }

      JRPdfExporter exporter = new JRPdfExporter();
      File selectedFile = chooser.getSelectedFile();

      if (FilenameUtils.getExtension(selectedFile.getName()).equalsIgnoreCase("pdf")) {
        // filename is OK as-is
      } else {
        selectedFile = new File(selectedFile.toString() + ".pdf");
      }
      String fileName = selectedFile.getPath();

      selectedFile.mkdirs();
      try {
        selectedFile.createNewFile();
      } catch (IOException ex) {
        Footer.displayError(Loc.get("ERROR_FILE_NAME_INVALID"));
        doReport(printList, name, parent);
        return;
      }

      if (selectedFile.exists()) {
        boolean delete = selectedFile.delete();
        if (!delete) {
          Footer.displayError(Loc.get("ERROR_FILE_IN_USE"));
          doReport(printList, name, parent);
          return;
        }
      }

      exporter.setExporterInput(SimpleExporterInput.getInstance(printList));
      exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(fileName));
      SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
      configuration.setCreatingBatchModeBookmarks(true);
      exporter.setConfiguration(configuration);

      exporter.exportReport();
      Desktop.getDesktop().open(selectedFile);
      Footer.displayConfirmation(fileName);
    } catch (JRException | JRRuntimeException | IOException e) {
      LOG.error(e);
      Footer.displayError(Loc.get("ERROR_WHILE_GENERATING_THE_PDF_FILE"));
    }

  }

  public static void doReport(Map<String, List<JasperPrint>> printMap, String name,
      Component parent) {
    try {

      JFileChooser chooser = new FileSaver();
      chooser.setFileFilter(
          new FileNameExtensionFilter("<html><body><b>PDF</b> [Adobe PDF]</body><html>", "pdf"));
      chooser.setSelectedFile(new File(name));

      int retVal = chooser.showSaveDialog(parent);
      if (retVal != JFileChooser.APPROVE_OPTION) {
        return;
      }
      File selectedFile = chooser.getSelectedFile();
      selectedFile.delete();
      for (Map.Entry<String, List<JasperPrint>> jasperEntry : printMap.entrySet()) {
        List<JasperPrint> printList = jasperEntry.getValue();

        JRPdfExporter exporter = new JRPdfExporter();
        File pdfFile;
        String selectedFileName = selectedFile.getPath();
        String extension = FilenameUtils.getExtension(selectedFileName);
        if (extension.equalsIgnoreCase("pdf")) {
          // filename is OK as-is
          pdfFile = new File(
              selectedFileName.substring(0, selectedFileName.length() - (extension.length() + 1))
                  + " " + jasperEntry.getKey() + ".pdf");
        } else {
          pdfFile = new File(selectedFile.toString() + " " + jasperEntry.getKey() + ".pdf");
        }
        String fileName = pdfFile.getPath();

        pdfFile.mkdirs();
        try {
          pdfFile.createNewFile();
        } catch (IOException ex) {
          Footer.displayError(Loc.get("ERROR_FILE_NAME_INVALID"));
          doReport(printMap, name, parent);
          return;
        }

        if (pdfFile.exists()) {
          boolean delete = pdfFile.delete();
          if (!delete) {
            Footer.displayError(Loc.get("ERROR_FILE_IN_USE"));
            doReport(printMap, name, parent);
            return;
          }
        }

        exporter.setExporterInput(SimpleExporterInput.getInstance(printList));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(fileName));
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        configuration.setCreatingBatchModeBookmarks(true);
        exporter.setConfiguration(configuration);

        exporter.exportReport();
      }
      Footer.displayConfirmation(selectedFile.getName());
    } catch (JRException | JRRuntimeException e) {
      LOG.error(e);
      Footer.displayError(Loc.get("ERROR_WHILE_GENERATING_THE_PDF_FILE"));
    }

  }

  protected abstract String getFileName(String name, ProjectDataSet currentProject);

  protected BigButton getReportButton(final String name, final Report report) {
    return getReportButton(name, "", null, report);
  }

  protected BigButton getReportButton(final String name, final Icon icon, final Report report) {
    return getReportButton(name, "", icon, report);
  }

  protected BigButton getReportButton(final String name, final String subLine,
      final Report report) {
    return getReportButton(name, subLine, null, report);
  }

  protected BigButton getReportButton(final String name, final String subLine, final Icon icon,
      final Report report) {
    BigButton btnFeatureListWithFinding = new BigButton(name, subLine, icon);
    btnFeatureListWithFinding.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        new Thread(new Runnable() {
          @Override
          public void run() {
            Footer.startWorking();
            ProjectDataSet currentProject = null;
            try {
              currentProject = mainFrame.getController().getCurrentProject();
              ArrayList<JasperPrint> report1 = report.getReport(currentProject);
              if (report1.isEmpty()) {
                Footer.displayError(Loc.get("ERROR_NO_DATA_AVAILABLE_FOR_REPORT"));
                return;
              }
              doReport(report1, getFileName(name, currentProject), AbstractReport.this);
            } catch (NotLoadedException | StatementNotExecutedException | NoRightException | NotLoggedInException | JRException | IOException e1) {
              LOG.error(e1, e1);
              Footer.displayError(Loc.get("REPORT_ERROR"));
            } finally {
              Footer.hideProgressBar();
            }
          }
        }).start();
      }

    });
    return btnFeatureListWithFinding;
  }

  @Override
  public ButtonPanel getButtonBar() {
    return null;
  }

  @Override
  public SidebarPanel getSideBar() {
    return null;
  }

  @Override
  public boolean forceSidebar() {
    return false;
  }

  public static abstract class Report {

    public abstract ArrayList<JasperPrint> getReport(ProjectDataSet currentProject)
        throws StatementNotExecutedException, NoRightException, NotLoggedInException, JRException, NotLoadedException, IOException;
  }

  public static class SingleComparator implements Comparator<Map<String, ?>> {

    final private String column;
    private boolean compareNumber;


    public SingleComparator(ColumnType columnType) {
      this(columnType.getColumnName(), false);
    }

    public SingleComparator(String column) {
      this(column, false);
    }

    /**
     * Compares the values belonging to the selected columntype. If specified this uses a numeric
     * compare
     */
    public SingleComparator(ColumnType columnType, boolean compareNumber) {
      this(columnType.getColumnName(), compareNumber);
    }

    public SingleComparator(String column, boolean compareNumber) {
      this.column = column;
      this.compareNumber = compareNumber;
    }

    @Override
    public int compare(Map<String, ?> o1, Map<String, ?> o2) {
      Object c1 = o1.get(column);
      Object c2 = o2.get(column);
      return compareTo(c1, c2, compareNumber);

    }
  }

  public static class MultComparator implements Comparator<Map<String, ?>> {

    ArrayList<String> columnsToSort = new ArrayList<>();
    private boolean compareNumber;

    public MultComparator(ColumnType columnType1, ColumnType columnType2) {
      this(columnType1, columnType2, false);
    }

    public MultComparator(String column1, String column2) {
      this(column1, column2, false);
    }

    public MultComparator(ColumnType columnType1, ColumnType columnType2, boolean compareNumber) {
      this(columnType1.getColumnName(), columnType2.getColumnName(), compareNumber);
    }

    public MultComparator(String column1, String column2, boolean compareNumber) {
      columnsToSort.add(column1);
      columnsToSort.add(column2);
      this.compareNumber = compareNumber;
    }

    public MultComparator(boolean compareNumber, String... compare) {
      columnsToSort.addAll(Arrays.asList(compare));
      this.compareNumber = compareNumber;
    }

    public MultComparator(String... compare) {
      columnsToSort.addAll(Arrays.asList(compare));
      this.compareNumber = false;
    }

    @Override
    public int compare(Map<String, ?> o1, Map<String, ?> o2) {

      for (String s : columnsToSort) {
        Object c1 = o1.get(s);
        Object c2 = o2.get(s);
        int i = compareTo(c1, c2, compareNumber);
        if (i != 0) {
          return i;
        }
      }
      return 0;
    }

  }

  /**
   * Compares one object to the other using either the string compare of the objects or if specified
   * the number comparator
   */
  public static int compareTo(Object o1, Object o2, boolean compareNumber) {
    if (o1 != null && o2 != null) {
      String s1 = o1.toString();
      String s2 = o2.toString();
      if (compareNumber) {
        return compareNumber(s1, s2);
      } else {
        return s1.compareTo(s2);
      }
    }

    if (o1 == null && o2 == null) {
      return 0;
    }
    if (o1 == null) {
      return -1;
    }
    return 1;
  }

  public static int compareNumber(Object o1, Object o2) {
    if (o1 != null && o2 != null) {
      String number1 = (String) o1;
      String number2 = (String) o2;

      try {
        String[] split1 = number1.split("-");
        String[] split2 = number2.split("-");
        for (int i = 0; i < Math.min(split1.length, split2.length); i++) {
          String s = split1[i];
          if (s.isEmpty()) {
            s = "0";
          }
          int i1 = Integer.valueOf(s);
          String s1 = split2[i];
          if (s1.isEmpty()) {
            s1 = "0";
          }
          int i2 = Integer.valueOf(s1);
          if (i1 != i2) {
            return Integer.compare(i1, i2);
          }
        }

        if (split1.length == split2.length) {
          return 0;
        }
        return Integer.compare(split1.length, split2.length);

      } catch (NumberFormatException ignore) {
      }
      return number1.compareTo(number2);
    }
    if (o1 == null) {
      return -1;
    }
    return 1;
  }
}
