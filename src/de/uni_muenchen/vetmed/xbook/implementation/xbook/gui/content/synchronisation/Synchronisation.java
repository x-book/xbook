package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectInformation;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel.Position;
import de.uni_muenchen.vetmed.xbook.api.gui.content.CannotBeReturnedOnPressingBackButton;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableArrayList;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableString;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractProjectManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.framework.swing.component.CheckBoxList;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.filter.SyncFilter;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.filter.SyncFilterRowData;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.filter.SyncFilterRowMode;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.sorting.SyncSortButton;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.sorting.SyncSortHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.EntriesSynchronisationProgress;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.ProjectSynchronisation;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.SynchronisationProgress;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This frame is used for the synchronization.
 * <p>
 * <p>
 * It displays two lists, one of the locally available projects, one of the globally available
 * projects, from which the user may select which projects to synchronize. </p>
 *
 * @author fnuecke
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Synchronisation extends AbstractContent implements PropertyChangeListener,
        CannotBeReturnedOnPressingBackButton {

    private static final Log LOGGER = LogFactory.getLog(Synchronisation.class);
    private ArrayList<SyncSortButton> allSortButtons;
    private ArrayList<Serialisable> globalProjectData;

    /**
     * Possible logging types that can be logged to the information field.
     */
    private enum LogType {

        /**
         * Default message (black).
         */
        DEFAULT,
        /**
         * Warning message (orange).
         */
        WARNING,
        /**
         * Error message (red).
         */
        ERROR,
        /**
         * System notice (grey).
         */
        SYSTEM,
        /**
         * Confirmation (green).
         */
        CONFIRMATION
    }

    /**
     * A list of components to disable while the synchronization is in progress.
     */
    private final ArrayList<Component> disableWhileSynchronizing = new ArrayList<>();
    private XImageButton syncButton;
    private XImageButton reloadButton;
    private boolean isRunning = false;
    private HashMap<Key, SyncProjectMapper> mapping = new HashMap<>();
    /**
     * List with locally available projects.
     */
    private CheckBoxList listLocal;
    /**
     * List with globally available projects.
     */
    private CheckBoxList listGlobal;
    /**
   * Helper variable to check if currently synchronizing the project lists after a selection change
   * (to avoid infinite loops).
     */
    private boolean syncingLists;
    /**
     * Progress bar for displaying overall progress.
     */
    private JProgressBar progressOverall;
    /**
     * Progress bar for displaying current project progress.
     */
    private JProgressBar progressProject;
    /**
     * Progress bar for displaying current progress progress.
     */
    private JProgressBar progressDataset;
    /**
     * Wrapper around the overall progress bar to display the label.
     */
    private JPanel progressOverallWrapper;
    /**
     * Wrapper around the project progress bar to display the label.
     */
    private JPanel progressProjectWrapper;
    /**
     * Wrapper around the process progress bar to display the label.
     */
    private JPanel progressDatasetWrapper;
    /**
     * Text area used to output status messages regarding the synchronization.
     */
    private JTextPane output;
    /**
     * Synchronizer used to synchronize databases.
     */
    private ProjectSynchronisation synchronizer;
    /**
     * The button to start the synchronisation.
     */
    private JButton buttonSynchronize;
    private boolean loadedBefore = false;
    private JPanel contentWrapper;
    protected SidebarPanel sidebar;

    /**
     * The button to open the filter panel.
     */
    private XImageButton filterButton;
    /**
     * The button to unset the filter settings.
     */
    private XImageButton filterRevertButton;

    private final AbstractMainFrame<?> abstractMainFrame;
    protected final SyncFilter syncFilter;

    protected SyncSortHelper syncSortHelper = new SyncSortHelper();

    /**
   * Creates a new synchronization window which allows selecting the projects to synchronize and
   * initialize the synchronization.
     *
     * @param mainFrame The basic MainFrame object.
     * @throws IllegalStateException if another SynchronizationFrame is already opened.
     */
    public Synchronisation(final AbstractMainFrame mainFrame) throws IllegalStateException {
        super();
        abstractMainFrame = mainFrame;
        syncFilter = mainFrame.getSynchronisationFilterScreen(this);
        init();
    }

    @Override
    protected JPanel getContent() {
        contentWrapper = new JPanel();

        // Layout...
        contentWrapper.setLayout(new BorderLayout());
        contentWrapper.setBackground(Colors.CONTENT_BACKGROUND);

        GridBagLayout layout = new GridBagLayout();
        JPanel gridContent = new JPanel(layout);
        gridContent.setBackground(Colors.CONTENT_BACKGROUND);

        JPanel topWrapper = new JPanel(new StackLayout());
        contentWrapper.add(BorderLayout.NORTH, topWrapper);

        topWrapper.add(new XTitle(Loc.get("SYNCHRONISATION")));

        // sorting box
        if (isSortingEnabled()) {
            JPanel sortingContainer = new JPanel();
            sortingContainer.setBackground(Colors.CONTENT_BACKGROUND);
            sortingContainer.setBorder(
          BorderFactory
              .createTitledBorder("<html><body><b>" + Loc.get("SORTING") + "</b></body></html>"));
            topWrapper.add(sortingContainer);

            allSortButtons = getAllSortButtons();
            for (SyncSortButton sortButton : allSortButtons) {
                sortingContainer.add(sortButton);
            }
        }

        // main content
        contentWrapper.add(BorderLayout.CENTER, gridContent);

        // Create components that go inside this window.
        // Starting with the projects panel.
        JPanel projects = new JPanel();
        projects.setBackground(Colors.CONTENT_BACKGROUND);
        gridContent.add(projects);
        buildConstraints(projects, layout, 0, 4);
        projects.setBorder(BorderFactory.createEmptyBorder());
        projects.setLayout(new BoxLayout(projects, BoxLayout.X_AXIS));

        // Create the two lists for local and global projects.
    listLocal = buildProjectList(projects,
        "<html><body><b>" + Loc.get("LOCAL_PROJECTS") + "</b></body></html>");
        listGlobal = buildProjectList(projects,
        "<html><body><b>" + Loc.get("GLOBAL_PROJECTS") + " (" + Loc.get("SERVER")
            + ")</b></body></html>");

        // Add listeners to allow synchronizing the project selection.
        // This is just a cosmetic, but improves usability quite a bit (it
        // would be enough if a project were selected in just one of the lists).
        listLocal.getCheckBoxListSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                synchronizeSelection(listLocal, listGlobal);
            }
        });
    listGlobal.getCheckBoxListSelectionModel()
        .addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                synchronizeSelection(listGlobal, listLocal);
            }
        });

        // Create the overall progress bar.
        progressOverallWrapper = new JPanel();
        progressOverallWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        gridContent.add(progressOverallWrapper);
        buildConstraints(progressOverallWrapper, layout, 0, 1, 2, 0);
        progressOverallWrapper.setLayout(new BoxLayout(progressOverallWrapper, BoxLayout.Y_AXIS));

        progressOverall = new JProgressBar();
        setOverallLabel();
        progressOverallWrapper.add(progressOverall);

        // Create the current project progress bar.
        progressProjectWrapper = new JPanel();
        progressProjectWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        gridContent.add(progressProjectWrapper);
        buildConstraints(progressProjectWrapper, layout, 0, 2, 1, 0);
        progressProjectWrapper.setLayout(new BoxLayout(progressProjectWrapper, BoxLayout.Y_AXIS));

        progressProject = new JProgressBar();
        setProjectLabel();
        progressProjectWrapper.add(progressProject);

        // Create the current process progress bars.
        progressDatasetWrapper = new JPanel();
        progressDatasetWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        gridContent.add(progressDatasetWrapper);
        buildConstraints(progressDatasetWrapper, layout, 1, 2, 1, 0);
        progressDatasetWrapper.setLayout(new BoxLayout(progressDatasetWrapper, BoxLayout.Y_AXIS));

        progressDataset = new JProgressBar();
        setDatasetLabel();
        progressDatasetWrapper.add(progressDataset);

        // The information text pane used for displaying log messages.
        JPanel log = new JPanel();
        log.setBackground(Colors.CONTENT_BACKGROUND);
        gridContent.add(log);
        buildConstraints(log, layout, 3, 2);
        log.setBorder(
        BorderFactory
            .createTitledBorder("<html><body><b>" + Loc.get("INFORMATION") + "</b></body></html>"));
        log.setLayout(new BoxLayout(log, BoxLayout.Y_AXIS));

        JScrollPane outputScroller = new JScrollPane();
        outputScroller.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        log.add(outputScroller);
        outputScroller.setPreferredSize(new Dimension());

        output = new JTextPane();
        outputScroller.setViewportView(output);
        output.setBorder(BorderFactory.createEmptyBorder());
        output.setEditable(false);

        logMessage(Loc.get("SYNCHRONISATION_NOTICE"), LogType.DEFAULT);
        logMessage(Loc.get("SYNCHRONISATION_INITIALISING"), LogType.SYSTEM);

        // Create connection to global database
        contentWrapper.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        loadedBefore = true;
        return contentWrapper;
    }

    protected ArrayList<SyncSortButton> getAllSortButtons() {
        ArrayList<SyncSortButton> array = new ArrayList<>();
        array.add(new SyncSortButton(this, syncSortHelper, AbstractProjectManager.PROJECT_PROJECTNAME));
    array
        .add(new SyncSortButton(this, syncSortHelper, AbstractProjectManager.PROJECT_PROJECTOWNER));
        return array;
    }

    public void doSort(ColumnType columnType) {


        for (int i = 0; i < allSortButtons.size(); i++) {
            Footer.setProgressBarValue((i / (allSortButtons.size() * 2.0) * 100));
            if (!allSortButtons.get(i).getColumnType().equals(columnType)) {
                allSortButtons.get(i).resetLabel();
            }
        }
        syncSortHelper.set(columnType);
        for (int i = 0; i < allSortButtons.size(); i++) {
      Footer
          .setProgressBarValue(((i + allSortButtons.size()) / (allSortButtons.size() * 2.0) * 100));
            if (allSortButtons.get(i).getColumnType().equals(columnType)) {
                allSortButtons.get(i).updateLabel();
            }
        }
        updateProjectLists();


    }

    /**
     * Updates the project lists.
     *
     * @returns <code>true</code> if the server currently allows synching, <code>false<code> else.
     */
    private synchronized boolean updateProjectLists() {

    SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
            Footer.setProgressBarValue(0);
            Footer.showProgressBar();
        }
    });
        mapping.clear();

        // Clear lists.
        listLocal.setModel(new DefaultListModel());
        listGlobal.setModel(new DefaultListModel());

        // setting the filter style
        if (isFilteringEnabled()) {
            if (!syncFilter.getCurrentSelection().isSomethingSet()) {
                filterButton.setStyle(XImageButton.Style.COLOR);
                filterRevertButton.setStyle(XImageButton.Style.GRAY);
            } else {
                filterButton.setStyle(XImageButton.Style.GREEN);
                filterRevertButton.setStyle(XImageButton.Style.RED);
            }
        }

        // load the projects
        logMessage(Loc.get("LOADING_PROJECTS"), LogType.SYSTEM);
        try {
            final AbstractSynchronisationController controller =
                    (AbstractSynchronisationController) abstractMainFrame.getController();
            try {
                controller.updateProjectRights();
                controller.updateProjectRightsGroup();
            } catch (NotConnectedException e) {
                System.out.println("NotConnectedException in Synchronization.java   ");
            }
            // Fill in available projects.
      final DefaultListModel<SyncProjectWrapper> localModel = new DefaultListModel<>();
            ArrayList<ProjectDataSet> projectsLocal = new ArrayList<>();
            try {

        //Maybe also return a ProjectInformation object. This would allow better comparision.
                for (ProjectDataSet project :
                        controller.getLocalManager().getProjectManager().getProjects()) {
                    // filter the projects for the specified settings in the filter
                    if (!syncFilter.getCurrentSelection().isSomethingSet()) {
                        projectsLocal.add(project);
                    } else {
                        filterLocalProjects(project, projectsLocal);
                    }

                }
            } catch (StatementNotExecutedException ex) {
                projectsLocal = new ArrayList<>();
            }
            Collections.sort(projectsLocal, new Comparator<ProjectDataSet>() {
                @Override
                public int compare(ProjectDataSet o1, ProjectDataSet o2) {
                    int i = o1.getProjectRow().get(syncSortHelper.getColumnType()).toLowerCase().compareTo(
                            o2.getProjectRow().get(syncSortHelper.getColumnType()).toLowerCase());
                    switch (syncSortHelper.getState()) {
                        case ASC:
                            return i;
                        case DESC:
                            return i * -1;
                        case DEFAULT:
                        default:
                            return i;
                    }
                }
            });

            // LOCAL
                for (ProjectDataSet project : projectsLocal) {
                    SyncProjectWrapper wrapper = new SyncProjectWrapper(project);

                    SyncProjectMapper mapper = new SyncProjectMapper();
                    mapper.setLocal(wrapper);
                    mapping.put(project.getProjectKey(), mapper);

                    // ...
                    // ...
                    localModel.addElement(wrapper);

                }

            // GLOBAL
      final DefaultListModel<SyncProjectWrapper> globalModel = new DefaultListModel<>();

      SerialisableArrayList<SerialisableString> list = new SerialisableArrayList<>();
      for (IBaseManager iBase : (abstractMainFrame.getController()).getLocalManager()
          .getSyncTables()) {
        list.add(new SerialisableString(iBase.getTableName()));
      }
      Message ms = (controller).sendMessage(Commands.REQUEST_PROJECT_LIST_WITH_INFO, list);

            if (ms.getResult().wasSuccessful()) {

                globalProjectData = ms.getData();
                Collections.sort(globalProjectData, new Comparator<Serialisable>() {
                    @Override
                    public int compare(Serialisable s1, Serialisable s2) {
            ProjectDataSet o1 = ((ProjectInformation) s1).getProject();
            ProjectDataSet o2 = ((ProjectInformation) s2).getProject();
                        int i = o1.getProjectRow().get(syncSortHelper.getColumnType()).toLowerCase().compareTo(
                                o2.getProjectRow().get(syncSortHelper.getColumnType()).toLowerCase());
                        switch (syncSortHelper.getState()) {
                            case ASC:
                                return i;
                            case DESC:
                                return i * -1;
                            case DEFAULT:
                            default:
                                return i;
                        }
                    }
                });
        for (Serialisable globalProjectDatum : globalProjectData) {
          ProjectInformation projectInformation = (ProjectInformation) globalProjectDatum;

          final ProjectDataSet project = projectInformation.getProject();
                    // apply filter
                    if (syncFilter.getCurrentSelection().isSomethingSet()) {
                        boolean displayIt = filterProjects(project);
                        if (!displayIt) {
                            continue;
                        }
                    }

                    // display content
                    if (project.isDeleted()) {
                        if (projectsLocal.contains(project)) {
                            // TODO Display message and delete local project if it is not available on server anymore
                            System.out.println("TODO delete Project " + project);
                        }
                    } else {
            SyncProjectWrapperGlobal wrapper = new SyncProjectWrapperGlobal(
                projectInformation);
                        SyncProjectMapper mapper;
                        if (mapping.containsKey(project.getProjectKey())) {
                            mapper = mapping.get(project.getProjectKey());
                        } else {
                            mapper = new SyncProjectMapper();
                            mapping.put(project.getProjectKey(), mapper);
                        }
                        mapper.setGlobal(wrapper);
                        // ...

                        // ...
                        globalModel.addElement(wrapper);

                    }
                }
            } else {
                System.out.println("ERRORRR: " + ms.getResult());
            }

            listLocal.setModel(localModel);
            listLocal.selectNone();
            listGlobal.setModel(globalModel);
            listGlobal.selectNone();
            logMessage(Loc.get("LOADING_DETAILED_PROJECT_INFORMATION"), LogType.SYSTEM);
            updateLabels();
            logMessage(Loc.get("COMPLETED"), LogType.SYSTEM);
        } catch (IOException | NotLoggedInException | NotConnectedException | StatementNotExecutedException ex) {
            Logger.getLogger(Synchronisation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException e) {
            e.printStackTrace();
            logMessage(Loc.get("FAILED_GETTING_DATABASE_CONNECTION"), LogType.ERROR);
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Footer.hideProgressBar();

            }
        });

        return true;
    }

    private void setConnectionAvailable(boolean b) {
        reloadButton.setVisible(b);
        if (filterButton != null) {
            filterButton.setVisible(b);
        }
        if (filterRevertButton != null) {
            filterRevertButton.setVisible(b);
        }
        syncButton.setVisible(b);
    }

    /**
     * Defines all ColumnTypes which should be filterable.
     *
     * @return The filterable ColumnTypes.
     */
    protected ArrayList<ColumnType> getColumnTypesToFilter() {
        ArrayList<ColumnType> c = new ArrayList<>();
        c.add(AbstractProjectManager.PROJECT_PROJECTNAME);
        c.add(AbstractProjectManager.PROJECT_PROJECTOWNER);
        return c;
    }

    private boolean filterProjects(ProjectDataSet project) {
        boolean displayIt = true;

        SyncFilterRowData valuesProjectName = syncFilter.getCurrentSelection().getProjectNameValues();
        if (!valuesProjectName.getValues().isEmpty()) {
            boolean addItLocal = false;
            for (String s : valuesProjectName.getValues()) {
                boolean contains =
            valuesProjectName.getMode() == SyncFilterRowMode.CONTAINS && project.getProjectName()
                .toLowerCase().contains(
                                s.toLowerCase());
                boolean equals =
            valuesProjectName.getMode() == SyncFilterRowMode.IS && project.getProjectName()
                .toLowerCase().equals(
                                s.toLowerCase());
                if (contains || equals) {
                    addItLocal = true;
                    break;
                }
            }
            displayIt &= addItLocal;
        }

        SyncFilterRowData valuesProjectOwner = syncFilter.getCurrentSelection().getProjectOwnerValues();
        if (!valuesProjectOwner.getValues().isEmpty()) {
            boolean addItLocal = false;
            for (String s : valuesProjectOwner.getValues()) {
                try {
                    String owner = mainFrame.getController().getDisplayName(project.getProjectOwnerId());
                    boolean contains =
              valuesProjectName.getMode() == SyncFilterRowMode.CONTAINS && owner.toLowerCase()
                  .contains(
                                    s.toLowerCase());
          boolean equals =
              valuesProjectName.getMode() == SyncFilterRowMode.IS && owner.toLowerCase().equals(
                            s.toLowerCase());
                    if (contains || equals) {
                        addItLocal = true;
                        break;
                    }
                } catch (NotLoggedInException | StatementNotExecutedException e) {
                    e.printStackTrace();
                } catch (NumberFormatException e) {
                    // ignore invalid id
                }
            }
            displayIt &= addItLocal;
        }

        HashMap<ColumnType, SyncFilterRowData> additionalFilters =
                syncFilter.getCurrentSelection().getAdditionalFilters();

    for (Entry<ColumnType, SyncFilterRowData> columnTypeArrayListEntry : additionalFilters
        .entrySet()) {
            SyncFilterRowData value = columnTypeArrayListEntry.getValue();
            if (!value.getValues().isEmpty()) {
                boolean addItLocal = false;
                for (String s : value.getValues()) {
                    if (project.getProjectRow().get(columnTypeArrayListEntry.getKey()).toLowerCase().contains(
                            s.toLowerCase())) {
            boolean contains =
                value.getMode() == SyncFilterRowMode.CONTAINS && project.getProjectRow().get(
                                columnTypeArrayListEntry.getKey()).toLowerCase().contains(s.toLowerCase());
                        boolean equals = value.getMode() == SyncFilterRowMode.IS && project.getProjectRow().get(
                                columnTypeArrayListEntry.getKey()).toLowerCase().equals(s.toLowerCase());
                        if (contains || equals) {
                            addItLocal = true;
                            break;
                        }
                    }
                }
                displayIt &= addItLocal;
            }
        }

        return displayIt;
    }

  protected void filterLocalProjects(ProjectDataSet project,
      ArrayList<ProjectDataSet> projectsLocal) {
        if (filterProjects(project)) {
            projectsLocal.add(project);
        }
    }

    /**
     * Checks if at least one project is selected.
     *
     * @return <code>true</code> if at least one project is selected, * * else
     * <code>false</code>.
     */
    private boolean areProjectsSelected() {
        return listLocal.getCheckBoxListSelectedIndices().length > 0
                || listGlobal.getCheckBoxListSelectedIndices().length > 0;
    }

    /**
     * Gets a list of all selected projects.
     *
     * @return a list of all selected projects.
     */
    private ArrayList<ProjectDataSet> getSelectedProjects() {
        ArrayList<ProjectDataSet> projects = new UniqueArrayList<>();
        for (Object element : listLocal.getCheckBoxListSelectedValues()) {
            projects.add(((SyncProjectWrapper) element).getProjectDataSet());
        }
        for (Object element : listGlobal.getCheckBoxListSelectedValues()) {
            projects.add(((SyncProjectWrapper) element).getProjectDataSet());
        }
        return projects;
    }

    /**
     * Utility function to create the project lists.
     *
     * @param parent the container into which to add the lists.
     * @param title  the title of the list.
     * @return the actual list component created.
     */
    private CheckBoxList<SyncProjectWrapper> buildProjectList(JComponent parent, String title) {

        JPanel container = new JPanel();
        container.setBackground(Colors.CONTENT_BACKGROUND);
        parent.add(container);
        container.setBorder(BorderFactory.createTitledBorder(title));
        GridBagLayout layout = new GridBagLayout();
        container.setLayout(layout);

        JScrollPane listScroller = new JScrollPane();
        container.add(listScroller);
        buildConstraints(listScroller, layout, 0, 2);
        listScroller.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        listScroller.setPreferredSize(new Dimension());
        final CheckBoxList<SyncProjectWrapper> list = new CheckBoxList<>();
        listScroller.setViewportView(list);
        disableWhileSynchronizing.add(list);
        list.setBorder(BorderFactory.createEmptyBorder());
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        XButton all = new XButton(Loc.get("ALL"));
        container.add(all);
        buildConstraints(all, layout, 0, 1, 1, 0);
        disableWhileSynchronizing.add(all);
        all.setFocusable(false);
        all.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                list.selectAll();
            }
        });

        XButton none = new XButton(Loc.get("NONE"));
        container.add(none);
        buildConstraints(none, layout, 1, 1, 1, 0);
        disableWhileSynchronizing.add(none);
        none.setFocusable(false);
        none.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                list.selectNone();
            }
        });

        return list;
    }

    /**
     * Utility method for setting up gridbaglayouts.
     */
    private void buildConstraints(Component c, GridBagLayout layout, int y, int w) {
        buildConstraints(c, layout, 0, y, w, 1);
    }

    /**
     * Utility method for setting up gridbaglayouts.
     */
  private static void buildConstraints(Component c, GridBagLayout layout, int x, int y, int w,
      double wy) {
        layout.setConstraints(c, new GridBagConstraints(x, y, w, 1, (double) 1, wy,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
                5, 5, 5, 5), 0, 0));
    }

    /**
     * Synchronizes two checkbox lists based on the first given list.
     * <p>
     * <p>
   * All elements selected in the first list will also be enabled in the second list, all elements
   * selected in the second list which are
     * <em>not</em>
     * selected in the first list will be deselected. </p>
     *
     * @param list1 the first list (base).
     * @param list2 the second list (target).
     */
    private void synchronizeSelection
    (CheckBoxList<SyncProjectWrapper> list1, CheckBoxList<SyncProjectWrapper> list2) {
        if (syncingLists) {
            return;
        }
        syncingLists = true;
        for (SyncProjectWrapper entry : list1.getCheckBoxListSelectedValues()) {
            list2.addCheckBoxListSelectedValue(entry, true);
        }

        for (SyncProjectWrapper entry : list2.getCheckBoxListSelectedValues()) {
            if (modelContains(list1.getModel(), entry)
                    && !list1.getCheckBoxListSelectedValues().contains(entry)) {
                list2.removeCheckBoxListSelectedValue(entry, false);
            }
        }
        syncingLists = false;
    }

    private void updateLabels() {
        String entryCountString = "?";

        // init the progress bar in the footer
        int counter = 0;
        for (Entry<Key, SyncProjectMapper> entry : mapping.entrySet()) {
            final int c = counter++;

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    // update the progress bar
                    Footer.setProgressBarValue((c / (mapping.size() * 1.0)) * 100);

                }
            });

            // updating the entry
            SyncProjectMapper map = entry.getValue();
            if (map.getLocal() != null) {
                try {
                    ProjectDataSet project = map.getLocal().projectDataSet;
                    String owner = mainFrame.getController().getDisplayName(project.getProjectOwnerId());
                    int entryCount = -1;
                    try {
                        entryCount = mainFrame.getController().getEntryCount(project);
                    } catch (EntriesException | NoRightException ex) {
                        LOGGER.error(ex, ex);
                    }
                    map.getLocal().setDisplayText("<html>"
                            + getRowTextPrefix(project, owner, entryCount, true)
                            + "<b>" + project.getProjectName() + "</b>"
                            + getRowTextPostfix(project, owner, entryCount, true));

                    int unsynchedEntryCount = -1;
                    try {
            unsynchedEntryCount = abstractMainFrame.getController()
                .getNumberOfUncommittedEntries(project);
                    } catch (EntriesException | NoRightException | NotLoggedInException | StatementNotExecutedException ex) {
                        Logger.getLogger(Synchronisation.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (map.getGlobal() != null) {
            getGlobalLabel(map.global.getProjectInformation());
                        int globalCount = map.getGlobal().getCountElements();
                        if (unsynchedEntryCount != 0 || entryCount - unsynchedEntryCount != globalCount) {
                            entryCountString = " <font color=red><b>[ DIFF ]</b></font>";
                        } else {

              //TODO Check if last update is equal, global data for this would be already available!
                            entryCountString = " <font color=green>[ OK ]</font>";
                        }
                        map.getLocal().setTooltipText(
                                createLocalTooltipText(project, owner, entryCount, globalCount, unsynchedEntryCount,
                                        entryCountString));
                        map.getGlobal().setTooltipText(
                                createLocalTooltipText(project, owner, entryCount, globalCount, unsynchedEntryCount,
                                        entryCountString));
                    } else {
                        entryCountString = " <font color=orange><b>[ UNSYNCED ]</b></font>";
                        map.getLocal().setTooltipText(
                                createLocalTooltipText(project, owner, entryCount, null, unsynchedEntryCount,
                                        entryCountString));
                    }

                    map.getLocal().setDisplayText(map.getLocal().getDisplayText() + entryCountString);
                } catch (NotLoggedInException | StatementNotExecutedException | NotConnectedException | IOException ex) {
                    Logger.getLogger(Synchronisation.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {

          ProjectInformation projectInformation = map.getGlobal().getProjectInformation();
          getGlobalLabel(projectInformation);
          final ProjectDataSet project = projectInformation.getProject();
                    String owner = mainFrame.getController().getDisplayName(project.getProjectOwnerId());
                    int globalCount = map.getGlobal().getCountElements();
                    map.getGlobal().setTooltipText(createGlobalTooltipText(project, owner, globalCount));
                } catch (NotLoggedInException | StatementNotExecutedException | NotConnectedException | IOException ex) {
                    Logger.getLogger(Synchronisation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    revalidate();
                    repaint();
                }
            });
        }

    }

    /**
   * The text that is appended to the project name of the row. Can be overwritten to insert
   * book-specfic information.
     */
  protected String getRowTextPostfix(ProjectDataSet project, String owner, int entryCount,
      boolean isAvailableLocal) {
        return " [ " + entryCount + " ] <font color=#AAAAAA>" + owner + "</font>";
    }

    /**
     * The text that is appended before the project name of the row. Can be overwritten to insert book-specfic
     * information.
     *
     * @param project The project to load the text for
     * @param owner The Owner of the Project
     * @param entryCount the number of entries
     * @param isAvailableLocal indicates if the project is available local
     * @return the text
     */
  protected String getRowTextPrefix(ProjectDataSet project, String owner, int entryCount,
      boolean isAvailableLocal) {
        return "";
    }

  private void getGlobalLabel(ProjectInformation projectInformation) throws
            NotLoggedInException, StatementNotExecutedException, NotConnectedException, IOException {
    final ProjectDataSet project = projectInformation.getProject();
        SyncProjectMapper mapper = mapping.get(project.getProjectKey());
        boolean isAvailableLocal = mapper.getLocal() != null;

        String owner = mainFrame.getController().getDisplayName(project.getProjectOwnerId());

    int entryCount = projectInformation.getNumberOfEntries();
    entryCount--;//remove "project";

//        Message message = new Message(Commands.REQUEST_NUMBER_OF_ENTRIES);
//        message.getData().add(project.getProjectKey());
//        SerialisableArrayList<SerialisableString> list = new SerialisableArrayList<>();
//        for (IBaseManager iBase : (abstractMainFrame.getController()).getLocalManager().getSyncTables()) {
//            list.add(new SerialisableString(iBase.getTableName()));
//        }
//        message.getData().add(list);
//        Message returnMessage = ((AbstractSynchronisationController) abstractMainFrame.getController()).sendMessage(
//                message);
//        if (returnMessage.getResult().wasSuccessful()) {
//            for (Serialisable s : returnMessage.getData()) {
//                SerialisableArrayList returnList = (SerialisableArrayList) s;
//                int i = 0;
//                for (Object data : returnList.getData()) {
//                    SerialisableInt sInt = (SerialisableInt) data;
//                    entryCount += sInt.getValue();
//                    //TODO maybe display list in tooltip?
//                    //System.out.println("manager: "+list.getData().get(i++)+" amount: "+sInt.getValue());
//                }
//            }
//        }
        StringBuilder label = new StringBuilder();
        label.append("<html><body>");
        label.append(getRowTextPrefix(project, owner, entryCount, isAvailableLocal));
        if (isAvailableLocal) {
            label.append("<b>");
        }
        label.append(project.toString());
        if (isAvailableLocal) {
            label.append("</b>");
        }
        mapper.global.setCountElements(entryCount);
    label.append(getRowTextPostfix(project, owner, entryCount, isAvailableLocal))
        .append("</body></html>");
        mapper.global.setDisplayText(label.toString());
    }

    /**
   * Utility function to check if an element exists in an array. This function has a linear runtime
   * behavior.
     *
     * @param array the array to search through.
     * @param value the value to search for.
     * @return <code>true</code> if the array contains the element, * * else
     * <code>false</code>.
     */
    private static boolean arrayContains(Object[] array, Object value) {
        for (Object entry : array) {
            if (entry.equals(value)) {
                return true;
            }
        }
        return false;
    }

    /**
   * Utility function to check if an element exists in a list model. This function has a linear
   * runtime behavior.
     *
     * @param model the list model to search through.
     * @param value the value to search for.
     * @return <code>true</code> if the array contains the element, * * else
     * <code>false</code>.
     */
    private static boolean modelContains(ListModel model, Object value) {
        for (int i = 0; i < model.getSize(); ++i) {
            if (model.getElementAt(i).equals(value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Logs a default message to the information text area.
     *
     * @param message the message to log.
     */
    private void logMessage(String message) {
        logMessage(message, LogType.DEFAULT);
    }

    /**
     * Logs a message to the information text area.
     *
     * @param message the message to log.
     * @param type    the message type, only results in a different color.
     */
    private void logMessage(String message, LogType type) {
        if (message == null) {
            return;
        }
        // Set color depending on the type.
        SimpleAttributeSet style = new SimpleAttributeSet();
        switch (type) {
            case CONFIRMATION:
                StyleConstants.setBold(style, true);
                StyleConstants.setForeground(style, new Color(0, 211, 41)); // green
                break;
            case WARNING:
                StyleConstants.setBold(style, true);
                StyleConstants.setForeground(style, new Color(255, 128, 0)); // orange
                break;
            case ERROR:
                StyleConstants.setBold(style, true);
                StyleConstants.setForeground(style, Color.RED);
                break;
            case SYSTEM:
                StyleConstants.setBold(style, false);
                StyleConstants.setForeground(style, new Color(160, 160, 160)); // gray
                break;
            case DEFAULT:
            default:
                StyleConstants.setBold(style, false);
                StyleConstants.setForeground(style, Color.BLACK);
                break;
        }

        // Append the text with timestamp.
        try {
            Document document = output.getDocument();
            document.insertString(document.getLength(),
                    String.format("[%s] %s\n",
                            DateFormat.getTimeInstance().format(new Date()),
                            message),
                    style);
            output.setCaretPosition(document.getLength());
        } catch (BadLocationException ignored) {
        }
    }

    /**
     * Used to listen to updates of the synchronization process.
     * <p>
     * <p>
     * {@inheritDoc} </p>
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (!evt.getPropertyName().equals(
        XBookConfiguration.PROPERTY_PROGRESS) || !(evt
        .getNewValue() instanceof SynchronisationProgress)) {
            return;
        }
    final EntriesSynchronisationProgress progress = (EntriesSynchronisationProgress) evt
        .getNewValue();
        switch (progress.getType()) {
            case STARTED:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        logMessage(progress.getMessage(), LogType.SYSTEM);
                        setOverallLabel();
                        setProjectLabel();
                        setDatasetLabel();
                    }
                });
                break;
            case WARNING:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        logMessage(progress.getMessage(), LogType.WARNING);
                    }
                });
                break;
            case ERROR:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        logMessage(progress.getMessage(), LogType.ERROR);

                        setOverallLabel("<b><font color=red>(" + Loc.get("ERRORED") + ")</font></b>");
                        setProjectLabel();
                        setDatasetLabel();

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                updateProjectLists();
                                for (Component c : disableWhileSynchronizing) {
                                    c.setEnabled(true);
                                }
                            }
                        }).start();
                        stopSyncing();
                        contentWrapper.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    }
                });
                break;
            case COMPLETE:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        logMessage(progress.getMessage(), LogType.CONFIRMATION);

                        setOverallLabel("<b><font color=green>(" + Loc.get("COMPLETED") + ")</font></b>");
                        setProjectLabel();
                        setDatasetLabel();
                        progressOverall.setValue(progressOverall.getMaximum());
                        progressProject.setValue(progressProject.getMaximum());
                        progressDataset.setValue(progressDataset.getMaximum());
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                updateProjectLists();
                                for (Component c : disableWhileSynchronizing) {
                                    c.setEnabled(true);
                                }
                            }
                        }).start();
                        stopSyncing();
                        contentWrapper.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    }
                });
                break;
            case INTERRUPTED:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        logMessage(progress.getMessage(), LogType.WARNING);

                        setOverallLabel("<b><font color=orange>(" + Loc.get("INTERRUPTED") + ")</font></b>");
                        setProjectLabel();
                        setDatasetLabel();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                updateProjectLists();
                                for (Component c : disableWhileSynchronizing) {
                                    c.setEnabled(true);
                                }
                            }
                        }).start();
                        stopSyncing();
                        contentWrapper.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    }
                });
                break;
            case START_NEW_PROJECT:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        logMessage(progress.getMessage());
                        setOverallLabel(progress.getCurrentEntryCount(), progress.getMaxEntryCount());
                        setProjectLabel();
                        setDatasetLabel();
                    }
                });
                break;
            case NEXT_STEP:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        logMessage(progress.getMessage());
                        setProjectLabel(progress.getCurrentEntryCount(), progress.getMaxEntryCount());
                        setDatasetLabel();
                    }
                });
                break;
            case UPDATE:
            default:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        logMessage(progress.getMessage());
                        setDatasetLabel(progress.getCurrentEntryCount(), progress.getMaxEntryCount());
                    }
                });
                break;
        }
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel buttonPanel = new ButtonPanel();

        final PropertyChangeListener observer = this;

        // create, setup and add sync button
        reloadButton = buttonPanel.addImageButton(Position.NORTH_WEST, null, Loc.get("RELOAD"),
                Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new Thread(new Runnable(){
                            @Override
                            public void run() {
                                updateProjectLists();
                            }
                        }).start();
                    }
                });
        reloadButton.setLabel(Loc.get("RELOAD"));
        reloadButton.setFocusable(false);
        reloadButton.setStyle(XImageButton.Style.GRAY);

        // create, setup and add sync button
    syncButton = buttonPanel
        .addImageButton(Position.NORTH_EAST, Images.BUTTONPANEL_SYNC, Loc.get("SYNCHRONISE"),
                Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        onSyncButtonClicked(observer);
                    }
                });
        syncButton.setFocusable(false);

        if (isFilteringEnabled()) {
            filterButton = buttonPanel.addImageButton(Position.NORTH_CENTER, null, Loc.get("FILTER"),
                    new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            openFilter();
                        }
                    });
            filterButton.setLabel(Loc.get("FILTER"));
            filterButton.setPreferredSize(new Dimension(130, filterButton.getPreferredSize().height));

      filterRevertButton = buttonPanel
          .addImageButton(Position.NORTH_CENTER, null, Loc.get("RESET_FILTER"),
                    new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            resetFilter();
                        }
                    });
            filterRevertButton.setLabel("X");
            filterRevertButton.setStyle(XImageButton.Style.GRAY);
        }

        return buttonPanel;
    }

    private void resetFilter() {
        syncFilter.reset();
        new Thread(new Runnable(){
            @Override
            public void run() {
                updateProjectLists();
            }
        }).start();    }

    private void onSyncButtonClicked(PropertyChangeListener observer) {
        if (isRunning) {
            syncButton.setEnabled(false);
      ((AbstractSynchronisationController) abstractMainFrame.getController()).getSynchroniser()
          .interruptSync();
        } else {
            if (areProjectsSelected()) {
                try {
                    output.setText("");
                    for (Component c : disableWhileSynchronizing) {
                        c.setEnabled(false);
                    }
                    contentWrapper.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    synchronizer = new ProjectSynchronisation(
                            ((AbstractSynchronisationController) abstractMainFrame.getController()),
                            getSelectedProjects());
                    synchronizer.addPropertyChangeListener(observer);
                    synchronizer.execute();
                    syncButton.setIcon(Images.BUTTONPANEL_CANCEL, Loc.get("INTERRUPT"));
                    isRunning = true;
                } catch (Exception ex) {
                    Logger.getLogger(Synchronisation.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                logMessage(Loc.get("NO_PROJECTS_ARE_SELECTED"), LogType.WARNING);
            }
        }
    }

    private void stopSyncing() {
        syncButton.setIcon(Images.BUTTONPANEL_SYNC, Loc.get("SYNCHRONISE"));
        syncButton.setEnabled(true);
        isRunning = false;
    }

    @Override
    public SidebarPanel getSideBar() {
        if (sidebar == null) {
            sidebar = new SidebarPanel();
            sidebar.addTitle(Loc.get("SYNCHRONISATION"));
            sidebar.addTextBlock(Loc.get("SIDEBAR_SYNCHRONISATION"));
        }
        return sidebar;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    @Override
    public void updateContent() {
        super.updateContent();
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
          if (((AbstractSynchronisationController) mainFrame.getController()).getLocalManager()
              != null && ((AbstractSynchronisationController) mainFrame.getController())
              .isServerConnected() && ((AbstractSynchronisationController) mainFrame
              .getController()).isLoggedInGlobal()) {
                        boolean syncAllowed = updateProjectLists();
                        contentWrapper.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        if (!syncAllowed) {
                            buttonSynchronize.setEnabled(false);
                            logMessage(Loc.get("FEATURE_SYNC_CURRENTLY_NOT_AVAILABLE_DUE_TO_MAINTENANCE_WORK"));
                        } else {
                            if (!loadedBefore) {
                                logMessage(Loc.get("SELECT_THE_PROJECTS_YOU_WISH_TO_SYNCHRONISE"));
                                loadedBefore = true;
                            }
                        }
                        setConnectionAvailable(true);
                    } else {
                        contentWrapper.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        logMessage(Loc.get("ERROR_WHILE_CONNECTING_TO_GLOBAL_DATABASE"), LogType.ERROR);
                        setConnectionAvailable(false);
                    }

                } catch (NotLoggedInException ex) {
                    Logger.getLogger(Synchronisation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }

  private void updateBar(JProgressBar progressBar, JPanel wrapper, String label,
      Integer countElement, Integer
            maxElements, String text) {
        DecimalFormat df = new DecimalFormat("0.0");

        StringBuilder sb = new StringBuilder();
        sb.append("<html><body>");
        sb.append("<b>");
        sb.append(label);
        sb.append("</b>");
        if (countElement != null && maxElements != null) {
            double percentage = countElement / (maxElements * 1.0);
            sb.append("<b>: </b>");
            sb.append(countElement);
            sb.append(" / ");
            sb.append(maxElements);
            sb.append(" (");
            sb.append(df.format(percentage * 100));
            sb.append("%)");

            progressBar.setMaximum(maxElements);
            progressBar.setValue(countElement);
        } else {
            progressBar.setValue(0);
        }
        if (text != null) {
            sb.append(" ");
            sb.append(text);
        }
        sb.append("</body><html>");
        wrapper.setBorder(BorderFactory.createTitledBorder(sb.toString()));
    }

    private void setOverallLabel(Integer countElement, Integer maxElements, String text) {
    updateBar(progressOverall, progressOverallWrapper, Loc.get("OVERALL_PROGRESS"), countElement,
        maxElements,
                text);
    }

    private void setOverallLabel(Integer countElement, Integer maxElements) {
        setOverallLabel(countElement, maxElements, null);
    }

    private void setOverallLabel(String text) {
        setOverallLabel(null, null, text);
    }

    private void setOverallLabel() {
        setOverallLabel(null, null, null);
    }

    private void setProjectLabel(Integer countElement, Integer maxElements, String text) {
    updateBar(progressProject, progressProjectWrapper, Loc.get("PROGRESS_CURRENT_PROJECT"),
        countElement,
                maxElements, text);
    }

    private void setProjectLabel(Integer countElement, Integer maxElements) {
        setProjectLabel(countElement, maxElements, null);
    }

    private void setProjectLabel() {
        setProjectLabel(null, null, null);
    }

    private void setDatasetLabel(Integer countElement, Integer maxElements, String text) {
    updateBar(progressDataset, progressDatasetWrapper, Loc.get("PROGRESS_CURRENT_DATA_SET"),
        countElement,
                maxElements, text);
    }

    private void setDatasetLabel(Integer countElement, Integer maxElements) {
        setDatasetLabel(countElement, maxElements, null);
    }

    private void setDatasetLabel() {
        setDatasetLabel(null, null, null);
    }

  protected String createLocalTooltipText(ProjectDataSet project, String projectOwner,
      Integer localEntryCount,
                                            Integer
                                                    globalEntryCount, Integer uncommittedEntryCount, String status) {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><body><b><u>").append(project.getProjectName()).append("</u></b><br>");
        sb.append("<b>").append(Loc.get("OWNER")).append(":</b> ").append(projectOwner).append("<br>");
    sb.append("<b>").append(Loc.get("LOCAL_ENTRY_COUNT")).append(":</b> ").append(localEntryCount)
        .append("<br>");
        if (globalEntryCount != null) {
      sb.append("<b>").append(Loc.get("GLOBAL_ENTRY_COUNT")).append(":</b> ")
          .append(globalEntryCount).append(
                    "<br>");
        }
        sb.append("<b>").append(Loc.get("UNCOMMITTED_ENTRY_COUNT")).append(":</b> ").append(
                uncommittedEntryCount).append("<br>");
        sb.append("<b>").append(Loc.get("STATUS")).append(":</b> ").append(status).append("<br>");
        if (status.contains("OK")) {
            sb.append("<font color=gray>").append(
          Loc.get("NOTE_THAT_THERE_MIGHT_BE_ENTRIES_THAT_CAN_BE_SYNCED_EVEN_IF_THE_STATUS_IS_OK"))
          .append(
                    "</font>");
        }
        return sb.toString();
    }

  protected String createGlobalTooltipText(ProjectDataSet project, String projectOwner,
      Integer globalEntryCount) {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><body><b><u>").append(project.getProjectName()).append("</u></b><br>");
        sb.append("<b>").append(Loc.get("OWNER")).append(":</b> ").append(projectOwner).append("<br>");
    sb.append("<b>").append(Loc.get("GLOBAL_ENTRY_COUNT")).append(":</b> ").append(globalEntryCount)
        .append("<br>");
        sb.append("<b>").append(Loc.get("STATUS")).append(":</b> ").append(
                "<b><font color=orange>[ UNSYNCED ]</font></b>").append("<br>");
    sb.append("<font color=gray>")
        .append(Loc.get("THIS_PROJECT_IS_NOT_SYNCED_TO_THE_LOCAL_DATABSE")).append(
                "</font>");
        return sb.toString();
    }

  public static class SyncProjectWrapperGlobal extends SyncProjectWrapper {


    private final ProjectInformation projectInformation;

    public SyncProjectWrapperGlobal(ProjectInformation projectInformation) {
      super(projectInformation.getProject());
      this.projectInformation = projectInformation;
    }

    public ProjectInformation getProjectInformation() {
      return projectInformation;
    }
  }

    public static class SyncProjectWrapper {

        private ProjectDataSet projectDataSet;
        private String displayText;
        private String tooltipText = "";
        private int countElements;

        public SyncProjectWrapper(ProjectDataSet projectDataSet) {
            this.projectDataSet = projectDataSet;
            this.displayText = projectDataSet.toString();
        }

        public ProjectDataSet getProjectDataSet() {
            return projectDataSet;
        }

        public void setDisplayText(String displayText) {
            this.displayText = displayText;
        }

        public String getDisplayText() {
            return displayText;
        }

        public void setCountElements(int countElements) {
            this.countElements = countElements;
        }

        public int getCountElements() {
            return countElements;
        }

        @Override
        public String toString() {
            return displayText;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof SyncProjectWrapper)) {
                return false;
            }
            SyncProjectWrapper wrapper = (SyncProjectWrapper) obj;
            return projectDataSet.equals(wrapper.projectDataSet);
        }

        public void setTooltipText(String tooltipText) {
            this.tooltipText = tooltipText;
        }

        public String getTooltip() {
            return tooltipText;
        }
    }

    public static class SyncProjectMapper {

        private SyncProjectWrapper local;
    private SyncProjectWrapperGlobal global;

    public SyncProjectWrapperGlobal getGlobal() {
            return global;
        }

        public SyncProjectWrapper getLocal() {
            return local;
        }

    public void setGlobal(SyncProjectWrapperGlobal global) {
            this.global = global;
        }

        public void setLocal(SyncProjectWrapper local) {
            this.local = local;
        }
    }

    private void openFilter() {
        mainFrame.displaySynchronisationFilterScreen(this);
    }

    protected boolean isSortingEnabled() {
        return false;
    }

    protected boolean isFilteringEnabled() {
        return false;
    }
}
