package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.TooManyDigitsException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IMinMaxFloatField;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.MathsHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * An float field with a label as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputMinMaxFloatField extends AbstractInputElement implements IMinMaxFloatField {

    private static final String DEFAULT_PREFIX_1 = "Min";
    private static final String DEFAULT_PREFIX_2 = "Max";
    private static final String DEFAULT_POSTFIX = "";

    private final ColumnType columnTypeMin;
    private final ColumnType columnTypeMax;
    private LabeledTextField minInputField;
    private LabeledTextField maxInputField;
    private Integer digitsAfterDecimalPoint;

    private String prefix1;
    private String prefix2;
    private String postfix;

    /**
     * Determines if the values should be switched if the second value is smaller than the first value, nor not. Is
     * checked on focus lost and is necessary for min-max or from-to fields, but not for e.g. coordinates x-y.
     */
    private boolean isValueSwitchingActive = true;

    /**
     * Determines if the value should be cloned to the second field, if the field loses the focus and one of the fields
     * is empty. Could be necessary to be able to enter values like "< 10" or "> 10" by leaving one field empty.
     */
    private boolean isAutoFillOfEmptyBoxesActive = true;

    /**
     * Constructor.
     *
     * @param columnTypeMin The ColumnType of the minimum input field.
     * @param columnTypeMax The ColumnType of the maximum input field.
     * @param title         The title that should be displayed as input field name.
     * @param postfix
     * @param prefix1
     * @param prefix2
     */
    public InputMinMaxFloatField(ColumnType columnTypeMin, ColumnType columnTypeMax, String title, String postfix,
                                 String prefix1, String prefix2) {
        super(new ColumnType(columnTypeMin, ColumnHelper.getTableName(columnTypeMin.getColumnName())).setDisplayName(title).setDisplayNameGeneral(title));

        this.columnTypeMin = columnTypeMin;
        this.columnTypeMax = columnTypeMax;
        this.titleString = title;
        this.prefix1 = prefix1;
        this.prefix2 = prefix2;
        this.postfix = postfix;
        setGridX(2);
    }

    /**
     * Constructor.
     *
     * @param columnTypeMin The ColumnType of the minimum input field.
     * @param columnTypeMax The ColumnType of the maximum input field.
     * @param title         The title that should be displayed as input field name.
     * @param postfix
     */
    public InputMinMaxFloatField(ColumnType columnTypeMin, ColumnType columnTypeMax, String title, String postfix) {
        this(columnTypeMin, columnTypeMax, title, postfix, DEFAULT_PREFIX_1, DEFAULT_PREFIX_2);
    }

    /**
     * Constructor.
     *
     * @param columnTypeMin The ColumnType of the minimum input field.
     * @param columnTypeMax The ColumnType of the maximum input field.
     * @param title         The title that should be displayed as input field name.
     */
    public InputMinMaxFloatField(ColumnType columnTypeMin, ColumnType columnTypeMax, String title) {
        this(columnTypeMin, columnTypeMax, title, DEFAULT_POSTFIX, DEFAULT_PREFIX_1, DEFAULT_PREFIX_2);
    }

    @Override
    public void actionOnFocusLost() {
        String inputMin = minInputField.getTextField().getText();
        String inputMax = maxInputField.getTextField().getText();

        if (inputMin.isEmpty() && inputMax.isEmpty()) {
            return;
        }

        if (isAutoFillOfEmptyBoxesActive) {
            if (inputMin.isEmpty() && !inputMax.isEmpty()) {
                minInputField.getTextField().setText(maxInputField.getTextField().getText());
            } else if (!inputMin.isEmpty() && inputMax.isEmpty()) {
                maxInputField.getTextField().setText(minInputField.getTextField().getText());
            }

            if (isValueSwitchingActive) {
                if (inputMin.isEmpty() && !inputMax.isEmpty()) {
                    minInputField.getTextField().setText(maxInputField.getTextField().getText());
                } else if (!inputMin.isEmpty() && inputMax.isEmpty()) {
                    maxInputField.getTextField().setText(minInputField.getTextField().getText());
                }

                // switch values if min is bigger that max
                float inputMinFloat = Float.parseFloat(minInputField.getTextField().getText());
                float inputMaxFloat = Float.parseFloat(maxInputField.getTextField().getText());
                if (inputMinFloat > inputMaxFloat) {
                    String min = "" + inputMaxFloat;
                    maxInputField.getTextField().setText("" + inputMinFloat);
                    minInputField.getTextField().setText(min);
                }
            }
        }

        super.actionOnFocusLost();
    }

    @Override
    public void actionOnFocusGain() {
        super.actionOnFocusGain();
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());

        String min = list.get(columnTypeMin);
        if (min != null && !min.equals("-1")) {
            try {
                min = "" + Float.parseFloat(min.replace(",", "."));
            } catch (NumberFormatException ex) {
                min = "-1";
            }
            minInputField.getTextField().setText(min);
        }

        String max = list.get(columnTypeMax);
        if (max != null && !max.equals("-1")) {
            try {
                max = "" + Float.parseFloat(max.replace(",", "."));
            } catch (NumberFormatException ex) {
                max = "-1";
            }
            maxInputField.getTextField().setText(max);
        }
    }

    @Override
    public void clear() {
        minInputField.getTextField().setText("");
        maxInputField.getTextField().setText("");
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        if (maxInputField.getTextField().getText().isEmpty()) {
            list.add(new DataColumn("-1", columnType.getColumnName()));
        } else {
            list.add(new DataColumn(getMaximumFloatAsString(), columnTypeMax.getColumnName()));
        }
        if (minInputField.getTextField().getText().isEmpty()) {
            list.add(new DataColumn("-1", columnType.getColumnName()));
        } else {
            list.add(new DataColumn(getMinimumFloatAsString(), columnTypeMin.getColumnName()));
        }
    }

    @Override
    public String getStringRepresentation() {
        return minInputField.getText() + "-" + maxInputField.getText();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(minInputField.getTextField());
        comp.add(maxInputField.getTextField());
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    public void setValueSwitchingActive(boolean b) {
        this.isValueSwitchingActive = b;
    }

    public void setAutoFillOfEmptyBoxesActive(boolean b) {
        this.isAutoFillOfEmptyBoxesActive = b;
    }

    @Override
    protected void createFieldInput() {
        JPanel grid = new JPanel(new GridLayout(1, 2));
        minInputField = new LabeledTextField(prefix1, postfix);
        minInputField.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 8));
        grid.add(minInputField);


        PlainDocument document = (PlainDocument) minInputField.getTextField().getDocument();
        document.setDocumentFilter(new InputFloatField.MyFloatFilter());


        maxInputField = new LabeledTextField(prefix2, postfix);
        maxInputField.setBorder(BorderFactory.createEmptyBorder(0, 8, 0, 0));
        grid.add(maxInputField);

        PlainDocument document2 = (PlainDocument) maxInputField.getTextField().getDocument();
        document2.setDocumentFilter(new InputFloatField.MyFloatFilter());

        minInputField.getTextField().addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                try {
                    String input = minInputField.getTextField().getText();
                    if (!input.isEmpty()) {
                        minInputField.getTextField().setText("" + MathsHelper.roundFloat(input,
                                digitsAfterDecimalPoint));
                    }
                } catch (NumberFormatException nfe) {
                    minInputField.getTextField().setText("");
                    minInputField.getTextField().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    Footer.displayError(Loc.get("NO_VALID_INPUT_ONLY_FLOAT_VALUES"));
                } catch (TooManyDigitsException r) {
                    maxInputField.getTextField().setText("");
                    maxInputField.getTextField().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    Footer.displayError(Loc.get("MORE_THAN_7_DIGITS_ARE_NOT_SUPPORTED"));
                }
            }

            @Override
            public void focusGained(FocusEvent e) {
                minInputField.getTextField().setBorder(new JTextField().getBorder());
            }
        });
        maxInputField.getTextField().addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                try {
                    String input = maxInputField.getTextField().getText();
                    if (!input.isEmpty()) {
                        maxInputField.getTextField().setText("" + MathsHelper.roundFloat(input,
                                digitsAfterDecimalPoint));
                    }
                } catch (NumberFormatException r) {
                    maxInputField.getTextField().setText("");
                    maxInputField.getTextField().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    Footer.displayError(Loc.get("NO_VALID_INPUT_ONLY_FLOAT_VALUES"));
                } catch (TooManyDigitsException r) {
                    maxInputField.getTextField().setText("");
                    maxInputField.getTextField().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    Footer.displayError(Loc.get("MORE_THAN_7_DIGITS_ARE_NOT_SUPPORTED"));
                }
            }

            @Override
            public void focusGained(FocusEvent e) {
                maxInputField.getTextField().setBorder(new JTextField().getBorder());
            }
        });

        setContent(grid);
    }

    // ========================================================================
    public AbstractInputElement setDigitsAfterDecimalPoint(Integer digitsAfterDecimalPoint) {
        this.digitsAfterDecimalPoint = digitsAfterDecimalPoint;
        return this;
    }

    @Override
    public void setMinimumFloat(Float number) {
        String value = "" + number;
        if (number == null || number == -1 || number == -1.0) {
            value = "";
        }
        minInputField.getTextField().setText(value);
    }

    @Override
    public void setMaximumFloat(Float number) {
        String value = "" + number;
        if (number == null || number == -1 || number == -1.0) {
            value = "";
        }
        maxInputField.getTextField().setText(value);
    }

    @Override
    public void setMinimumFloat(String number) {
        if (number.equals("-1")) {
            number = "";
        }
        try {
            number = "" + Float.parseFloat(number.replace(",", "."));
        } catch (NumberFormatException ex) {
            number = "";
        }
        minInputField.getTextField().setText(number);
    }

    @Override
    public void setMaximumFloat(String number) {
        if (number.equals("-1")) {
            number = "";
        }
        try {
            number = "" + Float.parseFloat(number.replace(",", "."));
        } catch (NumberFormatException ex) {
            number = "";
        }
        maxInputField.getTextField().setText(number);
    }

    @Override
    public String getMinimumFloatAsString() {
        return minInputField.getTextField().getText();
    }

    @Override
    public String getMaximumFloatAsString() {
        return maxInputField.getTextField().getText();
    }

    @Override
    public Float getMinimumFloat() {
        throw new UnsupportedOperationException("Not supported yet: Float getMinimumFloat()");
    }

    @Override
    public Float getMaximumFloat() {
        throw new UnsupportedOperationException("Not supported yet: Float getMaximumFloat()");
    }

    /**
     * Set the displayed value of the prefixes.
     *
     * @param prefix1 The new value of the first prefix.
     * @param prefix2 The new value of the second prefix.
     */
    public void setPrefixes(String prefix1, String prefix2) {
        this.prefix1 = prefix1;
        this.prefix2 = prefix2;
        if (minInputField != null) {
            minInputField.setPrefix(this.prefix1);
        }
        if (maxInputField != null) {
            maxInputField.setPrefix(this.prefix2);
        }
    }

    /**
     * Set the displayed value of the postfixes.
     *
     * @param postfix The new value of the postfixes.
     */
    public void setPostfix(String postfix) {
        this.postfix = postfix;
        if (minInputField != null) {
            minInputField.setPostfix(this.postfix);
        }
        if (maxInputField != null) {
            maxInputField.setPostfix(this.postfix);
        }
    }

    /**
     *
     */
    private class LabeledTextField extends JPanel {

        private JTextField textField;
        private JLabel prefixLabel;
        private JLabel postfixLabel;

        /**
         * Constructor.
         *
         * @param preLabel
         * @param postLabel
         */
        public LabeledTextField(String preLabel, String postLabel) {

            setLayout(new BorderLayout());
            setBackground(Colors.INPUT_FIELD_BACKGROUND);

            textField = new JTextField();
            textField.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
            textField.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent event) {
                    if (!(textField.getText().isEmpty())) {
                        try {
                            String input = textField.getText();
                            textField.setText(Float.parseFloat(input.replace(",", ".")) + "");
                        } catch (NumberFormatException e) {
                            textField.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                            textField.setText("");
                            Footer.displayWarning(Loc.get("NO_VALID_INPUT_ONLY_FLOAT_VALUES"));
                        }
                    }
                }

                @Override
                public void focusGained(FocusEvent e) {
                    textField.setBorder(new JTextField().getBorder());
                }
            });
            textField.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    if (textField.getText().isEmpty()) {
                        textField.setToolTipText(null);
                    } else {
                        textField.setToolTipText(textField.getText());
                    }
                }
            });
            add(BorderLayout.CENTER, textField);

            if (columnTypeMin.isMandatory() || columnTypeMax.isMandatory()) {
                colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
            } else {
                colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
            }

            this.prefixLabel = new JLabel(preLabel);
            this.prefixLabel.setBorder(BorderFactory.createEmptyBorder(0, 6, 0, 6));
            this.prefixLabel.setBackground(Colors.INPUT_FIELD_BACKGROUND);
            add(BorderLayout.WEST, this.prefixLabel);

            this.postfixLabel = new JLabel(postLabel);
            this.postfixLabel.setBorder(BorderFactory.createEmptyBorder(0, 6, 0, 6));
            this.postfixLabel.setBackground(Colors.INPUT_FIELD_BACKGROUND);
            add(BorderLayout.EAST, this.postfixLabel);
        }

        public JTextField getTextField() {
            return textField;
        }

        public String getText() {
            return textField.getText();
        }

        /**
         * Set the displayed value of the prefix.
         *
         * @param prefix The new value of the prefix.
         */
        public void setPrefix(String prefix) {
            prefixLabel.setText(prefix);
        }

        /**
         * Set the displayed value of the postfix.
         *
         * @param postfix The new value of the postfix.
         */
        public void setPostfix(String postfix) {
            postfixLabel.setText(postfix);
        }
    }
}
