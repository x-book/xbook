package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts;

import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiTextField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.IMultiTextField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputMultiTextField extends AbstractInputElement implements IMultiTextField {

    protected MultiTextField multiTextField;
    private final String table;

    public InputMultiTextField(ColumnType columnType, String table) {
        super(columnType);
        this.table = table;
        setGridX(1);
        setGridY(2);
    }

    @Override
    public void load(DataSetOld data) {
        DataTableOld dataTable = data.getOrCreateDataTable(table);
        for (DataRow list : dataTable) {
            final String s = list.get(columnType);
            if (s != null) {
                multiTextField.addItemToList(s);
            }
        }
    }

    @Override
    public void clear() {
        multiTextField.clearInput();
        multiTextField.removeAllItems();
    }

    @Override
    public void save(DataSetOld data) {
        DataTableOld list = data.getOrCreateDataTable(table);
        if (list == null) {
            list = data.addDataTable(new DataTableOld(table));
        }
        for (String s : multiTextField.getListItems()) {
            if (s != null) {
                DataRow row = new DataRow(table);
                row.add(new DataColumn(s, columnType));
                list.add(row);
            }
        }
    }

    /**
     * Return an arraylist of all selected animal IDs.
     *
     * @return All selected animal IDs.
     */
    @Override
    public String getStringRepresentation() {
        String s = "";
        for (String items : multiTextField.getListItems()) {
            s += items + ", ";
        }
        if (!s.isEmpty()) {
            s = s.substring(0, s.length() - ", ".length());
        }
        return s;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.addAll(multiTextField.getFocusableComponents());
        comp.add(multiTextField);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    @Override
    public void actionOnFocusLost() {
        multiTextField.addInputToList();
        super.actionOnFocusLost();
    }


    @Override
    protected void createFieldInput() {
        if (mode == Mode.STACK) {
            setPreferredSize(new Dimension(Sizes.STACK_DEFAULT_WIDTH, 125));
        }
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        multiTextField = new MultiTextField(columnType.getDisplayName());
        setContent(multiPanel);
        multiPanel.add(BorderLayout.CENTER, multiTextField);

        multiTextField.setSelectAllAndRemoveAllButtonVisible(false);
        multiTextField.getTextField().setPreferredSize(new Dimension(1, Sizes.INPUT_FIELD_HEIGHT));
        multiTextField.getListScroller().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        if (multiTextField.getListScroller().getMouseWheelListeners().length > 1) {
            multiTextField.getListScroller().removeMouseWheelListener(multiTextField.getListScroller().getMouseWheelListeners()[0]);
        }
        multiTextField.getList().setBackground(Color.WHITE);
    }

    @Override
    public boolean addText(String text) {
        return multiTextField.addItemToList(text);
    }

    @Override
    public boolean addText(List<String> text) {
        return multiTextField.addItemsToList(text);
    }

    @Override
    public List<String> getTextFromList() {
        return multiTextField.getListItems();
    }

    @Override
    public String getTextFromInput() {
        return multiTextField.getTextField().getText();
    }

    @Override
    public void removeText(String text) {
        multiTextField.removeItem(text);
    }

    @Override
    public void removeText(List<String> text) {
        multiTextField.removeItems(text);
    }

    @Override
    public void removeText(int index) {
        multiTextField.removeItem(index);
    }
}
