package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.database_viewer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 28.09.2017.
 */
public class TableView extends JSplitPane {
    private DatabaseViewer databaseViewer;
    private String tableName;
    private Connection connection;
    private JTable results;
    private JTextArea filterArea;

    private boolean showAll = false;

    String filter = "";
    public TableView(DatabaseViewer databaseViewer, String tableName, Connection connection) {
        super(VERTICAL_SPLIT);
        this.databaseViewer = databaseViewer;
        this.tableName = tableName;
        this.connection = connection;


        JPanel settingsPanel = new JPanel();
        settingsPanel.setLayout(new BorderLayout());
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final JCheckBox show_all = new JCheckBox("Show all entries");
        buttonPanel.add(show_all);
        show_all.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showAll = show_all.isSelected();
                if(showAll){
                    updateData();
                }
            }
        });
        filterArea = new JTextArea();
        filterArea.setPreferredSize(new Dimension(1, 200));
        filterArea.setSize(new Dimension(1, 200));
        settingsPanel.add(filterArea, BorderLayout.CENTER);
        settingsPanel.add(buttonPanel, BorderLayout.NORTH);
        JButton applyFilter = new JButton("Apply Filter");
        applyFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                filter = filterArea.getText();
                updateData();
            }
        });
        buttonPanel.add(applyFilter);

        results = new JTable();
        results.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(results);

        setLeftComponent(settingsPanel);
        setRightComponent(scrollPane);
        updateData();
    }

    private void updateData() {
        String query = "SELECT * FROM " + tableName;
        DefaultTableModel model = new DefaultTableModel();

        if (!filter.isEmpty()) {
            query += " WHERE " + filter;
        }
        if (!showAll) {
            query += " LIMIT 1000";
        }
        try {
            Statement statement = connection.createStatement();
            databaseViewer.addLogText(query);
            ResultSet resultSet = statement.executeQuery(query);
            int columnCount = resultSet.getMetaData().getColumnCount();
            Object[] headlines = new Object[columnCount];
            for (int i = 0; i < columnCount; i++) {
                headlines[i] = resultSet.getMetaData().getColumnName(i + 1);
            }

            model.setColumnIdentifiers(headlines);
            while (resultSet.next()) {
                Object[] data = new Object[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    data[i] = resultSet.getObject(i + 1);
                }
                model.addRow(data);
            }

        } catch (SQLException e) {
            databaseViewer.addLogText(e.getLocalizedMessage());
        }
        results.setModel(model);
    }
}
