package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.filter;

import javax.swing.*;
import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class SyncFilterRowTextField extends AbstractSyncFilterRow<JTextField> {

    public SyncFilterRowTextField(String labelText) {
        super(labelText);
    }

    @Override
    protected JTextField createInput() {
        return new JTextField();
    }

    @Override
    public SyncFilterRowData getCurrentData() {
        ArrayList<String> list = new ArrayList<>();
        if (!input.getText().isEmpty()) {
            for (String s : input.getText().split(SEP)) {
                list.add(s);
            }
        }

        return new SyncFilterRowData(currentMode, list);
    }

    @Override
    public void reset() {
        input.setText("");
    }

}