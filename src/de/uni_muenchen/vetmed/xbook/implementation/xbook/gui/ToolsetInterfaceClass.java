package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>, Alex
 */
//public class ToolsetInterfaceClass implements IController<ProjectDataSet> {
//
//
//    protected final AbstractController<?, ?> controller;
//
//    public ToolsetInterfaceClass(final AbstractController<?, ?> controller) {
//        this.controller = controller;
//    }
//
//    @Override
//    public List<String> getImportantTableNames() {
//        try {
//            return controller.getSynchronisationTableNames();
//        } catch (NotLoggedInException ex) {
//            Logger.getLogger(AbstractMainFrame.class.getName()).log(Level.SEVERE, null, ex);
//            return new ArrayList<>();
//        }
//    }
//
//    @Override
//    public List<String> getAllTableNames() {
//        try {
//            Set<String> tablesSet = controller.getAllTables();
//            List<String> tablesList = new ArrayList<>();
//            tablesList.addAll(tablesSet);
//            return tablesList;
//        } catch (NotLoggedInException ex) {
//            Logger.getLogger(AbstractMainFrame.class.getName()).log(Level.SEVERE, null, ex);
//            return new ArrayList<>();
//        }
//    }
//
//    @Override
//    public List<SimpleEntry<Integer, List<SimpleEntry<String, ArrayList<String>>>>> getExtractedExportResult(String managerName, ProjectDataSet project, List<ColumnHeader> columns) {
//        try {
////                    ProjectDataSet b = controller.getCurrentProject();
//            for (ProjectDataSet projectDataSet : controller.getProjects()) {
//                if (((ProjectDataSet) project).getProjectKey().toString().contains(projectDataSet.getProjectKey().toString())) {
//                    controller.loadProject(projectDataSet, false);
//                    break;
//                }
//            }
//            ExportResult exportResult = controller.getDataForColumns(managerName);
////                    ArrayList<EntryKey> aa = exportResult.getEntryKeyList();
//            LinkedHashMap<EntryKey, ExportRow> exportResultEntries = exportResult.getEntries();
//            Set<Map.Entry<EntryKey, ExportRow>> exportResultEntriesSet = exportResultEntries.entrySet();
////                    TreeMap<ColumnType, String> ac = exportResult.getHeadlines();
////                    System.out.println("*** 1 ***");
////                    for (String values : ac.values()) {
////                        System.out.println(values);
////                    }
////                    System.out.println("***   ***");
////                    ArrayList<Key> ad = exportResult.getKeyList();
////                    HashMap<Key, ArrayList<Key>> ae = exportResult.getKeysSortedByProject();
//
////                    LinkedHashMap<EntryKey, ExportRow> exportResultMeasurementsEntries = new LinkedHashMap<>();
////                    Set<Map.Entry<EntryKey, ExportRow>> exportResultMeasurementsEntriesSet = exportResultMeasurementsEntries.entrySet();
//
//
//            LinkedHashMap<Integer, ArrayList<String>> additionalValues = new LinkedHashMap<>();
//            additionalValues = getAdditionalEntries(managerName, columns, additionalValues, project);
//
//            // extract key,value pair from Export Result for each Entry
//            List<SimpleEntry<Integer, List<SimpleEntry<String, ArrayList<String>>>>> keyValueList = new ArrayList<>();
//            int key = 0;
//            for (Map.Entry<EntryKey, ExportRow> mapEntryFromExportResult : exportResultEntriesSet) {
//                List<SimpleEntry<String, ArrayList<String>>> value = new ArrayList<>();
//                final Set<Map.Entry<ColumnType, ExportColumn>> exportRowEntrySet = mapEntryFromExportResult.getValue().entrySet();
//                for (Map.Entry<ColumnType, ExportColumn> mapEntryFromExportRow : exportRowEntrySet) {
//                    value.add(new SimpleEntry<>(mapEntryFromExportRow.getKey().getColumnName(), mapEntryFromExportRow.getValue().getData()));
//                }
//                if (!additionalValues.isEmpty()) {
//                    final ArrayList<String> additionalPerEntryValue = additionalValues.get(key);
//                    if (additionalPerEntryValue != null) {
//                        for (int i = 0; i < additionalPerEntryValue.size(); i++) {
//                            String[] a = additionalValues.get(key).get(i).split("\\|");
//                            List<String> strings = Arrays.asList(additionalValues.get(key).get(i).split("\\|"));
//                            String simpleEntryKey = strings.get(0);
//                            ArrayList<String> simpleyEntryValue = new ArrayList<>();
//                            simpleyEntryValue.add(strings.get(1));
//                            value.add(new SimpleEntry<>(simpleEntryKey, simpleyEntryValue));
//                        }
//                    }
//
//                }
//                keyValueList.add(new SimpleEntry<>(key, value));
//                key++;
//            }
//
//
//            return keyValueList;
////                    return controller.getAllTables();
//        } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException | EntriesException | NoRightException ex) {
//            Logger.getLogger(AbstractMainFrame.class.getName()).log(Level.SEVERE, null, ex);
//            return new ArrayList<>();
//        }
//    }
//
//    protected LinkedHashMap<Integer, ArrayList<String>> getAdditionalEntries(String managerName, List<ColumnHeader> measurementsBool, LinkedHashMap<Integer, ArrayList<String>> measurementsPerEntry, ProjectDataSet project) {
//        return new LinkedHashMap<>();
//    }
//
//
//    @Override
//    public String getProjectKey(ProjectDataSet project) {
//        if (project != null) {
//            return project.getProjectKey().toString();
//        }
//        return null;
//    }
//
//    @Override
//    public List<ColumnHeader> getColumnsForTable(String managerName) {
//        List<ColumnHeader> header = new ArrayList<>();
//        try {
//            for (ColumnType columnType : controller.getColumnsForTable(managerName)) {
//                if (!columnType.getColumnName().contains("MessageNumber")&&!columnType.getColumnName().contains("Status")&&!columnType.getColumnName().contains("Deleted")) {
//                    header.add(columnType.getColumnHeader());
//                }
//            }
//
//        } catch (NotLoggedInException ex) {
//            Logger.getLogger(AbstractMainFrame.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return header;
//    }
//
//    @Override
//    public List<ColumnHeader> getKeysForTable(String managerName) {
//        List<ColumnHeader> header = new ArrayList<>();
//        try {
//            for (ColumnType columnType : controller.getKeysForTable(managerName)) {
//                header.add(columnType.getColumnHeader());
//            }
//        } catch (NotLoggedInException ex) {
//            Logger.getLogger(AbstractMainFrame.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return header;
//    }
//
//    @Override
//    public List<ProjectDataSet> getProjects() {
//        try {
//            return controller.getProjects();
//        } catch (NotLoggedInException | StatementNotExecutedException ex) {
//            Logger.getLogger(AbstractMainFrame.class.getName()).log(Level.SEVERE, null, ex);
//            return new ArrayList<>();
//        }
//
//    }
//
//    @Override
//    public String getLocForKey(String key) {
//        return Loc.get(key);
//    }
//
//    // Alex edit:
////            @Override
////            public ArrayList columnConverter(ArrayList<ColumnHeader> headers, String managerName) {
////                try {
////                    ArrayList<ColumnType> columns = new ArrayList<>();
////                    AbstractSynchronisationManager m = ((AbstractController) controller).getLocalManager().getManagerForName(managerName);
////                    ColumnTypeList list = m.getDataColumns();
////                    for (ColumnHeader header : headers) {
////                        for (ColumnType c : list) {
////                            String nameColumnType = c.getColumnName();
////                            String nameHeader = header.getColumnName();
////                            if (nameColumnType.equals(nameHeader)) {
////                                columns.add(c);
////                            }
////                        }
////                    }
////                    return columns;
////                } catch (NotLoggedInException ex) {
////                    Logger.getLogger(AbstractMainFrame.class.getName()).log(Level.SEVERE, null, ex);
////                    return new ArrayList();
////                }
////            }
//
//    // Alex edit:
////            @Override
////            public DataList getDataForColumns(ArrayList<ColumnHeader> headers, String managerName, List<ProjectDataSet> projects) {
////                List<Key> key = new ArrayList<>();
////                for (ProjectDataSet project : projects) {
////                    key.add(project.getProjectKey());
////                }
////                try {
////                    // convert all ColumnHeader to ColumnType
////
////                    // check for complex columns
////                    ArrayList<ColumnType> columns = new ArrayList<>();
////                    for (ColumnHeader ch : headers) {
////                        if (ch.getColumnName().contains("animal")) {
////                            columns.addAll((ArrayList<ColumnType>) columnConverter(headers, "animalvalues"));
////                        } else {
////                            columns.addAll((ArrayList<ColumnType>) columnConverter(headers, managerName));
////                        }
////                    }
////                    return controller.getDataForColumns(key, columns, managerName);
////                } catch (NotLoggedInException | NoRightException | StatementNotExecutedException | NotLoadedException | EntriesException ex) {
////                    Logger.getLogger(AbstractMainFrame.class.getName()).log(Level.SEVERE, null, ex);
////                    return new DataList();
////                }
////            }
//
//    @Override
//    public boolean hasProjects() {
//        return true;
//    }
//
//    @Override
//    public Collection<? extends ColumnHeader> getAdditionalOutputColumns(String managerName, List<ProjectDataSet> projectDataSet, List<ColumnHeader> outputType, List<ColumnHeader> columns) {
//        return new ArrayList<>();
//    }
//}
