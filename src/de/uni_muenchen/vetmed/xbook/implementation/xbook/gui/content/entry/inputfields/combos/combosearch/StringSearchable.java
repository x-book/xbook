/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.combosearch;


import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.combosearch.Searchable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * Implementation of the Searchable interface that searches a List of String
 * objects.  *
 * This implementation searches only the beginning of the words, and is not be
 * optimized
 *
 * for very large Lists.  *
 * @author The Guy in the corner.
 *
 */
public class StringSearchable implements Searchable<String> {
    private List<String> terms = new ArrayList();
    /**
     *
     * Constructs a new object based upon the parameter terms.      *
     * @param terms The characteristics of terms to search.
     *
     */
    public StringSearchable(List terms) {
        this.terms.addAll(terms);
    }
    @Override
    public Collection search(String value) {
        List founds = new ArrayList();
        for (String s : terms) {
            if (s.indexOf(value) >= 0) { // indexOf for any occurence
                founds.add(s);
            }
        }
        return founds;
    }
/*    
    @Override
    public CollectionX search(String value, ) {
    List founds = new ArrayList();
    for (String s : terms) {
        if (s.indexOf(value) >= 0) { // indexOf for any occurence
            founds.add(s);
        }
    }
    return founds;
    }
*/    
}
