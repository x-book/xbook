package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.IEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IIconButton;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectEdit;

import javax.swing.*;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class InputIconButton extends InputButton implements IIconButton {

    private final Icon DEFAULT_ICON = Images.BUTTON_SELECTED_ICON;
    private final String DEFAULT_TEXT = Loc.get("INSERT");
    private final String DEFAULT_SELECTED_TEXT = Loc.get("EDIT");

    /**
     * Constructor.
     *
     * @param columnType The database column data of the corresponding input field.
     * @param entry      The corresponding Abstract entry object.
     */
    public InputIconButton(ColumnType columnType, IEntry entry) {
        super(columnType, entry);
    }

    /**
     * Constructor.
     *
     * @param columnType  The database column data of the corresponding input field.
     * @param projectEdit The corresponding project edit object.
     */
    public InputIconButton(ColumnType columnType, AbstractProjectEdit projectEdit) {
        super(columnType, projectEdit);
    }

    @Override
    public void clear() {
        super.clear();
        removeIcon();
    }

    // =======================================================================
    @Override
    public void setDefaultIcon() {
        button.setIcon(DEFAULT_ICON);
        button.setText(DEFAULT_SELECTED_TEXT);
    }

    @Override
    public void setIcon(Icon icon) {
        button.setIcon(icon);
        if (icon == null) {
            button.setText(DEFAULT_TEXT);
        } else {
            button.setText(DEFAULT_SELECTED_TEXT);
        }
    }

    @Override
    public void removeIcon() {
        setIcon(null);
    }

}
