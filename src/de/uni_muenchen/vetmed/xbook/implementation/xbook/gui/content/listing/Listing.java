package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportResult;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.table.CustomTableModel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.*;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel.Position;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarListEntries;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractInputUnitManager;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class Listing extends AbstractContent {

    public static JCheckBox checkShowDatabaseInfo = null;
    public static final int ENTRIES_PER_PAGE = 600;
    private final Object syncObj = new Object();
    int numbersOfThread = 0;
    protected XTable table;
    protected ExportResult data;
    private JLabel numberOfElements;
    private int numberOfChangedEntries = 0;
    protected XButton buttonUpdateList;
    private IBaseManager currentBaseEntryManager = null;
    private boolean stopFetchingData;
    private boolean dataBeingLoaded;
    private boolean reload;
    private JScrollPane sp2;
    protected JComboBox<IBaseManager> comboTable;
    private final HashMap<IBaseManager, Filter> filters = new HashMap<>();
    private final HashMap<IBaseManager, Filter> filtersScreenBackup = new HashMap<>();
    private XImageButton filterButton;
    private XImageButton filterRevertButton;
    protected XImageButton openButton;
    protected XImageButton deleteButton;

    /**
     * Constructor of the ListEntries class. Initializes the content of the
     * screen.
     */
    public Listing() {
        super(false);
        init();
        reload = true;

    }

    public Listing(BaseEntryManager manager) {
        super(false);
        init();
        setSelectedTable(manager);
    }

    public static void refreshColumnVisibility(XTable table) {

        boolean isDbInfoVisible = XBookConfiguration.getListingShowDBInfoProperty(mainFrame.getController().getPreferences());

        // hide or display ID, DATABASE_ID, PID, PDBID
        Integer index_id = null;
        Integer index_dbid = null;
        Integer index_pid = null;
        Integer index_pdbid = null;
        Integer index_msgnumber = null;
        TableColumnModelExt model = ((TableColumnModelExt) table.getColumnModel());
        java.util.List<TableColumn> list = model.getColumns(true);
        for (int i = 0; i < list.size(); i++) {
            // stop searching if all indices were found
            if (index_id != null && index_dbid != null && index_pid != null && index_pdbid != null && index_msgnumber != null) {
                break;
            }

            // look for column indices
            String columnName = list.get(i).getHeaderValue().toString();
            TableColumn column = list.get(i);
            if (!(column instanceof TableColumnExt)) {
                continue;
            }
            TableColumnExt extModel = ((TableColumnExt) column);
            if (index_id == null && columnName.equals(AbstractInputUnitManager.ID.getDisplayName())) {
                index_id = i;
                extModel.setVisible(isDbInfoVisible);
                continue;
            }
            if (index_dbid == null && columnName.equals(AbstractInputUnitManager.DATABASE_ID.getDisplayName())) {
                index_dbid = i;
                extModel.setVisible(isDbInfoVisible);
                continue;
            }
            if (index_pid == null && columnName.equals(AbstractInputUnitManager.PROJECT_ID.getDisplayName())) {
                index_pid = i;
                extModel.setVisible(isDbInfoVisible);
                continue;
            }
            if (index_pdbid == null && columnName.equals(AbstractInputUnitManager.PROJECT_DATABASE_ID.getDisplayName())) {
                index_pdbid = i;
                extModel.setVisible(isDbInfoVisible);
            }
            if (index_msgnumber == null && columnName.equals(AbstractInputUnitManager.MESSAGE_NUMBER.getDisplayName())) {
                index_msgnumber = i;
                extModel.setVisible(false);
            }
        }

    }

    @Override
    protected JPanel getContent() {
        JPanel wrapper = new JPanel(new BorderLayout());
        wrapper.setBackground(Colors.CONTENT_BACKGROUND);

        // /////////////////////////////////////////////////////////////
        // INITIALISATION OF TABLE
        // /////////////////////////////////////////////////////////////
        createTable();
        sp2 = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        sp2.setBackground(Colors.CONTENT_BACKGROUND);
        wrapper.add(BorderLayout.CENTER, sp2);

        // /////////////////////////////////////////////////////////////
        // BOTTOM LINE
        // /////////////////////////////////////////////////////////////
        JPanel settingsLine = new JPanel(new BorderLayout());
        settingsLine.setBackground(Colors.CONTENT_BACKGROUND);
        settingsLine.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        createTableSelection(settingsLine);

        // Update button
        JPanel buttonUpdateListWrapper = new JPanel();
        buttonUpdateListWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        buttonUpdateList = new XButton();
        buttonUpdateList.setPreferredSize(new Dimension(300, 40));
        updateUpdateListButton();
        buttonUpdateList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateData();
            }
        });
        buttonUpdateListWrapper.add(buttonUpdateList);
        settingsLine.add(BorderLayout.CENTER, buttonUpdateListWrapper);

        // Number of elements label
        numberOfElements = new JLabel("");
        numberOfElements.setBackground(Colors.CONTENT_BACKGROUND);
        numberOfElements.setHorizontalAlignment(SwingConstants.CENTER);
        settingsLine.add(BorderLayout.EAST, ComponentHelper.wrapComponent(numberOfElements, Colors.CONTENT_BACKGROUND, 0, 6, 0, 0));
        wrapper.add(BorderLayout.SOUTH, settingsLine);

        return wrapper;
    }

    protected void createTableSelection(JPanel settingsLine) {
        // Table selection
        JPanel tableSelectionWrapper = new JPanel(new BorderLayout());
        tableSelectionWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        tableSelectionWrapper.add(BorderLayout.WEST, new JLabel(Loc.get("TABLE") + ": "));
        ArrayList<BaseEntryManager> listTablesDisplay = new ArrayList<>();
        try {
            for (IBaseManager baseManager : ((AbstractController<?, ?>) mainFrame.getController()).getLocalManager().getSyncTables()) {
                if (baseManager instanceof BaseEntryManager) {
                    AbstractBaseEntryManager abm = (AbstractBaseEntryManager) baseManager;
                    listTablesDisplay.add(abm);
                }
            }
        } catch (NotLoggedInException ignored) {
        }
        IBaseManager[] comboTableData = listTablesDisplay.toArray(new IBaseManager[0]);
        comboTable = new XComboBox<>(comboTableData);
        comboTable.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                setSelectedTable(comboTable.getItemAt(comboTable.getSelectedIndex()));
            }
        });
        comboTable.setPreferredSize(new Dimension(130, 1));
        tableSelectionWrapper.add(BorderLayout.CENTER, comboTable);

        if (comboTableData.length > 1) {
            settingsLine.add(BorderLayout.WEST, tableSelectionWrapper);
        } else {
            JLabel emptylabel = new JLabel();
            emptylabel.setPreferredSize(new Dimension(170, 1));
            settingsLine.add(BorderLayout.WEST, emptylabel);
        }
    }

    /**
     * Fills the table with data.
     */
    private void fillTableWithData() {
        synchronized (syncObj) {
            numbersOfThread++;
            if (dataBeingLoaded) {
                try {
                    syncObj.wait();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Listing.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            stopFetchingData = false;
            dataBeingLoaded = true;

            createTable();
            table.setModel(new CustomTableModel(null, 0));
            data = new ExportResult();
        }
        try {
            int numberOfEntries = getNumberOfEntries();
            int offset = 0;
            boolean moreEntries = true;

            ArrayList<ColumnType> list = new ArrayList<>();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Footer.showProgressBar();
                }
            });

            while (moreEntries) { // only show specific columns. Empty list == all columns
                if (stopFetchingData) {
                    table.setModel(new CustomTableModel(null, 0));
                    break;
                }

                ExportResult res = getData(list, offset);
                if (res.getHeadlines().equals(data.getHeadlines())) {
                    data.addAll(res);
                    final CustomTableModel model = (CustomTableModel) table.getModel();
                    for (ArrayList<String> rows : res) {
                        final Vector<String> rowData = new Vector<>();
                        for (String value : rows) {
                            if (value == null || value.equals("null") || value.equals("-1") || value.equals("-1.0")) {
                                rowData.add("");
                            } else {
                                rowData.add(value);
                            }
                        }
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                model.addRow(rowData);
                            }
                        });

                    }

                } else {
                    data.addAll(res);
                    final CustomTableModel model = new CustomTableModel(data.getHeadlines().values().toArray(new String[0]), data.getNumberOfEntrys());
                    int row = 0;
                    for (ArrayList<String> rows : data) {
                        int column = 0;
                        for (String value : rows) {
                            if (value == null || value.equals("null") || value.equals("-1") || value.equals("-1.0")) {
                                model.setValueAt("", row, column);
                            } else {
                                model.setValueAt(value, row, column);
                            }
                            column++;
                        }
                        row++;
                    }
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            table.setModel(model);
                            applyFilter(true);
                            refreshColumnVisibility(table);
                        }
                    });
                }

                offset += ENTRIES_PER_PAGE;
                final double percent = offset * 100.0 / (double) numberOfEntries;
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        Footer.setProgressBarValue(percent);
                    }
                });
                if (offset > numberOfEntries) {
                    moreEntries = false;
                }
                updateNumberOfElementsLabel();

            }
        } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException | NoRightException ex) {
            Logger.getLogger(Listing.class.getName()).log(Level.SEVERE, null, ex);
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Footer.hideProgressBar();
            }
        });

        synchronized (syncObj) {
            dataBeingLoaded = false;
            syncObj.notifyAll();
            numbersOfThread--;
        }

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                table.adjustColumns();
                ((XTable.XTableSorter) table.getRowSorter()).sort();
                refreshColumnVisibility(table);
            }
        });

    }

    protected int getNumberOfEntries() throws NotLoadedException, StatementNotExecutedException, NotLoggedInException {
        IBaseManager manager = comboTable.getItemAt(comboTable.getSelectedIndex());
        ProjectDataSet project = mainFrame.getController().getCurrentProject();
        return manager.getNumberofEntrys(project.getProjectKey());
    }

    protected ExportResult getData(ArrayList<ColumnType> list, int offset) throws NotLoadedException, NotLoggedInException, NoRightException, StatementNotExecutedException {
        return ((BaseEntryManager) comboTable.getItemAt(comboTable.getSelectedIndex())).getEntriesForListing(mainFrame.getController().getCurrentProject(), list, ColumnType.ExportType.GENERAL, false, offset, ENTRIES_PER_PAGE, null);
    }

    private MouseListener getAndSetMouseListener() {
        return new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                if (event.getSource() == table) {
                    if (event.getClickCount() == 2) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                loadEntry();
                            }
                        }).start();

                    }
                }
            }
        };
    }

    protected void setButtonsEnabled(boolean status) {
        table.setEnabled(status);
        openButton.setEnabled(status);

    }

    protected void loadEntry() {
        try {
            setButtonsEnabled(false);
            if (table.getSelectedRowCount() > 1) {
                loadMultiEntry();
                return;
            }
            int rowId = table.getSelectedRow();
            if (rowId == -1) {
                Footer.displayWarning(Loc.get("NO_ENTRY_SELECTED"));
                return;
            }
            Key key = data.getKeyAt(table.getRowSorter().convertRowIndexToModel(rowId));
            try {
                mainFrame.getController().loadAndDisplayEntry(((BaseEntryManager) comboTable.getItemAt(comboTable.getSelectedIndex())), key, mainFrame.getController().hasWriteRights());
            } catch (NotLoggedInException | NoRightException | StatementNotExecutedException | NotLoadedException | EntriesException ex) {
                Logger.getLogger(Listing.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            setButtonsEnabled(true);
        }
    }

    private void loadMultiEntry() {
        ArrayList<Key> keys = new ArrayList<>();
        for (int i : table.getSelectedRows()) {
            keys.add(data.getKeyAt(table.getRowSorter().convertRowIndexToModel(i)));
        }

        try {
            mainFrame.getController().loadMultiEntries(((BaseEntryManager) comboTable.getItemAt(comboTable.getSelectedIndex())), keys);
        } catch (NotLoggedInException | NoRightException | StatementNotExecutedException e) {
            e.printStackTrace();
        }
    }

    private void deleteEntry() {
        Logger.getLogger(Listing.class.getName()).log(Level.SEVERE, "Test", "Test");
        int rowId = table.getSelectedRow();
        if (rowId == -1) {
            Footer.displayWarning(Loc.get("NO_ENTRY_SELECTED"));
            return;
        }
        Object[] options = {Loc.get("YES"), Loc.get("CANCEL")};
        int n = JOptionPane.showOptionDialog(this,
                Loc.get("ARE_YOU_SURE_YOU_WANT_TO_DELETE_THIS_ENTRY"),
                Loc.get("DELETE"),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null, options, options[1]);
        if (n == JOptionPane.YES_OPTION) {
            Key key = data.getKeyAt(table.getRowSorter().convertRowIndexToModel(rowId));
            try {
                mainFrame.getController().deleteEntry(((BaseEntryManager) comboTable.getItemAt(comboTable.getSelectedIndex())), key);
            } catch (NotLoggedInException | NoRightException | StatementNotExecutedException | EntriesException | NotLoadedException ex) {
                Logger.getLogger(Listing.class.getName()).log(Level.SEVERE, null, ex);
                Footer.displayError(Loc.get("ENTRY_COULD_NOT_BE_DELETED"));
            } catch (EntryInUseException e) {
                e.printStackTrace();
                Footer.displayWarning(Loc.get("ENTRY_IN_USE_COULT_NOT_BE_DELETED"));
            }
        }
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();
        deleteButton = panel.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_DELETE, Loc.get("DELETE"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteEntry();
            }
        });

        if (checkShowDatabaseInfo == null) {
            checkShowDatabaseInfo = new JCheckBox(Loc.get("SHOW_DB_INFO"), XBookConfiguration.getListingShowDBInfoProperty(mainFrame.getController().getPreferences()));
            checkShowDatabaseInfo.setBackground(Colors.CONTENT_BACKGROUND);
            checkShowDatabaseInfo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    XBookConfiguration.setListingShowDBInfoProperty(mainFrame.getController().getPreferences(), checkShowDatabaseInfo.isSelected());
                    refreshColumnVisibility(table);
                }
            });
        }
        panel.getButtonsLeftNorth().add(checkShowDatabaseInfo);

        filterButton = panel.addImageButton(Position.NORTH_CENTER, null, Loc.get("FILTER"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openFilter();
            }
        });
        filterButton.setLabel(Loc.get("FILTER"));
        filterButton.setPreferredSize(new Dimension(130, filterButton.getPreferredSize().height));

        filterRevertButton = panel.addImageButton(Position.NORTH_CENTER, null, Loc.get("RESET_FILTER"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetFilter();
            }
        });
        filterRevertButton.setLabel("X");
        filterRevertButton.setStyle(XImageButton.Style.GRAY);

        double widthFilterButton = (Sizes.MODIFIER_BIG_BUTTON - 1.0) / 2 + 1;
        panel.addImageButton(Position.NORTH_EAST, Images.NAVIGATION_ICON_EXPORT_FILTERED, "<html>" + Loc.get("EXPORT") + "<br>(" + Loc.get("FILTERED_DATA") + ")</html>", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        exportSelection();
                    }
                }).start();
            }
        });

        openButton = panel.addImageButton(Position.NORTH_EAST, Images.BUTTONPANEL_LOAD, Loc.get("OPEN"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        loadEntry();
                    }
                }).start();
            }
        });

        return panel;
    }

    protected void exportSelection() {

        Footer.startWorking();

        final HashMap<IBaseManager, ArrayList<ColumnType>> entries = new HashMap<>();
        final ApiControllerAccess controller = mainFrame.getController();
        try {
            final HashMap<IBaseManager, TreeSet<ColumnType>> availableExportEntries = controller.getAvailableExportEntries();
            final IBaseManager selectedItem = (IBaseManager) comboTable.getSelectedItem();
            entries.put(selectedItem, new ArrayList<>(availableExportEntries.get(selectedItem)));
            ArrayList<Key> keys = new ArrayList<>();
            final int rowCount = table.getRowCount();
            for (int row = 0; row < rowCount; row++) {
                keys.add(data.getKeyAt(table.getRowSorter().convertRowIndexToModel(row)));
            }
            controller.startExport(entries, keys);
        } catch (StatementNotExecutedException | NotLoggedInException | NoRightException | NotLoadedException e) {
            e.printStackTrace();
        } finally {
            Footer.stopWorking();
        }
    }

    private void openFilter() {
        doFilterBackup();
        if (filters.get(comboTable.getItemAt(comboTable.getSelectedIndex())) == null) {
            filters.put(comboTable.getItemAt(comboTable.getSelectedIndex()), new Filter(this));
        }
        Content.setContent(filters.get(comboTable.getItemAt(comboTable.getSelectedIndex())));
        filterButton.setStyle(XImageButton.Style.GREEN);
        filterButton.setLabel("<html><b>" + Loc.get("ACTIVE_FILTER") + "!</b></html>");
        filterRevertButton.setStyle(XImageButton.Style.RED);
    }

    public void resetFilter() {
        ((XTable.XRowFilter) (table.getRowFilter())).removeAllFilters();

        if (filters.get(comboTable.getItemAt(comboTable.getSelectedIndex())) != null) {
            filters.get(comboTable.getItemAt(comboTable.getSelectedIndex())).removeAllRows();
        }
        filters.remove(comboTable.getItemAt(comboTable.getSelectedIndex()));
        filtersScreenBackup.remove(comboTable.getItemAt(comboTable.getSelectedIndex()));

        addCustomFilters();
        ((XTable.XTableSorter) table.getRowSorter()).sort();

        filterButton.setStyle(XImageButton.Style.COLOR);
        filterButton.setLabel(Loc.get("FILTER"));
        filterRevertButton.setStyle(XImageButton.Style.GRAY);

        updateNumberOfElementsLabel();
    }

    /**
     * Custom Filters that want to be included should be added here
     */
    protected void addCustomFilters() {
        ((XTable.XTableSorter) table.getRowSorter()).sort();
    }

    protected void doFilterBackup() {
        if (filters.get(comboTable.getItemAt(comboTable.getSelectedIndex())) == null) {
            filtersScreenBackup.remove(comboTable.getItemAt(comboTable.getSelectedIndex()));
            return;
        }
        filtersScreenBackup.put(comboTable.getItemAt(comboTable.getSelectedIndex()), filters.get(comboTable.getItemAt(comboTable.getSelectedIndex())).makeACopy());
    }

    protected void loadFilterBackup() {
        filters.put(comboTable.getItemAt(comboTable.getSelectedIndex()), filtersScreenBackup.get(comboTable.getItemAt(comboTable.getSelectedIndex())));
        filtersScreenBackup.remove(comboTable.getItemAt(comboTable.getSelectedIndex()));
    }

    public void applyFilter(boolean doUpdate) {
        Filter f = filters.get(comboTable.getItemAt(comboTable.getSelectedIndex()));
        if (doUpdate) {

            XTable.XRowFilter xRowFilter = ((XTable.XRowFilter) (table.getRowFilter()));
            xRowFilter.removeAllFilters();

            if (f != null && !f.getAllFilterRows().isEmpty()) {
                ArrayList<AbstractFilterRow> filterRows = f.getAllFilterRows();

                f.updateRowCombos();

                // remove all filter managers that have been "virtually" deleted before
                // and reset the arraylist of the deleted rows
                ArrayList<FilterManager> gmdelete = f.getAllFilterManagerToRemove();
                // apply the filter managers
                for (AbstractFilterRow row : filterRows) {
                    if (!gmdelete.contains(row.getFilterManager())) {
                        xRowFilter.addFilterManager(row.getFilterManager());
                    }
                }
                // reset the "virtual" deleted rows again
                f.resetAllFilterManagerToRemove();
                f.updateAllRows();
                // update the count label
                updateNumberOfElementsLabel();
            }
        }

        if (f == null || f.getAllFilterRows().isEmpty()) {
            resetFilter();
            filterButton.setStyle(XImageButton.Style.COLOR);
            filterRevertButton.setStyle(XImageButton.Style.GRAY);
        } else {
            filterButton.setStyle(XImageButton.Style.GREEN);
            filterRevertButton.setStyle(XImageButton.Style.RED);
        }
        addCustomFilters();
    }

    public void setFilter(BaseEntryManager manager, Filter filter) {
        filters.put(manager, filter);
    }

    @Override
    public SidebarPanel getSideBar() {
        return new SidebarListEntries();
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    private void updateData() {
        stopFetchingData = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Footer.startWorking();

                fillTableWithData();

                numberOfChangedEntries = 0;
                updateUpdateListButton();

                applyFilter(true);

                Footer.stopWorking();
            }
        }).start();
    }

    @Override
    public void updateContent() {
        super.updateContent();
        if (reload) {
            updateData();
            reload = false;
        }
    }

    private void updateNumberOfElementsLabel() {
        int total = 0;
        int filtered = table.getRowCount();
        if (data != null) {
            total = data.getNumberOfEntrys();
        }

        if (filtered == total) {
            numberOfElements.setText(
                    "<html><style='text-align: right'><b>"
                            + Loc.get("XX_ENTRIES_IN_TOTAL", total)
                            + "</b></style></html>");
        } else {
            numberOfElements.setText("<html><style='text-align: right'><b>"
                    + Loc.get("XX_FILTERED_ENTRIES", filtered) + "</b><br>" +
                    "(" + Loc.get("XX_ENTRIES_IN_TOTAL", total) + ")" +
                    "</style></html>");
        }
    }

    /**
     * Used to indicate that _one_ new entry for update is available
     *
     * @param manager
     */
    public void addUpdatedEntry(BaseEntryManager manager) {
        addUpdatedEntry(manager, 1);
    }

    /**
     * Used to indicate that any number of entries for update is available
     *
     * @param manager
     * @param numberOfUpdatesToAdd The number of updates to add.
     */
    public void addUpdatedEntry(BaseEntryManager manager, int numberOfUpdatesToAdd) {
        if (comboTable.getItemAt(comboTable.getSelectedIndex()) == manager || comboTable.getSelectedIndex() == -1) {
            numberOfChangedEntries += numberOfUpdatesToAdd;
            updateUpdateListButton();
        }
    }

    protected void updateUpdateListButton() {
        buttonUpdateList.setText(
                "<html><center><b>" + Loc.get("UPDATE_LIST") + "</b><br />"
                        + "(" + Loc.get("UP_TO_XX_UPDATES_AVAILABLE", numberOfChangedEntries) + ")</center></html>");
        // buttonUpdateList.setEnabled(numberOfChangedEntries != 0);
        buttonUpdateList.setEnabled(true);
    }

    protected void createTable() {
        table = new XTable();
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        if (sp2 != null) {
            sp2.setViewportView(table);
        }
        table.addMouseListener(getAndSetMouseListener());
    }

    public void unload() {
        stopFetchingData = true;
        reload = true;
    }

    public void setSelectedTable(IBaseManager manager) {
        if (currentBaseEntryManager != manager) {
            currentBaseEntryManager = manager;
            comboTable.setSelectedItem(manager);
            updateData();

            if (filters.get(manager) == null) {
                filters.put(manager, new Filter(this));
            }
            applyFilter(true);
        }
    }

}
