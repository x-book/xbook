package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawFileUploadWithMultiCategorySelectionField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class InputFileUploadWithMultiCategorySelectionField extends AbstractDefaultInputElement {

    /**
     * The file upload field object.
     */
    protected RawFileUploadWithMultiCategorySelectionField zField;
    private final String tableCategory;
    /**
     * The file filter options.
     */
    protected ArrayList<FileNameExtensionFilter> fileFilter;
    /**
     * The corresponding column type for the information of the file extension.
     */
    private final ColumnType columnTypeExtension;
    /**
     * The corresponding column type for the information whether the file has to be synchronized or not.
     */
    protected ColumnType columnTypeFileMustBeSynced;
    /**
     * The corresponding column type for category information.
     */
    protected ColumnType columnTypeCategory;
    protected int[] displayedCategoryIds;

    /**
     * Constructor.
     *
     * @param columnTypeFileName         The corresponding column type for file name.
     * @param columnTypeExtension        The corresponding column type for file extension.
     * @param columnTypeFileMustBeSynced The corresponding column type for the information whether the file has to be
     *                                   synchronized or not.
     * @param columnTypeCategory         The corresponding column type for category information.
     * @param displayedCategoryIds       The IDs of the categories that should be selectable in the category selection.
     * @param fileFilter                 The file filter options.
     */
    public InputFileUploadWithMultiCategorySelectionField(ColumnType columnTypeFileName, ColumnType columnTypeExtension,
                                                          ColumnType columnTypeFileMustBeSynced, ColumnType columnTypeCategory,
                                                          String tableCategory, int[] displayedCategoryIds,
                                                          ArrayList<FileNameExtensionFilter> fileFilter) {
        super(columnTypeFileName);
        this.columnTypeExtension = columnTypeExtension;
        this.tableCategory = tableCategory;
        this.fileFilter = fileFilter;
        this.columnTypeFileMustBeSynced = columnTypeFileMustBeSynced;
        this.columnTypeCategory = columnTypeCategory;
        this.displayedCategoryIds = displayedCategoryIds;

        setGridX(3);
        setGridY(2);
    }

    /**
     * Returns the current file name of the input field.
     *
     * @return
     */
    public String getInput() {
        return zField.getText();
    }

    @Override
    public AbstractInputElementFunctions createInputField() {
        if (zField == null) {
            zField = new RawFileUploadWithMultiCategorySelectionField(apiControllerAccess, columnType, columnTypeExtension,
                    columnTypeFileMustBeSynced, columnTypeCategory, tableCategory, displayedCategoryIds, fileFilter);
        }

        try {
            zField.setCategoryData(mainFrame.getController().getHashedCodeTableEntries(columnTypeCategory));
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(InputFileUploadWithMultiCategorySelectionField.class.getName()).log(Level.SEVERE, null, ex);
        }

        return zField;
    }

    /**
     * Set the selected categoryId of the file to a new value;
     *
     * @param path The new categoryId to set.
     */
    public void setPath(String path) {
        zField.setText(path);
    }

    @Override
    public void actionOnFocusLost() {
        super.actionOnFocusLost();
        zField.actionOnFocusLost();
        if (!zField.isValidPath()) {
            clear();
            setErrorStyle();
        }
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        zField.getTextField().setEnabled(false);
    }

    @Override
    public void setEnabledForRights(boolean b) {
        super.setEnabledForRights(b);
        zField.getTextField().setEnabled(false);
    }

    /**
     * Select a row with a specific value selected or deselected.
     *
     * @param value    The value of the row that should be selected or deselected.
     * @param selected The selected status that the row should be set to.
     */
    public void setSelected(String value, boolean selected) {
        zField.setSelected(value, selected);
    }

    /**
     * Select a row with a specific value enabled or disabled.
     *
     * @param value   The value of the row that should be selected or deselected.
     * @param enabled The enabled status that the row should be set to.
     */
    public void setEnabled(String value, boolean enabled) {
        zField.setEnabled(value, enabled);
    }

    /**
     * Updates the count label with the new value.
     */
    public void updateCount() {
        zField.updateCount();
    }

}
