package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarProjectRights;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class UserRights extends AbstractRights {

    public UserRights() {
        super(RightMode.USER_TO_PROJECT);
    }

    @Override
    protected void save() throws NotSavedException {
        try {
            Message rs = ((AbstractSynchronisationController) mainFrame.getController()).saveNewUserRights(getCurrentValues());
            mainFrame.displayProjectUserRightsScreen();
            if (rs.getResult().wasSuccessful()) {
                Footer.displayConfirmation(Loc.get("USER_RIGHTS_SAVED_SUCCESSFULLY"));
            } else {
                Footer.displayError(Loc.get("ERROR_WHILE_SAVING_USER_RIGHTS"));
                Logger.getLogger(UserRights.class.getName()).log(Level.SEVERE, rs.getResult().getErrorMessage());
                throw new NotSavedException();
            }
        } catch (NotConnectedException | NotLoggedInException | IOException | NotLoadedException | StatementNotExecutedException | NoRightException ex) {
            Logger.getLogger(UserRights.class.getName()).log(Level.SEVERE, null, ex);
            throw new NotSavedException();
        }
    }

    @Override
    protected ArrayList<RightsInformation> load() {
        try {
            ((AbstractSynchronisationController) mainFrame.getController()).updateProjectRights();
            return mainFrame.getController().getNewUserRights();
        } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException | NoRightException | NotConnectedException | IOException ex) {
            Logger.getLogger(UserRights.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    @Override
    protected boolean shouldDisplayLine(RightsInformation rinfo) {
        try {
            int userId = mainFrame.getController().getUserID();
            return rinfo.getID() != userId;
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(AbstractRights.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public SidebarPanel getSideBar() {
        return new SidebarProjectRights(Loc.get("USER_RIGHTS"));
    }

    @Override
    protected String getAddButtonTooltip() {
        return Loc.get("ADD_USER");
    }

    @Override
    protected ArrayList<ColumnType> getListOfRights() throws NotLoggedInException {
        return apiControllerAccess.getAvailableRights();
    }

    @Override
    protected ActionListener getAddButtonListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.displayProjectUserRightsAddUserScreen();
            }
        };
    }

    @Override
    protected void reloadThisPanel() {
        mainFrame.displayProjectUserRightsScreen();
    }
}
