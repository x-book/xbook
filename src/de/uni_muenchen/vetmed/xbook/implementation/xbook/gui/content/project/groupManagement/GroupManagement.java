package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.groupManagement;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Group;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class GroupManagement extends AbstractContent {

    private ColumnGroup groupOverview;
    private ColumnUser userOverview;
    private ColumnRank rankOverview;

    public GroupManagement() {
        super(false);
        updateCodeTables();
        init();
    }

    public GroupManagement(Group groupId) {
        this();
        setSelectedGroup(groupId);
    }

    @Override
    protected JPanel getContent() {

        JPanel wrapper = new JPanel(new BorderLayout());
        wrapper.setBackground(Colors.CONTENT_BACKGROUND);

        wrapper.add(BorderLayout.NORTH, new XTitle(Loc.get("GROUP_MANAGEMENT")));

        JPanel panel = new JPanel(new GridLayout(1, 3, 50, 0));
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        panel.add(groupOverview = getGroupOverview());
        panel.add(userOverview = getUserOverview());
        panel.add(rankOverview = getRankOverview());

        wrapper.add(BorderLayout.CENTER, panel);
        return wrapper;
    }

    protected ColumnRank getRankOverview() {
        return new ColumnRank(this, (AbstractMainFrame) mainFrame);
    }

    protected ColumnUser getUserOverview() {
        return new ColumnUser(this, (AbstractMainFrame) mainFrame);
    }

    protected ColumnGroup getGroupOverview() {
        return new ColumnGroup(this, (AbstractMainFrame) mainFrame);
    }

    @Override
    public void updateContent() {
        final AbstractSynchronisationController controller = ((AbstractSynchronisationController) mainFrame.getController());
        boolean isLoggedInGlobal = controller.isLoggedInGlobal();
        groupOverview.setVisible(isLoggedInGlobal);
        userOverview.setVisible(isLoggedInGlobal);
        rankOverview.setVisible(isLoggedInGlobal);
    }

    @Override
    public ButtonPanel getButtonBar() {
        return null;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    @Override
    public void setFocus() {
    }

    @Override
    public void actionOnDisconnect() {
    }

    public Group getSelectedGroup() {
        return groupOverview.getSelectedGroup();
    }

    protected void updateUserPanel() {
        userOverview.update();
    }

    protected void updateRankPanel() {
        rankOverview.update();
    }

    private void setSelectedGroup(Group groupId) {
        groupOverview.setSelectedGroup(groupId);
    }

    public void updateCodeTables() {
        try {
            ((AbstractSynchronisationController) mainFrame.getController()).updateGroup();
            ((AbstractSynchronisationController) mainFrame.getController()).updateGroupRights();
            ((AbstractSynchronisationController) mainFrame.getController()).updateGroupUser();
        } catch (NotLoggedInException | NotConnectedException | IOException | StatementNotExecutedException ex) {
            Logger.getLogger(GroupManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
