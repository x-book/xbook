package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.specific_reuseable;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An input field that allows entering the inventory number in ArchaeoBook/InBook.
 * <p/>
 * The inventory number is divided up into four strings and has the format XXXX_XXXXXXX_XX_XX. The strings are separated
 * by an underscore character.
 * <p/>
 * The first string might be a year number or a pre-defined inventory prefix. The second string is a numeric value. The
 * third string is an alphabetical value (in general "a", "b", ..). The last string is a numeric value again.
 *
 * @param <T> The controller.
 * @author Daniel Kaltenthaler <kaltenthaler@cesonia.io>
 */
public class InputABIBInventoryNumber<T extends AbstractController> extends AbstractInputElement {

    /**
     * The JTextField object for the first textfield.
     */
    private JTextField textField1;
    /**
     * The JTextField object for the second textfield.
     */
    private JTextField textField2;
    /**
     * The JTextField object for the third textfield.
     */
    private JTextField textField3;
    /**
     * The JTextField object for the fourth textfield.
     */
    private JTextField textField4;
    /**
     * The JLabel holding the preview of the inventory number.
     */
    private JLabel previewLabel;
    /**
     * The corresponding table name of the input field.
     */
    private final String tableName;

    /**
     * Constructor.
     *
     * @param columnType
     * @param tableName
     */
    public InputABIBInventoryNumber(ColumnType columnType, String tableName) {
        super(columnType);
        this.tableName = tableName;

        setGridX(2);
        setSidebar(new SidebarEntryField(columnType, ""));
    }

    /**
     * Updates the preview label with the current inserted values.
     */
    private void updatePreviewLabel() {
        if (getStringRepresentation().isEmpty()) {
            previewLabel.setText("");
        } else {
            previewLabel.setText(getStringRepresentation());
        }
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(tableName);
        final String s = list.get(columnType);

        setText(s);

        updatePreviewLabel();
    }

    private void setText(String value) {
        if (value == null || value.isEmpty()) {
            return;
        }
        String[] values = value.split("_");
        for (int i = 0; i < values.length && i < 4; i++) {
            if (i == 0) {
                textField1.setText(values[i]);
            } else if (i == 1) {
                textField2.setText(values[i]);
            } else if (i == 2) {
                textField3.setText(values[i]);
            } else if (i == 3) {
                textField4.setText(values[i]);
            }
        }
    }

    @Override
    public void clear() {
        textField1.setText("");
        textField2.setText("");
        textField3.setText("");
        textField4.setText("");
        updatePreviewLabel();
    }

    @Override
    public void save(DataSetOld data) {
        data.getDataRowForTable(tableName).add(new DataColumn(getStringRepresentation(), columnType.getColumnName()));
    }

    @Override
    public String getStringRepresentation() {
        String value = "";

        String t1 = textField1.getText();
        String t2 = textField2.getText();
        String t3 = textField3.getText();
        String t4 = textField4.getText();

        if (!t1.isEmpty()) {
            value += t1;
            if (!t2.isEmpty()) {
                value += "_" + t2;
                if (!t3.isEmpty()) {
                    value += "_" + t3;
                    if (!t4.isEmpty()) {
                        value += "_" + t4;
                    }
                }
            }
        }
        return value;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textField1);
        comp.add(textField2);
        comp.add(textField3);
        comp.add(textField4);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !getStringRepresentation().isEmpty();
    }


    @Override
    protected void createFieldInput() {
        // initialise the preview label
        previewLabel = new JLabel("");
        previewLabel.setForeground(Color.GRAY);

        // initialise the text fields.
        textField1 = new JTextField("");
        textField2 = new JTextField("");
        textField3 = new JTextField("");
        textField4 = new JTextField("");

        textField1.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
        textField2.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
        textField3.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
        textField4.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);

        textField1.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        textField2.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        textField3.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        textField4.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));

        textField1.setPreferredSize(new Dimension(40, 32));
        textField2.setPreferredSize(new Dimension(45, 32));
        textField3.setPreferredSize(new Dimension(35, 32));
        textField4.setPreferredSize(new Dimension(45, 32));

        // set the items together
        JPanel wrapper = new JPanel(new BorderLayout());
        wrapper.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        p.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        p.add(ComponentHelper.wrapComponent(textField1, 0, 0, 0, 5));
        p.add(ComponentHelper.wrapComponent(textField2, 0, 0, 0, 5));
        p.add(ComponentHelper.wrapComponent(textField3, 0, 0, 0, 5));
        p.add(ComponentHelper.wrapComponent(textField4, 0, 0, 0, 5));

        wrapper.add(BorderLayout.CENTER, p);
        wrapper.add(BorderLayout.EAST, previewLabel);

        setContent(wrapper);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
        ////////////////////////////////////////////////////////////
        // KEY LISTENER
        ////////////////////////////////////////////////////////////

        textField1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                int charLimit = 4;
                if (textField1.getText().length() > charLimit) {
                    textField1.setText(textField1.getText().substring(0, charLimit));
                }
                updatePreviewLabel();
            }
        });
        textField2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                int charLimit = 5;
                if (textField2.getText().length() > charLimit) {
                    textField2.setText(textField2.getText().substring(0, charLimit));
                }
                updatePreviewLabel();
            }
        });
        textField3.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                int charLimit = 4;
                if (textField3.getText().length() > charLimit) {
                    textField3.setText(textField3.getText().substring(0, charLimit));
                }
                updatePreviewLabel();
            }
        });
        textField4.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                int charLimit = 5;
                if (textField4.getText().length() > charLimit) {
                    textField4.setText(textField4.getText().substring(0, charLimit));
                }
                updatePreviewLabel();
            }
        });

        ////////////////////////////////////////////////////////////
        // FOCUS LISTENER
        ////////////////////////////////////////////////////////////
        textField1.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                textField1.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (textField1.getText().isEmpty()) {
                    return;
                }
                boolean validInput = false;
                CodeTableHashMap validValues;
                try {
                    validValues = ((T) mainFrame.getController()).getHashedCodeTableEntries(columnType);
                } catch (StatementNotExecutedException | NotLoggedInException ex) {
                    Logger.getLogger(InputABIBInventoryNumber.class.getName()).log(Level.SEVERE, null, ex);
                    validValues = new CodeTableHashMap();
                }
                try {
                    int number = Integer.parseInt(textField1.getText());
                    if (number > 1800 && number < 2100) {
                        validInput = true;
                    }
                } catch (NumberFormatException nfe) {
                    if (validValues.containsKey(textField1.getText())) {
                        validInput = true;
                    }
                }

                if (!validInput) {
                    textField1.setBorder(BorderFactory.createLineBorder(Color.red, 2));
                    textField1.setText("");

                    String values = "1800 - 2100, ";
                    for (String id : validValues.keySet()) {
                        values += id + ", ";
                    }
                    values = values.substring(0, values.length() - 2);

                    Footer.displayWarning(Loc.get("INVENTORY_NUMBER_1ST_NO_VALID_VALUE", values));
                    updatePreviewLabel();
                }
            }
        });

        textField2.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                textField2.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (textField2.getText().isEmpty()) {
                    return;
                }
                try {
                    Integer.parseInt(textField2.getText());
                } catch (NumberFormatException nfe) {
                    textField2.setBorder(BorderFactory.createLineBorder(Color.red, 2));
                    textField2.setText("");
                    Footer.displayWarning(Loc.get("NO_VALID_INPUT_ONLY_INTEGER_VALUES"));
                    updatePreviewLabel();
                }
            }
        });

        textField3.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                textField3.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
            }

            @Override
            public void focusLost(FocusEvent e) {
                String input = textField3.getText();
                if (input.contains("_") || input.contains(" ")) {
                    textField3.setBorder(BorderFactory.createLineBorder(Color.red, 2));
                    textField3.setText("");
                    Footer.displayWarning(Loc.get("INVENTORY_NUMBER_3RD_NO_VALID_INPUT_NO_UNDERSCORE_NO_SPACE"));
                    updatePreviewLabel();
                }
            }
        });

        textField4.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                textField4.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (textField4.getText().isEmpty()) {
                    return;
                }
                try {
                    Integer.parseInt(textField4.getText());
                } catch (NumberFormatException nfe) {
                    textField4.setBorder(BorderFactory.createLineBorder(Color.red, 2));
                    textField4.setText("");
                    Footer.displayWarning(Loc.get("NO_VALID_INPUT_ONLY_INTEGER_VALUES"));
                    updatePreviewLabel();
                }
            }
        });

        // MOUSE LISTENER
        textField1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                String text = InputABIBInventoryNumber.this.getStringRepresentation();
                if (text.isEmpty()) {
                    textField1.setToolTipText(null);
                } else {
                    textField1.setToolTipText(text);
                }
            }
        });
        textField2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                String text = InputABIBInventoryNumber.this.getStringRepresentation();
                if (text.isEmpty()) {
                    textField2.setToolTipText(null);
                } else {
                    textField2.setToolTipText(text);
                }
            }
        });
        textField3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                String text = InputABIBInventoryNumber.this.getStringRepresentation();
                if (text.isEmpty()) {
                    textField3.setToolTipText(null);
                } else {
                    textField3.setToolTipText(text);
                }
            }
        });
        textField4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                String text = InputABIBInventoryNumber.this.getStringRepresentation();
                if (text.isEmpty()) {
                    textField4.setToolTipText(null);
                } else {
                    textField4.setToolTipText(text);
                }
            }
        });
    }

}
