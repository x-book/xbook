package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.InputElement;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.UpdateableEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;

import javax.swing.*;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractInputElementFunctions extends JPanel implements IInputElement {

    protected ColumnType columnType;
    private UpdateableEntry entry;
    protected InputElement inputElement;

    public AbstractInputElementFunctions(ColumnType columnType) {
        this.columnType = columnType;
        createField();
    }

    protected abstract void createField();

    @Override
    public void setEntry(UpdateableEntry entry) {
        this.entry = entry;
    }

    @Override
    public boolean isMandatory() {
        return columnType.isMandatory();
    }

    @Override
    public void actionOnFocusGain() {
    }

    @Override
    public void actionOnFocusLost() {
    }

    /**
     * Returns the table name corresponding to the current column type.
     *
     * @return The table name.
     */
    protected String getTableName() {
        return ColumnHelper.getTableName(columnType.getColumnName());
    }

    /**
     * @param inputElement
     */
    public void setInputElement(InputElement inputElement) {
        this.inputElement = inputElement;
    }

    @Override
    public void loadMultiEdit(DataSetOld data) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
