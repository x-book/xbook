package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.specific_reuseable;

import com.toedter.calendar.JTextFieldDateEditor;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTableOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.YesNoField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XDateChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler
 */
public class InputABIBAudit extends AbstractInputElement {

    private JLabel labelLog;
    private JButton buttonLog;
    private ArrayList<AuditLogEntry> logData;
    private YesNoField objectSeenField;
    private XDateChooser timeField;
    private final int GRID = 2;

    private final String tableNameAudit;

    private final ColumnType columnTypeObjectSeen;
    private final ColumnType columnTypeTime;
    private final ColumnType columnTypeUserID;

    /**
     * Object to format the date.
     */
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Constructor.
     * @param tableNameAudit
     * @param columnTypeObjectSeen
     * @param columnTypeTime
     * @param columnTypeUserID
     */
    public InputABIBAudit(String tableNameAudit, ColumnType columnTypeObjectSeen, ColumnType columnTypeTime, ColumnType columnTypeUserID) {
        super(columnTypeObjectSeen);

        this.tableNameAudit = tableNameAudit;
        this.columnTypeObjectSeen = columnTypeObjectSeen;
        this.columnTypeTime = columnTypeTime;
        this.columnTypeUserID = columnTypeUserID;

        setGridX(2);
        setGridY(2);
        setSidebar(new SidebarEntryField(Loc.get("AUDIT"), ""));
    }

    private JLabel createLabel(String text) {
        JLabel lab = new JLabel(text);
        lab.setBackground(Colors.INPUT_FIELD_BACKGROUND);
        return lab;
    }

    private void setLogLabel(int countInLog, AuditLogEntry newestLogEntry) {
        String label = "";

        if (newestLogEntry != null && newestLogEntry.time != null && newestLogEntry.user != null) {
            label += "<b>";
            if (newestLogEntry.objectSeen) {
                label += "<b>" + Loc.get("LAST_SEEN");
            } else {
                label += "<b>" + Loc.get("LAST_NOT_SEEN");
            }
            label += "<br>" + Loc.get("ON_DATE") + " " + newestLogEntry.getTime() + "</b>";
        }

        labelLog.setText("<html><div align='right'>" + label + "</div></html>");
        buttonLog.setEnabled(countInLog > 0);
        buttonLog.setText("<html><center>" + Loc.get("OPEN_LOG") + "<br>" + "(" + Loc.get("NUMBER_OF_ENTRIES_IN_LOG", countInLog) + ")" + "</center></html>");
    }

    private void removeLogLabel(boolean displayLabelNone) {
        if (displayLabelNone) {
            labelLog.setText(Loc.get("NONE"));
        } else {
            labelLog.setText("");
        }
        buttonLog.setEnabled(false);
        buttonLog.setText(Loc.get("OPEN_LOG"));
    }

    public ArrayList<AuditLogEntry> getLogData() {
        return logData;
    }

    @Override
    public void load(DataSetOld data) {
        DataTableOld list = data.getOrCreateDataTable(tableNameAudit);
        ArrayList<AuditLogEntry> log = new ArrayList<>();
        for (DataRow row : list) {
            Boolean objectSeen = null;
            String time = "";
            String user = "";

            try {
                user = mainFrame.getController().getDisplayName(Integer.parseInt(row.get(columnTypeUserID)));
            } catch (NotLoggedInException | NumberFormatException | StatementNotExecutedException ex) {
                user = "(ID: " + row.get(columnTypeUserID);
                Logger.getLogger(InputABIBAudit.class.getName()).log(Level.SEVERE, null, ex);
            }

            time = convertTime(row.get(columnTypeTime));
            final String s = row.get(columnTypeObjectSeen);
            if (s.equals(NO)) {
                objectSeen = false;
            } else if (s.equals(YES)) {
                objectSeen = true;
            }

            log.add(new AuditLogEntry(objectSeen, time, user));
        }
        this.logData = log;
        if (!log.isEmpty()) {
            setLogLabel(list.size(), log.get(log.size() - 1));
        } else {
            removeLogLabel(true);
        }
    }

    @Override
    public void clear() {
        objectSeenField.setSelectedNone();
        timeField.setDate(null);
        removeLogLabel(false);
        logData = new ArrayList<>();
    }

    @Override
    public void save(DataSetOld data) {
        if (!isValidInput()) {
            return;
        }
        String objectSeen = null;
        if (objectSeenField.isSelectedYes()) {
            objectSeen = YES;
        } else if (objectSeenField.isSelectedNo()) {
            objectSeen = NO;
        }
        Date time = timeField.getDate();
        if (time == null || objectSeen == null) {
            return;
        }

        DataTableOld list = data.getOrCreateDataTable(tableNameAudit);
        DataRow dataa = new DataRow(tableNameAudit);
        dataa.add(new DataColumn(objectSeen, columnTypeObjectSeen));
        dataa.add(new DataColumn(dateFormat.format(time), columnTypeTime));
        list.add(dataa);
    }

    /**
     * Converts the date from the format "yyyy-mm-dd hh:mm:ss" to "dd.mm.yyyy".
     *
     * @param oldFormat The date in the format "yyyy-mm-dd hh:mm:ss".
     * @return The date in the format "dd.mm.yyyy".
     */
    private String convertTime(String oldFormat) {
        String yyyy = oldFormat.substring(0, 4);
        String mm = oldFormat.substring(5, 7);
        String dd = oldFormat.substring(8, 10);

        return dd + "." + mm + "." + yyyy;
    }

    @Override
    public String getStringRepresentation() {
        String objectSeen = "";
        String time = "";
        if (objectSeenField.isSelectedYes()) {
            objectSeen += "1";
        } else if (objectSeenField.isSelectedNo()) {
            objectSeen += "0";
        }
        if (timeField.getDate() != null) {
            time = dateFormat.format(timeField.getDate());
        }
        return objectSeen + "/" + time;
    }

    private InputABIBAudit getThis() {
        return this;
    }

    @Override
    public boolean isValidInput() {
        if (!objectSeenField.isSelected() && timeField.getDate() == null) {
            return true;
        }
        if (objectSeenField.isSelected() && timeField.getDate() != null) {
            return true;
        }
        return false;
    }

    class AuditLogEntry {

        private boolean objectSeen;
        private String time;
        private String user;

        public AuditLogEntry(Boolean objectSeen, String time, String user) {
            this.objectSeen = objectSeen;
            this.user = user;
            this.time = time;
        }

        public boolean getObjectSeen() {
            return objectSeen;
        }

        public String getUser() {
            return user;
        }

        public String getTime() {
            return time;
        }
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.addAll(timeField.getFocusableComponents());
        comp.add(objectSeenField);
        comp.add(objectSeenField.getNo());
        comp.add(objectSeenField.getYes());
        return comp;
    }

    @Override
    public void setErrorStyle() {
        // see actionOnFocusLost()
    }

    @Override
    public void actionOnFocusLost() {
        String textInput = (((JTextFieldDateEditor) timeField.getDateEditor()).getText());
        if (!textInput.isEmpty() && timeField.getDate() == null) {
            timeField.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
            timeField.setDate(null);
            Footer.displayWarning(Loc.get("INVALID_INPUT_IN_FIELD", columnType.getDisplayName()));
        }
        if (!isValidInput()) {
            if (!objectSeenField.isSelected()) {
                objectSeenField.setSelectedNone();
                objectSeenField.getYes().setForeground(Color.RED);
                objectSeenField.getNo().setForeground(Color.RED);
                Footer.displayWarning(Loc.get("INVALID_INPUT_IN_FIELD", columnType));
            } else if (timeField.getDate() == null) {
                timeField.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                // is done within JDateField class
            }
        }
        super.actionOnFocusLost();
    }

    @Override
    public void actionOnFocusGain() {
        super.actionOnFocusGain();
        objectSeenField.getYes().setForeground(Color.BLACK);
        objectSeenField.getNo().setForeground(Color.BLACK);
        timeField.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
    }

    @Override
    protected void createFieldInput() {
        int panelWidthAbsolute = getPreferredSize().width;
        int panelWidthToUse = panelWidthAbsolute - 15;

        JPanel outerWrapper = new JPanel(new BorderLayout());

        JPanel panelRightWrapper = new JPanel(new BorderLayout());
        JPanel panelRight = new JPanel(new FlowLayout());
        JPanel objectSeenWrapper = new JPanel(new BorderLayout());
        objectSeenField = new YesNoField();
        objectSeenField.setPreferredSize(new Dimension(1 * (panelWidthToUse / GRID), Sizes.INPUT_FIELD_HEIGHT));
        objectSeenWrapper.add(BorderLayout.NORTH, new JLabel(Loc.get("OBJECT_SEEN")));
        objectSeenWrapper.add(BorderLayout.CENTER, objectSeenField);
        panelRight.add(objectSeenWrapper);

        JPanel dateFieldWrapper = new JPanel(new BorderLayout());
        timeField = new XDateChooser();
        timeField.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        panelRight.add(timeField);
        timeField.setPreferredSize(new Dimension(1 * (panelWidthToUse / GRID), Sizes.INPUT_FIELD_HEIGHT));

        dateFieldWrapper.add(BorderLayout.NORTH, new JLabel(Loc.get("ON_DATE")));
        dateFieldWrapper.add(BorderLayout.CENTER, timeField);
        panelRight.add(BorderLayout.CENTER, dateFieldWrapper);

        panelRightWrapper.add(BorderLayout.NORTH, panelRight);
        outerWrapper.add(BorderLayout.EAST, panelRightWrapper);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelLog = createLabel("");
        p.add(labelLog);
        buttonLog = new JButton("------------------");
        buttonLog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Content.setContent(new ContentABIBAuditList(Content.getCurrentContent(), getThis()));
            }
        });
        buttonLog.setEnabled(false);
        buttonLog.setPreferredSize(new Dimension((int) ((panelWidthToUse / GRID) * 1.0), Sizes.INPUT_FIELD_HEIGHT));
        p.add(buttonLog);
        outerWrapper.add(BorderLayout.SOUTH, p);

        setContent(outerWrapper);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }


}
