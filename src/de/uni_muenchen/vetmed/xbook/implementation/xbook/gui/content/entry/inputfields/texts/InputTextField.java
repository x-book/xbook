package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawTextField;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.ITextField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;
import java.awt.Component;
import java.util.ArrayList;

import javax.swing.*;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputTextField extends AbstractDefaultInputElement implements ITextField {

    /**
     * The text field object.
     */
    protected RawTextField zField;

    public InputTextField(ColumnType columnType) {
        super(columnType);
    }

    public InputTextField(ColumnType columnType, Mode mode) {
        super(columnType);
    }

    @Deprecated
    public InputTextField(ColumnType columnType, int gridX, int gridY, String sidebarText) {
        super(columnType);
        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebarText);
    }

    @Override
    public String getText() {
        return zField.getText();
    }

    public JTextField getTextField() {
        return zField.getTextField();
    }

    @Override
    public AbstractInputElementFunctions createInputField() {
        if (zField == null) {
            zField = new RawTextField(columnType);
        }
        return zField;
    }

    @Override
    public void setText(String text) {
        zField.setText(text);
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return zField.getMyFocusableComponents();
    }
}
