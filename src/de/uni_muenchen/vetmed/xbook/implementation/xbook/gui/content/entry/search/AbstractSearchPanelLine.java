package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.search;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.SearchEntryInfo;
import de.uni_muenchen.vetmed.xbook.api.datatype.SearchEntryInfo.SearchMode;
import de.uni_muenchen.vetmed.xbook.api.exception.ValueNotAvailableException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * An abstract search row for the filtering of values that provides the basic functionalities.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@cesonia.io>
 */
public abstract class AbstractSearchPanelLine extends JPanel {


    /**
     * The input field that is set by inherited classes.
     */
    protected JComponent inputField;
    /**
     * The corresponding column type.
     */
    protected ColumnType columnType;

    /**
     * A button which makes possible to switch the selected mode.
     */
    private XButton searchModeButton;
    /**
     * The current selected search mode.
     */
    protected SearchMode currentSearchMode;
    /**
     * Holds all available search modes that should appear in the search mask.
     */
    private ArrayList<SearchMode> availableSearchModes;

    /**
     * Constructor.
     *
     * @param columnType           The basic ColumnType.
     * @param availableSearchModes The available search modes. The first one of the list will automatically be selected
     *                             at default value. If array is empty, SearchMode.CONTAINS will be used as default
     *                             value.
     */
    public AbstractSearchPanelLine(ColumnType columnType, ArrayList<SearchMode> availableSearchModes) {
        setBackground(Colors.CONTENT_BACKGROUND);
        setLayout(new BorderLayout());

        this.columnType = columnType;
        if (availableSearchModes == null || availableSearchModes.isEmpty()) {
            this.availableSearchModes = new ArrayList<>();
            this.availableSearchModes.add(SearchMode.CONTAINS);
            this.currentSearchMode = SearchMode.CONTAINS;
        } else {
            this.availableSearchModes = availableSearchModes;
            this.currentSearchMode = this.availableSearchModes.get(0);
        }

        JPanel wrapper = new JPanel(new BorderLayout());
        wrapper.setBackground(Colors.CONTENT_BACKGROUND);

        // init the column label
        JLabel label = new JLabel(columnType.getDisplayName() + ":", SwingConstants.RIGHT);
        label.setBackground(Colors.CONTENT_BACKGROUND);
        label.setPreferredSize(new Dimension(150, 28));
        wrapper.add(BorderLayout.WEST, ComponentHelper.wrapComponent(label, Colors.CONTENT_BACKGROUND, 0, 4, 0, 0));

        JPanel inputWrapper = new JPanel(new BorderLayout());
        inputWrapper.setBackground(Colors.CONTENT_BACKGROUND);

        // init the mode button
        searchModeButton = new XButton();
        searchModeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchSearchMode();
            }
        });
        searchModeButton.setPreferredSize(new Dimension(80, 1));
        updateSearchModeButton();
        // only add the button to the GUI if there are more than one available modes
        if (availableSearchModes.size() > 1) {
            inputWrapper.add(BorderLayout.WEST, ComponentHelper.wrapComponent(searchModeButton, 0, 2, 0, 0));
        }

        // init the input field
        inputField = getInputField();
        inputField.setPreferredSize(new Dimension(400, 28));
        inputWrapper.add(BorderLayout.CENTER, inputField);

        wrapper.add(BorderLayout.CENTER, inputWrapper);
        add(BorderLayout.CENTER, ComponentHelper.wrapComponent(wrapper, Colors.CONTENT_BACKGROUND, 2, 0, 2, 0));
    }

    /**
     * Switches the selected search mode and select the next available one.
     */
    private void switchSearchMode() {
        if (currentSearchMode == SearchMode.CONTAINS) {
            // search the next available mode. Search in a loop until "contains"
            if (availableSearchModes.contains(SearchMode.EQUALS)) {
                currentSearchMode = SearchMode.EQUALS;
            } else {
                currentSearchMode = SearchMode.CONTAINS;
            }
        } else if (currentSearchMode == SearchMode.EQUALS) {
            // search the next available mode. Search in a loop until "equals"
            if (availableSearchModes.contains(SearchMode.CONTAINS)) {
                currentSearchMode = SearchMode.CONTAINS;
            } else {
                currentSearchMode = SearchMode.EQUALS;
            }
        }
        updateSearchModeButton();
    }

    /**
     * Update the search mode button dependent on the current selected search mode.
     */
    private void updateSearchModeButton() {
        if (currentSearchMode == SearchMode.CONTAINS) {
            searchModeButton.setText(Loc.get("CONTAINS"));
        } else if (currentSearchMode == SearchMode.EQUALS) {
            searchModeButton.setText(Loc.get("IS"));
        }
    }

    /**
     * Set the search mode to a new value and updates the button.
     */
    protected void setSearchMode(SearchMode mode) throws ValueNotAvailableException {
        if (availableSearchModes.contains(mode)) {
            this.currentSearchMode = mode;
        } else {
            throw new ValueNotAvailableException(mode);
        }
        updateSearchModeButton();
    }

    /**
     * Returns the current selected search mode.
     *
     * @return The current selected search mode.
     */
    public SearchMode getCurrentSearchMode() {
        return currentSearchMode;
    }

    /**
     * Returns the corresponding column type.
     *
     * @return The corresponding column type.
     */
    public ColumnType getColumnType() {
        return columnType;
    }

    /**
     * Returns the input of the input field.
     *
     * @return The input of the input field.
     */
    public abstract SearchEntryInfo getInput();

    /**
     * Returns the individual input field.
     *
     * @return The individual input field.
     */
    public abstract JComponent getInputField();

    /**
     * Clears the search input field and the corresponding search model button. Override this to enable the clearing of
     * the corresponding input field.
     */
    public void clearInput() {
        this.currentSearchMode = this.availableSearchModes.get(0);
        updateSearchModeButton();
    }
}
