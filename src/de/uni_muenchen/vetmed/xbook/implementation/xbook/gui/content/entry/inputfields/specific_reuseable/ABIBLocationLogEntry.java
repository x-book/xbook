package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.specific_reuseable;

/**
 *
 * @author ciaran.harrington@extern.lrz-muenchen.de
 */
public class ABIBLocationLogEntry {

    private String place;
    private String room;
    private String shelf;
    private String tray;
    private String tableau;
    private String user;
    private String time;

    public ABIBLocationLogEntry(String place, String room, String shelf, String tray, String tableau, String user, String time) {
        this.place = place;
        this.room = room;
        this.shelf = shelf;
        this.tray = tray;
        this.user = user;
        this.time = time;
        this.tableau = tableau;
    }

    public String getRoom() {
        return room;
    }

    public String getShelf() {
        return shelf;
    }

    public String getTray() {
        return tray;
    }

    public String getTableau() {
        return tableau;
    }

    public String getPlace() {
        return place;
    }

    public String getUser() {
        return user;
    }

    public String getTime() {
        return time;
    }
}
