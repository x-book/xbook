package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.StatusLabel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.InputMaskModes;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractCrossLinkedManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntryRoot;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InputCrossedLinkedEntriesMultiCount extends AbstractInputElement {

    private String singularLabel = Loc.get("LINK");
    private String pluralLabel = Loc.get("LINKS");

    private JLabel label;
    private Integer countElements = null;
    protected BaseEntryManager manager;
    protected AbstractCrossLinkedManager mappingManager;
    protected AbstractEntry connectedCrosslinkedEntry;

    public InputCrossedLinkedEntriesMultiCount(AbstractEntry entry, BaseEntryManager manager, AbstractCrossLinkedManager mappingManager) {
        super(new ColumnType("not_in_use" + manager.getLocalisedTableName(), ColumnType.Type.VALUE,
                ColumnType.ExportType.NONE).setDisplayName(manager.getLocalisedTableName()));
        this.manager = manager;
        this.mappingManager = mappingManager;
        this.connectedCrosslinkedEntry = entry;
    }

    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        label = new JLabel("", JLabel.CENTER);
        updateLabel(0);
        multiPanel.add(BorderLayout.CENTER, label);

        setRememberValueFieldDisabled();

        ComponentHelper.colorAllChildren(multiPanel, Colors.INPUT_FIELD_BACKGROUND);
        setContent(multiPanel);
    }

    @Override
    public String getStringRepresentation() {
        return "<n.a.>";
    }

    @Override
    public void load(DataSetOld data) {
        if (isMultiEdit() && !columnType.isMultiEditAllowed()) {
            setStatus(StatusLabel.Status.NOT_SUPPORTED);
            return;
        }
        EntryDataSet entryData = (EntryDataSet) data;
        try {
            // get id and dbnr of target entry
            ArrayList<Key> entryKey = mappingManager.load(entryData.getProjectKey(), entryData.getEntryKey(), manager.getTableName());
            updateLabel(entryKey.size());
        } catch (StatementNotExecutedException ex) {
            Logger.getLogger(InputCrossedLinkedEntriesMulti.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateLabel(Integer count) {
        this.countElements = count;
        if (label == null) {
            return;
        }

        try {
            InputMaskModes mode = ((AbstractEntryRoot) entry).getInputMaskMode();
            if (mode == InputMaskModes.NEW_ENTRY || count == null) {
                label.setText("—");
            } else if (count == 1) {
                label.setText("<html><center>" + Loc.get("COUNT") + ":<br><b>" + count + " " + singularLabel + "</b></center></html>");
            } else {
                label.setText("<html><center>" + Loc.get("COUNT") + ":<br><b>" + count + " " + pluralLabel + "</b></center></html>");
            }
        } catch (NullPointerException nfe) {
            label.setText("—");
        }
    }

    @Override
    public void save(DataSetOld data) throws IsAMandatoryFieldException {
        // nothing to save
    }

    @Override
    public void clear() {
        updateLabel(null);
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return new ArrayList<Component>();
    }

    public void setLabel(String singularLabel, String pluralLabel) {
        this.singularLabel = singularLabel;
        this.pluralLabel = pluralLabel;
        updateLabel(countElements);
    }
}


