package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.groupManagement;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights.AbstractProjectRightsAdd;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Group;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rank;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rights;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.datatype.User;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights.RightMode;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class AddUserToGroup extends AbstractProjectRightsAdd<User> {

    private final Group group;
    private final GroupManagement rangScreen;
    private ArrayList<Rank> ranks;
    private JComboBox rankCombo;
    private ArrayList<User> allUsers;

    public AddUserToGroup(GroupManagement rangScreen, Group group) {
        super(RightMode.USER_TO_GROUP);
        this.group = group;
        this.rangScreen = rangScreen;

        ArrayList<JComponent> customContent = addCustomContent();
        if (customContent != null) {
            for (JComponent c : customContent) {
                contentPanel.add(c);
            }
        }
    }

    @Override
    protected ArrayList<User> getComboData() {
        try {
            ArrayList<User> users = mainFrame.getController().getVisibleUsers();
            ArrayList<User> newUsers = new ArrayList<>();
            for (User u : users) {
                if (u.getId() != mainFrame.getController().getUserID()) {
                    newUsers.add(u);
                }
            }
            return newUsers;
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(AbstractProjectRightsAdd.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    @Override
    protected ArrayList<ColumnType> getAvailableRights() {
        return new ArrayList<>();
    }

    @Override
    protected void actionAfterSaving() {
        mainFrame.displayGroupManagementScreen(group);
    }

    protected ArrayList<JComponent> addCustomContent() {
        ArrayList<JComponent> array = new ArrayList<>();

        rankCombo = new JComboBox();
        rankCombo.setPreferredSize(new Dimension((int) (Sizes.BUTTON_WIDTH * 1.5), Sizes.BUTTON_HEIGHT));
        ranks = new ArrayList<>();
        try {
            ranks = mainFrame.getController().getRankOfGroup(group.getId());
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(AddUserToGroup.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Rank rank : ranks) {
            rankCombo.addItem(rank.getName());
        }
        array.add(rankCombo);
        if (ranks.isEmpty()) {
            rankCombo.setEnabled(false);
            array.add(new JLabel("NO_RANKS_AVAILABLE"));
        }

        return array;
    }

    @Override
    protected void save() {
        User userToSave = null;
        if (!textField.getText().isEmpty()) {
            User textInputUser;
            try {
                textInputUser = mainFrame.getController().getUserInformation(textField.getText());
                if (textInputUser == null) {
                    Footer.displayError(Loc.get("NOT_A_VALID_USER"));
                    textField.setText("");
                } else {
                    userToSave = textInputUser;
                }
            } catch (NotLoggedInException | StatementNotExecutedException | EntriesException ex) {
            }
        } else if (!(combo.getSelectedItem()).equals("")) {
            userToSave = allUsers.get(combo.getSelectedIndex() - 1);
        }

        if (userToSave == null) {
            Footer.displayError(Loc.get("NOT_A_VALID_USER"));
        } else {
            ArrayList<Rights> rights = new ArrayList<>();
            for (JCheckBox check : allCheckBoxes) {
                rights.add(new Rights(new ColumnType(check.getName(),ColumnType.Type.VALUE,ColumnType.ExportType.NONE), check.isSelected()));
            }
            ArrayList<RightsInformation> ri = new ArrayList<>();
            RightsInformation newRights = new RightsInformation(userToSave.getUserName(), userToSave.getId(), rights);
            ri.add(newRights);
            if (newRights.hasAnyRight() || newRights.getRights().isEmpty()) {
                UserRightInformation uri = new UserRightInformation(userToSave.getUserName(), userToSave.getId(), ri);
                try {
                    int rankid = -1;
                    for (Rank rank : ranks) {
                        if (rank.getName().equals( rankCombo.getSelectedItem())) {
                            rankid = rank.getId();
                        }
                    }
                    if (rankid != -1) {
                        Message ms = ((AbstractSynchronisationController) mainFrame.getController()).updateUserRank(group.getId(), uri.getUserID(), rankid);
                        if (ms.getResult().wasSuccessful()) {

                        } else {
                            System.out.println(ms.getResult());
                        }
                    }
                } catch (IOException | NotConnectedException | NotLoggedInException ex) {
                    Logger.getLogger(AddUserToGroup.class.getName()).log(Level.SEVERE, null, ex);
                }
                actionAfterSaving();
            } else {
                Footer.displayWarning(Loc.get("NO_RIGHTS_ASSIGNED"));
            }
        }
    }

    @Override
    protected void fillComboData() {
        allUsers = getComboData();
        for (User u : allUsers) {
            combo.addItem(u.getDisplayName() + " (" + u.getUserName() + ")");
        }
    }
}
