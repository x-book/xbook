package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import com.toedter.calendar.JTextFieldDateEditor;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XDateChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IDateChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A date chooser as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputDateChooser extends AbstractInputElement implements IDateChooser {

    /**
     * The date chooser object.
     */
    protected XDateChooser dateChooser;
    /**
     * Object to format the date.
     */
    protected final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     *                   field.
     */
    public InputDateChooser(ColumnType columnType) {
        super(columnType);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     *                   field.
     * @param gridX      The number of grids that are used for the height of the
     *                   input field.
     * @param gridY      The number of grids that are used for the weight of the
     *                   input field.
     * @param sidebar    The custom sidebar for this input element.
     *                   <code>null</code> if no custom sidebar is available.
     */
    @Deprecated
    public InputDateChooser(ColumnType columnType, int gridX, int gridY, SidebarPanel sidebar) {
        super(columnType);

        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);
    }

    /**
     * Constructor.
     *
     * @param columnType  The database column data of the correspondenting input
     *                    field.
     * @param gridX       The number of grids that are used for the height of the
     *                    input field.
     * @param gridY       The number of grids that are used for the weight of the
     *                    input field.
     * @param sidebarText The sidebar text for this input element.
     */
    @Deprecated
    public InputDateChooser(ColumnType columnType, int gridX, int gridY, String sidebarText) {
        this(columnType, gridX, gridY, new SidebarEntryField(columnType, sidebarText));
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     *                   field.
     * @param gridX      The number of grids that are used for the height of the
     *                   input field.
     * @param gridY      The number of grids that are used for the weight of the
     *                   input field.
     */
    @Deprecated
    public InputDateChooser(ColumnType columnType, int gridX, int gridY) {
        this(columnType, gridX, gridY, (SidebarPanel) null);
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        final String s = list.get(columnType);
        try {
            if (s != null && !s.isEmpty()&&!columnType.getDefaultValue().equals(s)) {

                setDate(s);
            }
        } catch (ParseException ex) {
            Logger.getLogger(InputDateChooser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void clear() {
        dateChooser.setDate(null);
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        list.add(new DataColumn(getDate(), columnType.getColumnName()));
    }

    @Override
    public String getStringRepresentation() {
        if (dateChooser.getDate() == null) {
            return "";
        } else {
            return dateFormat.format(dateChooser.getDate());
        }
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return dateChooser.getFocusableComponents();
    }

    @Override
    public boolean isValidInput() {
        return dateChooser.getDate() != null;
    }

    @Override
    public void actionOnFocusLost() {
        super.actionOnFocusLost();
        String textInput = (((JTextFieldDateEditor) dateChooser.getDateEditor()).getText());
        if (!textInput.isEmpty() && dateChooser.getDate() == null) {
            dateChooser.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
            dateChooser.setDate(null);
            Footer.displayWarning(Loc.get("INVALID_INPUT_IN_FIELD", columnType.getDisplayName()));
        } else {
            dateChooser.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        }
    }

    @Override
    public void actionOnFocusGain() {
        super.actionOnFocusGain();
        dateChooser.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
    }


    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        dateChooser = new XDateChooser();
        dateChooser.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));

        // necessary because the calendar popup gain the focus, so the input field looses the
        // focus which results in problems with the input field focus actions.
        // therefore the InputDateChooser gets the focus on every property change of the JCalendar.
        dateChooser.getJCalendar().addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                InputDateChooser.super.setFocus();
            }
        });


        multiPanel.add(BorderLayout.CENTER, dateChooser);

        setContent(multiPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

    // ========================================================================
    @Override
    public void setDate(String date) throws ParseException {
        if ("0000-00-00".equals(date)) {
            dateChooser.setDate(null);
        } else
            dateChooser.setDate(dateFormat.parse(date));
    }

    @Override
    public String getDate() {
        if (dateChooser.getDate() == null) {
            return "0000-00-00";
        }
        return dateFormat.format(dateChooser.getDate());
    }

}
