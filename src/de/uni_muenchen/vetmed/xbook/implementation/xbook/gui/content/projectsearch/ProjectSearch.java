package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardProjectColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.manager.ISynchronisationManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XSubTitle;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractExtendedProjectEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractProjectManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing.Filter;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing.Listing;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.rows.ProjectSearchBooleanRow;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.rows.ProjectSearchComboRow;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.rows.ProjectSearchTextRow;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.rows.AbstractProjectSearchRow;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.rows.AbstractProjectSearchRow.DataType;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.rows.AbstractProjectSearchRow.SearchType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @param <T>
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ProjectSearch<T extends AbstractMainFrame<?>> extends AbstractContent {

    public static final boolean ACTIVATE_MAGICAL_STUFF = false;

    protected final T mainFrame;
    private JPanel wrapperPanel;
    private JPanel wrapperResult;
    private ArrayList<AbstractProjectSearchRow> allProjectRows;
    private Set<AbstractBaseEntryManager> managers;
    private JComboBox<AbstractBaseEntryManager> comboManager;
    protected HashMap<AbstractBaseEntryManager, ArrayList<AbstractProjectSearchRow>> entryRows;

    public ProjectSearch(T mainFrame) {
        super(true);

        this.mainFrame = mainFrame;
        init();
    }

    @Override
    protected JPanel getContent() {
        wrapperPanel = new JPanel(new StackLayout());

        wrapperResult = new JPanel(new StackLayout());
        wrapperPanel.add(ComponentHelper.wrapComponent(wrapperResult, 0, 0, 0, 0));

        wrapperPanel.add(new XTitle(Loc.get("PROJECT_SEARCH")));

        // fill the project rows
        wrapperPanel.add(new XSubTitle(Loc.get("PROJECT")));
        allProjectRows = getProjectRows();
        for (AbstractProjectSearchRow row : allProjectRows) {
            wrapperPanel.add(row);
        }
        wrapperPanel.add(new JLabel(" "));
        wrapperPanel.add(new JLabel(" "));

        entryRows = getEntryRows();
        managers = entryRows.keySet();

        // fill the entry rows
        for (Map.Entry<AbstractBaseEntryManager, ArrayList<AbstractProjectSearchRow>> manager : entryRows.entrySet()) {
            wrapperPanel.add(new XSubTitle(manager.getKey().localisedTableName));
            for (AbstractProjectSearchRow row : manager.getValue()) {
                wrapperPanel.add(row);
            }
            wrapperPanel.add(new JLabel(" "));
            wrapperPanel.add(new JLabel(" "));
        }

        return wrapperPanel;
    }

    private void displayResult() {
        wrapperResult.removeAll();

        wrapperResult.add(new XTitle(Loc.get("SEARCH_RESULT")));

        comboManager = new JComboBox<>();
        for (AbstractBaseEntryManager manager : managers) {
            comboManager.addItem(manager);
        }
        comboManager.setPreferredSize(new Dimension(120, 30));
        JLabel comboDescription = new JLabel("<html><b>" + Loc.get("TABLE_TO_OPEN") + ":</b></html>");
        if (managers.size() > 1) {
            wrapperResult.add(ComponentHelper.wrapComponent(comboDescription, 0, 0, 4, 150));
            wrapperResult.add(ComponentHelper.wrapComponent(comboManager, 0, 0, 10, 150));
        }

        int countResultRows = searchProjects();
        comboManager.setVisible(countResultRows > 0);
        comboDescription.setVisible(countResultRows > 0);

        wrapperResult.add(new JLabel(" "));
        wrapperResult.add(new JLabel(" "));

        wrapperResult.revalidate();
        wrapperResult.repaint();
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel bp = new ButtonPanel();
        bp.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_CLEAR, Loc.get("CLEAR"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clear();
            }
        });

        bp.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_SEARCH, Loc.get("RUN_SEARCH"), 2.0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayResult();
            }
        });
        return bp;

    }

    private void clear() {
        for (AbstractProjectSearchRow row : allProjectRows) {
            row.clear();
            row.switchTo(SearchType.INPUT);
            row.switchTo(DataType.CONTAINS);
        }
        for (ArrayList<AbstractProjectSearchRow> entrySet : entryRows.values()) {
            for (AbstractProjectSearchRow row : entrySet) {
                row.clear();
                row.switchTo(SearchType.INPUT);
                row.switchTo(DataType.CONTAINS);
            }
        }
        wrapperResult.removeAll();
        scrollToTop();
        wrapperResult.revalidate();
        wrapperResult.repaint();
    }

    @Override
    public SidebarPanel getSideBar() {
        SidebarPanel sidebar = new SidebarPanel();
        sidebar.addTitle(Loc.get("PROJECT_SEARCH"));
        sidebar.addHtmlPane(Loc.get("SIDEBAR_PROJECT_SEARCH"));
        return sidebar;
    }

    @Override
    public boolean forceSidebar() {
        return true;
    }

    public ArrayList<AbstractProjectSearchRow> getProjectRows() {
        ArrayList<AbstractProjectSearchRow> rows = new ArrayList<>();
        try {
            AbstractProjectManager projectManager = mainFrame.getController().getLocalManager().getProjectManager();
            for (ColumnType columnType : projectManager.getDataColumns()) {
                addRow(columnType, rows);
            }
            for (AbstractExtendedProjectEntryManager m : projectManager.getManagers()) {
                for (ColumnType columnType : m.getDataColumns()) {
                    addRow(columnType, rows);
                }
            }
        } catch (NotLoggedInException ex) {
            Logger.getLogger(ProjectSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rows;
    }

    protected HashMap<AbstractBaseEntryManager, ArrayList<AbstractProjectSearchRow>> getEntryRows() {
        HashMap<AbstractBaseEntryManager, ArrayList<AbstractProjectSearchRow>> hash = new HashMap<>();
        try {
            for (IBaseManager iBaseManager : mainFrame.getController().getLocalManager().getSyncTables()) {
                ArrayList<AbstractProjectSearchRow> rows = new ArrayList<>();
                if (iBaseManager instanceof AbstractBaseEntryManager) {
                    AbstractBaseEntryManager abstractBaseEntryManager = (AbstractBaseEntryManager) iBaseManager;
                    for (ColumnType columnType : abstractBaseEntryManager.getDataColumns()) {
                        addRow(columnType, rows);
                    }
                    for (ISynchronisationManager iSynchronisationManager : ((IBaseManager) iBaseManager).getManagers()) {
                        for (ColumnType columnType : iSynchronisationManager.getDataColumns()) {
                            addRow(columnType, rows);
                        }
                    }
                    hash.put(abstractBaseEntryManager, rows);
                }
            }
        } catch (NotLoggedInException ex) {
            Logger.getLogger(ProjectSearch.class.getName()).log(Level.SEVERE, null, ex);
        }

        return hash;
    }

    protected void addRow(ColumnType columnType, ArrayList<AbstractProjectSearchRow> rows) {
        if (!columnType.showInProjectSearch()) {
            return;
        }
        switch (columnType.getType()) {
            case VALUE:
            case VALUE_THESAURUS:
                rows.add(new ProjectSearchTextRow(columnType));
                break;
            case ID:
                rows.add(new ProjectSearchComboRow(columnType, mainFrame));
                break;
            case DATE:
            case TIME:
                rows.add(new ProjectSearchTextRow(columnType, AbstractProjectSearchRow.DataType.BIGGER));
                rows.add(new ProjectSearchTextRow(columnType, AbstractProjectSearchRow.DataType.SMALLER));
                break;
            case BOOLEAN:
                rows.add(new ProjectSearchBooleanRow(columnType));
                break;
        }
    }

    private int searchProjects() {
        try {
            UniqueArrayList<String> tableNames = new UniqueArrayList<>();
            String whereQuery = " WHERE ";
            whereQuery += getProjectQuery(tableNames);
            whereQuery += getEntryQuery(tableNames);
            whereQuery += IStandardProjectColumnTypes.PROJECT_DELETED + "='" + IStandardColumnTypes.DELETED_NO + "'";
            ArrayList<ProjectDataSet> result = mainFrame.getController().getProjectSearchResult(tableNames, whereQuery);
            if (result.isEmpty()) {
                wrapperResult.add(new JLabel("<html><b>" + Loc.get("NO_PROJECTS_FOUND_WITH_THESE_SEARCH_PARAMETERS") + "</b></html>"));
            } else {
                for (ProjectDataSet project : result) {
                    SearchResultRow row = getSearchResultRow(project);
                    wrapperResult.add(row);
                }
            }
            scrollToTop();
            return result.size();
        } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException | NoRightException ex) {
            Logger.getLogger(ProjectSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    protected SearchResultRow getSearchResultRow(ProjectDataSet project) {
        return new SearchResultRow(project);
    }

    private HashMap<String, ArrayList<AbstractProjectSearchRow>> getMappingForProjectRows() {
        HashMap<String, ArrayList<AbstractProjectSearchRow>> mapping = new HashMap<>();

        for (AbstractProjectSearchRow allProjectRow : allProjectRows) {
            if (allProjectRow.isEmpty()) {
                continue;
            }
            String tableName = ColumnHelper.getTableName(allProjectRow.getColumnType().getColumnName());
            ArrayList<AbstractProjectSearchRow> abstractProjectSearchRows = mapping.get(tableName);
            if (abstractProjectSearchRows == null) {
                abstractProjectSearchRows = new ArrayList<>();
                mapping.put(tableName, abstractProjectSearchRows);
            }
            abstractProjectSearchRows.add(allProjectRow);
        }
        return mapping;
    }

    private String getProjectQuery(UniqueArrayList<String> tableNames) {
        String whereQuery = "";
        HashMap<String, ArrayList<AbstractProjectSearchRow>> mapping = getMappingForProjectRows();
        for (Map.Entry<String, ArrayList<AbstractProjectSearchRow>> rows : mapping.entrySet()) {
            String tableName = rows.getKey();

            String where = "";
            if (!tableName.equals(IStandardProjectColumnTypes.TABLENAME_PROJECT)) {
                tableNames.add(tableName);
                where = IStandardProjectColumnTypes.PROJECT_ID_PROJECT + "=" + tableName + "." + IStandardColumnTypes.PROJECT_ID
                        + " AND " + IStandardProjectColumnTypes.PROJECT_DATABASENUMBER + "=" + tableName + "." + IStandardColumnTypes.PROJECT_DATABASE_ID + " AND ";
            }

            for (AbstractProjectSearchRow abstractProjectSearchRow : rows.getValue()) {

                ColumnType columnType = abstractProjectSearchRow.getColumnType();

                where = getWhereQueryFromList(tableNames, where, abstractProjectSearchRow, columnType);
                if (where.endsWith(" OR ")) {
                    where = where.substring(0, where.length() - 4);
                }
                where += ")";
                where += " AND ";

            }

            whereQuery += where;
        }
        return whereQuery;
    }

    private String getWhereQueryFromList(UniqueArrayList<String> tableNames, String where, AbstractProjectSearchRow abstractProjectSearchRow, ColumnType columnType) {
        switch (columnType.getType()) {
            case VALUE:
            case VALUE_THESAURUS:
            case DATE:
            case TIME:
                where += "(";
                switch (abstractProjectSearchRow.getDataType()) {
                    case EQUAL:
                        for (String s : abstractProjectSearchRow.getInput()) {
                            where += columnType + " = '" + s + "' OR ";
                        }
                        break;
                    case CONTAINS:
                        for (String s : abstractProjectSearchRow.getInput()) {
                            where += columnType + " LIKE '%" + s + "%' OR ";
                        }
                        break;
                    case BIGGER:
                        for (String s : abstractProjectSearchRow.getInput()) {
                            where += columnType + " >= '" + s + "' OR ";
                        }
                        break;
                    case SMALLER:
                        for (String s : abstractProjectSearchRow.getInput()) {
                            where += columnType + " <= '" + s + "' OR ";
                        }
                        break;
                }
                break;
            case ID:
                tableNames.add(columnType.getConnectedTableName());
                where += columnType + "=" + columnType.getConnectedTableName() + "." + IStandardColumnTypes.ID + " AND (";
                switch (abstractProjectSearchRow.getDataType()) {
                    case EQUAL:
                        for (String s : abstractProjectSearchRow.getInput()) {
                            where += columnType.getConnectedTableName() + "." + IStandardColumnTypes.VALUE + " = '" + s + "' OR ";
                        }
                        break;
                    case CONTAINS:
                        for (String s : abstractProjectSearchRow.getInput()) {
                            where += columnType.getConnectedTableName() + "." + IStandardColumnTypes.VALUE + " LIKE '%" + s + "%' OR ";
                        }
                        break;
                }
                break;

            //TODO different type? How handle yes no? in GUI or here?
            case BOOLEAN:
                where += "(";
                switch (abstractProjectSearchRow.getDataType()) {
                    case EQUAL:
                        for (String s : abstractProjectSearchRow.getInput()) {
                            where += columnType + " = '" + s + "' OR ";
                        }
                }

            default:

        }
        return where;
    }

    private HashMap<String, ArrayList<AbstractProjectSearchRow>> getMappingForEntryRows(ArrayList<AbstractProjectSearchRow> value) {
        HashMap<String, ArrayList<AbstractProjectSearchRow>> mapping = new HashMap<>();

        for (AbstractProjectSearchRow allProjectRow : value) {
            if (allProjectRow.isEmpty()) {
                continue;
            }
            String tableName = ColumnHelper.getTableName(allProjectRow.getColumnType().getColumnName());
            ArrayList<AbstractProjectSearchRow> abstractProjectSearchRows = mapping.get(tableName);
            if (abstractProjectSearchRows == null) {
                abstractProjectSearchRows = new ArrayList<>();
                mapping.put(tableName, abstractProjectSearchRows);
            }
            abstractProjectSearchRows.add(allProjectRow);
        }
        return mapping;
    }

    private String getEntryQuery(UniqueArrayList<String> tableNames) {
        String whereQuery = "";
        for (Map.Entry<AbstractBaseEntryManager, ArrayList<AbstractProjectSearchRow>> abstractBaseEntryManagerArrayListEntry : entryRows.entrySet()) {
            final String tableNameBaseManager = abstractBaseEntryManagerArrayListEntry.getKey().getTableName();

            HashMap<String, ArrayList<AbstractProjectSearchRow>> mapping = getMappingForEntryRows(abstractBaseEntryManagerArrayListEntry.getValue());
            for (Map.Entry<String, ArrayList<AbstractProjectSearchRow>> rows : mapping.entrySet()) {
                String tableName = rows.getKey();
                tableNames.add(tableName);
                String where = IStandardProjectColumnTypes.PROJECT_ID_PROJECT + "=" + tableName + "." + IStandardColumnTypes.PROJECT_ID
                        + " AND " + IStandardProjectColumnTypes.PROJECT_DATABASENUMBER + "=" + tableName + "." + IStandardColumnTypes.PROJECT_DATABASE_ID + " AND ";

                if (!tableName.equals(tableNameBaseManager)) {
                    tableNames.add(tableNameBaseManager);
                    where += tableNameBaseManager + "." + IStandardColumnTypes.ID + "=" + tableName + "." + IStandardColumnTypes.ID + " AND "
                            + tableNameBaseManager + "." + IStandardColumnTypes.DATABASE_ID + "=" + tableName + "." + IStandardColumnTypes.DATABASE_ID + " AND ";
                }

                for (AbstractProjectSearchRow abstractProjectSearchRow : rows.getValue()) {

                    ColumnType columnType = abstractProjectSearchRow.getColumnType();

                    where = getWhereQueryFromList(tableNames, where, abstractProjectSearchRow, columnType);
                    if (where.endsWith(" OR ")) {
                        where = where.substring(0, where.length() - 4);
                    }
                    where += ")";
                    where += " AND ";

                }

                whereQuery += where;
            }
        }
        return whereQuery;
    }

    protected class SearchResultRow extends JPanel {

        private JButton button;

        public SearchResultRow(final ProjectDataSet project) {
            setLayout(new BorderLayout());

            button = new JButton("<html>" + Loc.get("OPEN_LISTING_OF_PROJECT") + "</html>");
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        mainFrame.getController().loadProject(project, true);
                        Listing listing = mainFrame.getListingScreen();
                        for (Map.Entry<AbstractBaseEntryManager, ArrayList<AbstractProjectSearchRow>> entrySet : entryRows.entrySet()) {
                            AbstractBaseEntryManager key = entrySet.getKey();
                            ArrayList<AbstractProjectSearchRow> rows = entrySet.getValue();
                            Filter filter = new Filter(listing);
                            for (AbstractProjectSearchRow row : rows) {
                                if (row.getRawInput().isEmpty()) {
                                    continue;
                                }
                                ColumnType columnType = row.getColumnType();
                                switch (columnType.getType()) {
                                    case VALUE:
                                    case ID:
                                        if (row.getDataType() == AbstractProjectSearchRow.DataType.BIGGER) {
                                            filter.addFilterRowGreaterThan(columnType, row.getRawInput());
                                        } else if (row.getDataType() == AbstractProjectSearchRow.DataType.SMALLER) {
                                            filter.addFilterRowLowerThan(columnType, row.getRawInput());
                                        } else if (row.getDataType() == AbstractProjectSearchRow.DataType.EQUAL) {
                                            filter.addFilterRowIsText(columnType, row.getRawInput());
                                        } else if (row.getDataType() == AbstractProjectSearchRow.DataType.CONTAINS) {
                                            filter.addFilterRowContainsText(columnType, row.getRawInput());
                                        }
                                        break;
                                    case DATE:
                                    case TIME:
                                        if (row.getDataType() == AbstractProjectSearchRow.DataType.BIGGER) {
                                            filter.addFilterRowLaterThanDate(columnType, row.getRawInput());
                                        } else if (row.getDataType() == AbstractProjectSearchRow.DataType.SMALLER) {
                                            filter.addFilterRowEarlierThanDate(columnType, row.getRawInput());
                                        }
                                        break;
                                    default:
                                        System.out.println(" no case defined for " + columnType.getType().name() + " in columntype: " + columnType);
                                        break;
                                }
                            }
                            listing.setFilter(key, filter);
                            listing.applyFilter(true);
                        }
                        mainFrame.displayListingScreen(comboManager.getItemAt(comboManager.getSelectedIndex()));
                    } catch (StatementNotExecutedException | NotLoggedInException | NoRightException ex) {
                        Logger.getLogger(ProjectSearch.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            button.setPreferredSize(new Dimension(150, 40));
            add(BorderLayout.WEST, ComponentHelper.wrapComponent(button, 4, 6, 4, 0));

            String labelString = getString(project);
            add(new JLabel(labelString));
        }

        protected String getString(ProjectDataSet project) {
            String labelString = "<html>";
            labelString += "<b>" + project.getProjectName() + "</b>";
            try {
                String userName = mainFrame.getController().getUserName(project.getProjectOwnerId());
                labelString += " (" + userName + ")";
            } catch (NotLoggedInException | StatementNotExecutedException ex) {
                Logger.getLogger(ProjectSearch.class.getName()).log(Level.SEVERE, null, ex);
            }
            labelString += "<br>";
            labelString += "<small>(ID: " + project.getProjectId() + ", DBID: " + project.getProjectDatabaseId() + ")</small>";
            labelString += "</html>";
            return labelString;
        }

        public JButton getButton() {
            return button;
        }

    }

}
