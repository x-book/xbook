package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel.Position;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.AbstractSettingInputFieldVisibility;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractExtendedEntryManager;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.WrapLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XSubTitle;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Export extends AbstractContent {

    protected XImageButton buttonExport;
    protected XImageButton buttonSelectAll;
    protected XImageButton buttonSelectNone;
    /**
     * An arraylist that holds all JCheckBox objects.
     */
//    private final ArrayList<JCheckBox> allCheckBoxes;
    private final HashMap<IBaseManager, ArrayList<JCheckBox>> allCheckBoxes;
    private final HashMap<IBaseManager, JCheckBox> managerSelection;
    private HashMap<IBaseManager, TreeSet<ColumnType>> allColumnTypes;
    private final ArrayList<IBaseManager> allManagers;
    private int countManager;
    private boolean showInputFieldSelection;

    /**
     * Constructor.
     */
    public Export() {
        this(true);
    }

    /**
     * Constructor.
     *
     * @param showInputFieldSelection Display or hide the checkboxes to select the single input fields.
     */
    public Export(boolean showInputFieldSelection) {
        super();
        this.showInputFieldSelection = showInputFieldSelection;
        allCheckBoxes = new HashMap<>();
        managerSelection = new HashMap<>();
        allManagers = new ArrayList<>();
        init();
    }

    @Override
    protected JPanel getContent() {
        JPanel panel = new JPanel(new StackLayout());
        try {
            allColumnTypes = mainFrame.getController().getAvailableExportEntries();
            ArrayList<ColumnType.SectionAssignment> foundSections = new ArrayList<>();

            countManager = allColumnTypes.entrySet().size();

            // Buttons for selecting / deselecting ALL checkboxes, independent of section
            if (showInputFieldSelection) {
                panel.add(new XSubTitle(Loc.get("SELECT_ALL") + " / " + Loc.get("DESELECT_ALL")));

                JPanel allButtonPanel = new JPanel();
                JButton allButtonSelectAll = new JButton(Loc.get("SELECT_ALL"));
                allButtonSelectAll.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Iterator<Entry<IBaseManager, ArrayList<JCheckBox>>> it = allCheckBoxes.entrySet().iterator();
                        while (it.hasNext()) {
                            Entry<IBaseManager, ArrayList<JCheckBox>> c = it.next();
                            for (JCheckBox cb : c.getValue()) {
                                cb.setSelected(true);
                            }
                        }
                    }
                });
                allButtonPanel.add(allButtonSelectAll);
                JButton allButtonDeselectAll = new JButton(Loc.get("DESELECT_ALL"));
                allButtonDeselectAll.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Iterator<Entry<IBaseManager, ArrayList<JCheckBox>>> it = allCheckBoxes.entrySet().iterator();
                        while (it.hasNext()) {
                            Entry<IBaseManager, ArrayList<JCheckBox>> c = it.next();
                            for (JCheckBox cb : c.getValue()) {
                                cb.setSelected(false);
                            }
                        }
                    }
                });
                allButtonPanel.add(allButtonDeselectAll);
                panel.add(allButtonPanel);
            }

            // then create all checkboxes
            for (final Map.Entry<IBaseManager, TreeSet<ColumnType>> entry : allColumnTypes.entrySet()) {

                // add manager to "all managers" arraylist
                allManagers.add(entry.getKey());

                // look for the different sections
                for (ColumnType c : entry.getValue()) {
                    if (!foundSections.contains(c.getSectionProperty())) {
                        if (c.getSectionProperty() != null) {
                            foundSections.add(c.getSectionProperty());
                        }
                    }
                }

                // sort the sections to the correct order.
                Collections.sort(foundSections, new SortSections());

                JPanel titleBarWrapper = new JPanel(new BorderLayout());

                // add a elements to export single export collection, but only if there are more than 1
                if (countManager > 1) {
                    JPanel multiWrapper = new JPanel(new BorderLayout());

                    XImageButton singleExport = new XImageButton(Images.BUTTONPANEL_EXPORT);
                    singleExport.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            startExport(entry.getKey());
                        }
                    });
                    singleExport.setToolTipText(Loc.get("EXPORT"));

                    multiWrapper.add(BorderLayout.WEST, ComponentHelper.wrapComponent(singleExport, 3, 4, 2, 0));
                    JCheckBox managerCheck = new JCheckBox();
                    managerCheck.setSelected(true);
                    managerSelection.put(entry.getKey(), managerCheck);
                    multiWrapper.add(BorderLayout.CENTER, ComponentHelper.wrapComponent(managerCheck, 3, 4, 2, 0));

                    titleBarWrapper.add(BorderLayout.WEST, multiWrapper);

                    if (showInputFieldSelection) {
                        buttonSelectAll.setVisible(true);
                        buttonSelectNone.setVisible(true);
                    }
                    buttonExport.setIcon(Images.BUTTONPANEL_EXPORT_MULTI);
                }

                titleBarWrapper.add(BorderLayout.CENTER, new XTitle(entry.getKey().getLocalisedTableName()));
                panel.add(titleBarWrapper);

                int countSectionsToDisplay = containsMoreThanOneSectionToDisplay(foundSections);

                // iterate all sections
                ArrayList<JCheckBox> allCheckBoxesForManager = new ArrayList<>();
                allCheckBoxes.put(entry.getKey(), allCheckBoxesForManager);

                for (ColumnType.SectionAssignment section : foundSections) {
                    if (section.getSortedId() == AbstractExtendedEntryManager.COLUMN_SYSTEM
                            || section.getSortedId() == AbstractExtendedEntryManager.COLUMN_NODISPLAY) {
                        continue;
                    }
                    if (countSectionsToDisplay > 1) {
                        XTitle subTitle = new XTitle(section.getDisplayName());
                        subTitle.setBorderColor(Color.BLACK);
                        subTitle.setBackgroundBar(Colors.CONTENT_BACKGROUND);
                        subTitle.setForegroundBar(Colors.CONTENT_FOREGROUND);
                        panel.add(subTitle);
                        if (!showInputFieldSelection) {
                            subTitle.setVisible(false);
                        }
                    }

                    // create a panel where to add all elements of the section
                    JPanel sectionPanel = new JPanel(new WrapLayout());
                    final ArrayList<JCheckBox> allSectionBoxes = new ArrayList<>();
                    // look for elements of this section
                    for (ColumnType entries : entry.getValue()) {
                        if (entries.getSectionProperty() != null
                                && entries.getSectionProperty().getSortedId() != AbstractExtendedEntryManager.COLUMN_SYSTEM
                                && entries.getSectionProperty().getSortedId() != AbstractExtendedEntryManager.COLUMN_NODISPLAY) {
                            if (section.getDisplayName().equals(entries.getSectionProperty().getDisplayName())) {
                                JCheckBox check = new JCheckBox(entries.getDisplayName().replaceAll("<html>","" ).replaceAll("<br>","" ).replaceAll("</html>","" ));
                                check.setBackground(Colors.CONTENT_BACKGROUND);
                                check.setPreferredSize(new Dimension(180, 14));
                                check.setName(entries.getColumnName());
                                check.setSelected(true);
                                check.setToolTipText(entries.getDisplayName());
                                allSectionBoxes.add(check);
                                sectionPanel.add(check);

                                if (!showInputFieldSelection) {
                                    check.setVisible(false);
                                }
                            }
                        }
                    }
                    allCheckBoxesForManager.addAll(allSectionBoxes);

                    // add the section panel to the layout
                    if (countManager > 1) {
                        panel.add(ComponentHelper.wrapComponent(sectionPanel, 0, 10, 10, 80));
                    } else {
                        panel.add(ComponentHelper.wrapComponent(sectionPanel, 0, 10, 10, 10));
                    }
                    if (!showInputFieldSelection) {
                        sectionPanel.setVisible(false);
                    }

                    JPanel buttonPanel = new JPanel();
                    if (showInputFieldSelection) {
                        JButton buttonSelectAll = new JButton(Loc.get("SELECT_ALL_IN_SECTION"));
                        buttonSelectAll.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                for (JCheckBox check : allSectionBoxes) {
                                    check.setSelected(true);
                                }
                            }
                        });
                        buttonPanel.add(buttonSelectAll);
                        JButton buttonDeselectAll = new JButton(Loc.get("DESELECT_ALL_IN_SECTION"));
                        buttonDeselectAll.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                for (JCheckBox check : allSectionBoxes) {
                                    check.setSelected(false);
                                }
                            }
                        });
                        buttonPanel.add(buttonDeselectAll);
                        panel.add(buttonPanel);
                    }

                    if (!showInputFieldSelection) {
                        buttonPanel.setVisible(false);
                    }
                }
            }

        } catch (StatementNotExecutedException | NotLoggedInException ex) {
            Logger.getLogger(AbstractSettingInputFieldVisibility.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        ComponentHelper.colorAllChildren(panel, Colors.CONTENT_BACKGROUND);

        return panel;
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel buttonPanel = new ButtonPanel();

        buttonExport = buttonPanel.addImageButton(Position.NORTH_EAST, Images.BUTTONPANEL_EXPORT, Loc.get("EXPORT"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (countManager > 1) {
                            ArrayList<IBaseManager> selectedManagers = new ArrayList<>();
                            for (Entry<IBaseManager, JCheckBox> entry : managerSelection.entrySet()) {
                                if (entry.getValue().isSelected()) {
                                    selectedManagers.add(entry.getKey());
                                }
                            }
                            startExport(selectedManagers);
                        } else {
                            startExport();
                        }
                    }
                }).start();
            }
        });

        if (showInputFieldSelection) {
            buttonSelectAll = buttonPanel.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_SELECTALL, Loc.get("SELECT_ALL"), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    for (Entry<IBaseManager, JCheckBox> entry : managerSelection.entrySet()) {
                        entry.getValue().setSelected(true);
                    }
                }
            });
            buttonSelectAll.setStyle(XImageButton.Style.GRAY);
            buttonSelectAll.setVisible(false);

            buttonSelectNone = buttonPanel.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_SELECTNONE, Loc.get("REMOVE_ALL"), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    for (Entry<IBaseManager, JCheckBox> entry : managerSelection.entrySet()) {
                        entry.getValue().setSelected(false);
                    }
                }
            });
            buttonSelectNone.setStyle(XImageButton.Style.GRAY);
            buttonSelectNone.setVisible(false);
        }

        return buttonPanel;
    }

    /**
     * Run the export. Exports the data for all managers.
     */
    private void startExport() {
        startExport(allManagers);
    }

    /**
     * Run the export. Exports the data for one specific manager.
     */
    private void startExport(IBaseManager manager) {
        ArrayList list = new ArrayList();
        list.add(manager);
        startExport(list);
    }

    /**
     * Run the export. Exports the data for the selected managers.
     */
    private void startExport(ArrayList<IBaseManager> managers) {
        HashMap<IBaseManager, ArrayList<ColumnType>> entries = getSelectedEntries(managers);
        try {
            mainFrame.getController().startExport(entries, null);
        } catch (NotLoggedInException | StatementNotExecutedException | NoRightException | NotLoadedException ex) {
            Logger.getLogger(Export.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns an ArrayList holding all Strings of the name of the selected
     * checkboxes.
     */
    private HashMap<IBaseManager, ArrayList<ColumnType>> getSelectedEntries(ArrayList<IBaseManager> filter) {
        HashMap<IBaseManager, ArrayList<ColumnType>> selected = new HashMap<>();
        if (allColumnTypes != null) {
            // iterate all managers
            for (Entry<IBaseManager, ArrayList<JCheckBox>> e : allCheckBoxes.entrySet()) {

                // skip if manager is not in filter
                if (!filter.contains(e.getKey())) {
                    continue;
                }

                // create ArrayList of all selected check boxes for this manager
                ArrayList<ColumnType> values = new ArrayList<>();
                for (JCheckBox check : e.getValue()) {
                    if (check.isSelected()) {
                        // look for the different sections
                        for (ColumnType type : allColumnTypes.get(e.getKey())) {
                            if (check.getName().equals(type.getColumnName())) {
                                values.add(type);
                                break;
                            }
                        }
                    }
                }
                if (!values.isEmpty()) {
                    selected.put(e.getKey(), values);
                }
            }
        }
        return selected;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    /**
     * Methods that count all (different) sections, that should be displayed.
     * Sections that are system columns (like ID, DBID, etc...) or other
     * sections that are not to be displayed, will not be counted.
     *
     * @param foundSections
     * @return
     */
    private int containsMoreThanOneSectionToDisplay(ArrayList<ColumnType.SectionAssignment> foundSections) {
        UniqueArrayList<Integer> sections = new UniqueArrayList<>();
        for (ColumnType.SectionAssignment section : foundSections) {
            if (section.getSortedId() != AbstractExtendedEntryManager.COLUMN_SYSTEM
                    && section.getSortedId() != AbstractExtendedEntryManager.COLUMN_NODISPLAY) {
                sections.add(section.getSortedId());
            }
            ;
        }
        return sections.size();
    }

    /**
     * A custom class that allows comparing two SectionAssignment objects.
     */
    public class SortSections implements Comparator<ColumnType.SectionAssignment> {

        @Override
        public int compare(ColumnType.SectionAssignment a1, ColumnType.SectionAssignment a2) {
            return a1.getSortedId().compareTo(a2.getSortedId());
        }
    }
}
