package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType.SectionAssignment;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.settingPanel.AbstractSettingPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.WrapLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XSubTitle;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.IEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractExtendedEntryManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractSettingInputFieldVisibility extends AbstractSettingPanel {

    /**
     * An arraylist that holds all ColumnType objects that holds the data of the column information.
     */
    private ArrayList<ColumnType> allEntries;
    /**
     * An arraylist that holds all JCheckBox objects.
     */
    private ArrayList<JCheckBox> allCheckBoxes;
    /**
     * The basic mainframe object.
     */
    private IMainFrame mainFrame;
    /**
     * The panel that holds the standard content.
     */
    private JPanel inputfieldWrapper;
    /**
     * The panel that holds the error content.
     */
    private JPanel errorPanel;

    private HashMap<IBaseManager, TreeSet<ColumnType>> allColumnTypes;

    /**
     * Constructor.
     *
     * @param mainFrame The basic mainframe object.
     */
    public AbstractSettingInputFieldVisibility(IMainFrame mainFrame) {
        this.mainFrame = mainFrame;
        allEntries = new ArrayList<>();
        allCheckBoxes = new ArrayList<>();

        inputfieldWrapper = new JPanel(new StackLayout());
        errorPanel = new JPanel(new StackLayout());

        inputfieldWrapper.setVisible(true);
        errorPanel.setVisible(false);

        errorPanel.add(ComponentHelper.wrapComponent(createTextBlock(Colors.CONTENT_BACKGROUND, Loc.get(
                "INPUT_FIELD_VISIBILITY_INFORMATION")), 0, 10, 10, 10));
        errorPanel.add(ComponentHelper.wrapComponent(createTitleBar(Loc.get("ONLY_AVAILABLE_IF_A_PROJECT_IS_LOAD")),
                0, 10, 10, 10));
        if (mainFrame.getController().isProjectLoaded()) {
            try {
                inputfieldWrapper.add(ComponentHelper.wrapComponent(createTextBlock(Colors.CONTENT_BACKGROUND,
                        Loc.get("INPUT_FIELD_VISIBILITY_INFORMATION")), 0, 10, 10, 10));
                XSubTitle labelDescription = createTitleBar(Loc.get("VISIBILITY_SETTINGS_FOR_PROJECT_X",
                        mainFrame.getController().getCurrentProject().getProjectName()));
                inputfieldWrapper.add(ComponentHelper.wrapComponent(labelDescription, 0, 10, 10, 10));

                allColumnTypes = mainFrame.getController().getAvailableExportEntries();
                ArrayList<SectionAssignment> foundSections = new ArrayList<>();

                for (Map.Entry<IBaseManager, TreeSet<ColumnType>> entry : allColumnTypes.entrySet()) {

                    // look for the different sections
                    for (ColumnType c : entry.getValue()) {
                        if (!foundSections.contains(c.getSectionProperty())) {
                            if (c.getSectionProperty() != null) {
                                foundSections.add(c.getSectionProperty());
                            }
                        }
                    }

                    // sort the sections to the correct order.
                    Collections.sort(foundSections, new SortSections());

                    inputfieldWrapper.add(ComponentHelper.wrapComponent(new XTitle(entry.getKey().getLocalisedTableName()), 0, 10, 10, 10));

                    int countSectionsToDisplay = containsMoreThanOneSectionToDisplay(foundSections);

                    // iterate all sections
                    for (ColumnType.SectionAssignment section : foundSections) {
                        if (section.getSortedId() == AbstractExtendedEntryManager.COLUMN_SYSTEM
                                || section.getSortedId() == AbstractExtendedEntryManager.COLUMN_NODISPLAY) {
                            continue;
                        }
                        if (countSectionsToDisplay > 1) {
                            XTitle subTitle = new XTitle(section.getDisplayName());
                            subTitle.setBorderColor(Color.BLACK);
                            subTitle.setBackgroundBar(Colors.CONTENT_BACKGROUND);
                            subTitle.setForegroundBar(Colors.CONTENT_FOREGROUND);
                            inputfieldWrapper.add(ComponentHelper.wrapComponent(subTitle, 0, 10, 10, 10));
                        }

                        final ArrayList<JCheckBox> allCheckboxesOfThisSection = new ArrayList<>();

                        // create a panel where to add all elements of the section
                        JPanel sectionPanel = new JPanel(new WrapLayout());

                        // look for elements of this section
                        for (ColumnType entries : entry.getValue()) {
                            if (entries.isHideInVisibilityList()) {
                                continue;
                            }
                            if (entries.getSectionProperty() != null) {
                                if (section.getDisplayName().equals(entries.getSectionProperty().getDisplayName())) {
                                    String name = entries.getDisplayName();

                                    // remove the line break an the html tags to avoid two-column labels (do not fit)
                                    if (name.contains("<br>") || name.contains("<br/>")) {
                                        name = name.replace("<html>", "");
                                        name = name.replace("</html>", "");
                                        name = name.replace("<br>", " ");
                                        name = name.replace("</br>", " ");
                                    }

                                    System.out.println("- " + name);
                                    JCheckBox check = new JCheckBox(name);
                                    check.setToolTipText(name);

                                    int checkWidth = 180;
                                    check.setPreferredSize(new Dimension(checkWidth, 14));
                                    check.setName(entries.getColumnName());
                                    if (entries.isMandatory()) {
                                        check.setEnabled(false);
                                        check.setSelected(true);
                                    }
                                    allCheckBoxes.add(check);
                                    allEntries.add(entries);
                                    allCheckboxesOfThisSection.add(check);
                                    sectionPanel.add(check);
                                }
                            }
                        }

                        // add the section panel to the layout
                        inputfieldWrapper.add(ComponentHelper.wrapComponent(sectionPanel, 0, 10, 10, 10));

                        JPanel buttonPanel = new JPanel();
                        JButton buttonSelectAll = new JButton(Loc.get("SELECT_ALL"));
                        buttonSelectAll.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                for (JCheckBox c : allCheckboxesOfThisSection) {
                                    if (c.isEnabled()) {
                                        c.setSelected(true);
                                    }
                                }
                            }
                        });
                        buttonPanel.add(buttonSelectAll);
                        JButton buttonDeselectAll = new JButton(Loc.get("DESELECT_ALL"));
                        buttonDeselectAll.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                for (JCheckBox c : allCheckboxesOfThisSection) {
                                    if (c.isEnabled()) {
                                        c.setSelected(false);
                                    }
                                }
                            }
                        });
                        buttonPanel.add(buttonDeselectAll);
                        inputfieldWrapper.add(buttonPanel);

                    }

                }

            } catch (StatementNotExecutedException ex) {
                Logger.getLogger(AbstractSettingInputFieldVisibility.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NotLoggedInException | NotLoadedException ex) {
                inputfieldWrapper.setVisible(false);
                errorPanel.setVisible(true);
            }
        } else {
            inputfieldWrapper.setVisible(false);
            errorPanel.setVisible(true);
        }

        setLayout(new StackLayout());
        add(errorPanel);
        add(inputfieldWrapper);
        ComponentHelper.colorAllChildren(this, Colors.CONTENT_BACKGROUND);
    }

    /**
     * Methods that count all (different) sections, that should be displayed. Sections that are system columns (like ID,
     * DBID, etc...) or other sections that are not to be displayed, will not be counted.
     *
     * @param foundSections
     * @return
     */
    private int containsMoreThanOneSectionToDisplay(ArrayList<ColumnType.SectionAssignment> foundSections) {
        UniqueArrayList<Integer> sections = new UniqueArrayList<>();
        for (ColumnType.SectionAssignment section : foundSections) {
            if (section.getSortedId() != AbstractExtendedEntryManager.COLUMN_SYSTEM
                    && section.getSortedId() != AbstractExtendedEntryManager.COLUMN_NODISPLAY) {
                sections.add(section.getSortedId());
            }
            ;
        }
        return sections.size();
    }

    /**
     * Returns a Preferences object that hold the information where to save the settings in the registry.
     * <p/>
     * In general should return the object that is predefined in the Configuration class.
     *
     * @return A Preferences object that hold the information where to save the settings in the registry
     */
    protected Preferences getPreferences() {
        return AbstractConfiguration.bookConfig;
    }

    @Override
    public void save() {
        for (JCheckBox c : allCheckBoxes) {
            try {
                XBookConfiguration.setInputFieldProperty(getPreferences(),
                        mainFrame.getController().getCurrentProject().getProjectKey(), c.getName(), c.isSelected());
            } catch (NotLoadedException ex) {
                Logger.getLogger(AbstractSettingInputFieldVisibility.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        for (IEntry entry : mainFrame.getAllAvailableEntryPanels()) {
            entry.updateInputFieldVisibility();
        }
    }

    @Override
    public void reset() {
        for (JCheckBox c : allCheckBoxes) {
            try {
                c.setSelected(XBookConfiguration.getInputFieldProperty(getPreferences(),
                        mainFrame.getController().getCurrentProject().getProjectKey(), c.getName()));
            } catch (NotLoadedException ex) {
                inputfieldWrapper.setVisible(false);
                errorPanel.setVisible(true);
            }
        }
    }

    @Override
    public void setDefaultValues() {
        for (JCheckBox c : allCheckBoxes) {
            c.setSelected(true);
        }
    }

    /**
     * A custom class that allows comparing two SectionAssignment objects.
     */
    public class SortSections implements Comparator<SectionAssignment> {

        @Override
        public int compare(SectionAssignment a1, SectionAssignment a2) {
            return a1.getSortedId().compareTo(a2.getSortedId());
        }
    }
}
