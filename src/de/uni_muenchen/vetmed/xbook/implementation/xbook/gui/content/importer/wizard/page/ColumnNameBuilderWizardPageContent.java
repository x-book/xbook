package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.page;

import javax.swing.JTable;
import javax.swing.table.TableRowSorter;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.AbstractLookupTableBuilderWizardPageContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.AbstractLookupTableEditorContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.ImportWizard;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.WizardPage;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.page.model.ColumnNameTableModel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;

public abstract class ColumnNameBuilderWizardPageContent extends AbstractLookupTableBuilderWizardPageContent {

    private MultiLineTextLabel displayMissingMandatoryField;

    public ColumnNameBuilderWizardPageContent() {
    }

    @Override
    public void setPage(WizardPage page) {
        super.setPage(page);
        init();
    }

    protected abstract ColumnNameTableModel makeTableModel();

    @Override
    public abstract String getTitle();

    protected void checkCompleteness(ColumnNameTableModel model) {
        boolean complete = model.isMapComplete();
        updateDisplayMissingMandatoryField(model);
        // TODO ugly
        if (complete) {
            ((ImportWizard) getWizardPage().getWizard()).setColumnlookupTable(model.createLookupTable());
        }
        setPageComplete(complete);
    }

    protected abstract AbstractLookupTableEditorContent getLookupTableEditorContent(Collection<ColumnType> data);

    @Override
    protected JTable makeTable() {
        final JTable table = new JTable();

        ColumnNameTableModel model = makeTableModel();
        AbstractLookupTableEditorContent editor = getLookupTableEditorContent(model.getCodomain());

        table.setModel(model);
        TableRowSorter<ColumnNameTableModel> sorter = new TableRowSorter<>(model);

        table.setRowSorter(sorter);
        sorter.sort();

        // set xBook field clickable
        table.getColumnModel().getColumn(model.getEditorColumnIndex()).setCellEditor(editor);

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    table.editCellAt(table.getSelectedRow(), 1);
                }
            }
        });

        checkCompleteness(model);

        return table;
    }

    @Override
    protected JComponent getMissingMandatoryFieldDisplay() {
        JPanel missingWrapper = new JPanel(new BorderLayout());
        missingWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        missingWrapper.setBorder(BorderFactory.createEmptyBorder(0, 30, 6, 30));

        if (displayMissingMandatoryField == null) {
            displayMissingMandatoryField = new MultiLineTextLabel("");
            displayMissingMandatoryField.setBackground(Colors.CONTENT_BACKGROUND);
        }
        missingWrapper.add(BorderLayout.CENTER, displayMissingMandatoryField);
        return missingWrapper;
    }

    private void updateDisplayMissingMandatoryField(ColumnNameTableModel model) {
        ArrayList<ColumnType> missing = model.getMissingColumns();
        displayMissingMandatoryField.setVisible(!missing.isEmpty());
        if (missing.isEmpty()) {
            return;
        }

        String insert = "";
        for (ColumnType c : missing) {
            insert += c.getDisplayName() + ", ";
        }
        insert = insert.substring(0, insert.length() - ", ".length());
        displayMissingMandatoryField.setText("Es fehlen noch " + missing.size() + " Pflichtfelder:\n" + insert);
    }
}
