package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IPlaceHolder;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * A place holder as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputPlaceHolder extends AbstractInputElement implements IPlaceHolder {

    private JLabel label;

    /**
     * Constructor.
     *
     * @param title The title that should be displayed as field description.
     */
    public InputPlaceHolder(String title) {
        super(new ColumnType(title, ColumnType.Type.VALUE, ColumnType.ExportType.ALL).setColumnName(title));
        setTitle(title);
    }

    /**
     * Constructor.
     *
     * @param title       The displayed title of the input field.
     * @param gridX       The number of grids that are used for the height of the
     *                    input field.
     * @param gridY       The number of grids that are used for the weight of the
     *                    input field.
     * @param sidebarText The text that is displayed in the sidebar.
     */
    @Deprecated
    public InputPlaceHolder(String title, int gridX, int gridY, String sidebarText) {
        super(new ColumnType("", ColumnType.Type.VALUE, ColumnType.ExportType.ALL).setColumnName(title));
        setTitle(title);

        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebarText);
    }

    public InputPlaceHolder(String title, int gridX, int gridY) {
        this(title, gridX, gridY, null);
    }

    @Override
    public void load(DataSetOld data) {
        // NOTHING TO DO - ARR!
    }

    @Override
    public void clear() {
        // NOTHING TO DO - ARR!
    }

    @Override
    public void save(DataSetOld data) {
        // NOTHING TO DO - ARR!
    }

    @Override
    public String getStringRepresentation() {
        return "n.a.";
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> list = new ArrayList<>();
        list.add(label);
        return list;
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    @Override
    public void setEnabled(boolean b) {
        if (b) {
            label.setForeground(Color.BLACK);
        } else {
            label.setForeground(Color.LIGHT_GRAY);
        }
    }


    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        label = new JLabel("Place Holder");

        multiPanel.add(BorderLayout.CENTER, label);

        setContent(multiPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

}
