package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiDateChooser;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawMultiDateChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IMultiDateChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiDateElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputMultiDateChooser extends AbstractDefaultInputElement implements IMultiDateChooser {

    private final ColumnType columnTypeEnd;

    protected RawMultiDateChooser zField;

    public InputMultiDateChooser(ColumnType columnTypeBegin, ColumnType columnTypeEnd) {
        super(columnTypeBegin);
        this.columnTypeEnd = columnTypeEnd;

        setGridX(2);
        setGridY(2);
    }

    @Override
    public AbstractInputElementFunctions createInputField() {
        if (zField == null) {
            zField = new RawMultiDateChooser(columnType, columnTypeEnd);
        }
        return zField;
    }

    @Override
    public void setMode(Mode mode) {
        super.setMode(mode);
        if (mode == Mode.STACK) {
            setPreferredSize(new Dimension(Sizes.STACK_DEFAULT_WIDTH, 140));
        }
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return zField.getMyFocusableComponents();
    }

    @Override
    public boolean addElement(MultiDateElement element) {
        return zField.addElement(element);
    }

    @Override
    public boolean addElements(List<MultiDateElement> element) {
        return zField.addElements(element);
    }

    @Override
    public List<MultiDateElement> getElementsFromList() {
        return zField.getElementsFromList();
    }

    @Override
    public MultiDateElement getElementFromInput() {
        return zField.getElementFromInput();
    }

    @Override
    public void removeElement(MultiDateElement element) {
        zField.removeElement(element);
    }

    @Override
    public void removeElements(List<MultiDateElement> elements) {
        zField.removeElements(elements);
    }

    @Override
    public void removeElementAt(int index) {
        zField.removeElementAt(index);
    }

    public MultiDateChooser getMultiDateChooser() {
        return zField.getMultiDateChooser();
    }

}
