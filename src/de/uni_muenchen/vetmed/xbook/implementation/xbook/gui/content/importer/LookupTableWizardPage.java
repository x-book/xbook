package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer;


import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.AbstractWizardPageContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.WizardPage;
import java.util.ArrayList;

public class LookupTableWizardPage extends WizardPage {

    private ArrayList<String> columnValues = null;

    public LookupTableWizardPage(AbstractWizardPageContent wizardPageContentClass) {
        super(wizardPageContentClass);
    }

    public ArrayList<String> getColumnValues() {
        return columnValues;
    }

    public void setColumnValues(ArrayList<String> columnValues) {
        this.columnValues = columnValues;
    }
}
