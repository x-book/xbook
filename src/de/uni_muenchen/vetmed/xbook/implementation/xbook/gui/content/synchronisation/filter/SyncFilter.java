package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.filter;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractProjectManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.Synchronisation;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * This frame is used for the synchronization.
 * <p>
 * <p>
 * It displays two lists, one of the locally available projects, one of the
 * globally available projects, from which the user may select which projects to
 * synchronize. </p>
 *
 * @author fnuecke
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class SyncFilter extends AbstractContent {

    private Synchronisation syncPanel;
    private JPanel filterRowWrapper;
    private JButton addContainsTextLineButton;

    protected AbstractSyncFilterRow rowProjectName;
    protected AbstractSyncFilterRow rowProjectOwner;

    public SyncFilter(final Synchronisation syncPanel) {
        super();
        this.syncPanel = syncPanel;
        init();
    }

    @Override
    protected JPanel getContent() {
        JPanel wrapperPanel = new JPanel(new StackLayout());
        wrapperPanel.add(new XTitle(Loc.get("SYNCHRONIZATION_FILTER")));

        // generate the rows
        for (AbstractSyncFilterRow row : getProjectRows()) {
            wrapperPanel.add(row);
        }

        return wrapperPanel;
    }

    protected ArrayList<AbstractSyncFilterRow> getProjectRows() {
        ArrayList<AbstractSyncFilterRow> rows = new ArrayList<>();
        rows.add(rowProjectName = new SyncFilterRowTextField(AbstractProjectManager.PROJECT_PROJECTNAME.getDisplayName()));
        rows.add(rowProjectOwner = new SyncFilterRowTextField(AbstractProjectManager.PROJECT_PROJECTOWNER.getDisplayName()));
        return rows;
    }

    public SyncFilterData getCurrentSelection() {
        SyncFilterData data = new SyncFilterData();
        data.setValueProjectName(rowProjectName.getCurrentData());
        data.setValueProjectOwner(rowProjectOwner.getCurrentData());
        return data;
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        XImageButton clearButton = panel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_CLEAR, Loc.get("CLEAR"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });
        clearButton.setStyle(XImageButton.Style.GRAY);

        panel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_CONFIRM, Loc.get("CONFIRM"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Content.setContent(syncPanel);
            }
        });

        return panel;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    public void reset() {
        rowProjectName.reset();
        rowProjectOwner.reset();
    }
}
