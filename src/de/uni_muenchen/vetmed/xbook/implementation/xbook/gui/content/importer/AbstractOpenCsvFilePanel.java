package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JTable;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;

import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import javax.swing.JCheckBox;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XCheckBox;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XCollapsibleTitle;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XRadioButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTextField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer.CsvTable;
import java.awt.BorderLayout;
import java.awt.Color;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;

import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import java.awt.Dimension;
import java.nio.charset.Charset;
import java.util.ArrayList;
import javax.swing.JComboBox;

public abstract class AbstractOpenCsvFilePanel extends JPanel {

	private String[][] table = null;
	private XTitle oTitle;
	private XCollapsibleTitle collapsibleTitle;
	private JTable previewTable;
	private JTextField fileTextField;
	private XButton btnOpen;
	private JComboBox charsetComboBox;
	private JTextField quoteTextField;
	private JTextField escapeTextField;
	private JTextField headerTextField;
	private JCheckBox chckbxIgnoreLeadingWhite;
	private JCheckBox chckbxStrictQuotes;
	private ButtonGroup separatorButtonGroup;
	private JRadioButton rdbtnSemicolon;
	private JRadioButton rdbtnComma;
	private JRadioButton rdbtnTabulator;
	private final char DEFAULT_QUOTE = '\"';
	private final char DEFAULT_ESCAPE = '\\';
	private final int SEPARATOR_SEMICOLON = 0;
	private final int SEPARATOR_COMMA = 1;
	private final int SEPARATOR_TABULATOR = 2;
	private int previewRows;
	private TableChangedListener listener = null;
	private JPanel separatorPanel;
	private JPanel advancedOptionPanel;
	private JPanel previewPanel;

	private JPanel createAlphaLabelWrapper() {
		JPanel alphaLabelWrapper = new JPanel(new BorderLayout());
		JPanel alphaLabelInnerWrapper = new JPanel(new StackLayout());
		alphaLabelWrapper.setBackground(Color.YELLOW);
		alphaLabelWrapper.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		for (String s : getAlphaLabels()) {
			MultiLineTextLabel label = new MultiLineTextLabel(s);
			label.setBackground(Color.YELLOW);
			label.setFont(Fonts.FONT_SMALL_BOLD);
			alphaLabelInnerWrapper.add(label);
		}
		alphaLabelWrapper.add(BorderLayout.CENTER, alphaLabelInnerWrapper);

		return alphaLabelWrapper;
	}

	protected ArrayList<String> getAlphaLabels() {
		ArrayList<String> list = new ArrayList();
		list.add("<b>Please notice!</b>");
		list.add("The importer tool is a new feature and a very early release. "
				+ "It is working and can be used to import data to the xBook database, "
				+ "however there are some minor limitations:");
		list.add("- dates can only be imported in the format YYYY-MM-DD (e.g. \"1990-12-31\")");
		list.add("- some complex input fields cannot be imported");
		list.add("Please keep in mind that errors may occure while importing data files.");
		list.add("Please notice that errors may still occure if some data is saved in the wrong format. Feel free to test the importer, but only use this importer with real data if you know what you are doing and check the result after the import process.");
		return list;
	}

	public interface TableChangedListener {

		public void tableChanged(AbstractOpenCsvFilePanel src);
	}

	public AbstractOpenCsvFilePanel() {
		this(99999999);
	}

	public AbstractOpenCsvFilePanel(int previewRows) {
		this.previewRows = previewRows;

		setLayout(new BorderLayout());
		setBackground(Colors.CONTENT_BACKGROUND);
		reset();

	}

	public void reset() {
		removeAll();
		JPanel topPanel = new JPanel(new StackLayout());

		topPanel.add(createAlphaLabelWrapper());

		topPanel.setBackground(Colors.CONTENT_BACKGROUND);

		oTitle = new XTitle("");
		topPanel.add(oTitle);

		// File Chooser Line
		JPanel defaultOptionPanel = new JPanel();
		defaultOptionPanel.setBackground(Colors.CONTENT_BACKGROUND);
		topPanel.add(defaultOptionPanel);

		GridBagLayout gbl_defaultOpenFileTabPanel = new GridBagLayout();
		gbl_defaultOpenFileTabPanel.columnWidths = new int[]{100, 0, 0, 0};
		gbl_defaultOpenFileTabPanel.rowHeights = new int[]{0, 0, 0};
		gbl_defaultOpenFileTabPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_defaultOpenFileTabPanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		defaultOptionPanel.setLayout(gbl_defaultOpenFileTabPanel);

		JLabel lblFile = new XLabel(Loc.get("FILE"));
		GridBagConstraints gbc_lblFile = new GridBagConstraints();
		gbc_lblFile.anchor = GridBagConstraints.EAST;
		gbc_lblFile.insets = new Insets(0, 0, 5, 5);
		gbc_lblFile.gridx = 0;
		gbc_lblFile.gridy = 0;
		defaultOptionPanel.add(lblFile, gbc_lblFile);

		fileTextField = new XTextField(true);
		GridBagConstraints gbc_fileTextField = new GridBagConstraints();
		gbc_fileTextField.insets = new Insets(0, 0, 5, 5);
		gbc_fileTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_fileTextField.gridx = 1;
		gbc_fileTextField.gridy = 0;
		defaultOptionPanel.add(fileTextField, gbc_fileTextField);
		fileTextField.setColumns(10);

		btnOpen = new XButton(Loc.get("OPEN"));
		GridBagConstraints gbc_btnOpen = new GridBagConstraints();
		gbc_btnOpen.anchor = GridBagConstraints.WEST;
		gbc_btnOpen.insets = new Insets(0, 0, 5, 0);
		gbc_btnOpen.gridx = 2;
		gbc_btnOpen.gridy = 0;
		defaultOptionPanel.add(btnOpen, gbc_btnOpen);

		// Separator Selection Line
		separatorPanel = new JPanel();
		separatorPanel.setVisible(false);
		separatorPanel.setBackground(Colors.CONTENT_BACKGROUND);
		topPanel.add(separatorPanel);

		GridBagLayout gbl_selectionPanel = new GridBagLayout();
		gbl_selectionPanel.columnWidths = new int[]{100, 0, 0, 0};
		gbl_selectionPanel.rowHeights = new int[]{0, 0, 0};
		gbl_selectionPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_selectionPanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		separatorPanel.setLayout(gbl_selectionPanel);

		JLabel lblSeparator = new XLabel(Loc.get("SEPARATOR"));
		GridBagConstraints gbc_lblSeparator = new GridBagConstraints();
		gbc_lblSeparator.anchor = GridBagConstraints.EAST;
		gbc_lblSeparator.insets = new Insets(0, 0, 0, 5);
		gbc_lblSeparator.gridx = 0;
		gbc_lblSeparator.gridy = 0;
		separatorPanel.add(lblSeparator, gbc_lblSeparator);

		JPanel separatorPanelBoxes = new JPanel();
		separatorPanelBoxes.setBackground(Colors.CONTENT_BACKGROUND);
		GridBagConstraints gbc_separatorPanel = new GridBagConstraints();
		gbc_separatorPanel.insets = new Insets(0, 0, 0, 5);
		gbc_separatorPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_separatorPanel.gridx = 1;
		gbc_separatorPanel.gridy = 0;
		separatorPanel.add(separatorPanelBoxes, gbc_separatorPanel);
		separatorPanelBoxes.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		rdbtnSemicolon = new XRadioButton(Loc.get("SEMICOLON") + " ( ; )");
		rdbtnSemicolon.setSelected(true);
		rdbtnSemicolon.setMnemonic(SEPARATOR_SEMICOLON);
		separatorPanelBoxes.add(rdbtnSemicolon);

		rdbtnComma = new XRadioButton(Loc.get("COMMA") + " ( , )");
		rdbtnComma.setMnemonic(SEPARATOR_COMMA);
		separatorPanelBoxes.add(rdbtnComma);

		rdbtnTabulator = new XRadioButton(Loc.get("TABULATOR"));
		rdbtnTabulator.setMnemonic(SEPARATOR_TABULATOR);
		separatorPanelBoxes.add(rdbtnTabulator);

		separatorButtonGroup = new ButtonGroup();
		separatorButtonGroup.add(rdbtnSemicolon);
		separatorButtonGroup.add(rdbtnComma);
		separatorButtonGroup.add(rdbtnTabulator);

		// Advances option row
		advancedOptionPanel = new JPanel(new StackLayout());
		advancedOptionPanel.setVisible(false);
		advancedOptionPanel.setBackground(Colors.CONTENT_BACKGROUND);
		topPanel.add(advancedOptionPanel);

		collapsibleTitle = new XCollapsibleTitle(Loc.get("ADVANCED_OPTIONS"));
		advancedOptionPanel.add(collapsibleTitle);

		JPanel advancedOptionContent = new JPanel();
		advancedOptionContent.setBackground(Colors.CONTENT_BACKGROUND);
		advancedOptionPanel.add(advancedOptionContent);

		GridBagLayout gbl_advancedOpenFileTabPanel = new GridBagLayout();
		gbl_advancedOpenFileTabPanel.columnWidths = new int[]{100, 0, 0, 0, 0};
		gbl_advancedOpenFileTabPanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_advancedOpenFileTabPanel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_advancedOpenFileTabPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		advancedOptionContent.setLayout(gbl_advancedOpenFileTabPanel);

		JLabel lblCharset = new XLabel(Loc.get("CHARSET"));
		GridBagConstraints gbc_lblCharset = new GridBagConstraints();
		gbc_lblCharset.anchor = GridBagConstraints.EAST;
		gbc_lblCharset.insets = new Insets(0, 0, 5, 5);
		gbc_lblCharset.gridx = 0;
		gbc_lblCharset.gridy = 0;
		advancedOptionContent.add(lblCharset, gbc_lblCharset);
		Charset cp = Charset.forName("Cp1252");
		Charset iso = Charset.forName("ISO-8859-1");
		System.out.println(cp.equals(iso));
		System.out.println(cp.name());
		System.out.println(iso.name());
		String[] comboContentChar = {"Cp1252", "ISO-8859-1", "UTF-8", "UTF-16", "UTF-16BE", "UTF-16LE", "US-ASCII"};
		charsetComboBox = new JComboBox(comboContentChar);
		charsetComboBox.setSize(new Dimension(1, 30));
		charsetComboBox.setPreferredSize(new Dimension(1, 30));
		GridBagConstraints gbc_charsetComboBox = new GridBagConstraints();
		gbc_charsetComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_charsetComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_charsetComboBox.gridx = 1;
		gbc_charsetComboBox.gridy = 0;
		advancedOptionContent.add(charsetComboBox, gbc_charsetComboBox);

		JLabel lblQuote = new XLabel(Loc.get("QUOTE"));
		GridBagConstraints gbc_lblQuote = new GridBagConstraints();
		gbc_lblQuote.anchor = GridBagConstraints.EAST;
		gbc_lblQuote.insets = new Insets(0, 0, 5, 5);
		gbc_lblQuote.gridx = 0;
		gbc_lblQuote.gridy = 1;
		advancedOptionContent.add(lblQuote, gbc_lblQuote);

		quoteTextField = new XTextField(true);
		quoteTextField.setDocument(new CharFieldDocument());
		quoteTextField.setText("\"");
		GridBagConstraints gbc_quoteTextField = new GridBagConstraints();
		gbc_quoteTextField.insets = new Insets(0, 0, 5, 5);
		gbc_quoteTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_quoteTextField.gridx = 1;
		gbc_quoteTextField.gridy = 1;
		advancedOptionContent.add(quoteTextField, gbc_quoteTextField);
		quoteTextField.setColumns(10);

		chckbxStrictQuotes = new XCheckBox(Loc.get("STRICT_QUOTES"));
		GridBagConstraints gbc_chckbxStrictQuotes = new GridBagConstraints();
		gbc_chckbxStrictQuotes.anchor = GridBagConstraints.WEST;
		gbc_chckbxStrictQuotes.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxStrictQuotes.gridx = 2;
		gbc_chckbxStrictQuotes.gridy = 1;
		advancedOptionContent.add(chckbxStrictQuotes, gbc_chckbxStrictQuotes);

		chckbxIgnoreLeadingWhite = new XCheckBox(Loc.get("IGNORE_LEADING_WHITE_SPACE"));
		chckbxIgnoreLeadingWhite.setSelected(true);
		GridBagConstraints gbc_chckbxIgnoreLeadingWhite = new GridBagConstraints();
		gbc_chckbxIgnoreLeadingWhite.anchor = GridBagConstraints.WEST;
		gbc_chckbxIgnoreLeadingWhite.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxIgnoreLeadingWhite.gridx = 3;
		gbc_chckbxIgnoreLeadingWhite.gridy = 1;
		advancedOptionContent.add(chckbxIgnoreLeadingWhite, gbc_chckbxIgnoreLeadingWhite);

		JLabel lblEscape = new XLabel(Loc.get("ESCAPE_CHARACTER"));
		GridBagConstraints gbc_lblEscape = new GridBagConstraints();
		gbc_lblEscape.anchor = GridBagConstraints.EAST;
		gbc_lblEscape.insets = new Insets(0, 0, 5, 5);
		gbc_lblEscape.gridx = 0;
		gbc_lblEscape.gridy = 2;
		advancedOptionContent.add(lblEscape, gbc_lblEscape);

		escapeTextField = new XTextField(true);
		escapeTextField.setDocument(new CharFieldDocument());
		escapeTextField.setText("\\");
		GridBagConstraints gbc_escapeTextField = new GridBagConstraints();
		gbc_escapeTextField.insets = new Insets(0, 0, 5, 5);
		gbc_escapeTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_escapeTextField.gridx = 1;
		gbc_escapeTextField.gridy = 2;
		advancedOptionContent.add(escapeTextField, gbc_escapeTextField);
		escapeTextField.setColumns(10);

		JLabel lblHeader = new XLabel(Loc.get("HEADER"));
		GridBagConstraints gbc_lblHeader = new GridBagConstraints();
		gbc_lblHeader.anchor = GridBagConstraints.EAST;
		gbc_lblHeader.insets = new Insets(0, 0, 0, 5);
		gbc_lblHeader.gridx = 0;
		gbc_lblHeader.gridy = 3;
		advancedOptionContent.add(lblHeader, gbc_lblHeader);

		headerTextField = new XTextField(true);
		headerTextField.setDocument(new IntegerFieldDocument());
		headerTextField.setText("1");
		GridBagConstraints gbc_headerTextField = new GridBagConstraints();
		gbc_headerTextField.insets = new Insets(0, 0, 0, 5);
		gbc_headerTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_headerTextField.gridx = 1;
		gbc_headerTextField.gridy = 3;
		advancedOptionContent.add(headerTextField, gbc_headerTextField);
		headerTextField.setColumns(10);

		// Preview option row
		previewPanel = new JPanel(new BorderLayout());
		previewPanel.setVisible(false);
		previewPanel.setBackground(Colors.CONTENT_BACKGROUND);
		previewPanel.add(BorderLayout.NORTH, new XTitle(Loc.get("PREVIEW")));

		previewTable = new JTable();
		previewTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		JScrollPane scrollPane = new JScrollPane(previewTable);
		scrollPane.setBackground(Colors.CONTENT_BACKGROUND);
		previewPanel.add(BorderLayout.CENTER, scrollPane);

		add(BorderLayout.NORTH, topPanel);
		add(BorderLayout.CENTER, previewPanel);

		initStuff();

		// hide advanced option panel
		collapsibleTitle.setElementToHide(advancedOptionContent);
		collapsibleTitle.setCollapsed(true);

		revalidate();
		repaint();
	}

	private void initStuff() {
		chckbxIgnoreLeadingWhite.addChangeListener(new ToggleButtonSettingsListener(chckbxIgnoreLeadingWhite.isSelected()));
		chckbxStrictQuotes.addChangeListener(new ToggleButtonSettingsListener(chckbxStrictQuotes.isSelected()));

		rdbtnTabulator.addChangeListener(new ToggleButtonSettingsListener(rdbtnTabulator.isSelected()));
		rdbtnComma.addChangeListener(new ToggleButtonSettingsListener(rdbtnComma.isSelected()));
		rdbtnSemicolon.addChangeListener(new ToggleButtonSettingsListener(rdbtnSemicolon.isSelected()));

		fileTextField.addFocusListener(new TextFieldSettingsListener());
		quoteTextField.addFocusListener(new TextFieldSettingsListener());
		escapeTextField.addFocusListener(new TextFieldSettingsListener());
		headerTextField.addFocusListener(new TextFieldSettingsListener());

		charsetComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				fileSettingsChanged();
			}
		});

		btnOpen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.addChoosableFileFilter(new FileFilter() {
					@Override
					public String getDescription() {
						return "*.csv (Comma-Separated Values)";
					}

					@Override
					public boolean accept(File file) {
						return file.isDirectory() || file.getName().matches("^.+\\.(CSV|csv)$");
					}
				});

				int rVal = fileChooser.showOpenDialog(AbstractOpenCsvFilePanel.this);

				if (rVal == JFileChooser.APPROVE_OPTION) {
					fileTextField.setText(fileChooser.getSelectedFile().getAbsolutePath());
					fileSettingsChanged();
					separatorPanel.setVisible(true);
					advancedOptionPanel.setVisible(true);
					previewPanel.setVisible(true);
				}
			}
		});
	}

	class TextFieldSettingsListener implements FocusListener {

		private String text = ""; // in case focusLost() is first called

		@Override
		public void focusLost(FocusEvent event) {
			String tmp;
			try {
				tmp = ((JTextField) event.getSource()).getText();
				if (!text.equals(tmp)) {
					text = tmp;
					fileSettingsChanged();
				}
			} catch (NullPointerException e) {
				// event.getSource() == null
			}
		}

		@Override
		public void focusGained(FocusEvent event) {
			text = fileTextField.getText();
		}
	}

	class ToggleButtonSettingsListener implements ChangeListener {

		private boolean state;

		public ToggleButtonSettingsListener(boolean state) {
			this.state = state;
		}

		@Override
		public void stateChanged(ChangeEvent event) {
			try {
				if (state != ((JToggleButton) event.getSource()).isSelected()) {
					state = !state;
					fileSettingsChanged();
				}
			} catch (NullPointerException e) {
				// event.getSource() == null
			}
		}
	}

	protected class IntegerFieldDocument extends PlainDocument {

		private static final long serialVersionUID = 7383435613928675877L;

		@Override
		public void insertString(int offs, String str, AttributeSet a)
				throws BadLocationException {
			try {
				Integer.parseInt(str);
				super.insertString(offs, str, a);
			} catch (NumberFormatException e) {
			}
		}
	}

	protected class CharFieldDocument extends PlainDocument {

		private static final long serialVersionUID = -368932084636808572L;

		@Override
		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
			if (str != null && (getLength() + str.length()) <= 1) {
				super.insertString(offs, str, a);
			}
		}
	}

	private void fileSettingsChanged() {
		table = CsvTable.openCsvFile(getFilePath(), getSeparator(), getQuotechar(), getEscape(), getHeaderRowIndex(),
				hasStrictQuotes(), getCharset(), !hasLeadingWhiteSpaces());
		validateTable(table);
		updatePreviewTable(table, previewRows);

		if (listener instanceof TableChangedListener) {
			listener.tableChanged(this);
		}
	}

	private void updatePreviewTable(String[][] table, int nrows) {
		DefaultTableModel tableModel = new DefaultTableModel() {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		if (validateTable(table) && nrows > 1 && table.length > 1) {
			nrows = (nrows > table.length) ? table.length : nrows;
			// create headers
			for (int col = 0; col < table[0].length; col++) {
				tableModel.addColumn(table[0][col]);
			}
			// add rows
			for (int row = 1; row < nrows; row++) {
				tableModel.addRow(table[row]);
			}
		}
		previewTable.setModel(tableModel);
	}

	private String getFilePath() {
		return fileTextField.getText();
	}

	private char getSeparator() {
		switch (separatorButtonGroup.getSelection().getMnemonic()) {
			case SEPARATOR_SEMICOLON:
				return ';';
			case SEPARATOR_COMMA:
				return ',';
			case SEPARATOR_TABULATOR:
				return '\t';
			default:
				return ';';
		}
	}

	private char getQuotechar() {
		try {
			return quoteTextField.getText().toCharArray()[0];
		} catch (Exception e) {
			return DEFAULT_QUOTE;
		}
	}

	private char getEscape() {
		try {
			return escapeTextField.getText().toCharArray()[0];
		} catch (Exception e) {
			return DEFAULT_ESCAPE;
		}
	}

	private String getCharset() {
		return (String) charsetComboBox.getSelectedItem();
	}
	/*
	 * get header where its range is 0 to max(integer)
	 */

	private int getHeaderRowIndex() {
		try {
			int header = Integer.parseInt(headerTextField.getText());
			return (header > 0) ? (header - 1) : 0;
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	private boolean hasLeadingWhiteSpaces() {
		return !chckbxIgnoreLeadingWhite.isSelected();
	}

	private boolean hasStrictQuotes() {
		return chckbxStrictQuotes.isSelected();
	}

	public boolean validateTable(final String[][] table) {
		return table != null;
	}

	public void setTitle(String title) {
		oTitle.setText(title);
	}

	public void setTableChangedListener(TableChangedListener listener) {
		this.listener = listener;
	}

	public String[][] getTable() {
		return table;
	}
}
