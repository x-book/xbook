package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.Wizard;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer.UserMainTable;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer.lookup.LookupTable;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class ImportWizard extends Wizard {

    protected AbstractController controller = null;
    protected UserMainTable userTable = null;
    protected LookupTable<String, String> columnlookupTable = null;
    protected LookupTableWizardPage columnNameWizardPage = null;

    public ImportWizard(AbstractController controller) {
        this.controller = controller;
    }

    @Override
    public abstract void addPages();
//    {
//		addPage2(OpenCsvFileWizardPageContent.class);
//
//		columnNameWizardPage = new LookupTableWizardPage(
//				ColumnNameBuilderWizardPageContent.class);
//
//		addPage(columnNameWizardPage);
//	}

    @Override
    public abstract boolean performFinish();

    @Override
    public boolean canFinish() {
        return getColumnlookupTable() != null && getUserTable() != null;
    }

    public UserMainTable getUserTable() {
        return userTable;
    }

    public void setUserTable(UserMainTable userTable) {
        this.userTable = userTable;
        if (userTable != null) {
            columnNameWizardPage.setColumnValues(userTable.getColumnLabels());
        }
    }

    public LookupTable<String, String> getColumnlookupTable() {
        return columnlookupTable;
    }

    public void setColumnlookupTable(
            LookupTable<String, String> columnlookupTable) {
        this.columnlookupTable = columnlookupTable;
    }
}
