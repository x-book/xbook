package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.FilterManager;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTable;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTable.XTableSorter;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * <br>
 *
 * <br>* @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 * <br>
 */
public class Filter extends AbstractContent {

    private final Listing listingScreen;
    private JPanel filterRowWrapper;
    private ArrayList<AbstractFilterRow> allFilterRows;
    private final ArrayList<FilterManager> allFilterManagerToRemove;
    private JButton addContainsTextLineButton;
    private JButton addIsTextLineButton;
    private JButton addGreaterThanLineButton;
    private JButton addLowerThanLineButton;
    private JButton addEarlierThanDateLineButton;
    private JButton addLaterThanDateLineButton;

    public Filter(Listing listingScreen) {
        super();
        this.listingScreen = listingScreen;
        allFilterRows = new ArrayList<>();
        allFilterManagerToRemove = new ArrayList<>();
        init();
        ComponentHelper.colorAllChildren(this, Colors.CONTENT_BACKGROUND);
    }

    @Override
    public void actionOnDisplay() {
        super.actionOnDisplay();
    }

    @Override
    protected JPanel getContent() {
        JPanel wrapper = new JPanel(new StackLayout());

        JLabel activatedFilters = new JLabel("<html><span style='font-size: 11px'><b><u>" + Loc.get("ACTIVE_FILTERS") + ":</u></b></html>");
        activatedFilters.setPreferredSize(new Dimension(300, 30));
        wrapper.add(ComponentHelper.wrapComponent(activatedFilters, 0, 4, 0, 2));

        filterRowWrapper = new JPanel(new StackLayout());
        wrapper.add(filterRowWrapper);

        JLabel labelNewFilter = new JLabel("<html><span style='font-size: 11px'><b><u>" + Loc.get("ADD_NEW_FILTER") + ":</u></b></html>");
        labelNewFilter.setPreferredSize(new Dimension(300, 30));
        wrapper.add(ComponentHelper.wrapComponent(labelNewFilter, 0, 4, 0, 2));

        JPanel addLineWrapper = new JPanel(new BorderLayout());
        JPanel addLineWrapperInner = new JPanel(new FlowLayout());

        addContainsTextLineButton = new JButton("<html><center><b>" + Loc.get("CONTAINS") + " *</b></center></html>");
        addContainsTextLineButton.setEnabled(false);
        addContainsTextLineButton.setPreferredSize(new Dimension(120, 55));
        addContainsTextLineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addFilterRowContainsText();
            }
        });
        addLineWrapperInner.add(addContainsTextLineButton);

        addIsTextLineButton = new JButton("<html><center><b>" + Loc.get("IS") + " *</b></center></html>");
        addIsTextLineButton.setEnabled(false);
        addIsTextLineButton.setPreferredSize(new Dimension(120, 55));
        addIsTextLineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addFilterRowIsText();
            }
        });
        addLineWrapperInner.add(addIsTextLineButton);

        addLowerThanLineButton = new JButton("<html><center><b>" + Loc.get("LOWER_THAN") + "</b><br>(" + Loc.get("NUMERIC") + ")</center></html>");
        addLowerThanLineButton.setEnabled(false);
        addLowerThanLineButton.setPreferredSize(new Dimension(120, 55));
        addLowerThanLineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addFilterRowLowerThan();
            }
        });
        addLineWrapperInner.add(addLowerThanLineButton);

        addGreaterThanLineButton = new JButton("<html><center><b>" + Loc.get("GREATER_THAN") + "</b><br>(" + Loc.get("NUMERIC") + ")</center></html>");
        addGreaterThanLineButton.setEnabled(false);
        addGreaterThanLineButton.setPreferredSize(new Dimension(120, 55));
        addGreaterThanLineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addFilterRowGreaterThan();
            }
        });
        addLineWrapperInner.add(addGreaterThanLineButton);

        addEarlierThanDateLineButton = new JButton("<html><center><b>" + Loc.get("EARLIER_THAN") + " **</b><br>(" + Loc.get("DATE") + ")</center></html>");
        addEarlierThanDateLineButton.setEnabled(false);
        addEarlierThanDateLineButton.setPreferredSize(new Dimension(120, 55));
        addEarlierThanDateLineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addFilterRowEarlierThanDate();
            }
        });
        addLineWrapperInner.add(addEarlierThanDateLineButton);

        addLaterThanDateLineButton = new JButton("<html><center><b>" + Loc.get("LATER_THAN") + " **</b><br>(" + Loc.get("DATE") + ")</center></html>");
        addLaterThanDateLineButton.setEnabled(false);
        addLaterThanDateLineButton.setPreferredSize(new Dimension(120, 55));
        addLaterThanDateLineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addFilterRowLaterThanDate();
            }
        });
        addLineWrapperInner.add(addLaterThanDateLineButton);

        addLineWrapper.add(BorderLayout.WEST, addLineWrapperInner);

        wrapper.add(ComponentHelper.wrapComponent(addLineWrapper, 4, 4, 4, 1));

        wrapper.add(ComponentHelper.wrapComponent(new JLabel("* " + Loc.get("FILTER_MULTIPLE_VALUES_DESCRIPTION", AbstractFilterRow.SEPARATOR)), 4, 4, 4, 1));
        wrapper.add(ComponentHelper.wrapComponent(new JLabel("** " + Loc.get("FILTER_DATE_DESCRIPTION", AbstractFilterRow.SEPARATOR)), 4, 4, 4, 1));

//        addRow(new FilterRowContainsText(this, listingScreen));
        checkVisibilityAddButton();

        return wrapper;
    }

    public void addFilterRowContainsText() {
        addFilterRowContainsText(null, "");
    }

    public void addFilterRowContainsText(ColumnType columnType, String text) {
        FilterRowContainsText row = new FilterRowContainsText(Filter.this, listingScreen);
        row.setSelectedColumnType(columnType);
        row.setText(text);
        addRow(row);
    }

    public void addFilterRowIsText() {
        addFilterRowIsText(null, "");
    }

    public void addFilterRowIsText(ColumnType columnType, String text) {
        FilterRowIsText row = new FilterRowIsText(Filter.this, listingScreen);
        row.setSelectedColumnType(columnType);
        row.setText(text);
        addRow(row);
    }

    public void addFilterRowLowerThan() {
        addFilterRowLowerThan(null, "");
    }

    public void addFilterRowLowerThan(ColumnType columnType, String text) {
        FilterRowLowerThan row = new FilterRowLowerThan(Filter.this, listingScreen);
        row.setSelectedColumnType(columnType);
        row.setText(text);
        addRow(row);
    }

    public void addFilterRowGreaterThan() {
        addFilterRowGreaterThan(null, "");
    }

    public void addFilterRowGreaterThan(ColumnType columnType, String text) {
        FilterRowGreaterThan row = new FilterRowGreaterThan(Filter.this, listingScreen);
        row.setSelectedColumnType(columnType);
        row.setText(text);
        addRow(row);
    }

    public void addFilterRowLaterThanDate() {
        addFilterRowLaterThanDate(null, "");
    }

    public void addFilterRowLaterThanDate(ColumnType columnType, String text) {
        FilterRowLaterThanDate row = new FilterRowLaterThanDate(Filter.this, listingScreen);
        row.setSelectedColumnType(columnType);
        row.setText(text);
        addRow(row);
    }

    public void addFilterRowEarlierThanDate() {
        addFilterRowEarlierThanDate(null, "");
    }

    public void addFilterRowEarlierThanDate(ColumnType columnType, String text) {
        FilterRowEarlierThanDate row = new FilterRowEarlierThanDate(Filter.this, listingScreen);
        row.setSelectedColumnType(columnType);
        row.setText(text);
        addRow(row);
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();
        panel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_BACK, Loc.get("BACK"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Content.setContent(listingScreen);
                listingScreen.loadFilterBackup();
                listingScreen.applyFilter(false);
            }
        });
        panel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_CONFIRM, Loc.get("CONFIRM"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((XTableSorter) listingScreen.table.getRowSorter()).sort();
                Content.setContent(listingScreen);
                listingScreen.applyFilter(true);
            }
        });
        return panel;
    }

    @Override
    public SidebarPanel getSideBar() {
        SidebarPanel sidebar = new SidebarPanel();
        sidebar.addTitle(Loc.get("FILTER"));
        sidebar.addHtmlPane(Loc.get("SIDEBAR_LIST_FILTER"));
        return sidebar;
    }

    @Override
    public boolean forceSidebar() {
        return true;
    }

    public void checkVisibilityAddButton() {
        if (addContainsTextLineButton == null || addIsTextLineButton == null || addLowerThanLineButton == null || addGreaterThanLineButton == null) {
            return;
        }
        for (AbstractFilterRow row : allFilterRows) {
            if (row.isEmpty()) {
                addContainsTextLineButton.setEnabled(false);
                addIsTextLineButton.setEnabled(false);
                addLowerThanLineButton.setEnabled(false);
                addGreaterThanLineButton.setEnabled(false);
                addEarlierThanDateLineButton.setEnabled(false);
                addLaterThanDateLineButton.setEnabled(false);
                return;
            }
        }
        addContainsTextLineButton.setEnabled(true);
        addIsTextLineButton.setEnabled(true);
        addLowerThanLineButton.setEnabled(true);
        addGreaterThanLineButton.setEnabled(true);
        addEarlierThanDateLineButton.setEnabled(true);
        addLaterThanDateLineButton.setEnabled(true);
    }

    public void removeRow(AbstractFilterRow row) {
        filterRowWrapper.remove(row);
        filterRowWrapper.revalidate();
        filterRowWrapper.repaint();
        checkVisibilityAddButton();

        // filter manager should be removed from the table when applying the filter
        // (means: when returning to the listing with applying the filter)
        // do not remove the filter directly here, otherwise there occurs
        // problems with the return button.
        allFilterManagerToRemove.add(row.getFilterManager());
        updateAllRows();
    }

    private void addRow(AbstractFilterRow row) {
        allFilterRows.add(row);
        filterRowWrapper.add(row);
        filterRowWrapper.revalidate();
        filterRowWrapper.repaint();
        checkVisibilityAddButton();
        updateAllRows();
    }

    public ArrayList<AbstractFilterRow> getAllFilterRows() {
        return allFilterRows;
    }

    public ArrayList<FilterManager> getAllFilterManagerToRemove() {
        return allFilterManagerToRemove;
    }

    /**
     * Resets the arraylist that contains all filter managers that should be removed.
     * Also removes the filter rows of the corresponding filter managers.
     */
    public void resetAllFilterManagerToRemove() {
        for (AbstractFilterRow row : allFilterRows) {
            if (allFilterManagerToRemove.contains(row.getFilterManager())) {
                allFilterRows.remove(row);
            }
        }
        allFilterManagerToRemove.clear();
    }

    public void removeAllRows() {
        for (AbstractFilterRow row : allFilterRows) {
            ((XTable.XRowFilter) (listingScreen.table.getRowFilter())).removeFilterManager(row.getFilterManager());
        }
    }

    public Filter makeACopy() {
        Filter copy = new Filter(listingScreen);
        copy.allFilterRows = new ArrayList<>();
        for (AbstractFilterRow row : allFilterRows) {
            AbstractFilterRow clone = row.cloneIt(copy);
            if (clone != null) {
                copy.allFilterRows.add(clone);
            }
        }
        return copy;
    }

    @Override
    public void updateContent() {
        super.updateContent();
        filterRowWrapper.removeAll();
        for (AbstractFilterRow row : allFilterRows) {
            filterRowWrapper.add(row);
        }
        updateAllRows();
    }

    protected void updateAllRows() {
        for (int index = 0; index < allFilterRows.size(); index++) {
            allFilterRows.get(index).setAddLabelVisibile(index != 0);
        }
    }

    public void updateRowCombos() {
        for (AbstractFilterRow row : allFilterRows) {
            row.updateCombo();
        }
    }

}
