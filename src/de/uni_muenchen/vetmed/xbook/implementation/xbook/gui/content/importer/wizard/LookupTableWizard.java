package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.helper.LookupTableManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer.UserMainTable;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer.lookup.LookupTable;
import java.util.ArrayList;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class LookupTableWizard extends Wizard {

    protected UserMainTable table;
    protected LookupTable<String, String> columnLabelsLookupTable;
    protected AbstractController controller = null;
    protected ArrayList<ColumnType> columns;
    public LookupTableWizard(AbstractController controller, UserMainTable table,
            LookupTable<String, String> columnLabelsLookupTable) {
        
        if (table != null && columnLabelsLookupTable != null
                && controller != null) {
            this.controller = controller;
            this.table = table;
            this.columnLabelsLookupTable = columnLabelsLookupTable;
        } else {
            throw new NullPointerException();
        }
        try {
            columns = controller.getImportColumnLabels();
        } catch (StatementNotExecutedException | NotLoggedInException ex) {
            columns = new ArrayList<>();
        }
    }
    protected LookupTableManager manager = new LookupTableManager();

   

    public LookupTableManager getLookupTableManager() {
        return manager;
    }
    
    public ColumnType convertToType(String columnName){
         for (ColumnType name : columns) {
            if (name.getColumnName().equals(columnName)) {
                return name;
            }
        }
        throw new IllegalArgumentException();
    }
}
