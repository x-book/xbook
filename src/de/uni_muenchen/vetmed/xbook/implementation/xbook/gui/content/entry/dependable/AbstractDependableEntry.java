package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.dependable;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.navigation.AbstractSubNavigation;
import de.uni_muenchen.vetmed.xbook.api.gui.navigation.SubNavigationElement;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 23.06.2017.
 */
public abstract class AbstractDependableEntry extends AbstractEntry {

    protected final ArrayList<AbstractBaseEntryManager> allEntries;
    protected final AbstractBaseEntryManager thisEntry;
    protected DependableEntrySubNavigation ahSubNavigation;

    protected EntryDataSet entryDataSet;

    public AbstractDependableEntry(GeneralInputMaskMode mode, AbstractBaseEntryManager thisEntry, ArrayList<AbstractBaseEntryManager> allEntries) {
        super(mode);
        this.thisEntry = thisEntry;
        this.allEntries = allEntries;
        if (generalMode == GeneralInputMaskMode.SINGLE) {
            ahSubNavigation.setAllEntries(thisEntry, allEntries);
        }
        updateButtons();

    }

    public AbstractDependableEntry(GeneralInputMaskMode mode, boolean doInit, AbstractBaseEntryManager thisEntry, ArrayList<AbstractBaseEntryManager> allEntries) {
        super(mode, doInit);
        this.thisEntry = thisEntry;
        this.allEntries = allEntries;
        if (generalMode == GeneralInputMaskMode.SINGLE) {
            ahSubNavigation.setAllEntries(thisEntry, allEntries);
        }
        updateButtons();

    }

    public AbstractDependableEntry(GeneralInputMaskMode mode, EntryDataSet data, AbstractBaseEntryManager thisEntry, ArrayList<AbstractBaseEntryManager> allEntries) {
        super(mode, data);
        this.thisEntry = thisEntry;
        this.allEntries = allEntries;
        if (generalMode == GeneralInputMaskMode.SINGLE) {
            ahSubNavigation.setAllEntries(thisEntry, allEntries);
        }
        updateButtons();
    }

    @Override
    public AbstractSubNavigation getSubNavigation() {
        if (generalMode == GeneralInputMaskMode.SINGLE) {
            ahSubNavigation = new DependableEntrySubNavigation(mainFrame);
        }
        return ahSubNavigation;
    }


    @Override
    public void setEntryData(EntryDataSet data, boolean isEditMode) {
        super.setEntryData(data, isEditMode);
        this.entryDataSet = data;
        ahSubNavigation.setActivateOther(data != null, entryDataSet.getEntryKey(), entryDataSet.getProjectKey());

    }

    @Override
    public void unsetEntryData() {
        super.unsetEntryData();
        this.entryDataSet = null;
        ahSubNavigation.setActivateOther(false, null, null);
    }

    @Override
    protected AbstractBaseEntryManager getManager() {
        return thisEntry;
    }

    public static class DependableEntrySubNavigation extends AbstractSubNavigation {
        private boolean activateOther;
        private Key entryKey;
        private Key projectKey;
        private AbstractBaseEntryManager thisEntry;
        private ArrayList<AbstractBaseEntryManager> allEntries;

        public DependableEntrySubNavigation(IMainFrame mainFrame) {
            super(mainFrame);
        }

        /**
         * Creates the Navigation with the current entryManager and all other Entry managers that are displayed in the Navigation
         *
         * @param thisEntry
         * @param allEntries
         */
        public void setAllEntries(AbstractBaseEntryManager thisEntry, ArrayList<AbstractBaseEntryManager> allEntries) {
            this.thisEntry = thisEntry;
            this.allEntries = allEntries;
            updateElements();
        }

        /**
         * Defines if the other Elements shall be clickable or not.
         *
         * @param activateOther
         * @param entryKey
         * @param projectKey
         */
        public void setActivateOther(boolean activateOther, Key entryKey, Key projectKey) {
            this.activateOther = activateOther;
            this.entryKey = entryKey;
            this.projectKey = projectKey;
            updateElements();
        }

        @Override
        protected ArrayList<SubNavigationElement> createContent() {
            ArrayList<SubNavigationElement> subNavigationElements = new ArrayList<>();
            if (allEntries != null) {
                for (final AbstractBaseEntryManager manager : allEntries) {

                    boolean sameEntry = manager == thisEntry;
                    SubNavigationElement e = new SubNavigationElement(manager.getLocalisedTableName(), sameEntry) {
                        @Override
                        protected void actionOnClick() {
                            EntryDataSet dataSet = new EntryDataSet(entryKey, projectKey, manager.getDatabaseName(), manager.getTableName());
                            try {
                                manager.loadBase(dataSet);
                                mainFrame.displayEntryForManager(manager, dataSet, false);
                            } catch (StatementNotExecutedException e1) {
                                e1.printStackTrace();
                            }
                        }
                    };
                    e.setEnabled(sameEntry || activateOther);
                    subNavigationElements.add(e);
                }
            }
            return subNavigationElements;
        }


    }


    public void updateButtons() {
        AbstractBaseEntryManager nextTemp = null;
        if (allEntries != null) {
            for (int i = 0; i < allEntries.size(); i++) {
                if (allEntries.get(i).equals(thisEntry) && (i < allEntries.size() - 1)) {
                    nextTemp = allEntries.get(i + 1);
                    break;
                }
            }
        }
        final AbstractBaseEntryManager next = nextTemp;
        if (next != null) {
            buttonBar.removeFrom(ButtonPanel.Position.NORTH_EAST, buttonSaveAndNewDataset);
            buttonBar.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_SAVE_NEXT, "<html><center>" + Loc.get("SAVE_AND_NEXT_MASK") + "</center></html>", Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            saveEntry(false);
                            mainFrame.displayEntryForManager(next, entryDataSet, true);
                        }
                    }).start();
                }
            });
        } else {
            buttonBar.removeFrom(ButtonPanel.Position.NORTH_EAST, buttonSaveAndNewDataset);
            buttonSaveAndNewDataset = buttonBar.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_SAVENEW, "<html><center>" + Loc.get("SAVE_AND_NEW_DATASET") + "</center></html>", Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            saveEntry(true);
                        }
                    }).start();
                }
            });
        }
    }

    protected Component getComponentToRender() {
        return content;
    }

    private static void layoutComponent(Component c) {
        synchronized (c.getTreeLock()) {
            c.doLayout();
            if (c instanceof Container) {
                for (Component child : ((Container) c).getComponents()) {
                    layoutComponent(child);
                }
            }
        }
    }
}
