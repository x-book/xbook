package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.groupManagement;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Group;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.datatype.User;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ColumnUser extends AbstractGroupManagementColumn {

    private ArrayList<UserLine> allLines;
    private JPanel panel;
    protected final GroupManagement groupManagement;
    private XImageButton buttonAddUserToGroup;
    private boolean hasEditGroupRights = false;
    private int currentUserId = -1;

    public ColumnUser(GroupManagement rangScreen, AbstractMainFrame mainFrame) {
        super(mainFrame);
        this.groupManagement = rangScreen;
    }

    @Override
    protected String getTitle() {
        return Loc.get("USER_OVERVIEW");
    }

    @Override
    protected JScrollPane getContent() {
        allLines = new ArrayList<>();

        panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        JScrollPane scroll = new JScrollPane();
        scroll.setViewportView(ComponentHelper.wrapComponent(panel, Colors.CONTENT_BACKGROUND, 0,10,0,10));
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));

        return scroll;
    }

    public void update() {
        panel.removeAll();

        Group selectedGroup = groupManagement.getSelectedGroup();
        if (selectedGroup != null) {
            try {
                ArrayList<User> users = mainFrame.getController().getUsersOfGroup(selectedGroup.getId());
                for (User user : users) {
                    UserLine line = createUserLine(user);
                    if (line != null) {
                        panel.add(line);
                    }
                }
                buttonAddUserToGroup.setVisible(hasUserGroupRightsForSelectedGroup());

            } catch (NotLoadedException | StatementNotExecutedException ex) {
                Logger.getLogger(ColumnUser.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NotLoggedInException ex) {
                Logger.getLogger(ColumnUser.class.getName()).log(Level.SEVERE, null, ex);
                mainFrame.displayLoginScreen();
            }
        }
        panel.revalidate();
        panel.repaint();
    }

    @Override
    protected ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();

        buttonAddUserToGroup = new XImageButton(Loc.get("ADD_USER_TO_GROUP"));
        buttonAddUserToGroup.setWidth(200);
        buttonAddUserToGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddUserToGroupClicked();
            }
        });
        buttonAddUserToGroup.setVisible(false);
        panel.addTo(ButtonPanel.Position.SOUTH_EAST, buttonAddUserToGroup);

        return panel;
    }

    protected void onAddUserToGroupClicked() {
        mainFrame.displayProjectUserRightsAddUserToGroupScreen(groupManagement, groupManagement.getSelectedGroup());


    }
    protected void onEditoUserRankClicked(User user, String currentRank){
        mainFrame.displayProjectUserRightsEditUserRankScreen(groupManagement, groupManagement.getSelectedGroup(), user, currentRank);
    }

    private UserLine createUserLine(User user) {
        try {
            String rank = mainFrame.getController().getRankOfUser(user.getId(), groupManagement.getSelectedGroup().getId()).getName();
            UserLine line = new UserLine(user, rank);
            allLines.add(line);
            return line;
        } catch (NotLoggedInException | StatementNotExecutedException | EntriesException ex) {
            Logger.getLogger(ColumnUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    protected JComponent getDescription() {
        MultiLineTextLabel text = new MultiLineTextLabel(Loc.get("GROUP_MANAGEMENT_USER_DESCRIPTION"));
        text.setBackground(Colors.CONTENT_BACKGROUND);
        return ComponentHelper.wrapComponent(text, Colors.CONTENT_BACKGROUND, 0, 0, 0, 0);
    }

    private class UserLine extends JPanel {

        private final JPanel firstLine;
        private final JPanel secondLine;
        private final JPanel left;
        private final JPanel left2;
        private final JLabel labelListing;
        private final JLabel labelName;
        private final JPanel iconPanel;
        private JLabel deleteLabel;
        private JLabel editLabel;

        public UserLine(final User user, final String rank) {
            super(new BorderLayout());
            setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

            try {
                hasEditGroupRights = hasUserGroupRightsForSelectedGroup();
                currentUserId = mainFrame.getController().getUserID();
            } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException ex) {
                Logger.getLogger(ColumnUser.class.getName()).log(Level.SEVERE, null, ex);
            }

            firstLine = new JPanel(new BorderLayout());
            firstLine.setBackground(Colors.CONTENT_BACKGROUND);

            left = new JPanel(new FlowLayout(FlowLayout.LEADING));
            left.setBackground(Colors.CONTENT_BACKGROUND);

            labelListing = new JLabel("- ");
            labelListing.setBackground(Colors.CONTENT_BACKGROUND);
            left.add(labelListing);
            left.add(labelName = new JLabel(user.getDisplayName()));
            labelName.setFont(Fonts.FONT_STANDARD_BOLD);
            left.add(new JLabel("(" + user.getUserName() + ")"));
            firstLine.add(BorderLayout.WEST, left);

            iconPanel = new JPanel();
            iconPanel.setBackground(Colors.CONTENT_BACKGROUND);

            editLabel = new JLabel(Images.EDIT_SQUARE);
            editLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    editLabel.setIcon(Images.EDIT_SQUARE_HOVERED);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    editLabel.setIcon(Images.EDIT_SQUARE);
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    onEditoUserRankClicked(user,rank);
                }
            });
            if (hasEditGroupRights) {
                iconPanel.add(editLabel);
            }

            deleteLabel = new JLabel(Images.DELETE_SQUARE);
            deleteLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    deleteLabel.setIcon(Images.DELETE_SQUARE_HOVERED);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    deleteLabel.setIcon(Images.DELETE_SQUARE);
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    try {
                        Object[] options = {"<html><b>" + Loc.get("DELETE") + "</b></html>", Loc.get("CANCEL")};
                        int n = JOptionPane.showOptionDialog(mainFrame,
                                "<html>" + Loc.get("DO_YOU_REALLY_WANT_TO_REMOVE_THE_USER_FROM_THE_GROUP", user.getDisplayName(), groupManagement.getSelectedGroup().getName()) + "</html>",
                                "", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
                                null, options, options[0]);
                        if (n == JOptionPane.YES_OPTION) {
                            Message ms = ((AbstractSynchronisationController) mainFrame.getController()).removeUserFromGroup(user.getId(), groupManagement.getSelectedGroup().getId());
                            if (ms.getResult().wasSuccessful()) {
                                Footer.displayConfirmation(Loc.get("USER_SUCCESSFULLY_REMOVED_FROM_GROUP", user.getDisplayName(), groupManagement.getSelectedGroup().getName()));
                            } else {
                                Footer.displayError(Loc.get("ERROR_WHILE_REMOVING_THE_USER", user.getDisplayName() + ": " + ms.getResult().getErrorMessage()));
                            }
                            actionToReloadPanel();
                        }
                    } catch (IOException | NotConnectedException | NotLoggedInException ex) {
                        Footer.displayError(Loc.get("ERROR_WHILE_REMOVING_THE_USER", user.getDisplayName()));
                    }
                }
            });
            if (hasEditGroupRights || user.getId() == currentUserId) {
                iconPanel.add(deleteLabel);
            }

            firstLine.add(BorderLayout.EAST, iconPanel);

            add(BorderLayout.NORTH, firstLine);
            secondLine = new JPanel(new BorderLayout());

            secondLine.setBackground(Colors.CONTENT_BACKGROUND);
            left2 = new JPanel(new FlowLayout(FlowLayout.LEADING));

            left2.setBackground(Colors.CONTENT_BACKGROUND);
            JLabel labelRank = new JLabel(Loc.get("RANK") + ": " + rank);

            labelRank.setBorder(BorderFactory.createEmptyBorder(0, 27, 0, 0));
            left2.add(labelRank);

            secondLine.add(BorderLayout.WEST, left2);

            add(BorderLayout.SOUTH, secondLine);

            setCustomListeners();
        }

        private void setCustomListeners() {
            addMouseListener(getCustomMouseListener());
            labelName.addMouseListener(getCustomMouseListener());
            firstLine.addMouseListener(getCustomMouseListener());
            secondLine.addMouseListener(getCustomMouseListener());
            left.addMouseListener(getCustomMouseListener());
            labelListing.addMouseListener(getCustomMouseListener());
            iconPanel.addMouseListener(getCustomMouseListener());
            deleteLabel.addMouseListener(getCustomMouseListener());
            editLabel.addMouseListener(getCustomMouseListener());
        }

        private MouseListener getCustomMouseListener() {
            return new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                    labelName.setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                    firstLine.setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                    secondLine.setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                    left.setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                    left2.setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                    labelListing.setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                    iconPanel.setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    setBackground(Colors.CONTENT_BACKGROUND);
                    labelName.setBackground(Colors.CONTENT_BACKGROUND);
                    firstLine.setBackground(Colors.CONTENT_BACKGROUND);
                    secondLine.setBackground(Colors.CONTENT_BACKGROUND);
                    left.setBackground(Colors.CONTENT_BACKGROUND);
                    left2.setBackground(Colors.CONTENT_BACKGROUND);
                    labelListing.setBackground(Colors.CONTENT_BACKGROUND);
                    iconPanel.setBackground(Colors.CONTENT_BACKGROUND);
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                }
            };
        }
    }

    protected void actionToReloadPanel() {
        mainFrame.displayGroupManagementScreen(groupManagement.getSelectedGroup());
    }

    private boolean hasUserGroupRightsForSelectedGroup() throws NotLoggedInException, NotLoadedException, StatementNotExecutedException {
        return mainFrame.getController().hasEditGroupRights(groupManagement.getSelectedGroup().getId());
    }
}
