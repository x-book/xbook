package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.InterfaceUpdateableInputField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.extendedPanels.ExtendedMultiFileUploadField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputIconButton;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectEdit;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputMultiFileUploadField extends InputIconButton implements InterfaceUpdateableInputField {

    /**
     * The object for the extended content with the real input fields.
     */
    private ExtendedMultiFileUploadField extendedContent;
    private final String tableFile;
    private ArrayList<FileNameExtensionFilter> fileFilter;
    private ColumnType columnTypeTypeExtension;
    private ColumnType columnTypeFileMustBeSynced;
    private ColumnType columnTypeUUID;

    /**
     * Constructor.
     *
     * @param entry The entry screen object.
     */
    public InputMultiFileUploadField(ColumnType columnTypeFileName, ColumnType columnTypeTypeExtension,
                                     ColumnType columnTypeFileMustBeSynced, ColumnType columnTypeUUID,
                                     String tableFile, AbstractEntry entry,
                                     ArrayList<FileNameExtensionFilter> fileFilter) {
        super(columnTypeFileName, entry);
        this.columnTypeTypeExtension = columnTypeTypeExtension;
        this.columnTypeFileMustBeSynced = columnTypeFileMustBeSynced;
        this.columnTypeUUID = columnTypeUUID;
        this.tableFile = tableFile;
        this.fileFilter = fileFilter;
    }

    /**
     * Constructor.
     *
     * @param projectEdit The entry screen object.
     */
    public InputMultiFileUploadField(ColumnType columnTypeFileName, ColumnType columnTypeTypeExtension,
                                     ColumnType columnTypeFileMustBeSynced, ColumnType columnTypeUUID,
                                     String tableFile, AbstractProjectEdit projectEdit,
                                     ArrayList<FileNameExtensionFilter> fileFilter) {
        super(columnTypeFileName, projectEdit);
        this.columnTypeTypeExtension = columnTypeTypeExtension;
        this.columnTypeFileMustBeSynced = columnTypeFileMustBeSynced;
        this.columnTypeUUID = columnTypeUUID;
        this.tableFile = tableFile;
        this.fileFilter = fileFilter;
    }

    @Override
    public void save(DataSetOld data) {
        extendedContent.save(data);
    }

    @Override
    public void clear() {
        extendedContent.clear();
    }

    @Override
    public void load(DataSetOld data) {
        extendedContent.load(data);
    }

    @Override
    public void updateInputFieldSingle() {
        extendedContent.updateContent();
        if (extendedContent.getCurrentData().isEmpty()) {
            removeIcon();
        } else {
            setDefaultIcon();
        }
    }

    @Override
    public void updateInputFieldMultiEdit() {
        updateInputFieldSingle();
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();

        if (entry != null) {
            setTargetContent(extendedContent = new ExtendedMultiFileUploadField(apiControllerAccess, columnType,
                    columnTypeTypeExtension, columnTypeFileMustBeSynced, columnTypeUUID, tableFile, entry, this,
                    fileFilter));
        } else if (projectEdit != null) {
            setTargetContent(extendedContent = new ExtendedMultiFileUploadField(apiControllerAccess, columnType,
                    columnTypeTypeExtension, columnTypeFileMustBeSynced, columnTypeUUID, tableFile, projectEdit, this,
                    fileFilter));
        }
    }

    @Override
    public String getStringRepresentation() {
        return extendedContent.getStringRepresentation();
    }

    @Override
    public Collection<AbstractInputElement> getDependentOnFields() {
        return new ArrayList<>();
    }
}
