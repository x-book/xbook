package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.rows;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;

import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 * @author Daniel Kaltenthaler
 */
public class ProjectSearchTextRow extends AbstractProjectSearchRow {

    protected JTextField input;

    public ProjectSearchTextRow(ColumnType columnType) {
        super(columnType);
    }

    public ProjectSearchTextRow(ColumnType columnType, DataType dataType) {
        super(columnType, dataType);
    }

    @Override
    protected JComponent getInputElements() {
        return input = new JTextField();
    }

    @Override
    protected ArrayList<JComponent> getAllInputElements() {
        ArrayList<JComponent> elements = super.getAllInputElements();
        elements.add(input);
        return elements;
    }

    @Override
    public void clear() {
        input.setText("");
    }

    @Override
    public String getRawInput() {
        return input.getText();
    }

    @Override
    public ArrayList<String> getInput() {
        ArrayList<String> list = new ArrayList<>();
        //no upper case since ß gets translated to SS => search doesnt work
//        String rawInput = input.getText().toUpperCase();
        String rawInput = input.getText();
        if (!rawInput.isEmpty()) {
            for (String s : rawInput.split(SEP)) {
                String trimmed = s.trim();

                // check for numeric values
                if ((columnType.getType() == ColumnType.Type.VALUE || columnType.getType() == ColumnType.Type.VALUE_THESAURUS) && (dataType == DataType.BIGGER || dataType == DataType.SMALLER)) {
                    try {
                        Integer.parseInt(trimmed);
                    } catch (NumberFormatException nfe) {
                        Footer.displayWarning(Loc.get("EXPECTED_NUMERIC_VALUES_IN_FIELD_X_VALUES_IGNORED", columnType.getDisplayName()));
                        continue;
                    }
                }

                // check for date values
                if ((columnType.getType() == ColumnType.Type.DATE ||columnType.getType() == ColumnType.Type.TIME ) && (dataType == DataType.BIGGER || dataType == DataType.SMALLER)) {
                    boolean correctFormat = true;
                    String[] date = trimmed.split("-");
                    if (date.length != 3) {
                        correctFormat = false;
                    } else {
                        try {
                            Integer.parseInt(date[0]);
                            Integer.parseInt(date[1]);
                            Integer.parseInt(date[2]);
                        } catch (NumberFormatException nfe) {
                            correctFormat = false;
                        }
                    }
                    if (!correctFormat) {
                        Footer.displayWarning(Loc.get("EXPECTED_DATE_VALUES_IN_FIELD_X_VALUES_IGNORED", columnType.getDisplayName()));
                        continue;
                    }
                }

                // if everything is ok: Add the value to the list
                list.add(trimmed);
            }
        }
        return list;
    }

}
