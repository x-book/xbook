package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.InterfaceAddableData;
import java.util.ArrayList;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportResult;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;

public abstract class AbstractInputCrossedLinkedEntries extends AbstractInputElement implements InterfaceAddableData {

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     */
    public AbstractInputCrossedLinkedEntries(ColumnType columnType) {
        super(columnType);
        columnType.setMultiEditAllowed(false);
    }

    @Override
    public abstract void load(DataSetOld data);

    @Override
    public abstract void save(DataSetOld data);

    @Override
    public boolean isValidInput() {
        return true;
    }

    @Override
    public void addData(ArrayList<Key> keys) {
        for (Key k : keys) {
            addSingleRow(k);
        }
    }

    @Override
    public void addData(Key key) {
        ArrayList<Key> keys = new ArrayList<>();
        keys.add(key);
        addData(keys);
    }

    protected abstract void addSingleRow(Key key);

    protected Key getValues(ExportResult exportResult) {
        if (exportResult == null) {
            return null;
        }
        return exportResult.getKeyAt(0);
    }

}
