package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.IButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTextBlock;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarUpdate;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.SynchronisationProgress;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A panel where the user can check if new updates are available and (if yes)
 * can start the updates.
 * <p/>
 * In general there are three areas of the application that can be updates: The
 * program version, the database version and the code tables. Each of these
 * areas can be updates, but in general they have to be updates step-by-step in
 * the correct order. Depending on the status of these three areas, the status
 * if the database is initialised, if it is a mandatory updateExtention (e.g.
 * for synchronisation) or not (e.g. for entering data in offline MODE) there
 * are several possibilities the user can/must do.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class UpdatePanel extends AbstractContent implements PropertyChangeListener {

    private enum UpdateType {

        PROGRAM, DATABASE, CODETABLES;
    }

    public enum SourceType {

        LOGIN, MANUAL_CONNECTION
    }

    /**
     * The header bar for the programm version.
     */
    private UpdateHeaderBar headerBarProgrammVersion;
    /**
     * The header bar for the database version.
     */
    private UpdateHeaderBar headerBarDatabaseVersion;
    /**
     * The header bar for the code tables.
     */
    private UpdateHeaderBar headerBarCodeTables;
    /**
     * The spacer below the header bar of the programm version.
     */
    private JLabel spacerProgrammVersion;
    /**
     * The spacer below the header bar of the database version.
     */
    private JLabel spacerDatabaseVersion;
    /**
     * The spacer below the header bar of the code tables.
     */
    private JLabel spacerCodeTables;
    /**
     * The source from where the updateExtention panel was called.
     */
    private SourceType sourceType;
    /**
     * The status if the program version is up to date or not.
     */
    private boolean isProgramUpToDate;
    /**
     * The status if the database version is up to date or not.
     */
    private boolean isDatabaseUpToDate;
    /**
     * The status if the code tables are up to date or not.
     */
    private boolean areCodeTablesUpToDate;
    /**
     * The status if the client is connected to the server or not.
     */
    private boolean isConnectedToServer;
    /**
     * The content panel.
     */
    private JPanel contentPanel;

    private boolean autoContinue;
    /**
     * The content that was displayed before loading the updateExtention screen.
     */
    private AbstractContent previousContent;
    private AbstractSynchronisationController controller;
    private UpdateType lastUpdateTry = null;
    private ButtonPanel buttonPanel;
    private ArrayList<IButton> allButtons = new ArrayList();
    /**
     * Status if the database was updated right before calling the update
     * screen.
     */
    private boolean dbWasUpdatedBefore;

    /**
     * Constructor.
     * <p>
     * Initialize the setting panel with the given parameters.
     *
     * @param previousContent       The content that was displayed before loading the
     *                              updateExtention screen.
     * @param sourceType            The status if it is an mandatory updateExtention or an
     *                              optional updateExtention.
     * @param isProgramUpToDate     The status if the program version is up to date
     *                              or not.
     * @param isDatabaseUpToDate    The status if the database version is up to
     *                              date or not.
     * @param areCodeTablesUpToDate The status if the code tables are up to date
     *                              or not.
     * @param isConnectedToServer   The status if the client is connected to the
     *                              server or not.
     * @param controller            The basic abstract controller.
     */
    public UpdatePanel(AbstractContent previousContent, SourceType sourceType, boolean isProgramUpToDate, boolean isDatabaseUpToDate,
                       boolean areCodeTablesUpToDate, boolean isConnectedToServer, AbstractSynchronisationController controller, boolean autoContinue) {
        this(previousContent, sourceType, isProgramUpToDate, isDatabaseUpToDate, areCodeTablesUpToDate, isConnectedToServer, controller, autoContinue, false);
    }

    /**
     * Constructor.
     * <p>
     * Initialize the setting panel with the given parameters.
     *
     * @param previousContent       The content that was displayed before loading the
     *                              updateExtention screen.
     * @param sourceType            The status if it is an mandatory updateExtention or an
     *                              optional updateExtention.
     * @param isProgramUpToDate     The status if the program version is up to date
     *                              or not.
     * @param isDatabaseUpToDate    The status if the database version is up to
     *                              date or not.
     * @param areCodeTablesUpToDate The status if the code tables are up to date
     *                              or not.
     * @param isConnectedToServer   The status if the client is connected to the
     *                              server or not.
     * @param controller            The basic abstract controller.
     * @param dbWasUpdatedBefore    Status if the database was updated right before
     *                              calling the update screen.
     */
    public UpdatePanel(AbstractContent previousContent, SourceType sourceType, boolean isProgramUpToDate, boolean isDatabaseUpToDate,
                       boolean areCodeTablesUpToDate, boolean isConnectedToServer, AbstractSynchronisationController controller, boolean autoContinue, boolean dbWasUpdatedBefore) {
        super();

        this.previousContent = previousContent;
        this.sourceType = sourceType;
        this.isProgramUpToDate = isProgramUpToDate;
        this.isDatabaseUpToDate = isDatabaseUpToDate;
        this.areCodeTablesUpToDate = areCodeTablesUpToDate;
        this.isConnectedToServer = isConnectedToServer;
        this.controller = controller;
        this.dbWasUpdatedBefore = dbWasUpdatedBefore;
        this.autoContinue = autoContinue;
        init();
    }

    @Override
    protected JPanel getContent() {
        final JPanel panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        // the "step" grid
        JPanel grid = new JPanel(new GridLayout(1, 3, 10, 0));
        grid.setBackground(Colors.CONTENT_BACKGROUND);
        headerBarProgrammVersion = new UpdateHeaderBar(Loc.get("STEP_NUMBER", "1"), Loc.get("PROGRAM_VERSION"), Color.WHITE);
        grid.add(headerBarProgrammVersion);
        headerBarDatabaseVersion = new UpdateHeaderBar(Loc.get("STEP_NUMBER", "2"), Loc.get("DATABASE_VERSION"), Color.WHITE);
        grid.add(headerBarDatabaseVersion);
        headerBarCodeTables = new UpdateHeaderBar(Loc.get("STEP_NUMBER", "3"), Loc.get("CODE_TABLES"), Color.WHITE);
        grid.add(headerBarCodeTables);
        panel.add(grid);

        // the spacer grid
        JPanel gridSpacer = new JPanel(new GridLayout(1, 3, 10, 0));
        gridSpacer.setBackground(Colors.CONTENT_BACKGROUND);

        spacerProgrammVersion = new JLabel(" ");
        spacerProgrammVersion.setBackground(Color.WHITE);
        spacerProgrammVersion.setOpaque(true);
        spacerProgrammVersion.setFont(new Font("Sans-Serif", Font.PLAIN, 4));
        gridSpacer.add(spacerProgrammVersion);

        spacerDatabaseVersion = new JLabel(" ");
        spacerDatabaseVersion.setBackground(Color.WHITE);
        spacerDatabaseVersion.setOpaque(true);
        spacerDatabaseVersion.setFont(new Font("Sans-Serif", Font.PLAIN, 4));
        gridSpacer.add(spacerDatabaseVersion);

        spacerCodeTables = new JLabel(" ");
        spacerCodeTables.setBackground(Color.WHITE);
        spacerCodeTables.setOpaque(true);
        spacerCodeTables.setFont(new Font("Sans-Serif", Font.PLAIN, 4));
        gridSpacer.add(spacerCodeTables);

        panel.add(gridSpacer);

        // the content
        contentPanel = new JPanel(new StackLayout());
        contentPanel.setBackground(Color.orange);
        contentPanel.setOpaque(true);
        panel.add(contentPanel);

        generateContent();

        return panel;
    }

    private void addContentLabel(String text, Color background) {
        addContentLabel(text, background, false);
    }

    /**
     * Add a content label with a specific background color.
     *
     * @param text       The text of the text.
     * @param background The background color.
     */
    private void addContentLabel(String text, Color background, boolean bold) {
        XTextBlock contentText = new XTextBlock(text);
        contentText.setBackground(background);
        contentText.setOpaque(true);
        contentText.setBorder(new EmptyBorder(10, 5, 10, 5));
        if (bold) {
            contentText.setFont(contentText.getFont().deriveFont(Font.BOLD));
        }
        contentPanel.add(contentText);
    }

    /**
     * Add a button to updateExtention the programm version.
     *
     * @param background The background color.
     */
    private void addUpdateProgrammButton(Color background) {
        JPanel p = new JPanel();
        p.setBackground(background);
        XButton updateButton = new XButton(Loc.get("UPDATE_PROGRAM_NOW"));
        allButtons.add(updateButton);
        updateButton.setBackground(background);
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setAllButtonsEnabled(false);
                ((AbstractController) mainFrame.getController()).startUpdater();
                lastUpdateTry = UpdateType.PROGRAM;
            }
        });
        p.add(updateButton);
        contentPanel.add(p);
    }

    /**
     * Add a button to updateExtention the database version.
     *
     * @param background The background color.
     */
    private void addUpdateDatabaseButton(Color background) {
        JPanel p = new JPanel();
        p.setBackground(background);
        XButton updateButton = new XButton(Loc.get("UPDATE_DATABASE_NOW"));
        allButtons.add(updateButton);
        updateButton.setBackground(background);
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateDatabase();
            }
        });
        p.add(updateButton);
        contentPanel.add(p);
    }

    /**
     * Add a button to updateExtention the code tables.
     *
     * @param background The background color.
     */
    private void addUpdateCodeTablesButton(Color background) {
        JPanel p = new JPanel();
        p.setBackground(background);
        XButton updateButton = new XButton(Loc.get("UPDATE_CODE_TABLES_NOW"));
        allButtons.add(updateButton);
        updateButton.setBackground(background);
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doCodeTableUpdate();
            }
        });
        p.add(updateButton);
        contentPanel.add(p);
    }

    private void updateDatabase() {
        setAllButtonsEnabled(false);
        lastUpdateTry = UpdateType.DATABASE;
        synchronized (UpdatePanel.this) {
            Footer.startWorking();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        switch (((AbstractSynchronisationController) mainFrame.getController()).updateDatabase(UpdatePanel.this)) {
                            case UPDATE_FAILED:
                                Footer.displayError(Loc.get("UPDATE_FAILED"));
                                setAllButtonsEnabled(true);
                                break;
                            case UPDATE_LATER: // can't tell wait for it
                                // do stuff later when done
                                return;
                        }
                    } catch (NotLoggedInException | StatementNotExecutedException ex) {
                        ex.printStackTrace();
                    }
                }
            }).start();
            Footer.stopWorking();
        }
    }

    /**
     * Execute the code table update.
     */
    private void doCodeTableUpdate() {
        doCodeTableUpdate(false);
    }

    /**
     * Execute the code table update.
     */
    public void doCodeTableUpdate(boolean updateAll) {
        setAllButtonsEnabled(false);
        lastUpdateTry = UpdateType.CODETABLES;
        ((AbstractSynchronisationController) ((AbstractMainFrame) mainFrame).getController()).updateEntries(UpdatePanel.this, updateAll);
    }

    private void doContinue() {
        System.out.println("continue");
        if (sourceType == SourceType.MANUAL_CONNECTION) {
            System.out.println("manual");
            setAllButtonsEnabled(false);
            ((AbstractSynchronisationController) controller).updateExtention(null, AbstractController.ResultType.CONTINUE_TO_APPLICATION, previousContent);
        } else {
            setAllButtonsEnabled(true);
            controller.startSynchronizer();

            if (!mainFrame.getController().isProjectLoaded()) {
                controller.update(null, AbstractController.ResultType.CONTINUE_TO_PROJECT);
            } else {
                Content.setContent(previousContent);
            }
        }
    }

    /**
     * Generate the content of the updateExtention panel.
     */
    private void generateContent() {
        if (sourceType == SourceType.LOGIN) {
            if (isConnectedToServer) {
                if (isProgramUpToDate && isDatabaseUpToDate && areCodeTablesUpToDate) {
                    headerBarProgrammVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                    headerBarDatabaseVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                    headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);

                    spacerProgrammVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerDatabaseVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                    contentPanel.setBackground(Colors.CONTENT_BACKGROUND);
                    addContentLabel(Loc.get("THE_APPLICATION_IS_UP_TO_DATE"), Colors.CONTENT_BACKGROUND);

                    addContinueToProjectOverviewButton();

                    addBackToLoginButton();
                    if (autoContinue) {
                        doContinue();
                    }
                    return;
                } else if (!isProgramUpToDate && isDatabaseUpToDate && areCodeTablesUpToDate) {
                    headerBarProgrammVersion.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_YELLOW);
                    headerBarDatabaseVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                    headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);

                    spacerProgrammVersion.setBackground(Colors.UPDATE_ACTIVE);
                    spacerDatabaseVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                    contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_PROGRAM_UPDATE_REQUIRED"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_RECOMMENDED"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_NOT_SYNC_NOT_POSSIBLE_WITHOUT_UPDATE"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_PROGRAM_UPDATE_CLICK_UPDATE_BUTTON"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_NO_DATA_GET_LOST"), Colors.UPDATE_ACTIVE, true);

                    addUpdateProgrammButton(Colors.UPDATE_ACTIVE);
                    addSkipProgrammUpdateButton();
                    addBackToLoginButton();
                    return;
                } else if (!isProgramUpToDate && !isDatabaseUpToDate && areCodeTablesUpToDate) {
                    headerBarProgrammVersion.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_RED);
                    headerBarDatabaseVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_RED);
                    headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);

                    spacerProgrammVersion.setBackground(Colors.UPDATE_ACTIVE);
                    spacerDatabaseVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                    contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_PROGRAM_UPDATE_REQUIRED"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_PROGRAM_UPDATE_CLICK_UPDATE_BUTTON"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_NO_DATA_GET_LOST"), Colors.UPDATE_ACTIVE, true);

                    addUpdateProgrammButton(Colors.UPDATE_ACTIVE);
                    addBackToLoginButton();
                    return;
                } else if (!isProgramUpToDate && !isDatabaseUpToDate && !areCodeTablesUpToDate) {
                    headerBarProgrammVersion.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_RED);
                    headerBarDatabaseVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_RED);
                    headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_RED);

                    spacerProgrammVersion.setBackground(Colors.UPDATE_ACTIVE);
                    spacerDatabaseVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                    contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_PROGRAM_UPDATE_REQUIRED"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_PROGRAM_UPDATE_CLICK_UPDATE_BUTTON"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_NO_DATA_GET_LOST"), Colors.UPDATE_ACTIVE, true);

                    addUpdateProgrammButton(Colors.UPDATE_ACTIVE);
                    addBackToLoginButton();
                    return;
                } else if (isProgramUpToDate && !isDatabaseUpToDate && areCodeTablesUpToDate) {
                    headerBarProgrammVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                    headerBarDatabaseVersion.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_RED);
                    headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);

                    spacerProgrammVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerDatabaseVersion.setBackground(Colors.UPDATE_ACTIVE);
                    spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                    contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_DATABASE_UPDATE_REQUIRED"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_DATABASE_UPDATE_AVAILABLE_HINT"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_NOT_SYNC_NOT_POSSIBLE_WITHOUT_UPDATE"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_DATABASE_UPDATE_CLICK_UPDATE_BUTTON"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_NO_DATA_GET_LOST"), Colors.UPDATE_ACTIVE, true);

                    addUpdateDatabaseButton(Colors.UPDATE_ACTIVE);

                    addBackToLoginButton();
                    if (autoContinue) {
                        updateDatabase();
                    }
                    return;
                } else if (isProgramUpToDate && !isDatabaseUpToDate && !areCodeTablesUpToDate) {
                    headerBarProgrammVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                    headerBarDatabaseVersion.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_RED);
                    headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_RED);

                    spacerProgrammVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerDatabaseVersion.setBackground(Colors.UPDATE_ACTIVE);
                    spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                    contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_DATABASE_UPDATE_REQUIRED"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_DATABASE_UPDATE_AVAILABLE_HINT"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_NOT_SYNC_NOT_POSSIBLE_WITHOUT_UPDATE"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_DATABASE_UPDATE_CLICK_UPDATE_BUTTON"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_NO_DATA_GET_LOST"), Colors.UPDATE_ACTIVE, true);

                    addUpdateDatabaseButton(Colors.UPDATE_ACTIVE);
                    addBackToLoginButton();
                    if (autoContinue) {
                        updateDatabase();
                    }
                    return;
                } else if (isProgramUpToDate && isDatabaseUpToDate && !areCodeTablesUpToDate) {
                    headerBarProgrammVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                    headerBarDatabaseVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                    headerBarCodeTables.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_RED);

                    spacerProgrammVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerDatabaseVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerCodeTables.setBackground(Colors.UPDATE_ACTIVE);

                    contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_CODE_TABLES_REQUIRED"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_CODE_TABLES_UPDATE_CLICK_UPDATE_BUTTON"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_NO_DATA_GET_LOST"), Colors.UPDATE_ACTIVE, true);

                    addUpdateCodeTablesButton(Colors.UPDATE_ACTIVE);
                    addBackToLoginButton();

                    if (!dbWasUpdatedBefore || autoContinue) {
                        doCodeTableUpdate();
                    }
                    return;
                } else if (!isProgramUpToDate && isDatabaseUpToDate && !areCodeTablesUpToDate) {
                    headerBarProgrammVersion.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_RED);
                    headerBarDatabaseVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                    headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_RED);

                    spacerProgrammVersion.setBackground(Colors.UPDATE_ACTIVE);
                    spacerDatabaseVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                    contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_PROGRAM_UPDATE_REQUIRED"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_PROGRAM_UPDATE_CLICK_UPDATE_BUTTON"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_NO_DATA_GET_LOST"), Colors.UPDATE_ACTIVE, true);

                    addUpdateProgrammButton(Colors.UPDATE_ACTIVE);
                    addBackToLoginButton();
                    return;
                }
            } else { // if (!isConnectedToServer)
                if (isDatabaseUpToDate && areCodeTablesUpToDate) {
                    headerBarProgrammVersion.set(Colors.UPDATE_UNKNOWN, Images.TEMP_STATUS_NONE);
                    headerBarDatabaseVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                    headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);

                    spacerProgrammVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerDatabaseVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                    contentPanel.setBackground(Colors.CONTENT_BACKGROUND);
                    addContentLabel(Loc.get("NO_CONNECTION_TO_THE_SERVER"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_CANNOT_CHECK_FOR_PROGRAM_UPDATES"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_WORKING_WITH_APPLICATION_IS_POSSIBLE"), Colors.UPDATE_ACTIVE);

                    addContinueToProjectOverviewButton();
                    addBackToLoginButton();
                    if (autoContinue) {
                        doContinue();
                    }
                    return;
                } else if (!isDatabaseUpToDate && areCodeTablesUpToDate) {
                    headerBarProgrammVersion.set(Colors.UPDATE_UNKNOWN, Images.TEMP_STATUS_NONE);
                    headerBarDatabaseVersion.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_RED);
                    headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);

                    spacerProgrammVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerDatabaseVersion.setBackground(Colors.UPDATE_ACTIVE);
                    spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                    contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("NO_CONNECTION_TO_THE_SERVER"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_DATABASE_UPDATE_AVAILABLE_HINT"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_REQUIRED_BUT_NO_CONNECTION_TO_THE_SERVER"), Colors.UPDATE_ACTIVE);

                    addBackToLoginButton();
                    return;
                } else if (!isDatabaseUpToDate && !areCodeTablesUpToDate) {
                    headerBarProgrammVersion.set(Colors.UPDATE_UNKNOWN, Images.TEMP_STATUS_NONE);
                    headerBarDatabaseVersion.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_RED);
                    headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_RED);

                    spacerProgrammVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerDatabaseVersion.setBackground(Colors.UPDATE_ACTIVE);
                    spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                    contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("NO_CONNECTION_TO_THE_SERVER"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_DATABASE_UPDATE_AVAILABLE_HINT"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_REQUIRED_BUT_NO_CONNECTION_TO_THE_SERVER"), Colors.UPDATE_ACTIVE);

                    addBackToLoginButton();
                    return;
                } else if (isDatabaseUpToDate && !areCodeTablesUpToDate) {
                    headerBarProgrammVersion.set(Colors.UPDATE_UNKNOWN, Images.TEMP_STATUS_NONE);
                    headerBarDatabaseVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                    headerBarCodeTables.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_RED);

                    spacerProgrammVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerDatabaseVersion.setBackground(Colors.CONTENT_BACKGROUND);
                    spacerCodeTables.setBackground(Colors.UPDATE_ACTIVE);

                    contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("NO_CONNECTION_TO_THE_SERVER"), Colors.UPDATE_ACTIVE);
                    addContentLabel(Loc.get("UPDATE_CODE_TABLES_REQUIRED"), Colors.UPDATE_ACTIVE, true);
                    addContentLabel(Loc.get("UPDATE_REQUIRED_BUT_NO_CONNECTION_TO_THE_SERVER"), Colors.UPDATE_ACTIVE);

                    addBackToLoginButton();
                    return;
                }
            }
        } else if (sourceType == SourceType.MANUAL_CONNECTION) {
            if (isProgramUpToDate && isDatabaseUpToDate && areCodeTablesUpToDate) {
                headerBarProgrammVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                headerBarDatabaseVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);

                spacerProgrammVersion.setBackground(Colors.CONTENT_BACKGROUND);
                spacerDatabaseVersion.setBackground(Colors.CONTENT_BACKGROUND);
                spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                contentPanel.setBackground(Colors.CONTENT_BACKGROUND);
                addContentLabel(Loc.get("THE_APPLICATION_IS_UP_TO_DATE"), Colors.CONTENT_BACKGROUND);

                addContinueToApplicationButton();
                if (autoContinue) {
                    doContinue();
                }
                return;
            } else if (isProgramUpToDate && !isDatabaseUpToDate && areCodeTablesUpToDate) {
                headerBarProgrammVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                headerBarDatabaseVersion.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_RED);
                headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);

                spacerProgrammVersion.setBackground(Colors.CONTENT_BACKGROUND);
                spacerDatabaseVersion.setBackground(Colors.UPDATE_ACTIVE);
                spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                addContentLabel(Loc.get("UPDATE_DATABASE_UPDATE_REQUIRED"), Colors.UPDATE_ACTIVE, true);
                addContentLabel(Loc.get("UPDATE_DATABASE_UPDATE_AVAILABLE_HINT"), Colors.UPDATE_ACTIVE, true);
                addContentLabel(Loc.get("UPDATE_NOT_SYNC_NOT_POSSIBLE_WITHOUT_UPDATE"), Colors.UPDATE_ACTIVE);
                addContentLabel(Loc.get("UPDATE_DATABASE_UPDATE_CLICK_UPDATE_BUTTON"), Colors.UPDATE_ACTIVE);
                addContentLabel(Loc.get("UPDATE_NO_DATA_GET_LOST"), Colors.UPDATE_ACTIVE, true);

                addUpdateDatabaseButton(Colors.UPDATE_ACTIVE);

                addBackToLoginButton();
                if (autoContinue) {
                    updateDatabase();
                }
                return;
            } else if (!isProgramUpToDate && isDatabaseUpToDate && areCodeTablesUpToDate) {
                headerBarProgrammVersion.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_RED);
                headerBarDatabaseVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);

                spacerProgrammVersion.setBackground(Colors.UPDATE_ACTIVE);
                spacerDatabaseVersion.setBackground(Colors.CONTENT_BACKGROUND);
                spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                addContentLabel(Loc.get("UPDATE_PROGRAM_UPDATE_AVAILABLE"), Colors.UPDATE_ACTIVE, true);
                addContentLabel(Loc.get("UPDATE_RECOMMENDED"), Colors.UPDATE_ACTIVE, true);
                addContentLabel(Loc.get("UPDATE_NOT_SYNC_NOT_POSSIBLE_WITHOUT_UPDATE"), Colors.UPDATE_ACTIVE);
                addContentLabel(Loc.get("UPDATE_PROGRAM_UPDATE_CLICK_UPDATE_BUTTON"), Colors.UPDATE_ACTIVE);
                addContentLabel(Loc.get("UPDATE_NO_DATA_GET_LOST"), Colors.UPDATE_ACTIVE, true);

                addUpdateProgrammButton(Colors.UPDATE_ACTIVE);
                addSkipToApplicationButton();
                addBackToApplicationButton();
                return;
            } else if (isProgramUpToDate && isDatabaseUpToDate && !areCodeTablesUpToDate) {
                headerBarProgrammVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                headerBarDatabaseVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                headerBarCodeTables.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_RED);

                spacerProgrammVersion.setBackground(Colors.CONTENT_BACKGROUND);
                spacerDatabaseVersion.setBackground(Colors.CONTENT_BACKGROUND);
                spacerCodeTables.setBackground(Colors.UPDATE_ACTIVE);

                contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                addContentLabel(Loc.get("UPDATE_CODE_TABLES_REQUIRED"), Colors.UPDATE_ACTIVE, true);
                addContentLabel(Loc.get("UPDATE_CODE_TABLES_UPDATE_CLICK_UPDATE_BUTTON"), Colors.UPDATE_ACTIVE);
                addContentLabel(Loc.get("UPDATE_NO_DATA_GET_LOST"), Colors.UPDATE_ACTIVE, true);

                addUpdateCodeTablesButton(Colors.UPDATE_ACTIVE);
                addCloseApplicationButton();
                if (autoContinue) {
                    doCodeTableUpdate();
                }
                return;
            } else if (!isProgramUpToDate && isDatabaseUpToDate && !areCodeTablesUpToDate) {
                headerBarProgrammVersion.set(Colors.UPDATE_ACTIVE, Images.TEMP_STATUS_YELLOW);
                headerBarDatabaseVersion.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_GREEN);
                headerBarCodeTables.set(Colors.UPDATE_INACTIVE, Images.TEMP_STATUS_RED);

                spacerProgrammVersion.setBackground(Colors.UPDATE_ACTIVE);
                spacerDatabaseVersion.setBackground(Colors.CONTENT_BACKGROUND);
                spacerCodeTables.setBackground(Colors.CONTENT_BACKGROUND);

                contentPanel.setBackground(Colors.UPDATE_ACTIVE);
                addContentLabel(Loc.get("UPDATE_PROGRAM_UPDATE_AVAILABLE"), Colors.UPDATE_ACTIVE, true);
                addContentLabel(Loc.get("UPDATE_RECOMMENDED"), Colors.UPDATE_ACTIVE, true);
                addContentLabel(Loc.get("UPDATE_NOT_SYNC_NOT_POSSIBLE_WITHOUT_UPDATE"), Colors.UPDATE_ACTIVE);
                addContentLabel(Loc.get("UPDATE_PROGRAM_UPDATE_CLICK_UPDATE_BUTTON"), Colors.UPDATE_ACTIVE);
                addContentLabel(Loc.get("UPDATE_NO_DATA_GET_LOST"), Colors.UPDATE_ACTIVE, true);

                addUpdateProgrammButton(Colors.UPDATE_ACTIVE);
                addSkipToApplicationButton();
                addBackToApplicationButton();
                return;
            }
        }
        String msg = "UPDATE PANEL: Missing case: " +
                "sourceType:" + sourceType + "," +
                "isProgramUpToDate:" + isProgramUpToDate + "," +
                "isDatabaseUpToDate:" + isDatabaseUpToDate + "," +
                "areCodeTablesUpToDate:" + areCodeTablesUpToDate;
        Logger.getLogger(UpdatePanel.class.getName()).log(Level.WARNING, msg);
    }

    @Override
    public ButtonPanel getButtonBar() {
        return buttonPanel = new ButtonPanel();
    }

    @Override
    public SidebarPanel getSideBar() {
        return new SidebarUpdate();
    }

    @Override
    public boolean forceSidebar() {
        return true;
    }

    /**
     * Used to listen to updates of the synchronization process.
     * <p/>
     * <p>
     * {@inheritDoc} </p>
     */
    @Override
    public synchronized void propertyChange(PropertyChangeEvent evt) {
        // TODO: TextPanel einbauen, der eingeblendet wird wenn etwas aktualisiert wird. Diese Funktionen füllen das Textpanel dann.
        if (!evt.getPropertyName().equals(XBookConfiguration.PROPERTY_PROGRESS) || !(evt.getNewValue() instanceof SynchronisationProgress)) {
            return;
        }
        final SynchronisationProgress progress = (SynchronisationProgress) evt.getNewValue();
        switch (progress.getType()) {

            case STARTED:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public synchronized void run() {
                        Footer.showProgressBar();
                    }
                });
                break;
            case WARNING:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public synchronized void run() {

                        Footer.hideProgressBar();
                        Footer.displayWarning(progress.getMessage());
                        setAllButtonsEnabled(true);
                    }
                });
                break;
            case ERROR:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        Footer.hideProgressBar();
                        Footer.displayError(progress.getMessage());
                        setAllButtonsEnabled(true);
                    }
                });
                break;
            case COMPLETE:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        Footer.hideProgressBar();
                        Footer.displayConfirmation(Loc.get("UPDATE_WAS_SUCCESSFUL"));
                        setAllButtonsEnabled(true);
                        UpdatePanel newUpdateScreen = null;
                        if (lastUpdateTry == UpdateType.DATABASE) {
                            newUpdateScreen = new UpdatePanel(previousContent, sourceType,
                                    isProgramUpToDate, true, areCodeTablesUpToDate, isConnectedToServer, controller, autoContinue, true);
                        } else if (lastUpdateTry == UpdateType.CODETABLES) {

                            if (!dbWasUpdatedBefore) {
                                doContinue();
                                return;
                            }
                            newUpdateScreen = new UpdatePanel(previousContent, sourceType,
                                    isProgramUpToDate, isDatabaseUpToDate, true, isConnectedToServer, controller, autoContinue);
                        }
                        //autocontinue has problems for up 2 date screen, because project overview screen is displayed before newUpdateScreen to to no threading
                        if (!autoContinue || (!isProgramUpToDate) || !isDatabaseUpToDate) {
                            Content.setContent(newUpdateScreen);
                        }
                    }
                });
                break;
            case UPDATE:
            default:
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public synchronized void run() {
                        Footer.setProgressBarValue(progress.getProgress());
                    }
                });
                break;
        }
    }

    /**
     * A panel that displays the status if a updateExtention area has to be
     * updates.
     */
    private class UpdateHeaderBar extends JPanel {

        /**
         * The displayed icon.
         */
        private JLabel icon;

        /**
         * Constructor.
         * <p/>
         * Setup the design and add it to the panel.
         *
         * @param topLine         The text of the first line in the bar.
         * @param subline         The text of the second line in the bar.
         * @param backgroundColor The background color of the bar.
         */
        public UpdateHeaderBar(String topLine, String subline, Color backgroundColor) {
            super();

            setLayout(new BorderLayout());
            setOpaque(true);
            setBorder(new EmptyBorder(10, 10, 10, 10));
            setBackground(backgroundColor);

            icon = new JLabel();
            icon.setIcon(Images.TEMP_STATUS_YELLOW);
            icon.setBackground(backgroundColor);
            icon.setBorder(new EmptyBorder(0, 0, 0, 10));
            setSize(32, 32);
            add(BorderLayout.WEST, icon);

            add(BorderLayout.CENTER, new JLabel("<html>" + topLine + ":<br /><b>" + subline + "</b></html>"));
        }

        /**
         * Set the color and the icon.
         *
         * @param background The background color.
         * @param icon32x32  The icon (size of 32x32 pixel)
         */
        public void set(Color background, ImageIcon icon32x32) {
            icon.setIcon(icon32x32);
            icon.setBackground(background);
            setBackground(background);
        }
    }

    private void setAllButtonsEnabled(boolean state) {
        for (IButton b : allButtons) {
            b.setEnabled(state);
        }
    }

    private void addContinueToProjectOverviewButton() {
        allButtons.add(buttonPanel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_FORWARD, Loc.get("CONTINUE"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doContinue();
            }
        }));
    }

    private void addSkipToApplicationButton() {
        allButtons.add(buttonPanel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_SKIP, Loc.get("SKIP"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setAllButtonsEnabled(false);
                controller.updateExtention(null, AbstractController.ResultType.CONTINUE_TO_APPLICATION, previousContent);
            }
        }));
    }

    private void addSkipProgrammUpdateButton() {
        allButtons.add(buttonPanel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_SKIP, Loc.get("SKIP"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setAllButtonsEnabled(false);
                controller.update(null, AbstractController.ResultType.SKIP_UPDATING_PROGRAMM);
                mainFrame.displayProjectOverviewScreen();
            }
        }));
    }

    private void addBackToLoginButton() {
        allButtons.add(buttonPanel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_BACK, Loc.get("BACK"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setAllButtonsEnabled(false);
                controller.update(null, AbstractController.ResultType.BACK_TO_LOGIN);
            }
        }));
    }

    private void addContinueToApplicationButton() {
        allButtons.add(buttonPanel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_CONFIRM, Loc.get("CONTINUE"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setAllButtonsEnabled(false);
                ((AbstractSynchronisationController) controller).updateExtention(null, AbstractController.ResultType.CONTINUE_TO_APPLICATION, previousContent);
            }
        }));
    }

    private void addBackToApplicationButton() {
        allButtons.add(buttonPanel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_BACK, Loc.get("BACK"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setAllButtonsEnabled(false);
                ((AbstractSynchronisationController) controller).updateExtention(null, AbstractController.ResultType.CONTINUE_TO_APPLICATION, previousContent);
            }
        }));
    }

    private void addCloseApplicationButton() {
        allButtons.add(buttonPanel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_CLOSE, Loc.get("CLOSE_APPLICATION"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setAllButtonsEnabled(false);
                controller.update(null, AbstractController.ResultType.CLOSE_APPLICATION);
            }
        }));
    }
}
