package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.filter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public abstract class AbstractSyncFilterRow<T extends Component> extends JPanel {

    /**
     * The logger object of this class.
     */
    private static final Log LOGGER = LogFactory.getLog(AbstractSyncFilterRow.class);

    protected final int ROW_HEIGHT = 25;
    protected final int LABEL_WIDTH = 200;
    private final SyncFilterRowMode DEFAULT_MODE = SyncFilterRowMode.CONTAINS;
    protected SyncFilterRowMode currentMode = DEFAULT_MODE;

    protected final String SEP = " OR ";

    protected final T input;
    protected final JButton buttonContainsEquals;

    public AbstractSyncFilterRow(String labelText) {
        super(new BorderLayout());

        JLabel label = new JLabel("<html><b>" + labelText + ": </b></html>", SwingConstants.RIGHT);
        label.setPreferredSize(new Dimension(LABEL_WIDTH, ROW_HEIGHT));
        add(BorderLayout.WEST, label);

        JPanel inputPanel = new JPanel(new BorderLayout());

        buttonContainsEquals = new JButton(DEFAULT_MODE.getLabel());
        buttonContainsEquals.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchMode();
            }
        });
        inputPanel.add(BorderLayout.WEST, buttonContainsEquals);
        inputPanel.add(BorderLayout.CENTER, input = createInput());
        add(BorderLayout.CENTER, inputPanel);

        setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));
    }

    protected abstract T createInput();

    private void switchMode() {
        if (currentMode == SyncFilterRowMode.CONTAINS) {
            this.currentMode = SyncFilterRowMode.IS;
            buttonContainsEquals.setText(SyncFilterRowMode.IS.getLabel());
        } else {
            this.currentMode = SyncFilterRowMode.CONTAINS;
            buttonContainsEquals.setText(SyncFilterRowMode.CONTAINS.getLabel());

        }
    }

    public abstract SyncFilterRowData getCurrentData();

    public abstract void reset();

}