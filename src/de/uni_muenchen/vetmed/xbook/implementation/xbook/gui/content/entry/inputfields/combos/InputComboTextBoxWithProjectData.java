package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.event.EventRegistry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboTextBoxWithProjectData;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputComboTextBoxWithProjectData extends InputComboTextBox implements IComboTextBoxWithProjectData{

    protected ColumnType columnTypeToLoad;
    protected boolean setCustomInputAllowed;

    /**
     * Constructor.
     *
     * @param columnTypeToSave The database column data of the correspondenting input
     * field where to save the input.
     * @param columnTypeToLoad The database column data of the correspondenting input
     * field from where to load the inserted data.
     */
    public InputComboTextBoxWithProjectData(ColumnType columnTypeToSave, ColumnType columnTypeToLoad) {
        super(columnTypeToSave);
        this.columnTypeToLoad = columnTypeToLoad;
    }

    @Deprecated
    public InputComboTextBoxWithProjectData(ColumnType columnTypeToSave, ColumnType columnTypeToLoad, int gridX, int gridY, SidebarPanel sidebar) {
        this(columnTypeToSave, columnTypeToLoad, false, gridX, gridY, sidebar);
    }

    @Deprecated
    public InputComboTextBoxWithProjectData(ColumnType columnTypeToSave, ColumnType columnTypeToLoad, int gridX, int gridY, String sidebarText) {
        this(columnTypeToSave, columnTypeToLoad, false, gridX, gridY, sidebarText);
    }

    @Deprecated
    public InputComboTextBoxWithProjectData(ColumnType columnTypeToSave, ColumnType columnTypeToLoad, boolean setCustomInputAllowed, int gridX, int gridY, SidebarPanel sidebar) {
        this(columnTypeToSave, columnTypeToLoad);
        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);
        this.setCustomInputAllowed = setCustomInputAllowed;
    }

    @Deprecated
    public InputComboTextBoxWithProjectData(ColumnType columnTypeToSave, ColumnType columnTypeToLoad, boolean setCustomInputAllowed, int gridX, int gridY, String sidebarText) {
        this(columnTypeToSave, columnTypeToLoad);
        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebarText);
        this.setCustomInputAllowed = setCustomInputAllowed;
    }

    @Override
    protected ColumnType getDataFromColumnType() {
        return columnTypeToLoad;
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();

        this.columnTypeToLoad = columnTypeToLoad;
        reloadAvailableValues();
        getCombo().setCustomInputEnabled(setCustomInputAllowed);
        EventRegistry.addValueListener(getTableName(columnTypeToLoad), this);
    }

    // ========================================================================
    public void setCustomInputAllowed(boolean setCustomInputAllowed) {
        this.setCustomInputAllowed = setCustomInputAllowed;
    }


}
