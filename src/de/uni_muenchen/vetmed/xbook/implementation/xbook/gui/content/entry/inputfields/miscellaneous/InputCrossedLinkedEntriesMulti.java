package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.StatusLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractCrossLinkedManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.search.SearchEntry;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InputCrossedLinkedEntriesMulti extends AbstractInputCrossedLinkedEntries {

    private static Log LOGGER = LogFactory.getLog(InputCrossedLinkedEntriesMulti.class);

    /**
     * The text field object.
     */
    protected JTextField textField;
    protected DefaultListModel<ExportResult> listModel;
    private JList<Key> listSelectedItems;
    protected AbstractBaseEntryManager manager;
    protected AbstractCrossLinkedManager mappingManager;
    protected AbstractEntry connectedCrosslinkedEntry;
    private XButton addButton;
    private XButton removeButton;
    private Integer maxItemsAllowed = 999;

    private SearchEntry searchPanel;

    /**
     * Constructor.
     *
     * @param entry          The corresponding Entry object.
     * @param manager        The corresponding manager.
     * @param mappingManager The corresponding cross linked manager.
     * @param isMandatory    Whether the input field is a mandatory field or not.
     */
    public InputCrossedLinkedEntriesMulti(AbstractEntry entry, AbstractBaseEntryManager manager,
                                          AbstractCrossLinkedManager mappingManager, boolean isMandatory) {
        super(new ColumnType("not_in_use" + manager.getLocalisedTableName(), ColumnType.Type.VALUE,
                ColumnType.ExportType.NONE).setDisplayName(manager.getLocalisedTableName()).setMandatory(isMandatory));

        this.manager = manager;
        this.mappingManager = mappingManager;
        this.connectedCrosslinkedEntry = entry;

        setGridY(2);
    }

    @Override
    public void load(DataSetOld data) {
        if (isMultiEdit() && !columnType.isMultiEditAllowed()) {
            setStatus(StatusLabel.Status.NOT_SUPPORTED);
            return;
        }
        EntryDataSet entryData = (EntryDataSet) data;
        try {
            // get id and dbnr of target entry
            ArrayList<Key> entryKey = mappingManager.load(entryData.getProjectKey(), entryData.getEntryKey(),
                    manager.getTableName());
            for (Key key : entryKey) {
                if (checkIfValueIsValid(entryData.getProjectKey(), key)) {
                    addSingleRow(key);
                }
            }
        } catch (StatementNotExecutedException ex) {
            Logger.getLogger(InputCrossedLinkedEntriesMulti.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean checkIfValueIsValid(Key projectKey, Key key) {
        if (mainFrame.getController().getFeatureConfiguration().isPlausibilityCheckAvailable()) {
            try {
                boolean b = manager.isEntryValid(projectKey, key );

                if (!b) {
                    invalidValueLoaded = true;
                    setErrorStyle();
                    JOptionPane.showMessageDialog(InputCrossedLinkedEntriesMulti.this,
                            Loc.get("INVALID_VALUES_DETECTED_DIALOG_MESSAGE", key.getID() + "/" + key.getDBID(),
                                    columnType.getDisplayName()),
                            Loc.get("INVALID_VALUES_DETECTED"),
                            JOptionPane.WARNING_MESSAGE);
                    return false;
                }
            } catch (StatementNotExecutedException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public void save(DataSetOld data) {
        DataTableOld table = data.getOrCreateDataTable(mappingManager.tableName);

        for (int i = 0; i < listModel.getSize(); i++) {
            Key key = getValues(listModel.getElementAt(i));
            if (key == null) {
                continue;
            }
            DataRow row = new DataRow(mappingManager.tableName);
            row.put(mappingManager.getId(manager.getTableName()), key.getID() + "");
            row.put(mappingManager.getDbid(manager.getTableName()), key.getDBID() + "");
            row.put(AbstractCrossLinkedManager.TABLE_LINKED_TO, manager.getTableName());
            table.add(row);
        }
    }

    @Override
    public String getStringRepresentation() {
        return "<n.a.>";
    }

    @Override
    public void clear() {
        invalidValueLoaded = false;
        setDefaultStyle(true);
        listModel.clear();
        String s = columnType.getDisplayName();
        boolean b = isEnabled();
        if (addButton != null && listModel != null && maxItemsAllowed != null && isEnabled()) {
            addButton.setEnabled(listModel.size() < maxItemsAllowed);
        }
    }

    @Override
    public boolean isValidInput() {
        return !listModel.isEmpty();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(multiPanel);
        comp.add(addButton.getButton());
        comp.add(removeButton.getButton());
        comp.add(listSelectedItems);
        return comp;
    }

    @Override
    protected void addSingleRow(Key key) {
        if (maxItemsAllowed != null && listModel.size() >= maxItemsAllowed) {
            Footer.displayWarning(Loc.get("THE_INPUT_FIELD_XX_ONLY_ALLOWS_XX_ENTRIES", columnType.getDisplayName(),
                    maxItemsAllowed));
            return;
        }
        try {
            Key projectKey = apiControllerAccess.getCurrentProject().getProjectKey();
            ExportResult export = getCustomizedExportResult(manager.getEntriesForDisplay(projectKey, key));
            if (!listModel.contains(export)) {
                listModel.addElement(export);
                revalidate();

            }
            if (maxItemsAllowed != null && listModel.size() >= maxItemsAllowed) {
                addButton.setEnabled(false);
            }
        } catch (StatementNotExecutedException | NotLoadedException ex) {
            Logger.getLogger(InputCrossedLinkedEntriesMulti.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected ExportResult getCustomizedExportResult(ExportResult export) {
        return export;
    }

    public void setSearchPanel(SearchEntry searchPanel) {
        this.searchPanel = searchPanel;
        searchPanel.setPreviousPanel(connectedCrosslinkedEntry);
        searchPanel.setAddablePanel(this);
        searchPanel.setManager(manager);
    }

    /**
     * Returns if there is selected any data in the input field or not.
     *
     * @return <code>true</code> if there is selected any data,
     * <code>false</code> else.
     */
    public boolean hasData() {
        return listModel.getSize() > 0;
    }

    public XButton getAddButton() {
        return addButton;
    }

    public XButton getRemoveButton() {
        return removeButton;
    }

    public JList<Key> getListSelectedItems() {
        return listSelectedItems;
    }

    public void actionOnPressRemoveButton() {
        removeSelectedItems();
    }

    public void actionOnPressAddButton() {
        if (searchPanel == null) {
            LOGGER.error("Search Panel is null");
            return;
        }
        searchPanel.clearFilter();
        Content.setContent(searchPanel);
    }

    /**
     * Set how many items are allowed in this field. Use <code>null</code> for infinity items.
     *
     * @param items The number of items that are allowed in this field. Ise
     *              <code>null</code> for infinity.
     */
    public void setMaxItemsAllowed(Integer items) {
        this.maxItemsAllowed = items;
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);

        // set the list active in any way, items should be selectable and openable
        listSelectedItems.setEnabled(true);
        colorizeEnableSpecificItems(listSelectedItems, true);
    }

    @Override
    public void setEnabledForRights(boolean b) {
        super.setEnabledForRights(b);

        // set the list active in any way, items should be selectable and openable
        listSelectedItems.setEnabled(true);
        colorizeEnableSpecificItems(listSelectedItems, true);
    }

    protected void removeSelectedItems() {
        if (listModel.isEmpty()) {
            return;
        }
        int[] selected;
        if (listModel.size() == 1 && maxItemsAllowed == 1) {
            selected = new int[]{0};
        } else {
            selected = listSelectedItems.getSelectedIndices();
        }
        for (int i = selected.length - 1; i >= 0; i--) {
            listModel.removeElementAt(selected[i]);
        }
        if (maxItemsAllowed != null && listModel.size() < maxItemsAllowed) {
            addButton.setEnabled(true);
        }
    }

    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        addButton = new XButton(Loc.get("ADD"));
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionOnPressAddButton();
            }
        });
        multiPanel.add(BorderLayout.NORTH, addButton);

        removeButton = new XButton(Loc.get("REMOVE_SELECTED"));
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionOnPressRemoveButton();
            }
        });
        multiPanel.add(BorderLayout.SOUTH, removeButton);

        listModel = new DefaultListModel<>();
        listSelectedItems = new JList(listModel) {

            @Override
            public String getToolTipText(MouseEvent event) {
                int index = locationToIndex(event.getPoint());
                if (index > -1) {
                    return listModel.getElementAt(index).toString();
                } else {
                    return "";
                }
            }

        };
        listSelectedItems.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        listSelectedItems.setLayoutOrientation(JList.VERTICAL);
        listSelectedItems.setBackground(new Color(240, 240, 240));
        listSelectedItems.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                // double-click detected
                if (evt.getClickCount() == 2) {
                    System.out.println("selected index: " + listSelectedItems.getSelectedIndex());
                    if (getListSelectedItems().getSelectedIndex() < 0) {
                        return;
                    }
                    Key key = getValues(listModel.getElementAt(listSelectedItems.getSelectedIndex()));
                    if (key == null) {
                        return;
                    }
                    try {
                        apiControllerAccess.loadAndDisplayEntry(manager, key, apiControllerAccess.hasWriteRights());
                    } catch (NotLoggedInException | NoRightException | StatementNotExecutedException | NotLoadedException | EntriesException ex) {
                        Logger.getLogger(InputCrossedLinkedEntriesMulti.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        JScrollPane listScroller = new JScrollPane(listSelectedItems);
        listScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        listScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        listScroller.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        listScroller.removeMouseWheelListener(listScroller.getMouseWheelListeners()[0]);
        multiPanel.add(BorderLayout.CENTER, listScroller);

        setContent(multiPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

    public ArrayList<ExportResult> getCurrentData() {
        ArrayList<ExportResult> elements = new ArrayList<>();
        for (int i = 0; i < listModel.size(); i++) {
            elements.add(listModel.getElementAt(i));
        }
        return elements;
    }

}
