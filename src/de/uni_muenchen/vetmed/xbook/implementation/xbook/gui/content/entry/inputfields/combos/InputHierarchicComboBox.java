package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.event.CodeTableListener;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.HierarchicComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.UpdateableEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IHierarchicComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputHierarchicComboBox extends AbstractInputElement implements IHierarchicComboBox {

    /**
     * The height of each single combo box.
     */
    protected int comboHeight = 20;

    /**
     * The content panel where the combo boxes and the labels will be added.
     */
    protected HierarchicComboBox dynamicComboBox;

    /**
     * A wrapper panel that holds all elements of this input field.
     */
    protected JPanel wrapperPanel;

    private boolean orderByValue;
    private String[] labels = null;

    /**
     * A place where so save all created combo boxes. Necessary for the listeners to be able to get the new combo boxes
     * as well.
     */
    private final ArrayList<Component> allComboBoxes = new ArrayList<>();

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input field.
     */
    public InputHierarchicComboBox(ColumnType columnType) {
        super(columnType);
    }

    @Override
    public void load(DataSetOld data) {
        dynamicComboBox.load(data);
        checkIfValueIsValid(data);
    }

    private void checkIfValueIsValid(DataSetOld data) {
        if (mainFrame.getController().getFeatureConfiguration().isPlausibilityCheckAvailable()) {
            DataRow list = data.getDataRowForTable(getTableName());
            final String valueToCheck = list.get(columnType);
            try {
                boolean b = apiControllerAccess.isValidCodeID(columnType, valueToCheck);

                if (!b) {
                    invalidValueLoaded = true;
                    setErrorStyle();
                    JOptionPane.showMessageDialog(InputHierarchicComboBox.this,
                            Loc.get("INVALID_VALUES_DETECTED_DIALOG_MESSAGE", valueToCheck,
                                    columnType.getDisplayName()),
                            Loc.get("INVALID_VALUES_DETECTED"),
                            JOptionPane.WARNING_MESSAGE);
                }
            } catch (NotLoggedInException e) {
                mainFrame.displayLoginScreen();
            } catch (StatementNotExecutedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void clear() {
        dynamicComboBox.clear();
    }

    @Override
    public void save(DataSetOld data) {
        dynamicComboBox.save(data);
    }

    @Override
    public String getStringRepresentation() {
        return dynamicComboBox.inputToString();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.addAll(allComboBoxes);
        comp.add(dynamicComboBox);
        comp.addAll(dynamicComboBox.getFocusableComponents());
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return dynamicComboBox.getSelectedInput() != null;
    }

    public HierarchicComboBox getDynamicComboBox(boolean orderByValue, String[] labels) {
        if (dynamicComboBox == null) {
            dynamicComboBox = new HierarchicComboBox(apiControllerAccess, columnType, orderByValue, labels);
            dynamicComboBox.setSidebar(sidebar);
            dynamicComboBox.setComboHeight(comboHeight);
            allComboBoxes.addAll(dynamicComboBox.getAllBoxes());
        }
        return dynamicComboBox;
    }

    public HierarchicComboBox getDynamicComboBox() {
        return dynamicComboBox;
    }

    @Override
    public void setEntry(UpdateableEntry entry) {
        super.setEntry(entry);
        dynamicComboBox.setEntry(entry);
    }


    @Override
    protected void createFieldInput() {
        wrapperPanel = new JPanel(new GridLayout(1, 2));
        wrapperPanel.add(getDynamicComboBox(orderByValue, labels));

        setContent(wrapperPanel);

        if (columnType.isMandatory()) {
            dynamicComboBox.setMandatory(true);
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }

        apiControllerAccess.addCodeTableEventListener(new CodeTableListener() {
            @Override
            public void onCodeTablesUpdated() {
                dynamicComboBox.reloadAvailableValues();
                reload();
            }
        });
        dynamicComboBox.setSidebar(sidebar);

    }

    @Override
    public void setSidebar(SidebarPanel sidebar) {
        super.setSidebar(sidebar);
        if (dynamicComboBox != null) {
            dynamicComboBox.setSidebar(sidebar);
        }
    }

    // ========================================================================
    @Override
    public void setSelectedValue(String id) {
        dynamicComboBox.setValue(id);
    }

    @Override
    public void setOrderByValue(boolean orderByValue) {
        this.orderByValue = orderByValue;
    }

    @Override
    public void setLabels(String[] labels) {
        this.labels = labels;
    }

    @Override
    public DataColumn getSelectedInput() {
        return dynamicComboBox.getSelectedInput();
    }

    @Override
    public void setComboHeight(int comboHeight) {
        this.comboHeight = comboHeight;
        if (dynamicComboBox != null) {
            dynamicComboBox.setComboHeight(comboHeight);
        }
    }
}
