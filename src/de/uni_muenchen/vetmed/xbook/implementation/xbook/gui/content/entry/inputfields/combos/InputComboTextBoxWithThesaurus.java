package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos;

import de.uni_muenchen.vetmed.xbook.api.event.CodeTableListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboTextBoxWithThesaurus;
import java.util.Collections;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputComboTextBoxWithThesaurus extends InputComboTextBox implements IComboTextBoxWithThesaurus {

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     */
    public InputComboTextBoxWithThesaurus(ColumnType columnType) {
        super(columnType);
    }

    /**
     * 
     * @return 
     */
    public UniqueArrayList<String> getData() {
        try {
            UniqueArrayList<String> s = new UniqueArrayList<>();
            s.add("");
            s.addAll(apiControllerAccess.getHashedCodeTableEntries(columnType).values());
            s.addAll(apiControllerAccess.getInputUnitInformation(columnType.getColumnName()));
            Collections.sort(s);
            return s;
        } catch (NotLoggedInException | StatementNotExecutedException | NotLoadedException ex) {
            Logger.getLogger(InputComboTextBoxWithThesaurus.class.getName()).log(Level.SEVERE, null, ex);
            return new UniqueArrayList<>();
        }
    }

    @Override
    public void reloadAvailableValues() {
        Object selected = combo.getSelectedItem();
        combo.removeAllItems();
        UniqueArrayList<String> s = getData();
        combo.setItems(s);
        combo.setSelectedItem(selected);
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();

        combo.setEditable(true);

        apiControllerAccess.addCodeTableEventListener(new CodeTableListener() {
            @Override
            public void onCodeTablesUpdated() {
                reloadAvailableValues();
            }
        });
    }

}
