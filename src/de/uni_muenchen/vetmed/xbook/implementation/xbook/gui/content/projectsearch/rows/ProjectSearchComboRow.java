package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.rows;

import de.uni_muenchen.vetmed.xbook.api.datatype.CodeTableHashMap;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ProjectSearchComboRow extends ProjectSearchTextRow {

	protected JComboBox<String> combo;
	protected JButton button;

	public ProjectSearchComboRow(ColumnType columnType, AbstractMainFrame mainFrame) {
		super(columnType);

		JPanel comboWrapper = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		combo = new JComboBox<>();
		combo.setPreferredSize(new Dimension(150, ROW_HEIGHT));

		try {
			CodeTableHashMap data = mainFrame.getController().getHashedCodeTableEntries(columnType);
			for (Map.Entry<String, String> hash : data.entrySet()) {
				combo.addItem(hash.getValue());
			}
		} catch (NotLoggedInException | StatementNotExecutedException ex) {
			Logger.getLogger(ProjectSearchComboRow.class.getName()).log(Level.SEVERE, null, ex);
		}

		comboWrapper.add(ComponentHelper.wrapComponent(combo, 0, GAP_H, 0, 0));
		button = new JButton(">>");
		button.setPreferredSize(new Dimension(50, ROW_HEIGHT));
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String t = input.getText();
				if (t.isEmpty()) {
					t = combo.getSelectedItem().toString();
				} else {
					t += SEP + combo.getSelectedItem().toString();
				}
				input.setText(t);
			}
		});
		comboWrapper.add(ComponentHelper.wrapComponent(button, 0, GAP_H, 0, 0));

		leftWrapper.add(comboWrapper);
	}

	@Override
	protected ArrayList<JComponent> getAllInputElements() {
		ArrayList<JComponent> elements = super.getAllInputElements();
		elements.add(combo);
		elements.add(button);
		return elements;
	}

}
