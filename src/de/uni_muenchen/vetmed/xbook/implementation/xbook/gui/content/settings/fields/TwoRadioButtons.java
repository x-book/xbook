package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.fields;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;

/**
 * A panel that holds two radio buttons that are arranged horizontal.
 */
public class TwoRadioButtons extends JPanel {

    /**
     * The radio button for the "no" option.
     */
    private JRadioButton radioNo;
    /**
     * The radio button for the "yes" option.
     */
    private JRadioButton radioYes;

    /**
     * Constructor.
     *
     * @param background The background color.
     * @param labelNo    The label for the "no" option.
     * @param labelYes   The label for the "yes" option.
     */
    public TwoRadioButtons(Color background, String labelNo, String labelYes) {
        setLayout(new BorderLayout());
        setBackground(background);

        JPanel subPanel = new JPanel(new FlowLayout());
        subPanel.setBackground(background);

        radioNo = new JRadioButton(labelNo);
        radioNo.setBackground(background);
        radioYes = new JRadioButton(labelYes);
        radioYes.setBackground(background);
        subPanel.add(radioYes);
        subPanel.add(radioNo);

        add(BorderLayout.WEST, subPanel);

        ButtonGroup group = new ButtonGroup();
        group.add(radioYes);
        group.add(radioNo);
    }

    /**
     * Set the "yes" option selected or not. The "no" option will be set, too.
     *
     * @param status <code>true</code> if the "yes" option is selected, else <code>false</code>.
     */
    public void setYesSelected(boolean status) {
        radioYes.setSelected(status);
        radioNo.setSelected(!status);
    }

    /**
     * Returns whether the "yes" option is selected or not.
     *
     * @return <code>true</code> if the "yes" option is selected, else <code>false</code>.
     */
    public boolean isYesSelected() {
        return radioYes.isSelected();
    }

    /**
     * Adds a custom Action Listener that is called then the selection is changed.
     *
     * @param listener The ActionListener to add.
     */
    public void addCustomActionListener(ActionListener listener) {
        radioNo.addActionListener(listener);
        radioYes.addActionListener(listener);
    }

    /**
     * Enables or disables the input fields.
     *
     * @param b The enability status.
     */
    public void setEnabled(boolean b) {
        radioNo.setEnabled(b);
        radioYes.setEnabled(b);
    }
}
