package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.dependable;

import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;

import java.util.ArrayList;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 23.06.2017.
 */
public abstract class AbstractBaseDependableEntry extends AbstractDependableEntry {
    protected Key loadedEntry;

    public AbstractBaseDependableEntry(GeneralInputMaskMode mode, AbstractBaseEntryManager thisEntry, ArrayList<AbstractBaseEntryManager> allEntries) {
        super(mode, thisEntry, allEntries);
    }

    public AbstractBaseDependableEntry(GeneralInputMaskMode mode, boolean doInit, AbstractBaseEntryManager thisEntry, ArrayList<AbstractBaseEntryManager> allEntries) {
        super(mode, doInit, thisEntry, allEntries);
    }

    public AbstractBaseDependableEntry(GeneralInputMaskMode mode, EntryDataSet data, AbstractBaseEntryManager thisEntry, ArrayList<AbstractBaseEntryManager> allEntries) {
        super(mode, data, thisEntry, allEntries);
    }
}
