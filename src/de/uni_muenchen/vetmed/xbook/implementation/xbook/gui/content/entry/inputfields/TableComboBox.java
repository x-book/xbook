package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields;

import java.awt.Component;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import de.uni_muenchen.vetmed.xbook.api.helper.DataManagementHelper;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;

/**
 *
 * @author t.sizova
 */
public class TableComboBox extends DefaultComboBoxModel {
    /**
     * The combo box object.
     */
    protected JComboBox combo;
    /**
     * An object that holds all specific data for the input field.
     */
    final ColumnType columnType;
      /**
     * The elements that are selectable in the combo box.
     */
    protected ArrayList<DataColumn> items;
    
    public TableComboBox(final ColumnType columnType)  {
        
        super();
        this.columnType = columnType;  
        combo = new JComboBox(DataManagementHelper.convertDbDataHashArrayListToStringArray(items, true));
        combo.setEditable(true);

//        JTextField tf = (JTextField) combo.getEditor().getEditorComponent();
//        tf.addFocusListener(new FocusAdapter() {
//            @Override
//            public void focusLost(FocusEvent event) {
//                JTextField tf = (JTextField) combo.getEditor().getEditorComponent();
//                if (tf.getText().isEmpty()) {
//                    return;
//                }
//
//                try {
//                    items = apiControllerAccess.getController().getCodeTableEntries(columnType);
//                } 
//                catch (NotLoggedInException | StatementNotExecutedException ex) {
//                    Logger.getLogger(InputComboIdBox2012.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                // first check if the string of the textfield is a valid string
//                for (DbDataHash i : items) {
//                    if (i.getValue().equals(tf.getText())) {
//                        return;
//                    }
//                }
//
//                // otherwise check if it is valid id
//                try {
//                    String input = tf.getText();
//                    for (DbDataHash i : items) {
//                        if (i.getID().equals(input)) {
//                            setSelectedItem(i.getValue());
//                            return;
//                        }
//                    }
//                    // if no valid id
//                    setSelectedItem("");
//                    Footer2012.displayWarning(Loc.getString("ID_NOT_FOUND"));
//                } catch (NumberFormatException nfe) {
//                    // if input is unknown text
//                    setSelectedItem("");
//                    Footer2012.displayWarning(Loc.getString("INPUT_VALUE_NOT_VALID", columnType.getDisplayName()));
//                }
//            }
//        });
    }

     /**
     * Add the elements of a collection to the combo box.
     *
     * @param items The items to add.
     */
    public void addItemsToCombo(ArrayList<String> items) {
        for (String s : items) {
            combo.addItem(s);
        }
    }
    
    public ArrayList<Component> getFocusableComponents() {
        ArrayList<Component> components = new ArrayList<>();
        components.add(combo.getEditor().getEditorComponent());
        return components;
    }
}
