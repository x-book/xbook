package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.specific_reuseable;

import com.toedter.calendar.JTextFieldDateEditor;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XDateChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Daniel Kaltenthaler
 */
public class InputABIBInsurance extends AbstractInputElement {

    private JLabel labelLog;
    private JButton buttonLog;
    private ArrayList<InsuranceLogEntry> logData;
    private JTextField valuationField;
    private JComboBox<String> comboCurrency;
    private XDateChooser dateField;
    private final int GRID = 2;
    private CodeTableHashMap currencies;
    /**
     * The elements that are selectable in the combo box.
     */
    protected ArrayList<DataColumn> items;
    /**
     * Object to format the date.
     */
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private DecimalFormat decimalFormat;

    private final String tableNameInsurance;

    private final ColumnType columnTypeDate;
    private final ColumnType columnTypeCurrency;
    private final ColumnType columnTypeValuation;

    private int countInLog;

    /**
     * Constructor.
     *
     * @param tableNameInsurance
     * @param columnTypeDate
     * @param columnTypeCurrency
     * @param columnTypeValuation
     */
    public InputABIBInsurance(String tableNameInsurance, ColumnType columnTypeDate, ColumnType columnTypeCurrency, ColumnType columnTypeValuation) {
        super(columnTypeValuation);

        this.columnTypeDate = columnTypeDate;
        this.columnTypeCurrency = columnTypeCurrency;
        this.columnTypeValuation = columnTypeValuation;

        this.tableNameInsurance = tableNameInsurance;

        setGridX(2);
        setGridY(2);
        setSidebar(new SidebarEntryField(Loc.get("VALUATION_OF_INSURANCE"), ""));
    }

    private JLabel createLabel(String text) {
        JLabel lab = new JLabel(text);
        lab.setBackground(Colors.INPUT_FIELD_BACKGROUND);
        return lab;
    }

    private void setLogLabel(int countInLog, InsuranceLogEntry newestLogEntry) {
        this.countInLog = countInLog;
        String label = "";

        if (newestLogEntry != null && newestLogEntry.getCurrency() != null && newestLogEntry.getDate() != null) {
            label += "<b>" + Loc.get("CURRENT_VALUATION_OF_INSURANCE") + ":</b>";
            label += "<br>" + newestLogEntry.getValuation() + " " + newestLogEntry.getCurrency();
        }

        labelLog.setText("<html><div align='right'>" + label + "</div></html>");
        buttonLog.setEnabled(countInLog > 0);
        buttonLog.setText("<html><center>" + Loc.get("OPEN_LOG") + "<br>" + "(" + Loc.get("NUMBER_OF_ENTRIES_IN_LOG", countInLog) + ")" + "</center></html>");
    }

    private void removeLogLabel(boolean displayLabelNone) {
        if (displayLabelNone) {
            labelLog.setText(Loc.get("NONE"));
        } else {
            labelLog.setText("");
        }
        buttonLog.setEnabled(false);
        buttonLog.setText(Loc.get("OPEN_LOG"));
    }

    public ArrayList<InsuranceLogEntry> getLogData() {
        return logData;
    }

    public JButton getButtonLog() {
        return buttonLog;
    }

    @Override
    public void load(DataSetOld data) {
        DataTableOld list = data.getOrCreateDataTable(tableNameInsurance);
        ArrayList<InsuranceLogEntry> log = new ArrayList<>();
        for (DataRow row : list) {
            String valuation = "";
            String date = "";
            String currency = "";
            date = convertTime(row.get(columnTypeDate));
            currency = currencies.convertIdAsStringToString(row.get(columnTypeCurrency));
            Integer intValue = Integer.parseInt(row.get(columnTypeValuation));
            valuation = decimalFormat.format((intValue / 100.0));

            log.add(new InsuranceLogEntry(valuation, currency, date));
        }
        this.logData = log;
        if (!log.isEmpty()) {
            setLogLabel(list.size(), log.get(log.size() - 1));
        } else {
            removeLogLabel(true);
        }
    }

    public void clearInput() {
        valuationField.setText("");
        comboCurrency.setSelectedIndex(0);
        setDefaultCurrencyValue("€");
        dateField.setDate(null);
    }

    @Override
    public void clear() {
        clearInput();
        removeLogLabel(false);
        logData = new ArrayList<>();
        setDefaultCurrencyValue("€");
    }

    @Override
    public void save(DataSetOld data) {
        if (!isValidInput()) {
            return;
        }
        String valuation = "" + floatAsStringToInteger(valuationField.getText());
        String currency = currencies.convertStringToIdAsString((String) comboCurrency.getSelectedItem());
        Date date = dateField.getDate();

        if (date == null || currency == null || valuation == null || valuation.isEmpty()) {
            return;
        }

        DataTableOld list = data.getOrCreateDataTable(tableNameInsurance);
        DataRow dataa = new DataRow(tableNameInsurance);
        dataa.add(new DataColumn(valuation, columnTypeValuation));
        dataa.add(new DataColumn(currency, columnTypeCurrency));
        dataa.add(new DataColumn(dateFormat.format(date), columnTypeDate));
        list.add(dataa);
    }

    /**
     * Converts the date from the format "yyyy-mm-dd hh:mm:ss" to "dd.mm.yyyy".
     *
     * @param oldFormat The date in the format "yyyy-mm-dd hh:mm:ss".
     * @return The date in the format "dd.mm.yyyy".
     */
    private String convertTime(String oldFormat) {
        String yyyy = oldFormat.substring(0, 4);
        String mm = oldFormat.substring(5, 7);
        String dd = oldFormat.substring(8, 10);

        return dd + "." + mm + "." + yyyy;
    }

    @Override
    public String getStringRepresentation() {
        return valuationField.getText();
    }

    private InputABIBInsurance getThis() {
        return this;
    }

    @Override
    public boolean isValidInput() {
        if (!valuationField.getText().isEmpty() && !((String) comboCurrency.getSelectedItem()).isEmpty() && dateField.getDate() != null) {
            return true;
        }
        if (valuationField.getText().isEmpty() && dateField.getDate() == null) {
            return true;
        }
        return false;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> list = new ArrayList<>();
        list.addAll(dateField.getFocusableComponents());
        list.add(comboCurrency);
        list.add(buttonLog);
        list.add(valuationField);
        return list;
    }

    @Override
    public void setErrorStyle() {
        // see actionOnFocusLost()
    }



    private String getDefaultCurrency() {
        return (String) currencies.values().toArray()[currencies.size() / 2];
    }

    private String getDefaultValuation() {
        return "100";
    }

    private Date getDefaultDate() {

        return new GregorianCalendar(2013, 7, 13).getTime();
    }

    @Override
    public void actionOnFocusGain() {
        super.actionOnFocusGain();
        dateField.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        valuationField.setBorder(new JTextField().getBorder());
    }

    @Override
    public void actionOnFocusLost() {
        String textInput = (((JTextFieldDateEditor) dateField.getDateEditor()).getText());
        if (!textInput.isEmpty() && dateField.getDate() == null) {
            dateField.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
            dateField.setDate(null);
            Footer.displayWarning(Loc.get("INVALID_INPUT_IN_FIELD", columnType.getDisplayName()));
        }
        if (dateField.getDate() == null && !valuationField.getText().isEmpty()) {
            dateField.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
            Footer.displayWarning(Loc.get("INVALID_INPUT_IN_FIELD", columnType));
        }
        if (dateField.getDate() != null && valuationField.getText().isEmpty()) {
            valuationField.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
            Footer.displayWarning(Loc.get("INVALID_INPUT_IN_FIELD", columnType));
        }
        super.actionOnFocusLost();
    }

    /**
     * Converts the given string to a integer without comma seperation. This is
     * only usable for currency in which you only have 2 values after the comma.
     * eg 1,28 are converted to 128 (the currency is not used inside this
     * method)
     *
     * @param s
     * @return
     * @throws NumberFormatException
     */
    private Integer floatAsStringToInteger(String s) throws NumberFormatException {
        System.out.print("convert '" + s + "' to ");

        if (s.contains(".")) {
            if (s.indexOf(".") < s.length() - 3) {
                throw new NumberFormatException();
            }
        }
        if (s.contains(",")) {
            if (s.indexOf(",") < s.length() - 3) {
                throw new NumberFormatException();
            }
        }
        s = s.replace(".", ",");

        if (!s.contains(".") && !s.contains(",")) {
            s += ",00";
        } else if (s.endsWith(",") || s.endsWith(".")) {
            s += "00";
        } // fill to two decimal values after "." or ","
        else if (s.substring(s.length() - 2, s.length() - 1).equals(".")
                || s.substring(s.length() - 2, s.length() - 1).equals(",")) {
            s += "0";
        }

        s = s.replace(",", "");
        // s = s.replace(".", "");
        System.out.println("'" + s + "'");
        return Integer.parseInt(s);
    }

    @Override
    protected void createFieldInput() {
        decimalFormat = new DecimalFormat();
        decimalFormat.setMinimumFractionDigits(2);
        decimalFormat.setMaximumFractionDigits(2);
        // do not display the thousand-grouping
        decimalFormat.setGroupingUsed(false);

        int panelWidthAbsolute = getPreferredSize().width;
        int panelWidthToUse = panelWidthAbsolute - 15;

        JPanel outerWrapper = new JPanel(new BorderLayout());

        JPanel panelRightWrapper = new JPanel(new BorderLayout());
        JPanel panelTop = new JPanel(new GridLayout(1, 2));
        JPanel valuationWrapper = new JPanel(new BorderLayout());
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        JPanel leftWrapper = new JPanel(new BorderLayout());
        valuationField = new JTextField();
        valuationField.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
        valuationField.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        valuationField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent event) {
                if (!(valuationField.getText().isEmpty())) {
                    try {
                        // first set the correct integer format (without dots and commas)
                        //valuationField.setText("" + floatAsStringToInteger(valuationField.getText()) / 100.0);
                        valuationField.setText("" + decimalFormat.format(floatAsStringToInteger(valuationField.getText()) / 100.0));
                    } catch (NumberFormatException e) {
                        clearInput();
                        Footer.displayWarning(Loc.get("NO_VALID_INPUT_ONLY_FLOAT_VALUES"));
                        setErrorStyle();
                    }
                }
            }
        });
        valuationField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (valuationField.getText().isEmpty()) {
                    valuationField.setToolTipText(null);
                } else {
                    valuationField.setToolTipText(valuationField.getText());
                }
            }
        });
        valuationField.setPreferredSize(new Dimension(1 * (panelWidthToUse / GRID), Sizes.INPUT_FIELD_HEIGHT));
        valuationWrapper.add(BorderLayout.NORTH, new JLabel(Loc.get("VALUATION")));
        valuationWrapper.add(BorderLayout.CENTER, valuationField);
        leftWrapper.add(BorderLayout.CENTER, valuationWrapper);

        JPanel currencyWrapper = new JPanel(new BorderLayout());
        try {
            currencies = mainFrame.getController().getHashedCodeTableEntries(columnTypeCurrency);
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            currencies = new CodeTableHashMap();
        }

        comboCurrency = new JComboBox<>();
        for (String value : currencies.values()) {
            comboCurrency.addItem(value);
        }
        comboCurrency.setPreferredSize(new Dimension(50, Sizes.INPUT_FIELD_HEIGHT));
        currencyWrapper.add(BorderLayout.NORTH, new JLabel(" "));
        currencyWrapper.add(BorderLayout.CENTER, comboCurrency);
        leftWrapper.add(BorderLayout.EAST, currencyWrapper);
        panelTop.add(ComponentHelper.wrapComponent(leftWrapper, 0, 0, 0, 0));

        JPanel dateFieldWrapper = new JPanel(new BorderLayout());
        dateField = new XDateChooser();
        dateField.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        dateField.setPreferredSize(new Dimension(1 * (panelWidthToUse / GRID), Sizes.INPUT_FIELD_HEIGHT));

        dateFieldWrapper.add(BorderLayout.NORTH, new JLabel(Loc.get("DATE")));
        dateFieldWrapper.add(BorderLayout.CENTER, dateField);
        panelTop.add(ComponentHelper.wrapComponent(dateFieldWrapper, 0, 0, 0, 10));

        panelRightWrapper.add(BorderLayout.NORTH, panelTop);
        outerWrapper.add(BorderLayout.NORTH, panelRightWrapper);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelLog = createLabel("");
        p.add(labelLog);
        buttonLog = new JButton("------------------");
        buttonLog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Content.setContent(new ContentABIBInsuranceList(Content.getCurrentContent(), getThis()));
            }
        });
        buttonLog.setEnabled(false);
        buttonLog.setPreferredSize(new Dimension((int) ((panelWidthToUse / GRID) * 1.0), Sizes.INPUT_FIELD_HEIGHT));
        p.add(buttonLog);
        outerWrapper.add(BorderLayout.SOUTH, p);

        setContent(outerWrapper);
    }

    public void setDefaultCurrencyValue(String value) {
        comboCurrency.setSelectedItem(value);
    }

    @Override
    public void setEnabledForRights(boolean b) {
        super.setEnabledForRights(b);
        buttonLog.setEnabled(countInLog > 0);

    }

}
