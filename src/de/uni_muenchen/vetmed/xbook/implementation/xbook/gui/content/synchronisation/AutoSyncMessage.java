package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class AutoSyncMessage extends AbstractContent {

    private AbstractContent previousContent;
    private Key loadedEntryKey;

    public AutoSyncMessage(AbstractContent previousContent, Key loadedEntryKey) {
        this.previousContent = previousContent;
        this.loadedEntryKey = loadedEntryKey;
        init();
    }

    @Override
    protected JPanel getContent() {
        JPanel wrapper = new JPanel(new StackLayout());
        wrapper.setBackground(Colors.CONTENT_BACKGROUND);
        MultiLineTextLabel text = new MultiLineTextLabel(Loc.get("AUTO_SYNC_MESSAGE_TEXT"));
        text.setBackground(Colors.CONTENT_BACKGROUND);
        wrapper.add(text);
        return wrapper;
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();
        panel.addButtonToSouthWest(Loc.get("RELOAD_ENTRY"), 1.5, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //TODO !!!
//                try {
//                    mainFrame.getController().loadAndDisplayEntry(loadedEntryKey, true);
//                } catch (NotLoggedInException | NoRightException | StatementNotExecutedException | NotLoadedException | EntriesException ex) {
//                    Logger.getLogger(AutoSyncMessage.class.getName()).log(Level.SEVERE, null, ex);
//                }
            }
        });
        panel.addButtonToSouthEast(Loc.get("CONTINUE"), 1.5, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Content.setContent(previousContent);
            }
        });
        return panel;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

}
