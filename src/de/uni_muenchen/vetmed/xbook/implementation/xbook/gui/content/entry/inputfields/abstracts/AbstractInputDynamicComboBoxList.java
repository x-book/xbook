package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts;

import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;

import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JList;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.DynamicComboBoxListId;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;

/**
 * A combobox as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 * @param <T>
 */
public abstract class AbstractInputDynamicComboBoxList<T> extends AbstractInputElement {

    protected DynamicComboBoxListId<T> dynamicCombo;
    protected String table;

    public AbstractInputDynamicComboBoxList(ColumnType columnType, String table) {
        super(columnType);
        this.table = table;
    }

    public abstract Comparator getComparator();

    public abstract T getEmptyItem();

    @Override
    public abstract void load(DataSetOld data);

    @Override
    public void clear() {
        dynamicCombo.clearInput();
        dynamicCombo.removeAllItems();
        reload();
    }

    @Override
    public abstract void save(DataSetOld data);

    @Override
    public String getStringRepresentation() {
        ArrayList<T> item = dynamicCombo.getListItems(false);
        if (item == null) {
            return "";
        }
        String value = "";
        for (T t : item) {
            value += t.toString() + ",";
        }
        return StringHelper.cutString(value, ",");
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> list = new ArrayList<>();
        list.add(dynamicCombo);
        list.add(dynamicCombo.getButtonAdd().getButton());
        list.add(dynamicCombo.getButtonRemove().getButton());
        list.add(dynamicCombo.getList());
        return list;
    }

    @Override
    public void reload() {
        try {
            dynamicCombo.getComboBox().removeAllItems();
            ArrayList<String> values = new ArrayList<>();
            values.add("");
            values.addAll(apiControllerAccess.getInputUnitInformation(columnType.getColumnName()));
            dynamicCombo.getComboBox().setItems(values);
        } catch (NotLoggedInException | StatementNotExecutedException | NotLoadedException ex) {
            Logger.getLogger(AbstractInputDynamicComboBoxList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    @Override
    public void actionOnFocusLost() {
        dynamicCombo.addInputToList();
        super.actionOnFocusLost();
    }

    /**
     * Returns the JList object out of the dynamicCombo box.
     *
     * @return The list.
     */
    public JList getList() {
        return dynamicCombo.getList();
    }

    public DynamicComboBoxListId<T> getCombo() {
        return dynamicCombo;
    }

    @Override
    protected void createFieldInput() {
        this.table = table;

        dynamicCombo = new DynamicComboBoxListId(columnType.getDisplayName()) {

            @Override
            public Comparator getComparator() {
                return AbstractInputDynamicComboBoxList.this.getComparator();
            }

            @Override
            public T getEmptyItem() {
                return AbstractInputDynamicComboBoxList.this.getEmptyItem();
            }

            @Override
            protected LinkedHashMap<String, T> reloadDatabaseValues() {
                throw new UnsupportedOperationException("Not Implemented for now");
            }

        };
        dynamicCombo.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
        dynamicCombo.setSelectAllAndRemoveAllButtonVisible(false);
        dynamicCombo.getComboBox().setPreferredSize(new Dimension(1, Sizes.INPUT_FIELD_HEIGHT));
        reload();
        setContent(dynamicCombo);
    }

}
