/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputMultiTextField;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;

/**
 *
 * @author ciaran.harrington@extern.lrz-muenchen.de
 */
public class InputOpenUrlMulti extends InputMultiTextField {

    /**
     * The button to open the link.
     */
    private JButton buttonOpen;

    public InputOpenUrlMulti(ColumnType columnType, String table) {
        super(columnType, table);
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = super.getMyFocusableComponents();
        comp.add(buttonOpen);
        return comp;
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();

        buttonOpen = new JButton(Loc.get("OPEN"));
        buttonOpen.setBorder(BorderFactory.createEmptyBorder(0, 12, 0, 12));
        buttonOpen.setBackground(Colors.INPUT_FIELD_BACKGROUND);
        buttonOpen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Desktop desktop = Desktop.getDesktop();
                    if (!multiTextField.isEmpty()) {
                        desktop.browse(new URI(multiTextField.getSelectedItemInList()));
                    }
                } catch (URISyntaxException | IOException ex) {
                    Footer.displayError(Loc.get("INVALID_URL_CANNOT_OPEN"));
                    Logger.getLogger(InputOpenURLButton.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        multiPanel.add(BorderLayout.EAST, ComponentHelper.wrapComponent(buttonOpen, Colors.INPUT_FIELD_BACKGROUND, 0, 0, 0, 6));
    }
}
