package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractActiveableInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

public class InputActivableField extends AbstractActiveableInputElement {


    private AbstractInputElementFunctions field;

    public InputActivableField(AbstractInputElementFunctions field, ColumnType columnTypeBoolean, ColumnType columnTypeField) {
        super(columnTypeBoolean, columnTypeField);
        this.field = field;
    }

    @Override
    public AbstractInputElementFunctions createActivableInputField() {

        return field;
    }
}
