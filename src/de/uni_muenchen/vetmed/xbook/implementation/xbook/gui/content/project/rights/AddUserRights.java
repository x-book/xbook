package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rights;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.datatype.User;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class AddUserRights extends AbstractProjectRightsAdd<User> {

    private ArrayList<User> allUsers;

    public AddUserRights() {
        super(RightMode.USER_TO_PROJECT);
    }

    @Override
    protected ArrayList<User> getComboData() {
        try {
            ArrayList<User> users = mainFrame.getController().getVisibleUsers();
            ArrayList<User> newUsers = new ArrayList<>();
            for (User u : users) {
                if (u.getId() != mainFrame.getController().getUserID()) {
                    newUsers.add(u);
                }
            }
            return newUsers;
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(AbstractProjectRightsAdd.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    @Override
    protected ArrayList<ColumnType> getAvailableRights() {
        try {
            return mainFrame.getController().getAvailableRights();
        } catch (NotLoggedInException ex) {
            Logger.getLogger(AddUserRights.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }

    @Override
    protected void actionAfterSaving() {
        mainFrame.displayProjectUserRightsScreen();
    }

    @Override
    protected void save() {
        User userToSave = null;
        if (!textField.getText().isEmpty()) {
            User textInputUser;
            try {
                textInputUser = mainFrame.getController().getUserInformation(textField.getText());
                if (textInputUser == null) {
                    Footer.displayError(Loc.get("NOT_A_VALID_USER"));
                    textField.setText("");
                } else {
                    userToSave = textInputUser;
                }
            } catch (NotLoggedInException | StatementNotExecutedException | EntriesException ex) {
            }
        } else if (!((String) combo.getSelectedItem()).equals("")) {
            userToSave = allUsers.get(combo.getSelectedIndex() - 1);
        }

        if (userToSave == null) {
            Footer.displayError(Loc.get("NOT_A_VALID_USER"));
        } else {
            ArrayList<Rights> rights = new ArrayList<>();
            for (JCheckBox check : allCheckBoxes) {
                rights.add(new Rights(new ColumnType(check.getName(),ColumnType.Type.VALUE,ColumnType.ExportType.NONE), check.isSelected()));
            }
            ArrayList<RightsInformation> ri = new ArrayList<>();
            RightsInformation newRights = new RightsInformation(userToSave.getUserName(), userToSave.getId(), rights);
            ri.add(newRights);
            if (newRights.hasAnyRight() || newRights.getRights().isEmpty()) {
                UserRightInformation uri = new UserRightInformation(userToSave.getUserName(), userToSave.getId(), ri);
                try {
                    Message ms = ((AbstractSynchronisationController)mainFrame.getController()).saveNewUserRights(uri.getRightsInformation());
                    if (ms.getResult().wasSuccessful()) {
                        Footer.displayConfirmation(Loc.get("USER_RIGHTS_SUCCESSFULLY_SAVED", userToSave.getUserName()));
                    } else {
                        Footer.displayError(Loc.get("ERROR_WHILE_SAVING_THE_USER_RIGHTS", userToSave.getUserName())+"<br>"+ms.getResult().getErrorMessage());
                        Logger.getLogger(AddUserRights.class.getName()).log(Level.SEVERE,ms.getResult().getErrorMessage());
                    }
                } catch (NotConnectedException | NotLoggedInException | IOException | NotLoadedException | StatementNotExecutedException | NoRightException ex) {
                    Footer.displayError(Loc.get("ERROR_WHILE_SAVING_THE_USER_RIGHTS", userToSave.getUserName()));
                }
                actionAfterSaving();
            } else {
                Footer.displayWarning(Loc.get("NO_RIGHTS_ASSIGNED"));
            }
        }
    }

    @Override
    protected void fillComboData() {
        allUsers = getComboData();
        for (User u : allUsers) {
            combo.addItem(u.getDisplayName() + " (" + u.getUserName() + ")");
        }
    }
}
