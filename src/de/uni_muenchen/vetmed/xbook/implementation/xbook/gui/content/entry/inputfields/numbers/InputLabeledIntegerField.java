package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers;

import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.ILabeledIntegerField;

/**
 * An integer field with a label as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputLabeledIntegerField extends InputIntegerField implements ILabeledIntegerField {

	private final String labelText;

	/**
	 * Constructor.
	 *
	 * @param columnType The database column data of the correspondenting input field.
	 * @param labelText The text of the label.
	 */
	public InputLabeledIntegerField(ColumnType columnType, String labelText) {
		super(columnType);
		this.labelText = labelText;
	}

	/**
	 * Constructor.
	 *
	 * @param columnType The database column data of the correspondenting input field.
	 * @param gridX The number of grids that are used for the height of the input field.
	 * @param gridY The number of grids that are used for the weight of the input field.
	 * @param labelText The text of the label.
	 * @param sidebar
	 */
	@Deprecated
	public InputLabeledIntegerField(ColumnType columnType, int gridX, int gridY, String labelText, SidebarPanel sidebar) {
		super(columnType);
		setGridX(gridX);
		setGridY(gridY);
		setSidebar(sidebar);
		this.labelText = labelText;
	}

	@Deprecated
	public InputLabeledIntegerField(ColumnType columnType, int gridX, int gridY, String labelText, String sidebarText) {
		this(columnType, gridX, gridY, labelText, new SidebarEntryField(columnType, sidebarText));
	}

	@Deprecated
	public InputLabeledIntegerField(ColumnType columnType, int gridX, int gridY, String labelText) {
		this(columnType, gridX, gridY, labelText, (SidebarPanel) null);
	}

	@Override
	protected void createFieldInput() {
		super.createFieldInput();

		JLabel label = new JLabel(labelText);
		label.setBorder(BorderFactory.createEmptyBorder(0, 6, 0, 6));
		label.setBackground(Colors.INPUT_FIELD_BACKGROUND);
		multiPanel.add(BorderLayout.EAST, label);
	}
}
