package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.AbstractWizardPageContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.WizardPage;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import java.awt.BorderLayout;
import javax.swing.JComponent;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;

public abstract class AbstractLookupTableBuilderWizardPageContent extends AbstractWizardPageContent {

    private JPanel lookupTableBuilderPanel = null;

    @Override
    public void setPage(WizardPage page) {
        super.setPage(page); //To change body of generated methods, choose Tools | Templates.
        init();
    }

    private JPanel createContent() {
        lookupTableBuilderPanel = new JPanel();

        lookupTableBuilderPanel.setLayout(new BorderLayout());
        lookupTableBuilderPanel.setBackground(Colors.CONTENT_BACKGROUND);

        reset();

        return lookupTableBuilderPanel;
    }

    @Override
    public JPanel getContent() {
        if (lookupTableBuilderPanel == null) {
            lookupTableBuilderPanel = createContent();
        }
        return lookupTableBuilderPanel;
    }

    @Override
    public void reset() {
        if (lookupTableBuilderPanel == null) {
            createContent();
        }

        JPanel topWrapper = new JPanel(new StackLayout());
        topWrapper.setBackground(Colors.CONTENT_BACKGROUND);

        lookupTableBuilderPanel.removeAll();
        XTitle xTitle = new XTitle(getTitle());
        topWrapper.add(xTitle);

        JComponent missingMandatoryField = getMissingMandatoryFieldDisplay();
        if (missingMandatoryField != null) {
            topWrapper.add(missingMandatoryField);
        }

        lookupTableBuilderPanel.add(BorderLayout.NORTH, topWrapper);

        JTable table = makeTable(); // TODO check makeTable()==null
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setBackground(Colors.CONTENT_BACKGROUND);
        lookupTableBuilderPanel.add(BorderLayout.CENTER, scrollPane);
        revalidate();
        repaint();
    }

    abstract protected JTable makeTable();

    protected JComponent getMissingMandatoryFieldDisplay() {
        return null;
    }
}
