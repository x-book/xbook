package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawMultiFileChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.IMultiFileChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiFileChooserElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractActiveableInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputMultiFileChooserActivateable extends AbstractActiveableInputElement implements IMultiFileChooser {

    /**
     * The text field object.
     */
    protected RawMultiFileChooser zField;

    private final ColumnType columnTypeFileName;
    private final ColumnType columnTypeHash;
    private FileNameExtensionFilter fileFilter;
    private final String booleanTableName;

    public InputMultiFileChooserActivateable(ColumnType columnTypeBoolean, ColumnType columnTypeFile, ColumnType columnTypeFileName, ColumnType columnTypeHash, String booleanTableName, FileNameExtensionFilter fileFilter) {
        super(columnTypeBoolean, columnTypeFile);

        this.columnTypeFileName = columnTypeFileName;
        this.columnTypeHash = columnTypeHash;
        this.fileFilter = fileFilter;
        this.booleanTableName = booleanTableName;

        setGridX(2);
        setGridY(3);
    }

    @Override
    public AbstractInputElementFunctions createActivableInputField() {
        if (zField == null) {
            zField = new RawMultiFileChooser(columnType, columnTypeFileName, columnTypeHash, fileFilter);
        }
        return zField;
    }

    @Override
    public void setMode(Mode mode) {
        super.setMode(mode);
        if (mode == Mode.STACK) {
            setPreferredSize(new Dimension(Sizes.STACK_DEFAULT_WIDTH, 150));
        }
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return zField.getMyFocusableComponents();
    }

    @Override
    public boolean addElement(MultiFileChooserElement element) {
        return zField.addElement(element);
    }

    @Override
    public boolean addElements(List<MultiFileChooserElement> element) {
        return zField.addElements(element);
    }

    @Override
    public List<MultiFileChooserElement> getElementsFromList() {
        return zField.getElementsFromList();
    }

    @Override
    public MultiFileChooserElement getElementFromInput() {
        return zField.getElementFromInput();
    }

    @Override
    public void removeElement(MultiFileChooserElement element) {
        zField.removeElement(element);
    }

    @Override
    public void removeElements(List<MultiFileChooserElement> elements) {
        zField.removeElements(elements);
    }

    @Override
    public void removeElementAt(int index) {
        zField.removeElementAt(index);
    }

    @Override
    protected String getBooleanTableName() {
        return booleanTableName;
    }

    public void setFileFilter(FileNameExtensionFilter fileFilter) {
        this.fileFilter = fileFilter;
    }

    public void setMaxFileSize(long maxFileSize) {
        zField.setMaxFileSize(maxFileSize);
    }
}
