package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.event.*;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel.Position;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarProjectOverview;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The project content panel.
 * <p/>
 * Holds an overview of all available projects in the local database of the
 * current user. Has serveral methods to load or edit an existing project and to
 * create a new project. Projects should also be available to be removed from
 * the local database and to be deleted from the global databse.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractProjectOverview extends AbstractContent implements ProjectListener, ValueEventListener, ConnectionListener, CodeTableListener {

    private static final Log log = LogFactory.getLog(AbstractProjectOverview.class);
    /**
     * The default height of one project line.
     */
    private final int LINE_HEIGHT = 30;
    /**
     * The outer wrappers of the single project lines. Used for scaling with a
     * border layout.
     */
    protected ArrayList<JPanel> wrapperLineOuter;
    /**
     * The inner wrappers of the single project lines where to add the single
     * project information.
     */
    protected ArrayList<JPanel> wrapperLineInner;
    /**
     * The inner wrappers of the single project lines where to add the single
     * project information.
     */
    protected ArrayList<JPopupMenu> popups;
    /**
     * The project meta files.
     */
    protected ArrayList<ProjectDataSet> projects;
    /**
     * The current selected project line. -1 if no project is selected.
     */
    protected int currentSelected = -1;
    private JPanel panel;
    private ArrayList<JPanel> allLines = new ArrayList<>();

    protected boolean isHideableAllowed;
    private JCheckBox showHiddenProjects;

    protected XImageButton deleteButton;
    protected XImageButton editButton;

    private boolean enablePopup = true;

    private final Color HIDDEN_COLOR = Color.GRAY;

    protected boolean projectsUpdated = true;

    /**
     * Constructor.
     */
    public AbstractProjectOverview() {
        this(false);
    }

    public AbstractProjectOverview(boolean isHideable) {
        super();
        this.isHideableAllowed = isHideable;
        wrapperLineOuter = new ArrayList<>();
        wrapperLineInner = new ArrayList<>();
        popups = new ArrayList<>();
        projects = new ArrayList<>();
        init();
        updateHiddenProjectsLabel();

        mainFrame.getController().addProjectListener(this);
        EventRegistry.addValueListener(this);
    }

    @Override
    protected JPanel getContent() {
        panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);
        return panel;
    }

    public synchronized void reloadProjectList() {
        Footer.startWorking();

        JScrollBar verticalScrollBar = scroll.getVerticalScrollBar();
        final int scrollValue = scroll.getVerticalScrollBar().getValue();
        final Rectangle visibleRect = scroll.getVisibleRect();
        // clear
        panel.removeAll();
        popups.clear();
        wrapperLineOuter.clear();
        wrapperLineInner.clear();
        currentSelected = -1;

        // reload everything
        panel.add(getTitleBar());
        try {
            if (isHideableAllowed && showHiddenProjects != null) {
                projects = mainFrame.getController().getProjects(showHiddenProjects.isSelected());
            } else {
                projects = mainFrame.getController().getProjects();
            }
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(AbstractProjectOverview.class.getName()).log(Level.SEVERE, null, ex);
        }

        // create a project line for each available project.
        if (projects.isEmpty()) {
            JPanel noProjectsLabelWrapper = new JPanel(new BorderLayout());
            noProjectsLabelWrapper.setBackground(Colors.CONTENT_BACKGROUND);
            MultiLineTextLabel noProjectsLabel = new MultiLineTextLabel(Loc.get("PROJECT_OVERVIEW_NO_PROJECTS"));
            noProjectsLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 13));
            noProjectsLabel.setBorder(BorderFactory.createEmptyBorder(10, 45, 0, 45));
            noProjectsLabel.setBackground(Colors.CONTENT_BACKGROUND);
            noProjectsLabelWrapper.add(BorderLayout.NORTH, noProjectsLabel);
            panel.add(noProjectsLabelWrapper);
        } else {
            final HashMap<Key, ArrayList<CustomRow>> customRowsAfterProjectName = new HashMap<>();
            final HashMap<Key, ArrayList<CustomRow>> customRowsAtTheEnd = new HashMap<>();
            for (ProjectDataSet project : projects) {
                customRowsAfterProjectName.put(project.getProjectKey(), new ArrayList<CustomRow>());
                customRowsAtTheEnd.put(project.getProjectKey(), new ArrayList<CustomRow>());
            }

            addCustomRowsAfterProjectName(customRowsAfterProjectName);
            addCustomRows(customRowsAtTheEnd);
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {

                    for (int i = 0; i < projects.size(); i++) {
                        panel.add(getProjectLine(i, customRowsAfterProjectName, customRowsAtTheEnd));
                    }
                    panel.revalidate();
                    panel.repaint();
                    content.updateUI();
                    JScrollBar verticalScrollBar = scroll.getVerticalScrollBar();
                    //why you not work
                    scroll.scrollRectToVisible(visibleRect);
                }
            });
        }


        Footer.stopWorking();
    }

    protected abstract void addCustomRows(HashMap<Key, ArrayList<CustomRow>> projectRows);

    protected abstract void addCustomRowsAfterProjectName(HashMap<Key, ArrayList<CustomRow>> projectRows);

    /**
     * Creates the title bar above the single project lines.
     * <p/>
     * Display a short label as description for the elements below.
     *
     * @return The final project line as a JPanel.
     */
    public JPanel getTitleBar() {
        final JPanel elementWrapper = new JPanel(new BorderLayout());
        elementWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        JPanel element = new JPanel();
        element.setBackground(Colors.CONTENT_BACKGROUND);

        // an empty label (spacer) above the project icon
        JLabel icon = new JLabel("");
        icon.setPreferredSize(new Dimension(35, LINE_HEIGHT));
        icon.setBorder(new EmptyBorder(0, 0, 0, 0));
        element.add(icon);

        // the project name label
        JLabel title = new JLabel("<html><b><i>" + getProjectNameLabel() + "</i></b><br><i>(" + getProjectOwnerLabel() + ")</i></html>");
        title.setPreferredSize(new Dimension(250, LINE_HEIGHT));
        title.setBorder(new EmptyBorder(0, 0, 0, 0));
        element.add(title);

        ArrayList<CustomRow> rows1 = getCustomRowsAfterProjectName(null, -1);
        if (rows1 != null) {
            for (CustomRow row : rows1) {
                JLabel label = new JLabel("<html><center><b><i>" + row.label + "</i></b></center></html>", SwingConstants.CENTER);
                label.setPreferredSize(new Dimension(row.width, LINE_HEIGHT));
                label.setBorder(new EmptyBorder(0, 0, 0, 0));
                label.setBackground(Color.CYAN);
                element.add(label);
            }
        }
        if (mainFrame.getController() instanceof AbstractSynchronisationController) {
            // the project right label
            JLabel rights = new JLabel("<html><center><b><i>" + Loc.get("RIGHTS") + "</i></b></center></html>", SwingConstants.CENTER);
            rights.setPreferredSize(new Dimension(76, LINE_HEIGHT));
            rights.setBorder(new EmptyBorder(0, 0, 0, 0));
            element.add(rights);
        }
        ArrayList<CustomRow> rows2 = getCustomRows();
        if (rows2 != null) {
            for (CustomRow row : rows2) {
                JLabel label = new JLabel("<html><center><b><i>" + row.label + "</i></b></center></html>", SwingConstants.CENTER);
                label.setPreferredSize(new Dimension(row.width, LINE_HEIGHT));
                label.setBorder(new EmptyBorder(0, 0, 0, 0));
                label.setBackground(Color.CYAN);
                element.add(label);
            }
        }

        elementWrapper.add(BorderLayout.WEST, element);
        return elementWrapper;
    }

    /**
     * Creates a project line that displays all necessary information.
     *
     * @param index The index of the project from the arraylists of the class
     *              variables.
     * @return The final project line as a JPanel.
     */
    private ElementWrapper getProjectLine(final int index, HashMap<Key, ArrayList<CustomRow>> customAfterProjectName, HashMap<Key, ArrayList<CustomRow>> customRowsAtTheEnd) {
        final ProjectDataSet thisProject = projects.get(index);

        // add the innner and outer wrapper to the arraylist
//        final JPanel elementWrapper = new JPanel(new BorderLayout());
        ElementWrapper elementWrapper = new ElementWrapper(index);
        wrapperLineOuter.add(elementWrapper);

        final JPanel element = new JPanel(new FlowLayout());
        wrapperLineInner.add(element);

        // colorize the background
        if (index % 2 == 1) {
            element.setBackground(Colors.TABLE_LINE_DARK);
            elementWrapper.setBackground(Colors.TABLE_LINE_DARK);
        } else {
            element.setBackground(Colors.TABLE_LINE_LIGHT);
            elementWrapper.setBackground(Colors.TABLE_LINE_LIGHT);
        }

        // build the content
        // indent if hidden
        if (thisProject.isHidden()) {
            JLabel indent = new JLabel();
            indent.setPreferredSize(new Dimension(15, LINE_HEIGHT));
            indent.setBorder(new EmptyBorder(0, 0, 0, 0));
            indent.setBackground(Color.LIGHT_GRAY);
            indent.setOpaque(true);
            element.add(indent);
        }

        // a project ican
        JLabel icon = new JLabel(Images.PROJECT_FOLDER);
        icon.setPreferredSize(new Dimension(35, LINE_HEIGHT));
        icon.setBorder(new EmptyBorder(0, 0, 0, 0));
        element.add(icon);

        // the conflicted icon
        if (XBookConfiguration.isConflicted(thisProject.getProjectKey())) {
            JLabel conflictedIcon = new JLabel(Images.PROJECT_CONFLICTED);
            conflictedIcon.setPreferredSize(new Dimension(35, LINE_HEIGHT));
            conflictedIcon.setBorder(new EmptyBorder(0, 0, 0, 0));
            element.add(conflictedIcon);
        }

        // the project name and project owner
        JPanel panelNameOwner = new JPanel(new BorderLayout());
        panelNameOwner.setOpaque(false);
        JLabel projectName = new JLabel(getTitleProjectName(thisProject));
        int projectNameWidth = 250;
        if (XBookConfiguration.isConflicted(thisProject.getProjectKey())) {
            projectNameWidth = projectNameWidth - 35 - 5;
        }
        if (thisProject.isHidden()) {
            projectNameWidth = projectNameWidth - 15 - 5;
        }
        projectName.setPreferredSize(new Dimension(projectNameWidth, LINE_HEIGHT));
        projectName.setBorder(new EmptyBorder(0, 0, 0, 0));
        projectName.setFont(Fonts.FONT_BIG_BOLD);
        projectName.setOpaque(false);
        panelNameOwner.add(BorderLayout.NORTH, projectName);

        JLabel owner = new JLabel();
        try {
            owner.setText(getTitleProjectOwnerName(thisProject));
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(AbstractProjectOverview.class.getName()).log(Level.SEVERE, null, ex);
        }

        panelNameOwner.add(BorderLayout.SOUTH, owner);
        element.add(panelNameOwner);

        ArrayList<CustomRow> rows1 = customAfterProjectName.get(thisProject.getProjectKey());
        if (rows1 != null) {
            for (CustomRow row : rows1) {
                element.add(row.columnRowPanel);
            }
        }

        if (mainFrame.getController() instanceof AbstractSynchronisationController) {
            // the icon for the right "read"
            JLabel iconReadRight = new JLabel();
            try {
                if (mainFrame.getController().hasReadRights(thisProject)) {
                    iconReadRight.setIcon(Images.PROJECT_READ_RIGHTS);
                }
            } catch (NotLoggedInException | StatementNotExecutedException | NotLoadedException ex) {
                Logger.getLogger(AbstractProjectOverview.class.getName()).log(Level.SEVERE, null, ex);
            }
            iconReadRight.setPreferredSize(new Dimension(22, LINE_HEIGHT));
            iconReadRight.setBorder(new EmptyBorder(0, 0, 0, 0));
            element.add(iconReadRight);

            // the icon for the right "write"
            JLabel iconWriteRight = new JLabel();
            try {
                if (mainFrame.getController().hasWriteRights(thisProject)) {
                    iconWriteRight.setIcon(Images.PROJECT_WRITE_RIGHTS);
                }
            } catch (NotLoggedInException | StatementNotExecutedException ex) {
                Logger.getLogger(AbstractProjectOverview.class.getName()).log(Level.SEVERE, null, ex);
            }
            iconWriteRight.setPreferredSize(new Dimension(22, LINE_HEIGHT));
            iconWriteRight.setBorder(new EmptyBorder(0, 0, 0, 0));
            element.add(iconWriteRight);

            // the icon for the right "project owner"
            JLabel iconProjectOwner = new JLabel();
            try {
                if (mainFrame.getController().hasEditRights(thisProject)) {
                    iconProjectOwner.setIcon(Images.PROJECT_OWNER);
                }
            } catch (NotLoggedInException | StatementNotExecutedException ex) {
                Logger.getLogger(AbstractProjectOverview.class.getName()).log(Level.SEVERE, null, ex);
            }
            iconProjectOwner.setPreferredSize(new Dimension(22, LINE_HEIGHT));
            iconProjectOwner.setBorder(new EmptyBorder(0, 0, 0, 0));
            element.add(iconProjectOwner);
        }

        ArrayList<CustomRow> rows2 = customRowsAtTheEnd.get(thisProject.getProjectKey());
        if (rows2 != null) {
            for (CustomRow row : rows2) {
                element.add(row.columnRowPanel);
            }
        }

        elementWrapper.add(BorderLayout.WEST, element);

        // colorize all elements if necessary
        if (thisProject.isHidden()) {
            projectName.setForeground(HIDDEN_COLOR);
            owner.setForeground(HIDDEN_COLOR);
            for (CustomRow row : rows2) {
                row.setForegroundColor(HIDDEN_COLOR);
            }
        }

        //TODO don't do this for each line!
        // initialise the mouse popup 

        if (enablePopup) {
            final JPopupMenu popup = new JPopupMenu();
            popups.add(popup);
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {

                    JMenuItem item;

                    popup.add(item = new JMenuItem(Loc.get("LOAD") + ": " + thisProject.getProjectName()));
                    item.setIcon(Images.NAVIGATION_ICON_SMALL_OPEN);
                    item.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            openProject();
                        }
                    });
                    popup.addSeparator();

                    popup.add(item = new JMenuItem(Loc.get("EDIT")));
                    item.setIcon(Images.NAVIGATION_ICON_SMALL_EDIT);
                    item.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            editProject();
                        }
                    });

                    if (isHideableAllowed) {
                        popup.add(item = new JMenuItem((thisProject.isHidden() ? Loc.get("UNHIDE") : Loc.get("HIDE")) + ": " + thisProject.getProjectName()));
                        item.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                hideProject();
                            }
                        });
                    }

                    popup.addSeparator();

                    popup.add(item = new JMenuItem(Loc.get("PROJECT_RIGHTS") + ": " + Loc.get("USER")));
                    item.setIcon(Images.NAVIGATION_ICON_SMALL_PERSON);
                    item.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            new Thread(new Runnable() {

                                @Override
                                public void run() {
                                    doOpenProject();
                                    mainFrame.displayProjectUserRightsScreen();
                                }
                            }).start();
                        }
                    });

                    popup.add(item = new JMenuItem(Loc.get("PROJECT_RIGHTS") + ": " + Loc.get("GROUP")));
                    item.setIcon(Images.NAVIGATION_ICON_SMALL_PERSON);
                    item.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            new Thread(new Runnable() {

                                @Override
                                public void run() {
                                    doOpenProject();
                                    mainFrame.displayProjectGroupRightsScreen();
                                }
                            }).start();
                        }
                    });

                    popup.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

                }
            });
        }


        // now set the mouse listener
        elementWrapper.addMouseListener(getMouseListener(index));

        return elementWrapper;
    }

    protected MouseAdapter getMouseListener(final int index) {
        return new MouseAdapter() {
            private boolean clickedState = false;

            @Override
            public void mouseClicked(MouseEvent e) {
                log.debug(index + " clicked");
                // on double click open the project
                if (e.getClickCount() == 2) {
                    openProject();
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (clickedState) {
                    // on single left click select the project
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        setSelectedProject(index);
                        clickedState = false;
                    }
                    // on single right click open the popup menu
                    if (e.getButton() == MouseEvent.BUTTON3) {
                        setSelectedProject(index);
                        popups.get(index).show(e.getComponent(), e.getX(), e.getY());
                    }
                    // colorize all rows with the default color
                    int row = 1;
                    for (int i = 0; i < wrapperLineInner.size(); i++) {
                        if (row % 2 == 1) {
                            wrapperLineInner.get(i).setBackground(Colors.TABLE_LINE_DARK);
                            wrapperLineOuter.get(i).setBackground(Colors.TABLE_LINE_DARK);
                        } else {
                            wrapperLineInner.get(i).setBackground(Colors.TABLE_LINE_LIGHT);
                            wrapperLineOuter.get(i).setBackground(Colors.TABLE_LINE_LIGHT);
                        }
                        row++;
                    }
                }
                wrapperLineInner.get(index).setBackground(Colors.TABLE_LINE_SELECTED_COLOR_LIGHT);
                wrapperLineOuter.get(index).setBackground(Colors.TABLE_LINE_SELECTED_COLOR_LIGHT);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                clickedState = true;
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if (currentSelected != index) {
                    wrapperLineInner.get(index).setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                    wrapperLineOuter.get(index).setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                clickedState = false;

                if (currentSelected == index) {
                    wrapperLineInner.get(index).setBackground(Colors.TABLE_LINE_SELECTED_COLOR_LIGHT);
                    wrapperLineOuter.get(index).setBackground(Colors.TABLE_LINE_SELECTED_COLOR_LIGHT);
                    return;
                }
                if (index % 2 == 1 && index != currentSelected) {
                    wrapperLineInner.get(index).setBackground(Colors.TABLE_LINE_DARK);
                    wrapperLineOuter.get(index).setBackground(Colors.TABLE_LINE_DARK);
                } else {
                    wrapperLineInner.get(index).setBackground(Colors.TABLE_LINE_LIGHT);
                    wrapperLineOuter.get(index).setBackground(Colors.TABLE_LINE_LIGHT);
                }
            }
        };
    }

    /**
     * Set a specific project as selected project.
     * <p/>
     * Set the current project into the class variable and colorize the active
     * project.
     *
     * @param index The index of the specific project in the ArrayLists of the
     *              class variables.
     */
    private void setSelectedProject(int index) {
        if (currentSelected != -1) {
            if (index % 2 == 1) {
                wrapperLineInner.get(currentSelected).setBackground(Colors.TABLE_LINE_DARK);
                wrapperLineOuter.get(currentSelected).setBackground(Colors.TABLE_LINE_DARK);
            } else {
                wrapperLineInner.get(currentSelected).setBackground(Colors.TABLE_LINE_LIGHT);
                wrapperLineOuter.get(currentSelected).setBackground(Colors.TABLE_LINE_LIGHT);
            }
        }
        currentSelected = index;
        if (index != -1) {
            wrapperLineInner.get(currentSelected).setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
            wrapperLineOuter.get(currentSelected).setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
        }
    }

    /**
     * Open a project.
     */
    private void openProject() {
        if (currentSelected == -1) {
            Footer.displayWarning(Loc.get("NO_PROJECT_SELECTED"));
        } else {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    doOpenProject();
                }
            }).start();
        }
    }

    private void doOpenProject() {
        try {
            Footer.startWorking();
            mainFrame.getController().loadProject(projects.get(currentSelected), true);
        } catch (StatementNotExecutedException | NotLoggedInException | NoRightException ex) {
            Footer.displayError(Loc.get("ERROR"));
            Logger.getLogger(AbstractProjectOverview.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Throwable what) {
            what.printStackTrace();
        }
        Footer.stopWorking();
    }

    /**
     * Open the panel to edit the current selected project.
     */
    private void editProject() {
        if (currentSelected == -1) {
            Footer.displayWarning(Loc.get("NO_PROJECT_SELECTED"));
        } else {
            mainFrame.displayProjectEditScreen(projects.get(currentSelected));
        }
    }

    /**
     * Open the panel to edit the current selected project.
     */
    private void hideProject() {
        if (currentSelected == -1) {
            Footer.displayWarning(Loc.get("NO_PROJECT_SELECTED"));
        } else {
            ProjectDataSet projectDataSet = projects.get(currentSelected);
            try {
                mainFrame.getController().hideProject(projectDataSet.getProjectKey(), !projectDataSet.isHidden());
                updateHiddenProjectsLabel();
                reloadProjectList();
            } catch (NotLoggedInException | StatementNotExecutedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Open the panel to edit the current selected project.
     */
    private void deleteProjectLocal() {
        if (currentSelected == -1) {
            Footer.displayWarning(Loc.get("NO_PROJECT_SELECTED"));
        } else {
            int n = JOptionPane.showConfirmDialog(this, "<html>" + Loc.get("SURE_TO_DELETE_PROJECT_LOCAL", projects.get(currentSelected).getProjectName()) + "</html>", Loc.get("WARNING"), JOptionPane.YES_NO_OPTION);
            if (n == JOptionPane.YES_OPTION) {
                try {
                    ((AbstractController) mainFrame.getController()).deleteProjectLocaly(projects.get(currentSelected));
                    mainFrame.displayProjectOverviewScreen();
                } catch (NotLoggedInException | StatementNotExecutedException | NoRightException ex) {
                    Logger.getLogger(AbstractProjectOverview.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel bp = new ButtonPanel();

        bp.addImageButton(Position.NORTH_WEST, Images.NAVIGATION_ICON_LISTING, getProjectListingLabel(), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.displayProjectListingScreen();
            }
        });

        JLabel separator = new JLabel();
        separator.setOpaque(false);
        separator.setSize(new Dimension(50, 1));
        bp.addTo(Position.NORTH_WEST, separator);

        bp.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_ADD, getCreateNewProjectLabel(), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.displayNewProjectScreen();
            }
        });

        editButton = bp.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_EDIT, Loc.get("EDIT"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editProject();
            }
        });

        deleteButton = bp.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_DELETE, Loc.get("LOCAL_DELETE"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteProjectLocal();
            }
        });

        bp.addImageButton(Position.NORTH_EAST, Images.BUTTONPANEL_LOAD, Loc.get("LOAD"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openProject();
            }
        });

        if (isHideableAllowed) {

            // button hide/unhide
            bp.addImageButton(Position.NORTH_CENTER, Images.BUTTONPANEL_HIDEUNHIDE, Loc.get("HIDEUNHIDE"), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    hideProject();
                }
            });

            // checkbox "show hidden projects"
            showHiddenProjects = new JCheckBox(Loc.get("SHOW_HIDDEN_PROJECTS"), false);
            showHiddenProjects.setBackground(Colors.CONTENT_BACKGROUND);
            showHiddenProjects.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    reloadProjectList();
                }
            });
            bp.getButtonsCenterNorth().add(showHiddenProjects);
        }

        return bp;
    }

    @Override
    public SidebarPanel getSideBar() {
        return new SidebarProjectOverview();
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    @Override
    public void updateContent() {
        super.updateContent();
        if (projectsUpdated) {
            projectsUpdated = false;
            reloadProjectList();
        }
        //

    }

    /**
     * @param thisProject The current project.
     * @param index       The index of the row.
     * @return
     */
    public abstract ArrayList<CustomRow> getCustomRowsAfterProjectName(ProjectDataSet thisProject, int index);

    /**
     * @return
     */
    public abstract ArrayList<CustomRow> getCustomRows();

    /**
     * Returns a custom row that displays the amount of persons the project is
     * shared with.
     *
     * @param projectRows
     */
    public void addSharedWithCustomRow(HashMap<Key, ArrayList<CustomRow>> projectRows) {
        try {
            HashMap<Key, ArrayList<String>> usersWithProjectRights = mainFrame.getController().getUsersWithProjectRights(projectRows.keySet());
            HashMap<Key, ArrayList<String>> groupsWithProjectRights = mainFrame.getController().getGroupsWithProjectRights(projectRows.keySet());

            for (Map.Entry<Key, ArrayList<CustomRow>> entry : projectRows.entrySet()) {
                Key key = entry.getKey();
                ArrayList<CustomRow> value = entry.getValue();
                value.add(new SharedWithCustomRow(usersWithProjectRights.get(key), groupsWithProjectRights.get(key)));
            }
        } catch (NotLoggedInException | EntriesException | NoRightException | StatementNotExecutedException e) {
            e.printStackTrace();
        }
    }

    protected CustomRow addSharedWithCustomRow() {
        return new SharedWithCustomRow(null, null);
    }

    /**
     * Returns a custom row that displays the amount of entries in the project.
     *
     * @param projectRows
     * @return A custom row that displays the amount of entries in the project.
     */
    protected void addEntriesCustomRow(HashMap<Key, ArrayList<CustomRow>> projectRows) {
        try {
            HashMap<Key, Integer> entryCount = mainFrame.getController().getNumberOfEntries();
            HashMap<Key, Integer> unsynced = mainFrame.getController().getNumberOfUnsyncedEntries();
            HashMap<Key, Integer> conflictedEntries = mainFrame.getController().getNumberOfConflictedEntries();
            for (Map.Entry<Key, ArrayList<CustomRow>> entry : projectRows.entrySet()) {

                Key key = entry.getKey();
                ArrayList<CustomRow> customRows = entry.getValue();
//                if (customRows == null) {
//                    customRows = new ArrayList<>();
//                    projectRows.put(key, customRows);
//                }
                Integer entryCount1 = entryCount.get(key);

                Integer unsynched = unsynced.get(key);
                Integer conflicted = conflictedEntries.get(key);
                customRows.add(getCustomRow(entryCount1 == null ? 0 : entryCount1, unsynched == null ? 0 : unsynched, conflicted == null ? 0 : conflicted));
            }
        } catch (NotLoggedInException | StatementNotExecutedException e) {
            e.printStackTrace();
        }
    }

    protected CustomRow getEntriesCustomRow() {
        return new CustomRow(Loc.get("ENTRIES"), 160, new OneColumnRow(""));
    }

    protected CustomRow getCustomRow(int entryCount, int unsynched, int conflicted) throws StatementNotExecutedException {

        final ApiControllerAccess controller = mainFrame.getController();
        String firstLine = Loc.get("DATASETS") + ": " + entryCount;
        if (controller instanceof AbstractSynchronisationController) {
            String secondLine;

            if (unsynched > 0) {
                secondLine = "<b><font color=orange>" + unsynched + " " + Loc.get("UNSYNCHRONIZED") + "</font></b>";
            } else {
                secondLine = "<b><font color=green>" + Loc.get("LOCAL_SYNCED") + "</font></b>";
            }

            if (conflicted > 0) {
                String thirdLine = "<b><font color=orange>" + conflicted + " " + Loc.get("CONFLICTS") + "</font></b>";
                return new CustomRow(Loc.get("ENTRIES"), 160, new ThreeColumnRow(firstLine, secondLine, thirdLine));
            }

            return new CustomRow(Loc.get("ENTRIES"), 160, new TwoColumnRow(firstLine, secondLine));
        } else {
            return new CustomRow(Loc.get("ENTRIES"), 160, new OneColumnRow(firstLine));
        }

    }

    protected String getProjectNameLabel() {
        return Loc.get("PROJECT_NAME");
    }

    protected String getEditProjectNameLabel() {
        return Loc.get("EDIT_PROJECT");
    }

    protected String getCreateNewProjectLabel() {
        return Loc.get("CREATE_NEW_PROJECT");
    }

    protected String getProjectListingLabel() {
        return Loc.get("PROJECT") + " " + Loc.get("LISTING");
    }

    protected String getTitleProjectName(ProjectDataSet thisProject) {
        return thisProject.getProjectName();
    }

    protected String getTitleProjectOwnerName(ProjectDataSet thisProject) throws NotLoggedInException, StatementNotExecutedException {
        return mainFrame.getController().getDisplayName(thisProject.getProjectOwnerId());
    }

    protected String getProjectOwnerLabel() {
        return Loc.get("OWNER");
    }

    protected void setPopupEnabled(boolean status) {
        this.enablePopup = status;
    }

    private void updateHiddenProjectsLabel() {
        if (isHideableAllowed) {
            String text = Loc.get("SHOW_HIDDEN_PROJECTS");
            try {
                int count = mainFrame.getController().getNumberOfHiddenProjects();
                if (count > 0) {
                    text = "<html>" + Loc.get("SHOW_HIDDEN_PROJECTS") + "<br />"
                            + "<b>(" + Loc.get("X_HIDDEN_PROJECTS", count) + ")</b></html>";
                }
            } catch (NotLoggedInException | StatementNotExecutedException | EntriesException ex) {
                Logger.getLogger(AbstractProjectOverview.class.getName()).log(Level.SEVERE, null, ex);
            }
            showHiddenProjects.setText(text);
        }
    }

    @Override
    public void onValueUpdated() {
        projectsUpdated = true;
    }

    @Override
    public void onProjectAdded(ProjectEvent evt) {
        projectsUpdated = true;
    }

    @Override
    public void onProjectChanged(ProjectEvent evt) {
        projectsUpdated = true;
    }

    @Override
    public void onProjectDeleted(ProjectEvent evt) {
        projectsUpdated = true;
    }

    @Override
    public void onProjectLoaded(ProjectEvent evt) {

    }

    @Override
    public void onProjectUnloaded(ProjectEvent evt) {

    }

    @Override
    public void onConnected() {
        projectsUpdated = true;

    }

    @Override
    public void onDisconnected() {
        projectsUpdated = true;

    }

    @Override
    public void onCodeTablesUpdated() {
        projectsUpdated = true;
    }

    protected class CustomRow {

        protected String label;
        protected int width;
        protected ColumnRowInterface columnRowInterface;
        protected JPanel columnRowPanel;

        public CustomRow(String label, int width, ColumnRowInterface columnRow) {
            this.label = label;
            this.width = width;
            this.columnRowPanel = (JPanel) columnRow;
            this.columnRowInterface = columnRow;

            if (columnRowInterface != null) {
                columnRowInterface.setWidth(width);
            }

            setOpaque(false);
        }

        public String getLabel() {
            return label;
        }

        public int getWidth() {
            return width;
        }

        public void setForegroundColor(Color foreground) {
            columnRowInterface.setForegroundColor(foreground);
        }

    }

    protected class SharedWithCustomRow extends CustomRow {

        private ColumnRowInterface row;

        public SharedWithCustomRow(ArrayList<String> users, ArrayList<String> groups) {
            super(Loc.get("SHARED_WITH"), 100, null);

            // calculate the number of shared users
            if (users == null) {
                users = new ArrayList<>();
            }
            int countSharedUsers = users.size();

            // calculate the number of shared groups
            if (groups == null) {
                groups = new ArrayList<>();
            }
            int countSharedGroups = groups.size();

            if (countSharedUsers <= 1 && countSharedGroups == 0) {
                row = new OneColumnRow("—");
            } else if (countSharedUsers > 1 && countSharedGroups > 0) {
                row = new TwoColumnRow(countSharedUsers + " " + Loc.get("USERS"), countSharedGroups + " " + Loc.get("GROUPS"));
            } else if (countSharedUsers > 1) {
                row = new OneColumnRow(countSharedUsers + " " + Loc.get("USERS"));
            } else /* if (countSharedGroups > 0 )*/ {
                row = new OneColumnRow(countSharedUsers + " " + Loc.get("GROUPS"));
            }
            row.setWidth(98);

            columnRowPanel = (JPanel) row;
            columnRowInterface = row;

            StringBuilder popupText = new StringBuilder();
            popupText.append("<html>");
            if (!users.isEmpty()) {
                popupText.append("<b>").append(Loc.get("USERS")).append(": </b>");
                int akk = 0;
                for (String s : users) {
                    if (akk++ % 3 == 0) {
                        popupText.append("<br>");
                    }
                    popupText.append(s).append(", ");
                }
                popupText.delete(popupText.length() - 2, popupText.length());
                popupText.append("<br>");
            }
            if (!groups.isEmpty()) {
                popupText.append("<b>").append(Loc.get("GROUPS")).append(": </b>");
                int akk = 0;
                for (String s : groups) {
                    if (akk++ % 3 == 0) {
                        popupText.append("<br>");
                    }
                    popupText.append(s).append(", ");
                }
                popupText.delete(popupText.length() - 2, popupText.length());
            }
            popupText.append("</html>");
            columnRowPanel.setToolTipText(popupText.toString());
            columnRowPanel.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    Container parent = columnRowPanel.getParent().getParent();
                    parent.dispatchEvent(SwingUtilities.convertMouseEvent(e.getComponent(), e, parent));
                }

                @Override
                public void mousePressed(MouseEvent e) {
                    Container parent = columnRowPanel.getParent().getParent();
                    parent.dispatchEvent(SwingUtilities.convertMouseEvent(e.getComponent(), e, parent));
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    Container parent = columnRowPanel.getParent().getParent();
                    parent.dispatchEvent(SwingUtilities.convertMouseEvent(e.getComponent(), e, parent));
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    Container parent = columnRowPanel.getParent().getParent();
                    parent.dispatchEvent(SwingUtilities.convertMouseEvent(e.getComponent(), e, parent));
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    Container parent = columnRowPanel.getParent().getParent();
                    parent.dispatchEvent(SwingUtilities.convertMouseEvent(e.getComponent(), e, parent));
                }
            });
        }

        @Override
        public void setForegroundColor(Color foreground) {
            row.setForegroundColor(foreground);
        }

    }

    protected class ThreeColumnRow extends OneColumnRow {

        public ThreeColumnRow(String textRow1, String textRow2, String textRow3) {
            super("<html><center>" + textRow1 + "<br>" + textRow2 + "<br>" + textRow3 + "</center></html>");
        }

        @Override
        public void setWidth(int width) {
            testPanel.setPreferredSize(new Dimension(width, 48));
        }
    }

    protected class TwoColumnRow extends OneColumnRow {

        public TwoColumnRow(String textRow1, String textRow2) {
            super("<html><center>" + textRow1 + "<br>" + textRow2 + "</center></html>");
        }

        @Override
        public void setWidth(int width) {
            testPanel.setPreferredSize(new Dimension(width, 34));
        }
    }

    protected class OneColumnRow extends JPanel implements ColumnRowInterface {

        protected JPanel testPanel;
        protected JLabel row;

        public OneColumnRow(String textRow) {
            setLayout(new BorderLayout());
            setBorder(new EmptyBorder(0, 0, 0, 0));
            setOpaque(false);

            testPanel = new JPanel();
            testPanel.setOpaque(false);
            testPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
            add(BorderLayout.CENTER, testPanel);

            //row = new JLabel("<html><center>" + textRow + "</center></html>");
            row = new JLabel(textRow, SwingConstants.CENTER);
            row.setBorder(new EmptyBorder(0, 0, 0, 0));
            row.setOpaque(false);
            testPanel.add(BorderLayout.CENTER, row);
        }

        @Override
        public void setWidth(int width) {
            testPanel.setPreferredSize(new Dimension(width, 20));
        }

        @Override
        public void setForegroundColor(Color foreground) {
            row.setForeground(foreground);
        }

    }

    protected class IconColumn extends JPanel implements ColumnRowInterface {

        private JLabel row;
        private Icon icon;

        public IconColumn(Icon icon) {
            this.icon = icon;

            setLayout(new BorderLayout());
            setOpaque(false);

            row = new JLabel("", SwingConstants.CENTER);
            row.setIcon(icon);
            row.setSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
            row.setPreferredSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
            row.setOpaque(false);
            add(BorderLayout.NORTH, row);
        }

        @Override
        public void setWidth(int width) {
            row.setPreferredSize(new Dimension(width, icon.getIconHeight()));
        }

        @Override
        public void setForegroundColor(Color foreground) {
            row.setForeground(foreground);
        }
    }

    protected interface ColumnRowInterface {

        public abstract void setWidth(int width);

        public abstract void setForegroundColor(Color foreground);
    }

    protected int getInt(Integer integer) {
        if (integer == null) {
            return 0;
        }
        return integer;
    }

    private static class ElementWrapper extends JPanel {
        private int index;

        public ElementWrapper(int index) {
            super(new BorderLayout());
            this.index = index;
        }

        public int getIndex() {
            return index;
        }
    }
}
