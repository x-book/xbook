package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer;

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public abstract class LookupTableEditorTableModel<T> implements TableModel {

	private List<T> data = new ArrayList<>();

	public abstract Object[] getRow(T entry);

	public abstract String[] getColumnNames();

	public LookupTableEditorTableModel(List<T> data) {
		this.data = data;
	}

	@Override
	public int getColumnCount() {
		if (data.isEmpty())
			return 0;
		return getRow(data.get(0)).length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return getColumnNames()[columnIndex];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return getRow(data.get(rowIndex))[columnIndex];
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public void addTableModelListener(TableModelListener listener) {
		// do nothing
	}

	@Override
	public Class<?> getColumnClass(int arg0) {
		return Object.class;
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener listener) {
		// do nothing
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// do nothing
	}

}
