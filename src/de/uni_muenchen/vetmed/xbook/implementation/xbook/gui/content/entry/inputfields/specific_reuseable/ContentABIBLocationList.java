package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.specific_reuseable;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.GridBagHelper;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class ContentABIBLocationList extends AbstractContent {

    private final AbstractContent previousContent;
    private final InputABIBLocation locationInputField;
    private final ArrayList<JCheckBox> checkBoxes;

    public ContentABIBLocationList(AbstractContent previousContent, InputABIBLocation locationInputField) {
        this.previousContent = previousContent;
        this.locationInputField = locationInputField;
        checkBoxes = new ArrayList<>();
        init();
    }

    @Override
    protected JPanel getContent() {
        ArrayList<ABIBLocationLogEntry> data = locationInputField.getLogData();

        JPanel pp = new JPanel(new BorderLayout());
        pp.setBackground(Colors.CONTENT_BACKGROUND);

        JPanel panel = new JPanel(new GridBagLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.WEST;

        GridBagHelper.setConstraints(gbc, 0, 0, 6, 1);
        panel.add(ComponentHelper.wrapComponent(new JLabel("<html><b>" + Loc.get("CURRENT_LOCATION") + ":</b></html>"), Colors.CONTENT_BACKGROUND, 12, 0, 6, 0), gbc);

        ABIBLocationLogEntry singleData = data.get(data.size() - 1);
        GridBagHelper.setConstraints(gbc, 1, 0);
        panel.add(ComponentHelper.wrapComponent(new JLabel(singleData.getPlace()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
        GridBagHelper.setConstraints(gbc, 1, 1);
        panel.add(ComponentHelper.wrapComponent(new JLabel(singleData.getRoom()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
        GridBagHelper.setConstraints(gbc, 1, 2);
        panel.add(ComponentHelper.wrapComponent(new JLabel(singleData.getShelf()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
        GridBagHelper.setConstraints(gbc, 1, 4);
        panel.add(ComponentHelper.wrapComponent(new JLabel(singleData.getTray()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
        GridBagHelper.setConstraints(gbc, 1, 5);
        panel.add(ComponentHelper.wrapComponent(new JLabel(singleData.getTableau()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
        GridBagHelper.setConstraints(gbc, 1, 6);
        panel.add(ComponentHelper.wrapComponent(new JLabel(singleData.getUser()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
        GridBagHelper.setConstraints(gbc, 1, 7);
        panel.add(ComponentHelper.wrapComponent(new JLabel(singleData.getTime()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
        GridBagHelper.setConstraints(gbc, 1, 8);
        panel.add(getCheck(data.size() - 1), gbc);

        GridBagHelper.setConstraints(gbc, 2, 0, 4, 1);
        panel.add(ComponentHelper.wrapComponent(new JLabel("<html><b>" + Loc.get("RECENT_LOCATIONS") + ":</b></html>"), Colors.CONTENT_BACKGROUND, 12, 0, 6, 0), gbc);

        int row = 3;

        for (int i = data.size() - 2; i >= 0; i--) {
            ABIBLocationLogEntry dataSet = data.get(i);
            GridBagHelper.setConstraints(gbc, row, 0);
            panel.add(ComponentHelper.wrapComponent(new JLabel(dataSet.getPlace()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
            GridBagHelper.setConstraints(gbc, row, 1);
            panel.add(ComponentHelper.wrapComponent(new JLabel(dataSet.getRoom()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
            GridBagHelper.setConstraints(gbc, row, 2);
            panel.add(ComponentHelper.wrapComponent(new JLabel(dataSet.getShelf()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
            GridBagHelper.setConstraints(gbc, row, 4);
            panel.add(ComponentHelper.wrapComponent(new JLabel(dataSet.getTray()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
            GridBagHelper.setConstraints(gbc, row, 5);
            panel.add(ComponentHelper.wrapComponent(new JLabel(dataSet.getTableau()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
            GridBagHelper.setConstraints(gbc, row, 6);
            panel.add(ComponentHelper.wrapComponent(new JLabel(dataSet.getUser()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
            GridBagHelper.setConstraints(gbc, row, 7);
            panel.add(ComponentHelper.wrapComponent(new JLabel(dataSet.getTime()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
            GridBagHelper.setConstraints(gbc, row, 8);
            panel.add(getCheck(i), gbc);
            row++;
        }

        pp.add(BorderLayout.NORTH, panel);
        return pp;
    }

    private JPanel getCheck(int id) {
//		JCheckBox check = new JCheckBox("Löschen (" + id + ") ?");
//		check.setName(id + "");
//		checkBoxes.add(check);
//		return ComponentHelper.wrapComponent(check, Colors.CONTENT_BACKGROUND, 3, 16, 3, 0);
        return ComponentHelper.wrapComponent(new JLabel(""), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0);
    }

    private void deleteSelectedRows() {
        for (int i = 0; i < checkBoxes.size(); i++) {
            if (checkBoxes.get(i).isSelected()) {
                locationInputField.getLogData().remove(i);
            }
        }
        Content.setContent(new ContentABIBLocationList(previousContent, locationInputField));
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel buttonPanel = new ButtonPanel();
        buttonPanel.addButtonToNorthWest(Loc.get("BACK"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Content.setContent(previousContent);
            }
        });
//		buttonPanel.addButtonToNorthCenter(Messages.get(Loc.get("DELETE")), new ActionListener() {
//
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				deleteSelectedRows();
//			}
//		});
        return buttonPanel;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

}
