package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.table.CustomTableModel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.FilterManager;
import javax.swing.RowFilter;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class FilterRowLowerThan extends AbstractFilterRow {

    public FilterRowLowerThan(Filter filterSceen, Listing listingScreen) {
        super(filterSceen, listingScreen);
    }

    @Override
    public FilterManager createFilterManager() {
        return new FilterManager() {
            @Override
            public boolean include(RowFilter.Entry<? extends CustomTableModel, ? extends Integer> entry) {
                int columnId = entry.getModel().findColumn((String) combo.getSelectedItem());
                if (columnId != -1) {
                    String value = entry.getStringValue(columnId);

                    try {
                        // check if value is numeric
                        Double valueTable = Double.parseDouble(value);
                        // check if value is numeric
                        Double valueFilter = Double.parseDouble(text.getText());
                        // compare Values
                        return valueTable < valueFilter;
                    } catch (NumberFormatException nfe) {
                        // not a number, not compareable
                        return false;
                    }
                }
                return true;
            }
        };
    }

    @Override
    protected String getDescriptionLabel() {
        return "<b>" + Loc.get("LOWER_THAN").toUpperCase() + "</b>";
    }

    @Override
    protected String getGhostText() {
        return Loc.get("NUMERIC");
    }

}
