package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.filter;

import de.uni_muenchen.vetmed.xbook.api.Loc;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public enum SyncFilterRowMode {

    CONTAINS(Loc.get("CONTAINS")),
    IS(Loc.get("IS"));

    String label;

    SyncFilterRowMode(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}

