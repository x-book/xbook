package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.ISynchronisationManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Group;
import de.uni_muenchen.vetmed.xbook.api.event.LoginAdapter;
import de.uni_muenchen.vetmed.xbook.api.event.ProjectAdapter;
import de.uni_muenchen.vetmed.xbook.api.event.ProjectEvent;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.exception.SwitchToNewModeImpossibleException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;
import de.uni_muenchen.vetmed.xbook.api.gui.AbstractSplashScreen;
import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.AbstractProgressBar;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.navigation.INavigation;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.DevPanel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.InputFieldFactory;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing.Listing;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing.ProjectListing;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.login.Login;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.login.Register;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectEdit;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectOverview;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.Export;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.groupManagement.*;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights.AddGroupRights;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights.AddUserRights;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights.GroupRights;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights.UserRights;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.ProjectSearch;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.ServerSearch;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.UpdatePanel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.*;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.filter.SyncFilter;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.navigation.AbstractNavigation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @param <T>
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractMainFrame<T extends AbstractController<?, ?>> extends JFrame implements IMainFrame {

    /**
     * The default width of the application window in pixel.
     */
    public static final int DEFAULT_WIDTH = 01024;
    /**
     * The default height of the application window in pixel.
     */
    public static final int DEFAULT_HEIGHT = 786;
    /**
     * The default minimum width of the application window in pixel.
     */
    private final int DEFAULT_MINIMUM_WIDTH = 800;
    /**
     * The default minimum height of the application window in pixel.
     */
    private final int DEFAULT_MINIMUM_HEIGHT = 600;
    protected AbstractNavigation navigation;
    protected Synchronisation synchronisation;
    protected SyncFilter synchronisationFilter;
    protected AbstractContent projectSearch;
    protected AbstractContent serverSearch;
    protected Listing listing;
    protected AbstractEntry entry;
    protected AbstractEntry entryMultiEdit;
    protected final T controller;

    private static AbstractMainFrame thisInstance;

    /**
     *
     */
    public AbstractMainFrame(T controller) {
        super();
        this.thisInstance = this;

        this.controller = controller;
        InputFieldFactory.createFactory();
        AbstractContent.setMainFrame(this);
        AbstractContent.setApiControllerAccess(controller);
        AbstractInputElement.setApiControllerAccess(controller);
        AbstractITRaw.setApiControllerAccess(controller);
        // AbstractITInputField.setApiControllerAccess(controller);
        AbstractInputElement.setMainFrame(this);
        SidebarPanel.setApiControllerAccess(controller);

        // set the favicon
        setFavicon();

        // set the default starting size and the minimum size of the application
        setSize(XBookConfiguration.getInt(XBookConfiguration.WIDTH, DEFAULT_WIDTH), XBookConfiguration.getInt(XBookConfiguration.HEIGTH, DEFAULT_HEIGHT));
        setMinimumSize(new Dimension(DEFAULT_MINIMUM_WIDTH, DEFAULT_MINIMUM_HEIGHT));
        setExtendedState(XBookConfiguration.getBoolean(XBookConfiguration.FULLSCREEN_MODE) ? MAXIMIZED_BOTH : NORMAL);

        // center the window on the desktop on start
        setLocationRelativeTo(null);

        // the application should exit when clicking on the close button
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setupLayout();

        // finally hide the splash screend and display the content
        // splash.hide();
        AbstractSplashScreen.hide();

        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                XBookConfiguration.setProperty(XBookConfiguration.WIDTH, getWidth());
                XBookConfiguration.setProperty(XBookConfiguration.HEIGTH, getHeight());
            }
        });
        addWindowStateListener(new WindowAdapter() {
            @Override
            public void windowStateChanged(WindowEvent e) {
                XBookConfiguration.setProperty(XBookConfiguration.FULLSCREEN_MODE, e.getNewState() == MAXIMIZED_BOTH);
            }
        });

        setTitle(controller.getBookName() + " - " + Loc.get("VERSION") + " " + XBookConfiguration.getProgramVersionExtended());

        Content.setContent(getLoginScreen());
        setVisible(true);
        Content.getCurrentContent().setFocus();

        controller.addLoginListener(new LoginAdapter() {
            @Override
            public void onLogin() {
                updateNavigation();
            }

            @Override
            public void onLogout() {
                updateNavigation();
            }
        });
        controller.addProjectListener(new ProjectAdapter() {
            @Override
            public void onProjectLoaded(ProjectEvent evt) {
                updateNavigation();
            }

            @Override
            public void onProjectUnloaded(ProjectEvent evt) {
                System.out.println("00000000000000000000000000");
                updateNavigation();
            }
        });

    }

    /**
     * Created the basic layout of the application and put all components
     * together.
     */
    private void setupLayout() {
        // do some settings to the JFrame object
        getContentPane().setBackground(Colors.CONTENT_BACKGROUND);

        // put the layout together
        setLayout(new BorderLayout());

        // split the sub-navigation and conent area
        JPanel center = new JPanel(new BorderLayout());
        center.add(BorderLayout.CENTER, new Content());

        // add everything
        add(BorderLayout.CENTER, center);
        navigation = createNavigation();
        add(BorderLayout.NORTH, navigation);
        if (isSidebarAllowed()) {
            add(BorderLayout.EAST, new Sidebar());
        } else {
            //new sidebar but don't add it
            new Sidebar();
        }
        add(BorderLayout.SOUTH, getFooter());
    }

    /**
     * Returns the Footer object.
     *
     * @return The Footer object.
     */
    protected Footer getFooter() {
        return new Footer(this);
    }

    /**
     * Set a favicon to the title bar.
     */
    protected abstract void setFavicon();

    /**
     * Get the specific controller of the application.
     *
     * @return The specific controller of the application.
     */
    @Override
    public T getController() {
        return controller;
    }

    public abstract AbstractSplashScreen getSplashScreen();

    @Override
    public void displaySplashScreen() {
        getSplashScreen();
    }

    /* ********************************************************************** */
    /*                      DISPLAY SPECIFIC CONTENT PANELS
     /* ********************************************************************** */

    /**
     * Display the login screen.
     */
    @Override
    public void displayLoginScreen() {
        if (getController().isLoggedIn()) {
            getController().logout();
        }
        Content.setContent(getLoginScreen());
    }

    /**
     * Display the settings screen.
     */
    @Override
    public void displaySettingsScreen() {
        Content.setContent(getSettingsScreen());
    }

    /**
     * Display the export screen.
     */
    @Override
    public void displayExportScreen() {
        Content.setContent(getExportScreen());
    }

    /**
     * Display the register screen.
     */
    @Override
    public void displayRegisterScreen() {
        Content.setContent(getRegisterScreen());
    }

    /**
     * Display the project overview screen.
     */
    @Override
    public void displayProjectOverviewScreen() {
        Content.setContent(getProjectOverviewScreen());
    }

    /**
     * Display the development panel screen.
     */
    @Override
    public void displayDevPanelScreen() {
        Content.setContent(getDevPanelScreen());
    }


//    /**
//     * Display the development panel screen.
//     */
//    @Override
//    public void displayTestFxPanelScreen3() {
//        Content.setContent(getTestFxPanelScreen3());
//    }

    /**
     * Display the project overview screen.
     */
    @Override
    public void displayNewProjectScreen() {
        Content.setContent(getNewProjectScreen());
    }

    /**
     * Display the project user rights screen.
     */
    @Override
    public void displayProjectUserRightsScreen() {
        try {
            if (controller.hasEditRights()) {
                Content.setContent(getProjectUserRightsScreen());
            } else {
                Footer.displayError(Loc.get("ERROR_MESSAGE>NO_RIGHT_TO_EDIT_USER_RIGHTS"));
            }
        } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException ex) {
            Footer.displayError(Loc.get("ERROR_MESSAGE>NO_RIGHT_TO_EDIT_USER_RIGHTS"));
            Logger.getLogger(AbstractMainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void displayProjectUserRightsAddUserScreen() {
        Content.setContent(getProjectUserRightsAddUserScreen());
    }

    public void displayProjectUserRightsAddUserToGroupScreen(GroupManagement rangScreen, Group group) {
        Content.setContent(getProjectUserRightsAddUserToGroupScreen(rangScreen, group));
    }

    @Override
    public void displayProjectUserRightsAddGroupScreen() {
        Content.setContent(getProjectUserRightsAddGroupScreen());
    }

    public void displayProjectUserRightsEditUserRankScreen(GroupManagement rangScreen, Group group, User user, String currentRank) {
        Content.setContent(getProjectUserRightsEditUserRankScreen(rangScreen, group, user, currentRank));
    }

    @Override
    public void displayProjectUserRightsCreateGroupScreen() {
        Content.setContent(getProjectUserRightsCreateGroupScreen());
    }

    public void displayProjectUserRightsCreateRankScreen(GroupManagement rangScreen) {
        Content.setContent(getProjectUserRightsCreateRankScreen(rangScreen));
    }

    /**
     * Display the project group rights screen.
     */
    @Override
    public void displayProjectGroupRightsScreen() {
        try {
            if (controller.hasEditRights()) {
                Content.setContent(getProjectGroupRightsScreen());
            } else {
                Footer.displayError(Loc.get("ERROR_MESSAGE>NO_RIGHT_TO_EDIT_GROUP_RIGHTS"));
            }
        } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException ex) {
            Footer.displayError(Loc.get("ERROR_MESSAGE>NO_RIGHT_TO_EDIT_GROUP_RIGHTS"));
            Logger.getLogger(AbstractMainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Display the project group rights screen.
     */
    @Override
    public void displayGroupManagementScreen() {
        Content.setContent(getGroupManagementScreen());
    }

    @Override
    public void displayGroupManagementScreen(Group group) {
        Content.setContent(getGroupManagementScreen(group));
    }

    @Override
    public void displayMultiEditEntryScreenInputUnit() {
        if (entryMultiEdit == null) {
            entryMultiEdit = getEntryInputScreen(GeneralInputMaskMode.MULTI_EDIT);
        }
        Content.setContent(entryMultiEdit);
    }

    @Override
    public void displayEntryScreenInputUnit(boolean isNewEntry, boolean ignoreRememberValues) {
        if (entry == null) {
            entry = getEntryInputScreen(GeneralInputMaskMode.SINGLE);
        }
        if (isNewEntry) {
            try {
                entry.toNewEntryMode(ignoreRememberValues);
            } catch (SwitchToNewModeImpossibleException ex) {
                Logger.getLogger(AbstractMainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Content.setContent(entry);
    }

    /**
     * Display the entry input screen with a specific entry.
     *
     * @param entry
     * @param data
     * @param isEditMode
     */
    public void displayEntryScreenInputUnit(AbstractEntry entry, EntryDataSet data, boolean isEditMode) {
        if (entry == null) {
            entry = getEntryInputScreen(GeneralInputMaskMode.SINGLE);
        }

        entry.setEntryData(data, isEditMode);
        Content.setContent(entry);
    }

    @Override
    public void displayListingScreen() {
        Content.setContent(getListingScreen());
    }

    @Override
    public void displayListingScreen(BaseEntryManager manager) {
        Content.setContent(getListingScreen(manager));
    }

    /**
     * Display the project edit screen.
     */
    @Override
    public void displayProjectEditScreen(ProjectDataSet project) {
        Content.setContent(getProjectEditScreen(project));
    }

    /**
     * Display the synchronisation screen.
     */
    @Override
    public void displaySynchronisationScreen() {
        Content.setContent(getSynchronisationScreen());
    }

    /**
     *
     */
    public void displaySynchronisationFilterScreen(Synchronisation syncPanel) {
        Content.setContent(getSynchronisationFilterScreen(syncPanel));
    }

    /**
     * Display the auto synchronisation message screen.
     */
    @Override
    public void displayAutoSynchronisationMessageScreen(AbstractContent previousContent, Key loadedEntryKey) {
        Content.setContent(getAutoSynchronisationMessageScreen(previousContent, loadedEntryKey));
    }

    /**
     * Display the synchronisation conflict screen.
     */
    @Override
    public void displayConflictScreen() {
        Content.setContent(getConflictScreen());
    }

    @Override
    public void displaySolveConflictScreen(DataSetOld localData, DataSetOld serverData, ISynchronisationManager syncInterface, ManagerType managerType) {
        Content.setContent(getSolveConflictScreen(localData, serverData, syncInterface, managerType));
    }

    /**
     * Display the importer screen.
     */
    @Override
    public void displayImportScreen() {
        Content.setContent(getImportScreen());
    }

    /**
     * Display the importer screen.
     *
     * @param previousPanel
     */
    public void displayNotifyProjectOwnerScreen(SolveConflict previousPanel) {
        Content.setContent(getNotifyProjectOwnerScreen(previousPanel));
    }

    /* ********************************************************************** */
    /*                      DISPLAY SPECIFIC CONTENT PANELS
     /* ********************************************************************** */

    /**
     * Get the login screen.
     *
     * @return The login screen.
     */
    protected AbstractContent getLoginScreen() {
        return new Login();
    }

    /**
     * Get the settings screen.
     *
     * @return The settings screen.
     */
    protected abstract AbstractContent getSettingsScreen();

    /**
     * Get the export screen.
     *
     * @return The export screen.
     */
    protected AbstractContent getExportScreen() {
        return new Export();
    }

    /**
     * Get the entry input screen.
     *
     * @param m Defines the general input mask mode, e.g. "Single" or "Multi
     *          Edit".
     * @return The entry input screen.
     */
    public abstract AbstractEntry getEntryInputScreen(GeneralInputMaskMode m);

    /**
     * Get the register screen.
     *
     * @return The register screen.
     */
    protected AbstractContent getRegisterScreen() {
        return new Register();
    }

    /**
     * Get the project overview screen.
     *
     * @return The project overview screen.
     */
    public abstract AbstractProjectOverview getProjectOverviewScreen();


    /**
     * Get the anaylsis panel screen.
     *
     * @return The anaylsis panel screen.
     */
//    ToolsetSwing3 toolsetSwing3;
//
//    protected AbstractContent getTestFxPanelScreen3() {
//        if (toolsetSwing3 == null) {
//            toolsetSwing3 = new ToolsetSwing3(new ToolsetInterfaceClass(controller));
//        }
//        return toolsetSwing3;
//    }

    /**
     * Get the development panel screen.
     *
     * @return The development panel screen.
     */
    protected AbstractContent getDevPanelScreen() {
        return new DevPanel();
    }

    /**
     * @param project
     * @return
     */
    public abstract AbstractProjectEdit getProjectEditScreen(ProjectDataSet project);

    /**
     * Get the project overview screen.
     *
     * @return The project overview screen.
     */
    public abstract AbstractProjectEdit getNewProjectScreen();

    /**
     * Get the project user rights screen.
     *
     * @return The project user rights screen.
     */
    public AbstractContent getProjectUserRightsScreen() {
        return new UserRights();
    }

    public AbstractContent getProjectUserRightsAddUserScreen() {
        return new AddUserRights();
    }

    public AbstractContent getProjectUserRightsAddUserToGroupScreen(GroupManagement rangScreen, Group group) {
        return new AddUserToGroup(rangScreen, group);
    }

    public AbstractContent getProjectUserRightsAddGroupScreen() {
        return new AddGroupRights();
    }

    public AbstractContent getProjectUserRightsEditUserRankScreen(GroupManagement rangScreen, Group group, User user, String currentRank) {
        return new EditUserRank(rangScreen, group, user, currentRank);
    }

    public AbstractContent getProjectUserRightsCreateGroupScreen() {
        return new CreateGroup();
    }

    public AbstractContent getProjectUserRightsCreateRankScreen(GroupManagement rangScreen) {
        return new CreateRank(rangScreen);
    }

    /**
     * Get the project group rights screen.
     *
     * @return The project group rights screen.
     */
    public AbstractContent getProjectGroupRightsScreen() {
        return new GroupRights();
    }

    /**
     * Get the project group rights screen.
     *
     * @return The project group rights screen.
     */
    public AbstractContent getGroupManagementScreen() {
        return new GroupManagement();
    }

    /**
     * Get the project group rights screen.
     *
     * @param group
     * @return The project group rights screen.
     */
    public AbstractContent getGroupManagementScreen(Group group) {
        return new GroupManagement(group);
    }

    /**
     * Get the project overview screen.
     *
     * @return The project overview screen.
     */
    public Listing getListingScreen() {
        if (listing == null) {
            listing = new Listing();
        }
        return listing;
    }

    /**
     * Get the project overview screen.
     *
     * @param manager
     * @return The project overview screen.
     */
    public Listing getListingScreen(BaseEntryManager manager) {
        if (listing == null) {
            listing = new Listing(manager);
        } else {
            listing.setSelectedTable(manager);
        }
        return listing;
    }

    /**
     * Get the conflict screen.
     *
     * @return The conflict screen.
     */
    public AbstractContent getConflictScreen() {
        return new ConflictScreen();
    }

    /**
     * Get the importer screen.
     *
     * @return The importer screen.
     */
    public abstract AbstractContent getImportScreen();

    /**
     * Get the solve conflict screen.
     *
     * @param localData
     * @param serverData
     * @param syncInterface
     * @param managerType
     * @return The solve conflict screen.
     */
    public AbstractContent getSolveConflictScreen(DataSetOld localData, DataSetOld serverData, ISynchronisationManager syncInterface, ManagerType managerType) {
        return new SolveConflict(localData, serverData, syncInterface, managerType);
    }

    /**
     * Get the synchronisation screen.
     *
     * @return The synchronisation screen.
     */
    public AbstractContent getSynchronisationScreen() {
        if (synchronisation == null) {
            synchronisation = new Synchronisation(this);
            ((AbstractSynchronisationController) getController()).getSynchroniser().addPropertyChangeListener(synchronisation);
        }
        return synchronisation;
    }

    /**
     * Get the synchronisation screen.
     *
     * @return The synchronisation screen.
     */
    public SyncFilter getSynchronisationFilterScreen(Synchronisation syncPanel) {
        if (synchronisationFilter == null) {
            synchronisationFilter = new SyncFilter(syncPanel);
        }
        return synchronisationFilter;
    }

    /**
     * Get the synchronisation screen.
     *
     * @param previousPanel
     * @return The synchronisation screen.
     */
    public AbstractContent getNotifyProjectOwnerScreen(SolveConflict previousPanel) {
        return new NotifyProjectOwnerScreen(previousPanel);
    }

    /**
     * Get the auto synchronisation message screen.
     *
     * @param previousContent
     * @param loadedEntryKey
     * @return The auto synchronisation message screen.
     */
    public AbstractContent getAutoSynchronisationMessageScreen(AbstractContent previousContent, Key loadedEntryKey) {
        return new AutoSyncMessage(previousContent, loadedEntryKey);
    }

    /**
     * Display the updateExtention screen.
     *
     * @param sourceType
     * @param isProgramUpToDate
     * @param isDatabaseUpToDate
     * @param areCodeTablesUpToDate
     * @param isConnectedToServer
     * @return
     */
    public UpdatePanel displayUpdateScreen(UpdatePanel.SourceType sourceType, boolean isProgramUpToDate, boolean isDatabaseUpToDate,
                                           boolean areCodeTablesUpToDate, boolean isConnectedToServer, boolean autoContinue) {
        UpdatePanel update = new UpdatePanel(Content.getCurrentContent(), sourceType, isProgramUpToDate, isDatabaseUpToDate, areCodeTablesUpToDate, isConnectedToServer, ((AbstractSynchronisationController) controller), autoContinue);
        Content.setContent(update);
        return update;
    }

    public AbstractNavigation getNavigation() {
        return navigation;
    }

    public abstract AbstractNavigation createNavigation();

    @Override
    public abstract AbstractProgressBar getProgressBar();

    @Override
    public abstract ArrayList<Icon> getWorkingIcons();

    public void unloadListing() {
        if (listing != null) {
            listing.unload();
        }
    }

    @Override
    public int getContentSize() {
        int width = getBounds().width;
        return width;
    }

    @Override
    public ArrayList<AbstractEntry> getAllAvailableEntryPanels() {
        ArrayList<AbstractEntry> list = new ArrayList<>();
        if (entry != null) {
            list.add(entry);
        }
        return list;
    }

    @Override
    public void updateNavigation() {
        if (!controller.isLoggedIn()) {
            updateNavigation(INavigation.Mode.NOT_LOGGED_IN);
        } else if (!controller.isProjectLoaded()) {
            updateNavigation(INavigation.Mode.NO_PROJECT_LOADED);
        } else {
            updateNavigation(INavigation.Mode.STANDARD);
        }
    }

    @Override
    public void updateNavigation(INavigation.Mode mode) {
        navigation.setMode(mode);
    }

    public boolean isSidebarAllowed() {
        return true;
    }

    public static AbstractMainFrame getMainFrame() {
        return thisInstance;
    }

    /**
     * Display search screen.
     */
    @Override
    public void displayProjectSearchScreen() {
        Content.setContent(getProjectSearchScreen());
    }

    /**
     * Get the project search screen.
     *
     * @return The project search screen.
     */
    protected AbstractContent getProjectSearchScreen() {
        if (projectSearch == null) {
            projectSearch = new ProjectSearch(this);
        }
        return projectSearch;
    }

    /**
     * Display server search screen.
     */
    @Override
    public void displayServerSearchScreen() {
        Content.setContent(getServerSearchScreen());
    }

    /**
     * Get the search screen.
     *
     * @return The search screen.
     */
    protected AbstractContent getServerSearchScreen() {
        if (serverSearch == null) {
            serverSearch = new ServerSearch(this);
        }
        return serverSearch;
    }

    @Override
    public void displayBarcodeSearchScreen() {
        System.out.println("displayBarcodeSearchScreen() not implemented yet!");
    }

    @Override
    public void displayProjectListingScreen() {
        Content.setContent(getProjectListingScreen());
    }

    private ProjectListing getProjectListingScreen() {
        // use new instance so project listing has not be be updated every time a project is saved
        // if (projectListing == null) {

        // }
        return new ProjectListing();
    }
}
