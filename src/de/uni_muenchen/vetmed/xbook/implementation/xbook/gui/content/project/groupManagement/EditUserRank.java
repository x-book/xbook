package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.groupManagement;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel.Position;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rank;
import de.uni_muenchen.vetmed.xbook.api.datatype.User;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Group;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class EditUserRank extends AbstractContent {

    protected final Group group;
    private final User user;
    private JComboBox rankCombo;
    private ArrayList<Rank> ranks;
    private JPanel panel;
    private final GroupManagement rangScreen;
    private String currentRank;

    public EditUserRank(GroupManagement rangScreen, Group group, User user, String currentRank) {
        super();
        this.currentRank = currentRank;
        init();

        this.group = group;
        this.user = user;
        this.rangScreen = rangScreen;

        for (JComponent c : getCustomContent()) {
            panel.add(c);
        }
    }

    @Override
    protected JPanel getContent() {
        panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);
        panel.add(new JLabel(Loc.get("EDIT_RANK")));

        return panel;
    }

    protected void actionAfterSaving() {
        mainFrame.displayGroupManagementScreen(group);
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();

        panel.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_CANCEL, Loc.get("CANCEL"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionAfterSaving();
            }
        });

        panel.addImageButton(Position.NORTH_EAST, Images.BUTTONPANEL_SAVE, Loc.get("SAVE"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int rankId = -1;
                for (Rank rank : ranks) {
                    if (rank.getName().equals((String) rankCombo.getSelectedItem())) {
                        rankId = rank.getId();
                    }
                }
                if (rankId != -1) {
                    try {
                        Message ms = ((AbstractSynchronisationController) mainFrame.getController()).updateUserRank(group.getId(), user.getId(), rankId);
                        if (!ms.getResult().wasSuccessful()) {
                            Footer.displayError(ms.getResult().getErrorMessage());
                        }

                    } catch (IOException | NotConnectedException | NotLoggedInException ex) {
                        Logger.getLogger(EditUserRank.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    actionAfterSaving();
                }
            }
        });
        return panel;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    @Override
    public void setFocus() {
    }

    @Override
    public void actionOnDisconnect() {
    }

    private ArrayList<JComponent> getCustomContent() {
        ArrayList<JComponent> array = new ArrayList<>();

        rankCombo = new JComboBox();
        rankCombo.setPreferredSize(new Dimension((int) (Sizes.BUTTON_WIDTH * 1.5), Sizes.BUTTON_HEIGHT));
        ranks = new ArrayList<>();
        try {
            ranks = mainFrame.getController().getRankOfGroup(group.getId());
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(AddUserToGroup.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Rank rank : ranks) {
            rankCombo.addItem(rank.getName());
        }
        rankCombo.setSelectedItem(currentRank);
        array.add(rankCombo);
        if (ranks.isEmpty()) {
            rankCombo.setEnabled(false);
            array.add(new JLabel(Loc.get("NO_RANKS_AVAILABLE")));
        }

        return array;
    }
}
