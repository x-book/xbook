package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.linkedEntriesHelper;

import javax.swing.JComponent;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class VisualColumnHelper {

    private int width;
    private JComponent element;

    public VisualColumnHelper(int width, JComponent element) {
        this.width = width;
        this.element = element;
    }

    public JComponent getElement() {
        return element;
    }

    public int getWidth() {
        return width;
    }
    
    
    
}
