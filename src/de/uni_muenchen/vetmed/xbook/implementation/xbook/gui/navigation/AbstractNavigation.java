package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.navigation;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.navigation.*;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractNavigation<T extends IMainFrame> extends AbstractBaseNavigation<T> {

    protected MainNavigationPopupElement reportError;
    protected MainNavigationPopupElement wiki;
    protected MainNavigationPopupSeparator separatorBeforeAbout;

    /**
     * Constructor
     * <p>
     * Sets the basic layout of the navigation bar.
     *
     * @param mainFrame The basic main frame object.
     */
    public AbstractNavigation(T mainFrame) {
        super(mainFrame);
    }

    @Override
    public void loadElements() {
        addSeparator(0);
        initProjectElements();
        initDataEntryElements();
        initListingElements();
        addSeparator(Prio.M_SEP1);
        initRightsElements();
        initToolsElements();
        addSeparator(Prio.M_SEP2);
        initHelpElements();
        addBookSpecificIssueTracker();
        initLogoutElements();
        if (AbstractConfiguration.MODE == AbstractConfiguration.Mode.DEVELOPMENT) {
            addSeparator(Prio.M_SEP3);
            addSeparator(Prio.M_SEP3);
            addSeparator(Prio.M_SEP3);
            initDevElements();
        }
        addCustomItems();
        loadCustomProjectElements();
        if (controller.isProjectLoaded()) {
            loadCustomListingElements();
        }

    }

    /**
     * Initialised all main navigation elements and navigation popup elements for the item 'projects'.
     */
    protected void initProjectElements() {
        project = createMainNavigationElement(Prio.M_PROJECT, getProjectsLabel(), Images.NAVIGATION_ICON_PROJECTS,
                new Runnable() {
                    @Override
                    public void run() {
                        mainFrame.displayProjectOverviewScreen();
                    }
                }, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);

        createNavigationPopupElement(project, Prio.P_PROJECT_OVERVIEW, getProjectsOverviewLabel(),
                Images.NAVIGATION_ICON_SMALL_PAPER, new Runnable() {
                    @Override
                    public void run() {
                        mainFrame.displayProjectOverviewScreen();
                    }
                }, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);

        createNavigationPopupElement(project, Prio.P_PROJECT_OVERVIEW + 1, Loc.get("PROJECT") + " " + Loc.get(
                "LISTING"), Images.NAVIGATION_ICON_SMALL_PAPER, new Runnable() {
            @Override
            public void run() {
                mainFrame.displayProjectListingScreen();
            }
        }, INavigation.Mode.STANDARD, INavigation.Mode.NO_PROJECT_LOADED);

        addPopupSeparator(project, Prio.P_PROJECT_NEW - 1);

        createNavigationPopupElement(project, Prio.P_PROJECT_NEW, getCreateNewProjectLabel(),
                Images.NAVIGATION_ICON_SMALL_PLUS, new Runnable() {
                    @Override
                    public void run() {
                        mainFrame.displayNewProjectScreen();
                    }
                }, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);

        createNavigationPopupElement(project, Prio.P_PROJECT_NEW + 1, Loc.get("EDIT_PROJECT"),
                Images.NAVIGATION_ICON_SMALL_EDIT, new Runnable() {
                    @Override
                    public void run() {
                        try {
                            mainFrame.displayProjectEditScreen(controller.getCurrentProject());
                        } catch (NotLoadedException e) {
                            e.printStackTrace();
                        }
                    }
                }, INavigation.Mode.STANDARD);

    }

    /**
     * Initialised all main navigation elements and navigation popup elements for the item 'data entry'.
     */
    protected void initDataEntryElements() {
        dataEntry = createMainNavigationElement(Prio.M_DATA_ENTRY, Loc.get("DATA_ENTRY_NAVIGATION"),
                Images.NAVIGATION_ICON_DATA_ENTRY, new Runnable() {
                    @Override
                    public void run() {
                        mainFrame.displayEntryScreenInputUnit(false, false);
                    }
                }, INavigation.Mode.STANDARD);

        addPopupSeparator(dataEntry, Prio.M_DATA_ENTRY + 1);

        createNavigationPopupElement(dataEntry, Prio.P_DATA_ENTRY_BACK, Loc.get("BACK_TO_DATA_ENTRY"),
                Images.NAVIGATION_ICON_SMALL_PAPER, new Runnable() {
                    @Override
                    public void run() {
                        mainFrame.displayEntryScreenInputUnit(false, false);
                    }
                }, INavigation.Mode.STANDARD);

        createNavigationPopupElement(dataEntry, Prio.P_DATA_ENTRY_NEW, Loc.get("NEW_DATASET"),
                Images.NAVIGATION_ICON_SMALL_PLUS, new Runnable() {
                    @Override
                    public void run() {
                        mainFrame.displayEntryScreenInputUnit(true, false);
                    }
                }, INavigation.Mode.STANDARD);
    }

    /**
     * Initialised all main navigation elements and navigation popup elements for the item 'listing'.
     */
    protected void initListingElements() {
        listing = createMainNavigationElement(Prio.M_LISTING, Loc.get("LISTING"), Images.NAVIGATION_ICON_LISTING,
                new Runnable() {
                    @Override
                    public void run() {
                        mainFrame.displayListingScreen();
                    }
                }, INavigation.Mode.STANDARD);
    }

    /**
     * Initialised all main navigation elements and navigation popup elements for the item 'group management'.
     */
    protected void initRightsElements() {
    }

    /**
     * Initialised all main navigation elements and navigation popup elements for the item 'tools'.
     */
    protected void initToolsElements() {
        tools = createMainNavigationElement(Prio.M_TOOLS, Loc.get("TOOLS"), Images.NAVIGATION_ICON_SETTINGS,
                INavigation.Mode.NOT_LOGGED_IN, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);

        createNavigationPopupElement(tools, Prio.P_TOOLS_EXPORT, Loc.get("EXPORT"),
                Images.NAVIGATION_ICON_SMALL_EXPORT, new Runnable() {
                    @Override
                    public void run() {
                        mainFrame.displayExportScreen();
                    }
                }, INavigation.Mode.STANDARD);

        // show Import only in OssoBook
        if (controller.getBookName().equals("OssoBook")) {
            createNavigationPopupElement(tools, Prio.P_TOOLS_IMPORT, Loc.get("IMPORT") + " (alpha)",
                    Images.NAVIGATION_ICON_SMALL_IMPORT, new Runnable() {
                        @Override
                        public void run() {
                            mainFrame.displayImportScreen();
                        }
                    }, INavigation.Mode.STANDARD);
        }

        addPopupSeparator(tools, Prio.P_TOOLS_IMPORT + 1);

        createNavigationPopupElement(tools, Prio.P_TOOLS_SETTINGS, Loc.get("SETTINGS"),
                Images.NAVIGATION_ICON_SMALL_SETTINGS, new Runnable() {
                    @Override
                    public void run() {
                        mainFrame.displaySettingsScreen();
                    }
                }, INavigation.Mode.NOT_LOGGED_IN, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);

//        createMainNavigationElement(Prio.M_SEARCH + 3, Loc.get("IMPORT"), Images.NAVIGATION_ICON_IMPORT, new
//        Runnable() {
//            @Override
//            public void run() {
//                mainFrame.displayImportScreen();
//            }
//        }, Mode.NO_PROJECT_LOADED, Mode.STANDARD);
    }

    /**
     * Initialised all main navigation elements and navigation popup elements for the item 'help'.
     */
    protected void initHelpElements() {
        help = createMainNavigationElement(Prio.M_HELP, Loc.get("HELP"), Images.NAVIGATION_ICON_HELP,
                INavigation.Mode.NOT_LOGGED_IN, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);

        reportError = createNavigationPopupElement(help, Prio.P_HELP_REPORT, Loc.get(
                "REPORT_ERROR") + " (GitLab - xBook)", Images.NAVIGATION_ICON_SMALL_AT, new Runnable() {
            @Override
            public void run() {
                try {
                    Desktop desktop = Desktop.getDesktop();
                    desktop.browse(new URI("http://xbook.vetmed.uni-muenchen.de/gitlab/xbook/xbook/issues"));
                } catch (URISyntaxException | IOException ex) {
                    Logger.getLogger(AbstractBaseNavigation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, Mode.NOT_LOGGED_IN, Mode.NO_PROJECT_LOADED, Mode.STANDARD);

        wiki = createNavigationPopupElement(help, Prio.P_HELP_WIKI, "xBook Wiki...", Images.NAVIGATION_ICON_SMALL_LINK,
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            URI uri = new URI("http://xbook.vetmed.uni-muenchen.de/wiki/");
                            Desktop.getDesktop().browse(uri);
                        } catch (URISyntaxException | IOException ex) {
                            Logger.getLogger(AbstractBaseNavigation.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }, INavigation.Mode.NOT_LOGGED_IN, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);

        separatorBeforeAbout = addPopupSeparator(help, Prio.P_HELP_WIKI + 1);

        createNavigationPopupElement(help, Prio.P_HELP_ABOUT, Loc.get("ABOUT"), Images.NAVIGATION_ICON_SMALL_INFO,
                new Runnable() {
                    @Override
                    public void run() {
                        mainFrame.displaySplashScreen();
                    }
                }, INavigation.Mode.NOT_LOGGED_IN, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);
    }

    /**
     * Initialised all main navigation elements and navigation popup elements for the item 'dev'.
     */
    protected void initDevElements() {
        dev = createMainNavigationElement(Prio.M_DEV, "Dev", Images.NAVIGATION_ICON_PACMAN_GHOST, new Runnable() {
            @Override
            public void run() {
                mainFrame.displayDevPanelScreen();
            }
        }, INavigation.Mode.NOT_LOGGED_IN, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);
    }

    /**
     * Initialised all main navigation elements and navigation popup elements for the item 'logout'.
     */
    protected void initLogoutElements() {
        logout = createMainNavigationElement(Prio.M_LOGOUT, Loc.get("LOGOUT_NAVIGATION"),
                Images.NAVIGATION_ICON_LOGOUT, new Runnable() {
                    @Override
                    public void run() {
                        mainFrame.getController().logout();
                    }
                }, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);
    }

    /**
     * Loads all custom project elements.
     */
    public void loadCustomProjectElements() {
    }

    /**
     * Loads all custom listing elements as soon as the managers knows which are available.
     */
    public void loadCustomListingElements() {

        // get all sync table managers (== input units to be displayed)
        ArrayList<BaseEntryManager> listTablesDisplay = new ArrayList<>();
        try {
            for (IBaseManager baseManager :
                    ((AbstractController<?, ?>) mainFrame.getController()).getLocalManager().getSyncTables()) {
                if (baseManager instanceof BaseEntryManager) {
                    AbstractBaseEntryManager abm = (AbstractBaseEntryManager) baseManager;
                    listTablesDisplay.add(abm);
                }
            }
        } catch (NotLoggedInException ignored) {
        }
        final BaseEntryManager[] managers = listTablesDisplay.toArray(new BaseEntryManager[0]);

        // only create navigation popup elements of the listings if there are more than 1
        if (managers.length > 1) {
            // create for each navigation element one navigation popup element
            for (int index = 0; index < managers.length; index++) {
                final int finalIndex = index;
                createNavigationPopupElement(listing, Prio.P_LISTING_CUSTOM++,
                        Loc.get("LISTING") + ": " + managers[index], new Runnable() {
                            @Override
                            public void run() {
                                mainFrame.displayListingScreen(managers[finalIndex]);
                            }
                        }, INavigation.Mode.STANDARD);
            }

            createNavigationPopupElement(listing, Prio.P_LISTING_BACK++, Loc.get("BACK_TO_LISTING"),
                    Images.NAVIGATION_ICON_SMALL_PAPER, new Runnable() {
                        @Override
                        public void run() {
                            mainFrame.displayListingScreen();
                        }
                    }, INavigation.Mode.STANDARD);

            addPopupSeparator(listing, Prio.P_LISTING_BACK++);
        }
    }

    /**
     * Adds (if necessary) an own menu element for report error link of the specific Book.
     */
    private void addBookSpecificIssueTracker() {
        if (getBookSpecificIssueTrackerURL() != null) {
            createNavigationPopupElement(help, ++Prio.P_HELP_REPORT,
                    Loc.get("REPORT_ERROR") + " (GitLab - " + controller.getBookName() + ")",
                    Images.NAVIGATION_ICON_SMALL_AT, new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Desktop desktop = Desktop.getDesktop();
                                desktop.browse(new URI(getBookSpecificIssueTrackerURL()));
                            } catch (URISyntaxException | IOException ex) {
                                Logger.getLogger(AbstractNavigation.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }, INavigation.Mode.NOT_LOGGED_IN, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);
        }
    }


    /**
     * Returns the URL as a String to the Gitlab Issue Tracker of the specific Book.
     * <p>
     * Return <i>null</i> if no link is available or if no issue tracker should be displayed.
     *
     * @return The URL to the Gitlabl Issue Tracker of the specific Book.
     */
    protected abstract String getBookSpecificIssueTrackerURL();

}
