package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawFileUploadWithCategorySelectionField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class InputFileUploadWithCategorySelectionField extends AbstractDefaultInputElement {

    /**
     * The file upload field object.
     */
    protected RawFileUploadWithCategorySelectionField zField;
    /**
     * The file filter options.
     */
    protected ArrayList<FileNameExtensionFilter> fileFilter;
    /**
     * The corresponding column type for the information whether the file has to be synchronized or not.
     */
    protected ColumnType columnTypeFileMustBeSynced;
    /**
     * The corresponding column type for category information.
     */
    protected ColumnType columnTypeCategory;
    /**
     * The corresponding column type for the information of the file extension.
     */
    private final ColumnType columnTypeExtension;
    protected int[] paths;

    /**
     * Constructor.
     *
     * @param columnTypeFileName         The corresponding column type for file name.
     * @param columnTypeExtension        The corresponding column type for file extension.
     * @param columnTypeFileMustBeSynced The corresponding column type for the information whether the file has to be
     *                                   synchronized or not.
     * @param columnTypeCategory         The corresponding column type for category information.
     * @param displayedCategoryIds       The available categoryId where the files should be saved in the data
     *                                   structure.
     * @param fileFilter                 The file filter options.
     */
    public InputFileUploadWithCategorySelectionField(ColumnType columnTypeFileName, ColumnType columnTypeExtension,
                                                     ColumnType columnTypeFileMustBeSynced,
                                                     ColumnType columnTypeCategory, int[] displayedCategoryIds,
                                                     ArrayList<FileNameExtensionFilter> fileFilter) {
        super(columnTypeFileName);
        this.fileFilter = fileFilter;
        this.columnTypeExtension = columnTypeExtension;
        this.columnTypeFileMustBeSynced = columnTypeFileMustBeSynced;
        this.columnTypeCategory = columnTypeCategory;
        this.paths = displayedCategoryIds;

        setGridX(3);
        setGridY(2);
    }

    /**
     * Returns the current file name of the input field.
     *
     * @return
     */
    public String getInput() {
        return zField.getText();
    }

    @Override
    public AbstractInputElementFunctions createInputField() {
        if (zField == null) {
            zField = new RawFileUploadWithCategorySelectionField(apiControllerAccess, columnType, columnTypeExtension
                    , columnTypeFileMustBeSynced, columnTypeCategory, paths, fileFilter);
        }

        try {
            zField.setItems(mainFrame.getController().getHashedCodeTableEntries(columnTypeCategory));
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(InputFileUploadWithMultiCategorySelectionField.class.getName()).log(Level.SEVERE, null,
                    ex);
        }

        return zField;
    }

    /**
     * Set the selected categoryId of the file to a new value;
     *
     * @param path The new categoryId to set.
     */
    public void setPath(String path) {
        zField.setText(path);
    }

    @Override
    public void actionOnFocusLost() {
        super.actionOnFocusLost();
        zField.actionOnFocusLost();
        if (!zField.isValidPath()) {
            clear();
            setErrorStyle();
        }
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        zField.getTextField().setEnabled(false);
    }

    @Override
    public void setEnabledForRights(boolean b) {
        super.setEnabledForRights(b);
        zField.getTextField().setEnabled(false);
    }

}
