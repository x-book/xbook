package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.specific_reuseable;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.HierarchicComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.UpdateableEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryCodeTableField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.BarcodeHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.InputComboIdBox;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputABIBLocation extends AbstractInputElement {

    private JLabel labelLog;
    private JButton buttonLog;
    private ArrayList<ABIBLocationLogEntry> logData;
    private HierarchicComboBox comboLocation;
    private JTextField textfieldRack;
    private JTextField textfieldTray;
    private JTextField textfieldTableau;
    private final int GRID = 6;
    /**
     * The textfield where to enter the barcode.
     */
//    private JTextField textfieldBarcode;
    /**
     * The field to display the visualization of the barcode.
     */
//    private BarcodeHelper.BarcodeWrapper barcode;

    private static ColumnType columnTypeCurrentLocation;
    private static ColumnType columnTypeRackShowcase;
    private static ColumnType columnTypeTray;
    private static ColumnType columnTypeTableau;
    private static ColumnType columnTypeUserId;
    private static ColumnType columnTypeTime;
//    private static ColumnType columnTypeBarcode;

    private static String tableNameCurrentLocation;

    public enum LabelType {

        PDF, HTML
    }

    /**
     * The elements that are selectable in the combo box.
     */
    protected static CodeTableHashMap items;

    /**
     * Constructor.
     *
     * @param tableNameCurrentLocation
     * @param columnTypeCurrentLocation
     * @param columnTypeRackShowcase
     * @param columnTypeTray
     * @param columnTypeTableau
     * @param columnTypeUserID
     * @param columnTypeTime
     * @param columnTypeBarcode
     */
    public InputABIBLocation(String tableNameCurrentLocation, ColumnType columnTypeCurrentLocation, ColumnType columnTypeRackShowcase, ColumnType columnTypeTray, ColumnType columnTypeTableau, ColumnType columnTypeUserID, ColumnType columnTypeTime) {
        super(columnTypeCurrentLocation);

        this.columnTypeCurrentLocation = columnTypeCurrentLocation;
        this.columnTypeRackShowcase = columnTypeRackShowcase;
        this.columnTypeTray = columnTypeTray;
        this.columnTypeTableau = columnTypeTableau;
        this.columnTypeUserId = columnTypeUserID;
        this.columnTypeTime = columnTypeTime;
//        this.columnTypeBarcode = columnTypeBarcode;

        this.tableNameCurrentLocation = tableNameCurrentLocation;

        setGridX(4);
        setGridY(2);
        setSidebar(new SidebarEntryCodeTableField(columnTypeCurrentLocation, Loc.get("SIDEBAR_ENTRY>CURRENT_LOCATION")));
    }

    private JLabel createLabel(String text) {
        JLabel lab = new JLabel(text);
        lab.setBackground(Colors.INPUT_FIELD_BACKGROUND);
        return lab;
    }

    public static String getPDFformatLocation(EntryDataSet data) {
        ArrayList<ABIBLocationLogEntry> log = getLogEntries(data);
        String label = "";
        if (!log.isEmpty()) {
            label = getLogLabel(log.size(), log.get(log.size() - 1), LabelType.PDF);
        }
        return label;
    }

    private static String getLogLabel(int countInLog, ABIBLocationLogEntry newestLogEntry, LabelType labeltype) {
        String label = "";
        if (labeltype == labeltype.HTML) {
            String sep = " / ";
            label = "<b>" + Loc.get("CURRENT_LOCATION") + ": <br>";
            if (!newestLogEntry.getPlace().isEmpty()) {

                label += newestLogEntry.getPlace() + sep;
            }
            if (!newestLogEntry.getRoom().isEmpty()) {

                label += newestLogEntry.getRoom() + sep;
            }
            if (label.endsWith(sep)) {
                label = label.substring(0, label.length() - sep.length());
            }
            label += "</b><br>";
            if (!newestLogEntry.getShelf().isEmpty()) {

                label += Loc.get("RACK_SHOWCASE") + ": <b>" + newestLogEntry.getShelf() + "</b>, ";
            }
            if (!newestLogEntry.getTray().isEmpty()) {

                label += Loc.get("TRAY") + ": <b>" + newestLogEntry.getTray() + "</b>, ";
            }
            if (!newestLogEntry.getTableau().isEmpty()) {

                label += Loc.get("TABLEAU") + ": <b>" + newestLogEntry.getTableau() + "</b>";
            }

            if (label.endsWith(", ")) {
                label = label.substring(0, label.length() - ", ".length());
            }
        }
        if (labeltype == labeltype.PDF) {
            if (!newestLogEntry.getPlace().isEmpty()) {

                label = newestLogEntry.getPlace() + "\n";
            }
            if (!newestLogEntry.getRoom().isEmpty()) {

                label += newestLogEntry.getRoom() + "\n";
            }
            if (!newestLogEntry.getShelf().isEmpty()) {

                label += newestLogEntry.getShelf() + "\n";
            }
            if (!newestLogEntry.getTray().isEmpty()) {

                label += newestLogEntry.getTray() + "\n";
            }
            if (!newestLogEntry.getTableau().isEmpty()) {

                label += newestLogEntry.getTableau() + "\n";
            }
        }

        return label;
    }

    private void setLogLabel(int countInLog, ABIBLocationLogEntry newestLogEntry) {
        String label = getLogLabel(countInLog, newestLogEntry, LabelType.HTML);
        String labelTextRightAlign = "<html><div align='right'>" + label + "</div></html>";
        String labelTextLeftAlign = "<html><div align='left'>" + label + "</div></html>";
        labelLog.setText(labelTextRightAlign);
        labelLog.setToolTipText(labelTextLeftAlign);
        buttonLog.setEnabled(countInLog > 0);
        buttonLog.setText("<html><center>" + Loc.get("OPEN_LOG") + "<br>" + "(" + Loc.get("NUMBER_OF_ENTRIES_IN_LOG", countInLog) + ")" + "</center></html>");
    }

    private void removeLogLabel(boolean displayLabelNone) {
        if (displayLabelNone) {
            labelLog.setText(Loc.get("NONE"));
            labelLog.setToolTipText(Loc.get("NONE"));
        } else {
            labelLog.setText("");
            labelLog.setToolTipText("");
        }
        buttonLog.setEnabled(false);
        buttonLog.setText(Loc.get("OPEN_LOG"));
    }

    public ArrayList<ABIBLocationLogEntry> getLogData() {
        return logData;
    }

    private static ArrayList<ABIBLocationLogEntry> getLogEntries(DataSetOld data) {
        DataTableOld list = data.getOrCreateDataTable(tableNameCurrentLocation);
        ArrayList<ABIBLocationLogEntry> log = new ArrayList<>();
        for (DataRow row : list) {
            String place = "";
            String room = "";
            String shelf = "";
            String tray = "";
            String tableau = "";
            String time = "";
            String user = "";
            try {
                ArrayList<String> dataParents = apiControllerAccess.getMultiComboBoxDataParents(columnTypeCurrentLocation.getConnectedTableName(), row.get(columnTypeCurrentLocation));
                if (dataParents.size() > 1 && dataParents.get(1) != null) {
                    place = items.convertIdAsStringToString(dataParents.get(1));
                    room = items.convertIdAsStringToString(dataParents.get(0));
                } else {
                    place = items.convertIdAsStringToString(dataParents.get(0));
                }
            } catch (NotLoggedInException | StatementNotExecutedException ex) {
                Logger.getLogger(InputABIBLocation.class.getName()).log(Level.SEVERE, null, ex);
            }
            shelf = row.get(columnTypeRackShowcase);
            tray = row.get(columnTypeTray);
            tableau = row.get(columnTypeTableau);
            try {
                user = apiControllerAccess.getDisplayName(Integer.parseInt(row.get(columnTypeUserId)));
            } catch (NotLoggedInException | NumberFormatException | StatementNotExecutedException ex) {
                user = "(ID: " + row.get(columnTypeUserId);
                Logger.getLogger(InputABIBLocation.class.getName()).log(Level.SEVERE, null, ex);
            }
            time = convertTime(row.get(columnTypeTime));

            log.add(new ABIBLocationLogEntry(place, room, shelf, tray, tableau, user, time));
        }
        return log;
    }

    @Override
    public void load(DataSetOld data) {
        ArrayList<ABIBLocationLogEntry> log = getLogEntries(data);
        this.logData = log;
        if (!log.isEmpty()) {
            setLogLabel(log.size(), log.get(log.size() - 1));
        } else {
            removeLogLabel(true);
        }

        // load the barcode textfield
//        DataRow list = data.getDataRowForTable(getTableName());
//        final String s = list.get(columnTypeBarcode);
//        textfieldBarcode.setText(s);

        // load the barcode visualization
//        try {
//            barcode.updateBarcode(BarcodeFactory.createCode128(textfieldBarcode.getText()));
//        } catch (BarcodeException e1) {
//            barcode.updateBarcode(null);
//        }

    }

    @Override
    public void clear() {
        comboLocation.clear();
        textfieldRack.setText("");
        textfieldTray.setText("");
        textfieldTableau.setText("");
        removeLogLabel(false);
        logData = new ArrayList<>();
        // barcode fields
//        textfieldBarcode.setText("");
//        barcode.updateBarcode(null);
    }

    @Override
    public void save(DataSetOld data) {
        if (comboLocation.getSelectedInput() != null) {
            String positionId = comboLocation.getSelectedInput().getColumnName();
            String shelf = textfieldRack.getText();
            String tray = textfieldTray.getText();
            String tableau = textfieldTableau.getText();
//            String barcode = textfieldBarcode.getText();

            DataTableOld list = data.getOrCreateDataTable(tableNameCurrentLocation);
            if (positionId != null) {
                DataRow dataa = new DataRow(tableNameCurrentLocation);
                dataa.add(new DataColumn(positionId, columnTypeCurrentLocation.getColumnName()));
                dataa.add(new DataColumn(shelf, columnTypeRackShowcase.getColumnName()));
                dataa.add(new DataColumn(tray, columnTypeTray.getColumnName()));
                dataa.add(new DataColumn(tableau, columnTypeTableau.getColumnName()));
//                dataa.add(new DataColumn(barcode, columnTypeBarcode.getColumnName()));
                list.add(dataa);
            }
        }
    }

    /**
     * Converts the date from the format "yyyy-mm-dd hh:mm:ss" to "dd.mm.yyyy".
     *
     * @param oldFormat The date in the format "yyyy-mm-dd hh:mm:ss".
     * @return The date in the format "dd.mm.yyyy".
     */
    private static String convertTime(String oldFormat) {
        String yyyy = oldFormat.substring(0, 4);
        String mm = oldFormat.substring(5, 7);
        String dd = oldFormat.substring(8, 10);

        return dd + "." + mm + "." + yyyy;
    }

    @Override
    public String getStringRepresentation() {
        return comboLocation.getSelectedInput() + " / " +
                textfieldRack.getText() + "/" +
                textfieldTray.getText() + "/" +
                textfieldTableau.getText();
//            + " / " +
//                textfieldBarcode.getText();
    }

    private InputABIBLocation getThis() {
        return this;
    }

    @Override
    public boolean isValidInput() {
        return comboLocation.getSelectedInput() != null;
    }

    @Override
    public void setSidebar(SidebarPanel sidebar) {
        super.setSidebar(sidebar);
        if (comboLocation != null) {
            comboLocation.setSidebar(sidebar);
        }
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textfieldRack);
        comp.add(textfieldTray);
        comp.add(textfieldTableau);
        comp.add(buttonLog);
        comp.add(comboLocation);
//        comp.add(textfieldBarcode);
//        comp.add(barcode);
        for (HierarchicComboBox.Box b : comboLocation.getAllBoxes()) {
            comp.add(b);
            comp.add(b.getEditor().getEditorComponent());
        }
        return comp;
    }


    @Override
    protected void createFieldInput() {
        int panelWidthAbsolute = getPreferredSize().width;
        int panelWidthToUse = panelWidthAbsolute - 30;

        JPanel outerWrapper = new JPanel(new BorderLayout());

        String[] labelCombo = new String[]{Loc.get("ADD_LOCATION"), Loc.get("ROOM")};
        int[] widthsCombo = new int[]{2 * (panelWidthToUse / GRID), panelWidthToUse / GRID};

        comboLocation = new HierarchicComboBox(apiControllerAccess, false, columnTypeCurrentLocation, false, labelCombo, widthsCombo);
        Dimension dim = new Dimension((int) (3 * (panelWidthAbsolute / GRID) * 0.7), 2 * Sizes.INPUT_FIELD_HEIGHT);
        comboLocation.setSize(dim);
        comboLocation.setMinimumSize(dim);
        comboLocation.setPreferredSize(dim);
        outerWrapper.add(BorderLayout.WEST, comboLocation);

        JPanel panelRightWrapper = new JPanel(new BorderLayout());
        JPanel panelRight = new JPanel(new FlowLayout());
        JPanel shelfWrapper = new JPanel(new BorderLayout());
        textfieldRack = new JTextField();
        textfieldRack.setEnabled(false);
        textfieldRack.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        textfieldRack.setPreferredSize(new Dimension(1 * (panelWidthToUse / GRID), Sizes.INPUT_FIELD_HEIGHT));
        textfieldRack.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (textfieldRack.getText().isEmpty()) {
                    textfieldRack.setToolTipText(null);
                } else {
                    textfieldRack.setToolTipText(textfieldRack.getText());
                }
            }
        });
        shelfWrapper.add(BorderLayout.NORTH, new JLabel(Loc.get("RACK_SHOWCASE")));
        shelfWrapper.add(BorderLayout.CENTER, textfieldRack);
        panelRight.add(shelfWrapper);

        JPanel trayWrapper = new JPanel(new BorderLayout());
        textfieldTray = new JTextField();
        textfieldTray.setEnabled(false);
        textfieldTray.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        textfieldTray.setPreferredSize(new Dimension(1 * (panelWidthToUse / GRID), Sizes.INPUT_FIELD_HEIGHT));
        textfieldTray.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (textfieldTray.getText().isEmpty()) {
                    textfieldTray.setToolTipText(null);
                } else {
                    textfieldTray.setToolTipText(textfieldTray.getText());
                }
            }
        });
        trayWrapper.add(BorderLayout.NORTH, new JLabel(Loc.get("TRAY")));
        trayWrapper.add(BorderLayout.CENTER, textfieldTray);
        panelRight.add(trayWrapper);

        JPanel tableauWrapper = new JPanel(new BorderLayout());
        textfieldTableau = new JTextField();
        textfieldTableau.setEnabled(false);
        textfieldTableau.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        textfieldTableau.setPreferredSize(new Dimension(1 * (panelWidthToUse / GRID), Sizes.INPUT_FIELD_HEIGHT));
        textfieldTableau.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (textfieldTableau.getText().isEmpty()) {
                    textfieldTableau.setToolTipText(null);
                } else {
                    textfieldTableau.setToolTipText(textfieldTableau.getText());
                }
            }
        });
        tableauWrapper.add(BorderLayout.NORTH, new JLabel(Loc.get("TABLEAU")));
        tableauWrapper.add(BorderLayout.CENTER, textfieldTableau);
        panelRight.add(tableauWrapper);

        panelRightWrapper.add(BorderLayout.EAST, panelRight);
        outerWrapper.add(BorderLayout.EAST, panelRightWrapper);

        JPanel wrapperLogButton = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        labelLog = createLabel("");
        wrapperLogButton.add(labelLog);
        buttonLog = new JButton("------------------");
        buttonLog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Content.setContent(new ContentABIBLocationList(Content.getCurrentContent(), getThis()));
            }
        });
        buttonLog.setEnabled(false);
        buttonLog.setPreferredSize(new Dimension((int) ((panelWidthToUse / GRID) * 1.3), Sizes.INPUT_FIELD_HEIGHT));
        wrapperLogButton.add(buttonLog);
        panelRightWrapper.add(BorderLayout.SOUTH, ComponentHelper.wrapComponent(wrapperLogButton, 10, 0, 0, 0));

        // Barcode Line
//        JPanel barcodeWrapper = new JPanel(new BorderLayout());
//        JPanel barcodeWrapper2 = new JPanel(new FlowLayout());
//        JPanel barcodeTextWrapper = new JPanel(new BorderLayout());
//        textfieldBarcode = new JTextField();
//        textfieldBarcode.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
//        textfieldBarcode.setPreferredSize(new Dimension(2 * (panelWidthToUse / GRID), Sizes.INPUT_FIELD_HEIGHT));
//        textfieldBarcode.addMouseListener(new MouseAdapter() {
//            @Override
//            public void mouseEntered(MouseEvent e) {
//                if (textfieldBarcode.getText().isEmpty()) {
//                    textfieldBarcode.setToolTipText(null);
//                } else {
//                    textfieldBarcode.setToolTipText(textfieldRack.getText());
//                }
//            }
//        });
//        textfieldBarcode.getDocument().addDocumentListener(new DocumentListener() {
//            @Override
//            public void insertUpdate(DocumentEvent e) {
//                try {
//                    barcode.updateBarcode(BarcodeFactory.createCode128(textfieldBarcode.getText()));
//                } catch (BarcodeException e1) {
//                    barcode.updateBarcode(null);
//                }
//            }
//
//            @Override
//            public void removeUpdate(DocumentEvent e) {
//                try {
//                    barcode.updateBarcode(BarcodeFactory.createCode128(textfieldBarcode.getText()));
//                } catch (BarcodeException e1) {
//                    barcode.updateBarcode(null);
//                }
//            }
//
//            @Override
//            public void changedUpdate(DocumentEvent e) {
//                try {
//                    barcode.updateBarcode(BarcodeFactory.createCode128(textfieldBarcode.getText()));
//                } catch (BarcodeException e1) {
//                    barcode.updateBarcode(null);
//                }
//            }
//        });
//        barcodeTextWrapper.add(BorderLayout.NORTH, new JLabel(Loc.get("BARCODE")));
//        barcodeTextWrapper.add(BorderLayout.CENTER, textfieldBarcode);
//
//        barcodeWrapper2.add(barcodeTextWrapper);
//        barcode = BarcodeHelper.getBarcode128("-");
//        barcodeWrapper2.add(barcode);
//        barcodeWrapper.add(BorderLayout.WEST, barcodeWrapper2);
//        outerWrapper.add(BorderLayout.SOUTH, ComponentHelper.wrapComponent(barcodeWrapper, 3));

        setContent(outerWrapper);

        try {
            items = apiControllerAccess.getHashedCodeTableEntries(columnType);

        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(InputComboIdBox.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }

        comboLocation.addAdditionalItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                boolean status = comboLocation.getSelectedInput() != null;
                textfieldRack.setEnabled(status);
                textfieldTableau.setEnabled(status);
                textfieldTray.setEnabled(status);
                if (!status) {
                    textfieldRack.setText("");
                    textfieldTableau.setText("");
                    textfieldTray.setText("");
                }
            }
        });

        // set manual the sidebar
        setSidebar(sidebar);

    }

    @Override
    public void setEntry(UpdateableEntry entry) {
        super.setEntry(entry);
        if (comboLocation != null) {
            comboLocation.setEntry(entry);
        }
    }

    @Override
    public void setEnabledForRights(boolean b) {
        boolean btnEnabled = buttonLog.isEnabled();
        super.setEnabledForRights(b);
        buttonLog.setEnabled(btnEnabled);

    }

    @Override
    public void setEnabled(boolean b) {
        boolean btnEnabled = buttonLog.isEnabled();
        super.setEnabled(b);
        buttonLog.setEnabled(btnEnabled);
    }

}
