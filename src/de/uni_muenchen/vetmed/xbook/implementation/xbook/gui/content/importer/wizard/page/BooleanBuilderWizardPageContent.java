package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.page;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.AbstractLookupTableBuilderWizardPageContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.LookupTableWizardPage;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.LookupTableWizard;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.page.editor.LookupTableEditorContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.page.model.LookupTableModel;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class BooleanBuilderWizardPageContent extends AbstractLookupTableBuilderWizardPageContent {

    protected ColumnType column;

    public BooleanBuilderWizardPageContent(ColumnType column) {
        this.column = column;
    }

    protected void checkCompleteness(LookupTableModel model) {
        setPageComplete(model.isMapComplete());
        ((LookupTableWizard) getWizardPage().getWizard()).getLookupTableManager().setLookupTable(column, model.createLookupTable());
    }

    private LookupTableModel makeTableModel() {
        LookupTableModel model = new LookupTableModel(mainFrame, getName());
        if (getWizardPage() instanceof LookupTableWizardPage) {
            model.addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                    if (e.getSource() instanceof LookupTableModel) {
                        checkCompleteness((LookupTableModel) e.getSource());
                    }
                }
            });
            ArrayList<Map.Entry<String, String>> list = new ArrayList();
            list.add(new AbstractMap.SimpleEntry<>("-1", "--- " + Loc.get("NO_VALUE") + " ---"));
            list.add(new AbstractMap.SimpleEntry<>("0", Loc.get("NO")));
            list.add(new AbstractMap.SimpleEntry<>("1", Loc.get("YES")));
            model.setCodomain(list);

            model.setDomain(((LookupTableWizardPage) getWizardPage()).getColumnValues());

            model.createMap();
        } else {
            model = null;
        }
        return model;
    }

    @Override
    protected JTable makeTable() {
        JTable table = new JTable();

        LookupTableModel model = makeTableModel();
        LookupTableEditorContent editor = new LookupTableEditorContent(getName(), model.getCodomain());

        table.setModel(model);
        table.setRowSorter(new TableRowSorter<TableModel>(model));
        table.getColumnModel().getColumn(model.getEditorColumnIndex()).setCellEditor(editor);

        checkCompleteness(model);

        return table;
    }

    @Override
    public String getTitle() {
        return column.getDisplayName();
    }
}
