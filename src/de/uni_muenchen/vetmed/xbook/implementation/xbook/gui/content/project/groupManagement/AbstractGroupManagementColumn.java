package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.groupManagement;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XSubTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import java.awt.BorderLayout;
import javax.swing.*;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractGroupManagementColumn extends JPanel {

    protected AbstractMainFrame<AbstractController<?,?>> mainFrame;

    public AbstractGroupManagementColumn(AbstractMainFrame mainFrame) {
        super(new BorderLayout());

        this.mainFrame = mainFrame;

        setBackground(Colors.CONTENT_BACKGROUND);
        XSubTitle titleLabel = new XSubTitle(getTitle());
        titleLabel.setOpaque(false);

        JPanel northWrapper = new JPanel(new StackLayout());
        northWrapper.setOpaque(false);
        northWrapper.add(titleLabel);
        northWrapper.add(getDescription());
        northWrapper.add(getButtonBar());

        add(BorderLayout.NORTH, northWrapper);
        add(BorderLayout.CENTER, getContent());
        // add(BorderLayout.SOUTH, getButtonBar());
    }

    protected abstract String getTitle();

    protected abstract JScrollPane getContent();

    protected abstract JPanel getButtonBar();

    protected abstract JComponent getDescription();
}
