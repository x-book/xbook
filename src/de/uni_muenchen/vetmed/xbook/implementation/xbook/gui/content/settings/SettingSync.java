package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.settingPanel.AbstractSettingPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.fields.TimeSelection;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.fields.TwoRadioButtons;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.PlannedSynchronisation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class SettingSync extends AbstractSettingPanel {

    /**
     * The radio buttons object for auto sync.
     */
    private TwoRadioButtons autoSync;
    /**
     * The radio buttons object for timed sync.
     */
    private TwoRadioButtons plannedSync;
    /**
     * The radio buttons object for the setting to sync all projects or only the local ones.
     */
    private TwoRadioButtons plannedSyncSyncAll;
    private TimeSelection plannedSyncTime;
    private AbstractSynchronisationController controller;

    /**
     * Constructor.
     *
     * @param controller The basic controller object.
     */
    public SettingSync(AbstractSynchronisationController controller) {
        this.controller = controller;

        setLayout(new StackLayout());

        // auto-sync
        addTitleBar(Loc.get("AUTO_SYNCHRONISATION") + " *");
        addTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("AUTO_SYNCHRONISATION_SETTING_INFORMATION"));
        autoSync = addTwoRadioButtons(Colors.CONTENT_BACKGROUND, Loc.get("ON"), Loc.get("OFF"));

        // planned sync
        addTitleBar(Loc.get("PLANNED_SYNC") + " *");

        addTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("PLANNED_SYNC_SETTING_INFORMATION"));
        plannedSync = addTwoRadioButtons(Colors.CONTENT_BACKGROUND, Loc.get("YES"), Loc.get("NO"));
        plannedSync.addCustomActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updatePlannedSynchronisationEnability();
            }
        });

        addTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("PLANNED_SYNC_TIMESELECTION_SETTING_INFORMATION"));
        plannedSyncTime = addTimeSelection();

        addTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("PLANNED_SYNC_SYNCALL_SETTING_INFORMATION"));
        plannedSyncSyncAll = addTwoRadioButtons(Colors.CONTENT_BACKGROUND, Loc.get("YES"), Loc.get("NO"));

        addTextBlock("<i><b>*)</b> " + Loc.get("RESTART_THE_APPLICATION_TO_APPLY_CHANGES") + "</i>");

        ComponentHelper.colorAllChildren(this, Colors.CONTENT_BACKGROUND);

        updatePlannedSynchronisationEnability();
    }

    /**
     * Calculates if the additional timed synchronization fields must be enabled or not.
     */
    private void updatePlannedSynchronisationEnability() {
        boolean setEnable = plannedSync.isYesSelected();
        plannedSyncSyncAll.setEnabled(setEnable);
        plannedSyncTime.setEnabled(setEnable);
    }

    @Override
    public void save() {
        // auto-sync
        XBookConfiguration.setBookProperty(XBookConfiguration.AUTOSYNC, autoSync.isYesSelected());
        Footer.setAutoSync(XBookConfiguration.getBookBoolean(XBookConfiguration.AUTOSYNC));

        // planned sync
        XBookConfiguration.setBookProperty(XBookConfiguration.PLANNED_SYNC, plannedSync.isYesSelected());
        XBookConfiguration.setBookProperty(XBookConfiguration.PLANNED_SYNC_HOUR, plannedSyncTime.getHour());
        XBookConfiguration.setBookProperty(XBookConfiguration.PLANNED_SYNC_MINUTE, plannedSyncTime.getMinute());
        XBookConfiguration.setBookProperty(XBookConfiguration.PLANNED_SYNC_SYNC_ALL, plannedSyncSyncAll.isYesSelected());

        Footer.updateTimedButton();

        PlannedSynchronisation ts = controller.getPlannedSynchronisation();
        if (plannedSync.isYesSelected()) {
            ts.setTimer();
        } else {
            ts.stopTimer();
        }
    }

    @Override
    public void reset() {
        // auto-sync
        autoSync.setYesSelected(XBookConfiguration.getBookBoolean(XBookConfiguration.AUTOSYNC));

        // planned sync
        plannedSync.setYesSelected(XBookConfiguration.getBookBoolean(XBookConfiguration.PLANNED_SYNC));
        plannedSyncTime.setHour(XBookConfiguration.getBookInt(XBookConfiguration.PLANNED_SYNC_HOUR));
        plannedSyncTime.setMinute(XBookConfiguration.getBookInt(XBookConfiguration.PLANNED_SYNC_MINUTE));
        plannedSyncSyncAll.setYesSelected(XBookConfiguration.getBookBoolean(XBookConfiguration.PLANNED_SYNC_SYNC_ALL));
        updatePlannedSynchronisationEnability();
    }

    @Override
    public void setDefaultValues() {
        // auto-sync
        autoSync.setYesSelected(true);

        // planned sync
        plannedSync.setYesSelected(false);
        plannedSyncTime.setToDefault();
        plannedSyncSyncAll.setYesSelected(false);
        updatePlannedSynchronisationEnability();
    }
}
