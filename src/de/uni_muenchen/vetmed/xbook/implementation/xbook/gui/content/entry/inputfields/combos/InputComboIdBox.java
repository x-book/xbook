package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.event.CodeTableListener;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboIdBox;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A general object for the OssoBook input fields which contains a combobox to select something. The return value for
 * saving the items to the database are returned as a Integer value.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputComboIdBox extends AbstractInputElement implements IComboIdBox {

    /**
     * The combo box object.
     */
    protected XComboBox<String> combo;
    /**
     * The elements that are selectable in the combo box.
     */
    protected static HashMap<ColumnType, CodeTableHashMap> data = new HashMap<>();
    /**
     * A wrapper panel that holds all elements of this input field.
     */
    protected JPanel wrapperPanel;
    /**
     * <code>true</code> if there should be added an empty String as item.
     */
    private boolean addEmptyEntry;

    public InputComboIdBox(ColumnType columnType) {
        super(columnType);
        getCombo();
    }

    /**
     * Constructor.
     *
     * @param columnType    The database column data of the corresponding input field.
     * @param gridX         The number of grids that are used for the height of the input field.
     * @param gridY         The number of grids that are used for the weight of the input field.
     * @param addEmptyEntry State whether an empty entry should be added to the list or not.
     * @param sidebar       The custom sidebar for this input element. <code>null</code> if no custom sidebar is
     *                      available.
     */
    @Deprecated
    public InputComboIdBox(final ColumnType columnType, int gridX, int gridY, boolean addEmptyEntry,
                           SidebarPanel sidebar) {
        super(columnType);
        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);
        setAddEmptyEntry(addEmptyEntry);
    }

    @Override
    public void load(DataSetOld dataSet) {
        DataRow list = dataSet.getDataRowForTable(getTableName());
        final String value = list.get(columnType);
        if (value != null) {
            combo.setSelectedItem(data.get(columnType).convertIdAsStringToString(value));
        }

        checkIfValueIsValid(value);
    }

    private void checkIfValueIsValid(String valueToCheck) {
        if (mainFrame.getController().getFeatureConfiguration().isPlausibilityCheckAvailable()) {
            if (!valueToCheck.equals("-1")) {
                try {
                    boolean b = apiControllerAccess.isValidCodeID(columnType, valueToCheck);

                    if (!b) {
                        invalidValueLoaded = true;
                        setErrorStyle();
                        JOptionPane.showMessageDialog(InputComboIdBox.this,
                                Loc.get("INVALID_VALUES_DETECTED_DIALOG_MESSAGE", valueToCheck,
                                        columnType.getDisplayName()),
                                Loc.get("INVALID_VALUES_DETECTED"),
                                JOptionPane.WARNING_MESSAGE);
                    }
                } catch (NotLoggedInException e) {
                    mainFrame.displayLoginScreen();
                } catch (StatementNotExecutedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Sets a new set of data to the combo box.
     *
     * @param newData       The data to set.
     * @param addEmptyEntry State whether an empty entry should be added to the list or not.
     */
    protected void setItems(CodeTableHashMap newData, boolean addEmptyEntry) {
        ArrayList<String> allValues = new ArrayList<>();
        if (addEmptyEntry) {
            allValues.add("");
        }
        for (Map.Entry<String, String> hash : newData.entrySet()) {
            allValues.add(hash.getValue());
        }
        Collections.sort(allValues, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.toUpperCase().compareTo(o2.toUpperCase());
            }
        });
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(allValues.toArray(new String[0]));
        combo.setModel(model);

        // should not clear here, only set new items. Clearing only interesting, when selected value is not
        // available
        // anymore???
        // clear();
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        // if no item was selected, do nothing
        if (getSelectedId() == null) {
            list.add(new DataColumn("-1", columnType.getColumnName()));
        } else {
            list.add(new DataColumn(getSelectedId() + "", columnType.getColumnName()));
        }
    }

    @Override
    public String getStringRepresentation() {
        if (getSelectedId() == null) {
            return "";
        }
        return "" + combo.getSelectedItem();
    }

    public String getRawData() {
        if (getSelectedId() == null) {
            return "-1";
        }
        return "" + getSelectedId();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(combo);
        comp.add(combo.getEditor().getEditorComponent());
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !((String) combo.getSelectedItem()).isEmpty();
    }

    protected CodeTableHashMap getData() {
        try {
            if (!data.containsKey(columnType)) {
                data.put(columnType, apiControllerAccess.getHashedCodeTableEntries(columnType));
            }
            return data.get(columnType);

        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(InputComboIdBox.class.getName()).log(Level.SEVERE, null, ex);
            return new CodeTableHashMap();
        }
    }

    private void reloadAvailableValues() {
        Object savedValue = combo.getSelectedItem();
        CodeTableHashMap items = getData();
        combo.removeAllItems();

        ArrayList<String> listArray = new ArrayList<>();
        if (addEmptyEntry) {
            listArray.add("");
        }
        listArray.addAll(items.values());
        combo.setItems(listArray);

        combo.setSelectedItem(savedValue);
    }

    protected String getValueForInputID(String id) {
        return data.get(columnType).get(id);
    }

    @Override
    protected void createFieldInput() {
        wrapperPanel = new JPanel();
        wrapperPanel.setLayout(new BorderLayout());

        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        combo = getCombo();

        //only outosort if there is no sort column
        combo.setAutoSort(!columnType.isSort());

        combo.setCustomInputEnabled(false);
        combo.setIntegerInputEnabled(true);
        reloadAvailableValues();

        combo.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        combo.setSelectedItem("");
        combo.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (combo.getSelectedItem() == null || combo.getSelectedItem().toString().isEmpty()) {
                    combo.setToolTipText(null);
                } else {
                    combo.setToolTipText(combo.getSelectedItem().toString());
                }
            }
        });
        combo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
//                    actionOnFocusLost();
                }
            }
        });
        multiPanel.add(BorderLayout.CENTER, combo);
        wrapperPanel.add(BorderLayout.CENTER, multiPanel);
        setContent(wrapperPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }

        //
        apiControllerAccess.addCodeTableEventListener(new CodeTableListener() {
            @Override
            public void onCodeTablesUpdated() {
                reloadAvailableValues();
            }
        });
    }

    // ========================================================================
    @Override
    public void setAddEmptyEntry(boolean addEmptyEntry) {
        this.addEmptyEntry = addEmptyEntry;
    }

    @Override
    public String getSelectedValue() {
        return (String) combo.getSelectedItem();
    }

    @Override
    public String getSelectedId() {
        if (combo.getSelectedItem() == null || combo.getSelectedItem().equals("")) {
            return null;
        }
        return data.get(columnType).convertStringToIdAsString(getSelectedValue());
    }

    @Override
    public void setSelectedValue(String value) {
        if (value != null) {
            combo.setSelectedItem(value);
        }
    }

    @Override
    public void setSelectedId(String id) {
        if (id != null) {
            combo.setSelectedItem(data.get(columnType).convertIdAsStringToString(id));
        }
    }

    @Override
    public void clear() {
        invalidValueLoaded = false;
        setDefaultStyle(true);
        reloadAvailableValues();
        combo.setSelectedItem("");
    }

    public XComboBox<String> getCombo() {
        if (combo == null) {
            combo = new XComboBox() {
                @Override
                protected void actionOnFocusLost(JTextField tf) {
                    if (isIntegerInputEnabled()) {
                        String input = tf.getText();
                        try {
                            //check if value is an integer
                            Integer.parseInt(input);
                            String value = getValueForInputID(input);
                            if (value == null || value.isEmpty()) {
                                setErrorStyle();
                                Footer.displayWarning(Loc.get("ID_NOT_FOUND"));
                            }
                            tf.setText(value);
                            return;
                        } catch (NumberFormatException nfe) {
                            // do nothing
                        }
                    }
                    super.actionOnFocusLost(tf);
                }

            };
        }

        return combo;
    }

}
