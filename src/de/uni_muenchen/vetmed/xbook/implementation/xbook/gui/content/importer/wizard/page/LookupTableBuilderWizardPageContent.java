package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.page;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.LookupTableWizardPage;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.page.editor.LookupTableEditorContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.page.model.LookupTableModel;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.AbstractLookupTableBuilderWizardPageContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.LookupTableWizard;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map.Entry;

public class LookupTableBuilderWizardPageContent extends AbstractLookupTableBuilderWizardPageContent {

    protected ColumnType column;

    public LookupTableBuilderWizardPageContent(ColumnType column) {
        this.column = column;
    }

    @Override
    public String getName() {
        return column.getDisplayName();
    }

    public String getTableName() {
        return column.getConnectedTableName();
    }

    protected void checkCompleteness(LookupTableModel model) {
        setPageComplete(model.isMapComplete());
        ((LookupTableWizard) getWizardPage().getWizard()).getLookupTableManager().setLookupTable(column, model.createLookupTable());
    }

    private LookupTableModel makeTableModel() {
        LookupTableModel model = new LookupTableModel(mainFrame, getName());
        if (getWizardPage() instanceof LookupTableWizardPage) {

            try {
                model.addTableModelListener(new TableModelListener() {
                    @Override
                    public void tableChanged(TableModelEvent e) {
                        if (e.getSource() instanceof LookupTableModel) {
                            checkCompleteness((LookupTableModel) e.getSource());
                        }
                    }
                });
                ArrayList<Entry<String,String>> list = new ArrayList((mainFrame.getController()).getHashedCodeTableEntries(column).entrySet());
                list.add(new AbstractMap.SimpleEntry<>("-1", "--- " + Loc.get("NO_VALUE") + " ---"));
                model.setCodomain(list);

                model.setDomain(((LookupTableWizardPage) getWizardPage()).getColumnValues());

                model.createMap();
            } catch (StatementNotExecutedException | NotLoggedInException e1) {
                model = null;
            }
        } else {
            model = null;
        }
        return model;
    }

    @Override
    protected JTable makeTable() {
        JTable table = new JTable();

        LookupTableModel model = makeTableModel();
        LookupTableEditorContent editor = new LookupTableEditorContent(getName(), model.getCodomain());

        table.setModel(model);
        table.setRowSorter(new TableRowSorter<TableModel>(model));
        table.getColumnModel().getColumn(model.getEditorColumnIndex()).setCellEditor(editor);

        checkCompleteness(model);

        return table;
    }

    @Override
    public String getTitle() {
        return column.getDisplayName();
    }
}
