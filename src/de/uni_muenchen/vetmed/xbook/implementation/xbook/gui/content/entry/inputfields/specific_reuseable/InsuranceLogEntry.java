package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.specific_reuseable;

/**
 *
 * @author kaltenthaler
 */
public class InsuranceLogEntry {

    private final String valuation;
    private final String currency;
    private final String date;

    public InsuranceLogEntry(String valuation, String currency, String date) {
        this.valuation = valuation;
        this.currency = currency;
        this.date = date;
    }

    public String getValuation() {
        return valuation;
    }

    public String getCurrency() {
        return currency;
    }

    public String getDate() {
        return date;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof InsuranceLogEntry)) {
            return false;
        }
        InsuranceLogEntry obj1 = (InsuranceLogEntry) obj;
        return obj1.valuation.equals(valuation)
                && obj1.currency.equals(currency)
                && obj1.date.equals(date);
    }

    @Override
    public int hashCode() {
        return valuation.hashCode() + currency.hashCode() + date.hashCode();
    }

}
