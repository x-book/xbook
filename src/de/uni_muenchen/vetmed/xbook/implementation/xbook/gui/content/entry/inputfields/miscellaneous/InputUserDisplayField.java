package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IUserDisplayField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputTextField;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputUserDisplayField extends InputTextField implements IUserDisplayField {

    /**
     * The ID of the user.
     */
    private String userId;

    /**
     * The display name of the user.
     */
    private String userDisplayName;

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     *                   field.
     */
    public InputUserDisplayField(ColumnType columnType) {
        super(columnType);
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        list.add(new DataColumn(userId, columnType));
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        userId = list.get(columnType);
        String name = userId;
        try {
            name = apiControllerAccess.getDisplayName(Integer.valueOf(userId));
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(InputUserDisplayField.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NumberFormatException ex) {
            // nothing to do
        }
        zField.setText(name);
    }

    @Override
    public void reload() {
        if (userId == null) {
            clear();
        } else if (userDisplayName != null) {
            zField.setText(userDisplayName);
        } else {
            zField.setText(userId);
        }
    }

    @Override
    public void clear() {
        try {
            userId = apiControllerAccess.getUserID() + "";
            userDisplayName = apiControllerAccess.getDisplayName();
            zField.setText(userDisplayName);
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(InputUserDisplayField.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();

        zField.setEnabled(false);
        checkRememberValue.setSelected(false);
        checkRememberValue.setEnabled(false);
    }

    // ========================================================================
    @Override
    public String getUserDisplayName() {
        return userDisplayName;
    }

    @Override
    public String getUserId() {
        return userId;
    }

}
