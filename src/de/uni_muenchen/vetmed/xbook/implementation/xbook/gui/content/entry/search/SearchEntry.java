package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.search;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType.ExportType;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryKey;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportResult;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.SearchEntryInfo;
import de.uni_muenchen.vetmed.xbook.api.datatype.SearchEntryInfo.SearchMode;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.table.CustomTableModel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.table.TableColumnAdjuster;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTable;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel.Position;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.InterfaceAddableData;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing.Listing;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class SearchEntry extends AbstractContent {

  private AbstractContent previousPanel;
  private InterfaceAddableData addablePanel;
  private XTable table;
  private JScrollPane sp2;
  private final ArrayList<AbstractSearchPanelLine> lines = new ArrayList<>();
  private ExportResult result;
  private JCheckBox checkShowDatabaseInfo = null;
  private BaseEntryManager manager;

  /**
   * Constructor.
   *
   * @param columnTypes The column types which shall searchable and be displayed as an input row.
   */
  public SearchEntry(ColumnType... columnTypes) {
    super();
    for (ColumnType c : columnTypes) {
      lines.add(new SearchPanelLineText(c, getAvailableSearchModes()));
    }
    init();
  }

  public void setPreviousPanel(AbstractContent previousPanel) {
    this.previousPanel = previousPanel;
  }

  public void setAddablePanel(InterfaceAddableData addablePanel) {
    this.addablePanel = addablePanel;
  }

  public void setManager(BaseEntryManager manager) {
    this.manager = manager;
  }

  @Override
  protected JPanel getContent() {
    JPanel outerWrapper = new JPanel(new BorderLayout());
    outerWrapper.setBackground(Colors.CONTENT_BACKGROUND);

    JPanel topWrapper = new JPanel(new BorderLayout());
    topWrapper.setBackground(Colors.CONTENT_BACKGROUND);

    MultiLineTextLabel mltl = new MultiLineTextLabel(Loc.get("CROSSED_LINKED_SEARCH_INSTRUCTION"));
    mltl.setBackground(Colors.CONTENT_BACKGROUND);
    topWrapper.add(BorderLayout.NORTH,
        ComponentHelper.wrapComponent(mltl, Colors.CONTENT_BACKGROUND, 0, 100, 4,
            154));
    topWrapper.add(BorderLayout.WEST, createSearchFields());

    JPanel buttonWrapper = new JPanel(new BorderLayout());
    buttonWrapper.setBackground(Colors.CONTENT_BACKGROUND);
    JPanel buttonWrapper2 = new JPanel(new BorderLayout());
    buttonWrapper2.setBackground(Colors.CONTENT_BACKGROUND);
    XImageButton searchButton = new XImageButton(Images.BUTTONPANEL_SEARCH,
        Sizes.MODIFIER_BIG_BUTTON);
    searchButton.setToolTipText(Loc.get("SEARCH"));
    searchButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        loadDataToTable();
      }
    });
    buttonWrapper2.add(BorderLayout.WEST, searchButton);
    buttonWrapper.add(BorderLayout.NORTH, buttonWrapper2);
    topWrapper.add(BorderLayout.CENTER,
        ComponentHelper.wrapComponent(buttonWrapper, Colors.CONTENT_BACKGROUND, 2
            , 0, 4, 10));

    outerWrapper.add(BorderLayout.NORTH, topWrapper);

    JPanel tableWrapper = new JPanel(new BorderLayout());
    tableWrapper.setBackground(Colors.CONTENT_BACKGROUND);
    createTable();
    sp2 = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
        JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
    sp2.setBackground(Colors.CONTENT_BACKGROUND);
    tableWrapper.add(BorderLayout.CENTER, sp2);
    outerWrapper.add(BorderLayout.CENTER, tableWrapper);

    return outerWrapper;
  }

  private void addAndBack() {
    int[] rowIds = table.getSelectedRows();
    if (rowIds.length == 0) {
      Footer.displayWarning(Loc.get("NO_ENTRY_SELECTED"));
      return;
    }
    ArrayList<Key> keys = new ArrayList<>();
    for (int i : rowIds) {
      keys.add(result.getKeyAt(table.getRowSorter().convertRowIndexToModel(i)));
    }
    addablePanel.addData(keys);
    Content.setContent(previousPanel);
  }

  protected ExportResult searchData(ProjectDataSet project,
      HashMap<ColumnType, SearchEntryInfo> filter)
      throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
    return manager.getEntries(project, null, ExportType.CROSS_LINKED, false, 0, 0, filter, true);
  }

  private void loadDataToTable() {
    final Map<? extends ColumnType, SearchEntryInfo> defaultFilter = getDefaultFilter();
    HashMap<ColumnType, SearchEntryInfo> filter = new HashMap<>();
    ExportResult loadedResult;
    result = new ExportResult();
    try {
      ProjectDataSet project = mainFrame.getController().getCurrentProject();
      for (AbstractSearchPanelLine line : lines) {
        if (!line.getInput().getText().isEmpty()) {
          filter.put(line.getColumnType(), line.getInput());
        }
      }

      if (defaultFilter != null) {
        filter.putAll(defaultFilter);
      }

      final HashMap<ColumnType, SearchEntryInfo> filter1 = new HashMap<>();
      loadedResult = searchData(project, filter1);
    } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException | NoRightException ex) {
      Logger.getLogger(SearchEntry.class.getName()).log(Level.SEVERE, null, ex);
      loadedResult = new ExportResult();
    }

    result.setHeadlines(loadedResult.getHeadlines());
    final TreeMap<ColumnType, String> headlines = loadedResult.getHeadlines();
    String[] head = headlines.values().toArray(new String[0]);
    DefaultTableModel model = new CustomTableModel();
    model.setColumnIdentifiers(head);

    final LinkedHashMap<EntryKey, ExportRow> entries = loadedResult.getEntries();
    for (Entry<EntryKey, ExportRow> entry : entries.entrySet()) {
      ExportRow value = entry.getValue();
      boolean hasAllKeys = true;
      if (defaultFilter != null) {
        //if filter is for cross linked field, this may not return a value.
        //therefore make sure that if a value is expected at least one of the keys is returned
        //and the column type of that column is not NONE!!
        for (ColumnType columnType : defaultFilter.keySet()) {
          if (columnType.getExportType() != ExportType.NONE && !value.containsKey(columnType)) {
            hasAllKeys = false;
            break;
          }
        }
      }
      if (!hasAllKeys) {
        continue;
      }
      boolean entrysFiltered = false;

      for (Entry<ColumnType, SearchEntryInfo> columnTypeSearchEntryInfoEntry : filter
          .entrySet()) {
        final String value1 = value.getValue(columnTypeSearchEntryInfoEntry.getKey());
        final SearchEntryInfo value2 = columnTypeSearchEntryInfoEntry.getValue();
        switch (value2.getCurrentSearchMode()) {
          case CONTAINS: {
            if (!value1.toLowerCase().contains(value2.getText().toLowerCase())||(value1.equals("-1")&&!value2.getText().contains("-"))) {

              entrysFiltered = true;
              break;
            }
            break;
          }
          case EQUALS:
            if (!value1.equalsIgnoreCase(value2.getText())) {
              entrysFiltered = true;
              break;
            }
        }
        if (entrysFiltered) {
          break;
        }
      }
      if (entrysFiltered) {
        continue;
      }

      result.addEntry(entry );


      Vector<String> rowData = new Vector<>();

      for (Map.Entry<ColumnType, String> headline : headlines.entrySet()) {
        ColumnType key = headline.getKey();
        if (value.containsKey(key)) {
          final String stringData = value.get(key).getStringData();
          if (stringData == null || stringData.equals("null") || stringData.equals("-1")
              || stringData.equals("-1.0")) {
            rowData.add("");
          } else {
            rowData.add(stringData);
          }
        } else {
          rowData.add("");
        }
      }
      model.addRow(rowData);
    }

    table.setModel(model);
    Listing.refreshColumnVisibility(table);
    adjustColumns(table);
  }

  protected Map<? extends ColumnType, SearchEntryInfo> getDefaultFilter() {
    return null;
  }

  private void createTable() {

    table = new XTable() {
      @Override
      public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        try {
          Component c = super.prepareRenderer(renderer, row, column);

          // colorize the single rows
          if (!isRowSelected(row)) {
            c.setBackground(row % 2 == 0 ? null : Colors.CONTENT_BACKGROUND);
          }

          return c;
        } catch (NullPointerException ex) {
          return null;
        }
      }
    };
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    table.setShowHorizontalLines(false);
    table.setShowVerticalLines(false);
    table.setGridColor(Color.GRAY);
    table.setIntercellSpacing(new Dimension(0, 1));
    table.setRowHeight(table.getRowHeight() + 4);
    table.setAutoCreateRowSorter(true);
    table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    table.setRowSelectionAllowed(true);
    table.addMouseListener(getAndSetMouseListener());
    if (sp2 != null) {
      sp2.setViewportView(table);
    }
    revalidate();
    repaint();
    adjustColumns(table);
  }

  /**
   * Sets the preferred width of the visible column specified by vColIndex. The column will be just
   * wide enough to show the column head and the widest cell in the column. margin pixels are added
   * to the left and right (resulting in an additional width of 2*margin pixels).
   */
  public static void adjustColumns(JTable table) {
    (new TableColumnAdjuster(table)).adjustColumns();
  }

  private MouseListener getAndSetMouseListener() {
    return new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent event) {
        if (event.getSource() == table) {
          if (event.getClickCount() == 2) {
            addAndBack();
          }
        }
      }
    };
  }

  private JPanel createSearchFields() {
    JPanel p = new JPanel(new StackLayout());
    p.setBackground(Colors.CONTENT_BACKGROUND);
    for (AbstractSearchPanelLine pl : lines) {
      p.add(pl);
    }
    return p;
  }

  public void clearFilter() {
    for (AbstractSearchPanelLine line : lines) {
      line.clearInput();
    }
    result = new ExportResult();
    loadDataToTable();
    DefaultTableModel model = new CustomTableModel();
    table.setModel(model);
  }

  @Override
  public ButtonPanel getButtonBar() {
    ButtonPanel panel = new ButtonPanel();
    panel.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_BACK, Loc.get("BACK"),
        new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            Content.setContent(previousPanel);
          }
        });

    if (checkShowDatabaseInfo == null) {
      checkShowDatabaseInfo = new JCheckBox(Loc.get("SHOW_DB_INFO"),
          XBookConfiguration
              .getListingShowDBInfoProperty(mainFrame.getController().getPreferences()));
      checkShowDatabaseInfo.setBackground(Colors.CONTENT_BACKGROUND);
      checkShowDatabaseInfo.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          XBookConfiguration
              .setListingShowDBInfoProperty(mainFrame.getController().getPreferences(),
                  checkShowDatabaseInfo.isSelected());
          loadDataToTable();
        }
      });
    }
    panel.getButtonsLeftNorth().add(checkShowDatabaseInfo);

    panel.addImageButton(Position.NORTH_EAST, Images.BUTTONPANEL_ADD, Loc.get("ADD_AND_BACK"),
        Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            addAndBack();
          }
        });
    return panel;
  }

  @Override
  public SidebarPanel getSideBar() {
    return null;
  }

  @Override
  public boolean forceSidebar() {
    return false;
  }

  @Override
  public void setFocus() {
  }

  @Override
  public void actionOnDisconnect() {
  }

  /**
   * Returns the available search modes that should be enabled in the search mask.
   *
   * @return A list containing the available search modes.
   */
  public ArrayList<SearchMode> getAvailableSearchModes() {
    ArrayList<SearchMode> modes = new ArrayList<>();
    modes.add(SearchMode.CONTAINS);
    return modes;
  }
}
