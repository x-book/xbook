package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.ITTable;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawITTable;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawTextField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IITTable;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.ITextField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputITTable extends AbstractDefaultInputElement implements IITTable {

    protected RawITTable zField;

    protected InputITTable() {
        super(Mode.BLANK, ColumnType.ITTABLE);
    }

    public ITTable getTable() {
        return zField.getTable();
    }

    public static InputITTable createTable(ITTable table) {
        InputITTable inputITTable = new InputITTable();
        inputITTable.zField = new RawITTable(table);
        return inputITTable;
    }

    @Override
    public AbstractInputElementFunctions createInputField() {
//        if (zField == null) {
//            zField = new RawITTable();
//        }
        return zField;
    }

    @Override
    protected void calculateSize() {
        //no please!
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return zField.getMyFocusableComponents();
    }
}
