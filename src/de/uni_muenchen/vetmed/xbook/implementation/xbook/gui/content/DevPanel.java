package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content;


import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableArrayList;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInt;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableString;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractProjectManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.adminPanel.AdminPanel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.database_viewer.DatabaseViewer;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.test.TestConflictContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class DevPanel extends AbstractContent {

  private static final Log LOGGER = LogFactory.getLog(DevPanel.class);

  public DevPanel() {
    super();
    init();
  }

  @Override
  protected JPanel getContent() {
    JPanel wrapper = new JPanel(new StackLayout());
    wrapper.setBackground(Colors.CONTENT_BACKGROUND);
    wrapper.add(new XTitle("Development Panel"));

    // ---------------------------------------------------------------
    JButton buttonSendMessage = new JButton("Send Message");
    wrapper.add(buttonSendMessage);
    buttonSendMessage.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          Message r = ((AbstractSynchronisationController) mainFrame.getController())
              .sendMessage(Commands.COMMAND, new SerialisableInt(Commands.MESSAGE.ordinal()));
          System.out.println(r.getResult());
        } catch (NotConnectedException ex) {
          Logger.getLogger(DevPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    });

    // ---------------------------------------------------------------
    JButton buttonPrintColumnTypeValues = new JButton("Update Language");
    wrapper.add(buttonPrintColumnTypeValues);
    buttonPrintColumnTypeValues.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          ((AbstractSynchronisationController) mainFrame.getController()).updateLanguageTables();
        } catch (NotLoggedInException | StatementNotExecutedException | NotConnectedException | IOException ex) {
          Logger.getLogger(DevPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    });

    // ---------------------------------------------------------------
    JButton adminPanel = new JButton("Admin Panel");
    wrapper.add(adminPanel);
    adminPanel.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
//                try {
        Content.setContent(new AdminPanel());
//                } catch (NotLoggedInException | StatementNotExecutedException | NotConnectedException | IOException ex) {
//                    Logger.getLogger(DevPanel.class.getName()).log(Level.SEVERE, null, ex);
//                }
      }
    });

//    JButton testFxPanel3 = new JButton("Test FX Panel (Toolset 3)");
//    wrapper.add(testFxPanel3);
//    testFxPanel3.addActionListener(new ActionListener() {
//      @Override
//      public void actionPerformed(ActionEvent e) {
////        mainFrame.displayTestFxPanelScreen3();
//      }
//    });

    // ---------------------------------------------------------------
    JButton testConflicts = new JButton("Test Conflicts");
    wrapper.add(testConflicts);
    testConflicts.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        Content.setContent(new TestConflictContent());
      }
    });
    // ---------------------------------------------------------------
    JButton testProjectConflict = new JButton("Test Conflicts");
    wrapper.add(testProjectConflict);
    testProjectConflict.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        DataSetOld ds = new DataSetOld(new Key(1, 1799), "", "");
        AbstractProjectManager projectManager = null;
        try {
          projectManager = ((AbstractController) mainFrame.getController()).getLocalManager()
              .getProjectManager();
          DataSetOld nextUncomittedEntry = projectManager.getNextUncomittedEntry(ds);
          projectManager.setConflicted(nextUncomittedEntry);
//                    AbstractInputUnitManager abstractInputUnitManager = ((AbstractController) mainFrame.getController()).getLocalManager().getAbstractInputUnitManager();
//                    EntryDataSet nextUncomittedEntry = abstractInputUnitManager.getNextUncomittedEntry(ds);
//                    abstractInputUnitManager.setConflicted(nextUncomittedEntry);
//                    projectManager = abstractInputUnitManager

        } catch (NotLoggedInException e1) {

        } catch (StatementNotExecutedException e1) {
          e1.printStackTrace();
        } catch (NoRightException e1) {
          e1.printStackTrace();
        } catch (IOException e1) {
          e1.printStackTrace();
        }
      }
    });

    // ---------------------------------------------------------------
    JButton serverSearch = new JButton("Server Search");
    wrapper.add(serverSearch);
    serverSearch.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        mainFrame.displayServerSearchScreen();
      }
    });

    // ---------------------------------------------------------------
    JButton databaseViewer = new JButton("Database Viewer");
    wrapper.add(databaseViewer);
    databaseViewer.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          Content.setContent(new DatabaseViewer());
        } catch (NotLoggedInException e1) {
          e1.printStackTrace();
        }
      }
    });
    // ---------------------------------------------------------------
    JButton imageUpload = new JButton("Image upload test");
    wrapper.add(imageUpload);
    imageUpload.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          AbstractSynchronisationController<?, ?> controller = (AbstractSynchronisationController<?, ?>) mainFrame
              .getController();

          final JFileChooser jFileChooser = new JFileChooser();
          final int i = jFileChooser.showOpenDialog(DevPanel.this);
          if (i != JFileChooser.APPROVE_OPTION) {
            return;
          }
          final File selectedFile = jFileChooser.getSelectedFile();

          final BufferedImage image = ImageIO.read(selectedFile);
          ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
          ImageIO.write(image, "png", outputStream);
          final byte[] bytes = outputStream.toByteArray();
          String encoded = DatatypeConverter.printBase64Binary(bytes);
//

          final Message message = new Message(Commands.TEST_UPLOAD, encoded);

          final Message response = controller.sendMessage(message);
          LOGGER.debug(response);
          LOGGER.debug("write successfull");

        } catch (NotLoggedInException | NotConnectedException | IOException e1) {
          e1.printStackTrace();
        }
      }
    });
    // ---------------------------------------------------------------
    JButton imageDownload = new JButton("Image download test");
    wrapper.add(imageDownload);
    imageDownload.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        try {

          final Message message = new Message(Commands.TEST_DOWNLOAD);

          AbstractSynchronisationController controller = (AbstractSynchronisationController) mainFrame
              .getController();
          final Message response = controller.sendMessage(message);
          final SerialisableString serialisable = (SerialisableString) response.getData().get(0);
          final String string = serialisable.getString();
          final byte[] bytes = DatatypeConverter.parseBase64Binary(string);
          ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
          ImageFrame frame = new ImageFrame(inputStream);
          frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
          frame.setVisible(true);

          LOGGER.debug("read successfull");
        } catch (NotLoggedInException | NotConnectedException | IOException e1) {
          e1.printStackTrace();
        }
      }
    }); // ---------------------------------------------------------------
    JButton directoryScan = new JButton("Test Scan");
    wrapper.add(directoryScan);
    directoryScan.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        try {

          final Message message = new Message(Commands.TEST_SCAN);

          AbstractSynchronisationController controller = (AbstractSynchronisationController) mainFrame
              .getController();
          final Message response = controller.sendMessage(message);
          final ArrayList<Serialisable> data = response.getData();

          for (Serialisable datum : data) {
            LOGGER.debug(datum);
          }

          LOGGER.debug("read successfull");
        } catch (NotLoggedInException | NotConnectedException | IOException e1) {
          e1.printStackTrace();
        }
      }
    });
    // ---------------------------------------------------------------
    JButton file_download = new JButton("File Download");
    wrapper.add(file_download);
    file_download.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        try {

          AbstractSynchronisationController controller = (AbstractSynchronisationController) mainFrame
              .getController();
          final String filename = "5";
          controller.downloadAndOpenFile(filename, new Key(1, 1544));
        } catch (NotLoggedInException | NotConnectedException | IOException | StatementNotExecutedException | NoRightException e1) {
          LOGGER.error(e1, e1);
        }
      }
    });// ---------------------------------------------------------------
    JButton file_downloadAll = new JButton("Archive Data");
    wrapper.add(file_downloadAll);
    file_downloadAll.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        try {

          AbstractSynchronisationController controller = (AbstractSynchronisationController) mainFrame
              .getController();
          controller.archiveFiles();
        } catch (NotLoggedInException | NotConnectedException | IOException | StatementNotExecutedException | NoRightException e1) {
          LOGGER.error(e1, e1);
        }
      }
    });// ---------------------------------------------------------------
    JButton testNEwProjectListing = new JButton("TEST PROJECT LISTING");
    wrapper.add(testNEwProjectListing);
    testNEwProjectListing.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        AbstractSynchronisationController<?,?> controller = (AbstractSynchronisationController) mainFrame
            .getController();

        Message ms = null;
        try {
          final Date date = new Date();

          LOGGER.debug("START REQUEST");
          ms = (controller).sendMessage(Commands.REQUEST_PROJECT_LIST);

          if (ms.getResult().wasSuccessful()) {

            final ArrayList<Serialisable> data = ms.getData();

            for (Serialisable datum : data) {
              ProjectDataSet projectDataSet = (ProjectDataSet) datum;

              Message message = new Message(Commands.REQUEST_NUMBER_OF_ENTRIES);
              message.getData().add(projectDataSet.getProjectKey());
              SerialisableArrayList<SerialisableString> list = new SerialisableArrayList<>();
              for (IBaseManager iBase : controller.getLocalManager().getSyncTables()) {
                list.add(new SerialisableString(iBase.getTableName()));
              }
              message.getData().add(list);
              Message returnMessage = controller.sendMessage(
                  message);
            }



          }

          final Date endDate = new Date();
          LOGGER.debug("TOTAL TIME: " + (endDate.getTime() - date.getTime()));




        } catch (NotConnectedException | NotLoggedInException | IOException ex) {
          ex.printStackTrace();
        }

      }
    });// ---------------------------------------------------------------
    JButton testNEwProjectListingNew = new JButton("TEST PROJECT LISTING NEW");
    wrapper.add(testNEwProjectListingNew);
    testNEwProjectListingNew.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        AbstractSynchronisationController<?,?> controller = (AbstractSynchronisationController) mainFrame
            .getController();

        Message ms = null;
        try {
          final Date date = new Date();

          LOGGER.debug("START REQUEST");


          Message message = new Message(Commands.REQUEST_PROJECT_LIST_WITH_INFO);
          SerialisableArrayList<SerialisableString> list = new SerialisableArrayList<>();
          for (IBaseManager iBase : controller.getLocalManager().getSyncTables()) {
            list.add(new SerialisableString(iBase.getTableName()));
          }
          message.getData().add(list);

          ms = (controller).sendMessage(message);

          if (ms.getResult().wasSuccessful()) {
            LOGGER.debug("SUCCESS");

          }

          final Date endDate = new Date();
          LOGGER.debug("TOTAL TIME: " + (endDate.getTime() - date.getTime()));




        } catch (NotConnectedException | NotLoggedInException | IOException ex) {
          ex.printStackTrace();
        }

      }
    });// ---------------------------------------------------------------
    JButton testsendmailToProjectOwner = new JButton("Send mail to project Owner");
    wrapper.add(testsendmailToProjectOwner);
    testsendmailToProjectOwner.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        try {

          AbstractSynchronisationController controller = (AbstractSynchronisationController) mainFrame
              .getController();
          Message message = controller.sendMessageToProjectOwner("test", "hello", "hello!");
          if (!message.wasSuccessfull()) {
            Footer.displayError(message.getResult().getErrorMessage());
          } else {
            Footer.displayConfirmation(Loc.get("SUCCESS"));
          }
        } catch (NotLoggedInException | NotConnectedException | IOException | StatementNotExecutedException | NoRightException | NotLoadedException e1) {
          LOGGER.error(e1, e1);
        }
      }
    });
    return wrapper;

  }

  @Override
  public ButtonPanel getButtonBar() {
    return null;
  }

  @Override
  public SidebarPanel getSideBar() {
    return null;
  }

  @Override
  public boolean forceSidebar() {
    return false;
  }


  class ImageFrame extends JFrame {

    public ImageFrame(ByteArrayInputStream inputStream) {
      setTitle("ImageTest");
      setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

      ImageComponent component = new ImageComponent(inputStream);
      add(component);

    }

    public static final int DEFAULT_WIDTH = 300;
    public static final int DEFAULT_HEIGHT = 200;
  }


  class ImageComponent extends JComponent {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Image image;

    public ImageComponent(ByteArrayInputStream inputStream) {
      try {
        image = ImageIO.read(inputStream);

      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    public void paintComponent(Graphics g) {
      if (image == null) {
        return;
      }
      int imageWidth = image.getWidth(this);
      int imageHeight = image.getHeight(this);

      g.drawImage(image, 50, 50, this);

      for (int i = 0; i * imageWidth <= getWidth(); i++) {
        for (int j = 0; j * imageHeight <= getHeight(); j++) {
          if (i + j > 0) {
            g.copyArea(0, 0, imageWidth, imageHeight, i * imageWidth, j * imageHeight);
          }
        }
      }
    }

  }
}
