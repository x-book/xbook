package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IIntegerField;
import de.uni_muenchen.vetmed.xbook.api.gui.documentFilter.IntegerDocumentFilter;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * An integer field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputIntegerField extends AbstractInputElement implements IIntegerField {

    /**
     * The text field object.
     */
    protected JTextField textField;
    /**
     * The default value.
     */
    private Integer defaultValue;

    public InputIntegerField(ColumnType columnType) {
        super(columnType);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input field.
     * @param gridX      The number of grids that are used for the height of the input field.
     * @param gridY      The number of grids that are used for the weight of the input field.
     * @param sidebar    The custom sidebar for this input element.
     *                   <code>null</code> if no custom sidebar is available.
     */
    @Deprecated
    public InputIntegerField(ColumnType columnType, int gridX, int gridY, SidebarPanel sidebar) {
        super(columnType);

        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);
    }

    /**
     * Constructor.
     *
     * @param columnType  The database column data of the correspondenting input field.
     * @param gridX       The number of grids that are used for the height of the input field.
     * @param gridY       The number of grids that are used for the weight of the input field.
     * @param sidebarText The sidebar text for this input element.
     */
    @Deprecated
    public InputIntegerField(ColumnType columnType, int gridX, int gridY, String sidebarText) {
        this(columnType, gridX, gridY, (new SidebarEntryField(columnType, sidebarText)));
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input field.
     * @param gridX      The number of grids that are used for the height of the input field.
     * @param gridY      The number of grids that are used for the weight of the input field.
     */
    @Deprecated
    public InputIntegerField(ColumnType columnType, int gridX, int gridY) {
        this(columnType, gridX, gridY, (SidebarPanel) null);
    }

    public void setDefaultValue(Integer value) {
        defaultValue = value;
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        String value = list.get(columnType);
        if (value == null || value.equals("-1") || value.equals("-1.0")) {
            textField.setText("");
        } else {
            textField.setText(value);
        }
    }

    @Override
    public void clear() {
        if (defaultValue == null) {
            textField.setText("");
        } else {
            textField.setText("" + defaultValue);
        }
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        if (textField.getText().isEmpty()) {
            list.add(new DataColumn("-1", columnType.getColumnName()));
        } else {
            list.add(new DataColumn(textField.getText(), columnType.getColumnName()));
        }
    }

    @Override
    public String getStringRepresentation() {
        if (textField.getText().isEmpty()) {
            return "-1";
        }
        return "" + textField.getText();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textField);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !textField.getText().isEmpty();
    }


    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        textField = new JTextField();
        textField.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
        textField.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));

        PlainDocument document = (PlainDocument) textField.getDocument();
        document.setDocumentFilter(new IntegerDocumentFilter());

        textField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent event) {
                if (!(textField.getText().isEmpty())) {
                    try {
                        Integer.parseInt(textField.getText());
                    } catch (NumberFormatException e) {
                        clear();
                        Footer.displayWarning(Loc.get("NO_VALID_INPUT_ONLY_INTEGER_VALUES"));
                        setErrorStyle();
                    }
                }
            }

            @Override
            public void focusGained(FocusEvent e) {
                setDefaultStyle(true);
            }
        });
        textField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (textField.getText().isEmpty()) {
                    textField.setToolTipText(null);
                } else {
                    textField.setToolTipText(textField.getText());
                }
            }
        });
        multiPanel.add(BorderLayout.CENTER, textField);

        setContent(multiPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

    @Override
    public void setInteger(String number) {
        textField.setText(number);
    }

    @Override
    public void setInteger(Integer number) {
        if (number != null) {
            textField.setText("" + number);
        } else {
            clear();
        }
    }

    @Override
    public String getIntegerAsString() {
        return textField.getText();
    }

    @Override
    public Integer getInteger() {
        if (textField.getText().isEmpty()) {
            return null;
        } else {
            return Integer.parseInt(textField.getText());
        }
    }

    public JTextField getTextField() {
        return textField;
    }
}
