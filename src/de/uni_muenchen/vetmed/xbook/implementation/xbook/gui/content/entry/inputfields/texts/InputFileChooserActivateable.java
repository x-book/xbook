package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawFileChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.IFileChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractActiveableInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputFileChooserActivateable extends AbstractActiveableInputElement implements IFileChooser {

    /**
     * The text field object.
     */
    protected RawFileChooser zField;
    protected FileNameExtensionFilter fileFilter;
    protected ColumnType columnTypeFile;

    public InputFileChooserActivateable(ColumnType columnTypeBoolean, ColumnType columnTypeFile, ColumnType columnTypeFileName, FileNameExtensionFilter fileFilter) {
        super(columnTypeBoolean, columnTypeFileName);
        this.fileFilter = fileFilter;
        this.columnTypeFile = columnTypeFile;
    }

    @Override
    public String getText() {
        return zField.getFileName();
    }

    public JTextField getTextField() {
        return zField.getTextField();
    }

    @Override
    public AbstractInputElementFunctions createActivableInputField() {
        if (zField == null) {
            zField = new RawFileChooser(columnTypeFile, columnType, fileFilter);
        }
        return zField;
    }

    @Override
    public void setText(String text) {
        zField.setFileName(text);
    }

    public void setMaxFileSize(long maxFileSize) {
        zField.setMaxFileSize(maxFileSize);
    }}
