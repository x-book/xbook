package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.INumberAsStringField;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.util.ArrayList;

public class InputNumberAsStringField extends AbstractInputElement implements INumberAsStringField {

    /**
     * The text field object.
     */
    protected JTextField textField;
    /**
     * The default value.
     */
    private Integer defaultValue;

    public InputNumberAsStringField(ColumnType columnType) {
        super(columnType);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    @Deprecated
    public InputNumberAsStringField(ColumnType columnType, int gridX, int gridY, SidebarPanel sidebar) {
        super(columnType);
        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param sidebarText The sidebar text for this input element.
     */
    @Deprecated
    public InputNumberAsStringField(ColumnType columnType, int gridX, int gridY, String sidebarText) {
        this(columnType, gridX, gridY, (new SidebarEntryField(columnType, sidebarText)));
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     */
    @Deprecated
    public InputNumberAsStringField(ColumnType columnType, int gridX, int gridY) {
        this(columnType, gridX, gridY, (SidebarPanel) null);
    }

    public void setDefaultValue(Integer value) {
        defaultValue = value;
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        final String s = list.get(columnType);
        if (s != null) {
            setNumber(s);
        }
    }

    @Override
    public void clear() {
        if (defaultValue == null) {
            textField.setText("");
        } else {
            textField.setText("" + defaultValue);
        }
    }

    public String getDecimalInStringRound(String str, int maxDecimalPlaces) { // interpret
        str = textField.getText();
        str = str.toLowerCase();
        str = str.replaceAll("[^0-9.,]+", "");
        str = str.replace(',', '.');
        if (str.contains(".")) {
            String strlast = str.substring(str.lastIndexOf('.') + 1);
            str = str.replaceAll("[.]+", "");
            str = str.substring(0, str.length() - strlast.length());
            str = str + "." + strlast;
            BigDecimal bd = new BigDecimal(str);
            str = bd.setScale(maxDecimalPlaces, BigDecimal.ROUND_HALF_EVEN).toString();
        }
        while (str.endsWith("0") && (str.contains(",") || str.contains("."))) {
            str = str.substring(0, str.length() - 1);
        }
        if (str.endsWith(",") || str.endsWith(".")) {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    public void setStringInTextField(String s) {
        if (defaultValue == null) {
            textField.setText(s);
        }
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        list.add(new DataColumn(getNumberAsString(), columnType.getColumnName()));
    }

    @Override
    public String getStringRepresentation() {
        return textField.getText().replace(",", ".");
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textField);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !textField.getText().isEmpty();
    }


    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        textField = new JTextField();
        textField.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
        textField.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        textField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent event) {
                String str = "";
                if (!(textField.getText().isEmpty())) {
                    try {
                        Double.parseDouble(str = getDecimalInStringRound(textField.getText(), 3));
                    } catch (NumberFormatException e) {
                        clear();
                        Footer.displayWarning(Loc.get("NO_VALID_INPUT_ONLY_NUMBERS"));
                        setErrorStyle();
                    }
                    setStringInTextField(str);
                }
                textField.setText(textField.getText().replace(".", ","));
            }

            @Override
            public void focusGained(FocusEvent e) {
                setDefaultStyle(true);
            }
        });
        textField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                setDefaultStyle(true);
                if (textField.getText().isEmpty()) {
                    textField.setToolTipText(null);
                } else {
                    textField.setToolTipText(textField.getText());
                }
            }
        });
        multiPanel.add(BorderLayout.CENTER, textField);

        setContent(multiPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

    @Override
    public void setNumber(String number) {
        textField.setText(number.replace(".", ","));
    }

    @Override
    public String getNumberAsString() {
        return textField.getText().replace(",", ".");
    }
}
