package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.UpdateableEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.border.EmptyBorder;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractActiveableInputElement extends AbstractInputElement {

    private AbstractInputElementFunctions activableInputField;
    private JRadioButton radioNo;
    private JRadioButton radioYes;
    protected ColumnType columnTypeBoolean;

    public AbstractActiveableInputElement(ColumnType columnTypeBoolean, ColumnType columnTypeField) {
        super(columnTypeField);
        this.columnTypeBoolean = columnTypeBoolean;
        setGridY(2);
    }

    public abstract AbstractInputElementFunctions createActivableInputField();

    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        JPanel radioButtons = new JPanel(new StackLayout());
        radioNo = new JRadioButton(Loc.get("NO"));
        radioNo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (radioNo.isSelected()) {
                    radioYes.setSelected(false);
                }
                updateVisibility();
            }
        });
        radioButtons.add(radioNo);
        radioYes = new JRadioButton(Loc.get("YES"));
        radioYes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (radioYes.isSelected()) {
                    radioNo.setSelected(false);
                }
                updateVisibility();
            }
        });
        radioButtons.add(radioYes);
        multiPanel.add(BorderLayout.NORTH, radioButtons);

        multiPanel.add(BorderLayout.CENTER, activableInputField = createActivableInputField());

        updateVisibility();
        setContent(multiPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }

        activableInputField.setBorder(new EmptyBorder(10, 0, 0, 0));
    }

    @Override
    public void load(DataSetOld data) {
        activableInputField.load(data);

        DataRow list = data.getDataRowForTable(getBooleanTableName());
        String s = list.get(columnTypeBoolean);
        radioNo.setSelected(s.equals("0"));
        radioYes.setSelected(s.equals("1"));

        updateVisibility();
    }

    @Override
    public void clear() {
        activableInputField.clear();

        radioNo.setSelected(false);
        radioYes.setSelected(false);
        updateVisibility();
    }

    @Override
    public void save(DataSetOld data) throws IsAMandatoryFieldException {
        activableInputField.save(data);

        DataRow list = data.getDataRowForTable(getBooleanTableName());
        list.add(new DataColumn(getSelection(), columnTypeBoolean.getColumnName()));
    }

    @Override
    public String getStringRepresentation() {
        return activableInputField.getStringRepresentation();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = activableInputField.getMyFocusableComponents();
        comp.add(radioNo);
        comp.add(radioYes);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return activableInputField.isValidInput();
    }


    @Override
    public void setEntry(UpdateableEntry entry) {
        super.setEntry(entry);
        activableInputField.setEntry(entry);
    }

    private void updateVisibility() {
        boolean setVisible = radioYes.isSelected();
        activableInputField.setVisible(setVisible);
        if (!setVisible) {
            activableInputField.clear();
        }
    }

    private String getSelection() {
        if (radioNo.isSelected()) {
            return "0";
        } else if (radioYes.isSelected()) {
            return "1";
        } else {
            return "-1";
        }
    }

    /**
     * Returns the name of the table that holds the column for the boolean value
     * (yes/no). Must be overridden when the boolean value should be saved in
     * another column than the one of the value of the input field. (e.h. multi
     * input fields).
     *
     * @return The name of the table that holds the column for the boolean
     * value.
     */
    protected String getBooleanTableName() {
        return activableInputField.getTableName();
    }

}
