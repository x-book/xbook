package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.UpdateableEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractDefaultInputElement extends AbstractInputElement {

    protected AbstractInputElementFunctions inputField;

    public AbstractDefaultInputElement(Mode mode, ColumnType columnType) {
        super(mode, columnType);
    }
    public AbstractDefaultInputElement(ColumnType columnType) {
        super(columnType);
    }

    public abstract AbstractInputElementFunctions createInputField();

    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        multiPanel.add(BorderLayout.CENTER, inputField = createInputField());

        setContent(multiPanel);

        if (columnType != null && columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

    @Override
    public void load(DataSetOld data) {
        inputField.load(data);
    }

    @Override
    public void clear() {
        inputField.clear();
    }

    @Override
    public void save(DataSetOld data) throws IsAMandatoryFieldException {
        inputField.save(data);
    }

    @Override
    public String getStringRepresentation() {
        return inputField.getStringRepresentation();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return inputField.getMyFocusableComponents();
    }

    @Override
    public boolean isValidInput() {
        return inputField.isValidInput();
    }


    @Override
    public void setEntry(UpdateableEntry entry) {
        super.setEntry(entry);
        inputField.setEntry(entry);
    }

}
