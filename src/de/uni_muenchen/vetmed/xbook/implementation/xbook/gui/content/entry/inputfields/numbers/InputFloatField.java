package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.TooManyDigitsException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IFloatField;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.MathsHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * A float field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputFloatField extends AbstractInputElement implements IFloatField {

    /**
     * The text field object.
     */
    protected JTextField textField;
    /**
     * The panel wrapper for the input field content.
     */
    protected JPanel panel;
    private Integer digitsAfterDecimalPoint;

    public InputFloatField(ColumnType columnType) {
        super(columnType);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     * @param digitsAfterDecimalPoint The number of digits after the decimal
     * point. <code>null</code> means infinite.
     */
    @Deprecated
    public InputFloatField(ColumnType columnType, int gridX, int gridY, SidebarPanel sidebar, final Integer digitsAfterDecimalPoint) {
        super(columnType);
        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);
        setDigitsAfterDecimalPoint(digitsAfterDecimalPoint);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    @Deprecated
    public InputFloatField(ColumnType columnType, int gridX, int gridY, SidebarPanel sidebar) {
        this(columnType, gridX, gridY, sidebar, null);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param sidebarText The sidebar text for this input element.
     * @param digitsAfterDecimalPoint The number of digits after the decimal
     * point. <code>null</code> means infinite.
     */
    @Deprecated
    public InputFloatField(ColumnType columnType, int gridX, int gridY, String sidebarText, Integer digitsAfterDecimalPoint) {
        this(columnType, gridX, gridY, new SidebarEntryField(columnType, sidebarText), digitsAfterDecimalPoint);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param sidebarText The sidebar text for this input element.
     */
    @Deprecated
    public InputFloatField(ColumnType columnType, int gridX, int gridY, String sidebarText) {
        this(columnType, gridX, gridY, new SidebarEntryField(columnType, sidebarText), null);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param digitsAfterDecimalPoint The number of digits after the decimal
     * point. <code>null</code> means infinite.
     */
    @Deprecated
    public InputFloatField(ColumnType columnType, int gridX, int gridY, Integer digitsAfterDecimalPoint) {
        this(columnType, gridX, gridY, (SidebarPanel) null, digitsAfterDecimalPoint);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     */
    @Deprecated
    public InputFloatField(ColumnType columnType, int gridX, int gridY) {
        this(columnType, gridX, gridY, (SidebarPanel) null, null);
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        String value = list.get(columnType);
        if (value == null || value.equals("-1") || value.equals("-1.0")) {
            return;
        }
        try {
            value = "" + Float.parseFloat(value.replace(",", "."));
        } catch (NumberFormatException ex) {
            value = "";
        }
        textField.setText(value);
    }

    @Override
    public void clear() {
        textField.setText("");
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        if (textField.getText().isEmpty()) {
            list.add(new DataColumn("-1", columnType.getColumnName()));
        } else {
            list.add(new DataColumn(textField.getText(), columnType.getColumnName()));
        }
    }

    @Override
    public String getStringRepresentation() {
        return textField.getText();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textField);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !textField.getText().isEmpty();
    }


    @Override
    protected void createFieldInput() {
        panel = new JPanel();
        panel.setLayout(new BorderLayout());

        textField = new JTextField();
        PlainDocument document = (PlainDocument) textField.getDocument();

        document.setDocumentFilter(new MyFloatFilter());

        textField.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        textField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent event) {
                if (!(textField.getText().isEmpty())) {
                    try {
                        String input = textField.getText();
                        Float value = MathsHelper.roundFloat(input.replace(",", "."), digitsAfterDecimalPoint);
                        textField.setText(value + "");
                    } catch (NumberFormatException e) {
                        clear();
                        Footer.displayWarning(Loc.get("NO_VALID_INPUT_ONLY_FLOAT_VALUES"));
                        setErrorStyle();
                    } catch (TooManyDigitsException r) {
                        textField.setText("");
                        setErrorStyle();
                        Footer.displayError(Loc.get("MORE_THAN_7_DIGITS_ARE_NOT_SUPPORTED"));
                    }
                }
            }

            @Override
            public void focusGained(FocusEvent e) {
                setDefaultStyle(true);
            }
        });
        textField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (textField.getText().isEmpty()) {
                    textField.setToolTipText(null);
                } else {
                    textField.setToolTipText(textField.getText());
                }
            }
        });
        panel.add(BorderLayout.CENTER, textField);

        setContent(panel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

    // ========================================================================
    public AbstractInputElement setDigitsAfterDecimalPoint(Integer digitsAfterDecimalPoint) {
        this.digitsAfterDecimalPoint = digitsAfterDecimalPoint;
        return this;
    }

    @Override
    public void setFloat(String number) {
        if (number.equals("-1") || number.equals("-1.0")) {
            return;
        }
        try {
            number = "" + Float.parseFloat(number.replace(",", "."));
        } catch (NumberFormatException ex) {
            number = "";
        }
        textField.setText(number);
    }

    @Override
    public void setFloat(Float number) {
        if (number != null) {
            textField.setText(number + "");
        } else {
            clear();
        }
    }

    @Override
    public String getFloatAsString() {
        return textField.getText();
    }

    @Override
    public Float getFloat() {
        if (textField.getText().isEmpty()) {
            return null;
        } else {
            return Float.parseFloat(textField.getText().replace(",", "."));
        }
    }



    static class  MyFloatFilter extends DocumentFilter {
        @Override
        public void insertString(FilterBypass fb, int offset, String string,
                                 AttributeSet attr) throws BadLocationException {

            Document doc = fb.getDocument();
            StringBuilder sb = new StringBuilder();
            sb.append(doc.getText(0, doc.getLength()));
            sb.insert(offset, string);

            if (test(sb.toString())) {
                super.insertString(fb, offset, string.replace(",", "."), attr);
            } else {
                Footer.displayWarning(Loc.get("NO_VALID_INPUT_ONLY_INTEGER_VALUES"));
                // warn the user and don't allow the insert
            }
        }

        private boolean test(String text) {
            try {
                if (text.isEmpty()) {
                    return true;
                }
                Float.parseFloat(text.replace(",", "."));
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text,
                            AttributeSet attrs) throws BadLocationException {

            Document doc = fb.getDocument();
            StringBuilder sb = new StringBuilder();
            sb.append(doc.getText(0, doc.getLength()));
            sb.replace(offset, offset + length, text);

            if (test(sb.toString())) {
                super.replace(fb, offset, length, text.replace(",", "."), attrs);
            } else {
                Footer.displayWarning(Loc.get("NO_VALID_INPUT_ONLY_INTEGER_VALUES"));
            }

        }

        @Override
        public void remove(FilterBypass fb, int offset, int length)
                throws BadLocationException {
            Document doc = fb.getDocument();
            StringBuilder sb = new StringBuilder();
            sb.append(doc.getText(0, doc.getLength()));
            sb.delete(offset, offset + length);

            if (test(sb.toString())) {
                super.remove(fb, offset, length);
            } else {
                Footer.displayWarning(Loc.get("NO_VALID_INPUT_ONLY_INTEGER_VALUES"));
            }

        }
    }
}
