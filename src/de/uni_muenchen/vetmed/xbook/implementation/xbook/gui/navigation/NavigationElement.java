package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.navigation;

import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class NavigationElement extends JPanel {

    private final Color HOVER_BORDER = new Color(39, 39, 39);
    private final Color HOVER_BACKGROUND = new Color(55, 55, 55);
    private final JPanel outerWrapper;
    private final JPanel wrapperCollapseable;
    private final JLabel imageButton;
    private final JLabel title;
    private final JLabel imageCollapseable;
    private final JPanel wrapperButton;
    private final JPopupMenu popup = new JPopupMenu();
    private boolean isPopupVisible = false;
    private boolean openPopupOnClick = false;

    public NavigationElement(String label, Icon icon) {
        setLayout(new BorderLayout());
        setOpaque(false);

        setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

        outerWrapper = new JPanel(new BorderLayout());
        outerWrapper.setBorder(BorderFactory.createEmptyBorder(1, 1, 0, 1));
        outerWrapper.setOpaque(false);

		// Standard button
        wrapperButton = new JPanel(new BorderLayout());
        wrapperButton.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));
        wrapperButton.setOpaque(false);

        title = new JLabel("<html><body><center>" + label + "</center></body></html>");
        title.setForeground(Color.WHITE);
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setBorder(BorderFactory.createEmptyBorder(2, 0, 5, 0));
        wrapperButton.add(BorderLayout.NORTH, title);

        imageButton = new JLabel();
        imageButton.setHorizontalAlignment(JLabel.CENTER);
        imageButton.setBorder(BorderFactory.createEmptyBorder(0, 0, 3, 0));
        imageButton.setOpaque(false);
        imageButton.setIcon(icon);
        wrapperButton.add(BorderLayout.SOUTH, imageButton);

		// Collapseable button
        wrapperCollapseable = new JPanel(new BorderLayout());
        wrapperCollapseable.setBorder(BorderFactory.createEmptyBorder(0, 1, 0, 0));
        wrapperCollapseable.setOpaque(false);

        imageCollapseable = new JLabel();
        imageCollapseable.setHorizontalAlignment(JLabel.CENTER);
        imageCollapseable.setOpaque(false);
        imageCollapseable.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));
        imageCollapseable.setSize(new Dimension(Images.NAVIGATION_COLLAPSABLE_ICON_DOWN.getIconWidth(), Images.NAVIGATION_COLLAPSABLE_ICON_DOWN.getIconHeight()));
        imageCollapseable.setIcon(Images.NAVIGATION_COLLAPSABLE_ICON_DOWN);
        wrapperCollapseable.add(BorderLayout.CENTER, imageCollapseable);

		// arrange everything
        outerWrapper.add(BorderLayout.EAST, wrapperCollapseable);
        outerWrapper.add(BorderLayout.CENTER, wrapperButton);
        add(BorderLayout.CENTER, outerWrapper);

        addMouseListener(this);
        addMouseListener(title);
        addMouseListener(imageButton);
        addMouseListener(imageCollapseable);
        addMouseListener(wrapperButton);
        addMouseListener(wrapperCollapseable);
        addMouseListener(outerWrapper);

        addButtonClickListener(wrapperButton);
        addButtonClickListener(imageButton);
        addButtonClickListener(title);

        addCollapsableClickListener(imageCollapseable);
        addCollapsableClickListener(wrapperCollapseable);

        wrapperCollapseable.setVisible(false);
    }

    public JMenuItem addMenuItem(JMenuItem item) {
        popup.add(item);
        wrapperCollapseable.setVisible(popup.getSubElements().length > 0);
        return item;
    }

    public void addMenuSeparator() {
        popup.addSeparator();
        wrapperCollapseable.setVisible(popup.getSubElements().length > 0);
    }

    private void addButtonClickListener(final JComponent comp) {
        comp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (isEnabled()) {
                    if (openPopupOnClick) {
                        togglePopup(e);
                    } else {
                        actionOnClick();
                    }
                }
            }
        });
    }

    private void addCollapsableClickListener(JComponent comp) {
        comp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                togglePopup(e);
            }
        });
    }

    private void togglePopup(MouseEvent e) {
        if (isEnabled()) {
            if (isPopupVisible) {
                popup.setVisible(false);
                isPopupVisible = false;
            } else {
                popup.show(e.getComponent(), e.getX() - 55, 55);
                isPopupVisible = true;
            }
        }
    }

    private void addMouseListener(JComponent comp) {
        comp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (isEnabled()) {
                    outerWrapper.setBackground(HOVER_BACKGROUND);
                    outerWrapper.setOpaque(true);
                    outerWrapper.setBorder(BorderFactory.createLineBorder(HOVER_BORDER, 1));
                    wrapperCollapseable.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, HOVER_BORDER));
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                outerWrapper.setOpaque(false);
                outerWrapper.setBorder(BorderFactory.createEmptyBorder(1, 1, 0, 1));
                wrapperCollapseable.setBorder(BorderFactory.createEmptyBorder(0, 1, 0, 0));
            }
        });
    }

    @Override
    public void setEnabled(boolean state) {
        super.setEnabled(state);
        imageCollapseable.setEnabled(state);
        outerWrapper.setEnabled(state);
        wrapperCollapseable.setEnabled(state);
        imageButton.setEnabled(state);
        title.setEnabled(state);
        wrapperButton.setEnabled(state);
    }

    protected void actionOnClick() {
    }

    protected void setOpenPopupOnClick(boolean state) {
        openPopupOnClick = state;
    }
}
