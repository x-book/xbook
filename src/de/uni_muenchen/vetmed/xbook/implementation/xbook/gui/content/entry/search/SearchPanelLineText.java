package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.search;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.SearchEntryInfo;
import de.uni_muenchen.vetmed.xbook.api.datatype.SearchEntryInfo.SearchMode;

import javax.swing.*;
import java.util.ArrayList;

/**
 * The object to be able to enter a text as a search filter value.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@cesonia.io>
 */
public class SearchPanelLineText extends AbstractSearchPanelLine {

    /**
     * The textfield with the input for the search panel.
     */
    private JTextField input;

    /**
     * Constructor.
     *
     * @param columnType           The basic ColumnType.
     * @param availableSearchModes The available search modes. The first one of the list will automatically be selected
     *                             at default value. If array is empty, SearchMode.CONTAINS will be used as default
     *                             value.
     */
    public SearchPanelLineText(ColumnType columnType, ArrayList<SearchMode> availableSearchModes) {
        super(columnType, availableSearchModes);
    }

    @Override
    public SearchEntryInfo getInput() {
        return new SearchEntryInfo(input.getText(),currentSearchMode);
    }

    @Override
    public JComponent getInputField() {
        if (input == null) {
            input = new JTextField();
        }
        return input;
    }

    @Override
    public void clearInput() {
        super.clearInput();
        input.setText("");
    }
}
