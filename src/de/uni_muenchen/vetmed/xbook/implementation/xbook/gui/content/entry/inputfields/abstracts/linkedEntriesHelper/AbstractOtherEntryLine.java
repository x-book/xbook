package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.linkedEntriesHelper;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractOtherEntryLine extends JPanel {

    /**
     * The default height of one line.
     */
    protected static final int LINE_HEIGHT = 30;

    public AbstractOtherEntryLine(final AbstractMainFrame mainFrame) {
        setLayout(new FlowLayout(FlowLayout.LEADING));
        setPreferredSize(new Dimension(1, LINE_HEIGHT));

        // edit icon
        final JLabel editLabel = new JLabel(Images.EDIT_SQUARE);
        editLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                actionOnClickOnEdit();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                editLabel.setIcon(Images.EDIT_SQUARE_HOVERED);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                editLabel.setIcon(Images.EDIT_SQUARE);
            }
        });
        editLabel.setPreferredSize(new Dimension(40, editLabel.getMinimumSize().height));
        add(editLabel);

        // delete icon
        final JLabel deleteLabel = new JLabel(Images.DELETE_SQUARE);
        deleteLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int n = JOptionPane.showConfirmDialog(mainFrame,
                        Loc.get("DO_YOU_REALLY_WANT_TO_REMOVE_THIS_ENTRY"),
                        Loc.get("DELETE"),
                        JOptionPane.YES_NO_OPTION);
                if (n == JOptionPane.YES_OPTION) {
                    actionOnClickOnDelete();
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                deleteLabel.setIcon(Images.DELETE_SQUARE_HOVERED);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                deleteLabel.setIcon(Images.DELETE_SQUARE);
            }
        });
        deleteLabel.setPreferredSize(new Dimension(40, editLabel.getMinimumSize().height));
        add(deleteLabel);
    }

    protected abstract void displayData();

    protected abstract void actionOnClickOnDelete();

    protected abstract void actionOnClickOnEdit();
}
