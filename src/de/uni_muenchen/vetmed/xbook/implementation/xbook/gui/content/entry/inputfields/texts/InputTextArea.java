package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.documentHelper.LimitInputLengthDocument;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTextArea;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.ITextArea;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**
 * A text area as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputTextArea extends AbstractInputElement implements ITextArea {

    /**
     * The text field object.
     */
    private XTextArea textArea;

    public InputTextArea(ColumnType columnType) {
        super(columnType);
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        final String s = list.get(columnType);
        textArea.setText(s);
    }

    @Override
    public void clear() {
        textArea.setText("");
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        list.add(new DataColumn(textArea.getText(), columnType.getColumnName()));
    }

    @Override
    public String getStringRepresentation() {
        return textArea.getText();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textArea);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !textArea.getText().isEmpty();
    }



    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        textArea = new XTextArea();
        if (columnType.getMaxInputLength() != null) {
            textArea.setDocument(new LimitInputLengthDocument(columnType.getMaxInputLength()));
        }
        JScrollPane scroll = new JScrollPane(textArea);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setBorder(new EmptyBorder(0, 0, 0, 0));
        scroll.removeMouseWheelListener(scroll.getMouseWheelListeners()[0]);
        multiPanel.add(BorderLayout.CENTER, scroll);

        //
        // Add key listener to change the TAB behaviour in
        // JTextArea to transfer focus to other component forward
        // or backward.
        //
        textArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    if (e.getModifiers() > 0) {
                        textArea.transferFocusBackward();
                    } else {
                        textArea.transferFocus();
                    }
                    e.consume();
                }
            }
        });

        setContent(multiPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

    @Override
    public String getText() {
        return textArea.getText();
    }

    @Override
    public void setText(String text) {
        textArea.setText(text);
    }
}
