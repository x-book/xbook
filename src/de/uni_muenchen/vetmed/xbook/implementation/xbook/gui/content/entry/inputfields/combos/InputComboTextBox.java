package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos;

import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.event.*;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboTextBox;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputComboTextBox extends InputComboBox implements IComboTextBox, EntryListener, ValueEventListener {

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     *                   field.
     */
    public InputComboTextBox(ColumnType columnType) {
        super(columnType, new ArrayList<String>());
        values.add("");
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(combo);
        comp.add(combo.getEditor().getEditorComponent());
        return comp;
    }

    @Override
    public void load(DataSetOld data) {
        reload();
        DataRow list = data.getDataRowForTable(getTableName());
        final String s = list.get(columnType);
        if (s != null && !s.isEmpty()) {
            combo.setSelectedItem(s);
            ((JTextField) combo.getEditor().getEditorComponent()).setText(s);
        }
    }

    @Override
    public void clear() {
        super.clear();
        //no project no update
        if (apiControllerAccess.isProjectLoaded()) {
            reloadAvailableValues();
        }
        combo.setSelectedItem("");
        ((JTextField) combo.getEditor().getEditorComponent()).setText("");
    }

    public void reloadAvailableValues() {
        if (getDataFromColumnType() == null) {
            return;
        }
        try {
            String oldValue = "";
            if (combo != null) {
                oldValue = (String) combo.getSelectedItem();
            }

            combo.removeAllItems();
            UniqueArrayList<String> data = new UniqueArrayList<>();
            data.add("");
            try {
                data.addAll(getValues());
            } catch (NotLoadedException ignore) {
            }
            data.add(oldValue);
            combo.setItems(data);
            combo.setSelectedItem(oldValue);
            ((JTextField) combo.getEditor().getEditorComponent()).setText(oldValue);
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(InputComboTextBox.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected ArrayList<String> getValues() throws NotLoadedException, NotLoggedInException, StatementNotExecutedException {
        return apiControllerAccess.getInputUnitInformation(getDataFromColumnType().getColumnName());
    }

    @Override
    public void onEntryDeleted(EntryEvent e) {
        try {
            if (!apiControllerAccess.getCurrentProject().getProjectKey().equals(e.getProjectKey())) {
                return;
            }
        } catch (NotLoadedException e1) {
            e1.printStackTrace();
        }
        reloadAvailableValues();
    }

    @Override
    public void onEntryAdded(EntryEvent e) {
        ProjectDataSet project = null;
        try {
            project = apiControllerAccess.getCurrentProject();
        } catch (NotLoadedException e1) {
            e1.printStackTrace();
        }
        if (project == null) {
            return;
        }
        if (!project.getProjectKey().equals(e.getProjectKey())) {
            return;
        }
        String value = e.getData().get(columnType);
        if (value == null) {
            return;
        }
        if (((DefaultComboBoxModel) combo.getModel()).getIndexOf(value) == -1) {
            combo.addItem(value);
        }
    }

    @Override
    public void onEntryUpdated(EntryEvent e) {
        try {
            if (!apiControllerAccess.getCurrentProject().getProjectKey().equals(e.getProjectKey())) {
                return;
            }
        } catch (NotLoadedException e1) {
            e1.printStackTrace();
        }
        reloadAvailableValues();
    }

    /**
     * Returns the ColumnType that holds the data that should be inserted into
     * the combo box.
     *
     * @return The ColumnType
     */
    protected ColumnType getDataFromColumnType() {
        return columnType;
    }

    @Override
    public void onValueUpdated() {
        //no project no update
        if (!apiControllerAccess.isProjectLoaded()) {
            return;
        }
        reloadAvailableValues();
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();

        combo.setEditable(true);
        combo.setCustomInputEnabled(true);
        reloadAvailableValues();
        EventRegistry.addValueListener(getTableName(), this);

        apiControllerAccess.addProjectListener(new ProjectAdapter() {
            @Override
            public void onProjectLoaded(ProjectEvent evt) {
                reloadAvailableValues();
            }
        });
    }



}
