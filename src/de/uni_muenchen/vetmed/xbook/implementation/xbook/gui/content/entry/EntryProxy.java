package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputIdDbnrField;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel.Position;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.exception.WrongModeException;
import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class EntryProxy extends AbstractEntry {

    private AbstractEntry entry;
    private AbstractContent previousContent;
    private InputIdDbnrField linkedInputField;

    public EntryProxy(AbstractEntry entry, AbstractContent previousContent) {
        super(GeneralInputMaskMode.SINGLE, false);
        this.entry = entry;
        this.previousContent = previousContent;

        init();
        updateInputFieldVisibility();
		try {
			entry.loadEntry();
		} catch (WrongModeException ex) {
			Logger.getLogger(EntryProxy.class.getName()).log(Level.SEVERE, null, ex);
		}
    }

    @Override
    public void composeSections() {
        // do nothing
    }

    @Override
    protected void setEntryObject() {
        entry.setEntryObject();
    }

    @Override
    protected ArrayList<AbstractInputElement> getInputElements() {
        if (entry == null) {
            return new ArrayList<>();
        } else {
            return entry.getInputElements();
        }
    }

    @Override
    protected JPanel getContent() {
        return entry.getContent();
    }

    @Override
    protected boolean checkBeforeSaving() {
        if (!super.checkBeforeSaving()) {
            return false;
        }
        return entry.checkBeforeSaving();
    }

    @Override
    public ButtonPanel getButtonBar() {

        ButtonPanel buttons = new ButtonPanel();

        buttons.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_BACK, Loc.get("BACK"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Content.setContent(previousContent);
            }
        });

        buttonClear = buttons.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_CLEAR, Loc.get("CLEAR"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearFields();
            }
        });

        buttons.addImageButton(Position.NORTH_EAST, Images.BUTTONPANEL_SAVE, "<html><center>" + Loc.get("SAVE_AND_BACK") + "</center></html>", Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (previousContent != null) {
                    try {
                        if (!checkBeforeSaving()) {
                            return;
                        }
                        EntryDataSet entryData = entry.getCurrentValues();
                        mainFrame.getController().saveEntry(entryData);
                        clearFields();
                        Content.setContent(previousContent);
                        Key key = entryData.getEntryKey();
                        linkedInputField.selectEntry(key);
                    } catch (NotLoggedInException | EntriesException | NotLoadedException | StatementNotExecutedException | NoRightException ex) {
                        Logger.getLogger(EntryProxy.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IsAMandatoryFieldException ex) {
                        Footer.displayError(Loc.get("ENTER_A_VALUE_FOR_THE_MANDATORY_FIELD", ex.getColumnType()));
                    }
                }
            }
        });

        return buttons;
    }

    @Override
    public AbstractEntry copy(EntryDataSet data) {
        return entry.copy(data);
    }

    @Override
    public String getInputOfField(ColumnType type) {
        return entry.getInputOfField(type);
    }

    @Override
    public String getTableName() {
        return entry.getTableName();
    }

    public void setLinkedInputField(InputIdDbnrField linkedInputField) {
        this.linkedInputField = linkedInputField;
    }

}
