package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.CannotBeReturnedOnPressingBackButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTextField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;

public abstract class AbstractLookupTableEditorContent<T> extends AbstractContent
        implements CannotBeReturnedOnPressingBackButton, TableCellEditor, Serializable {

    private JPanel content = null;
    private JTable table = null;
    private ButtonPanel buttonPanel = null;
    private JTextField filterTextField = null;
    private TableRowSorter<TableModel> rowSorter = null;
    private final List<T> data = new ArrayList<>();
    private Object currentValue = null;
    private JButton tableCellEditorComponent = null;
    private final Queue<CellEditorListener> cellEditorListeners = new ConcurrentLinkedQueue<>();
    private MultiLineTextLabel missingMandatoryFields;

    public String getHeaderTitle() {
        return mainFrame.getController().getBookName() + ": " + Loc.get("TABLE");
    }

    public abstract Object[] getRow(T entry);

    public String[] getColumnNames() {
		return new String[] { Loc.get("TABLE_OF", mainFrame.getController().getBookName()) };
    }

    public AbstractLookupTableEditorContent(Collection<T> data) {
        super();
        this.data.addAll(data);
    }

    private JPanel createContent() {
        JPanel content = new JPanel();

        content.setLayout(new BorderLayout());
        content.setBackground(Colors.CONTENT_BACKGROUND);

        JPanel topWrapper = new JPanel(new StackLayout());
        topWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        content.add(BorderLayout.NORTH, topWrapper);

        topWrapper.add(new XTitle(getHeaderTitle()));
        
        // filter

        JPanel filterWrapper = new JPanel(new BorderLayout());
        filterWrapper.setBorder(BorderFactory.createEmptyBorder(0, 0, 6, 0));
        filterWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        topWrapper.add(filterWrapper);

        XLabel label = new XLabel(Loc.get("FILTER") + ":");
        label.setPreferredSize(new Dimension(100, 1));
        filterWrapper.add(BorderLayout.WEST, label);

        filterTextField = new XTextField();
        filterTextField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent arg0) {
                newFilter();
            }
        });
        filterTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                newFilter();
            }
        });
        filterWrapper.add(BorderLayout.CENTER, filterTextField);

        table = new JTable();
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setBackground(Colors.CONTENT_BACKGROUND);
        content.add(BorderLayout.CENTER, scrollPane);

        TableModel model = new LookupTableEditorTableModel<T>(data) {
            @Override
            public Object[] getRow(T entry) {
                return AbstractLookupTableEditorContent.this.getRow(entry);
            }

            @Override
            public String[] getColumnNames() {
                return AbstractLookupTableEditorContent.this.getColumnNames();
            }
        };

        table.setModel(model);
        rowSorter = new TableRowSorter<>(model);
        table.setRowSorter(rowSorter);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    assignValue();
                }
            }
        });

        return content;
    }

    private void newFilter() {
        if (rowSorter != null) {
            List<RowFilter<Object, Object>> filters = new ArrayList<>();

            for (String keyword : filterTextField.getText().split(" ")) {
                filters.add(RowFilter.regexFilter("(?i).*" + Pattern.quote(keyword) + ".*", 0));
            }

            try {
                rowSorter.setRowFilter(RowFilter.andFilter(filters));
            } catch (java.util.regex.PatternSyntaxException e) {
                return;
            }
        }
    }

    private void assignValue() {
        currentValue = data.get(rowSorter.convertRowIndexToModel(table.getSelectedRow()));
        stopCellEditing();
        Content.setPreviousContent();
    }

    private ButtonPanel createButtonPanel() {
        ButtonPanel buttonPanel = new ButtonPanel();

        XButton buttonAssign = buttonPanel.addButtonToNorthEast(Loc.get("ASSIGN"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                assignValue();
            }
        });
        buttonAssign.setStyle(XButton.Style.DEFAULT);

        XButton buttonCancel = buttonPanel.addButtonToNorthWest(Loc.get("CANCEL"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelCellEditing();
                Content.setPreviousContent();
            }
        });
        buttonCancel.setStyle(XButton.Style.DARKER);

        return buttonPanel;
    }

    @Override
    protected JPanel getContent() {
        if (content == null) {
            content = createContent();
        }
        return content;
    }

    @Override
    public ButtonPanel getButtonBar() {
        if (buttonPanel == null) {
            buttonPanel = createButtonPanel();
        }
        return buttonPanel;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    @Override
    public void setFocus() {
        getContent().transferFocus();
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
        System.out.println("addCellEditorListener(l)");
        cellEditorListeners.add(l);
    }

    @Override
    public void cancelCellEditing() {
        fireEditingCanceled();
    }

    @Override
    public Object getCellEditorValue() {
        return currentValue;
    }

    @Override
    public boolean isCellEditable(EventObject arg0) {
        return true;
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        cellEditorListeners.remove(l);
    }

    @Override
    public boolean shouldSelectCell(EventObject arg0) {
        return true;
    }

    @Override
    public boolean stopCellEditing() {
        fireEditingStoped();
        return true;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int col) {
        tableCellEditorComponent = createTableCellEditorComponent();
        Content.setContent(AbstractLookupTableEditorContent.this);
        reset();
        if (value == null) {
            tableCellEditorComponent.setText("");
        } else {
            tableCellEditorComponent.setText(value.toString());
        }
        currentValue = value;
        return tableCellEditorComponent;
    }

    private JButton createTableCellEditorComponent() {
        JButton button = new JButton();
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Content.setContent(AbstractLookupTableEditorContent.this);
            }
        });
        return button;
    }

    protected void fireEditingCanceled() {
        for (CellEditorListener l : cellEditorListeners) {
            l.editingCanceled(new ChangeEvent(this));
        }
    }

    protected void fireEditingStoped() {
        for (CellEditorListener l : cellEditorListeners) {
            l.editingStopped(new ChangeEvent(this));
        }
    }

    private void reset() {
        filterTextField.setText("");
        newFilter();
        table.getSelectionModel().clearSelection();
    }
}
