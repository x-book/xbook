package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos;

import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.DynamicComboBoxListValue.DynamicComboBoxListValueString;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IMultiComboTextBox;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Allows entering multiple values that are saved as values.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class InputMultiComboTextBox extends AbstractInputElement implements IMultiComboTextBox {

    protected DynamicComboBoxListValueString multiComboBox;
    private ArrayList<String> allData;
    private String table;

    public InputMultiComboTextBox(ColumnType columnType, String table) {
        super(columnType);
        this.table = table;
    }

    @Deprecated
    public InputMultiComboTextBox(ColumnType columnType, int gridX, int gridY, String table, SidebarPanel sidebar) {
        super(columnType);
        this.table = table;

        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);

    }

    @Deprecated
    public InputMultiComboTextBox(ColumnType columnType, int width, int height, String table) {
        this(columnType, width, height, table, new SidebarEntryField(columnType, ""));
    }

    @Override
    public void load(DataSetOld data) {
        DataTableOld dataTable = data.getOrCreateDataTable(table);
        for (DataRow list : dataTable) {
            final String s = list.get(columnType);
            if (s != null) {
                multiComboBox.addItemToList(s);
            }
        }
    }

    @Override
    public void clear() {
        multiComboBox.clearInput();
        multiComboBox.removeAllItems();
    }

    @Override
    public void save(DataSetOld data) {
        DataTableOld list = data.getOrCreateDataTable(table);
        for (String s : multiComboBox.getListItems()) {
            if (s != null) {
                DataRow row = new DataRow(table);
                row.add(new DataColumn(s, columnType));
                list.add(row);
            }
        }
    }

    public void reloadInputField() {
        multiComboBox.updateData(true);
    }

    public abstract ArrayList<String> getData();

    @Override
    public String getStringRepresentation() {
        String s = "";
        for (Object items : multiComboBox.getListItems()) {
            s += items + ", ";
        }
        if (!s.isEmpty()) {
            s = s.substring(0, s.length() - ", ".length());
        }
        return s;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.addAll(multiComboBox.getFocusableComponents());
        comp.add(multiComboBox.getComboBox());
        comp.add(multiComboBox.getComboBox().getEditor().getEditorComponent());
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    @Override
    public void actionOnFocusLost() {
        multiComboBox.addInputToList();
        super.actionOnFocusLost();
    }

    @Override
    protected void createFieldInput() {
        allData = getData();

        multiComboBox = new DynamicComboBoxListValueString(allData, columnType.getDisplayName()) {
            @Override
            protected ArrayList<String> reloadDatabaseValues() {
                return getData();
            }
        };
        setContent(multiComboBox);

        multiComboBox.setSelectAllAndRemoveAllButtonVisible(false);
        multiComboBox.getComboBox().setPreferredSize(new Dimension(1, Sizes.INPUT_FIELD_HEIGHT));
        multiComboBox.getComboBox().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        multiComboBox.getListScroller().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        if (multiComboBox.getListScroller().getMouseWheelListeners().length > 0) {
            multiComboBox.getListScroller().removeMouseWheelListener(multiComboBox.getListScroller().getMouseWheelListeners()[0]);
        }
        multiComboBox.getList().setBackground(Color.WHITE);
    }

    @Override
    public void addToSelection(String value) {
        multiComboBox.addItemToList(value);
    }

    @Override
    public void addToSelection(List<String> values) {
        for (String s : values) {
            addToSelection(s);
        }
    }

    @Override
    public void removeAllSelectedItems() {
        multiComboBox.removeAllItemsFromList();
    }

    @Override
    public void setSelectedData(String value) {
        clear();
        addToSelection(value);
    }

    @Override
    public void setSelectedData(List<String> values) {
        clear();
        addToSelection(values);
    }

    @Override
    public List<String> getSelectedData() {
        return multiComboBox.getListItems();
    }

}
