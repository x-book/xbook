package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.adminPanel;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractCodeTableManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.ProjectRightManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.TableManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Vector;
import javax.swing.AbstractCellEditor;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

/**
 *
 * @author Johannes Lohrer <johannes.lohrer@palaeo.vetmed.uni-muenchen.de>
 */
public class AdminPanel extends AbstractContent {

    JComboBox<String> box;
    JTable table;
    JPanel addPanel;
    ArrayList<NewEntryHolder> datas;
    ArrayList<String> primaryColumns = new ArrayList<>();
    HashMap<String, String> language;

    public AdminPanel() {
        init();

    }

    @Override
    protected JPanel getContent() {
        JPanel p = new JPanel(new BorderLayout());

        box = new JComboBox<>();
        JPanel west = new JPanel(new StackLayout());
        p.add(BorderLayout.WEST, west);
        west.add(box);
        table = new JTable();
        JPanel center = new JPanel(new BorderLayout());
        JScrollPane sp = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        center.add(BorderLayout.CENTER, sp);
        p.add(BorderLayout.CENTER, center);
        addPanel = new JPanel();
        center.add(BorderLayout.SOUTH, addPanel);
        box.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateTable();
            }
        });
//        TableCellListener tcl = new TableCellListener(table, new AbstractAction() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                TableCellListener tcl = (TableCellListener) e.getSource();
//                String columnId = table.getColumnName(tcl.getColumnType());
//                updateEntry(primaryColumns.contains(columnId), tcl.getRow(), tcl.getColumnType(), (String) tcl.getOldValue());
//
//            }
//        });
//        try {
//            language = mainFrame.getController().getLanguagesMap();
//        } catch (NotLoggedInException ex) {
//            Logger.getLogger(AdminPanel.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (StatementNotExecutedException ex) {
//            Logger.getLogger(AdminPanel.class.getName()).log(Level.SEVERE, null, ex);
//        }
        return p;
    }

    public void updateTable() {
        String tableName = (String) box.getSelectedItem();

        try {
            datas = new ArrayList<>();
            primaryColumns.clear();
            addPanel.removeAll();
            Object[] up = new Object[0];

            AbstractSynchronisationController<?,?> c = ((AbstractSynchronisationController) mainFrame.getController());
            c.updateTable(tableName);

            primaryColumns = c.getPrimaryColumns(tableName);
            ArrayList<ArrayList<String>> list = c.getCodeTableData(tableName);
//            ArrayList<ArrayList<String>> list = new ArrayList<>();
            if (list.size() > 0) {
                up = list.get(0).toArray();
            }

            addPanel.setLayout(new GridLayout(1, up.length));
            for (int i = 0; i < up.length; i++) {
                String column = (String) up[i];
                if (column.equals(TableManager.DELETED.getColumnName())) {
                    ArrayList<DataColumn> valueList = new ArrayList<>();
                    valueList.add(new DataColumn(Loc.get("NO"), TableManager.DELETED_NO));
                    valueList.add(new DataColumn(Loc.get("YES"), TableManager.DELETED_YES));
                    datas.add((NewEntryHolder) addPanel.add(new NewEntryHolder(column, valueList)));
                } else if (column.equals(AbstractCodeTableManager.LANGUAGE.toLowerCase())) {
                    ArrayList<DataColumn> valueList = new ArrayList<>();
                    for (Entry<String, String> o : getSortedEntries(language)) {
                        valueList.add(new DataColumn(o.getValue(), o.getKey()));
                    }
                    datas.add((NewEntryHolder) addPanel.add(new NewEntryHolder(column, valueList)));

                } else {
                    datas.add((NewEntryHolder) addPanel.add(new NewEntryHolder(column)));
                }
            }

            MyTableModel m = new MyTableModel(up, 0, language);
            for (int i = 1; i < list.size(); i++) {
                m.addRow(list.get(i).toArray());
            }
            System.out.println("hö?");

            table.setModel(m);
            try {
                TableColumn column = table.getColumn(TableManager.DELETED.getColumnName());

                String[] values = new String[]{Loc.get("NO"), Loc.get("YES")};
                column.setCellEditor(new MyTableCellEditor(values));

            } catch (IllegalArgumentException e) {
            }
            try {
                TableColumn column = table.getColumn(AbstractCodeTableManager.LANGUAGE.toLowerCase());
                ArrayList<Entry<String, String>> sortedList = getSortedEntries(language);
                String[] names = new String[sortedList.size()];
                for (int i = 0; i < sortedList.size(); i++) {
                    names[i] = sortedList.get(i).getValue();
                }
                column.setCellEditor(new MyTableCellEditor(names));
            } catch (IllegalArgumentException e) {
            }
            for (String name : primaryColumns) {
                table.getColumn(name).setHeaderRenderer(new DefaultTableCellRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable jtable, Object o, boolean bln, boolean bln1, int i, int i1) {
                        Component c = super.getTableCellRendererComponent(jtable, o, bln, bln1, i, i1); //To change body of generated methods, choose Tools | Templates.
                        c.setBackground(Color.red);
                        return c;
                    }
                });
            }
        } catch (NotLoggedInException | StatementNotExecutedException | NotConnectedException | IOException  ex) {
            ex.printStackTrace();
            table.setModel(new DefaultTableModel());
        }

        revalidate();

    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel b = new ButtonPanel();
        b.addButtonToSouthEast(Loc.get("ADD"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertData();
            }
        });
        return b;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    @Override
    public void setFocus() {
    }

//    @Override
//    public NavigationImage getMainNavImage() {
//        return Images.MAIN_NAV_PLACEHOLDER;
//    }
//
//    @Override
//    public NavigationImage getSubNavImage() {
//        return Images.MAIN_NAV_PLACEHOLDER;
//    }
    @Override
    public void updateContent() {
        AbstractSynchronisationController<?,?> c = (AbstractSynchronisationController) mainFrame.getController();
        ArrayList<String> entries = c.getEntryDataTables();
//        ArrayList<String> entries = new ArrayList<>();
        ArrayList<String> asd = new ArrayList<>();
        asd.add("");

        for (String name : entries) {
            String dbName = ColumnHelper.getTableName(name);
            if (dbName.equals(TableManager.DATABASE_NAME_GENERAL)) {
                continue;
            }
            asd.add(ColumnHelper.removeDatabaseName(name));
        }
        asd.remove(ProjectRightManager.TABLENAME_PROJECTRIGHT);
        Collections.sort(asd);
        ComboBoxModel m = new DefaultComboBoxModel(asd.toArray());
        box.setModel(m);
    }

    @Override
    public void actionOnDisconnect() {
    }

    private void updateEntry(boolean primaryValueChanged, int row, int column, String old) {

        ArrayList<DataColumn> d = new ArrayList<>();

        for (int i = 0; i < table.getColumnCount(); i++) {

            String name = table.getColumnName(i);
            String value = (String) table.getValueAt(row, i);
            if (name.equals(TableManager.DELETED.getColumnName())) {

                if (value.equals(Loc.get("YES"))) {
                    value = "Y";
                } else {
                    value = "N";
                }
            } else if (name.equals(AbstractCodeTableManager.LANGUAGE.toLowerCase())) {
                for (Entry<String, String> en : language.entrySet()) {
                    if (en.getValue().equals(value)) {
                        value = en.getKey();
                    }
                }

            }
            d.add(new DataColumn(value, name));
        }

        for (DataColumn da : d) {
            System.out.println(da);
        }
//        try {
        if (primaryValueChanged) {

//                mainFrame.getController().addCodeTableEntry((String) box.getSelectedItem(), d);
            d.clear();
            for (int i = 0; i < table.getColumnCount(); i++) {

                String name = table.getColumnName(i);
                String value = (String) table.getValueAt(row, i);
                if (i == column) {
                    value = old;
                }
                if (name.equals(TableManager.DELETED.getColumnName())) {

                    value = "Y";

                } else if (name.equals(AbstractCodeTableManager.LANGUAGE.toLowerCase())) {
                    for (Entry<String, String> en : language.entrySet()) {
                        if (en.getValue().equals(value)) {
                            value = en.getKey();
                        }
                    }

                }
                d.add(new DataColumn(value, name));

            }
        }

//            mainFrame.getController().updateCodeTableEntry((String) box.getSelectedItem(), d, primaryColumns);
//        } catch (NotLoggedInException | StatementNotExecutedException | IOException| NotConnectedException ex) {
//            Logger.getLogger(AdminPanel.class.getName()).log(Level.SEVERE, null, ex);
//        }
        updateTable();
    }

    private void insertData() {
//        try {
        ArrayList<DataColumn> d = new ArrayList<>();
        for (NewEntryHolder n : datas) {
            d.add(n.getData());
        }
//            mainFrame.getController().addCodeTableEntry((String) box.getSelectedItem(), d);
        updateTable();
//        } catch (NotLoggedInException | StatementNotExecutedException| NotConnectedException| IOException ex) {
//            Logger.getLogger(AdminPanel.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }

    }

    public class MyTableModel extends DefaultTableModel {

        HashMap<String, String> map;

        private MyTableModel(Object[] up, int i, HashMap<String, String> languageMap) {
            super(up, i);
            map = languageMap;
        }

        @Override
        public void insertRow(int row, Vector rowData) {
            for (int i = 0; i < getColumnCount(); i++) {
                String name = getColumnName(i);
                if (name.equals(TableManager.DELETED.getColumnName())) {
                    Object a = rowData.get(i);
                    if (a.equals("Y")) {
                        rowData.set(i, Loc.get("YES"));
                    } else {
                        rowData.set(i, Loc.get("NO"));
                    }
                } else if (name.equals(AbstractCodeTableManager.LANGUAGE.toLowerCase())) {
                    String a = (String) rowData.get(i);
                    rowData.set(i, map.get(a));
                    if (a.equals("1")) {
                        rowData.set(i, Loc.get("german"));
                    } else if (a.equals("0")) {
                        rowData.set(i, Loc.get("int"));
                    } else if (a.equals("2")) {
                        rowData.set(i, Loc.get("english"));
                    } else if (a.equals("3")) {
                        rowData.set(i, Loc.get("french"));
                    } else if (a.equals("4")) {
                        rowData.set(i, Loc.get("spanish"));
                    }
                }
            }
            super.insertRow(row, rowData); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Object getValueAt(int row, int column) {
            String value = (String) super.getValueAt(row, column);
            String name = getColumnName(column);

            return value;
        }
    }

    public class MyTableCellEditor extends AbstractCellEditor implements TableCellEditor {

        private JComboBox editor;

        public MyTableCellEditor(String[] values) {
//             Create a new Combobox with the array of values.
            editor = new JComboBox(values);
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int rowIndex, int colIndex) {

//             Set the model data of the table
            if (isSelected) {
                editor.setSelectedItem(value);
                TableModel model = table.getModel();
                model.setValueAt(value, rowIndex, colIndex);
            }

            return editor;
        }

        @Override
        public Object getCellEditorValue() {
            return editor.getSelectedItem();
        }
    }

    public class NewEntryHolder extends JPanel {

        String columnName;
        Component c;
        ArrayList<DataColumn> mapping;

        private NewEntryHolder(String column, ArrayList<DataColumn> list) {
            super(new BorderLayout());
            Vector<String> v = new Vector<>();
            for (DataColumn h : list) {
                v.add(h.getValue());
            }
            mapping = list;
            c = add(BorderLayout.CENTER, new JComboBox<String>(v) {
                @Override
                public String toString() {
                    for (DataColumn h : mapping) {
                        if (h.getValue().equals(getSelectedItem())) {
                            return h.getColumnName();
                        }
                    }
                    return (String) getSelectedItem();
                }
            });
            columnName = column;
        }

        private NewEntryHolder(String column) {
            super(new BorderLayout());
            c = add(BorderLayout.CENTER, new JTextField() {
                @Override
                public String toString() {
                    return getText();
                }
            });
            columnName = column;

        }

        public DataColumn getData() {
            return new DataColumn(c.toString(), columnName);
        }

        @Override
        public String toString() {
            return columnName + ": " + c;
        }
    }

    public static ArrayList<Entry<String, String>> getSortedEntries(HashMap<String, String> entries) {
        ArrayList<Entry<String, String>> languageList = new ArrayList(entries.entrySet());
        java.util.Collections.sort(languageList, new Comparator<Entry<String, String>>() {
            @Override
            public int compare(Entry<String, String> o1, Entry<String, String> o2) {
                return o1.getKey().compareTo(o2.getKey());
            }
        });
        return languageList;

    }

}
