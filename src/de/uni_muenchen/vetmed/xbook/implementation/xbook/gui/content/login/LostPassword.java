package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.login;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTextBlock;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTextField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTitle;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTwoButtons;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Result;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class LostPassword extends AbstractContent {

    /**
     * The textfield to enter the user name.
     */
    private StackTextField username;
    /**
     * The textfield to enter the password.
     */
    private StackTextField email;

    /**
     * Constructor.
     */
    public LostPassword() {
        super();
        init();
    }

    @Override
    protected JPanel getContent() {

        JPanel panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        panel.add(new StackTitle(Loc.get("LOST_PASSWORD")));
        panel.add(new StackTextBlock(Loc.get("LOST_PASSWORD_INFORMATION")));
        panel.add(username = new StackTextField(Loc.get("USER")));
        panel.add(email = new StackTextField(Loc.get("EMAIL")));

        ActionListener listenerRegister = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (username.getString().isEmpty() || email.getString().isEmpty()) {
                        Footer.displayError(Loc.get("AT_LEAST_ONE_INPUT_FIELD_IS_NOT_FILLED"));
                        return;
                    }

                    // check if email address is a valid one
                    Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
                    Matcher m = p.matcher(email.getString());
                    boolean matchFound = m.matches();
                    if (!matchFound) {
                        email.clearInput();
                        email.setErrorStyle();
                        Footer.displayError(Loc.get("EMAIL_ADDRESS_INVALID"));
                        return;
                    }

                    try {
                        Message rs = ((AbstractController) mainFrame.getController()).resetPassword(username.getString(), email.getString());
                        if (rs == null) {
                            return;
                        } else if (rs.getResult().wasSuccessful()) {
                            Footer.displayConfirmation(Loc.get("PASSWORD_RESET_SUCCESSFUL"));
                            mainFrame.displayLoginScreen();
                            JOptionPane.showMessageDialog(LostPassword.this, Loc.get("PASSWORD_RESET_SUCCESSFUL_DIALOG"), Loc.get("PASSWORD_RESET_SUCCESSFUL"), JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            switch (rs.getResult().getStatus()) {
                                case Result.MAIL_OR_USERNAME_WRONG:
                                    Footer.displayError(Loc.get("ERROR_MESSAGE>MAIL_OR_USERNAME_WRONG"));
                                    break;
                                default:
                                    Footer.displayError(Loc.get("ERROR_MESSAGE>ERROR"));
                                    break;
                            }
                        }
                    } catch (NotConnectedException | NotLoggedInException | IOException ex) {
                        Footer.displayError(Loc.get("ERROR_MESSAGE>ERROR"));
                        Logger.getLogger(LostPassword.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (IsAMandatoryFieldException ex) {
                    Logger.getLogger(LostPassword.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        };

        ActionListener listenerReset = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                username.clearInput();
                email.clearInput();
            }
        };

        panel.add(new StackTwoButtons(Loc.get("REQUEST"), listenerRegister, Loc.get("RESET"), listenerReset));

        return panel;
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();
        panel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_BACK, Loc.get("BACK"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.displayLoginScreen();
            }
        });
        return panel;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

}
