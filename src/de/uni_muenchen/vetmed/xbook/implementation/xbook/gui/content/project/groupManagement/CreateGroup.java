package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.groupManagement;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel.Position;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class CreateGroup extends AbstractContent {

    private JTextField textField;

    public CreateGroup() {
        super();
        init();
    }

    @Override
    protected JPanel getContent() {
        JPanel panel = new JPanel(new StackLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);
        panel.add(new JLabel(Loc.get("CREATE_GROUP")));

        JPanel inputWrapper = new JPanel(new FlowLayout(FlowLayout.LEADING));
        inputWrapper.setBackground(Colors.CONTENT_BACKGROUND);

        textField = new JTextField();
        textField.setPreferredSize(new Dimension((int) (Sizes.BUTTON_WIDTH * 1.5), Sizes.BUTTON_HEIGHT));
        inputWrapper.add(textField);

        panel.add(inputWrapper);
        return panel;
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();

        panel.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_CANCEL, Loc.get("CANCEL"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.displayGroupManagementScreen();
            }
        });

        panel.addImageButton(Position.NORTH_EAST, Images.BUTTONPANEL_ADD, Loc.get("ADD"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
                mainFrame.displayGroupManagementScreen();
            }
        });
        return panel;
    }

    private void save() {
        if (textField.getText().isEmpty()) {
            Footer.displayError(Loc.get("PLEASE_ENTER_A_GROUP_NAME"));
            return;
        }
        try {
            Message ms = ((AbstractSynchronisationController)mainFrame.getController()).createGroup(textField.getText());
            if (ms.getResult().wasSuccessful()) {
                Footer.displayConfirmation(Loc.get("GROUP_SUCCESSFULLY_SAVED", textField.getText()));
            } else {
                Footer.displayError(Loc.get("ERROR_WHILE_SAVING_THE_GROUP", ms.getResult().toString()));
            }
        } catch (IOException | NotConnectedException | NotLoggedInException ex) {
            Footer.displayError(Loc.get("ERROR_WHILE_SAVING_THE_GROUP", textField.getText()));
        }
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    @Override
    public void setFocus() {
    }

    @Override
    public void actionOnDisconnect() {
    }
}
