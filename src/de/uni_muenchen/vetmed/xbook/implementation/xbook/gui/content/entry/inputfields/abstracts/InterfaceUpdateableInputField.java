package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts;

import java.util.Collection;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface InterfaceUpdateableInputField {

    void updateInputFieldSingle();
    
    void updateInputFieldMultiEdit();
    
    Collection<AbstractInputElement> getDependentOnFields();
}
