package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.navigation;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.navigation.INavigation;
import de.uni_muenchen.vetmed.xbook.api.gui.navigation.Prio;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.UpdatePanel;

import javax.swing.JOptionPane;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractNavigationWithSync extends AbstractNavigation {

    public AbstractNavigationWithSync(IMainFrame mainFrame) {
        super(mainFrame);
    }

    @Override
    public void loadElements() {
        super.loadElements();
        initSyncElements();
    }

    @Override
    protected void initRightsElements() {
        super.initRightsElements();

        // main navigation element
        rights = createMainNavigationElement(Prio.M_RIGHTS, Loc.get("RIGHTS"), Images.NAVIGATION_ICON_KEY,
                INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);

        createNavigationPopupElement(rights, Prio.P_RIGHTS_GROUPS, Loc.get("GROUP_MANAGEMENT"), Images.NAVIGATION_ICON_SMALL_GROUP3, new Runnable() {
            @Override
            public void run() {
                mainFrame.displayGroupManagementScreen();
            }
        }, INavigation.Mode.STANDARD, Mode.NO_PROJECT_LOADED);

        addPopupSeparator(rights, Prio.P_RIGHTS_USER_RIGHTS - 1);

        // user/group rights
        createNavigationPopupElement(rights, Prio.P_RIGHTS_USER_RIGHTS, getProjectRightsLabel() + ": " + Loc.get("USER"), Images.NAVIGATION_ICON_SMALL_PERSON, new Runnable() {
            @Override
            public void run() {
                mainFrame.displayProjectUserRightsScreen();
            }
        }, INavigation.Mode.STANDARD);

        createNavigationPopupElement(rights, Prio.P_RIGHTS_GROUP_RIGHTS, getProjectRightsLabel() + ": " + Loc.get("GROUP"), Images.NAVIGATION_ICON_SMALL_GROUP2, new Runnable() {
            @Override
            public void run() {
                mainFrame.displayProjectGroupRightsScreen();
            }
        }, INavigation.Mode.STANDARD);

    }

    @Override
    protected void initToolsElements() {
        super.initToolsElements();

        createNavigationPopupElement(tools, Prio.P_TOOLS_CODETABLES, Loc.get("RELOAD_CODE_TABLES"), Images.NAVIGATION_ICON_SMALL_RELOAD, new Runnable() {
            @Override
            public void run() {
                int n = JOptionPane.showConfirmDialog(AbstractNavigationWithSync.this, Loc.get("RELOAD_CODE_TABLES_DIALOG"), Loc.get("RELOAD_CODE_TABLES_DIALOG_TITLE"), JOptionPane.YES_NO_OPTION);
                if (n != JOptionPane.YES_OPTION) {
                    return;
                }
                UpdatePanel updateScreen = ((AbstractMainFrame) mainFrame).displayUpdateScreen(UpdatePanel.SourceType.MANUAL_CONNECTION, true, true, false, true, false);
                updateScreen.doCodeTableUpdate(true);
            }
        }, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);
    }

    /**
     * Initialised all main navigation elements and navigation popup elements
     * for the item 'sync'.
     */
    protected void initSyncElements() {
        sync = createMainNavigationElement(Prio.M_SYNC, Loc.get("SYNC"), Images.NAVIGATION_ICON_SYNCHRONISATION, new Runnable() {
            @Override
            public void run() {
                mainFrame.displaySynchronisationScreen();
            }
        }, INavigation.Mode.NO_PROJECT_LOADED, INavigation.Mode.STANDARD);
    }

}
