package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.languageChooser.LanguageChooser;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.settingPanel.AbstractSettingPanel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.fields.TwoRadioButtons;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class SettingGeneral extends AbstractSettingPanel {

    /**
     * The language chooser objet.
     */
    private final LanguageChooser languageBar;
    /**
     * The radio buttons object for displaying the launcher.
     */
    private final TwoRadioButtons displayLauncher;
    /**
     * The selection whether to skip the update screen automatically when
     * finished, or not.
     */
    private TwoRadioButtons autoSkip;

    /**
     * The basic controller object.
     */
    private final ApiControllerAccess controller;

    /**
     * Constructor.
     *
     * @param controller The basic controller object.
     */
    public SettingGeneral(ApiControllerAccess controller) {
        this.controller = controller;

        setLayout(new StackLayout());

        // language
        addTitleBar(Loc.get("LANGUAGE") + " *", false);
        addTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("LANGUAGE_SETTING_INFORMATION"));
        languageBar = addLanguageSelector();
        addTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("NOT_ALL_BOOKS_SUPPORT_ALL_LANGUAGES_INFO_TEXT"));

        // display launcher
        addTitleBar(Loc.get("DISPLAY_LAUNCHER") + " *");
        addTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("DISPLAY_LAUNCHER_SETTING_INFORMATION"));
        displayLauncher = addTwoRadioButtons(Colors.CONTENT_BACKGROUND, Loc.get("YES"), Loc.get("NO"));

        // auto skip
        if (controller instanceof AbstractSynchronisationController) {
            addTitleBar(Loc.get("AUTO_CONTINUE_AFTER_UPDATE"));
            addTextBlock(Loc.get("AUTO_CONTINUE_AFTER_UPDATE_INFO"));
            autoSkip = addTwoRadioButtons(Loc.get("YES"), Loc.get("NO"));
        }

        addTextBlock("<i><b>*)</b> " + Loc.get("RESTART_THE_APPLICATION_TO_APPLY_CHANGES") + "</i>");

        ComponentHelper.colorAllChildren(this, Colors.CONTENT_BACKGROUND);
    }

    @Override
    public void save() {
        // language
        XBookConfiguration.setProperty(XBookConfiguration.LANGUAGE, (String) languageBar.getSelectedItem());

        // display launcher
        XBookConfiguration.setProperty(XBookConfiguration.DISPLAY_LAUNCHER, displayLauncher.isYesSelected());

        // auto-skip
        if (controller instanceof AbstractSynchronisationController) {
            XBookConfiguration.setBookProperty(XBookConfiguration.AUTO_SKIP_UPDATE_WHEN_FINISHED, autoSkip.isYesSelected());
        }
    }

    @Override
    public void reset() {
        // language
        languageBar.setSelectedItem(XBookConfiguration.getProperty(XBookConfiguration.LANGUAGE));

        // display launcher
        displayLauncher.setYesSelected(XBookConfiguration.getBoolean(XBookConfiguration.DISPLAY_LAUNCHER));

        // auto-skip
        if (controller instanceof AbstractSynchronisationController) {
            autoSkip.setYesSelected(XBookConfiguration.getBoolean(XBookConfiguration.AUTO_SKIP_UPDATE_WHEN_FINISHED));
        }
    }

    @Override
    public void setDefaultValues() {
        // language
        languageBar.setSelectedItem("en");

        // display launcher
        displayLauncher.setYesSelected(false);

        // auto skip updates
        if (controller instanceof AbstractSynchronisationController) {
            autoSkip.setYesSelected(false);
        }
    }
}
