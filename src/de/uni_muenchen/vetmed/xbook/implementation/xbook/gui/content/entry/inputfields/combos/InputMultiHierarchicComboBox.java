package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IMultiHierarchicComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.DataManagementHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputMultiHierarchicComboBox extends InputHierarchicComboBox implements IMultiHierarchicComboBox {

    private DefaultListModel<DataColumn> listModel;
    private JList<DataColumn> listSelectedItems;
    protected XButton add;
    protected XButton remove;
    private JPanel panel;
    private final String table;

    public InputMultiHierarchicComboBox(ColumnType columnType, String table) {
        super(columnType);
        this.table = table;
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();

        panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(6, 12, 0, 0));

        add = new XButton("˃˃");
        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addInputToList();
            }
        });
        panel.add(BorderLayout.NORTH, add);

        listModel = new DefaultListModel<>();
        listSelectedItems = new JList<>(listModel);
        listSelectedItems.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        listSelectedItems.setLayoutOrientation(JList.VERTICAL);
        listSelectedItems.setBackground(new Color(240, 240, 240));
        listSelectedItems.addMouseMotionListener(new MouseAdapter() {

            @Override
            public void mouseMoved(MouseEvent e) {

                int index = listSelectedItems.locationToIndex(e.getPoint());
                if (index > -1) {
                    listSelectedItems.setToolTipText(convertIdToHierarchicString(listModel.getElementAt(index).getColumnName()));
                }
            }


        });

        DefaultListCellRenderer dlcr = (DefaultListCellRenderer) listSelectedItems.getCellRenderer();
        dlcr.setHorizontalAlignment(JLabel.RIGHT);

        JScrollPane scroll = new JScrollPane(listSelectedItems);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroll.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        scroll.removeMouseWheelListener(scroll.getMouseWheelListeners()[0]);
        panel.add(BorderLayout.CENTER, scroll);

        remove = new XButton("˂˂");
        remove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeSelectedItemFromList();
            }
        });
        panel.add(BorderLayout.SOUTH, remove);

        wrapperPanel.add(panel);

        if (columnType.isMandatory()) {
            panel.setBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            panel.setBackground(Colors.INPUT_FIELD_BACKGROUND);
        }

    }

    private String convertIdToHierarchicString(String id) {
        return DataManagementHelper.getHierarchicString(columnType, id, true);
    }

    /**
     * Adds the current input of the textfield into the list. Don't allow adding
     * items that are already in the list
     */
    public void addInputToList() {
        DataColumn data = dynamicComboBox.getSelectedInput();
        if (data != null) {
            if (addItemToList(data)) {
                dynamicComboBox.clear();
            }
        }
    }

    /**
     * Adds a new item to the list. Don't allow adding items that are already in
     * the list.
     *
     * @param data
     * @return <code>true</code> if the item was added successfully,
     * <code>false</code> if the item was not added because it is already in the
     * list.
     */
    public boolean addItemToList(DataColumn data) {
        if (data != null) {
            DataColumn dc = new DataColumn(DataManagementHelper.getHierarchicString(columnType, data.getColumnName(), true), data.getColumnName());
            if (!listModel.contains(dc)) {
                listModel.addElement(dc);
                listSelectedItems.scrollRectToVisible(new Rectangle(listSelectedItems.getWidth(), 0, listSelectedItems.getWidth(), 0));
                return true;
            }
        }
        Footer.displayWarning(Loc.get("NO_VALID_INPUT_ON_FIELD", columnType));
        return false;
    }

    /**
     * Removes the selected items from the list.
     */
    public void removeSelectedItemFromList() {
        int[] selected = listSelectedItems.getSelectedIndices();
        for (int i = selected.length - 1; i >= 0; i--) {
            listModel.removeElementAt(selected[i]);
        }
    }

    /**
     * Removes all items from the list.
     */
    public void removeAllItemsFromList() {
        listModel.clear();
    }

    private ArrayList<DataColumn> getListItems() {
        Object[] allItems = listModel.toArray();
        ArrayList<DataColumn> returnValue = new ArrayList<>();
        for (Object d : allItems) {
            returnValue.add((DataColumn) d);
        }
        return returnValue;
    }

    @Override
    public void load(DataSetOld data) {
        DataTableOld dataTable = data.getOrCreateDataTable(table);
        for (DataRow list : dataTable) {
            String value = list.get(columnType);
            if (value != null) {
                dynamicComboBox.setValue(value);
                addInputToList();
            }
        }
    }

    @Override
    public void save(DataSetOld data) {
        DataTableOld list = data.getOrCreateDataTable(table);
        if (list == null) {
            list = data.addDataTable(new DataTableOld(table));
        }
        for (DataColumn s : getListItems()) {
            if (s != null) {
                DataRow row = new DataRow(table);
                row.add(new DataColumn(s.getColumnName(), columnType));
                list.add(row);
            }
        }
    }

    @Override
    public boolean isValidInput() {
        return !getListItems().isEmpty();
    }

    @Override
    public void clear() {
        super.clear();
        listModel.clear();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = super.getMyFocusableComponents();
        comp.add(add);
        comp.add(remove);
        comp.add(listSelectedItems);
        return comp;
    }

    @Override
    public void actionOnFocusLost() {
        addInputToList();
        super.actionOnFocusLost();
    }

    @Override
    public String getStringRepresentation() {
        String value = "";
        for (DataColumn s : getListItems()) {
            if (s != null) {
                value += s + ", ";
            }
        }
        if (!value.isEmpty()) {
            value = value.substring(0, value.length() - ", ".length());
        }
        return value;
    }

    @Override
    public void reload() {
        super.reload();
        ArrayList<DataColumn> savedData = getListItems();
        removeAllItemsFromList();
        for (DataColumn data : savedData) {
            addItemToList(data);
        }
    }



    // ========================================================================
    @Override
    public void setSelectedValue(String id) {
        dynamicComboBox.setValue(id);
        addInputToList();
    }

    @Override
    public void setSelectedValue(List<String> ids) {
        for (String id : ids) {
            setSelectedValue(id);
        }
    }

    @Override
    public DataColumn getSelectedInput() {
        throw new UnsupportedOperationException("Not supported in multi hierarchic combo box. Please use method getSelectedInputs().");
    }

    @Override
    public List<DataColumn> getSelectedInputs() {
        return listSelectedItems.getSelectedValuesList();
    }

}
