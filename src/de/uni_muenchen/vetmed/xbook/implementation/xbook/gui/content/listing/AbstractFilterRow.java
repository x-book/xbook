package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.GhostTextField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.FilterManager;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XComboBox;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTable;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractFilterRow extends JPanel {

    protected static String SEPARATOR = " OR ";
    protected GhostTextField text;
    protected JComboBox combo;
    protected FilterManager filterManager;
    protected Filter filterSceen;
    protected Listing listingScreen;
    protected JLabel descriptionLabel;
    static private int counter = 0;
    public int id;
    protected JLabel addLabel;
    private String selectedComboValue = "";
    private boolean isUpdating = false;

    public AbstractFilterRow(final Filter filterSceen, Listing listingScreen) {
        super(new StackLayout());
        this.filterSceen = filterSceen;
        this.listingScreen = listingScreen;
        id = counter++;

        JPanel fullWrapper = new JPanel(new BorderLayout());

        addLabel = new JLabel("AND");
        add(addLabel);

        JPanel westWrapper = new JPanel(new FlowLayout());
        combo = new XComboBox();
        combo.setPreferredSize(new Dimension(160, 30));

        updateCombo();
        combo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                filterSceen.checkVisibilityAddButton();
                if (!isUpdating) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        Object selected = combo.getSelectedItem();
                        if (selected == null) {
                            selectedComboValue = "";
                        } else {
                            selectedComboValue = selected.toString();
                        }
                    }
                }
            }
        });
        combo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
//                Object selected = combo.getSelectedItem();
//                if (selected == null) {
//                    selectedComboValue = "";
//                } else {
//                    selectedComboValue = selected.toString();
//                }
            }
        });
        westWrapper.add(ComponentHelper.wrapComponent(combo, 4, 4, 4, 2));

        descriptionLabel = new JLabel("<html>" + getDescriptionLabel() + "</html>");
        descriptionLabel.setPreferredSize(new Dimension(100, 30));
        westWrapper.add(ComponentHelper.wrapComponent(descriptionLabel, 4, 4, 4, 2));

        fullWrapper.add(BorderLayout.WEST, westWrapper);

        text = new GhostTextField(getGhostText());
        text.setPreferredSize(new Dimension(1, 30));
        text.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                filterSceen.checkVisibilityAddButton();
            }
        });
        fullWrapper.add(BorderLayout.CENTER, ComponentHelper.wrapComponent(text, 4, 4, 4, 0));

        JButton removeLine = new JButton("<html><b>−</b></html>");
        removeLine.setPreferredSize(new Dimension(50, 30));
        removeLine.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeFromFilter();
            }
        });
        fullWrapper.add(BorderLayout.EAST, ComponentHelper.wrapComponent(removeLine, 4, 4, 4, 0));

        add(fullWrapper);

        ((XTable.XRowFilter) (listingScreen.table.getRowFilter())).addFilterManager(getFilterManager());
    }

    private void removeFromFilter() {
        filterSceen.removeRow(this);
    }

    public FilterManager getFilterManager() {
        if (filterManager == null) {
            filterManager = createFilterManager();
        }
        return filterManager;
    }

    public abstract FilterManager createFilterManager();

    public boolean isEmpty() {
        Object selected = combo.getSelectedItem();
        return selected == null || selected.equals("");
    }

    protected abstract String getDescriptionLabel();

    protected abstract String getGhostText();

    protected AbstractFilterRow cloneIt(Filter filter) {
        AbstractFilterRow filterClone = null;
        try {
            filterClone = this.getClass().getConstructor(Filter.class, Listing.class).newInstance(filter, listingScreen);

            filterClone.text.setDisplayText(text.getText());
            filterClone.text.setGhostText(text.getGhostText());

            for (int i = 0; i < combo.getItemCount(); i++) {
                filterClone.combo.addItem(combo.getItemAt(i));
            }
            filterClone.combo.setSelectedItem(combo.getSelectedItem());

            filterClone.descriptionLabel.setText(descriptionLabel.getText());

        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException ex) {
            Logger.getLogger(AbstractFilterRow.class.getName()).log(Level.SEVERE, null, ex);
        }

        return filterClone;
    }

    public void setAddLabelVisibile(boolean b) {
        addLabel.setVisible(b);
    }

    public void setText(String inputText) {
        text.setText(inputText);
    }

    public void setSelectedColumnType(ColumnType columnType) {
        if (columnType == null) {
            combo.setSelectedItem("");
            this.selectedComboValue = "";
        } else {
            combo.setSelectedItem(columnType.getDisplayName());
            this.selectedComboValue = columnType.getDisplayName();
        }
    }

    public void updateCombo() {
        isUpdating = true;
        combo.removeAllItems();
        int colCount = listingScreen.table.getModel().getColumnCount();
        combo.addItem("");
        for (int i = 0; i < colCount; i++) {
            combo.addItem(listingScreen.table.getModel().getColumnName(i));
        }

        combo.setSelectedItem(selectedComboValue);
        isUpdating = false;

    }

}
