package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.event.EventRegistry;
import de.uni_muenchen.vetmed.xbook.api.event.ValueEventListener;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.EntryProxy;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputIdDbnrField extends AbstractInputElement implements ValueEventListener {

    /**
     * The connected base entry manager.
     */
    private final AbstractBaseEntryManager manager;
    /**
     * The combo box object.
     */
    protected XComboBox combo;
    protected ColumnType columnTypeId;
    protected ColumnType columnTypeDbid;
    private final AbstractEntry targetEntry;
    private ArrayList<EntryDisplay> data;
    private XButton buttonAdd;

    /**
     * Constructor.
     *
     * @param columnTypeId
     * @param columnTypeDbid
     * @param manager
     * @param targetEntry
     */
    public InputIdDbnrField(ColumnType columnTypeId, ColumnType columnTypeDbid, AbstractBaseEntryManager manager,
                            AbstractEntry targetEntry) {
        super(columnTypeId);
        this.columnTypeId = columnTypeId;
        this.columnTypeDbid = columnTypeDbid;
        this.manager = manager;
        this.targetEntry = targetEntry;
    }

    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        combo = new XComboBox();
        insertData();
        combo.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        //WTF???
        combo.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<? extends Object> list, Object value, int index,
                                                          boolean isSelected, boolean cellHasFocus) {
                Object s1 = value;
                if (value != null && !value.toString().isEmpty()) {


                    list.setToolTipText(value.toString());
                    String s = value.toString();
                    s = s.replace("<html>", "");
                    s = s.replace("</html>", "");
                    s = s.replace("</b>", "");
                    s = s.replace("<b>", "");
                    s = s.replace("<br>", " | ");
                    int i = 0;
                    char[] chars = s.toCharArray();
                    int length = chars.length;
                    for (int j = 0; j < length; j++) {
                        if (chars[j] == '>') {
                            i++;
                        }
                    }
                    int min = Math.min(2, i);
                    String[] split = s.split(">");
                    int start = split.length - min - 1;
                    String newString = "";
                    for (int j = start; j < split.length; j++) {
                        newString += split[j].trim() + " > ";
                    }
                    s1 = StringHelper.cutString(newString, " > ");
                } else {
                    list.setToolTipText(null);
                    s1 = "<html></html>";
                }

                return super.getListCellRendererComponent(list, s1, index, isSelected, cellHasFocus);
            }
        });

        combo.setSelectedItem("");
        combo.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                setDefaultStyle(true);
                if (combo.getSelectedItem() == null || combo.getSelectedItem().toString().isEmpty()) {
                    combo.setToolTipText(null);
                } else {
                    combo.setToolTipText(combo.getSelectedItem().toString());
                }
            }
        });
        multiPanel.add(BorderLayout.CENTER, combo);

        if (targetEntry != null) {
            buttonAdd = new XButton(Loc.get("NEW"));
            buttonAdd.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    targetEntry.clearFields();
                    Content.setContent(targetEntry);
                    if (targetEntry instanceof EntryProxy) {
                        ((EntryProxy) targetEntry).setLinkedInputField(InputIdDbnrField.this);
                    }
                }
            });
            multiPanel.add(BorderLayout.EAST, ComponentHelper.wrapComponent(buttonAdd, 0, 0, 0, 1));
        }

        setContent(multiPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }

        EventRegistry.addValueListener(manager.getTableName(), this);
    }

    private void insertData() {
        Object selected = combo.getSelectedItem();

        combo.removeAllItems();
        try {
            Key projectKey = apiControllerAccess.getCurrentProject().getProjectKey();
            data = manager.getEntryOverview(projectKey);
            combo.addItem("");
            for (EntryDisplay entry : data) {
                combo.addItem(entry);
            }
        } catch (StatementNotExecutedException | NotLoadedException ex) {
            Logger.getLogger(InputIdDbnrField.class.getName()).log(Level.SEVERE, null, ex);
        }
        combo.setSelectedItem(selected);
        revalidate();
        repaint();
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        String id = list.get(columnType);
        String dbNumber = list.get(columnTypeDbid);
        if (id == null || dbNumber == null) {
            return;
        }
        EntryDisplay o = new EntryDisplay(id, dbNumber, "");
        combo.setSelectedItem(o);

        checkIfValueIsValid(data.getProjectKey(), new Key(id, dbNumber));
    }

    private void checkIfValueIsValid(Key projectKey, Key key) {
        if (mainFrame.getController().getFeatureConfiguration().isPlausibilityCheckAvailable()) {
            try {
                boolean b = manager.isEntryValid(projectKey, key);

                if (!b) {
                    invalidValueLoaded = true;
                    setErrorStyle();
                    JOptionPane.showMessageDialog(InputIdDbnrField.this,
                            Loc.get("INVALID_VALUES_DETECTED_DIALOG_MESSAGE", key.getID() + "/" + key.getDBID(),
                                    columnType.getDisplayName()),
                            Loc.get("INVALID_VALUES_DETECTED"),
                            JOptionPane.WARNING_MESSAGE);
                }
            } catch (StatementNotExecutedException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void clear() {
        invalidValueLoaded = false;
        setDefaultStyle(true);
        combo.setSelectedItem("");
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        list.add(new DataColumn(getSelectedID(), columnType));
        list.add(new DataColumn(getSelectedDBID(), columnTypeDbid));
    }

    public String getSelectedID() {
        String id = "-1";
        if (combo.getSelectedItem() instanceof EntryDisplay) {
            EntryDisplay input = (EntryDisplay) combo.getSelectedItem();
            id = input.getID();
        }
        return id;
    }

    public String getSelectedDBID() {
        String dbid = "-1";
        if (combo.getSelectedItem() instanceof EntryDisplay) {
            EntryDisplay input = (EntryDisplay) combo.getSelectedItem();
            dbid = input.getDatabaseId();
        }
        return dbid;
    }

    @Override
    public String getStringRepresentation() {
        return "id:" + getSelectedID() + ",dbid:" + getSelectedDBID();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(combo);
        comp.add(buttonAdd.getButton());
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !combo.getSelectedItem().equals("");
    }

    @Override
    public void reload() {
        insertData();
    }

    public ColumnType getColumnTypeDbid() {
        return columnTypeDbid;
    }

    public void selectEntry(Key key) {
        for (EntryDisplay entry : data) {
            if (entry.getID().equals("" + key.getID()) && entry.getDatabaseId().equals("" + key.getDBID())) {
                combo.setSelectedItem(entry);
                return;
            }
        }
    }

    @Override
    public void onValueUpdated() {
        //no project no update
        if (!apiControllerAccess.isProjectLoaded()) {
            return;
        }
        String id = getSelectedID();
        String dbid = getSelectedDBID();
        insertData();
        combo.setSelectedItem(new EntryDisplay(id, dbid, ""));
    }


    public void addItemListener(ItemListener listener) {
        combo.addItemListener(listener);
    }
}
