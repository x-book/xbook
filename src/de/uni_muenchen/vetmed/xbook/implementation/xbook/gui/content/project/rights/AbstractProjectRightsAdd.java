package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XSubTitle;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.GroupManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.ProjectRightManager;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 * @param <T>
 */
public abstract class AbstractProjectRightsAdd<T> extends AbstractContent {

    protected ArrayList<JCheckBox> allCheckBoxes = new ArrayList<>();
    protected JComboBox<String> combo;
    protected JTextField textField;
    protected JPanel contentPanel;

    private JCheckBox readCheck = null;
    private JCheckBox writeCheck = null;
    private JCheckBox projectEditCheck = null;

    private XTitle title;

    private final RightMode mode;

    public AbstractProjectRightsAdd(RightMode mode) {
        super();
        this.mode = mode;

        init();
        ComponentHelper.colorAllChildren(this, Colors.CONTENT_BACKGROUND);
    }

    @Override
    protected JPanel getContent() {
        contentPanel = new JPanel(new StackLayout());
        contentPanel.setBackground(Colors.CONTENT_BACKGROUND);
        contentPanel.add(title = new XTitle(""));
        if (mode == RightMode.USER_TO_PROJECT) {
            setTitle(Loc.get("ADD_RIGHTS_FOR_A_USER"));
        } else if (mode == RightMode.USER_TO_GROUP) {
            setTitle(Loc.get("ADD_USER_TO_GROUP"));
        } else if (mode == RightMode.GROUP_TO_PROJECT) {
            setTitle(Loc.get("ADD_RIGHTS_FOR_A_GROUP"));
        } else if (mode == RightMode.RANK_TO_GROUP) {
            setTitle(Loc.get("ADD_RIGHTS_FOR_A_RANK"));
        }

        JPanel inputWrapper = new JPanel(new FlowLayout(FlowLayout.LEADING));
        inputWrapper.setBackground(Colors.CONTENT_BACKGROUND);

        JPanel textFieldWrapper = new JPanel(new StackLayout());
        textField = new JTextField();

        if (mode == RightMode.USER_TO_PROJECT || mode == RightMode.USER_TO_GROUP) {
            textFieldWrapper.add(ComponentHelper.wrapComponent(new JLabel(Loc.get("ENTER_THE_USERNAME") + ":"), 0, 2, 4, 2));
        } else if (mode == RightMode.RANK_TO_GROUP) {
            textFieldWrapper.add(ComponentHelper.wrapComponent(new JLabel(Loc.get("ENTER_THE_RANKNAME") + ":"), 0, 2, 4, 2));
        }
        if (mode == RightMode.USER_TO_PROJECT || mode == RightMode.USER_TO_GROUP || mode == RightMode.RANK_TO_GROUP) {
            textField.setPreferredSize(new Dimension((int) (Sizes.BUTTON_WIDTH * 1.5), Sizes.BUTTON_HEIGHT));

            textField.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    String bak = textField.getText();
                    if (mode == RightMode.USER_TO_PROJECT || mode == RightMode.USER_TO_GROUP) {
                        combo.setSelectedItem("");
                    }
                    //otherwise either text is deleted or cursor always jumps to end of text
                    if (!textField.getText().equals(bak)) {
                        textField.setText(bak);
                    }
                }
            });
            textFieldWrapper.add(textField);
            inputWrapper.add(textFieldWrapper);
        }
        
        JPanel comboWrapper = new JPanel(new StackLayout());
        if (mode == RightMode.USER_TO_PROJECT || mode == RightMode.USER_TO_GROUP) {
            comboWrapper.add(ComponentHelper.wrapComponent(new JLabel(Loc.get("OR_SELECT_THE_USER")), 0, 2, 4, 0));
        } else if (mode == RightMode.GROUP_TO_PROJECT) {
            comboWrapper.add(ComponentHelper.wrapComponent(new JLabel(Loc.get("SELECT_THE_GROUP")), 0, 2, 4, 0));
        }

        if (mode == RightMode.USER_TO_PROJECT || mode == RightMode.USER_TO_GROUP || mode == RightMode.GROUP_TO_PROJECT) {
            combo = new JComboBox<>();
            combo.setPreferredSize(new Dimension((int) (Sizes.BUTTON_WIDTH * 1.5), Sizes.BUTTON_HEIGHT));
            combo.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (mode == RightMode.USER_TO_PROJECT || mode == RightMode.USER_TO_GROUP) {
                        textField.setText("");
                    }
                }
            });
            combo.addItem("");
            fillComboData();
            comboWrapper.add(combo);
            inputWrapper.add(comboWrapper);
        }

        contentPanel.add(inputWrapper);

        if (mode == RightMode.USER_TO_PROJECT || mode == RightMode.USER_TO_GROUP) {
            contentPanel.add(ComponentHelper.wrapComponent(new JLabel(Loc.get("USER_RIGHTS_DESCRIPTION_1")), 6, 6, 2, 6));
            contentPanel.add(ComponentHelper.wrapComponent(new JLabel(Loc.get("USER_RIGHTS_DESCRIPTION_2")), 0, 6, 0, 6));
        }

        // create check boxes
        if (mode == RightMode.USER_TO_GROUP) {
            contentPanel.add(ComponentHelper.wrapComponent(new XSubTitle(Loc.get("SELECTION_OF_THE_RANK")), 10, 0, 0, 0));
        } else {
            contentPanel.add(ComponentHelper.wrapComponent(new XSubTitle(Loc.get("SELECTION_OF_THE_RIGHTS")), 10, 0, 0, 0));
        }

        for (ColumnType c : getAvailableRights()) {
            JCheckBox check = new JCheckBox(c.getDisplayName());
            check.setName(c.getColumnName());
            check.setBackground(Colors.CONTENT_BACKGROUND);
            contentPanel.add(check);
            allCheckBoxes.add(check);

            if (c.equals(ProjectRightManager.PROJECT_RIGHTS_READ) || c.equals(GroupManager.PROJECT_RIGHT_GROUP_READ) || c.equals(GroupManager.GROUP_RIGHTS_READ)) {
                readCheck = check;
            }
            if (c.equals(ProjectRightManager.PROJECT_RIGHTS_WRITE) || c.equals(GroupManager.PROJECT_RIGHT_GROUP_WRITE) || c.equals(GroupManager.GROUP_RIGHTS_WRITE)) {
                writeCheck = check;
            }
            if (c.equals(ProjectRightManager.PROJECT_RIGHTS_EDIT_PROJECT) || c.equals(GroupManager.PROJECT_RIGHT_GROUP_EDIT_PROJECT) || c.equals(GroupManager.GROUP_RIGHTS_EDIT_PROJECT)) {
                projectEditCheck = check;
            }

        }

        if (readCheck != null && writeCheck != null) {
            readCheck.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    if (!readCheck.isSelected()) {
                        if (writeCheck.isSelected()) {
                            writeCheck.setSelected(false);
                        }
                        if (projectEditCheck.isSelected()) {
                            projectEditCheck.setSelected(false);
                        }
                    }
                }
            });
            writeCheck.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    if (writeCheck.isSelected()) {
                        if (!readCheck.isSelected()) {
                            readCheck.setSelected(true);
                        }
                    } else {
                        if (projectEditCheck.isSelected()) {
                            projectEditCheck.setSelected(false);
                        }
                    }
                }
            });
            projectEditCheck.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    if (projectEditCheck.isSelected()) {
                        if (!readCheck.isSelected()) {
                            readCheck.setSelected(true);
                        }
                        if (!writeCheck.isSelected()) {
                            writeCheck.setSelected(true);
                        }
                    }
                }
            });
        }

        return contentPanel;
    }

    protected abstract ArrayList<ColumnType> getAvailableRights();

    protected abstract ArrayList<T> getComboData();

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel panel = new ButtonPanel();

        panel.addImageButton(ButtonPanel.Position.NORTH_WEST, Images.BUTTONPANEL_CANCEL, Loc.get("CANCEL"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionAfterSaving();
            }
        });

        panel.addImageButton(ButtonPanel.Position.NORTH_EAST, Images.BUTTONPANEL_ADD, Loc.get("ADD"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
            }
        });

        return panel;
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    protected abstract void save();

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    @Override
    public void setFocus() {
    }

    @Override
    public void actionOnDisconnect() {
    }

    protected abstract void actionAfterSaving();

    protected abstract void fillComboData();

    protected class UserRightInformation {

        private ArrayList<RightsInformation> rightsInformation;
        private String userName;
        private int userID;

        public UserRightInformation(String userName, int userID, ArrayList<RightsInformation> rightsInformation) {
            this.userID = userID;
            this.userName = userName;
            this.rightsInformation = rightsInformation;
        }

        public ArrayList<RightsInformation> getRightsInformation() {
            return rightsInformation;
        }

        public int getUserID() {
            return userID;
        }

        public String getUserName() {
            return userName;
        }
    }
}
