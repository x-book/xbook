package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.sorting;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public enum SyncSortStates {
    DEFAULT, ASC, DESC
}