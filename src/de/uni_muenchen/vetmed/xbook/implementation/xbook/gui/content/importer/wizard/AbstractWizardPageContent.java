package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;

/**
 *
 * @author Eduardo Granado <eduardo.granado@unibas.ch>
 * @author Daniel Kaltenthaler
 * <daniel.kaltenthaler@palaeo.vetmed.uni-muechen.de>
 */
public abstract class AbstractWizardPageContent extends AbstractContent {

    private boolean isComplete = false;
    private ButtonPanel buttonPanel = null;
    private XButton buttonNext;
    private XButton buttonBack;
    private WizardPage page;

    public AbstractWizardPageContent() {
        super(false);
    }

    public void setPage(WizardPage page) {
        this.page = page;
    }

    public WizardPage getWizardPage() {
        return page;
    }

    @Override
    public abstract javax.swing.JPanel getContent();

    public abstract String getTitle();

    public boolean isCurrentPage() {
        return Content.getCurrentContent().equals(this);
    }

    public boolean isPageComplete() {
        return isComplete;
    }

    public abstract void reset();

    protected void setPageComplete(boolean isComplete) {
        this.isComplete = isComplete;

        if (getWizardPage().isLastPage()) {
            buttonNext.setEnabled(isComplete && getWizardPage().getWizard().canFinish());
        } else {
            buttonNext.setEnabled(isComplete);
        }
    }

    public boolean canFlipToNextPage() {
        return isPageComplete() && !page.isLastPage();
    }

    private ButtonPanel createButtonPanel() {
        buttonPanel = new ButtonPanel();

        if (getWizardPage().isLastPage()) {
            buttonNext = buttonPanel.addButtonToNorthEast("<html><b>" + Loc.get("CONFIRM") + "</b></html>", new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    buttonNext.setEnabled(false);
                    buttonBack.setEnabled(false);
                    boolean success = getWizardPage().getWizard().performFinish();

                    if (!success) {
                        System.out.println("fail");
                        buttonNext.setEnabled(true);
                        buttonBack.setEnabled(true);
                    }
                }
            });
            buttonNext.setStyle(XButton.Style.DEFAULT);
            buttonNext.setEnabled(getWizardPage().getWizard().canFinish());
        } else {
            buttonNext = buttonPanel.addButtonToNorthEast(Loc.get("NEXT"), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    try {
                        Content.setContent(getWizardPage().getNextPage().getContent());
                    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e1) {
                        e1.printStackTrace();
                    }
                }
            });
            buttonNext.setStyle(XButton.Style.DEFAULT);
            buttonNext.setEnabled(canFlipToNextPage());
        }

        if (!getWizardPage().isFirstPage()) {
            buttonBack = buttonPanel.addButtonToNorthWest(Loc.get("BACK"), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        Content.setContent(getWizardPage().getPreviousPage().getContent());
                    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            });
            buttonBack.setStyle(XButton.Style.DARKER);
        }

        return buttonPanel;
    }

    @Override
    public ButtonPanel getButtonBar() {
        if (buttonPanel == null) {
            buttonPanel = createButtonPanel();
        }
        return buttonPanel;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public void setFocus() {
        getContent().transferFocus();
    }

}
