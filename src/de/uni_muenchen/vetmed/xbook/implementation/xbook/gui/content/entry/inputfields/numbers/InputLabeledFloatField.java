package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers;

import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.ILabeledFloatField;

/**
 * An float field with a label as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputLabeledFloatField extends InputFloatField implements ILabeledFloatField {

	private final String labelText;

	/**
	 * Constructor.
	 *
	 * @param columnType The database column data of the correspondenting input field.
	 * @param labelText The text of the label.
	 */
	public InputLabeledFloatField(ColumnType columnType, String labelText) {
		super(columnType);
		this.labelText = labelText;
	}

	/**
	 * Constructor.
	 *
	 * @param columnType The database column data of the correspondenting input field.
	 * @param gridX The number of grids that are used for the height of the input field.
	 * @param gridY The number of grids that are used for the weight of the input field.
	 * @param labelText The text of the label.
	 * @param sidebar The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
	 * @param digitsAfterDecimalPoint The number of digits after the decimal point. <code>null</code> means infinite.
	 */
	@Deprecated
	public InputLabeledFloatField(ColumnType columnType, int gridX, int gridY, String labelText, SidebarPanel sidebar, Integer digitsAfterDecimalPoint) {
		super(columnType);

		setGridX(gridX);
		setGridY(gridY);
		setSidebar(sidebar);
		setDigitsAfterDecimalPoint(digitsAfterDecimalPoint);
		this.labelText = labelText;
	}

	/**
	 * Constructor.
	 *
	 * @param columnType The database column data of the correspondenting input field.
	 * @param gridX The number of grids that are used for the height of the input field.
	 * @param gridY The number of grids that are used for the weight of the input field.
	 * @param labelText The text of the label.
	 * @param sidebar The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
	 */
	@Deprecated
	public InputLabeledFloatField(ColumnType columnType, int gridX, int gridY, String labelText, SidebarPanel sidebar) {
		this(columnType, gridX, gridY, labelText, sidebar, null);
	}

	/**
	 * Constructor.
	 *
	 * @param columnType The database column data of the correspondenting input field.
	 * @param gridX The number of grids that are used for the height of the input field.
	 * @param gridY The number of grids that are used for the weight of the input field.
	 * @param labelText The text of the label.
	 * @param sidebarText The sidebar text for this input element.
	 * @param digitsAfterDecimalPoint The number of digits after the decimal point. <code>null</code> means infinite.
	 */
	@Deprecated
	public InputLabeledFloatField(ColumnType columnType, int gridX, int gridY, String labelText, String sidebarText, Integer digitsAfterDecimalPoint) {
		this(columnType, gridX, gridY, labelText, new SidebarEntryField(columnType, sidebarText), digitsAfterDecimalPoint);
	}

	/**
	 * Constructor.
	 *
	 * @param columnType The database column data of the correspondenting input field.
	 * @param gridX The number of grids that are used for the height of the input field.
	 * @param gridY The number of grids that are used for the weight of the input field.
	 * @param labelText The text of the label.
	 * @param sidebarText The sidebar text for this input element.
	 */
	@Deprecated
	public InputLabeledFloatField(ColumnType columnType, int gridX, int gridY, String labelText, String sidebarText) {
		this(columnType, gridX, gridY, labelText, new SidebarEntryField(columnType, sidebarText), null);
	}

	/**
	 * Constructor.
	 *
	 * @param columnType The database column data of the correspondenting input field.
	 * @param gridX The number of grids that are used for the height of the input field.
	 * @param gridY The number of grids that are used for the weight of the input field.
	 * @param labelText The text of the label.
	 * @param digitsAfterDecimalPoint The number of digits after the decimal point. <code>null</code> means infinite.
	 */
	@Deprecated
	public InputLabeledFloatField(ColumnType columnType, int gridX, int gridY, String labelText, Integer digitsAfterDecimalPoint) {
		this(columnType, gridX, gridY, labelText, (SidebarPanel) null, digitsAfterDecimalPoint);
	}

	/**
	 * Constructor.
	 *
	 * @param columnType The database column data of the correspondenting input field.
	 * @param gridX The number of grids that are used for the height of the input field.
	 * @param gridY The number of grids that are used for the weight of the input field.
	 * @param labelText The text of the label.
	 */
	@Deprecated
	public InputLabeledFloatField(ColumnType columnType, int gridX, int gridY, String labelText) {
		this(columnType, gridX, gridY, labelText, (SidebarPanel) null, null);
	}

	@Override
	protected void createFieldInput() {
		super.createFieldInput();

		JLabel label = new JLabel(labelText);
		label.setBorder(BorderFactory.createEmptyBorder(0, 6, 0, 6));
		label.setBackground(Colors.INPUT_FIELD_BACKGROUND);
		panel.add(BorderLayout.EAST, label);
	}
}
