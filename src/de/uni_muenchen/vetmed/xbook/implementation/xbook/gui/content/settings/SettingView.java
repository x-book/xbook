package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings;

import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.settingPanel.AbstractSettingPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.fields.TwoRadioButtons;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class SettingView extends AbstractSettingPanel {

    private final TwoRadioButtons sidebarVisible;
    private final TwoRadioButtons buttonBarOnTop;

    public SettingView() {
        setLayout(new StackLayout());

        addTitleBar(Loc.get("SIDEBAR"), false);
        addTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("SIDEBAR_SETTING_INFORMATION"));
        sidebarVisible = addTwoRadioButtons(Colors.CONTENT_BACKGROUND, Loc.get("ON"), Loc.get("OFF"));

        addTitleBar(Loc.get("BUTTON_BAR_ON_TOP") + " *");
        addTextBlock(Colors.CONTENT_BACKGROUND, Loc.get("BUTTON_BAR_ON_TOP_SETTING_INFORMATION"));
        buttonBarOnTop = addTwoRadioButtons(Colors.CONTENT_BACKGROUND, Loc.get("TOP"), Loc.get("BOTTOM"));

        addCustomContent();

        addTextBlock("<i><b>*)</b> " + Loc.get("RESTART_THE_APPLICATION_TO_APPLY_CHANGES") + "</i>");

        ComponentHelper.colorAllChildren(this, Colors.CONTENT_BACKGROUND);
    }

    @Override
    public void save() {
        setSidebarVisibilityProperty(sidebarVisible.isYesSelected());
        XBookConfiguration.setProperty(XBookConfiguration.BUTTON_BAR_ON_TOP, buttonBarOnTop.isYesSelected());
    }

    @Override
    public void reset() {
        sidebarVisible.setYesSelected(getSidebarVisibilityProperty());
        buttonBarOnTop.setYesSelected(XBookConfiguration.getBoolean(XBookConfiguration.BUTTON_BAR_ON_TOP));
    }

    @Override
    public void setDefaultValues() {
        sidebarVisible.setYesSelected(true);
        buttonBarOnTop.setYesSelected(true);
    }

    /**
     * Returns if the user has set the sidebar visibility property to true or
     * false.
     *
     * @return The user sidebar visibility property,
     */
    public static boolean getSidebarVisibilityProperty() {
        return XBookConfiguration.getBookBoolean(XBookConfiguration.SIDEBAR_VISIBLE);
    }

    /**
     * Set the user sidebar visibility property to a new value.
     *
     * @param b A boolean value to set the user sidebar visibility property.
     */
    public static void setSidebarVisibilityProperty(boolean b) {
        XBookConfiguration.setBookProperty(XBookConfiguration.SIDEBAR_VISIBLE, b);
        if (b) {
            Sidebar.showSidebar();
        } else {
            Sidebar.hideSidebar();
        }
        Footer.updateToggleSidebarButtonImages();
    }

    /**
     * Toggles the user sidebar visibility property.
     */
    public static void toggleSidebarVisibilityProperty() {
        setSidebarVisibilityProperty(!getSidebarVisibilityProperty());
    }

    /**
     * Method can be overriden to add custom constent.
     *
     * Remember to also override save/reset/setDefaultValue functionality.
     */
    public void addCustomContent() {
    }
}
