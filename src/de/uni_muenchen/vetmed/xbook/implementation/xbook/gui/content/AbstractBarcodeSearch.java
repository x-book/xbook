package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.WrapLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTextBlock;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTextField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTitle;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.StackTwoButtons;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.BarcodeHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractBarcodeSearch extends AbstractContent {

    private StackTextField barcodeInput;
    private JPanel barcodesPanel;

    private ArrayList<String> selectedBarcodes = new ArrayList<>();

    public AbstractBarcodeSearch() {
        super(true);
        init();
        ComponentHelper.colorAllChildren(this, Colors.CONTENT_BACKGROUND);
    }

    @Override
    protected JPanel getContent() {
        JPanel panel = new JPanel(new StackLayout());
        panel.add(new StackTitle(Loc.get("BARCODE_SEARCH")));
        panel.add(new StackTextBlock(getBarcodeDescriptionText()));
        panel.add(barcodeInput = new StackTextField(Loc.get("BARCODE"), (Integer) Sizes.STACK_LABEL_WIDTH_SMALL, null));
        panel.add(new StackTwoButtons(null, Loc.get("SEARCH"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        addBarcode();
                    }
                });
            }
        }, null, Loc.get("CLEAR"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                barcodeInput.clearInput();
            }
        }, Sizes.STACK_LABEL_WIDTH_SMALL));

        barcodesPanel = new JPanel(new WrapLayout());
        panel.add(barcodesPanel);

        return panel;
    }

    @Override
    public ButtonPanel getButtonBar() {
        return null;
    }

    @Override
    public SidebarPanel getSideBar() {
        SidebarPanel sidebar = new SidebarPanel();
        sidebar.addTitle(Loc.get("BARCODE_SEARCH"));
        sidebar.addTextBlock("TODO");
        return sidebar;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    private String getBarcodeDescriptionText() {
        return Loc.get("BARCODE_SEARCH_DESCRIPTION");
    }

    private void addBarcode() {
        try {
            String currentBarcode = barcodeInput.getValue();
            if (currentBarcode.isEmpty()) {
                return;
            }
            selectedBarcodes.add(barcodeInput.getValue());

            JPanel newBarcode = BarcodeHelper.getBarcode128B(currentBarcode);
            newBarcode.setName(currentBarcode);
            barcodesPanel.add(newBarcode);

            barcodesPanel.revalidate();
            barcodesPanel.repaint();
        } catch (IsAMandatoryFieldException ex) {
            // not neccessary here
        }
    }

    protected abstract void actionOnSearch();

}
