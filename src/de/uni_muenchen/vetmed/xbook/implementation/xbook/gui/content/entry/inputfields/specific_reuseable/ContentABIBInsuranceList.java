package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.specific_reuseable;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.GridBagHelper;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class ContentABIBInsuranceList extends AbstractContent {

    private final AbstractContent previousContent;
    private final InputABIBInsurance insuranceInputField;
    private final ArrayList<JCheckBox> checkBoxes;

    public ContentABIBInsuranceList(AbstractContent previousContent, InputABIBInsurance insuranceInputField) {
        this.previousContent = previousContent;
        this.insuranceInputField = insuranceInputField;
        checkBoxes = new ArrayList<>();
        init();
    }

    @Override
    protected JPanel getContent() {
        ArrayList<InsuranceLogEntry> data = insuranceInputField.getLogData();

        JPanel pp = new JPanel(new BorderLayout());
        pp.setBackground(Colors.CONTENT_BACKGROUND);

        JPanel panel = new JPanel(new GridBagLayout());
        panel.setBackground(Colors.CONTENT_BACKGROUND);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.WEST;

        GridBagHelper.setConstraints(gbc, 0, 0, 4, 1);
        panel.add(ComponentHelper.wrapComponent(new JLabel("<html><b>" + Loc.get("LAST_ENTRY") + ":</b></html>"), Colors.CONTENT_BACKGROUND, 12, 0, 6, 0), gbc);

        if (data.size() != 0) {
            InsuranceLogEntry singleData = data.get(data.size() - 1);
            GridBagHelper.setConstraints(gbc, 1, 0);
            panel.add(ComponentHelper.wrapComponent(new JLabel(singleData.getValuation() + " " + singleData.getCurrency()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
            GridBagHelper.setConstraints(gbc, 1, 1);
            panel.add(ComponentHelper.wrapComponent(new JLabel(Loc.get("ON_DATE") + " " + singleData.getDate()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
            GridBagHelper.setConstraints(gbc, 1, 2);
            panel.add(getCheck(data.size() - 1), gbc);
        }

        GridBagHelper.setConstraints(gbc, 2, 0, 4, 1);
        panel.add(ComponentHelper.wrapComponent(new JLabel("<html><b>" + Loc.get("RECENT_ENTRIES") + ":</b></html>"), Colors.CONTENT_BACKGROUND, 12, 0, 6, 0), gbc);

        int row = 3;

        for (int i = data.size() - 2; i >= 0; i--) {
            InsuranceLogEntry dataSet = data.get(i);
            GridBagHelper.setConstraints(gbc, row, 0);
            panel.add(ComponentHelper.wrapComponent(new JLabel(dataSet.getValuation() + " " + dataSet.getCurrency()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
            GridBagHelper.setConstraints(gbc, row, 1);
            panel.add(ComponentHelper.wrapComponent(new JLabel(Loc.get("ON_DATE") + " " + dataSet.getDate()), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0), gbc);
            GridBagHelper.setConstraints(gbc, row, 2);
            panel.add(getCheck(data.size() - 1), gbc);
            row++;
        }

        pp.add(BorderLayout.NORTH, panel);
        return pp;
    }

    private JPanel getCheck(int id) {
//		JCheckBox check = new JCheckBox("Löschen (" + id + ") ?");
//		check.setName(id + "");
//		checkBoxes.add(check);
//		return ComponentHelper.wrapComponent(check, Colors.CONTENT_BACKGROUND, 3, 16, 3, 0);
        return ComponentHelper.wrapComponent(new JLabel(""), Colors.CONTENT_BACKGROUND, 3, 16, 3, 0);
    }

    private void deleteSelectedRows() {
        for (int i = 0; i < checkBoxes.size(); i++) {
            if (checkBoxes.get(i).isSelected()) {
                insuranceInputField.getLogData().remove(i);
            }
        }
        Content.setContent(new ContentABIBInsuranceList(previousContent, insuranceInputField));
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel buttonPanel = new ButtonPanel();
        buttonPanel.addButtonToNorthWest(Loc.get("BACK"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Content.setContent(previousContent);
            }
        });
//		buttonPanel.addButtonToNorthCenter(Messages.get(Loc.get("DELETE")), new ActionListener() {
//
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				deleteSelectedRows();
//			}
//		});
        return buttonPanel;
    }

    @Override
    public SidebarPanel getSideBar() {
        return null;
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

}
