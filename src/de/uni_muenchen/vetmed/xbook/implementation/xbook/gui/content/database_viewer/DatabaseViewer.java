package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.database_viewer;

import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 28.09.2017.
 */
public class DatabaseViewer extends AbstractContent {

    private Connection connection;

    private TableView tableView;
    private JTextArea log;

    public DatabaseViewer() throws NotLoggedInException {
        this.connection = ((AbstractController) mainFrame.getController()).getConnection();
        init();
    }

    @Override
    protected JPanel getContent() {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Database");
        updateDatabases(root);
        JTree tree = new JTree(root) {
            @Override
            public Dimension getPreferredScrollableViewportSize() {
                return getPreferredSize();
            }
        };

        final JPanel mainPane = new JPanel(new BorderLayout());
        final JPanel contentPane = new JPanel();


        JSplitPane mainSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, contentPane, getLogArea());

        mainSplitPane.setResizeWeight(0.9);
        mainPane.add(mainSplitPane, BorderLayout.CENTER);
        final JTabbedPane tabbedResultPane = new JTabbedPane();
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setPreferredSize(new Dimension(300, 1));
        scrollPane.setSize(new Dimension(300, 1));
        scrollPane.setViewportView(tree);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        tree.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                int currentTab = tabbedResultPane.getSelectedIndex();
                if (tableView != null)
                    tabbedResultPane.remove(tableView);
                TreePath newLeadSelectionPath = e.getNewLeadSelectionPath();
                if (newLeadSelectionPath.getPathCount() == 3) {
                    Object[] path = newLeadSelectionPath.getPath();
                    tableView = new TableView(DatabaseViewer.this, path[1].toString() + "." + path[2].toString(), connection);
                    tabbedResultPane.insertTab("Data", null, tableView, "", 0);

                    tabbedResultPane.repaint();
                    tabbedResultPane.revalidate();
                    tabbedResultPane.setSelectedIndex(currentTab);
                }
            }
        });
        tabbedResultPane.addTab("Query", getQueryTab());
        contentPane.setLayout(new BorderLayout());
        contentPane.add(tabbedResultPane, BorderLayout.CENTER);

        contentPane.add(BorderLayout.WEST, scrollPane);
        return mainPane;
    }

    private Component getLogArea() {
        JScrollPane scrollPane = new JScrollPane();
        log = new JTextArea();
        scrollPane.setViewportView(log);
        return scrollPane;

    }

    private Component getQueryTab() {
        final JTabbedPane tabbedPane = new JTabbedPane();

        JScrollPane scrollPane = new JScrollPane();

        JPanel jPanel = new JPanel(new BorderLayout());
        final JTextArea textArea = new JTextArea();
        scrollPane.setViewportView(textArea);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        textArea.setPreferredSize(new Dimension(0, 100));

        JPanel buttonPanel = new JPanel(new StackLayout());
        JButton execute = new JButton("Execute");
        execute.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tabbedPane.removeAll();
                String text = textArea.getText();
                String[] split = text.split(";");
                try {
                    for (String s : split) {
                        s = s.trim();
                        String[] split1 = s.split(" ", 2);
                        addLogText(s + ";");
                        if (!split1[0].equalsIgnoreCase("select")) {
                            connection.createStatement().execute(s + ";");

                        } else {
                            ResultSet resultSet = connection.createStatement().executeQuery(s + ";");
                            JScrollPane scrollPane = new JScrollPane();
                            JTable table = new JTable();
                            scrollPane.setViewportView(table);
                            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                            DefaultTableModel defaultTableModel = new DefaultTableModel();

                            ResultSetMetaData metaData = resultSet.getMetaData();
                            String tableName = null;
                            boolean multipleTables = false;
                            int columnCount = metaData.getColumnCount();
                            Object[] identifiers = new Object[columnCount];
                            for (int i = 0; i < columnCount; i++) {
                                identifiers[i] = metaData.getColumnName(i + 1);
                                String tableName1 = metaData.getTableName(i + 1);
                                if (tableName == null) {
                                    tableName = tableName1;
                                } else if (!tableName.equalsIgnoreCase(tableName1)) {
                                    multipleTables = true;
                                }

                            }
                            defaultTableModel.setColumnIdentifiers(identifiers);
                            while (resultSet.next()) {
                                Object[] rowData = new Object[columnCount];
                                for (int i = 0; i < columnCount; i++) {
                                    rowData[i] = resultSet.getString(i + 1);
                                }
                                defaultTableModel.addRow(rowData);
                            }
                            table.setModel(defaultTableModel);

                            if (multipleTables)
                                tableName = "Result";
                            if (tableName != null)
                                tabbedPane.addTab(tableName, scrollPane);
                        }
                    }
                } catch (SQLException e1) {
                    addLogText(e1.getLocalizedMessage());
                }

            }
        });
        buttonPanel.add(execute);
        JButton clear = new JButton("Clear");
        clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.setText("");
            }
        });
        buttonPanel.add(clear);
        jPanel.add(buttonPanel, BorderLayout.EAST);
        jPanel.add(scrollPane, BorderLayout.CENTER);
        JSplitPane pane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, jPanel, tabbedPane);
        return pane;
    }

    @Override
    public ButtonPanel getButtonBar() {
        return null;
    }

    @Override
    public SidebarPanel getSideBar() {
        return new SidebarPanel();
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    private void updateDatabases(DefaultMutableTreeNode root) {
        ResultSet schemas = null;
        try {
            ResultSet catalogs = connection.getMetaData().getCatalogs();
            int columnCount = catalogs.getMetaData().getColumnCount();
            ArrayList<String> catalogList = new ArrayList<>();
            while (catalogs.next()) {
                catalogList.add(catalogs.getString(1));
                // schemaList.add(schemas.getString("SCHEMA_NAME"));
                for (int i = 0; i < columnCount; i++) {
                    System.out.print(catalogs.getString(i + 1) + " | ");

                }
                System.out.println();
            }

            boolean foundSchemas = false;
            ArrayList<String> schemaList = new ArrayList<>();
            for (String s : catalogList) {


                schemas = connection.getMetaData().getSchemas(s, "%");
                int columns = schemas.getMetaData().getColumnCount();
                System.out.println("schemas: ");
                for (int i = 0; i < columns; i++) {
                    System.out.print(schemas.getMetaData().getColumnName(i + 1) + " | ");

                }

                System.out.println();
                while (schemas.next()) {
                    foundSchemas = true;
                    schemaList.add(schemas.getString("SCHEMA_NAME"));
                    for (int i = 0; i < columns; i++) {
                        System.out.print(schemas.getString(i + 1) + " | ");
                    }
                    System.out.println();
                }
            }

            if (!foundSchemas)
                for (String s : catalogList) {
                    DefaultMutableTreeNode schema = new DefaultMutableTreeNode(s);

                    root.add(schema);
                    ArrayList<String> tableNames = new ArrayList<>();
                    String[] tblTypes = {"TABLE"};
                    ResultSet rs = connection.getMetaData().getTables(s, null,
                            "%", tblTypes);
                    while (rs.next()) {
                        tableNames.add(rs.getString("TABLE_NAME"));
                    }
                    System.out.println("Table names for:  " + s);
                    for (String tableName : tableNames) {
                        System.out.println(tableName);
                        schema.add(new DefaultMutableTreeNode(tableName));
                    }
                }
            else
                for (String s : schemaList) {

                    DefaultMutableTreeNode schema = new DefaultMutableTreeNode(s);

                    root.add(schema);
                    ArrayList<String> tableNames = new ArrayList<>();
                    String[] tblTypes = {"TABLE"};
                    ResultSet rs = connection.getMetaData().getTables(null, s,
                            "%", tblTypes);
                    while (rs.next()) {
                        tableNames.add(rs.getString("TABLE_NAME"));
                    }
                    System.out.println("Table names for:  " + s);
                    for (String tableName : tableNames) {
                        System.out.println(tableName);
                        schema.add(new DefaultMutableTreeNode(tableName));
                    }
                }


        } catch (SQLException e) {
            addLogText(e.getLocalizedMessage());

        }
    }

    public void addLogText(String text) {
        log.append(text + "\n");
    }

}
