package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.report;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * From https://stackoverflow.com/questions/29779707/getting-full-jfilechooser-input-and-preventing-other-actions-from-being-taken
 */
public class FileSaver extends JFileChooser {

    private String pngDescription = "*.png";
    private String jpgDescription = "*.png";

    public FileSaver() {
        super();
        FileNameExtensionFilter jpgFilter = new FileNameExtensionFilter(jpgDescription, "jpg");
        addChoosableFileFilter(jpgFilter);
        addChoosableFileFilter(new FileNameExtensionFilter(pngDescription, "png"));
        setAcceptAllFileFilterUsed(false);
        setFileFilter(jpgFilter);
    }

    @Override
    public void approveSelection() {
        boolean approveFileName = false;
        File newFile = getSelectedFile();
        System.out.println(newFile.getName());
        if (newFile.getName().matches(".*[\\\\/?<>:|\"%*].*")) { // Regex which matches if invalid characters present
            JOptionPane.showMessageDialog(this, "The following characters are not allowed: \\ / ? < > : | \" % *");
        } else if (newFile.exists()) {
            int overwriteReturn = JOptionPane.showConfirmDialog(this, "A file with this name already exists, would you like to overwrite it?", "File exists", JOptionPane.YES_NO_OPTION);
            if (overwriteReturn == JOptionPane.YES_OPTION) { // If the user chose to overwrite the file
                approveFileName = true;
            }
        } else {
            approveFileName = true;
        }
        if (approveFileName) {
            try {
                if (newFile.createNewFile()||newFile.exists()) {
                    super.approveSelection(); // Allow the dialog to close and the showSaveDialog() method to return APPROVE_OPTION
                }
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, "There was a problem saving the image; you might not have permission to save to the directory you chose.");
            }
        }
    }

}