package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.helper;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer.lookup.LookupTable;
import java.util.HashMap;

public class LookupTableManager {

    protected HashMap<ColumnType, LookupTable<String, String>> maping = new HashMap<>();

    public void setLookupTable(ColumnType type, LookupTable<String, String> table) {
        maping.put(type, table);
    }

    public DataColumn createDbDataHash(ColumnType type, String cellValue) {
        LookupTable<String, String> table = maping.get(type);
        String value = table.map(cellValue);
        return new DataColumn(value, type);
    }
}