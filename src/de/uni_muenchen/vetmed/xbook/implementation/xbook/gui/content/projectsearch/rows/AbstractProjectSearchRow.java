package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.rows;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.projectsearch.ProjectSearch;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractProjectSearchRow extends JPanel {

    public enum DataType {

        EQUAL, CONTAINS, BIGGER, SMALLER
    }

    public enum SearchType {

        INPUT, EMPTY, ANY
    }

    protected final int ROW_HEIGHT = 25;
    protected final int GAP_V = 2;
    protected final int GAP_H = 6;

    protected final String SEP = " OR ";

    protected final DataType dataType;
    protected SearchType searchType = SearchType.INPUT;
    protected DataType stateContainsEqual;

    protected JButton buttonContainsEquals;
    protected JLabel switchLabel;

    protected JPanel inputWrapper;
    protected ColumnType columnType;

    protected JPanel leftWrapper;
    protected JPanel rightWrapper;

    public AbstractProjectSearchRow(ColumnType columnType) {
        this(columnType, DataType.CONTAINS);
    }

    public AbstractProjectSearchRow(ColumnType columnType, DataType dataType) {
        this.dataType = dataType;
        this.columnType = columnType;

        setLayout(new BorderLayout());

        String labelText = (columnType == null) ? "(missinglabel)" : columnType.getDisplayName();
        JLabel label = new JLabel("<html><b>" + labelText + ":</b></html>", SwingConstants.RIGHT);
        label.setPreferredSize(new Dimension(200, ROW_HEIGHT));
        add(BorderLayout.WEST, ComponentHelper.wrapComponent(label, GAP_V, GAP_H, GAP_V, 0));

        inputWrapper = new JPanel(new BorderLayout());
        inputWrapper.setPreferredSize(new Dimension(1, ROW_HEIGHT));

        leftWrapper = new JPanel(new FlowLayout(SwingConstants.WEST, 0, 0));

        addSwitchLabel();
        addContainsEqualsButton();
        addCustomInputElements();

        inputWrapper.add(BorderLayout.WEST, leftWrapper);

        inputWrapper.add(BorderLayout.CENTER, getInputElements());

        rightWrapper = new JPanel(new FlowLayout(SwingConstants.WEST, 0, 0));
        inputWrapper.add(BorderLayout.EAST, rightWrapper);

        add(BorderLayout.CENTER, ComponentHelper.wrapComponent(inputWrapper, GAP_V, 0, GAP_V, 0));
    }

    public DataType getDataType() {
        return dataType;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    /**
     * Returns if there was entered any data in the row.
     *
     * @return <code>true</code> if any value was entered, <code>false</code>
     * else.
     */
    public boolean isEmpty() {
        return getInput().isEmpty();
    }

    public void switchTo(SearchType type) {
        if (type == SearchType.EMPTY) {
            switchLabel.setIcon(Images.SEARCH_SWITCH_EMPTY);
            switchLabel.setToolTipText(Loc.get("FIND_PROJECTS_WITH_NO_INPUT"));
            searchType = SearchType.EMPTY;
            setAllEnabled(false);
            clear();
        } else if (type == SearchType.ANY) {
            switchLabel.setIcon(Images.SEARCH_SWITCH_ANY);
            switchLabel.setToolTipText(Loc.get("FIND_PROJECTS_WITH_ANY_INPUT"));
            searchType = SearchType.ANY;
            setAllEnabled(false);
            clear();
        } else if (type == SearchType.INPUT) {
            switchLabel.setIcon(Images.SEARCH_SWITCH_INPUT);
            switchLabel.setToolTipText(Loc.get("FIND_PROJECTS_WITH_SPECIFIED_VALUES"));
            searchType = SearchType.INPUT;
            setAllEnabled(true);
        }
    }

    public void switchTo(DataType type) {
        if (type == DataType.EQUAL) {
            buttonContainsEquals.setText(Loc.get("EQUAL"));
            stateContainsEqual = DataType.EQUAL;
        } else if (type == DataType.CONTAINS) {
            buttonContainsEquals.setText(Loc.get("CONTAINS"));
            stateContainsEqual = DataType.CONTAINS;
        }
    }

    /**
     * Set all input elements of the row enabled or not.
     *
     * @param enabled true if the components should be enabled, false otherwise
     */
    protected void setAllEnabled(boolean enabled) {
        for (JComponent col : getAllInputElements()) {
            if (col != null) {
                col.setEnabled(enabled);
            }
        }
    }

    protected void addCustomInputElements() {
        JLabel note = new JLabel();
        note.setPreferredSize(new Dimension(70, ROW_HEIGHT));
        if ((columnType.getType() == ColumnType.Type.DATE ||columnType.getType() == ColumnType.Type.TIME )&& dataType == DataType.BIGGER) {
            note.setText(Loc.get("LATER_THAN") + ": ");
            leftWrapper.add(note);
        } else if ((columnType.getType() == ColumnType.Type.DATE ||columnType.getType() == ColumnType.Type.TIME )&& dataType == DataType.SMALLER) {
            note.setText(Loc.get("EARLIER_THAN") + ": ");
            leftWrapper.add(note);
        } else if ((columnType.getType() == ColumnType.Type.VALUE || columnType.getType() == ColumnType.Type.VALUE_THESAURUS) && dataType == DataType.BIGGER) {
            note.setText(Loc.get("BIGGER_THAN") + ": ");
            leftWrapper.add(note);
        } else if ((columnType.getType() == ColumnType.Type.VALUE || columnType.getType() == ColumnType.Type.VALUE_THESAURUS) && dataType == DataType.SMALLER) {
            note.setText(Loc.get("SMALLER_THAN") + ": ");
            leftWrapper.add(note);
        }
    }

    private void addContainsEqualsButton() {
        buttonContainsEquals = new JButton();
        switchTo(dataType);
        buttonContainsEquals.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (stateContainsEqual == DataType.CONTAINS) {
                    switchTo(DataType.EQUAL);
                } else {
                    switchTo(DataType.CONTAINS);
                }
            }
        });
        if (ProjectSearch.ACTIVATE_MAGICAL_STUFF) {
            if (dataType == DataType.CONTAINS || dataType == DataType.EQUAL) {
                leftWrapper.add(ComponentHelper.wrapComponent(buttonContainsEquals, 0, 5, 0, 0));
            }
        }
    }

    private void addSwitchLabel() {
        switchLabel = new JLabel();
        switchLabel.setSize(new Dimension(25, 25));
        switchLabel.setPreferredSize(new Dimension(25, 25));
        switchLabel.setIcon(Images.SEARCH_SWITCH_INPUT);
        switchLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (searchType == SearchType.INPUT) {
                    switchTo(SearchType.EMPTY);
                } else if (searchType == SearchType.EMPTY) {
                    switchTo(SearchType.ANY);
                } else if (searchType == SearchType.ANY) {
                    switchTo(SearchType.INPUT);
                }
            }
        });
        if (ProjectSearch.ACTIVATE_MAGICAL_STUFF) {
            leftWrapper.add(ComponentHelper.wrapComponent(switchLabel, 0, 5, 0, 0));
        }

        switchTo(SearchType.INPUT);
    }

    /**
     * Returns the corresponding ColumnType of the search row.
     *
     * @return The ColumnType.
     */
    public ColumnType getColumnType() {
        return columnType;
    }

    protected ArrayList<JComponent> getAllInputElements() {
        ArrayList<JComponent> elements = new ArrayList<>();
        elements.add(buttonContainsEquals);
        return elements;
    }

    /**
     * Returns all input elements of the row.
     *
     * @return All input elements.
     */
    protected abstract JComponent getInputElements();

    /**
     * Clears the input of the input field.
     */
    public abstract void clear();

    /**
     * Returns the input(s) that are given in the search row.
     *
     * @return The input values.
     */
    public abstract ArrayList<String> getInput();

    /**
     * Returns the raw input that was entered in the search row.
     *
     * @return The raw input.
     */
    public abstract String getRawInput();

}
