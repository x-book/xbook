package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.checkboxX2.CheckBoxX2;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.ICheckBox;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * A checkbox as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputCheckBox extends AbstractInputElement implements ICheckBox {

    /**
     * The checkbox object.
     */
    protected JCheckBox check;

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     *                   field.
     */
    public InputCheckBox(ColumnType columnType) {
        super(columnType);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     *                   field.
     * @param gridX      The number of grids that are used for the height of the
     *                   input field.
     * @param gridY      The number of grids that are used for the weight of the
     *                   input field.
     * @param sidebar    The custom sidebar for this input element.
     *                   <code>null</code> if no custom sidebar is available.
     */
    @Deprecated
    public InputCheckBox(ColumnType columnType, int gridX, int gridY, SidebarPanel sidebar) {
        super(columnType);

        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);
    }

    /**
     * Constructor.
     *
     * @param columnType  The database column data of the correspondenting input
     *                    field.
     * @param gridX       The number of grids that are used for the height of the
     *                    input field.
     * @param gridY       The number of grids that are used for the weight of the
     *                    input field.
     * @param sidebarText The sidebar text for this input element.
     */
    @Deprecated
    public InputCheckBox(ColumnType columnType, int gridX, int gridY, String sidebarText) {
        this(columnType, gridX, gridY, (new SidebarEntryField(columnType, sidebarText)));
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     *                   field.
     * @param gridX      The number of grids that are used for the height of the
     *                   input field.
     * @param gridY      The number of grids that are used for the weight of the
     *                   input field.
     */
    @Deprecated
    public InputCheckBox(ColumnType columnType, int gridX, int gridY) {
        this(columnType, gridX, gridY, (SidebarPanel) null);
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        final String s = list.get(columnType);
        if (s != null) {
            setSelected(s.equals("1"));
        }
    }

    @Override
    public void clear() {
        check.setSelected(false);
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        String saveValue = "-1";
        if (isSelected()) {
            saveValue = "1";
        }
        list.add(new DataColumn(saveValue, columnType.getColumnName()));
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(check);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    public JCheckBox getCheck() {
        return check;
    }

    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        check = new CheckBoxX2();
        if (mode == Mode.INPUT_FIELD) {
            check.setBorder(BorderFactory.createEmptyBorder(0, 61, 0, 0));
        }
        multiPanel.add(BorderLayout.CENTER, check);

        setContent(multiPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

    // ========================================================================
    @Override
    public void setSelected(boolean status) {
        check.setSelected(status);
    }

    @Override
    public boolean isSelected() {
        return check.isSelected();
    }

    @Override
    public String getStringRepresentation() {
        return "" + isSelected();
    }
    
    

}
