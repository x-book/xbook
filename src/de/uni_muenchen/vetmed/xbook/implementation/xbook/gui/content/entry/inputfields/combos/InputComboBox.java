package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;

/**
 * A combobox as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputComboBox extends AbstractInputElement implements IComboBox {

    /**
     * The combo box object.
     */
    protected XComboBox<String> combo;
    /**
     * An array that hold the available values of the combo box.
     */
    protected ArrayList<String> values;

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param values The String elements that are selectable in the combo box.
     */
    public InputComboBox(ColumnType columnType, ArrayList<String> values) {
        super(columnType);
        this.values = values;
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     * @param values The String elements that are selectable in the combo box.
     */
    @Deprecated
    public InputComboBox(ColumnType columnType, int gridX, int gridY, SidebarPanel sidebar, ArrayList<String> values) {
        super(columnType);
        this.values = values;
        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param values The String elements that are selectable in the combo box.
     */
    @Deprecated
    public InputComboBox(ColumnType columnType, int gridX, int gridY, ArrayList<String> values) {
        this(columnType, gridX, gridY, null, values);
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        final String s = list.get(columnType);
        if (s != null) {
            setSelectedValue(s);
        }
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        list.put(columnType.getColumnName(), getSelectedValue());
    }

    @Override
    public String getStringRepresentation() {
        return getSelectedValue();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(combo);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !((String) combo.getSelectedItem()).isEmpty();
    }

    public XComboBox getCombo() {
        return combo;
    }

    @Override
    protected void createFieldInput() {
        multiPanel = new JPanel();
        multiPanel.setLayout(new BorderLayout());

        combo = new XComboBox();
        combo.setModel(new DefaultComboBoxModel());
        if (values != null) {
            for (String s : values) {
                combo.addItem(s);
            }
        } else {
            System.out.println("warning: values empty for " + columnType);
        }
        combo.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        combo.setSelectedItem("");
        combo.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (combo.getSelectedItem() == null || combo.getSelectedItem().toString().isEmpty()) {
                    combo.setToolTipText(null);
                } else {
                    combo.setToolTipText(combo.getSelectedItem().toString());
                }
            }
        });
        multiPanel.add(BorderLayout.CENTER, combo);

        setContent(multiPanel);

        if (columnType.isMandatory()) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
        }
    }

    @Override
    public String getSelectedValue() {
        return combo.getSelectedItem().toString();
    }

    @Override
    public void setSelectedValue(String value) {
        combo.setSelectedItem(value);
    }

    @Override
    public void addItemToCombo(String item) {
        combo.addItem(item);
    }

    @Override
    public void addItemsToCombo(Collection<String> items) {
        for (String s : items) {
            addItemToCombo(s);
        }
    }

    @Override
    public void clearItemsFromCombo() {
        combo.clearItems();
        addItemToCombo("");
        setSelectedValue("");
    }

    @Override
    public void clear() {
        setSelectedValue("");
    }

    @Override
    public void setEditable(boolean b) {
        combo.setEditable(b);
        combo.setCustomInputEnabled(b);
    }

}
