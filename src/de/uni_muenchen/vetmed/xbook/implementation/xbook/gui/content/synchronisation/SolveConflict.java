package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.ISynchronisationManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLine;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLineGroup;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarSolveConflict;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ConflictMessageBuilder;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.*;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class SolveConflict extends AbstractContent {

    private JPanel wrapper;
    private ArrayList<SolveConflictLineGroup> lines;
    private Key entryKey;
    private Key projectKey;
    private String databaseName;
    private XImageButton deleteButton;
    private final ISynchronisationManager syncInterface;

    private AbstractMainFrame abstractMainFrame;

    public SolveConflict(DataSetOld localData, DataSetOld serverData, ISynchronisationManager syncInterface, ManagerType managerType) {
        super();
        init();
        this.syncInterface = syncInterface;
        abstractMainFrame = (AbstractMainFrame) mainFrame;

        if (managerType == ManagerType.PROJECT) {
            projectKey = localData.getProjectKey();
            databaseName = localData.getDatabaseName();

            lines = new ArrayList<>();
            setDynamicDataContent(localData, serverData, syncInterface);
            wrapper.add(getSelectAllButtons());
        } else { // if (managerType == ManagerType.ENTRY)
            if (localData instanceof EntryDataSet) {
                EntryDataSet es = (EntryDataSet) localData;
                entryKey = es.getEntryKey();
                projectKey = es.getProjectKey();
                databaseName = es.getDatabaseName();

                lines = new ArrayList<>();

                setEntryDataContent(es, serverData, syncInterface);
                wrapper.add(getSelectAllButtons());
            } else {
                throw new RuntimeException("Not a entry data set");
            }
        }
    }

    @Override
    protected JPanel getContent() {
        wrapper = new JPanel(new StackLayout());
        wrapper.setBackground(Colors.CONTENT_BACKGROUND);
        return wrapper;
    }

    private void setEntryDataContent(EntryDataSet localData, DataSetOld serverData, ISynchronisationManager syncInterface) {
        try {
            // add general information:
            wrapper.add(getSectionTitle(Loc.get("GENERAL")));
            SolveConflictLineGroup idGroup = new SolveConflictLineGroup(Loc.get("GENERAL"), false);
            HashMap<ColumnType, String> keys = new HashMap<>();
            keys.put(AbstractExtendedEntryManager.ID, localData.getEntryKey().getID() + "");
            idGroup.add(new SolveConflictLine(keys, localData.getEntryKey().getID() + "", keys,
                    localData.getEntryKey().getID() + "", AbstractExtendedEntryManager.ID.getDisplayName(), syncInterface.getTableName()));
            keys.clear();
            keys.put(AbstractExtendedEntryManager.DATABASE_ID, localData.getEntryKey().getDBID() + "");
            idGroup.add(new SolveConflictLine(keys, localData.getEntryKey().getDBID() + "", keys,
                    localData.getEntryKey().getDBID() + "", AbstractExtendedEntryManager.DATABASE_ID.getDisplayName(), syncInterface.getTableName()));
            keys.clear();
            keys.put(AbstractExtendedEntryManager.PROJECT_ID, localData.getProjectKey().getID() + "");

            idGroup.add(new SolveConflictLine(keys, localData.getProjectKey().getID() + "", keys,
                    localData.getProjectKey().getID() + "", AbstractExtendedEntryManager.PROJECT_ID.getDisplayName(), syncInterface.getTableName()));
            keys.clear();
            keys.put(AbstractExtendedEntryManager.PROJECT_DATABASE_ID, localData.getProjectKey().getDBID() + "");

            idGroup.add(new SolveConflictLine(keys, localData.getProjectKey().getDBID() + "", keys,
                    localData.getProjectKey().getDBID() + "", AbstractExtendedEntryManager.PROJECT_DATABASE_ID.getDisplayName(), syncInterface.getTableName()));
            wrapper.add(idGroup);
            lines.add(idGroup);

            if (syncInterface instanceof BaseEntryManager) {
                AbstractBaseEntryManager abm = (AbstractBaseEntryManager) syncInterface;
                getConflictForManager(abm, localData, serverData);
                for (AbstractSynchronisationManager abstractEntryManager : abm.getManagers()) {
                    getConflictForManager(abstractEntryManager, localData, serverData);
                }
            }
            for (SolveConflictLineGroup group : lines) {
                group.setProjectKey(projectKey);
                group.setEntryKey(entryKey);
            }
            wrapper.repaint();
            wrapper.revalidate();
        } catch (StatementNotExecutedException | EntriesException ex) {
            ex.printStackTrace();
        }
    }

    protected void getConflictForManager(AbstractSynchronisationManager abstractEntryManager, DataSetOld localData, DataSetOld serverData) throws EntriesException, StatementNotExecutedException, HeadlessException {
        ArrayList<SolveConflictLineGroup> templines = abstractEntryManager.getConflictLines(localData, serverData);
        if (!templines.isEmpty()) {
            wrapper.add(getSectionTitle(abstractEntryManager.getLocalisedTableName()));
            for (SolveConflictLineGroup line : templines) {
                if (abstractEntryManager instanceof BaseEntryManager) {
                    if (line.isDeletedGlobal()) {
                        JOptionPane.showMessageDialog(SolveConflict.this, Loc.get("SOLVE_CONFLICT_ENTRY_WAS_DELETED_ON_SERVER"));
                        deleteButton.setVisible(true);
                    }
                }
                wrapper.add(line);
                lines.add(line);
            }

        }
    }

    private void setDynamicDataContent(DataSetOld localData, DataSetOld serverData, ISynchronisationManager syncInterface) {
        try {
            wrapper.add(getSectionTitle(Loc.get("GENERAL")));
            SolveConflictLineGroup idGroup = new SolveConflictLineGroup(Loc.get("GENERAL"), false);
            HashMap<ColumnType, String> keys = new HashMap<>();

            keys.put(AbstractExtendedEntryManager.PROJECT_ID, localData.getProjectKey().getID() + "");
            idGroup.add(new SolveConflictLine(keys, localData.getProjectKey().getID() + "", keys, localData.getProjectKey().getID() + "", AbstractExtendedEntryManager.PROJECT_ID.getDisplayName(), syncInterface.getTableName()));

            idGroup.add(new SolveConflictLine(keys, localData.getProjectKey().getDBID() + "", keys, localData.getProjectKey().getDBID() + "", AbstractExtendedEntryManager.PROJECT_DATABASE_ID.getDisplayName(), syncInterface.getTableName()));
            wrapper.add(idGroup);
            lines.add(idGroup);

            if (syncInterface instanceof AbstractBaseProjectEntryManager) {
                AbstractBaseProjectEntryManager abm = (AbstractBaseProjectEntryManager) syncInterface;
                getConflictForManager(abm, localData, serverData);
                for (AbstractExtendedProjectEntryManager abstractEntryManager : abm.getManagers()) {
                    getConflictForManager(abstractEntryManager, localData, serverData);
                }
            }
            wrapper.repaint();
            wrapper.revalidate();
        } catch (StatementNotExecutedException | EntriesException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel buttonPanel = new ButtonPanel();
        buttonPanel.addImageButton(ButtonPanel.Position.SOUTH_WEST, Images.BUTTONPANEL_BACK, Loc.get("BACK"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.displayConflictScreen();
            }
        });

        buttonPanel.addImageButton(ButtonPanel.Position.SOUTH_WEST, Images.BUTTONPANEL_MAIL, Loc.get("NOTIFY_PROJECT_OWNER"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                abstractMainFrame.displayNotifyProjectOwnerScreen(SolveConflict.this);
            }
        });

        deleteButton = buttonPanel.addImageButton(ButtonPanel.Position.SOUTH_EAST, Images.BUTTONPANEL_DELETE, Loc.get("DELETE"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ///////////////////////////
//                try {
//                    mainFrame.getController().deleteLocalEntry(entryKey);
//                } catch (NoRightException | NotLoggedInException | StatementNotExecutedException | NotLoadedException ex) {
//                    Logger.getLogger(SolveConflict.class.getName()).log(Level.SEVERE, null, ex);
//                }
            }
        });
        deleteButton.setVisible(false);

        buttonPanel.addImageButton(ButtonPanel.Position.SOUTH_EAST, Images.BUTTONPANEL_SAVE, Loc.get("SOLVE_CONFLICT"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doSolvingConflict();
            }
        });
        return buttonPanel;
    }

    private void doSolvingConflict() {
        try {
            String bookName = mainFrame.getController().getBookName();
            String projectName = mainFrame.getController().getCurrentProject().getProjectName();
            String userName = mainFrame.getController().getDisplayName();

            String headline = "[" + bookName + "] " + Loc.get("CONFLICT_SOLVED_IN_PROJECT", projectName);

            if (!solveConflict()) {
                Logger.getLogger(SolveConflict.class.getName()).log(Level.SEVERE, Loc.get("ERROR_WHILE_SOLVING_THE_CONFLICT"));

                Footer.displayError(Loc.get("ERROR_WHILE_SOLVING_THE_CONFLICT"));
                return;
            }

            String messageString = ConflictMessageBuilder.getMessageSolved(projectName, userName, lines);
            String messageHtml = ConflictMessageBuilder.getMessageSolvedHtml(projectName, userName, lines);
            Message m = ((AbstractSynchronisationController) mainFrame.getController()).sendMessageToProjectOwner(headline, messageString, messageHtml);
            if (m.getResult().wasSuccessful()) {
                Footer.displayConfirmation(Loc.get("CONFLICT_SUCCESSFULLY_SOLVED") + " " + Loc.get("MESSAGE_SUCCESSFULLY_SENT"));
            } else {
                Footer.displayError(Loc.get("CONFLICT_SUCCESSFULLY_SOLVED") + " " + Loc.get("ERROR_WHILE_SENDING_THE_MESSAGE") + ": " + m.getResult());
            }

        } catch (NotConnectedException | IOException | NoRightException ex) {
            Logger.getLogger(NotifyProjectOwnerScreen.class.getName()).log(Level.SEVERE, null, ex);
            Footer.displayError(Loc.get("CONFLICT_SUCCESSFULLY_SOLVED") + " " + Loc.get("ERROR_WHILE_SENDING_THE_MESSAGE"));
        } catch (NotLoadedException | MissingInputException | NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(SolveConflict.class.getName()).log(Level.SEVERE, null, ex);
            Footer.displayError(Loc.get("ERROR_WHILE_SOLVING_THE_CONFLICT"));
        }
    }

    @Override
    public SidebarPanel getSideBar() {
        return new SidebarSolveConflict();
    }

    @Override
    public boolean forceSidebar() {
        return true;
    }

    private JPanel getSelectAllButtons() {
        JPanel selectButtonPanel = new JPanel(new GridLayout(1, 3));
        selectButtonPanel.setBorder(BorderFactory.createEmptyBorder(6, 0, 6, 0));
        selectButtonPanel.setBackground(Colors.CONTENT_BACKGROUND);
        XImageButton buttonLocal = new XImageButton(Loc.get("USE_LOCAL_DATA"));
        buttonLocal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectAllLocalValues();
            }
        });
        selectButtonPanel.add(buttonLocal);
        selectButtonPanel.add(new JLabel(""));
        XImageButton buttonServer = new XImageButton(Loc.get("USE_SERVER_DATA"));
        buttonServer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectAllServerValues();
            }
        });
        selectButtonPanel.add(buttonServer);
        return selectButtonPanel;
    }

    private void selectAllLocalValues() {
        for (SolveConflictLineGroup group : lines) {
            for (SolveConflictLine line : group.getLines()) {
                line.selectLocal();
            }
        }
    }

    public void selectAllServerValues() {
        for (SolveConflictLineGroup group : lines) {
            for (SolveConflictLine line : group.getLines()) {
                line.selectServer();
            }
        }
    }

    public boolean solveConflict() throws MissingInputException, StatementNotExecutedException {
        DataSetOld entryData;
        if (entryKey == null) {
            entryData = new DataSetOld(projectKey, databaseName, syncInterface.getTableName());
        } else {
            entryData = new EntryDataSet(entryKey, projectKey, databaseName, syncInterface.getTableName());
        }
        for (SolveConflictLineGroup line : lines) {
            line.save(entryData, false);
        }
        DataRow dataRow = entryData.getDataRow(syncInterface.getTableName());
        if (dataRow.isEmpty()) {
            dataRow.put(AbstractSynchronisationManager.PROJECT_ID, projectKey.getID() + "");
            dataRow.put(AbstractSynchronisationManager.PROJECT_DATABASE_ID, projectKey.getDBID() + "");
            if(entryKey != null){
                dataRow.put(AbstractSynchronisationManager.ID, entryKey.getID() + "");
                dataRow.put(AbstractSynchronisationManager.DATABASE_ID, entryKey.getDBID() + "");

            }
        }
        IBaseManager base = (IBaseManager) syncInterface;
        //solve conflict without updating the status
        base.solveConflict(entryData);
        if (entryKey == null) {
            entryData = new DataSetOld(projectKey, databaseName, syncInterface.getTableName());
        } else {
            entryData = new EntryDataSet(entryKey, projectKey, databaseName, syncInterface.getTableName());
        }
        for (SolveConflictLineGroup line : lines) {
            line.save(entryData, true);
        }

        try {
            //update the global entry with the global status
            if (!((AbstractSynchronisationController) mainFrame.getController()).solveConflict(entryData, base)) {
                base.setConflicted(entryData);
                return false;

            }
        } catch (NotLoadedException | NotConnectedException | NoRightException | NotLoggedInException | IOException ex) {
            Logger.getLogger(SolveConflict.class.getName()).log(Level.SEVERE, null, ex);
            base.setConflicted(entryData);
        }

        Footer.displayConfirmation(Loc.get("CONFLICT_SOLVED"));
        mainFrame.displayConflictScreen();
        return true;
    }

    private JPanel getSectionTitle(String title) {
        JPanel panel = new JPanel(new GridLayout(1, 3));
        panel.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));

        panel.setBackground(Color.BLACK);
        panel.add(new JLabel(""));
        JLabel p = new JLabel(title);
        p.setHorizontalAlignment(SwingConstants.CENTER);
        p.setBackground(Color.BLACK);
        p.setOpaque(true);
        p.setForeground(Color.WHITE);
        panel.add(p);
        panel.add(new JLabel(""));
        return panel;
    }

    public ArrayList<SolveConflictLineGroup> getLines() {
        return lines;
    }

}
