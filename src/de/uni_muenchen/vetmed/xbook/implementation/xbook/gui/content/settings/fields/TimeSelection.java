package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.fields;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 * A panel that holds a time input possibility.
 */
public class TimeSelection extends JPanel {
    /**
     * The textfield to enter the hour.
     */
    private JTextField hour;
    /**
     * The textfield to enter the minute.
     */
    private JTextField minute;

    /**
     * Constructor.
     *
     * @param background The background color.
     */
    public TimeSelection(Color background) {
        setLayout(new BorderLayout());
        setBackground(background);

        JPanel subPanel = new JPanel(new FlowLayout());
        subPanel.setBackground(background);

        hour = new JTextField("");
        hour.setHorizontalAlignment(JTextField.CENTER);
        hour.setPreferredSize(new Dimension(35, 22));
        hour.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                checkValues();
            }
        });

        minute = new JTextField("");
        minute.setHorizontalAlignment(JTextField.CENTER);
        minute.setPreferredSize(new Dimension(35, 22));
        minute.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                checkValues();
            }
        });

        subPanel.add(hour);
        subPanel.add(new JLabel(":"));
        subPanel.add(minute);
        subPanel.add(new JLabel(" (00:00 - 23:59)"));

        add(BorderLayout.WEST, subPanel);
    }

    /**
     * Set the default value for the hour.
     */
    private void setToDefaultHour() {
        hour.setText("00");
    }

    /**
     * Set the default value for the minute.
     */
    private void setToDefaultMinute() {
        minute.setText("00");
    }

    /**
     * Set the default value.
     */
    public void setToDefault() {
        setToDefaultHour();
        setToDefaultMinute();
    }

    /**
     * Check the values (incl. formatting) and reset to default value if the value is invalid.
     */
    private void checkValues() {
        // check hour
        boolean hourCorrect = true;
        try {
            int h = Integer.parseInt(hour.getText());
            if (h < 0 || h > 23) {
                hourCorrect = false;
            }
            if (h >= 0 && h <= 9) {
                hour.setText("0" + h);
            }
        } catch (NumberFormatException nfe) {
            hourCorrect = false;
        }
        if (!hourCorrect) {
            setToDefaultHour();
        }

        // check minute
        boolean minuteCorrect = true;
        try {
            int m = Integer.parseInt(minute.getText());
            if (m < 0 || m > 59) {
                minuteCorrect = false;
            }
            if (m >= 0 && m <= 9) {
                minute.setText("0" + m);
            }
        } catch (NumberFormatException nfe) {
            minuteCorrect = false;
        }
        if (!minuteCorrect) {
            setToDefaultMinute();
        }
    }

    /**
     * Returns the minute value.
     *
     * @return The minute value.
     */
    public int getMinute() {
        return Integer.parseInt(minute.getText());
    }

    /**
     * Returns the hour value.
     *
     * @return The hour value.
     */
    public int getHour() {
        return Integer.parseInt(hour.getText());
    }

    /**
     * Sets the minute value.
     *
     * @param min The minute value.
     */
    public void setMinute(int min) {
        minute.setText("" + min);
        checkValues();
    }

    /**
     * Sets the hour value.
     *
     * @param hr The hour value.
     */
    public void setHour(int hr) {
        hour.setText("" + hr);
        checkValues();
    }

    /**
     * Enables or disables the input fields.
     *
     * @param b The enability status.
     */
    public void setEnabled(boolean b) {
        hour.setEnabled(b);
        minute.setEnabled(b);
    }
}
