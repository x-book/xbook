package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.table.CustomTableModel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.FilterManager;
import java.awt.Color;
import java.util.Date;
import javax.swing.RowFilter;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class FilterRowEarlierThanDate extends AbstractFilterRow {

    public FilterRowEarlierThanDate(Filter filterSceen, Listing listingScreen) {
        super(filterSceen, listingScreen);
    }

    @Override
    public FilterManager createFilterManager() {
        return new FilterManager() {
            @Override
            public boolean include(RowFilter.Entry<? extends CustomTableModel, ? extends Integer> entry) {
                int columnId = entry.getModel().findColumn((String) combo.getSelectedItem());
                if (columnId != -1) {
                    String value = entry.getStringValue(columnId);
                    if (value.isEmpty()) {
                        return false;
                    }
                    try {
                        // check if the value has the format "yyyy-mm-dd"
                        String[] valueSplitted = value.split("-");
                        Integer valueYear = Integer.parseInt(valueSplitted[0]);
                        Integer valueMonth = Integer.parseInt(valueSplitted[1]);
                        Integer valueDay = Integer.parseInt(valueSplitted[2]);
                        // create a new Date object
                        Date valueDate = new Date(valueYear - 1900, valueMonth - 1, valueDay);

                        // check if the value has the format "yyyy-mm-dd"
                        String[] filterSplitted = text.getText().split("-");
                        Integer filterYear = Integer.parseInt(filterSplitted[0]);
                        Integer filterMonth = Integer.parseInt(filterSplitted[1]);
                        Integer filterDay = Integer.parseInt(filterSplitted[2]);
                        // create a new Date object
                        Date filterDate = new Date(filterYear - 1900, filterMonth - 1, filterDay);
                        return valueDate.before(filterDate);
                    } catch (NumberFormatException nfe) {
                        // not a number, not compareable
                        return false;
                    }
                }
                return true;
            }
        };
    }

    @Override
    protected String getDescriptionLabel() {
        return "<b>" + Loc.get("EARLIER_THAN").toUpperCase() + "</b>";
    }

    @Override
    protected String getGhostText() {
        return Loc.get("DATE") + " (yyyy-mm-dd)";
    }

}
