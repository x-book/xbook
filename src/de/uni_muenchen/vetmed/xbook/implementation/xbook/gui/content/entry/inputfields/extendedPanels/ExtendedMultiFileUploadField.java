package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.extendedPanels;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTableOld;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawFileUploadField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.IEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.AbstractExtendedInputContent;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputMultiFileUploadField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectEdit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ExtendedMultiFileUploadField extends AbstractExtendedInputContent {

    /**
     * The basic logger object.
     */
    private Log LOGGER = LogFactory.getLog(RawFileUploadField.class);

    /**
     * The wrapper panel.
     */
    private JPanel wrapper;
    /**
     * The corresponding multi file upload field object.
     */
    private InputMultiFileUploadField inputField;
    private ColumnType columnTypeFileName;
    private ColumnType columnTypeTypeExtension;
    private ColumnType columnTypeFileMustBeSynced;
    private ColumnType columnTypeUUID;
    private ArrayList<ExtendedRawFileUploadField> allRows = new ArrayList<>();
    private JPanel rowsWrapper;
    private ArrayList<FileNameExtensionFilter> fileFilter;
    private String tableFile;
    /**
     * The basic controller object.
     */
    private ApiControllerAccess controller;

    public ExtendedMultiFileUploadField(ApiControllerAccess controller, ColumnType columnTypeFileName,
                                        ColumnType columnTypeTypeExtension, ColumnType columnTypeFileMustBeSynced,
                                        ColumnType columnTypeUUID, String tableFile, IEntry entry,
                                        InputMultiFileUploadField inputField,
                                        ArrayList<FileNameExtensionFilter> fileFilter) {
        super(entry);
        this.inputField = inputField;
        this.columnTypeFileName = columnTypeFileName;
        this.columnTypeTypeExtension = columnTypeTypeExtension;
        this.columnTypeFileMustBeSynced = columnTypeFileMustBeSynced;
        this.columnTypeUUID = columnTypeUUID;
        this.fileFilter = fileFilter;
        this.tableFile = tableFile;
        this.controller = controller;
        init();
    }

    public ExtendedMultiFileUploadField(ApiControllerAccess controller, ColumnType columnTypeFileName,
                                        ColumnType columnTypeTypeExtension, ColumnType columnTypeFileMustBeSynced,
                                        ColumnType columnTypeUUID, String tableFile, AbstractProjectEdit projectEdit,
                                        InputMultiFileUploadField inputField,
                                        ArrayList<FileNameExtensionFilter> fileFilter) {
        super(projectEdit);
        this.inputField = inputField;
        this.columnTypeFileName = columnTypeFileName;
        this.columnTypeTypeExtension = columnTypeTypeExtension;
        this.columnTypeFileMustBeSynced = columnTypeFileMustBeSynced;
        this.columnTypeUUID = columnTypeUUID;
        this.fileFilter = fileFilter;
        this.tableFile = tableFile;
        this.controller = controller;
        init();
    }

    @Override
    protected JPanel getContent() {
        wrapper = new JPanel(new StackLayout());
        wrapper.setBackground(Colors.CONTENT_BACKGROUND);

        wrapper.add(new XTitle(columnTypeFileName.getDisplayName()));

        JPanel buttonAddWrapper = new JPanel(new BorderLayout());
        buttonAddWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        JPanel buttonAddWrapper2 = new JPanel(new FlowLayout());
        buttonAddWrapper2.setBackground(Colors.CONTENT_BACKGROUND);

        // button to load several files at the same time
        XButton buttonLoadMulti = new XButton(Loc.get("ADD"));
        buttonLoadMulti.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                chooser.setMultiSelectionEnabled(true);
                chooser.setAcceptAllFileFilterUsed(false);
                for (FileNameExtensionFilter filter : fileFilter) {
                    chooser.addChoosableFileFilter(filter);
                }
                int returnVal = chooser.showOpenDialog(ExtendedMultiFileUploadField.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File[] files = chooser.getSelectedFiles();
                    for (File file : files) {
                        ExtendedRawFileUploadField row = addRow(UUID.randomUUID().toString());
                        row.getField().setText(file.getAbsolutePath().replace("\\", "/"));
                    }
                }
            }
        });
        buttonAddWrapper2.add(buttonLoadMulti);

        buttonAddWrapper.add(BorderLayout.WEST, buttonAddWrapper2);
        wrapper.add(ComponentHelper.wrapComponent(buttonAddWrapper, Colors.CONTENT_BACKGROUND, 10, 0, 10, 0));

        rowsWrapper = new JPanel(new StackLayout());
        wrapper.add(rowsWrapper);

        return wrapper;
    }

    /**
     * Adds a new row object and return it.
     *
     * @return The added row.
     */
    private ExtendedRawFileUploadField addRow(String uuid) {
        ExtendedRawFileUploadField row = getNewRow(uuid);
        addRow(row);
        return row;
    }

    /**
     * Adds an existing row object and return it.
     *
     * @param row The row to add.
     * @return The added row.
     */
    private ExtendedRawFileUploadField addRow(ExtendedRawFileUploadField row) {
        // first remove all empty rows, so that an new empty row can be inserted afterwards without being removed again
        removeEmptyRows(); // also updates the rows
        rowsWrapper.add(row);
        allRows.add(row);
        return row;
    }

    /**
     * Creates the object of a new row and returns it.
     *
     * @return The new row.
     */
    private ExtendedRawFileUploadField getNewRow(String uuid) {
        ExtendedRawFileUploadField row = new ExtendedRawFileUploadField(controller, columnTypeFileName,
                columnTypeTypeExtension,
                columnTypeFileMustBeSynced, fileFilter, uuid);
        ComponentHelper.colorAllChildren(row, Colors.CONTENT_BACKGROUND);
        return row;
    }

    @Override
    public void setFocus() {
    }

    @Override
    public void actionOnDisconnect() {
    }

    @Override
    public SidebarPanel getSideBar() {
        return new SidebarPanel("Sidebar TODO");
    }

    /**
     * Saves the data.
     *
     * @param data
     */
    @Override
    public void save(DataSetOld data) {
        DataTableOld list = data.getOrCreateDataTable(tableFile);
        for (ExtendedRawFileUploadField row : allRows) {
            if (row.field.isValidInput()) {
                DataRow dataRow = new DataRow(tableFile);
                row.field.save(dataRow);
                dataRow.put(columnTypeUUID, row.uuid);
                list.add(dataRow);
            }
        }
    }

    /**
     * Clears the panel.
     */
    public void clear() {
        allRows.clear();
        inputField.removeIcon();
    }

    /**
     * Load data.
     *
     * @param data
     */
    public void load(DataSetOld data) {
        DataTableOld dataTable = data.getOrCreateDataTable(tableFile);
        for (DataRow list : dataTable) {
            String uuid = list.get(columnTypeUUID);
            ExtendedRawFileUploadField row = addRow(uuid);
            row.field.load(list);
        }
        updateRows();
        System.out.println(allRows.size());
    }

    /**
     * Returns the current entered data.
     *
     * @return The current entered data.
     */
    public ArrayList<String> getCurrentData() {
        ArrayList<String> data = new ArrayList<>();
        for (ExtendedRawFileUploadField row : allRows) {
            String path = row.getField().getTextField().getText();
            if (!path.isEmpty()) {
                data.add(path);
            }
        }
        return data;
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    @Override
    public String getStringRepresentation() {
        String value = "";
        for (String s : getCurrentData()) {
            value += s + ",";
        }
        return StringHelper.cutString(value, ",");
    }

    /**
     * Repaints the row wrapper and uploads the GUI elements of all rows.
     */
    private void updateRows() {
        rowsWrapper.removeAll();
        ArrayList<ExtendedRawFileUploadField> allRowsCopy = new ArrayList<>();
        for (ExtendedRawFileUploadField row : allRows) {
            allRowsCopy.add(row);
        }
        for (ExtendedRawFileUploadField row : allRowsCopy) {
            rowsWrapper.add(row);
        }
        revalidate();
        repaint();

        // update the icon on the button of the corresponding input field.
        if (allRows.isEmpty()) {
            inputField.removeIcon();
        } else {
            inputField.setDefaultIcon();
        }
    }

    @Override
    public boolean checkContent() {
        boolean status = true;
        for (ExtendedRawFileUploadField row : allRows) {
            if (row.getField().isValidPath()) {
                continue;
            } else {
                row.getField().getTextField().setForeground(Color.red);
                status = false;
            }
        }
        if (!status) {
            Footer.displayWarning(Loc.get("THERE_ARE_INVALID_PATHS"));
        }
        return status;
    }

    /**
     * Removes all empty rows from the logic and replaint the GUI afterwards.
     */
    private void removeEmptyRows() {
        ArrayList<ExtendedRawFileUploadField> allRowsCopy = new ArrayList<>();
        for (ExtendedRawFileUploadField row : allRows) {
            allRowsCopy.add(row);
        }
        for (ExtendedRawFileUploadField row : allRowsCopy) {
            if (row.getField().getText().isEmpty()) {
                allRows.remove(row);
            }
        }
        updateRows();
    }

    /**
     * A single row that basically holds a file upload field but which is adjusted to the functionality of the multi
     * file upload field.
     */
    private class ExtendedRawFileUploadField extends JPanel {
        /**
         * A file upload field which functionality is used as a basic for this row.
         */
        private RawFileUploadField field;

        private String uuid;

        /**
         * Constructor.
         *
         * @param controller                 The basic controller object.
         * @param columnTypeFileName         The corresponding column type for file name.
         * @param columnTypeFileExtension    The corresponding column type for file extension.
         * @param columnTypeFileMustBeSynced The corresponding column type for the information whether the file has to
         *                                   be synchronized or not.
         * @param fileFilter                 The file filter options.
         * @param uuid
         */
        public ExtendedRawFileUploadField(ApiControllerAccess controller, ColumnType columnTypeFileName,
                                          ColumnType columnTypeFileExtension, ColumnType columnTypeFileMustBeSynced,
                                          ArrayList<FileNameExtensionFilter> fileFilter, String uuid) {
            super(new BorderLayout());
            this.uuid = uuid;

            // initialize and adjust the file upload field
            field = new RawFileUploadField(controller, columnTypeFileName, columnTypeFileExtension,
                    columnTypeFileMustBeSynced, fileFilter) {
                @Override
                public void clear() {
                    allRows.remove(ExtendedRawFileUploadField.this);
                    updateRows();
                }

                @Override
                protected void onInputChanged() {
                    super.onInputChanged();
                    field.getTextField().setForeground(Color.BLACK);
                }
            };
            field.getButtonView().setText(Loc.get("EDIT"));
            field.getLabelStatus().setHorizontalTextPosition(SwingConstants.LEFT);

            JPanel element = ComponentHelper.wrapComponent(field, Colors.CONTENT_BACKGROUND, 4, 10, 4, 10);
            add(BorderLayout.CENTER, element);
        }

        /**
         * Returns the file upload field object.
         *
         * @return The file upload field object.
         */
        public RawFileUploadField getField() {
            return field;
        }

    }

}
