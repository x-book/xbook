package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts;

import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import java.util.ArrayList;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface InterfaceAddableData {

    public void addData(ArrayList<Key> keys);

    public void addData(Key key);

}
