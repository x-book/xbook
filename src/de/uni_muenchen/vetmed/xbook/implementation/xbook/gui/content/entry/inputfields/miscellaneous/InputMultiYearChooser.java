package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiYearChooser;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawMultiYearChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IMultiYearChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiYearElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputMultiYearChooser extends AbstractDefaultInputElement implements IMultiYearChooser {

    private final ColumnType columnTypeEnd;
    /**
     * The text field object.
     */
    protected RawMultiYearChooser zField;

    public InputMultiYearChooser(ColumnType columnTypeBegin, ColumnType columnTypeEnd) {
        super(columnTypeBegin);
        this.columnTypeEnd = columnTypeEnd;

        setGridX(2);
        setGridY(2);
    }

    @Override
    public AbstractInputElementFunctions createInputField() {
        if (zField == null) {
            zField = new RawMultiYearChooser(columnType, columnTypeEnd);
        }
        return zField;
    }

    @Override
    public void setMode(Mode mode) {
        super.setMode(mode);
        if (mode == Mode.STACK) {
            setPreferredSize(new Dimension(Sizes.STACK_DEFAULT_WIDTH, 100));
        }
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return zField.getMyFocusableComponents();
    }

    @Override
    public boolean addElement(MultiYearElement element) {
        return zField.addElement(element);
    }

    @Override
    public boolean addElements(List<MultiYearElement> element) {
        return zField.addElements(element);
    }

    @Override
    public List<MultiYearElement> getElementsFromList() {
        return zField.getElementsFromList();
    }

    @Override
    public MultiYearElement getElementFromInput() {
        return zField.getElementFromInput();
    }

    @Override
    public void removeElement(MultiYearElement element) {
        zField.removeElement(element);
    }

    @Override
    public void removeElements(List<MultiYearElement> elements) {
        zField.removeElements(elements);
    }

    @Override
    public void removeElementAt(int index) {
        zField.removeElementAt(index);
    }

    public MultiYearChooser getMultiYearChooser() {
        return zField.getMultiYearChooser();
    }

}
