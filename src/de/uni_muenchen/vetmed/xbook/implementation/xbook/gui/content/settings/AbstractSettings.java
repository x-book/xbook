package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.*;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.settingPanel.AbstractSettingPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.ButtonPanel.Position;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.content.CannotBeReturnedOnPressingBackButton;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarSettings;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SettingPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;

/**
 * An abstract class to implement the basic setting options that are necessary for all xBooks.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractSettings extends AbstractContent implements CannotBeReturnedOnPressingBackButton {

    /**
     * The setting panel.
     */
    protected SettingPanel setting;
    /**
     * The setting panel for user data.
     */
    private SettingProfile settingUserData;

    /**
     * Constructor.
     */
    public AbstractSettings() {
        super(false);
        init();
    }

    @Override
    protected javax.swing.JPanel getContent() {
        setting = new SettingPanel(SettingPanel.NavigationPosition.WEST, Colors.CONTENT_BACKGROUND);

        setting.addNavigationElement(Loc.get("GENERAL"), getSettingsGeneral());
        if (mainFrame.getController() instanceof AbstractSynchronisationController) {
            setting.addNavigationElement(Loc.get("SYNCHRONISATION"), getSettingSync());
        }
        setting.addNavigationElement(Loc.get("VIEW"), getSettingsView());
        setting.addNavigationElement(Loc.get("PROFILE"), settingUserData = getSettingsProfile());
        addCustomContent();

        setting.setDefaultValues();
        setting.reset();

        setting.setButtonPanelVisible(false);
        return setting;
    }

    /**
     * Additional settings or custom content for specific books.
     */

    protected abstract void addCustomContent();

    /**
     * Checks if there has been done any input into the password fields. If yes print a message if the saving progress should be continued.
     * Returns the status if should continue or not.
     *
     * @return The status if the saving progress should continue or not.
     */
    private boolean checkPasswordFieldBeforeSaving() {
        if (settingUserData.isInputInPasswordFields()) {
            int i = JOptionPane.showConfirmDialog(
                    (AbstractMainFrame) mainFrame, Loc.get("PASSWORD_CHANGE_NOT_SAVED_CONTINUE"),
                    Loc.get("VALUES_FOR_CHANGING_PASSWORD_WERE_FOUND"),
                    JOptionPane.YES_NO_OPTION);
            if (i == 0) { // ja
                settingUserData.clearPasswordFields();
                return true;
            } else if (i == 1) { // nein
                return false;
            }
        }
        return true;
    }

    @Override
    public ButtonPanel getButtonBar() {
        ButtonPanel b = new ButtonPanel();
        b.addImageButton(Position.NORTH_EAST, Images.BUTTONPANEL_SAVE, Loc.get("SAVE"), Sizes.MODIFIER_BIG_BUTTON, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!checkPasswordFieldBeforeSaving()) {
                    return;
                }
                setting.save();
                Footer.displayConfirmation(Loc.get("SETTINGS_SUCCESSFULLY_SAVED"));
            }
        });

        // necessary because otherway there is no chance to get back to the login screen
        if (!mainFrame.getController().isLoggedIn()) {
            b.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_BACK, Loc.get("BACK"), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    mainFrame.displayLoginScreen();
                }
            });
        }

        b.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_REVERT, Loc.get("RESET_UNSAVED_CHANGES"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setting.reset();
                Footer.displayWarning(Loc.get("UNSAVED_CHANGES_WERE_RESTORED"));
            }
        });

        b.addImageButton(Position.NORTH_WEST, Images.BUTTONPANEL_DEFAULT, Loc.get("RESET_TO_DEFAULT_VALUES"), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setting.setDefaultValues();
                Footer.displayWarning(Loc.get("SETTINGS_WERE_RESTORED_TO_DEFAULT_VALUES"));
            }
        });

        return b;
    }

    @Override
    public SidebarPanel getSideBar() {
        return new SidebarSettings();
    }

    @Override
    public boolean forceSidebar() {
        return false;
    }

    @Override
    public void setFocus() {
    }

    @Override
    public void actionOnDisconnect() {
    }

    protected SettingSync getSettingSync() {
        return new SettingSync(((AbstractSynchronisationController) mainFrame.getController()));
    }

    protected SettingProfile getSettingsProfile() {
        return new SettingProfile((AbstractMainFrame) mainFrame);
    }

    protected SettingView getSettingsView() {
        return new SettingView();
    }

    public AbstractSettingPanel getSettingsGeneral() {
        return new SettingGeneral(mainFrame.getController());
    }
}
