package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.listing;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportResult;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectOverview;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 17.02.2017.
 */
public class ProjectListing extends Listing {

    @Override
    protected JPanel getContent() {
        JPanel c = super.getContent();
        buttonUpdateList.setVisible(false);
        deleteButton.setVisible(false);
        checkShowDatabaseInfo.setVisible(false);
        return c;
    }

    @Override
    protected void createTable() {
        super.createTable();
        activateRightClickSelection();
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        addPopupMenu();
    }

    /**
     * Make a right click on a row select the row.
     */
    private void activateRightClickSelection() {
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                Point p = e.getPoint();
                int row = table.rowAtPoint(p);
                int col = table.columnAtPoint(p);

                // The autoscroller can generate drag events outside the Table's range.
                if ((col == -1) || (row == -1)) {
                    return;
                }
                if (SwingUtilities.isRightMouseButton(e)) {
                    //for example, do this
                    table.setRowSelectionInterval(row, row);
                }
            }
        });
    }

    private void addPopupMenu() {
        final JPopupMenu popup = new JPopupMenu();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                JMenuItem item;

                popup.add(item = new JMenuItem(Loc.get("LOAD")));
                item.setIcon(Images.NAVIGATION_ICON_SMALL_OPEN);
                item.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        loadEntry();
                    }
                });
                popup.addSeparator();

                popup.add(item = new JMenuItem(Loc.get("EDIT")));
                item.setIcon(Images.NAVIGATION_ICON_SMALL_EDIT);
                item.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        editProject();
                    }
                });

                popup.addSeparator();

                popup.add(item = new JMenuItem(Loc.get("PROJECT_RIGHTS") + ": " + Loc.get("USER")));
                item.setIcon(Images.NAVIGATION_ICON_SMALL_PERSON);
                item.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new Thread(new Runnable() {

                            @Override
                            public void run() {
                                if (doOpenProject()) {
                                    mainFrame.displayProjectUserRightsScreen();
                                }
                            }
                        }).start();
                    }
                });

                popup.add(item = new JMenuItem(Loc.get("PROJECT_RIGHTS") + ": " + Loc.get("GROUP")));
                item.setIcon(Images.NAVIGATION_ICON_SMALL_PERSON);
                item.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new Thread(new Runnable() {

                            @Override
                            public void run() {
                                if (doOpenProject()) {
                                    mainFrame.displayProjectGroupRightsScreen();
                                }
                            }
                        }).start();
                    }
                });

                popup.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

            }
        });
        table.setComponentPopupMenu(popup);
    }

    @Override
    protected void createTableSelection(JPanel settingsLine) {
        // Table selection
        JPanel tableSelectionWrapper = new JPanel(new BorderLayout());
        tableSelectionWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        tableSelectionWrapper.add(BorderLayout.WEST, new JLabel(Loc.get("TABLE") + ": "));
        ArrayList<IBaseManager> listTablesDisplay = new ArrayList<>();
        try {
            listTablesDisplay.add(((AbstractController) mainFrame.getController()).getLocalManager().getProjectManager());
        } catch (NotLoggedInException e) {
            e.printStackTrace();
        }
        IBaseManager[] comboTableData = listTablesDisplay.toArray(new IBaseManager[0]);
        comboTable = new XComboBox<>(comboTableData);

        comboTable.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                setSelectedTable(comboTable.getItemAt(comboTable.getSelectedIndex()));
            }
        });
        comboTable.setPreferredSize(new Dimension(130, 1));
        tableSelectionWrapper.add(BorderLayout.CENTER, comboTable);

        if (comboTableData.length > 1) {
            settingsLine.add(BorderLayout.WEST, tableSelectionWrapper);
        } else {
            JLabel emptylabel = new JLabel();
            emptylabel.setPreferredSize(new Dimension(170, 1));
            settingsLine.add(BorderLayout.WEST, emptylabel);
        }
    }

    @Override
    protected void exportSelection() {

        Footer.startWorking();

        final AbstractController controller = ((AbstractController) mainFrame.getController());
        try {
            ArrayList<Key> keys = new ArrayList<>();

            final int rowCount = table.getRowCount();
            for (int row = 0; row < rowCount; row++) {
                keys.add(data.getProjectKeyAt(table.getRowSorter().convertRowIndexToModel(row)));
            }
            controller.startProjectExport(keys);
        } finally {
            Footer.stopWorking();
        }
    }

    @Override
    protected ExportResult getData(ArrayList<ColumnType> list, int offset) throws NotLoadedException, NotLoggedInException, NoRightException, StatementNotExecutedException {
        return ((AbstractController) mainFrame.getController()).getLocalManager().getProjectManager().getEntryData(offset, ENTRIES_PER_PAGE);
    }

    @Override
    protected int getNumberOfEntries() throws NotLoadedException, StatementNotExecutedException, NotLoggedInException {
        return ((AbstractController) mainFrame.getController()).getLocalManager().getProjectManager().getNumberOfProjects();
    }

    private Key getSelectedRowKey() throws NoEntrySelectedException {
        int rowId = table.getSelectedRow();
        if (rowId == -1) {
            throw new NoEntrySelectedException();
        }
        return data.getProjectKeyAt(table.getRowSorter().convertRowIndexToModel(rowId));
    }

    @Override
    protected void loadEntry() {
        setButtonsEnabled(false);
        try {
            Key key = getSelectedRowKey();
            ProjectDataSet project = ((AbstractController) mainFrame.getController()).getLocalManager().getProjectManager().getProject(key);
            mainFrame.getController().loadProject(project, true);
        } catch (NoEntrySelectedException e) {
            Footer.displayWarning(Loc.get("NO_ENTRY_SELECTED"));
        } catch (NotLoggedInException | NoRightException | StatementNotExecutedException | NullPointerException ex) {
            Logger.getLogger(Listing.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            setButtonsEnabled(true);
        }
    }

    private void editProject() {
        setButtonsEnabled(false);
        try {
            Key key = getSelectedRowKey();
            ProjectDataSet project = ((AbstractController) mainFrame.getController()).getLocalManager().getProjectManager().getProject(key);
            mainFrame.displayProjectEditScreen(project);
        } catch (NoEntrySelectedException e) {
            Footer.displayWarning(Loc.get("NO_ENTRY_SELECTED"));
        } catch (NotLoggedInException | StatementNotExecutedException | NullPointerException ex) {
            Logger.getLogger(Listing.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            setButtonsEnabled(true);
        }
    }

    private boolean doOpenProject() {
        setButtonsEnabled(false);
        try {
            Key key = getSelectedRowKey();
            Footer.startWorking();
            ProjectDataSet project = ((AbstractController) mainFrame.getController()).getLocalManager().getProjectManager().getProject(key);
            mainFrame.getController().loadProject(project, true);
        } catch (NoEntrySelectedException e) {
            Footer.displayWarning(Loc.get("NO_ENTRY_SELECTED"));
            return false;
        } catch (StatementNotExecutedException | NotLoggedInException | NoRightException ex) {
            Footer.displayError(Loc.get("ERROR"));
            Logger.getLogger(AbstractProjectOverview.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (Throwable what) {
            what.printStackTrace();
            return false;
        } finally {
            setButtonsEnabled(true);
            Footer.stopWorking();
        }
        return true;
    }

}
