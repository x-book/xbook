package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLine;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.exception.MissingInputException;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractSynchronisationManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lohrer on 28.04.2015.
 */
public class SolveConflictLineUpdateOrDelete extends SolveConflictLine {


    public SolveConflictLineUpdateOrDelete(HashMap<ColumnType, String> valueLocal, String displayLocal, HashMap<ColumnType, String> valueServer, String displayServer, String description, String tableName) {
        super(valueLocal, displayLocal, valueServer, displayServer, description, tableName);

    }

    @Override
    public void save(DataRow dataToSave) throws MissingInputException {
        HashMap<ColumnType, String> value = getFirstSelectedValue();
        if (value.isEmpty()) {
            dataToSave.add(new DataColumn(IStandardColumnTypes.DELETED_YES, AbstractSynchronisationManager.DELETED.getColumnName()));
            value = getFirstNotSelectedValue();
        } else {
            dataToSave.add(new DataColumn(IStandardColumnTypes.DELETED_NO, AbstractSynchronisationManager.DELETED.getColumnName()));
        }
        for (Map.Entry<ColumnType, String> entry : value.entrySet()) {
            dataToSave.put(entry.getKey().getColumnName(), entry.getValue());
        }
    }
}
