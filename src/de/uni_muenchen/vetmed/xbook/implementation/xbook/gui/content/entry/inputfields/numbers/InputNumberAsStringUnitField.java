package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers;

import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.INumberAsStringUnitField;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputNumberAsStringUnitField extends InputNumberAsStringField implements INumberAsStringUnitField {

    private JComboBox comboUnit;
    private final ArrayList<Unit> units;

    public InputNumberAsStringUnitField(ColumnType columnType, ArrayList<Unit> units) {
        super(columnType);
        this.units = units;
    }

    @Deprecated
    public InputNumberAsStringUnitField(ColumnType columnType, int gridX, int gridY, SidebarPanel sidebar, ArrayList<Unit> units) {
        super(columnType, gridX, gridY, sidebar);
        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);
        this.units = units;
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        String value = list.get(columnType);
        if (value != null) {
            textField.setText(value.replace(".", ","));
        }

        // if a value was loaded, then load the unit with multiplicator "1"
        if (!textField.getText().isEmpty()) {
            for (Unit unit : units) {
                if (unit.getMultiplicator() == 1) {
                    comboUnit.setSelectedItem(unit.getUnit());
                    return;
                }
            }
        }
    }

    @Override
    public void clear() {
        textField.setText("");
        comboUnit.setSelectedItem(units.get(0).getUnit());
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        if (textField.getText().isEmpty()) {
            list.add(new DataColumn("", columnType.getColumnName()));
        } else {
            String selectedUnit = (String) comboUnit.getSelectedItem();
            for (Unit unit : units) {
                if (selectedUnit.equals(unit.getUnit())) {
                    BigDecimal bdText = new BigDecimal(textField.getText().replace(",", "."));
                    BigDecimal bdMul = new BigDecimal(unit.getMultiplicator());
                    bdText = bdText.multiply(bdMul);
                    bdText.setScale(3, RoundingMode.CEILING);
                    list.add(new DataColumn("" + bdText, columnType.getColumnName()));
                    return;
                }
            }
            list.add(new DataColumn("", columnType.getColumnName()));
        }
    }

    @Override
    public String getStringRepresentation() {
        return textField.getText() + "/" + comboUnit.getSelectedItem();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = super.getMyFocusableComponents();
        comp.add(comboUnit);
        return comp;
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();

        ArrayList<String> unit = new ArrayList<>();
        for (Unit u : units) {
            unit.add(u.getUnit());
        }

        // check if default multiplicator is available
        boolean defaultValueAvailable = false;
        for (Unit u : units) {
            if (u.getMultiplicator() == 1) {
                defaultValueAvailable = true;
            }
        }
        if (!defaultValueAvailable) {
            System.out.println("No valid default multiplicator is available for " + this.getClass());
            new Exception().printStackTrace();
            System.exit(-1);
        }

        String[] s = unit.toArray(new String[unit.size()]);
        this.comboUnit = new JComboBox(s);
        comboUnit.setPreferredSize(new Dimension(50, Sizes.INPUT_FIELD_HEIGHT));
        multiPanel.add(BorderLayout.EAST, comboUnit);
    }

}
