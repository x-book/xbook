package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.wizard.page.editor;

import java.util.Collection;
import java.util.Map.Entry;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer.AbstractLookupTableEditorContent;

public class LookupTableEditorContent extends AbstractLookupTableEditorContent<Entry<String, String>> {

	private String label = null;

	public LookupTableEditorContent(String label,
			Collection<Entry<String, String>> data) {
		super(data);
		this.label = label;
		init();
	}

	@Override
	public String getHeaderTitle() {
		return label;
	}

	@Override
	public Object[] getRow(Entry<String, String> entry) {
		return new Object[] { entry.getValue() };
	}

	@Override
	public String[] getColumnNames() {
		return new String[] { label };
	}

}
