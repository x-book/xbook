package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.filter;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class SyncFilterData {

    private SyncFilterRowData valueProjectName = SyncFilterRowData.createEmptySet();
    private SyncFilterRowData valueProjectOwner = SyncFilterRowData.createEmptySet();
    private HashMap<ColumnType, SyncFilterRowData> additionalFilters = new HashMap<>();

    public boolean isSomethingSet() {
        for (String s : valueProjectName.getValues()) {
            if (!s.isEmpty()) {
                return true;
            }
        }
        for (String s : valueProjectOwner.getValues()) {
            if (!s.isEmpty()) {
                return true;
            }
        }

        for (SyncFilterRowData value : additionalFilters.values()) {
            for (String s : value.getValues()) {
                if (!s.isEmpty()) {
                    return true;
                }
            }
        }

        return false;
    }

    public void setValueProjectName(SyncFilterRowData values) {
        this.valueProjectName = values;
    }

    public void setValueProjectOwner(SyncFilterRowData values) {
        this.valueProjectOwner = values;
    }

    public SyncFilterRowData getProjectNameValues() {
        return valueProjectName;
    }

    public SyncFilterRowData getProjectOwnerValues() {
        return valueProjectOwner;
    }

    public HashMap<ColumnType, SyncFilterRowData> getAdditionalFilters() {
        return additionalFilters;
    }

    public void addAdditionalValues(ColumnType columnType, SyncFilterRowData currentData) {
        additionalFilters.put(columnType, currentData);
    }
}