package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputTextField;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IOpenURLButton;
import java.awt.Component;
import java.util.ArrayList;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputOpenURLButton extends InputTextField implements IOpenURLButton {

    /**
     * The button to open the link.
     */
    private JButton buttonOpen;

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     */
    public InputOpenURLButton(ColumnType columnType) {
        super(columnType);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     */
    @Deprecated
    public InputOpenURLButton(ColumnType columnType, int gridX, int gridY) {
        this(columnType, gridX, gridY, (SidebarPanel) null);
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param sidebarText The sidebar text for this input element.
     */
    @Deprecated
    public InputOpenURLButton(ColumnType columnType, int gridX, int gridY, String sidebarText) {
        this(columnType, gridX, gridY, new SidebarEntryField(columnType, sidebarText));
    }

    /**
     * Constructor.
     *
     * @param columnType The database column data of the correspondenting input
     * field.
     * @param gridX The number of grids that are used for the height of the
     * input field.
     * @param gridY The number of grids that are used for the weight of the
     * input field.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    @Deprecated
    public InputOpenURLButton(ColumnType columnType, int gridX, int gridY, SidebarPanel sidebar) {
        super(columnType);

        setGridX(gridX);
        setGridY(gridY);
        setSidebar(sidebar);
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = super.getMyFocusableComponents();
        comp.add(buttonOpen);
        return comp;
    }

    @Override
    protected void createFieldInput() {
        super.createFieldInput();

        buttonOpen = new JButton(Loc.get("OPEN"));
        buttonOpen.setBorder(BorderFactory.createEmptyBorder(0, 12, 0, 12));
        buttonOpen.setBackground(Colors.INPUT_FIELD_BACKGROUND);
        buttonOpen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Desktop desktop = Desktop.getDesktop();
                    desktop.browse(new URI(zField.getText()));
                } catch (URISyntaxException | IOException ex) {
                    Footer.displayError(Loc.get("INVALID_URL_CANNOT_OPEN"));
                    Logger.getLogger(InputOpenURLButton.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        multiPanel.add(BorderLayout.EAST, ComponentHelper.wrapComponent(buttonOpen, Colors.INPUT_FIELD_BACKGROUND, 0, 0, 0, 6));
    }

}
