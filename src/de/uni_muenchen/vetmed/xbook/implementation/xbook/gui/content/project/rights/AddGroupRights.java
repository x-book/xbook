package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Group;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rights;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class AddGroupRights extends AbstractProjectRightsAdd<Group> {

    private ArrayList<Group> allGroups;

    public AddGroupRights() {
        super(RightMode.GROUP_TO_PROJECT);
    }

    @Override
    protected ArrayList<Group> getComboData() {
        try {
            ArrayList<Group> groups = mainFrame.getController().getGroups();
            return groups;
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(AbstractProjectRightsAdd.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    @Override
    protected ArrayList<ColumnType> getAvailableRights() {
        try {
            return mainFrame.getController().getAvailableRightsGroup();
        } catch (NotLoggedInException ex) {
            Logger.getLogger(AddGroupRights.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }

    @Override
    protected void actionAfterSaving() {
        mainFrame.displayProjectGroupRightsScreen();
    }

    @Override
    protected void save() {
        Group groupToSave = null;
        if (!((String) combo.getSelectedItem()).equals("")) {
            groupToSave = allGroups.get(combo.getSelectedIndex() - 1);
        }

        if (groupToSave == null) {
            Footer.displayError(Loc.get("NOT_A_VALID_GROUP"));
        } else {
            ArrayList<Rights> rights = new ArrayList<>();
            for (JCheckBox check : allCheckBoxes) {
                rights.add(new Rights(new ColumnType(check.getName(),ColumnType.Type.VALUE,ColumnType.ExportType.NONE), check.isSelected()));
            }
            ArrayList<RightsInformation> ri = new ArrayList<>();
            RightsInformation newRights = new RightsInformation(groupToSave.getName(), groupToSave.getId(), rights);
            ri.add(newRights);
            if (newRights.hasAnyRight() || newRights.getRights().isEmpty()) {
                UserRightInformation uri = new UserRightInformation(groupToSave.getName(), groupToSave.getId(), ri);
                try {
                    Message ms = ((AbstractSynchronisationController) mainFrame.getController()).saveNewGroupRights(uri.getRightsInformation());
                    if (ms.getResult().wasSuccessful()) {
                        Footer.displayConfirmation(Loc.get("GROUP_RIGHTS_SUCCESSFULLY_SAVED", groupToSave.getName()));
                    } else {
                        Footer.displayError(Loc.get("ERROR_WHILE_SAVING_THE_GROUP_RIGHTS", groupToSave.getName())+"<br>"+ms.getResult().getErrorMessage());
                        Logger.getLogger(AddGroupRights.class.getName()).log(Level.SEVERE,ms.getResult().getErrorMessage());
                    }
                } catch (NotConnectedException | NotLoggedInException | IOException | NotLoadedException | StatementNotExecutedException | NoRightException ex) {
                    Logger.getLogger(AddUserRights.class.getName()).log(Level.SEVERE, null, ex);
                }
                actionAfterSaving();
            } else {
                Footer.displayWarning(Loc.get("NO_RIGHTS_ASSIGNED"));
            }
        }
    }

    @Override
    protected void fillComboData() {
        allGroups = getComboData();
        for (Group u : allGroups) {
            combo.addItem(u.getName());
        }
    }
}
