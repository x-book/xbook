package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiSelection;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawMultiSelection;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import java.awt.*;
import java.util.ArrayList;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputMultiSelection extends AbstractDefaultInputElement {

    /**
     * The text field object.
     */
    protected RawMultiSelection zField;

    /**
     * Constructor.
     *
     * @param columnType The basic colum type object.
     */
    public InputMultiSelection(ColumnType columnType) {
        super(columnType);
    }

    @Override
    public AbstractInputElementFunctions createInputField() {
        if (zField == null) {
            zField = new RawMultiSelection(mainFrame, columnType);
        }
        return zField;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return zField.getMyFocusableComponents();
    }

    @Override
    public void load(DataSetOld data) {
        zField.load(data);
    }

    @Override
    public void clear() {
        zField.clear();
    }

    @Override
    public void save(DataSetOld data) throws IsAMandatoryFieldException {
        zField.save(data);
    }

    @Override
    public String getStringRepresentation() {
        return zField.getStringRepresentation();
    }

    @Override
    public boolean isValidInput() {
        return zField.isValidInput();
    }

    /**
     * Returns the multi selection object.
     *
     * @return The multi selection object.
     */
    public MultiSelection getMultiSelection() {
        return zField.getMultiSelection();
    }


    /**
     * Set the state if the elements are always disabled (and therefore are only selectable by extern elements).
     *
     * @param areElementsAlwaysDisabled The state if the elements are always disabled, or not.
     */
    public void setAreElementsAlwaysDisabled(boolean areElementsAlwaysDisabled) {
        zField.setAreElementsAlwaysDisabled(areElementsAlwaysDisabled);
    }
}
