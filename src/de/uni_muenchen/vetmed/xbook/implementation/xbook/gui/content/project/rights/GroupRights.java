package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.rights;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarProjectRights;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class GroupRights extends AbstractRights {

    public GroupRights() {
        super(RightMode.GROUP_TO_PROJECT);
    }

    @Override
    protected void save() {
        try {
            Message rs = ((AbstractSynchronisationController) mainFrame.getController()).saveNewGroupRights(getCurrentValues());
            mainFrame.displayProjectGroupRightsScreen();
            if (rs.getResult().wasSuccessful()) {
                Footer.displayConfirmation(Loc.get("GROUP_RIGHTS_SAVED_SUCCESSFULLY"));
            } else {
                Footer.displayError(Loc.get("ERROR_WHILE_SAVING_GROUP_RIGHTS"));
                Logger.getLogger(GroupRights.class.getName()).log(Level.SEVERE, rs.getResult().getErrorMessage());
            }
        } catch (NotConnectedException | NotLoggedInException | IOException | NotLoadedException | StatementNotExecutedException | NoRightException ex) {
            Logger.getLogger(GroupRights.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected ArrayList<RightsInformation> load() {
        try {
            ((AbstractSynchronisationController) mainFrame.getController()).updateProjectRightsGroup();
            return mainFrame.getController().getNewGroupRights();
        } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException | NoRightException | NotConnectedException | IOException ex) {
            Logger.getLogger(GroupRights.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    @Override
    protected boolean shouldDisplayLine(RightsInformation rinfo) {
        return true;
    }

    @Override
    public SidebarPanel getSideBar() {
        return new SidebarProjectRights(Loc.get("GROUP_RIGHTS"));
    }

    @Override
    protected String getAddButtonTooltip() {
        return Loc.get("ADD_GROUP");
    }

    @Override
    protected ArrayList<ColumnType> getListOfRights() throws NotLoggedInException {
        return apiControllerAccess.getAvailableRightsGroup();
    }

    @Override
    protected ActionListener getAddButtonListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.displayProjectUserRightsAddGroupScreen();
            }
        };
    }

    @Override
    protected void reloadThisPanel() {
        mainFrame.displayProjectGroupRightsScreen();
    }

}
