package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawTwoDatesChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.ITwoDatesChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import java.text.ParseException;

/**
 * A date chooser as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputTwoDatesChooser extends AbstractDefaultInputElement implements ITwoDatesChooser {

    /**
     * The text field object.
     */
    protected RawTwoDatesChooser zField;
    private final ColumnType columnTypeDate2;
    private RawTwoDatesChooser.LabelType labelType = RawTwoDatesChooser.LabelType.NONE;

    public InputTwoDatesChooser(ColumnType columnTypeDate1, ColumnType columnTypeDate2, String title, RawTwoDatesChooser.LabelType labelType) {
        super(columnTypeDate1);
        this.columnTypeDate2 = columnTypeDate2;
        this.labelType = labelType;
        this.titleString = title;

        setGridX(2);
    }

    public InputTwoDatesChooser(ColumnType columnTypeDate1, ColumnType columnTypeDate2, RawTwoDatesChooser.LabelType labelType) {
        this(columnTypeDate1, columnTypeDate2, columnTypeDate1.getDisplayName(), labelType);
    }

    public InputTwoDatesChooser(ColumnType columnTypeDate1, ColumnType columnTypeDate2) {
        this(columnTypeDate1, columnTypeDate2, RawTwoDatesChooser.LabelType.NONE);
    }

    @Override
    public AbstractInputElementFunctions createInputField() {
        if (zField == null) {
            zField = new RawTwoDatesChooser(columnType, columnTypeDate2, labelType);
        }
        return zField;
    }

    @Override
    public void setDate1(String date) throws ParseException {
        zField.setDate1(date);
    }

    @Override
    public String getDate1() {
        return zField.getDate1();
    }

    @Override
    public void setDate2(String date) throws ParseException {
        zField.setDate2(date);
    }

    @Override
    public String getDate2() {
        return zField.getDate2();
    }


}
