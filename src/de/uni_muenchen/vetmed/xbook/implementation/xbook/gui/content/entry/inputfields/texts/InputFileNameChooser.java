package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawFileNameChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.IFileNameChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.ArrayList;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class InputFileNameChooser extends AbstractDefaultInputElement implements IFileNameChooser {

    /**
     * The text field object.
     */
    protected RawFileNameChooser zField;
    /**
     * The file filter options.
     */
    protected ArrayList<FileNameExtensionFilter> fileFilter;
    /**
     * The current selected mode.
     */
    private RawFileNameChooser.Mode mode;

    /**
     * Constructor.
     *
     * @param columnType The corresponding ColumnType.
     * @param fileFilter The file filter options.
     * @param mode       The selected mode.
     */
    public InputFileNameChooser(ColumnType columnType, ArrayList<FileNameExtensionFilter> fileFilter, RawFileNameChooser.Mode mode) {
        super(columnType);
        this.fileFilter = fileFilter;
        this.mode = mode;
    }

    /**
     * Constructor.
     * <p>
     * Uses the "File" option as default value for the mode.
     *
     * @param columnType The corresponding ColumnType.
     * @param fileFilter The file filter options.
     */
    public InputFileNameChooser(ColumnType columnType, ArrayList<FileNameExtensionFilter> fileFilter) {
        this(columnType, fileFilter, RawFileNameChooser.Mode.FILE);
    }

    @Override
    public String getText() {
        return zField.getText();
    }

    /**
     * Returns the TextField object of the input field.
     *
     * @return The TextField object of the input field.
     */
    public JTextField getTextField() {
        return zField.getTextField();
    }

    @Override
    public AbstractInputElementFunctions createInputField() {
        if (zField == null) {
            zField = new RawFileNameChooser(columnType, fileFilter, mode);
        }
        return zField;
    }

    @Override
    public void setText(String text) {
        zField.setText(text);
    }

}
