package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawFileUploadField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractDefaultInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.util.ArrayList;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class InputFileUploadField extends AbstractDefaultInputElement {

    /**
     * The file upload field object.
     */
    protected RawFileUploadField zField;
    /**
     * The file filter options.
     */
    protected ArrayList<FileNameExtensionFilter> fileFilter;
    /**
     * The corresponding column type for the information whether the file has to be synchronized or not.
     */
    protected ColumnType columnTypeFileMustBeSynced;
    /**
     * The corresponding column type for the information of the file extension.
     */
    private final ColumnType columnTypeExtension;

    /**
     * Constructor.
     *
     * @param columnTypeFileName         The corresponding column type for file name.
     * @param columnTypeExtension        The corresponding column type for file extension.
     * @param columnTypeFileMustBeSynced The corresponding column type for the information whether the file has to be
     *                                   synchronized or not.
     * @param fileFilter                 The file filter options.
     */
    public InputFileUploadField(ColumnType columnTypeFileName, ColumnType columnTypeExtension,
                                ColumnType columnTypeFileMustBeSynced, ArrayList<FileNameExtensionFilter> fileFilter) {
        super(columnTypeFileName);
        this.fileFilter = fileFilter;
        this.columnTypeExtension = columnTypeExtension;
        this.columnTypeFileMustBeSynced = columnTypeFileMustBeSynced;

        setGridX(3);
    }

    /**
     * Returns the current file name of the input field.
     *
     * @return
     */
    public String getInput() {
        return zField.getText();
    }

    @Override
    public AbstractInputElementFunctions createInputField() {
        if (zField == null) {
            zField = new RawFileUploadField(apiControllerAccess, columnType, columnTypeExtension,
                    columnTypeFileMustBeSynced, fileFilter);
        }
        return zField;
    }

    @Override
    public void actionOnFocusLost() {
        super.actionOnFocusLost();
        zField.actionOnFocusLost();
        if (!zField.isValidPath()) {
            clear();
            setErrorStyle();
        }
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        zField.getTextField().setEnabled(false);
    }

    @Override
    public void setEnabledForRights(boolean b) {
        super.setEnabledForRights(b);
        zField.getTextField().setEnabled(false);
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = super.getMyFocusableComponents();
        comp.addAll(zField.getMyFocusableComponents());
        return comp;
    }
}
