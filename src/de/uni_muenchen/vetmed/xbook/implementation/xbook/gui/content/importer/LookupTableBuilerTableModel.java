package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.importer;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.importer.lookup.LookupTableBuilderAdapter;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class LookupTableBuilerTableModel<D, C, T> extends LookupTableBuilderAdapter<D, C, T> implements TableModel {

    private List<TableModelListener> listeners = new ArrayList<>();
    private List<D> dTable = new ArrayList<>();

    public abstract Object[] getRow(D d, C c);

    public abstract int getEditorColumnIndex();

    public abstract String[] getColumnNames();

    private List<D> getTable() {
        if (dTable.isEmpty()) {
            dTable.addAll(getInternalMap().keySet());
            Collections.sort(dTable, new Comparator<D>() {
                @Override
                public int compare(D o1, D o2) {
                    return o1.toString().toUpperCase().compareTo(o2.toString().toUpperCase());
                }
            });
        }
        return dTable;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;
        try {
            D d = getTable().get(rowIndex);
            value = getRow(d, getInternalMap().get(d))[columnIndex];
        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
            value = null;
        }
        return value;
    }

    @Override
    public int getColumnCount() {
        if (!getInternalMap().isEmpty()) {
            D d = getTable().get(0);
            return getRow(d, getInternalMap().get(d)).length;
        }
        return 0;
    }

    @Override
    public int getRowCount() {
        return getTable().size();
    }

    @Override
    public String getColumnName(int columnIndex) {
        String columnName;
        try {
            columnName = getColumnNames()[columnIndex];
        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
            columnName = "Column " + columnIndex;
        }
        return columnName;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        try {
            if (getEditorColumnIndex() == columnIndex && getCodomain().contains( aValue)) {
                D d = getTable().get(rowIndex);
                mapsTo(d, (C) aValue);
            }
            // ignore all other values
        } catch (ClassCastException | NullPointerException e) {
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return rowIndex < getRowCount() && columnIndex == getEditorColumnIndex();
    }

    @Override
    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public void fireTableChanged() {
        for (TableModelListener listener : listeners) {
            listener.tableChanged(new TableModelEvent(this));
        }
    }

    @Override
    public void mapsTo(D d, C c) {
        super.mapsTo(d, c);
        fireTableChanged();
    }
}
