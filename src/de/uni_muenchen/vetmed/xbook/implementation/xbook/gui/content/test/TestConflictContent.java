package de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.test;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportResult;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseProjectEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.ConflictScreen;

import java.util.ArrayList;

public class TestConflictContent extends ConflictScreen {


    @Override
    protected ArrayList<DataSetOld> getProjectConflicts(ProjectDataSet pr, AbstractBaseProjectEntryManager absync) throws StatementNotExecutedException, NotLoadedException {
        return absync.getEntry(pr);
    }

    @Override
    protected ExportResult getConflictableEntries(int LIMIT, ProjectDataSet pr, AbstractBaseEntryManager manager) throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException {
        return  manager.getEntries(pr, manager.getConflictDisplayColumns(), ColumnType.ExportType.GENERAL, false, 0, LIMIT, null,false);
    }
}
