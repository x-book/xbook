package de.uni_muenchen.vetmed.xbook.implementation.xbook.export;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ExportResult;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.helper.FileTypeHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractProjectManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class AbstractExport {

    /**
     * The types of available export formats.
     */
    public enum Type {
        CSV, XLS, XLSX
    }

    /**
     * Holds the number of elements that are fetched per operation. Benchmark result: 600 is better than 200. More is
     * worse.
     */
    protected final int ENTRIES_PER_OPERATION = 600;
    protected AbstractController controller;
    protected String columnDivider = ";";
    private static final Log _log = LogFactory.getLog(AbstractExport.class);

    public void setController(AbstractController controller) {
        this.controller = controller;
    }

    public class ExportFile {
        public String path;
        public String ending;
    }

    public boolean exportData(ProjectDataSet project, HashMap<IBaseManager, ArrayList<ColumnType>> list,
                              ArrayList<Key> keys)
            throws SQLException, NotLoggedInException, NotLoadedException,
            NoRightException, IOException, InvalidFormatException {
        if (_log.isDebugEnabled()) {
            _log.debug("Starting CSV-Export:");
        }
        ExportFile exportFile = getExportFile(getDefaultFile(project));
        if (exportFile == null) {
            return false;
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Footer.showProgressBar();
            }
        });
        doExport(project, list, exportFile.ending, exportFile, keys);

        exportSpecifData(exportFile, project, list, keys);

        if (_log.isDebugEnabled()) {
            _log.debug("Finished Export:");
        }
        return true;

    }

    protected ExportFile getExportFile(File defaultFile) {
        String path = "";
        String ending = "";
        File f = null;
        // choose file

        JFileChooser chooser = new JFileChooser();
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setMultiSelectionEnabled(false);
        chooser.setSelectedFile(defaultFile);
        for (FileFilter exportFormat : getAvailableExportFormats()) {
            chooser.setFileFilter(exportFormat);
        }

        boolean accepted = false;
        String name = "";
        while (!accepted) {
            int retval = chooser.showSaveDialog((controller.getMainFrame()));
            if (retval != JFileChooser.APPROVE_OPTION) {
                return null;
            }
            f = chooser.getSelectedFile();
            path = f.getParent();

            name = f.getName();
            int index = name.lastIndexOf('.');
            if (index == -1) {
                ending = ((FileNameExtensionFilter) chooser.getFileFilter()).getExtensions()[0];
                f = new File(path + System.getProperty("file.separator") + name + "." + ending);
            } else {
                ending = name.substring(index + 1, name.length());
                if (ending.equals("csv") || ending.equals("xls") || ending.equals("xlsx")) {
                    name = name.substring(0, index);
                } else {
                    ending = ((FileNameExtensionFilter) chooser.getFileFilter()).getExtensions()[0];
                    f = new File(path + System.getProperty("file.separator") + name + "." + ending);
                }

            }
            accepted = true;
            if (f.exists()) {
                int sel = JOptionPane.showConfirmDialog(controller.getMainFrame(),
                        Loc.get("FILE_ALREADY_EXISTS", f.getName()), Loc.get("FILE_ALREADY_EXISTS_HEADER"),
                        JOptionPane.YES_NO_OPTION);
                if (sel == JOptionPane.YES_OPTION) {
                    accepted = true;
                    f.delete();
                } else {
                    accepted = false;
                }
            }
        }
        ExportFile exportFile = new ExportFile();
        exportFile.ending = ending;
        exportFile.path = path + System.getProperty("file.separator") + name;
        return exportFile;
    }

    protected File getDefaultFile(ProjectDataSet project) {
        return new File(project.getProjectName());
    }

    /**
     * Start exporting data for the given project, list of columns and export it to the given export file
     *
     * @param project    The Project to export data for
     * @param list       The list of Columns to export
     * @param ending     The ending of the filename
     * @param exportFile The export file to save
     * @throws NotLoggedInException
     * @throws NotLoadedException
     * @throws StatementNotExecutedException
     * @throws NoRightException
     * @throws IOException
     */
    protected void doExport(ProjectDataSet project, HashMap<IBaseManager, ArrayList<ColumnType>> list, String ending,
                            ExportFile exportFile,
                            ArrayList<Key> keys) throws NotLoggedInException, NotLoadedException,
            StatementNotExecutedException, NoRightException, IOException, InvalidFormatException {
        for (Map.Entry<IBaseManager, ArrayList<ColumnType>> entrySet : list.entrySet()) {
            IBaseManager manager = entrySet.getKey();
            ArrayList<ColumnType> value = entrySet.getValue();
            String addition = "";
            switch (ending) {
                case "csv":
                    addition = "_" + manager.getLocalisedTableName();
                default:
            }
            File f = new File(exportFile.path + addition + "." + exportFile.ending);
            export(manager, project, value, ending, f, manager.getLocalisedTableName(), keys);
        }
    }

    private void export(IBaseManager manager, ProjectDataSet project, ArrayList<ColumnType> list, String ending, File f,
                        String name,
                        ArrayList<Key> keys) throws StatementNotExecutedException, NotLoggedInException,
            NotLoadedException, NoRightException, IOException, InvalidFormatException {
        /**
         * *******************************************Write********************************
         */
        AbstractExportFile abstractExportFile = null;
        int numberOfEntries = manager.getNumberofEntrys(project.getProjectKey());
        int offset = 0;
        boolean moreEntries = true;
        ExportResult entries;

        while (moreEntries) {
            //we need to hide ID and Database Numbers
            if (manager instanceof BaseEntryManager) {
                entries = ((BaseEntryManager) manager).getEntries(project, list, ColumnType.ExportType.GENERAL, false,
                        offset, ENTRIES_PER_OPERATION, keys, null, true);
            } else if (manager instanceof AbstractProjectManager) {
                ArrayList<Key> projectKey = new ArrayList<>();
                projectKey.add(project.getProjectKey());

                entries = ((AbstractProjectManager) manager).getEntryData(offset, ENTRIES_PER_OPERATION, projectKey);
            } else {
                throw new StatementNotExecutedException("");
            }
            offset += ENTRIES_PER_OPERATION;
            final double percent = offset * 100.0 / (double) numberOfEntries;
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Footer.setProgressBarValue(percent);
                }
            });
            if (offset > numberOfEntries) {
                moreEntries = false;
            }
            if (abstractExportFile == null) {
                switch (ending) {
                    case "csv":
                        abstractExportFile = new CSVExportFile(f);
                        break;
                    case "xls":
                        abstractExportFile = new XLSExportFile(f, name);
                        break;
                    case "xlsx":
                        abstractExportFile = new XLSXExportFile(f, name);
                        break;
                    default:
                        throw new IOException("Ending not supported");
                }

                if (abstractExportFile != null) {
                    abstractExportFile.setHeadLines(entries.getHeadlines().values());
                }
            }
            if (abstractExportFile != null) {
                abstractExportFile.write(entries);
            }

        }

        try {
            _log.debug("close");
            if (abstractExportFile != null) {

                abstractExportFile.close();
            }
        } catch (IOException e) {
            LogFactory.getLog(AbstractExport.class).error("Close failed: " + e.getMessage());
        }
    }

    /**
     * Exports Book specific data. This must be overriden if custom Export ist wanted
     *
     * @param exportFile
     * @param project
     * @param columns
     */
    protected void exportSpecifData(ExportFile exportFile, ProjectDataSet project,
                                    HashMap<IBaseManager, ArrayList<ColumnType>> columns, ArrayList<Key> keys) {
    }

    /**
     * @param s
     * @return
     */
    // Eliminiere die Zeilenumbr\u00FCche im String
    protected static String elimCR(String s) {
        if (s.lastIndexOf("\n") > -1) {
            s = s.replaceAll("\n", " ");
        }
        return s;
    }

    // Eliminiere die Kommata im String
    protected static String escapeComma(String input) {
        if (input != null) {
            if (input.lastIndexOf(",") > -1) {
                //input = input.replaceAll(",", " - ");
                input = "\"" + input + "\"";
            }
        }
        return input;
    }

    public static abstract class AbstractExportFile {

        protected File file;

        public AbstractExportFile(File file) {

            this.file = file;
        }

        public void close() throws IOException {

        }

        public abstract void write(ArrayList<ArrayList<String>> data);

        public abstract void write(ExportResult data);

        public abstract void setHeadLines(Collection<String> values);
    }

    public static class CSVExportFile extends AbstractExportFile {

        protected String columnDivider = ";";

        public CSVExportFile(File file) {
            super(file);
        }

        @Override
        public void write(ArrayList<ArrayList<String>> data) {
            FileWriter fw = null;
            BufferedWriter bw = null;
            try {
                String s = "";
                String result;

                fw = new FileWriter(file, true);
                bw = new BufferedWriter(fw);

                for (ArrayList<String> someting : data) {

                    s = "";
                    for (String value : someting) {
                        if (value == null || value.equals("null") || value.equals("-1")) {
                            value = "";
                        }
                        s += columnDivider + value;
                    }
                    s = elimCR(s);
                    result = s.substring(columnDivider.length(), s.length()) + "\n";
                    bw.write(result);

                }
                bw.close();
                fw.close();

            } catch (IOException ex) {
                Logger.getLogger(AbstractExport.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (fw != null) {
                    try {
                        fw.close();
                    } catch (IOException ignore) {
                    }
                }
                if (bw != null) {
                    try {
                        bw.close();
                    } catch (IOException ignore) {
                    }
                }
            }
        }

        @Override
        public void setHeadLines(Collection<String> headLines) {
            FileWriter fw = null;
            BufferedWriter bw = null;

            try {
                String s = "";
                String result;

                for (String value : headLines) {

                    if (value == null || value.equals("null") || value.equals("-1")) {
                        value = "";
                    }
                    s += columnDivider + value;
                }
                s = elimCR(s);
                fw = new FileWriter(file);
                bw = new BufferedWriter(fw);
                result = s.substring(columnDivider.length(), s.length()) + "\n";
                bw.write(result);
                bw.close();
                fw.close();

            } catch (IOException ex) {
                Logger.getLogger(CSVExportFile.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (fw != null) {
                    try {
                        fw.close();
                    } catch (IOException ignore) {
                    }
                }
                if (bw != null) {
                    try {
                        bw.close();
                    } catch (IOException ignore) {
                    }
                }
            }
        }

        @Override
        public void write(ExportResult data) {
            FileWriter fw = null;
            BufferedWriter bw = null;

            try {
                fw = new FileWriter(file, true);
                bw = new BufferedWriter(fw);
                String result;
                StringBuilder s;
                for (ArrayList<String> someting : data) {

                    s = new StringBuilder();
                    for (String value : someting) {
                        if (value == null || value.equals("null") || value.equals("-1")) {
                            value = "";
                        }

                        s.append(columnDivider).append(value);
                    }
                    result = elimCR(s.toString());
                    result = result.substring(columnDivider.length(), result.length()) + "\n";
                    bw.write(result);

                }
                bw.close();
                fw.close();

            } catch (IOException ex) {
                Logger.getLogger(CSVExportFile.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (fw != null) {
                    try {
                        fw.close();
                    } catch (IOException ignore) {
                    }
                }
                if (bw != null) {
                    try {
                        bw.close();
                    } catch (IOException ignore) {
                    }
                }
            }
        }
    }

    public static class XLSXExportFile extends XLSExportFile {

        public XLSXExportFile(File file, String sheetName) throws IOException, InvalidFormatException {
            super(file, sheetName);
        }

        @Override
        protected Workbook createWorksheet() {
            return new XSSFWorkbook();
        }
    }


    public static class XLSExportFile extends AbstractExportFile {

        Workbook workbook;
        Sheet sheet;
        int currentRow = 1;

        public XLSExportFile(File file, String sheetName) throws IOException, InvalidFormatException {
            super(file);
            if (file.exists()) {

                workbook = WorkbookFactory.create(new FileInputStream(file));
            } else {
                workbook = createWorksheet();
            }

            // replace invalid characters for sheet names
            sheetName = sheetName.replace(" / ", " ");
            String invalidChars = "@#$%&()+~`\"':;,.|/";
            for (int i = 0; i < invalidChars.length(); i++) {
                sheetName = sheetName.replace(Character.toString(invalidChars.charAt(i)), "");
            }

            _log.debug("create sheet: '" + sheetName + "'");

            sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName(sheetName));
        }

        protected Workbook createWorksheet() {
            return new HSSFWorkbook();
        }

        @Override
        public void setHeadLines(Collection<String> values) {
            try {
                int i = 0;
                Row row = sheet.createRow(0);


                for (String value : values) {

                    if (value == null || value.equals("null") || value.equals("-1")) {
                        value = "";
                    }
                    row.getCell(i++, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).setCellValue(value);

                }
            } catch (Exception ex) {
                LogFactory.getLog(StatementNotExecutedException.class).error("XLS Error: " + ex.getMessage());
            }
        }

        @Override
        public void write(ArrayList<ArrayList<String>> data) {
            try {

                for (ArrayList<String> someting : data) {
                    Row row = sheet.createRow(currentRow);

                    int column = 0;
                    for (String value : someting) {
                        if (value == null || value.equals("null") || value.equals("-1")) {
                            value = "";
                        }
                        row.getCell(column++, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).setCellValue(value);
                    }
                    currentRow++;
                }

            } catch (Exception ex) {
                LogFactory.getLog(StatementNotExecutedException.class).error("XLS Error: " + ex.getMessage());
            }
        }

        @Override
        public void write(ExportResult data) {

            try {

                int column;
//                Label label;
                for (ArrayList<String> someting : data) {

                    column = 0;
                    Row row = sheet.createRow(currentRow);

                    for (String value : someting) {
                        if (value == null || value.equals("null") || value.equals("-1")) {
                            value = "";
                        }
                        row.getCell(column++, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).setCellValue(value);

//                        label = new Label(column++, currentRow,
//                                value);//TODO if problem replace numbers with new Number();
//
//                        sheet.addCell(label);
                    }
                    currentRow++;
                }

            } catch (Exception ex) {
                LogFactory.getLog(StatementNotExecutedException.class).error("XLS Error: " + ex.getMessage());
            }
        }

        @Override
        public void close() throws IOException {
            try (FileOutputStream out = new FileOutputStream(file)) {
                workbook.write(out);
            } catch (Throwable ex) {
                _log.error(ex, ex);
                throw ex;
            } finally {
                workbook.close();
            }


        }
    }


    public void exportProjects(AbstractProjectManager projectManager,
                               List<Key> keyList) throws NotLoadedException, StatementNotExecutedException,
            IOException, InvalidFormatException, NotLoggedInException, NoRightException {

        ExportFile exportFile = getExportFile(new File(Loc.get("PROJECT")));

        if (exportFile != null) {
            export(projectManager, exportFile, "Projects", keyList);
        }

    }

    private void export(AbstractProjectManager projectManager, ExportFile exportFile, String name,
                        List<Key> keyList) throws StatementNotExecutedException, NotLoggedInException,
            NotLoadedException, NoRightException, IOException, InvalidFormatException {
        File f = new File(exportFile.path + "." + exportFile.ending);
        /**
         * *******************************************Write********************************
         */
        AbstractExportFile abstractExportFile = null;
        int numberOfEntries = projectManager.getNumberOfProjects();
        int offset = 0;
        boolean moreEntries = true;
        ExportResult entries;

        while (moreEntries) {
            //we need to hide ID and Database Numbers
            entries = projectManager.getEntryData(offset, ENTRIES_PER_OPERATION, keyList);
//            entries = manager.getEntries(project, list, ColumnType.ExportType.GENERAL, false, offset,
//            ENTRIES_PER_OPERATION, null, false);
            offset += ENTRIES_PER_OPERATION;
            final double percent = offset * 100.0 / (double) numberOfEntries;
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Footer.setProgressBarValue(percent);
                }
            });
            if (offset > numberOfEntries) {
                moreEntries = false;
            }
            if (abstractExportFile == null) {
                switch (exportFile.ending) {
                    case "csv":
                        abstractExportFile = new CSVExportFile(f);
                        break;
                    case "xls":
                        abstractExportFile = new XLSExportFile(f, name);
                        break;
                    case "xlsx":
                        abstractExportFile = new XLSXExportFile(f, name);
                        break;
                }

                if (abstractExportFile != null) {
                    abstractExportFile.setHeadLines(entries.getHeadlines().values());
                }
            }
            if (abstractExportFile != null) {
                abstractExportFile.write(entries);
            }

        }
        try {
            if (abstractExportFile != null) {
                abstractExportFile.close();
            }
        } catch (Exception e) {
            LogFactory.getLog(AbstractExport.class).error("Close failed: " + e.getMessage());
        }
    }

    /**
     * Returns a list of available export file filters which are set to the file chooser. Override this method to change
     * the settings individual for a book.
     *
     * @return A list of available export formats which are supported.
     */
    public ArrayList<FileFilter> getAvailableExportFormats() {
        ArrayList<FileFilter> filters = new ArrayList<>();
        filters.add(new FileNameExtensionFilter(
                "<html><body>Comma Separated Values (<b>.csv</b>) <font " +
                        "color=#7d7d7d><i>(" + Loc.get("FILENAME_IS_PREFIX") + ")</i></font></body></html>",
                "csv"));

        filters.addAll(FileTypeHelper.getFileFilter(
                FileTypeHelper.FileTypes.XLS,
                FileTypeHelper.FileTypes.XLSX));

        return filters;
    }

}
