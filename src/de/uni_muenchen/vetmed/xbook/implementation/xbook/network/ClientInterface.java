package de.uni_muenchen.vetmed.xbook.implementation.xbook.network;

import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import java.io.IOException;
import java.util.Observer;

/**
 *
 * @author lohrer
 */
public interface ClientInterface {

    boolean isRunning();

    void resumeClient();

    Message sendMessage(Message message) throws NotConnectedException;

    void addObserver(Observer obs);
}
