package de.uni_muenchen.vetmed.xbook.implementation.xbook.network;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTable;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectInformation;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rights;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.network.ISerializableTypes;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableArrayList;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableCommands;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInt;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableString;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableIntTest;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.Column;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.Index;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.Table;
import java.io.IOException;

/**
 * @author Johannes Lohrer <johanneslohrer@msn.com>
 */
public class AbstractSerialisableFactory implements ISerializableTypes {

  /**
   * Deserializes an object from the given stream
   *
   * @param inputInterface@return The newly created Object
   */
  public <T extends Serialisable> T deserialise(SerialisableInputInterface inputInterface)
      throws IOException {
    int id = inputInterface.readInt();
    return (T) deserialise(inputInterface, id);

  }

  /**
   * Deserializes a object with the given id from the given stream. Derived methodes should also
   * call this methode for creation of standard Objects.
   *
   * @param is The Stream to serialize the object from
   * @param id The ID of the Object
   * @return The newly created object
   */
  protected final Serialisable deserialise(SerialisableInputInterface is, int id)
      throws IOException {

    switch (id) {

      case KEY_ID:
        return new Key(is);
      case TABLE_ID:
        return new Table(is);
      case COLUMN_ID:
        return new Column(is);
      case DATAHASH_ID:
        return new DataColumn(is);
      case DATA_SET_ID:
        return new DataSetOld(is);
      case PROJECT_DATA_ID:
        return new ProjectDataSet(is);
      case ENTRYDATA_ID:
        return new EntryDataSet(is);
      case RESULT_ID:
        return new Result(is);
      case DATA_SET_NEW_ID:
        return new DataSet(is);
      case ENTRY_TABLE_NEW_ID:
        return new DataTable(is);
      case STRING_ID:
        return new SerialisableString(is);
      case MESSAGE_ID:
        return new Message(is);
      case ARRAY_ID:
        return new SerialisableArrayList(is);
      case COMMAND_ID:
        return new SerialisableCommands(is);
      case INT_ID:
        return new SerialisableInt(is);
      case BYTEARR_ID:
        return new SerializableIntTest(is);
      case INDEX_ID:
        return new Index(is);
      case RIGHTS_ID: {
        return new Rights(is);
      }
      case RIGHTS_INFORMATION:
        return new RightsInformation(is);
      case PROJECT_INFORMATION_ID:
        return new ProjectInformation(is);

      default:
        throw new UnsupportedOperationException("Not supported yet. " + id);
    }
  }
}
