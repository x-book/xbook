package de.uni_muenchen.vetmed.xbook.implementation.xbook.network;

import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URLEncoder;

/**
 *
 * @author j.lohrer
 */
public class SerialisableOutputStreamWriter extends OutputStreamWriter implements SerializableOutputInterface {

    public static final String ENCODING = "UTF-8";
    public SerialisableOutputStreamWriter(OutputStream stream) throws IOException {

        super(stream);

    }
//    public void writeObject(SerializableObject serializedObj) throws IOException
//   {
//      
//      serializedObj.write(this);
//   }

    @Override
    public void writeCommand(Commands command) throws IOException {
        writeInt(command.ordinal());
    }

    @Override
    public void writeInt(int number) throws IOException {
        write(number+"#");


    }

    @Override
    public void writeString(String x) throws IOException {
        String encoded = URLEncoder.encode(x, ENCODING);
        writeInt(x.getBytes(ENCODING).length);

        write(encoded);

    }
    

    @Override
    public void writeBoolean(boolean b) throws IOException {
        writeInt(b ? 1 : 0);
    }
    
}
