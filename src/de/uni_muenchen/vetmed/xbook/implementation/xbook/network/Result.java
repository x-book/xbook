package de.uni_muenchen.vetmed.xbook.implementation.xbook.network;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;

import java.io.IOException;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class Result extends Serialisable {

    public static final int FAIL = -1;
    public static final int OK = 0;
    public static final int USERNAME_TAKEN = 1;
    public static final int MAIL_TAKEN = 2;
    public static final int MAIL_OR_USERNAME_WRONG = 3;
    public static final int PASSWORD_INCORRECT = 4;
    public static final int INCORRECT_ARGUMENT_LENGTH = 5;
    public static final int UNKNOWN_COMMAND = 6;
    public static final int UNKNOWN_PROJECT = 7;
    public static final int NOT_PROJECT_OWNER = 8;
    public static final int ENTRY_NOT_FOUND = 9;
    public static final int CONFLICT_OCCURED = 10;
    public static final int NO_READ_RIGHTS = 11;
    public static final int NO_WRITE_RIGHTS = 12;
    public static final int UNKNOWN_USER = 13;
    public static final int NOT_ADMIN = 14;
    public static final int NOT_DEVELOPER = 15;
    public static final int INCORRECT_ARGUMENT_TYPE = 16;
    public static final int NOT_GROUP_OWNER = 17;
    public static final int GROUP_NOT_EMPTY = 18;
    public static final int NOT_LOGGED_IN = 19;
    public static final int GROUP_ADMIN_RANK_EDIT_NOT_POSSIBLE = 20;
    public static final int GROUP_ADMIN_LEAVE_GROUP_NOT_POSSIBLE = 21;
    public static final int GROUP_ADMIN_RANK_CHANGE_NOT_POSSIBLE = 22;
    public static final int LIMITED_FUNCTIONALITY = 23;
    public static final int FILE_NOT_FOUND = 24;
    public static final int NOT_SUPPORTED = 25;

    public static final int NO_CONNECTION_TO_DATABASE = 100;
    public static final int INVALID_QUERY = 101;
    public static final int QUERY_EXECUTION_ERROR = 102;
    public static final int NO_VALID_DATABASE_SELECTED = 104;
    public static final int MISSING_MESSAGE_ID = 105;
    public static final int SESSION_EXPIRED = 106;
    public static final int SESSION_STOPPED = 107;
    public static final int NO_LOGIN_TAG_MISSING = 108;
    public static final int TABLE_NOT_FOUND = 109;
    public static final int DUPLICATE_KEY = 110;


    public static final int NO_SYNC_RIGHT = 150;
    public static final int NO_LOGIN_RIGHT = 151;
    public static final int NO_CREATE_GROUPS_RIGHT = 152;
    public static final int USER_BLOCKED = 153;


    private boolean success;
    private int status;

    public Result(SerialisableInputInterface is) throws IOException {
        success = is.readBoolean();
        status = is.readInt();
        if (XBookConfiguration.MODE == XBookConfiguration.Mode.DEVELOPMENT) {
            if (status == INCORRECT_ARGUMENT_TYPE || status == INCORRECT_ARGUMENT_LENGTH) {
                System.out.println("inc " + toString());
            }
        }
    }

    public Result(boolean result, int status) {
        this.success = result;
        this.status = status;
    }

    /**
     * Returns wether the Result was successfull or not
     *
     * @return
     */
    public boolean wasSuccessful() {
        return success;
    }

    /**
     * Returns the Status of the result
     *
     * @return
     */
    public int getStatus() {
        return status;
    }

    @Override
    protected int getClassID() {
        return AbstractSerialisableFactory.RESULT_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeBoolean(success);
        outputStream.writeInt(status);
    }

    public String getErrorMessage() {
        return toString();
    }

    @Override
    public String toString() {
        switch (status) {
            case FAIL:
                return Loc.get("ERROR_MESSAGE>ERROR");
            case OK:
                return Loc.get("ERROR_MESSAGE>NO_ERROR");
            case USERNAME_TAKEN:
                return Loc.get("ERROR_MESSAGE>USERNAME_TAKEN");
            case MAIL_TAKEN:
                return Loc.get("ERROR_MESSAGE>MAIL_TAKEN");
            case MAIL_OR_USERNAME_WRONG:
                return Loc.get("ERROR_MESSAGE>MAIL_OR_USERNAME_WRONG");
            case PASSWORD_INCORRECT:
                return Loc.get("ERROR_MESSAGE>PASSWORD_INCORRECT");
            case INCORRECT_ARGUMENT_LENGTH:
                return Loc.get("ERROR_MESSAGE>INCORRECT_ARGUMENT_LENGTH");
            case UNKNOWN_COMMAND:
                return Loc.get("ERROR_MESSAGE>UNKNOWN_COMMAND");
            case UNKNOWN_PROJECT:
                return Loc.get("ERROR_MESSAGE>UNKNOWN_PROJECT");
            case NOT_PROJECT_OWNER:
                return Loc.get("ERROR_MESSAGE>NOT_PROJECT_OWNER");
            case CONFLICT_OCCURED:
                return Loc.get("ERROR_MESSAGE>CONFLICT_OCCURED");
            case ENTRY_NOT_FOUND:
                return Loc.get("ERROR_MESSAGE>ENTRY_NOT_FOUND");
            case NO_READ_RIGHTS:
                return Loc.get("ERROR_MESSAGE>NO_READ_RIGHTS");
            case NO_WRITE_RIGHTS:
                return Loc.get("ERROR_MESSAGE>NO_WRITE_RIGHTS");
            case UNKNOWN_USER:
                return Loc.get("ERROR_MESSAGE>UNKNOWN_USER");
            case NOT_ADMIN:
                return Loc.get("ERROR_MESSAGE>NOT_ADMIN");
            case NOT_DEVELOPER:
                return Loc.get("ERROR_MESSAGE>NOT_DEVELOPER");
            case INCORRECT_ARGUMENT_TYPE:
                return Loc.get("ERROR_MESSAGE>INCORRECT_ARGUMENT_TYPE");
            case NOT_GROUP_OWNER:
                return Loc.get("ERROR_MESSAGE>NOT_GROUP_OWNER");
            case GROUP_NOT_EMPTY:
                return Loc.get("ERROR_MESSAGE>GROUP_NOT_EMPTY");
            case GROUP_ADMIN_RANK_CHANGE_NOT_POSSIBLE:
                return Loc.get("GROUP_ADMIN_RANK_CHANGE_NOT_POSSIBLE");
            case GROUP_ADMIN_LEAVE_GROUP_NOT_POSSIBLE:
                return Loc.get("GROUP_ADMIN_LEAVE_GROUP_NOT_POSSIBLE");
            case GROUP_ADMIN_RANK_EDIT_NOT_POSSIBLE:
                return Loc.get("GROUP_ADMIN_RANK_EDIT_NOT_POSSIBLE");
            case LIMITED_FUNCTIONALITY:
                return Loc.get("LIMITED_FUNCTIONALITY");
            case NOT_LOGGED_IN:
                return Loc.get("NOT_LOGGED_IN");
            case QUERY_EXECUTION_ERROR:
                return Loc.get("QUERY_EXECUTION_ERROR");
            case NO_VALID_DATABASE_SELECTED:
                return Loc.get("NO_VALID_DATABASE_SELECTED");
            case INVALID_QUERY:
                return Loc.get("INVALID_QUERY");
            case MISSING_MESSAGE_ID:
                return Loc.get("MISSING_MESSAGE_ID");
            case SESSION_EXPIRED:
                return Loc.get("SESSION_EXPIRED");
            case SESSION_STOPPED:
                return Loc.get("SESSION_STOPPED");
            case NO_LOGIN_TAG_MISSING:
                return Loc.get("NO_LOGIN_TAG_MISSING");
            case TABLE_NOT_FOUND:
                return Loc.get("TABLE_NOT_FOUND");
            case NO_CONNECTION_TO_DATABASE:
                return Loc.get("NO_CONNECTION_TO_DATABASE");
            case NO_SYNC_RIGHT:
                return Loc.get("NO_SYNC_RIGHT");
            case NO_LOGIN_RIGHT:
                return Loc.get("NO_LOGIN_RIGHT");
            case NO_CREATE_GROUPS_RIGHT:
                return Loc.get("NO_CREATE_GROUPS_RIGHT");
            case USER_BLOCKED:
                return Loc.get("USER_BLOCKED");
            case FILE_NOT_FOUND:
                return Loc.get("ERROR_MESSAGE>FILE_NOT_FOUND");
            case NOT_SUPPORTED:
                return Loc.get("ERROR_MESSAGE>NOT_SUPPORTED");
            case DUPLICATE_KEY:
                return Loc.get("ERROR_MESSAGE>DUPLICATE_KEY");
            default:
                return Loc.get("ERROR_MESSAGE>UNKNOWN") + status;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Result) {
            return ((Result) obj).status == this.status;
        }
        if (obj instanceof Integer) {
            return ((Integer) obj) == status;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.success ? 1 : 0);
        hash = 89 * hash + this.status;
        return hash;
    }
}
