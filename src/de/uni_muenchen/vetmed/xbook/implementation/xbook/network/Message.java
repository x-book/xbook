package de.uni_muenchen.vetmed.xbook.implementation.xbook.network;

import de.uni_muenchen.vetmed.xbook.api.network.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class Message extends Serialisable {

    private Commands command;
    private int messageID;
    private Result result;
    private final ArrayList<Serialisable> data;

    public Message() {
        data = new ArrayList<>();
        messageID = -1;
        result = new Result(false, Result.FAIL);
    }

    public Message(Commands command, ArrayList<? extends Serialisable> data) {
        this.command = command;
        this.data = new ArrayList<>();
        for (Serialisable value : data) {
            this.data.add(value);
        }
    }

    public Message(Commands command) {
        this.command = command;
        this.data = new ArrayList<>();
        result = new Result(true,Result.OK);
    }

    public Message(Commands command, Serialisable... data) {
        this.command = command;
        this.data = new ArrayList<>();
        this.data.addAll(Arrays.asList(data));
    }

    public Message(Commands command, String... data) {
        this.command = command;
        this.data = new ArrayList<>();
        for (String entry : data) {
            this.data.add(new SerialisableString(entry));
        }

    }

    public Message(SerialisableInputInterface is) throws IOException {
        command = is.readCommand();
        messageID = is.readInt();
        try {
            result = (Result) is.deserialize();
        } catch (RuntimeException ex) {
            System.out.println("exxx");
        }
        data = new ArrayList<>();
        int dataCount = is.readInt();
        for (int i = 0; i < dataCount; i++) {
            data.add(is.deserialize());
        }
    }

    public void setMessageID(int messageID) {
        this.messageID = messageID;
    }

    public int getMessageID() {
        return messageID;
    }

    public Commands getCommand() {
        return command;
    }

    public Result getResult() {
        return result;
    }

    public ArrayList<Serialisable> getData() {
        return data;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public boolean wasSuccessfull() {
        return result.wasSuccessful();
    }

    @Override
    protected int getClassID() {
        return AbstractSerialisableFactory.MESSAGE_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeCommand(command);
        outputStream.writeInt(messageID);
        outputStream.writeInt(data.size());
        for (Serialisable entry : data) {
            entry.serialize(outputStream);
        }
    }

}
