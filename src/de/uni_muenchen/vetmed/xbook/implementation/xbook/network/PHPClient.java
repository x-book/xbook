package de.uni_muenchen.vetmed.xbook.implementation.xbook.network;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import org.apache.log4j.Logger;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Observer;

/**
 * @author lohrer
 */
public class PHPClient implements ClientInterface {

    private final static Logger LOGGER = Logger.getLogger(PHPClient.class);
    boolean isRunning = true;
    private final AbstractSynchronisationController controller;
    private final AbstractSerialisableFactory factory;
    private final Proxy proxy;

    public PHPClient(AbstractSynchronisationController controller, AbstractSerialisableFactory factory, final String proxyHostAddress, int proxyPort, boolean useProxy) {
        this.controller = controller;
        this.factory = factory;
        if (useProxy) {
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHostAddress, proxyPort));
            LOGGER.info("using Proxy: " + proxy);

        } else {
            proxy = Proxy.NO_PROXY;
            LOGGER.info(" no proxy");
        }
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));

        //Allow all since we use a self signed certificate this is the only option.
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }

                }
        };

        SSLContext sc;
        try {
            sc = SSLContext.getInstance("SSL");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public void resumeClient() {
        isRunning = true;
        if (controller.isLoggedIn()) {
            controller.serverLogin();
        }
    }

    private void printMessage(Message message) {

        try {
            SerialisableOutputStreamWriter so = new SerialisableOutputStreamWriter(System.out);
            message.serialize(so);
            so.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("");

    }

    public void printResponse(Message message, URL url) {
        try {
            HttpURLConnection con = (HttpURLConnection) url.openConnection(proxy);
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setUseCaches(false);
            SerialisableOutputStreamWriter so = new SerialisableOutputStreamWriter(con.getOutputStream());
            so.write("data=");
            message.serialize(so);
            so.flush();
            System.out.println("RESPONSE FROM SERVER: ");
            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF8"))) {
                String currentLine;
                while ((currentLine = in.readLine()) != null) {
                    LOGGER.debug(currentLine);
                }
            }
        } catch (Exception ignore) {
            ignore.printStackTrace();

        }
//
    }

    private Message doSendMessage(final Message message) throws IOException {
        URL url = null;
        SerialisableOutputStreamWriter so = null;
        try {
            url = new URL(XBookConfiguration.getWebAppURL()+"connect.php");


            HttpURLConnection con = (HttpURLConnection) url.openConnection(proxy);
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setUseCaches(true);
            so = new SerialisableOutputStreamWriter(con.getOutputStream());
            so.write("data=");
            message.serialize(so);
            so.flush();
            // printMessage(message, url);
            SerialisableInputStreamReader is = new SerialisableInputStreamReader(con.getInputStream(), factory);
            if (XBookConfiguration.DISPLAY_NETWORK_INFORMATION) {
                printMessage(message);
                printResponse(message, new URL(XBookConfiguration.getWebAppURL() + "connect.php?debug=true"));
            }
            return (Message) is.deserialize();
        } catch (Exception ex) {
            final URL finalUrl = url;
            new Thread(new Runnable() {
                @Override
                public void run() {

                    printMessage(message);
                    printResponse(message, finalUrl);
                }
            }).start();
            isRunning = false;
            Footer.setConnectionStatus(Footer.Connection.DISCONNECTED);
            throw ex;
        } finally {
            if (so != null) {
                so.close();
            }

        }
    }

    @Override
    public Message sendMessage(Message message) throws NotConnectedException {
        try {
            Message returnMessage = doSendMessage(message);
            if (!returnMessage.getResult().wasSuccessful()) {
                if (returnMessage.getResult().getStatus() == Result.UNKNOWN_COMMAND) {

                    new Exception(returnMessage.getResult().getErrorMessage() +": "+ message.getCommand()).printStackTrace();
                }
                //try logging in and then send message again
                else if (returnMessage.getResult().getStatus() == Result.NOT_LOGGED_IN) {
                    if (controller.isLoggedInGlobal()) {
                        controller.serverLogin();
                        returnMessage = doSendMessage(message);
                    }
                } else if (returnMessage.getResult().getStatus() == Result.LIMITED_FUNCTIONALITY) {
                    controller.update(null, ApiControllerAccess.Event.SERVER_DISCONNECTED);
                }
            }
            return returnMessage;

        } catch (IOException ex) {
            LOGGER.error("error On command: " + message.getCommand(), ex);

            System.out.println("");
            SerialisableOutputStreamWriter so;
            try {
                so = new SerialisableOutputStreamWriter(System.out);
                message.serialize(so);
                so.flush();
            } catch (IOException ignore) {

            }
            if (ex instanceof UnknownHostException || ex instanceof NoRouteToHostException) {
                throw new NotConnectedException();
            }

            return new Message();
        }
    }

    @Override
    public void addObserver(Observer obs) {

    }

    public void cgiTest() {
        try {
            HttpURLConnection con = (HttpURLConnection) new URL("http://localhost/test/test.cgi").openConnection(proxy);
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setUseCaches(false);

            SerialisableOutputStreamWriter w = new SerialisableOutputStreamWriter(con.getOutputStream());
            w.write("data=Hallo öasdaldj´ß");
            w.flush();
//        SerialisableInputStreamReader r = new SerialisableInputStreamReader(con.getInputStream(), factory);
//        Message m = (Message) r.deserialize();
//       // System.out.println(r.readBoolean());

            //System.out.println(m);
            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF8"))) {
                String currentLine;
                while ((currentLine = in.readLine()) != null) {
                    System.out.println(currentLine);
                }
            }
        } catch (IOException ex) {
            LOGGER.error( null, ex);
        }
    }

    public void phpTest() {
        try {
            HttpURLConnection con = (HttpURLConnection) new URL("http://localhost/test/test.php").openConnection(proxy);
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setUseCaches(false);

            SerialisableOutputStreamWriter w = new SerialisableOutputStreamWriter(con.getOutputStream());
            String text = "Hallo öasdaldj´ß li%èvre";
            //text = "Ein normaler text ohne sonderzeichen";
            System.out.println(text.length());
            System.out.println(text.getBytes().length);
            w.write("data=");
            w.writeString(text);
            w.flush();
//            SerialisableInputStreamReader r = new SerialisableInputStreamReader(con.getInputStream(), factory);
//            r.readString();
//            System.out.println("1 "+r.readRealShort());
//            System.out.println("2 "+r.readRealShort());
//            System.out.println("3 "+r.readRealShort());
//            System.out.println("4 "+r.readRealShort());
//            System.out.println("5 "+r.readRealShort());
//            System.out.println("6 "+r.readRealShort());
//        Message m = (Message) r.deserialize();
//       // System.out.println(r.readBoolean());

            //System.out.println(m);
            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String currentLine;
                //no idea why but this seems to solve the problem
                in.read();
                while ((currentLine = in.readLine()) != null) {
                    System.out.println(currentLine);

//                    for (int i = 0; i < currentLine.length(); i++) {
//                        System.out.println(i + ": " + currentLine.charAt(i));
//                    }
                }
            }
        } catch (IOException ex) {
            LOGGER.error( null, ex);
        }
    }

    public void sendTEstMessage(Message message) throws NotConnectedException, NotLoggedInException, IOException {
        HttpURLConnection con = (HttpURLConnection) new URL("http://xbook.vetmed.uni-muenchen.de/webapp/functions/test.php").openConnection(proxy);

        con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setUseCaches(false);

        SerialisableOutputStreamWriter w = new SerialisableOutputStreamWriter(con.getOutputStream());
        w.write("data=");
        message.serialize(w);
        w.flush();
//        SerialisableInputStreamReader r = new SerialisableInputStreamReader(con.getInputStream(), factory);
//        Message m = (Message) r.deserialize();
//       // System.out.println(r.readBoolean());

        //System.out.println(m);
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String currentLine;
            while ((currentLine = in.readLine()) != null) {

                System.out.println(currentLine);
            }
        }
    }

    public void sendTestMessage2(Message message) throws NotConnectedException, NotLoggedInException, IOException {
        HttpURLConnection con = (HttpURLConnection) new URL("http://xbook.vetmed.uni-muenchen.de/webapp/functions/test.php").openConnection(proxy);
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setUseCaches(false);

        SerialisableOutputStreamWriter w = new SerialisableOutputStreamWriter(con.getOutputStream());
        w.write("data=");
        message.serialize(w);
        w.flush();
//        SerialisableInputStreamReader r = new SerialisableInputStreamReader(con.getInputStream(), factory);
//        Message m = (Message) r.deserialize();
//       // System.out.println(r.readBoolean());

        //System.out.println(m);
        StringBuilder test1 = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String currentLine;
            while ((currentLine = in.readLine()) != null) {
                test1.append(currentLine);
            }
        }
        String compareString = test1.toString();
        for (int i = 0; i < 10; i++) {
            con = (HttpURLConnection) new URL("http://xbook.vetmed.uni-muenchen.de/webapp/functions/test.php").openConnection(proxy);
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setUseCaches(false);

            w = new SerialisableOutputStreamWriter(con.getOutputStream());
            w.write("data=");
            message.serialize(w);
            w.flush();

            StringBuilder test = new StringBuilder();
            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String currentLine;
                while ((currentLine = in.readLine()) != null) {
                    test.append(currentLine);
                }
            }
            if (!test.toString().equals(compareString)) {
                System.out.println("ungleich!!!");
                System.out.println(test);
                System.out.println(compareString);
            }
        }

    }

}
