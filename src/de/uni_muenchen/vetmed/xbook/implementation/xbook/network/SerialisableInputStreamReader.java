package de.uni_muenchen.vetmed.xbook.implementation.xbook.network;

import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;

import java.io.*;

/**
 * Created by lohrer on 19.11.2014.
 */
public class SerialisableInputStreamReader extends InputStreamReader implements SerialisableInputInterface {

    AbstractSerialisableFactory factory;
    Commands[] myEnumValues = Commands.values();
    InputStream in;

    public SerialisableInputStreamReader(InputStream in, AbstractSerialisableFactory factory) throws UnsupportedEncodingException {
        super(in, "UTF-8");//
        this.factory = factory;
        this.in = in;
    }

    public int readRealShort() throws IOException {
        int ch2 = read();
        int ch1 = read();
        int size = ((ch1 << 8) + (ch2 << 0));
        return size;
    }

    public String readByte() throws IOException {
        char[] buf = new char[3];
        buf[0] = (char) read();
        buf[1] = (char) read();
        buf[2] = (char) read();

        return new String(buf);
    }

    public int readShort() throws IOException {
        String b1 = readByte();
        String b2 = readByte();
        try {
            int ch1 = Integer.parseInt(b1);
            int ch2 = Integer.parseInt(b2);

            int size = ((ch1 << 8) + (ch2 << 0));

            return size;
        } catch (NumberFormatException ex) {
            throw ex;
        }
    }

    @Override
    public String readString() throws IOException {
        StringBuilder sb = new StringBuilder();
        int numberOfStrings = readShort();
        for (int j = 0; j < numberOfStrings; j++) {
            int size = readShort();
            char[] buf = new char[size];
            for (int i = 0; i < size; i++) {
                int read = read();

                if (read == -1) {
                    System.out.println("-1!!!");
                }
                buf[i] = (char) read;
            }
            sb.append(buf);
        }
        return sb.toString();
    }

    @Override
    public int readInt() throws IOException {
        try {
            return Integer.parseInt(readString());
        } catch (NumberFormatException ex) {
            throw ex;
        }
    }

    @Override
    public boolean readBoolean() throws IOException {
        int ch = read();
        if (ch < 0) {
            throw new EOFException();
        }
        return (ch != 0);
    }

    @Override
    public Commands readCommand() throws IOException {
        return myEnumValues[readInt()];
    }

    @Override
    public <T extends Serialisable>T deserialize() throws IOException {
        return factory.deserialise(this);
    }
}
