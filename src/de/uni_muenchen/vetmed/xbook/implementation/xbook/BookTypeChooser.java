package de.uni_muenchen.vetmed.xbook.implementation.xbook;

import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import com.jidesoft.plaf.LookAndFeelFactory;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.languageChooser.LanguageChooser;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class BookTypeChooser extends JFrame {

	/**
	 * The default size of the frame.
	 */
	private final Dimension FRAME_SIZE = new Dimension(550, 650);
	/**
	 * The label for the language selection.
	 */
	private MultiLineTextLabel labelLanguage;
	/**
	 * The label for the book selection.
	 */
	private MultiLineTextLabel labelSelect;
	/**
	 * The label for the checkbox selection.
	 */
	private MultiLineTextLabel labelSave;
	/**
	 * The checkbox object.
	 */
	private JCheckBox check;
	/**
	 * The next button object.
	 */
	private JButton nextButton;
	/**
	 * The language chooser object.
	 */
	private LanguageChooser lang;
	/**
	 * The image button for OssoBook.
	 */
	private JLabel ossobookButton;
	/**
	 * The image button for ArchaeoBook.
	 */
	private JLabel archaeobookButton;
	/**
	 * The image button for AnthroBook.
	 */
	private JLabel anthroButton;
	/**
	 * The image button for ExcaBook.
	 */
	private JLabel excaButton;
	/**
	 * The image button for DemoBook.
	 */
	private JLabel demoButton;
	/**
	 * The image button for AnthroDepot.
	 */
	private JLabel inBookButton;
	/**
	 * The image button for PalaeoDepot.
	 */
	private JLabel palaeoDepotButton;
	/**
	 * The image button for AnthroDepot.
	 */
	private JLabel anthroDepotButton;
	/**
	 * The selected application.
	 */
	private Integer selectedApplication = null;
	/**
	 * Should be <code>true</code> as long as the exit button was not pressed.
	 */
	private boolean sleep = true;

	/**
	 *
	 */
	public BookTypeChooser() {
		super("");

		// set the favicon
		setFavicon();

		// set the default starting size and the minimum size of the application
		setSize(FRAME_SIZE);
		setMinimumSize(FRAME_SIZE);

		// center the window on the desktop on start
		setLocationRelativeTo(null);

		// the application should exit when clicking on the close button
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// set the Look&Feel
		try {
			UIManager.setLookAndFeel(WindowsLookAndFeel.class.getName());
			LookAndFeelFactory.installJideExtension();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		setLayout();

		// set English as the standard language
		String language = XBookConfiguration.getProperty(XBookConfiguration.LANGUAGE);
		translate(language);
		lang.setSelectedItem(language);

		setNoApplicationSelected();

		setVisible(true);
	}

	/**
	 * Set up the layout.
	 */
	private void setLayout() {
		setLayout(new FlowLayout());

		labelLanguage = new MultiLineTextLabel("");
		add(labelLanguage);

		JPanel languageWrapper = new JPanel();
		languageWrapper.setBorder(new EmptyBorder(0, 0, 10, 0));
		lang = new LanguageChooser(true);
		lang.setSize(300, 40);
		lang.setPreferredSize(new Dimension(300, 40));
		lang.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				translate((String) lang.getSelectedItem());
			}
		});
		languageWrapper.add(lang);
		add(languageWrapper);

		labelSelect = new MultiLineTextLabel("");
		add(labelSelect);

		JPanel imageWrapper = new JPanel(new GridLayout(4, 2));

		// OssoBook button
		ossobookButton = new JLabel();
		addCustomMouseListener(ossobookButton, XBookConfiguration.OSSOBOOK);
		ossobookButton.setIcon(Images.BOOK_SELECTION_BUTTON_OSSOBOOK);
		imageWrapper.add(ossobookButton);

		// ArchaeoBook button
		archaeobookButton = new JLabel();
		addCustomMouseListener(archaeobookButton, XBookConfiguration.ARCHAEOBOOK);
		archaeobookButton.setIcon(Images.BOOK_SELECTION_BUTTON_ARCHAEOBOOK);
		imageWrapper.add(archaeobookButton);

		// ExcaBook button
		excaButton = new JLabel();
		addCustomMouseListener(excaButton, XBookConfiguration.EXCABOOK);
		excaButton.setIcon(Images.BOOK_SELECTION_BUTTON_EXCABOOK);
		imageWrapper.add(excaButton);

		// AnthroBook button
		anthroButton = new JLabel();
		addCustomMouseListener(anthroButton, XBookConfiguration.ANTHROBOOK);
		anthroButton.setIcon(Images.BOOK_SELECTION_BUTTON_ANTHROBOOK);
		imageWrapper.add(anthroButton);

		// DemoBook button
		demoButton = new JLabel();
		addCustomMouseListener(demoButton, XBookConfiguration.DEMOBOOK);
		demoButton.setIcon(Images.BOOK_SELECTION_BUTTON_DEMOBOOK);
		imageWrapper.add(demoButton);

		// InBook button
		inBookButton = new JLabel();
		addCustomMouseListener(inBookButton, XBookConfiguration.INBOOK);
		inBookButton.setIcon(Images.BOOK_SELECTION_BUTTON_INBOOK);
		imageWrapper.add(inBookButton);

		// PalaeoDepot button
		palaeoDepotButton = new JLabel();
		addCustomMouseListener(palaeoDepotButton, XBookConfiguration.PALAEODEPOT);
		palaeoDepotButton.setIcon(Images.BOOK_SELECTION_BUTTON_PALAEODEPOT);
		imageWrapper.add(palaeoDepotButton);

		// AnthroDepot button
		anthroDepotButton = new JLabel();
		addCustomMouseListener(anthroDepotButton, XBookConfiguration.ANTHRODEPOT);
		anthroDepotButton.setIcon(Images.BOOK_SELECTION_BUTTON_ANTHRODEPOT);
		imageWrapper.add(anthroDepotButton);

		add(imageWrapper);

		JPanel nextButtonWrapper = new JPanel();
		nextButtonWrapper.setBorder(new EmptyBorder(0, 0, 10, 0));
		nextButton = new JButton("");
		nextButton.setEnabled(false);
		nextButton.setSize(300, 40);
		nextButton.setPreferredSize(new Dimension(150, 40));
		nextButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				openBook();
			}
		});
		nextButtonWrapper.add(nextButton);
		add(nextButtonWrapper);

		labelSave = new MultiLineTextLabel("");
		add(labelSave);

		check = new JCheckBox("");
		add(check);
	}

	/**
	 * Opens the selected book.
	 */
	private void openBook() {
		synchronized (BookTypeChooser.this) {
			if (selectedApplication != null) {
				setVisible(false);
				sleep = false;
				BookTypeChooser.this.notify();
			}
		}
	}

	/**
	 * Setup the mouse listener for specific application buttons.
	 *
	 * @param label The JLabel object holding the application button image.
	 * @param applicationId The ID of the application.
	 */
	private void addCustomMouseListener(final JLabel label, final int applicationId) {
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					openBook();
				};
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				setApplicationSelected(applicationId);
				nextButton.setEnabled(true);
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				label.setBorder(BorderFactory.createLineBorder(new Color(120, 120, 120), 3));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (selectedApplication == null || applicationId != selectedApplication) {
					label.setBorder(BorderFactory.createLineBorder(new Color(240, 240, 240), 3));
				} else {
					label.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
				}
			}
		});
	}

	/**
	 * Translates all Strings of the book type chooser.
	 *
	 * @param languageCode The language code.
	 */
	private void translate(String languageCode) {
		// first translate everything in English language
		setTitle("Book Type Chooser");
		labelLanguage.setText("Select your preferred language.");
		labelSelect.setText("Please select the application you want to run.");
		labelSave.setText("If you do not want to be asked again please select the checkbox to save your selection. You can always change the selecten in the settings.");
		check.setText("Do not ask again");
		nextButton.setText("Next...");

        // then translate the texts into another language if its not the English one
		// this garantees that the English text will be displayed when there is no translation available
		switch (languageCode) {
			case "de":
			case "de_ch":
				setTitle("Book Typ Auswahl");
				labelLanguage.setText("Wählen Sie Ihre bevorzugte Sprache.");
				labelSelect.setText("Bitte wählen Sie die Anwendung, die Sie starten möchten.");
				labelSave.setText("Wenn Sie nicht mehr gefragt werden möchten, markieren Sie bitte die CheckBox um Ihre Auswahl zu speichern. Sie können die Auswahl in den Einstellungen jederzeit ändern.");
				check.setText("Nicht wieder fragen");
				nextButton.setText("Weiter...");
				break;
			case "fr":
				break;
			case "es":
				break;
		}
	}

	/**
	 * Returns the book type ID.
	 *
	 * @return The book type ID.
	 */
	public synchronized int getBookType() {
		try {
			while (sleep) {
				wait();
			}
			if (selectedApplication == null) {
				return XBookConfiguration.NO_BOOK;
			} else {
				return selectedApplication;
			}
		} catch (InterruptedException ex) {
			return XBookConfiguration.NO_BOOK;
		}
	}

	/**
	 * Returns whether the "Do not show it again" button is selected.
	 *
	 * @return <code>true</code> if the button is selected, <code>false</code> else.
	 */
	public boolean isDoNotShowItAgainSelected() {
		return check.isSelected();
	}

	/**
	 * Get the selected langage as a language code.
	 *
	 * @return The language code.
	 */
	public String getLanguage() {
		return (String) lang.getSelectedItem();
	}

	/**
	 * Set a specific application selected.
	 *
	 * @param id The id of the application that is selected.
	 */
	private void setApplicationSelected(Integer id) {
		setNoApplicationSelected();
		if (id != null) {
			switch (id) {
				case XBookConfiguration.OSSOBOOK:
					ossobookButton.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
					selectedApplication = id;
					break;
				case XBookConfiguration.ARCHAEOBOOK:
					archaeobookButton.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
					selectedApplication = id;
					break;
				case XBookConfiguration.ANTHROBOOK:
					anthroButton.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
					selectedApplication = id;
					break;
				case XBookConfiguration.EXCABOOK:
					excaButton.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
					selectedApplication = id;
					break;
				case XBookConfiguration.DEMOBOOK:
					demoButton.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
					selectedApplication = id;
					break;
				case XBookConfiguration.INBOOK:
					inBookButton.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
					selectedApplication = id;
					break;
				case XBookConfiguration.PALAEODEPOT:
					palaeoDepotButton.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
					selectedApplication = id;
					break;
				case XBookConfiguration.ANTHRODEPOT:
					anthroDepotButton.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
					selectedApplication = id;
					break;
			}
		}
	}

	/**
	 * Set no application selected.
	 */
	private void setNoApplicationSelected() {
		selectedApplication = null;
		ossobookButton.setBorder(BorderFactory.createLineBorder(new Color(240, 240, 240), 3));
		archaeobookButton.setBorder(BorderFactory.createLineBorder(new Color(240, 240, 240), 3));
		anthroButton.setBorder(BorderFactory.createLineBorder(new Color(240, 240, 240), 3));
		excaButton.setBorder(BorderFactory.createLineBorder(new Color(240, 240, 240), 3));
		demoButton.setBorder(BorderFactory.createLineBorder(new Color(240, 240, 240), 3));
		inBookButton.setBorder(BorderFactory.createLineBorder(new Color(240, 240, 240), 3));
		palaeoDepotButton.setBorder(BorderFactory.createLineBorder(new Color(240, 240, 240), 3));
		anthroDepotButton.setBorder(BorderFactory.createLineBorder(new Color(240, 240, 240), 3));
	}

	/**
	 * Set the favicon of the frame.
	 */
	private void setFavicon() {
		setIconImages(Arrays.asList(
				Images.FAVICON_XBOOK_16.getImage(),
				Images.FAVICON_XBOOK_24.getImage(),
				Images.FAVICON_XBOOK_32.getImage(),
				Images.FAVICON_XBOOK_48.getImage(),
				Images.FAVICON_XBOOK_64.getImage(),
				Images.FAVICON_XBOOK_96.getImage(),
				Images.FAVICON_XBOOK_128.getImage()));
	}
}
