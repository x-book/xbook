package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer;

import java.util.*;
import java.util.Map.Entry;

/*
 * TODO need some important validation, see OssoBookImporter (stand alone version)
 */
public class UserMainTable implements Iterable<Entry<String, ArrayList<String>>> {
    private Map<String, ArrayList<String>> columnTableMap = null;
    private String[][] table = null;

    public UserMainTable(final String[][] table) {
        this.table = createTableWithUniqueColumnLabels(table);
        columnTableMap = createColumnTableMap(this.table);
    }

    @Override
    public Iterator<Entry<String, ArrayList<String>>> iterator() {
        return columnTableMap.entrySet().iterator();
    }

    public ArrayList<String> getColumnLabels() {
        return new ArrayList<>(columnTableMap.keySet());
    }

    private String[][] createTableWithUniqueColumnLabels(String[][] table) {
        String[][] tmp = copyTable(table);
        Set<String> header = new HashSet<>();
        for (int col = 0; col < tmp[0].length; col++) {
            if (!header.add(tmp[0][col])) {
                tmp[0][col] = tmp[0][col] + "$" + UUID.randomUUID().toString();
                header.add(tmp[0][col]);
            }
        }
        return tmp;
    }

    private String[][] copyTable(String[][] table) {
        String[][] cTable = new String[table.length][table[0].length];
        for (int row = 0; row < cTable.length; row++) {
            for (int col = 0; col < cTable[0].length; col++) {
                cTable[row][col] = table[row][col];
            }
        }
        return cTable;
    }

    private Map<String, ArrayList<String>> createColumnTableMap(String[][] table) {
        Map<String, ArrayList<String>> tableMap = new Hashtable<>();
        for (int col = 0; col < table[0].length; col++) {
            String columnLabel = table[0][col];
            ArrayList<String> columnSet = new ArrayList<>();
            for (int row = 1; row < table.length; row++) {
                columnSet.add(table[row][col]);
            }
            tableMap.put(columnLabel, columnSet);
        }
        return tableMap;
    }

    public List<Map<String, String>> rows() {
        List<Map<String, String>> rows = new ArrayList<>();
        for (int row = 1; row < table.length; row++) {
            Map<String, String> rowMap = new Hashtable<>();
            for (int col = 0; col < table[row].length; col++) {
                rowMap.put(table[0][col], table[row][col]);
            }
            rows.add(rowMap);
        }
        return rows;
    }

    /**
     * Returns the Rows
     * @param start
     * @param end
     * @return
     */
    public List<Map<String, String>> rows(int start, int amount) {
        List<Map<String, String>> rows = new ArrayList<>();
        for (int row = 1 + start; row < table.length && row-start<=amount; row++) {
            Map<String, String> rowMap = new Hashtable<>();
            for (int col = 0; col < table[row].length; col++) {
                rowMap.put(table[0][col], table[row][col]);
            }
            rows.add(rowMap);
        }
        return rows;
    }

    public int size() {
        return table.length -1;
    }


    public static void main(String[] args){
        String[][] test = new String [][]{{"Test"},{"0"},{"1"},{"2"},{"3"},{"4"}};
        final UserMainTable entries = new UserMainTable(test);
        boolean moreEntries = true;
        int offset = 0;
        int NUMBER_OF_ENTRIES = 4;
        final int numberOfEntries = entries.size();
        while (moreEntries) {

            final List<Map<String, String>> rows = entries.rows(offset, NUMBER_OF_ENTRIES);
            offset += NUMBER_OF_ENTRIES;
            if (offset > numberOfEntries) {
                moreEntries = false;
            }
            print(rows);
        }
    }

    private static void print(List<Map<String, String>> rows) {
        System.out.println("-----------");
        for (Map<String, String> row : rows) {
            for (String s : row.values()) {
                System.out.println(s);
            }
        }
    }
}
