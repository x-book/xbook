package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer.lookup;

import java.util.ArrayList;
import java.util.Set;

public interface LookupTableBuilder<D, C, T> {
	public void setDomain(ArrayList<D> domain);

	public void setCodomain(ArrayList<C> codomain);

	public ArrayList<C> getCodomain();

	public ArrayList<D> getDomain();

	/**
	 * g is an one-to-one function that maps elements c of getCodomain() to
	 * elements e, where elements e and elements d of getDomain() share the same
	 * type and thus can be compared with each other.
	 * 
	 * @param c
	 * @return
	 */
	public D convertValue(C c);

	/**
	 * For all elements d in getDomain() if there is an element c in
	 * getCodomain() where g(c)=d, then create a map f(d)=c otherwise create a
	 * map f(d)=null.
	 */
	public void createMap();

	/**
	 * Maps d to c. Where d is an element of getDomain() and c is an element of
	 * getCodomain(). Otherwise no mapping is done.
	 * 
	 * @param x
	 * @param y
	 */
	public void mapsTo(D d, C c);

	/**
	 * Returns true if every element of getDomain() maps to an element of
	 * getCodomain(). Returns false if there is an element in D that maps to
	 * null.
	 * 
	 * @return true or false
	 */
	public boolean isMapComplete();

	/**
	 * If map is complete, then create a lookup table, otherwise return null
	 * 
	 * @param <T>
	 * 
	 * @return - LookupTable or null
	 */
	public LookupTable<D, T> createLookupTable();

	/**
	 * Get all elements d of getDomain() where f(d)=null
	 * 
	 * @return - Set<D>
	 */
	public Set<D> getUnmappedElements();

	/**
	 * f is an one-to-one function that maps elements c of getCodomain() to
	 * elements t, where each element t is somehow related to the xbook
	 * database.
	 * 
	 * @param c
	 * @return
	 */
	public T convertKey(C c);
}
