package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class CsvTable {
	public static String[][] openCsvFile(String filePath, char separator,
			char quotechar, char escape, int line, boolean strictQuotes,String charset,
			boolean ignoreLeadingWhiteSpace) {
		try {
			                 InputStreamReader fileReader = new InputStreamReader(new FileInputStream(filePath),charset);
			CSVReader csvReader = new CSVReader(fileReader, separator,
					quotechar, escape, line, strictQuotes,
					ignoreLeadingWhiteSpace);
			List<String[]> rows = csvReader.readAll();
			csvReader.close();
			return rows.toArray(new String[][] {});
		} catch (Exception e) {
			// file not found or csvReader.readAll() failed
			return null;
		}
	}

	public static void writeCsvFile(String filePath, String[][] table)
			throws IOException {
		CSVWriter writer = new CSVWriter(new FileWriter(filePath), ';', '\"');
		for (String[] row : table) {
			writer.writeNext(row);
		}
		writer.close();
	}
}
