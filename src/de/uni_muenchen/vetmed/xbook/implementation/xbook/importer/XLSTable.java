//package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer;
//
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.List;
//import jxl.Sheet;
//import jxl.Workbook;
//import jxl.read.biff.BiffException;
//import jxl.write.Label;
//import jxl.write.WritableSheet;
//import jxl.write.WritableWorkbook;
//import jxl.write.WriteException;
//
///**
// *
// * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
// */
//public class XLSTable {
//
//    public static String[][] openXLSFile(String filePath, int startLine,int sheetNumber) {
//        try {
//
//            Workbook wb = Workbook.getWorkbook(new FileInputStream(filePath));
//            Sheet sheet = wb.getSheet(sheetNumber);
//            int columns = sheet.getColumns();
//            int rows = sheet.getRows() - startLine;
//            String[][] arr = new String[rows][columns];
//            for (int r = startLine; r < rows; r++) {
//                for (int c = 0; c < columns; c++) {
//                    arr[r][c] = sheet.getCell(c, r).getContents();
//                }
//            }
//            wb.close();
//            return arr;
//        } catch (IOException | BiffException | IndexOutOfBoundsException e) {
//            // file not found or csvReader.readAll() failed
//            return null;
//        }
//    }
//
//    public static void writeXLSFile(String filePath,String sheetName, String[][] table)
//            throws IOException, WriteException {
//        WritableWorkbook wb = Workbook.createWorkbook(new FileOutputStream(filePath));
//        WritableSheet sheet = wb.createSheet(sheetName, 0);
//        for (int row = 0; row < table.length; row++) {
//            for (int col = 0; col < table[0].length; col++) {
//                Label label = new Label(col, row, table[row][col]);
//                sheet.addCell(label);
//            }
//        }
//        wb.write();
//        wb.close();
//    }
//}
