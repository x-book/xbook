package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer.lookup;

public interface LookupTable<D, C> {
	public C map(D d);
}
