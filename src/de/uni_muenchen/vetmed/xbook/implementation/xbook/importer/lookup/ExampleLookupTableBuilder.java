package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer.lookup;

import java.util.Map.Entry;

public class ExampleLookupTableBuilder extends
		LookupTableBuilderAdapter<String, Entry<Integer, String>, Integer> {

	@Override
	public String convertValue(Entry<Integer, String> c) {
		return c.getValue();
	}

	@Override
	public Integer convertKey(Entry<Integer, String> c) {
		return c.getKey();
	}

}
