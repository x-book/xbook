package de.uni_muenchen.vetmed.xbook.implementation.xbook.importer.lookup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public abstract class LookupTableBuilderAdapter<D, C, T> implements
        LookupTableBuilder<D, C, T> {

    private Map<D, C> lookupTable = new HashMap<>();
    private ArrayList<C> codomain = new ArrayList<>();

    protected Map<D, C> getInternalMap() {
        return lookupTable;
    }

    @Override
    public void setDomain(ArrayList<D> domain) {
        if (!domain.isEmpty() && domain.get(0) instanceof String) {
            Collections.sort(domain, new Comparator<D>() {
                @Override
                public int compare(D o1, D o2) {
                    return o1.toString().toUpperCase().compareTo(o2.toString().toUpperCase());
                }
            });
        }
        for (D element : domain) {
            lookupTable.put(element, lookupTable.get(element));
        }
    }

    @Override
    public ArrayList<D> getDomain() {
        return new ArrayList(lookupTable.keySet());
    }

    @Override
    public void setCodomain(ArrayList<C> codomain) {
        if (!codomain.isEmpty()) {
            if (codomain.get(0) instanceof String) {
                Collections.sort(codomain, new Comparator<C>() {
                    @Override
                    public int compare(C o1, C o2) {
                        return o1.toString().toUpperCase().compareTo(o2.toString().toUpperCase());
                    }
                });
            } else if (codomain.get(0) instanceof Entry) {
                Collections.sort(codomain, new Comparator<C>() {
                    @Override
                    public int compare(C o1, C o2) {
                        return ((Entry<String, String>) o1).getValue().compareTo(((Entry<String, String>) o2).getValue());
                    }
                });
            }
        }


        this.codomain.clear();

        this.codomain.addAll(codomain);
    }

    @Override
    public ArrayList<C> getCodomain() {
        return codomain;
    }

    @Override
    public boolean isMapComplete() {
        for (Entry<?, ?> entry : lookupTable.entrySet()) {
            if (entry.getValue() == null) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void createMap() {
        for (C element : codomain) {
            if (lookupTable.containsKey(convertValue(element))) {
                lookupTable.put(convertValue(element), element);
            }
        }
    }

    @Override
    public void mapsTo(D d, C c) {
        if (lookupTable.containsKey(d) && codomain.contains(c)) {
            lookupTable.put(d, c);
        }
    }

    @Override
    public Set<D> getUnmappedElements() {
        Set<D> unmappedElements = new HashSet<>();
        for (Entry<D, C> entry : lookupTable.entrySet()) {
            if (entry.getValue() == null) {
                unmappedElements.add(entry.getKey());
            }
        }
        return unmappedElements;
    }

    @Override
    public LookupTable<D, T> createLookupTable() {
        return new LookupTable<D, T>() {
            @Override
            public T map(D d) {
                return convertKey(lookupTable.get(d));
            }
        };
    }
}
