package de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTableOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInt;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableString;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractProjectManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractQueryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractSynchronisationManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Result;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public final class Synchroniser extends Thread {

  private static final Log LOGGER = LogFactory.getLog(Synchroniser.class);

  private AbstractSynchronisationController<?, ?> controller;
  private boolean isUpdateAvailable = true;
  private boolean doSpecificUpdate = false;
  private boolean isWaiting = false;
  private boolean isError;
  private ArrayList<ProjectDataSet> projects;
  private ArrayList<PropertyChangeListener> listeners;
  private EntriesSynchronisationProgress oldProgress;
  ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
  Runnable r;
  ScheduledFuture<?> schedule;


  /**
   * Interrupts the current update cycle and restarts
   */
  private boolean interruptUpdate;
  private long time;
  private TimeUnit unit;

  public Synchroniser(AbstractSynchronisationController controller) {
    this.controller = controller;
    this.listeners = new ArrayList<>();
    this.projects = new ArrayList<>();
    r = new Runnable() {
      @Override
      public void run() {
        synchronized (Synchroniser.this) {
          doSpecificUpdate = true;
          Synchroniser.this.notifyAll();
        }
      }
    };
  }

  /**
   * Check if there is uncommited data or not yet synchronized data on the server
   */
  public void setUpdateAvailable() {
    isUpdateAvailable = true;
    System.out.println("update available");
    if (isWaiting) {
      synchronized (this) {

        notify();
      }
    }
  }

  @Override
  public synchronized void run() {
    try {
      while (true) {
        while ((!doSpecificUpdate && !isUpdateAvailable) || !controller.isServerConnected()
            || !controller.isLoggedInGlobal()) {
          try {
            if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
              System.out.println("waiting");
            }

            isWaiting = true;
            boolean doTimedSync = false;
            if (doTimedSync) {
              startSchedule(time, unit);
            }
            wait();
            isWaiting = false;
          } catch (InterruptedException ex) {
            Logger.getLogger(Synchroniser.class.getName()).log(Level.SEVERE, null, ex);
          }
        }

        isError = false;
        update(SynchronisationProgress.Type.STARTED, Loc.get("SYNCHRONISATION_STARTED"));
        Date date = new Date();
        doSpecificUpdate = false;
        interruptUpdate = false;//started new reset interrupt
        //new data to sync either (or both) way
        boolean syncall = projects.isEmpty();
        boolean autosync = XBookConfiguration.getBookBoolean(XBookConfiguration.AUTOSYNC);

        try {
          AbstractQueryManager<?, ?> manager = controller.getLocalManager();
          ArrayList<IBaseManager> syncInterfaces = manager.getSyncTables();
          int countNonEntryData = syncInterfaces.size() * 2;
          // first go through all projects
          int projectCount = 0;
          int numberOfProjects = projects.size();
          ArrayList<ProjectDataSet> projectList = controller.getProjects();
          if (numberOfProjects == 0) {
            numberOfProjects = projectList.size();
          }
          for (ProjectDataSet project : projectList) {
            if (interruptUpdate) { // break all start new
              break;
            }
            if (syncall && !autosync) { // only sync if autosync is enabled
              continue;
            }
            if (!syncall && !projects.contains(project)) { // only specified projects
              continue;
            }
            if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
              System.out.println("Sync project " + project);
            }

            try {
              //check rights for this project
              if (!controller.hasReadRights(project) && !controller.hasWriteRights(project)) {
                System.out.println("No right " + project);
                continue;
              }

              update(SynchronisationProgress.Type.START_NEW_PROJECT,
                  Loc.get("SYNCHRONISATION_OF_PROJECT_STARTED", project), projectCount++,
                  numberOfProjects);

              ///////WRITING///////////
              int tableCount = 0;

              if (controller.hasWriteRights(project)) { // only commit if user has rights to do so

                //make sure project is synced first for rights
                final AbstractProjectManager projectManager = manager.getProjectManager();
                commitData(project, projectManager);
                if (interruptUpdate) {//break all start new
                  break;
                }
                update(SynchronisationProgress.Type.NEXT_STEP, null, tableCount++,
                    countNonEntryData);
                for (IBaseManager table : manager.getSyncTables()) {
                  if (table instanceof AbstractProjectManager) {
                    continue;
                  }
                  if (interruptUpdate) {//break all start new
                    break;
                  }
                  if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
                    LOGGER.info("Synchronising table: " + table);
                  }
                  update(SynchronisationProgress.Type.NEXT_STEP, null, tableCount++,
                      countNonEntryData);
                  commitData(project, table);

                }
              }
              /////////////////////////READING//////////////
              tableCount = countNonEntryData / 2;
              if (interruptUpdate) {//break all start new
                break;
              }
              /////ENTRY DATA///////////////
              if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
                LOGGER.info("entry read");
              }
              for (IBaseManager table : manager.getSyncTables()) {
                if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
                  LOGGER.info("entry read " + table.getTableName());
                }
                update(SynchronisationProgress.Type.NEXT_STEP, null, tableCount++,
                    countNonEntryData);
                //read data from server
                pullData(project, table);

                if (interruptUpdate) {//break all start new
                  break;
                }
                //check if there are some unsynced entries left, this can be only found out now, as before it was possible that the sync was not complete, now it should be
                //that means of course that the changes can only be committed during the next synchronisation process, but at least they will be committed...

                table.updateUnsyncedEntries(project);
                //Deletes all deleted values

              }
            } catch (NoRightException ex) {
              Logger.getLogger(Synchroniser.class.getName()).log(Level.WARNING, null, ex);
            }
            projects.remove(project);
            if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
              LOGGER.info("project complete: " + project);
            }
          }
        } catch (IOException | NotConnectedException | NotLoggedInException | NotLoadedException | StatementNotExecutedException ex) {
          ex.printStackTrace();
        }
        if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
          System.out.println("sync complete");
        }
        if (syncall) {
          isUpdateAvailable = false;
        }
        //sync was interrupted
        if (interruptUpdate) {
          //sync was interrupted
          if (isError) {
            update(SynchronisationProgress.Type.ERROR, Loc.get("SYNCHRONISATION_INTERRUPTED"));
          } else {
            update(SynchronisationProgress.Type.INTERRUPTED,
                Loc.get("SYNCHRONISATION_INTERRUPTED"));
          }
        } else {
          update(SynchronisationProgress.Type.COMPLETE, Loc.get("SYNCHRONISATION_COMPLETE"));
        }
        if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
          Date date2 = new Date();
          System.out.println("Sync took " + (date2.getTime() - date.getTime()));
        }
      }
    } finally {
      update(SynchronisationProgress.Type.ERROR, Loc.get("SYNCHRONISATION_INTERRUPTED"));
    }
  }

  private void commitData(ProjectDataSet project, IBaseManager table)
      throws StatementNotExecutedException, NotLoggedInException, NoRightException, IOException, NotConnectedException, NotLoadedException {

    int numberOfEntries = table.getNumberOfUncommittedEntries(project.getProjectKey());
    if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
      System.out.println("syncing entries " + numberOfEntries);
    }

    DataSetOld entryData;
    int entryNumber = 0;
    while ((entryData = table.getNextUncomittedEntry(project)).hasEntries()) {
      if (interruptUpdate) {//break all start new
        break;
      }
      update(SynchronisationProgress.Type.UPDATE, null, entryNumber++, numberOfEntries);
      if (numberOfEntries < entryNumber) {
        System.out.println("entry number" + entryNumber + " from expected " + numberOfEntries);
        System.out.println(entryData.getProjectKey());
        if (entryData instanceof EntryDataSet) {
          System.out.println(((EntryDataSet) entryData).getEntryKey());
        }
      }
      if (commitEntry(entryData, table, project)) {
        break;
      }

    }

  }

  public boolean commitEntry(DataSetOld entryData, IBaseManager table, ProjectDataSet project)
      throws NotLoadedException, NotLoggedInException, StatementNotExecutedException, IOException, NotConnectedException {

    Message result = this.controller.sendMessage(Commands.COMMIT_ENTRY_DATA, entryData);
    if (!result.getResult().wasSuccessful()) {
      if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {

        System.out.println(result.getResult());
        System.out.println("conflict");
      }
      if (result.getResult().equals(Result.CONFLICT_OCCURED)) {
        boolean isConflict = false;
        DataSetOld data = (DataSetOld) result.getData().get(0);
        for (DataTableOld tableData : data.getTablesList()) {
          if (!tableData.isEmpty()) {
            if (!tableData.datasetsEqual(entryData.getOrCreateDataTable(tableData.getTableName()),
                table.getPrimaryColumnsForTable(tableData.getTableName()))) {
              isConflict = true;
            }
          }
        }
        if (isConflict) {
          table.setConflicted(entryData);
          XBookConfiguration.setConflict(project.getProjectKey(), true);
          if (this.controller.isProjectLoaded() && this.controller.getCurrentProject()
              .equals(project)) {
            Footer.setConflictedButtonVisible(true);
          }
        } else {
          if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
            if (entryData instanceof EntryDataSet) {
              System.out
                  .println("Konflikt in Eintrag: " + ((EntryDataSet) entryData).getEntryKey());
            }
            System.out.println("... der jedoch nicht wichtig ist");
          }
          table.setSynchronised(data, null);

          //update entry...
        }
      } else if (result.getResult().equals(Result.DUPLICATE_KEY)) {
        Footer.displayError(Loc.get("ERROR_DUPLICATE_KEY", entryData.getProjectKey()));
        interruptUpdate = true;
        isError = true;
        return true;
      } else {

        Logger.getLogger(Synchroniser.class.getName())
            .log(Level.SEVERE, "Sync Interrupted: " + result.getResult().getErrorMessage(),
                new Exception(result.getResult().getErrorMessage()));
        interruptUpdate = true;
        isError = true;
        return true;
      }
    } else {
      ArrayList<Serialisable> data = result.getData();
      DataSetOld returnData = null;
      if (!data.isEmpty()) {
        returnData = (DataSetOld) data.get(0);
      }
      table.setSynchronised(entryData, returnData);
    }
    return false;
  }

  public void setScheduleTime(long time, TimeUnit unit) {
    if (schedule != null) {
      if (!schedule.isDone() && schedule.getDelay(this.unit) > 0) {
        schedule.cancel(false);
      }
    }
    this.time = time;
    this.unit = unit;

    startSchedule(time, unit);

  }

  private void startSchedule(long time, TimeUnit unit) {
    schedule = scheduledExecutorService.schedule(r, time, unit);

  }

  private void pullData(ProjectDataSet project, IBaseManager table)
      throws StatementNotExecutedException, NotConnectedException, NotLoggedInException, IOException {
    String lastSynchronization = table.getLastSynchronisation(project.getProjectKey());

    ArrayList<Serialisable> list = new ArrayList<>();
    list.add(project.getProjectKey());
    SerialisableString lastSync = new SerialisableString(lastSynchronization);
    list.add(lastSync);
    list.add(new SerialisableString(table.getTableName()));
    Message ms;
    ms = controller.sendMessage(Commands.GET_NUMBER_OF_ENTRIES_TO_SYNC, list);
    int numberOfEntries = 0;
    if (ms.getResult().wasSuccessful()) {
      numberOfEntries = ((SerialisableInt) ms.getData().get(0)).getValue();
    }
    if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
      System.out.println("number of enties: " + numberOfEntries);
    }
    int entryNumber = 0;
    SerialisableInt firstRequest = new SerialisableInt(1);
    list.add(firstRequest);//maybe we didn't get all data so resend current status
    Commands command;
    if (table instanceof BaseEntryManager) {

      command = Commands.REQUEST_ENTRY_DATA;
    } else {
      command = Commands.REQUEST_PROJECT_DATA;
    }
    while ((ms = controller.sendMessage(command, list)).getResult().wasSuccessful()) {
      if (interruptUpdate) {//break all start new
        break;
      }
      boolean hasTime = false;
      for (Serialisable data : ms.getData()) {
        if (interruptUpdate) {//break all start new
          break;
        }
        update(SynchronisationProgress.Type.UPDATE, null, entryNumber++, numberOfEntries);
        DataSetOld entry = (DataSetOld) data;
        if (!table.isSynchronised(entry) && !hasTime) {//check if entries are synced
          if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
            System.out.println("not sync");
          }

          for (DataRow entryData : entry.getOrCreateDataTable(table.getTableName())) {
            final String syncTime = entryData
                .get(table.getTableName() + "." + AbstractSynchronisationManager.STATUS);

            if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
              System.out.println("last sync:" + syncTime);
            }
            lastSync.setString(syncTime);
            hasTime = true;

          }
          continue;
        } else {

          table.updateEntries(entry);
          if (table instanceof BaseEntryManager && entry instanceof EntryDataSet) {
            BaseEntryManager abm = (BaseEntryManager) table;
            EntryDataSet entryDataSet = (EntryDataSet) entry;
            //go through all AbstractEntryDataManagers and update

            controller.addUpdatedEntry(project, table);
            if (controller.isEntryLoaded(abm, entryDataSet)) {
              controller.doActionOnSyncUpdatingCurrentEntry(abm, entryDataSet);
            }
          }
          if (interruptUpdate) {//break all start new
            break;
          }
        }
        if (interruptUpdate) {//break all start new
          break;
        }
        lastSync.setString(table.getLastSynchronisation(project.getProjectKey()));

      }
      firstRequest.setValue(0);
    }
    if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
      LOGGER.debug( ms.getResult());
    }
  }

  public void update(final ArrayList<ProjectDataSet> projects, boolean force) {
    if (!force && !XBookConfiguration.getBookBoolean(XBookConfiguration.AUTOSYNC)) {
      return;
    }
    new Thread(new Runnable() {
      @Override
      public void run() {
        interruptUpdate = true;
        if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
          System.out.println("update");
        }
        synchronized (Synchroniser.this) {
          Synchroniser.this.projects = projects;
          doSpecificUpdate = true;
          if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
            System.out.println("notify");
          }
          Synchroniser.this.notify();
        }
        if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
          System.out.println("update done");
        }
      }
    }).start();

  }

  public void removeProject(final ProjectDataSet project) {
    if (projects.contains(project)) {
      new Thread(new Runnable() {
        @Override
        public void run() {
          interruptUpdate = true;
          if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
            System.out.println("update");
          }
          synchronized (Synchroniser.this) {
            projects.remove(project);
            if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
              System.out.println("notify");
            }
            Synchroniser.this.notify();
          }
          if (XBookConfiguration.LOG_SYNCHRONISATION_INFORMATION) {
            System.out.println("update done");
          }
        }
      }).start();
    }
  }

  /**
   * Adds a new property listener to the Synchroniser object.
   *
   * @param listener The property listener to add.
   */
  public void addPropertyChangeListener(PropertyChangeListener listener) {
    listeners.add(listener);
  }

  /**
   * Used to updateExtention the progress property of the synchronisation task.
   * <p/>
   * <p>
   * This updates the progress value and dispatches a property change event, with a new instance of
   * {@link SynchronisationProgress} holding the two values specified. </p>
   * <p/>
   * <p>
   * If set, uses the old percentual progress, else uses 0. </p>
   *
   * @param type the type of the progress updateExtention.
   * @param message the message to display.
   */
  private void update(SynchronisationProgress.Type type, String message) {
    if (oldProgress != null) {
      update(type, message, oldProgress.getCurrentEntryCount(), oldProgress.getMaxEntryCount());
    } else {
      update(type, message, 0, 0);
    }
  }

  /**
   * Used to updateExtention the progress property of the synchronization task.
   * <p/>
   * <p>
   * This updates the progress value and dispatches a property change event, with a new instance of
   * {@link SynchronisationProgress} holding the two values specified. </p>
   *
   * @param type the type of the progress updateExtention.
   * @param message the message to display.
   * @param currentEntryCount the current entry count (current progress of the synchronization).
   * @param maxEntryCount the maximum number of entries.
   */
  private void update(SynchronisationProgress.Type type, String message, int currentEntryCount,
      int maxEntryCount) {
    EntriesSynchronisationProgress newProgress = new EntriesSynchronisationProgress(type, message,
        currentEntryCount, maxEntryCount);
    PropertyChangeEvent event = new PropertyChangeEvent(this, XBookConfiguration.PROPERTY_PROGRESS,
        oldProgress, newProgress);
    for (PropertyChangeListener listener : listeners) {
      listener.propertyChange(event);
    }
    oldProgress = newProgress;
  }

  public void interruptSync() {
    interruptUpdate = true;
  }


}
