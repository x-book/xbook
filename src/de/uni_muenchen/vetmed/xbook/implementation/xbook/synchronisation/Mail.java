package de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation;

import java.io.File;
import java.util.ArrayList;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.activation.MimetypesFileTypeMap;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.sun.mail.smtp.SMTPTransport;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;

/**
 * class to send conflict mails
 *
 * @author j.lamprecht
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 *
 */
public class Mail {

    // project owner
    private final ArrayList<String> recipients;
    private final String projectName;
    // mail relay server
    private final String server;
    // password for login on mail relay server
    private final String password;
//    // username for login on mail relay server
    private final String username;
//    // ossobook server
   private final String sender;
    private Session s;

    /**
     * initializes mail to send without projectname
     *
     * @param recipients the Reciepients where to send the mail to
     */
    public Mail(ArrayList<String> recipients) {
        this(recipients, "");
    }

    /**
     * initialize mail to send
     *
     * @param recipients - project owner of given project
     * @param projectName
     */
    public Mail(ArrayList<String> recipients, String projectName) {
//        server = Configuration.getProperty("mail.server");
        username = XBookConfiguration.getProperty("mail.username");
        password = XBookConfiguration.getProperty("mail.password");
        sender = XBookConfiguration.getProperty("mail.sender");
        server = "mailout.lrz-muenchen.de";
        this.recipients = recipients;
        this.projectName = projectName;

        Properties props = new Properties();
        props.put("mail.smtp.host", server);
//        if (!username.equals("") || !password.equals("")) {
//            props.put("mail.smtp.auth", "true");
//        }
        s = Session.getInstance(props, null);
    }

    public void sendErrorMessage(String errorDescription, File logFile, String bookTypeName, String username) throws MessagingException {

        Message message = new MimeMessage(s);
        InternetAddress from = new InternetAddress(sender);
        message.setFrom(from);
        for (String recipient : recipients) {
            InternetAddress to = new InternetAddress(recipient);
            message.addRecipient(Message.RecipientType.TO, to);
        }
        message.setSubject(Loc.get("ERROR_REPORT_MAIL_TITLE", bookTypeName, username));

        MimeMultipart content = new MimeMultipart();

        MimeBodyPart messageText = new MimeBodyPart();
        messageText.setContent(errorDescription, "text/plain");
        content.addBodyPart(messageText);

        MimeBodyPart file = new MimeBodyPart();


        FileDataSource fds = new FileDataSource(logFile);
        MimetypesFileTypeMap ftm = new MimetypesFileTypeMap();
        ftm.addMimeTypes("application/zip");
        fds.setFileTypeMap(ftm);

        file.setDataHandler(new DataHandler(fds));
        file.setFileName(fds.getName());
        content.addBodyPart(file);
        message.setContent(content, "multipart/mixed");
        send(message);
    }

    /**
     * send conflict mail, append given conflict file
     *
     * @param conflictFile - file that contains occurred conflicts
     * @throws MessagingException
     */
    public void sendConflictMail(File conflictFile) throws MessagingException {
        String warningMessage = Loc.get("Mail.ERROR", projectName);
//		try {


        Message message = new MimeMessage(s);
        InternetAddress from = new InternetAddress(sender);
        message.setFrom(from);
        for (String recipient : recipients) {
            InternetAddress to = new InternetAddress(recipient);
            message.addRecipient(Message.RecipientType.TO, to);
        }
        message.setSubject(Loc.get("Mail.PROJECT", projectName));

        MimeMultipart content = new MimeMultipart();

        MimeBodyPart messageText = new MimeBodyPart();
        messageText.setContent(warningMessage, "text/plain");
        content.addBodyPart(messageText);

        MimeBodyPart file = new MimeBodyPart();
        FileDataSource fds = new FileDataSource(conflictFile);
        file.setDataHandler(new DataHandler(fds));
        file.setFileName(fds.getName());
        content.addBodyPart(file);
        message.setContent(content, "multipart/mixed");
        send(message);

//                


//		Transport transport = s.getTransport("smtp"); 
//		transport.connect();
//		message.saveChanges();
//
//		transport.sendMessage(message, message.getAllRecipients());
//		transport.close();
//		} catch (AddressException ad) {
//			ad.printStackTrace();
//		} catch (MessagingException m) {
//			m.printStackTrace();
//		}
    }

    private void send(Message message) throws MessagingException {



        SMTPTransport transport = ((SMTPTransport) s.getTransport("smtp"));
        transport.setStartTLS(true);
        transport.connect(server,587, username, password);

        message.saveChanges();
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }

    public void sendMessage(String subject, String text) throws MessagingException {
        Message message = new MimeMessage(s);
        InternetAddress from = new InternetAddress(sender);
        message.setFrom(from);
        for (String recipient : recipients) {
            InternetAddress to = new InternetAddress(recipient);
            message.addRecipient(Message.RecipientType.TO, to);
        }
        message.setSubject(subject);

        MimeMultipart content = new MimeMultipart();

        MimeBodyPart messageText = new MimeBodyPart();
        messageText.setContent(text, "text/plain");
        content.addBodyPart(messageText);

//		MimeBodyPart file = new MimeBodyPart();
////		FileDataSource fds = new FileDataSource(conflictFile);
////		file.setDataHandler(new DataHandler(fds));
////		file.setFileName(fds.getName());
//		content.addBodyPart(file);
        message.setContent(content, "multipart/mixed");
        send(message);
    }
}
