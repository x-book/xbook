package de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.AbstractSerialisableFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * defines scheme of a table in a database
 *
 * @author j.lamprecht
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class Table extends Serialisable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String name;
    private ArrayList<Column> columns;
    private ArrayList<String> primaryKey;
    private ArrayList<Index> indices;

    /**
     * defines table object of database
     *
     * @param name
     * @param columns
     * @param primaryKey
     */
    public Table(String name, ArrayList<Column> columns, ArrayList<String> primaryKey, ArrayList<Index> indices) {
        this.name = name;
        this.columns = columns;
        this.primaryKey = primaryKey;
        this.indices = indices;
    }

    public Table(SerialisableInputInterface is) throws IOException {
        name = is.readString();
        int size = is.readInt();
        columns = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            columns.add((Column) is.deserialize());
        }
        size = is.readInt();
        primaryKey = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            primaryKey.add(is.readString());
        }
        indices = new ArrayList<>();
        int indexSize = is.readInt();
        for (int i = 0; i < indexSize; i++) {
            indices.add((Index) is.deserialize());
        }
    }

    public String getName() {
        return name;
    }

    public ArrayList<Column> getColumns() {
        return columns;
    }

    public ArrayList<String> getPrimaryKeys() {
        return primaryKey;
    }

    public ArrayList<Index> getIndices() {
        return indices;
    }

    /**
     * compares primary keys of "this" to primary key of given table
     *
     * @param table
     * @return
     */
    public boolean getPrimaryKeyIdentic(Table table) {
        boolean primaryKeyIdentic = true;
        if (primaryKey.isEmpty()) {
            return false;
        }
        if (primaryKey.size() != table.getPrimaryKeys().size()) {
            return false;
        }
        for (int i = 0; i < primaryKey.size(); i++) {
            if (!primaryKeyIdentic) {
                return false;
            }
            for (int j = 0; j < table.getPrimaryKeys().size(); j++) {
                if (primaryKey.get(i).equalsIgnoreCase(
                        table.getPrimaryKeys().get(j))) {
                    primaryKeyIdentic = true;
                    break;
                }
                if (j == table.getPrimaryKeys().size() - 1) {
                    primaryKeyIdentic = false;
                }
            }
        }
        return primaryKeyIdentic;
    }

    //    @Override
//    public void write(SerializableOutputStream outputStream) throws IOException {
//        outputStream.writeString(name);
//        outputStream.writeInt(columns.size());
//        for (Column column : columns) {
//            outputStream.writeObject(column);
//        }
//        outputStream.writeInt(primaryKey.size());
//        for (String key : primaryKey) {
//            outputStream.writeString(key);
//        }
//        
//    }
//
//    @Override
//    public void read(SerializableInputStream inputStream) throws IOException {
//        name = inputStream.readString();
//        int columnSize = inputStream.readInt();
//        columns = new ArrayList<>();
//        for (int i = 0; i< columnSize;i++ ) {
//            Column column = new Column();
//            inputStream.readObject(column);
//            columns.add(column);
//        }
//        int primaryKeySize = inputStream.readInt();
//        primaryKey = new ArrayList<>();
//        for (int i = 0; i< primaryKeySize;i++ ) {
//            
//            primaryKey.add(inputStream.readString());
//        }
//    }
//
//    @Override
//    public SerializableObject copy() {
//        return new Table();
//    }
    @Override
    protected int getClassID() {
        return AbstractSerialisableFactory.TABLE_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeString(name);
        outputStream.writeInt(columns.size());
        for (Column column : columns) {
            column.serialize(outputStream);
        }
        outputStream.writeInt(primaryKey.size());
        for (String key : primaryKey) {
            outputStream.writeString(key);
        }
    }

    @Override
    public String toString() {
        String string = "Table: " + name + "\n";

        for (Column column : columns) {
            string += column + "\n";
        }
        string += "Keys: \n";
        for (String prim : primaryKey) {
            string += prim + "\n";
        }
        return string;
    }

    public boolean indicesIdentical(Table tableGlobal) {
        boolean primaryKeyIdentic = true;

        if (indices.size() != tableGlobal.getIndices().size()) {
            return false;
        }
        for (int i = 0; i < indices.size(); i++) {
            if (!primaryKeyIdentic) {
                return false;
            }
            for (int j = 0; j < tableGlobal.getIndices().size(); j++) {
                if (indices.get(i).equals(
                        tableGlobal.getIndices().get(j))) {
                    primaryKeyIdentic = true;
                    break;
                }
                if (j == tableGlobal.getIndices().size() - 1) {
                    primaryKeyIdentic = false;
                }
            }
        }
        return primaryKeyIdentic;
    }
}
