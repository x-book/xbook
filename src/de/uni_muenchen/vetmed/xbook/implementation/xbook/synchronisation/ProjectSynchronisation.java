package de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableArrayList;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInt;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableString;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractInputUnitManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractQueryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.SynchronisationProgress.Type;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class ProjectSynchronisation extends SwingWorker<Void, Void> {

    public static final String PROPERTY_PROGRESS = "progress";
    private EntriesSynchronisationProgress oldProgress;
    private AbstractSynchronisationController<?,?> controller;
    private ArrayList<ProjectDataSet> selectedProjects;

    public ProjectSynchronisation(AbstractSynchronisationController controller, ArrayList<ProjectDataSet> selectedProjects) throws NotLoggedInException {
        this.controller = controller;
        this.selectedProjects = selectedProjects;
    }

    @Override
    protected Void doInBackground() {
        int doneCounter = 0;
        int totalCount = selectedProjects.size();
        try {
            for (ProjectDataSet project : selectedProjects) {

                //check if project already exists (is not deleted) and if not initilialize it
                if (!controller.getLocalManager().getProjectManager().projectExists(project)) {
                    initialize(project);
                }
                //get entry with the highest ID for the current databaseNumber
                ArrayList<Serialisable> list = new ArrayList<>();
                list.add(project.getProjectKey());
                list.add(new SerialisableInt(controller.getLocalManager().getDatabaseManager().getDatabaseNumber()));
                SerialisableString tableName = new SerialisableString("");
                list.add(tableName);
                for (IBaseManager sync : controller.getLocalManager().getSyncTables()) {
                    if (sync instanceof BaseEntryManager) {
                        AbstractBaseEntryManager abm = (AbstractBaseEntryManager) sync;
                        tableName.setString(sync.getTableName());
                        //get entry with the highest ID for the current databaseNumber
                        //System.out.println(sync.getTableName());
                        Message response = controller.sendMessage(Commands.REQUEST_ENTRY_WITH_HIGHEST_NUMBER, list);
                        if (response.getResult().wasSuccessful()) {
                            for (Serialisable datae : response.getData()) {
//                                ArrayList<DataColumn> projectData = new ArrayList<>();
                                DataRow projectData = new DataRow(abm.tableName);
                                SerialisableArrayList<Serialisable> dataList = (SerialisableArrayList) datae;
                                for (Serialisable asd : dataList.getData()) {
                                    DataColumn hash = (DataColumn) asd;
                                    if (ColumnHelper.removeDatabaseName(hash.getColumnName()).equals(AbstractInputUnitManager.STATUS.getColumnName())) {
                                        hash.setValue("0");
                                    } else if (ColumnHelper.removeDatabaseName(hash.getColumnName()).equals(AbstractInputUnitManager.DELETED.getColumnName())) {
                                        hash.setValue("Y");
                                    }
                                    projectData.add(hash);
                                }

                                abm.insertData(projectData, true); // Insert record into local database.
                            }
                        } else {
                            System.out.println(response.getResult());
                        }
                    }
                }

                update(Type.UPDATE, null, doneCounter, totalCount);
            }
            controller.getSynchroniser().update(selectedProjects, true);
        } catch (StatementNotExecutedException | IOException | NotConnectedException | NotLoggedInException ex) {
            Logger.getLogger(ProjectSynchronisation.class.getName()).log(Level.SEVERE, null, ex);
            update(SynchronisationProgress.Type.ERROR, Loc.get("ERROR_WHILE_INITIALIZING_PROJECT"));
        }
        return null;
    }

    /**
     * Initialize a project, i.e. create it locally and fetch the data from the
     * global database.
     *
     * @param project the project to initialize to the local database.
     */
    private void initialize(ProjectDataSet project) throws NotLoggedInException, IOException, NotConnectedException, StatementNotExecutedException {
        String projectName = project.getProjectName();
        AbstractQueryManager manager = controller.getLocalManager();
        update(Type.UPDATE, Loc.get("INITIALISING_PROJECT", projectName));

        if (controller.getLocalManager().getProjectManager().projectExists(project, true)) {
            manager.getProjectManager().setActive(project);
        } else {
            Message ms = controller.sendMessage(Commands.REQUEST_PROJECT, project.getProjectKey());
            // Get all data on the project.

            // Anything found?
            if (!ms.getResult().wasSuccessful()) {
                System.out.println(ms.getResult());
                update(Type.WARNING, Loc.get("ERROR_WHILE_INITIALIZING_PROJECT", projectName));
                return;
            }

            // TODO Be Awesome and ciaran maybe also
            ArrayList<Serialisable> data = ms.getData();
            for (Serialisable datae : data) {
//                ArrayList<DataColumn> projectData = new ArrayList<>();
                DataRow projectData = new DataRow(manager.getProjectManager().tableName);
                SerialisableArrayList<Serialisable> list = (SerialisableArrayList) datae;
                for (Serialisable asd : list.getData()) {
                    projectData.add((DataColumn) asd);
                }
                manager.getProjectManager().insertData(projectData, true); // Insert record into local database.
            }

        }

        update(Type.UPDATE, Loc.get("DONE_INITIALISING_PROJECT", projectName));
    }

    /**
     * Used to updateExtention the progress property of the synchronization
     * task.
     *
     * <p>
     * This updates the progress value and dispatches a property change event,
     * with a new instance of {@link SynchronisationProgress} holding the two
     * values specified. </p>
     *
     * <p>
     * If set, uses the old percentual progress, else uses 0. </p>
     *
     * @param type the type of the progress updateExtention.
     * @param message the message to display.
     */
    private void update(SynchronisationProgress.Type type, String message) {
        if (oldProgress != null) {
            update(type, message, oldProgress.getCurrentEntryCount(), oldProgress.getMaxEntryCount());
        } else {
            update(type, message, 0, 0);
        }
    }

    /**
     * Used to updateExtention the progress property of the synchronization
     * task.
     *
     * <p>
     * This updates the progress value and dispatches a property change event,
     * with a new instance of {@link SynchronisationProgress} holding the two
     * values specified. </p>
     *
     * @param type the type of the progress updateExtention.
     * @param message the message to display.
     */
    private void update(SynchronisationProgress.Type type, String message, int currentEntriesCount, int maxEntriesCount) {
        EntriesSynchronisationProgress newProgress = new EntriesSynchronisationProgress(type, message, currentEntriesCount, maxEntriesCount);
        firePropertyChange(PROPERTY_PROGRESS, oldProgress, newProgress);
        oldProgress = newProgress;
    }
}
