package de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation;

/**
 * Utility class used to propagate progress to the rest of the world.
 *
 * <p> This class holds the type of the progress event (as {@link Type}) and the message related to the progress update.
 * </p>
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class SynchronisationProgress {

    /**
     * The type of the update.
     *
     * Updates represent generic events, such as starting this, finishing that, warnings are just that, i.e. if
     * something goes wrong, and the completion event is fired when the whole thing is done. The error is dispatched if
     * anything major goes wrong, e.g. the connection dies. This is equivalent to the completion type, just that it's
     * used if the sync was not successful.
     */
    public enum Type {

        STARTED,
        UPDATE,
        WARNING,
        COMPLETE,
        INTERRUPTED,
        ERROR,
        START_NEW_PROJECT,
        NEXT_STEP,
    }
    /**
     * The progress change type.
     */
    private final Type type;
    /**
     * The message text associated to the progress change.
     */
    private final String message;
    /**
     * The progress in percent (in an interval of [0,1]).
     */
    private final double progress;

    /**
     * Constructor.
     *
     * @param type
     * @param message
     * @param progress
     */
    SynchronisationProgress(Type type, String message, double progress) {
        this.type = type;
        this.message = message;
        this.progress = progress;
    }

    /**
     * The type of the update.
     *
     * @return the type of the update.
     */
    public Type getType() {
        return type;
    }

    /**
     * The message related to the update.
     *
     * @return the message related to the update.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Number representing the percentual progress of the synchronization.
     *
     * @return percentual progress of the synchronization.
     */
    public double getProgress() {
        return progress;
    }
}
