package de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class EntriesSynchronisationProgress extends SynchronisationProgress {

    private final int currentEntryCount;
    private final int maxEntryCount;

    public EntriesSynchronisationProgress(Type type, String message, int currentEntryCount, int maxEntryCount) {
        super(type, message, currentEntryCount / (maxEntryCount * 1.0));

        this.currentEntryCount = currentEntryCount;
        this.maxEntryCount = maxEntryCount;
    }

    public int getMaxEntryCount() {
        return maxEntryCount;
    }

    public int getCurrentEntryCount() {
        return currentEntryCount;
    }

}
