package de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.AbstractSerialisableFactory;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class Index extends Serialisable {

    private final String indexName;

    private final ArrayList<String> columns;

    public Index(String indexName, ArrayList<String> columns) {

        this.indexName = indexName;
        this.columns = columns;
    }

    public Index(SerialisableInputInterface is) throws IOException {
        indexName = is.readString();
        columns = new ArrayList<>();

        int number = is.readInt();
        for (int i = 0; i < number; i++) {
            columns.add(is.readString());
        }
    }

    public ArrayList<String> getColumns() {
        return columns;
    }

    public String getIndexName() {
        return indexName;
    }

    @Override
    protected int getClassID() {
        return AbstractSerialisableFactory.INDEX_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {

    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Index)) {
            return false;
        }
        final Index otherIndex = (Index) obj;
        if (columns.size() != otherIndex.columns.size()) {
            return false;
        }
        for (int i = 0; i < columns.size(); i++) {
            for (int j = 0; j < otherIndex.columns.size(); j++) {
                if (columns.get(i).equalsIgnoreCase(
                        otherIndex.columns.get(j))) {
                    break;
                }
                if (j == otherIndex.columns.size() - 1) {
                    return false;
                }
            }
        }
        return true;
    }
}
