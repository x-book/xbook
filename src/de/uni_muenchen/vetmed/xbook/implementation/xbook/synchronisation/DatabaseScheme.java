package de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.manager.ISynchronisationManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableArrayList;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInt;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractInputUnitManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractProjectManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractQueryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.UpdateManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * synchronizes the database scheme and logs differences
 *
 * @author j.lamprecht
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DatabaseScheme extends SwingWorker<Void, Void> {

    protected static final Log LOGGER = LogFactory.getLog(DatabaseScheme.class);

    ;
    protected AbstractSynchronisationController<?, ?> controller;
    protected AbstractQueryManager localManager;
    protected HashMap<String, Table> definitionTables;
    protected HashMap<String, Table> projectTables;
    protected String databaseName;
    private SynchronisationProgress oldProgress;
    private ArrayList<String> schemaLogs;
    private UpdateType updateType;

    /**
     * instantiates amongst others the log file
     *
     * @param controller
     */
    public DatabaseScheme(AbstractSynchronisationController controller) {
        this.controller = controller;
        try {
            localManager = controller.getLocalManager();

            databaseName = controller.getDbName();
            updateType = UpdateType.ALL;
            setScheme(getLocalDatabaseScheme());

        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(DatabaseScheme.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected Void doInBackground() {
        try {
            update(SynchronisationProgress.Type.STARTED, Loc.get("SYNCHRONIZATION_STARTED"));

            //check for connection to server
            if (!controller.isServerConnected()) {
                update(SynchronisationProgress.Type.ERROR, Loc.get("NO_CONNECTION_TO_LOCAL_OR_GLOBAL_DATABASE_AVAILABLE"));
                return null;
            }
            int dbNumber = XBookConfiguration.getInt(XBookConfiguration.DATABASENUMBER);

            try {
                localManager.disableAutoCommit();
                Savepoint localSavepoint = localManager.getUnderlyingConnection().setSavepoint();

                // Try the actual synchronization.
                try {
                    synchronizeScheme();
                    if (dbNumber == -1) {
                        dbNumber = Integer.parseInt(controller.sendMessageWithString(Commands.REQUEST_DATABASE_NUMBER));

                        System.out.println("dbnumber new:  " + dbNumber);
                    }
                    //get projects with local database number;
                    Message response = controller.sendMessage(Commands.REQUEST_DEFAULT_PROJECTS, new SerialisableInt(dbNumber));
                    if (response.getResult().wasSuccessful()) {
                        for (Serialisable datae : response.getData()) {
//                            ArrayList<DataColumn> projectData = new ArrayList<>();
                            DataRow projectData = new DataRow(localManager.getProjectManager().tableName);
                            SerialisableArrayList<Serialisable> list = (SerialisableArrayList<Serialisable>) datae;
                            for (Serialisable asd : list.getData()) {
                                DataColumn hash = (DataColumn) asd;
                                if (ColumnHelper.removeDatabaseName(hash.getColumnName()).equals(AbstractProjectManager.DELETED.getColumnName())) {
                                    hash.setValue(IStandardColumnTypes.DELETED_LOCAL);
                                } else if (ColumnHelper.removeDatabaseName(hash.getColumnName()).equals(AbstractInputUnitManager.STATUS.getColumnName())) {
                                    hash.setValue("0");
                                }
                                projectData.add(hash);
                            }
                            try {
                                localManager.getProjectManager().insertData(projectData, true); // Insert record into local database.
                            } catch (StatementNotExecutedException ignore) {
                                LOGGER.error("Exception can be ignored");
                            }

                        }
                    }
                    localManager.getUnderlyingConnection().commit();
                    localManager.enableAutoCommit();
                } catch (SQLException ex2) {
                    LOGGER.error(ex2,ex2);
                    localManager.getUnderlyingConnection().rollback(localSavepoint);
                    localManager.enableAutoCommit();
                    update(SynchronisationProgress.Type.ERROR, Loc.get("ERROR_SYNCHRONZIZING_NO_CHANGES_WERE_MADE"));
                    return null;
                }
            } catch (SQLException ex) {
                LOGGER.error(ex,ex);
                localManager.enableAutoCommit();
                update(SynchronisationProgress.Type.ERROR, Loc.get("ERROR_SYNCHRONZIZING_NO_CHANGES_WERE_MADE"));
                return null;
            }
            try {


                localManager.getDatabaseManager().updateVersion(dbNumber, controller.getBookDatabaseVersion());
                if (XBookConfiguration.sharedUserTable) {
                    localManager.getDatabaseManager().updateVersionGeneral(dbNumber,
                            XBookConfiguration.GENERAL_DATABASE_VERSION);
                }
                localManager.updateDatabaseNumber(dbNumber);

            }
            catch (StatementNotExecutedException ex){
                LOGGER.error(ex,ex);
                return null;
            }
            if (schemaLogs != null && schemaLogs.size() > 0) {
                StringBuilder schemaPaths = new StringBuilder();
                for (String path : schemaLogs) {
                    schemaPaths.append(path).append("\n");
                }
                update(SynchronisationProgress.Type.WARNING, Loc.get("DATABASE_SCHEMA_HAS_CHANGED", schemaPaths.toString()));
            }
            // Print changes to database schema and path to conflict file.

            System.out.println("scheme sync done");
            update(SynchronisationProgress.Type.COMPLETE, Loc.get("SYNCHRONIZATION_OF_CODES_COMPLETED"), 100);
            return null;

        } catch (IOException | NotConnectedException | NotLoggedInException | NumberFormatException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            update(SynchronisationProgress.Type.ERROR, ex.getLocalizedMessage());
            return null;
        }
    }

    /**
     * Sets the updateType
     *
     * @param updateType The indicator if the general Table shall be updated
     */
    public void setSyncGeneral(UpdateType updateType) {
        this.updateType = updateType;
    }

    /**
     * Used to updateExtention the progress property of the synchronization
     * task.
     * <p/>
     * <p>
     * This updates the progress value and dispatches a property change event,
     * with a new instance of {@link SynchronisationProgress} holding the two
     * values specified. </p>
     * <p/>
     * <p>
     * If set, uses the old percentual progress, else uses 0. </p>
     *
     * @param type    the type of the progress updateExtention.
     * @param message the message to display.
     */
    private void update(SynchronisationProgress.Type type, String message) {
        if (oldProgress != null) {
            update(type, message, oldProgress.getProgress());
        } else {
            update(type, message, 0);
        }
    }

    /**
     * Used to updateExtention the progress property of the synchronization
     * task.
     * <p/>
     * <p>
     * This updates the progress value and dispatches a property change event,
     * with a new instance of {@link SynchronisationProgress} holding the two
     * values specified. </p>
     *
     * @param type     the type of the progress updateExtention.
     * @param message  the message to display.
     * @param progress the percentual progress of the synchronization.
     */
    private void update(SynchronisationProgress.Type type, String message, double progress) {
        SynchronisationProgress newProgress = new SynchronisationProgress(type, message, progress);
        firePropertyChange(XBookConfiguration.PROPERTY_PROGRESS, oldProgress, newProgress);
        oldProgress = newProgress;
    }

    /**
     * @return absolute path to log file
     */
    public void synchronizeScheme() {

        // Get any changes to the database scheme in the global database and
        // apply them to the local database.
        update(SynchronisationProgress.Type.UPDATE, Loc.get("SYNCHRONIZING_DATABASE_SCHEMA"));

        try {
            ArrayList<Serialisable> globalTables = getGlobalDatabaseScheme();
            setScheme(globalTables);
            ArrayList<Serialisable> localTables = getLocalDatabaseScheme();
            checkDatabaseScheme(globalTables, localTables);

        } catch (StatementNotExecutedException e) {
            LogFactory.getLog(DatabaseScheme.class).error(e, e);
        }

        update(SynchronisationProgress.Type.UPDATE, Loc.get("SYNCHRONIZATION_OF_DATABASE_SCHEMA_COMPLETE"), 99);
    }

    /**
     * gets the global database scheme from the server
     *
     * @return
     */
    private ArrayList<Serialisable> getGlobalDatabaseScheme() {
        try {
            switch (updateType) {
                case ALL:
                    return controller.getObjects(Commands.REQUEST_TABLE_SCHEME_ALL);
                case GENERAL:
                    return controller.getObjects(Commands.REQUEST_TABLE_SCHEME_GENERAL);
                case SPECIFIC:
                default:
                    return controller.getObjects(Commands.REQUEST_TABLE_SCHEME);
            }

        } catch (NotConnectedException | NotLoggedInException | IOException ex) {
            ex.printStackTrace();
            return new ArrayList<>();
        }

    }

    /**
     * gets the local database scheme
     *
     * @return tables with their columns and primary keys
     */
    private ArrayList<Serialisable> getLocalDatabaseScheme()
            throws StatementNotExecutedException {
        ArrayList<Serialisable> tables = new ArrayList<>();
        ArrayList<String> tableNames = localManager.getSchemeManager().getTables();
        tableNames.addAll(localManager.getSchemeManager().getTablesGeneral());

        for (int i = 0; i < tableNames.size(); i++) {
            String tableName = tableNames.get(i);
            ArrayList<Column> columns = localManager.getSchemeManager().getTableDescription(tableName);
            ArrayList<Index> indices = localManager.getSchemeManager().getIndices(tableName);

            tables.add(new Table(tableName, columns, localManager.getSchemeManager().getPrimaryKeys(tableName), indices));
        }
        return tables;
    }

    /**
     * compares the global and local database scheme adapts the local scheme
     * logs the differences
     *
     * @param global
     * @param local
     */
    @SuppressWarnings("unchecked")
    private void checkDatabaseScheme(ArrayList<Serialisable> global, ArrayList<Serialisable> local)
            throws StatementNotExecutedException {
        ArrayList<Table> tableGlobal = (ArrayList<Table>) global.clone();
        ArrayList<Table> tableLocal = (ArrayList<Table>) local.clone();

        int i = 0;

        while (tableLocal.size() > i) {

            String tableName = tableLocal.get(i).getName();
//            if (LOGGER.isInfoEnabled()) {
//                LOGGER.info("synchronisiere Tabelle " + tableName);
//            }
            for (int j = 0; j < tableGlobal.size(); j++) {
                // table already exists
                if (tableName.equals(tableGlobal.get(j).getName())) {
                    // adapt column scheme in local database
                    adaptColumns(tableLocal.get(i),
                            tableGlobal.get(j));

                    // adapt primary keys
                    adaptPrimaryKey(tableLocal.get(i),
                            tableGlobal.get(j));
                    //adapt indices
                    adaptIndices(tableLocal.get(i),
                            tableGlobal.get(j));
                    // next table
                    tableLocal.remove(tableLocal.get(i));
                    tableGlobal.remove(tableGlobal.get(j));

                    // log changes
                    break;
                }
                if (j >= tableGlobal.size() - 1) {
                    i++;
                    update(SynchronisationProgress.Type.UPDATE, XBookConfiguration.PROPERTY_PROGRESS, i * 100.0 / tableGlobal.size());

                }
            }
            //all tables created move on
            if (tableGlobal.isEmpty()) {
                break;
            }
        }
        // log tables that don't exist in global database

        createTables(tableGlobal);
    }

    /**
     * compares primary keys of local and global table, adapts primary key if
     * necessary
     *
     * @param tableLocal
     * @param tableGlobal
     * @return
     */
    private String adaptPrimaryKey(Table tableLocal, Table tableGlobal)
            throws StatementNotExecutedException {
        String logEntry = "";

        String tableName = tableGlobal.getName();
        if (!tableLocal.getPrimaryKeyIdentic(tableGlobal)) {
            ArrayList<String> primaryKeys = tableGlobal.getPrimaryKeys();
            String[] primaryKey = new String[primaryKeys.size()];
            primaryKeys.toArray(primaryKey);
            if (tableLocal.getPrimaryKeys().size() > 0) {
                localManager.getSchemeManager().changePrimaryKey(tableName, primaryKey);
            } else {
                localManager.getSchemeManager().addPrimaryKey(tableName, primaryKey);
            }

            logEntry = Loc.get("DatabaseScheme.PRIMARY_KEY_CHANGED");
            for (String element : primaryKey) {
                logEntry = logEntry + element;
            }
        }
        return logEntry;
    }

    private String adaptIndices(Table tableLocal, Table tableGlobal)
            throws StatementNotExecutedException {
        String logEntry = "";

        String tableName = tableGlobal.getName();
        if (!tableLocal.indicesIdentical(tableGlobal)) {
            ArrayList<Index> indices = tableGlobal.getIndices();


            localManager.getSchemeManager().changeIndices(tableName, indices, tableLocal.getIndices());


            logEntry = Loc.get("DatabaseScheme.PRIMARY_KEY_CHANGED");
            for (Index element : indices) {
                logEntry = logEntry + element;
            }
        }
        return logEntry;
    }

    /**
     * compares every single column of the local table to its global pendant and
     * the other way round adapts local table columns to its namesake if
     * necessary logs differences
     *
     * @param localTable
     * @param globalTable
     * @return
     */
    @SuppressWarnings("unchecked")
    private void adaptColumns(Table localTable, Table globalTable) {
        // Get all existing columns.
        ArrayList<Column> local = (ArrayList<Column>) localTable.getColumns().clone();
        ArrayList<Column> global = (ArrayList<Column>) globalTable.getColumns().clone();

        int i = 0;
        // Check for columns in the global database to copy into the local one.
        outer:
        for (Column g : global) {
            for (Column l : local) {
                if (g.getName().equalsIgnoreCase(l.getName())) {
                    // Found, continue with next.
                    i++;
                    continue outer;
                }
            }
            // Not found, create locally.
            String after = null;
            if (i > 0) {
                after = global.get(i - 1).getName();
            }
            addColumn(g, globalTable.getName(), after);
            i++;
        }

        // Update the local vector.
        local = (ArrayList<Column>) localTable.getColumns().clone();

        // Check for local columns that don't exist in the global one.
        outer:
        for (Column l : local) {
            for (Column g : global) {
                if (l.getName().equalsIgnoreCase(g.getName())) {
                    // Found, continue with next.
                    continue outer;
                }
            }
            // Not found, log.
            try {
                localManager.getSchemeManager().removeColumn(globalTable.getName(), l.getName());
            } catch (StatementNotExecutedException ex) {
                Logger.getLogger(DatabaseScheme.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        try {
            outer:
            for (Column globalColumn : global) {
//                if (LOGGER.isInfoEnabled()) {
//                    LOGGER.info("synchronisiere Spalte "
//                            + globalColumn.getName());
//                }
                // Find the local copy of this column.
                for (Column localColumn : local) {
                    if (localColumn.getName().equalsIgnoreCase(globalColumn.getName())) {
                        // Found, updateExtention the column based on the global one if
                        // it changed (the column description i.e.).
                        if (!localColumn.equals(globalColumn)) {
                            localManager.getSchemeManager().changeColumn(localTable.getName(),
                                    globalColumn.getName(), globalColumn.getType(), globalColumn.getSize(),
                                    globalColumn.getNullable(), globalColumn.getDefaultValue(), globalColumn.getAutoincrement());

                        }
                        continue outer;
                    }
                }
            }
        } catch (StatementNotExecutedException e) {
            e.printStackTrace();
        }
        // Log all differences between the global and local database.

    }

    /**
     * adds a column to a local table
     *
     * @param column
     * @param tableName
     * @return
     */
    private String addColumn(Column column, String tableName, String after) {
        String logEntry = "";
        try {
//            if (LOGGER.isInfoEnabled()) {
//                LOGGER.info("f\u00FCge Spalte hinzu: " + column.getName());
//            }
            localManager.getSchemeManager().addColumn(tableName, column.getName(), column.getType(), column.getSize(), column.getNullable(), column.getDefaultValue(), column.getAutoincrement(), after);

        } catch (StatementNotExecutedException e) {
            e.printStackTrace();
        }
        return (logEntry);
    }

    /**
     * creates non existing table in local database
     *
     * @param tables
     */
    private void createTables(ArrayList<Table> tables) {
        for (int i = 0; i < tables.size(); i++) {

            final Table table = tables.get(i);
            String tableName = table.getName();
            ArrayList<Column> columns = table.getColumns();
            String[] columnNames = new String[columns.size()];
            String[] types = new String[columns.size()];
            String[] sizes = new String[columns.size()];
            String[] nullable = new String[columns.size()];
            String[] defaultValues = new String[columns.size()];
            String[] autoincrement = new String[columns.size()];

            ArrayList<String> primKey = table.getPrimaryKeys();
            String[] primaryKey = new String[table.getPrimaryKeys().size()];
            primKey.toArray(primaryKey);

            for (int j = 0; j < columns.size(); j++) {
                columnNames[j] = columns.get(j).getName();
                types[j] = columns.get(j).getType();
                sizes[j] = columns.get(j).getSize();
                nullable[j] = columns.get(j).getNullable();
                defaultValues[j] = columns.get(j).getDefaultValue();
                autoincrement[j] = columns.get(j).getAutoincrement();
            }
            LOGGER.debug("database scheme table name: " + tableName);
            try {
                localManager.getSchemeManager().createTable(tableName, columnNames, types, sizes,
                        nullable, defaultValues, autoincrement, primaryKey, table.getIndices());

            } catch (StatementNotExecutedException e) {
                LogFactory.getLog(DatabaseScheme.class).error(e, e);
            }
            update(SynchronisationProgress.Type.UPDATE, XBookConfiguration.PROPERTY_PROGRESS, i * 100 / tables.size());
        }
//        try {
//            //new tables lock again
//            if (tables.size() > 0) {
//                localManager.getDatabaseManager().lockDatabase(localManager.getDbName(), localManager.getSchemeManager().getTables());
//
//            }
//        } catch (StatementNotExecutedException ex) {
//            LogFactory.getLog(DatabaseScheme.class).error(ex, ex);
//        } catch (MetaStatementNotExecutedException ex) {
//            Logger.getLogger(DatabaseScheme.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    /**
     * remember global scheme
     *
     * @param tables
     */
    protected void setScheme(ArrayList<Serialisable> tables) {
        String book = databaseName + ".";
        definitionTables = new HashMap<>();
        projectTables = new HashMap<>();
        ArrayList<IBaseManager> list;
        try {
            list = controller.getLocalManager().getSyncTables();
        } catch (NotLoggedInException ex) {
            list = new ArrayList<>();
        }
        for (int i = 0; i < tables.size(); i++) {
            Table table = (Table) tables.get(i);
            boolean found = false;
            for (IBaseManager m : list) {
                if (table.getName().equals(book + m.getTableName())) {
                    projectTables.put(table.getName(), table);
                    found = true;
                    break;
                }
                for (ISynchronisationManager m2 : m.getManagers()) {
                    if (table.getName().equals(book + m2.getTableName())) {
                        projectTables.put(table.getName(), table);
                        found = true;
                        break;
                    }
                }
                if (found) {
                    break;
                }

            }//<------------Alles was nicht ausgeschlossen wurde bis hier wird syncronisiert--------------->
            if (!found && !table.getName().equals(book + "version")
                    && !table.getName().equals(book + "nachrichten")
                    && !table.getName().equals(book + UpdateManager.TABLENAME_UPDATE)
                    && !table.getName().equals(IStandardColumnTypes.DATABASE_NAME_GENERAL + ".version")) {

                definitionTables.put(table.getName(), table);
            }
        }
    }

    public Table getDefinitionTableScheme(String tableName) {
        return definitionTables.get(tableName);
    }

    public Table getProjectTableScheme(String tableName) {
        return projectTables.get(databaseName + "." + tableName);
    }

    /**
     * translate tables of database
     *
     * @return
     */
    public ArrayList<String> getDefinitionTableNames() {
        Set<String> tableSet = definitionTables.keySet();
        ArrayList<String> tables = new ArrayList<>(tableSet);

        return tables;
    }

    /**
     * The type of the updateExtention, whether all tables, only the current or
     * only the general tables shall be updated
     */
    public enum UpdateType {

        ALL, SPECIFIC, GENERAL
    }
}
