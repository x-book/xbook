package de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTable;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInt;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableString;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractQueryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class CodeTablesSynchronisation extends SwingWorker<Void, Void> {
    private static final Log LOGGER = LogFactory.getLog(CodeTablesSynchronisation.class);

    private SynchronisationProgress oldProgress;
    private boolean syncAll;
    protected AbstractSynchronisationController controller;
    private static final Log log = LogFactory.getLog(CodeTablesSynchronisation.class);

    public CodeTablesSynchronisation(AbstractSynchronisationController controller) {
        this(controller, false);
    }

    public CodeTablesSynchronisation(AbstractSynchronisationController controller, boolean syncAll) {

        this.controller = controller;
        this.syncAll = syncAll;
    }

    @Override
    protected Void doInBackground() {
        try {
            update(SynchronisationProgress.Type.STARTED, Loc.get("SYNCHRONIZATION_STARTED"));
            AbstractQueryManager localManager = controller.getLocalManager();
            //check for connection to server
            if (!controller.isServerConnected()) {
                update(SynchronisationProgress.Type.ERROR, Loc.get("NO_CONNECTION_TO_LOCAL_OR_GLOBAL_DATABASE_AVAILABLE"));
                return null;
            }
            try {
                localManager.disableAutoCommit();
                Savepoint localSavepoint = localManager.getUnderlyingConnection().setSavepoint();

                // Try the actual synchronization.
                try {
                    String lastUpdate = controller.sendMessageWithString(Commands.REQUEST_LAST_UPDATE);
                    //set last update only if sync had no problems
                    if (sychronizeCodeTables()) {
                        controller.getLocalManager().getUpdateManager().setLastUpdate(lastUpdate);
                    }
                    localManager.getUnderlyingConnection().commit();

                } catch (SQLException ex2) {
                    log.error(ex2);
                    localManager.getUnderlyingConnection().rollback(localSavepoint);
                    localManager.enableAutoCommit();

                }
            } catch (SQLException ex) {
                log.error(ex.getLocalizedMessage(), ex);
                localManager.enableAutoCommit();
                update(SynchronisationProgress.Type.ERROR, Loc.get("ERROR_SYNCHRONZIZING_NO_CHANGES_WERE_MADE"));
            } finally {
                localManager.enableAutoCommit();
            }
            // Print changes to database schema and path to conflict file.

            update(SynchronisationProgress.Type.COMPLETE, Loc.get("SYNCHRONIZATION_OF_CODES_COMPLETED"), 100);

            controller.codeTablesChanged();

        } catch (Exception ex) {
            log.error(ex.getLocalizedMessage(), ex);
            update(SynchronisationProgress.Type.ERROR, Loc.get("ERROR_SYNCHRONZIZING_NO_CHANGES_WERE_MADE"));
        }
        return null;
    }

    /**
     * Used to updateExtention the progress property of the synchronization
     * task.
     * <p/>
     * <p>
     * This updates the progress value and dispatches a property change event,
     * with a new instance of {@link SynchronisationProgress} holding the two
     * values specified. </p>
     * <p/>
     * <p>
     * If set, uses the old percentual progress, else uses 0. </p>
     *
     * @param type    the type of the progress updateExtention.
     * @param message the message to display.
     */
    private void update(SynchronisationProgress.Type type, String message) {
        if (oldProgress != null) {
            update(type, message, oldProgress.getProgress());
        } else {
            update(type, message, 0);
        }
    }

    /**
     * Used to updateExtention the progress property of the synchronization
     * task.
     * <p/>
     * <p>
     * This updates the progress value and dispatches a property change event,
     * with a new instance of {@link SynchronisationProgress} holding the two
     * values specified. </p>
     *
     * @param type     the type of the progress updateExtention.
     * @param message  the message to display.
     * @param progress the percentual progress of the synchronization.
     */
    private void update(SynchronisationProgress.Type type, String message, double progress) {
        SynchronisationProgress newProgress = new SynchronisationProgress(type, message, progress);
        firePropertyChange(XBookConfiguration.PROPERTY_PROGRESS, oldProgress, newProgress);
        oldProgress = newProgress;
    }

    private boolean sychronizeCodeTables() throws NotLoggedInException, StatementNotExecutedException, IOException, NotConnectedException {
        // Synchronize code tables. This is the same for initialization and
        // synchronization and represents the actual synchronizing of the data.
        update(SynchronisationProgress.Type.UPDATE, Loc.get("SYNCHRONIZING_CODE_TABLES"), 0.5);
        boolean allWentWell = true;
        // Loop through all known tables. This copies all the data from the
        // global database into the local one.
        int doneCounter = 0;
        AbstractQueryManager<?, ?> localManager = controller.getLocalManager();
        ArrayList<String> tableNames = controller.getEntryDataTables();
        int totalCount = tableNames.size();
        if (syncAll) {
            localManager.getDatabaseManager().unlockTables();
            for (String table : tableNames) {
                // remove all date from DB to collect everything again
                try {

                    localManager.getDefinitionManager().truncateDefinition(table);
                } catch (Exception ex) {
                    LOGGER.debug("ex sql code tables but doesnt matter");
                }
            }
        }
        Date d = new Date();
        for (String table : tableNames) {

            if (!synchronizeTable(table)) {
                allWentWell = false;
            }

            Message numberMessage = new Message(Commands.GET_NUMBER_OF_ENTRIES_IN_CODE_TABLES);
            numberMessage.getData().add(new SerialisableString(table));
            if (XBookConfiguration.DISPLAY_SOUTS) {
                LOGGER.debug("number for entries: " + table);
            }
            Message numberResult = controller.sendMessage(numberMessage);
            if (numberResult.wasSuccessfull() && !numberResult.getData().isEmpty()) {
                int numberOfEntriesGlobal = ((SerialisableInt) numberResult.getData().get(0)).getValue();
                int numberOfEntriesLocal = localManager.getDefinitionManager().getNumberOfEntries(table);
                if (numberOfEntriesGlobal != numberOfEntriesLocal) {
                    localManager.getDefinitionManager().truncateDefinition(table);
                    synchronizeTable(table);
                }
            } else {
                if (!numberResult.wasSuccessfull())
                    LOGGER.error("CODE TABLE SYNCHRONISATION did not work for table " + table);
            }

            // Notify world.
            update(SynchronisationProgress.Type.UPDATE, Loc.get("SYNCHRONIZATION_OF_TABLE_COMPLETED", table), 100 * doneCounter++ / totalCount);

        }
        Date d2 = new Date();
        LOGGER.debug("Sync took " + (d2.getTime() - d.getTime()));
        return allWentWell;
    }

    public boolean synchronizeTable(String table) throws NotLoggedInException, NotConnectedException, IOException, StatementNotExecutedException {

        AbstractQueryManager localManager = controller.getLocalManager();
        try {
            ArrayList<Serialisable> data = new ArrayList<>();
            data.add(new SerialisableString(table));
            data.add(new SerialisableString(localManager.getDefinitionManager().getLastSynchronisation(table)));
            Message message = controller.sendMessage(new Message(Commands.REQUEST_TABLE_DATA_NEW, data));
            if (!message.getResult().wasSuccessful()) {
                throw new IOException(message.getResult().getErrorMessage());
            }
            for (Serialisable hash : message.getData()) {
                DataTable dataSet = (DataTable) hash;

                localManager.changeDefinitionsNew(table, dataSet);
            }
            // Clean up: delete rows marked as deleted.
            localManager.getDefinitionManager().deleteDefinitionPermanent(table);
        } catch (StatementNotExecutedException e) {

            update(SynchronisationProgress.Type.WARNING, Loc.get("ERROR_WHILE_SYNCHRONIZING_CODE_TABLE", table));
            return false;
        }
        return true;
    }
}
