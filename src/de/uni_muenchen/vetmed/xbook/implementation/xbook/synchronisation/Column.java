package de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.AbstractSerialisableFactory;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;

import java.io.IOException;


/**
 * defines scheme of a database column
 *
 * @author j.lamprecht
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class Column extends Serialisable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final String name;
    private final String type;
    private final String size;
    private final String defaultValue;
    private final String nullable;
    private final String autoincrement;

    /**
     * definition of different column data
     *
     * @param is
     */
    public Column(SerialisableInputInterface is) throws IOException {
        name = is.readString();
        type = is.readString();
        size = is.readString();
        defaultValue = is.readString();
        nullable = is.readString();
        autoincrement = is.readString();
    }

    public Column(String name, String type, String size, String defaultValue,
                  String nullable, String autoincrement) {
        this.name = name;
        this.type = type;
        this.size = size;
        this.defaultValue = defaultValue;
        this.nullable = nullable;
        this.autoincrement = autoincrement;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getSize() {
        return size;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String getNullable() {
        return nullable;
    }

    public String getAutoincrement() {
        return autoincrement;
    }

    /**
     * compares "this" to given column
     *
     * @param column
     * @return
     */
    public boolean equals(Column column) {
        try {
            boolean equals = true;
            if ((!type.equalsIgnoreCase("INT") && !type.equalsIgnoreCase("INTEGER")) || (!column.type.equalsIgnoreCase("INT") && !column.type.equalsIgnoreCase("INTEGER"))) {
                equals = type.equals(column.getType());
            }


            return name.equalsIgnoreCase(column.getName())
                    && ((equals
                    && ((size == null || size.equals(column.getSize()))
                    && ((defaultValue == null && column.getDefaultValue() == null)
                    || (defaultValue != null && defaultValue.equals(column.getDefaultValue()))))
                    || isEnum(column)
                    || isTextEquals(column)
                    || isNumber(column))
                    && nullable.equals(column.getNullable())
                    && autoincrement.equals(column.getAutoincrement())
            );
        } catch (Exception ex) {
            throw ex;
        }
    }

    private boolean isNumber(Column column) {
        return column.getType().equalsIgnoreCase("FLOAT") && type.equalsIgnoreCase("DOUBLE");

    }

    private boolean isEnum(Column column) {

        //special conditions for enums, since h2 does not have enum types, we use varchar instead
        return column.getType().equalsIgnoreCase("ENUM") && type.equalsIgnoreCase("VARCHAR") && defaultValue.equalsIgnoreCase("N");
    }

    private boolean isTextEquals(Column column) {
        return column.getType().contains("TEXT") && type.equalsIgnoreCase("CLOB");
    }

    //    @Override
//    public void write(SerializableOutputStream outputStream) throws IOException {
//        outputStream.writeString(name);
//        outputStream.writeString(type);
//        outputStream.writeString(size);
//        outputStream.writeString(defaultValue);
//        outputStream.writeString(nullable);
//        outputStream.writeString(autoincrement);
//    }
//
//    @Override
//    public void read(SerializableInputStream inputStream) throws IOException {
//        name = inputStream.readString();
//        type = inputStream.readString();
//        size = inputStream.readString();
//        defaultValue = inputStream.readString();
//        nullable = inputStream.readString();
//        autoincrement = inputStream.readString();
//    }
//
//    @Override
//    public SerializableObject copy() {
//        return new Column();
//    }
    @Override
    protected int getClassID() {
        return AbstractSerialisableFactory.COLUMN_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeString(name);
        outputStream.writeString(type);
        outputStream.writeString(size);
        outputStream.writeString(defaultValue);
        outputStream.writeString(nullable);
        outputStream.writeString(autoincrement);
    }

    @Override
    public String toString() {
        String string = "Name: " + name + ", Type: " + type + ", Size: " + size + ", default: " + defaultValue + ", nullable: " + nullable
                + ", Autoinc: " + autoincrement;

        return string;
    }
}
