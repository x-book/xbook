package de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation;

import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.exception.NotConnectedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.network.Message;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 03.08.2017.
 */
public class PlannedSynchronisation implements PropertyChangeListener {
    private TimerTask task;
    private Timer timer;

    private File f;
    private FileChannel channel;
    private FileLock lock;
    private AbstractSynchronisationController<?, ?> controller;
    private Synchroniser synchroniser;

    public PlannedSynchronisation(AbstractSynchronisationController<?, ?> controller, Synchroniser synchroniser) {
        this.controller = controller;
        this.synchroniser = synchroniser;
        synchroniser.addPropertyChangeListener(this);
        timer = new Timer();
        loadSettings();
    }

    private void loadSettings() {
        boolean timed_synchronisation = XBookConfiguration.getBookBoolean(XBookConfiguration.PLANNED_SYNC);
        if (timed_synchronisation) {
            setTimer();
        }
    }

    public void setTimer() {
        // load settings
        int hour = XBookConfiguration.getBookInt(XBookConfiguration.PLANNED_SYNC_HOUR, 0);
        int minute = XBookConfiguration.getBookInt(XBookConfiguration.PLANNED_SYNC_MINUTE, 0);
        final boolean synchronizeAllProjects = XBookConfiguration.getBookBoolean(XBookConfiguration.PLANNED_SYNC_SYNC_ALL);

        timer.cancel();
        Calendar date = Calendar.getInstance();

        date.set(Calendar.HOUR_OF_DAY, hour);
        date.set(Calendar.MINUTE, minute);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        Calendar now = Calendar.getInstance();
        if (now.after(date)) {
            date.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH) + 1);
        }
        task = new TimerTask() {
            @Override
            public void run() {
                //TODO possibly check if connection is still there?
                //TODO check what happens if version is updated in the mean time?
                System.out.println("Timer Start");
                try {
                    check();
                    ArrayList<ProjectDataSet> projectDataSets = new UniqueArrayList<>();
                    try {
                        projectDataSets.addAll(controller.getProjects());
                        if (synchronizeAllProjects) {
                            Message messages = controller.sendMessage(Commands.REQUEST_PROJECT_LIST);
                            if (messages.getResult().wasSuccessful()) {
                                for (Serialisable serialisable : messages.getData()) {
                                    ProjectDataSet project = (ProjectDataSet) serialisable;
                                    if (!project.isDeleted()) {
                                        projectDataSets.add(project);
                                    }
                                }
                            }

                            System.out.println("Timer start project sync");
                            ProjectSynchronisation projectSynchronisation = new ProjectSynchronisation(controller, projectDataSets);
                            projectSynchronisation.addPropertyChangeListener(PlannedSynchronisation.this);
                            projectSynchronisation.execute();
                        } else {

                            synchroniser.update(projectDataSets, true);
                        }
                    } catch (StatementNotExecutedException e) {
                        e.printStackTrace();
                    }


                } catch (IOException | NotConnectedException | NotLoggedInException e) {
                    e.printStackTrace();
                }
            }
        };
        timer = new Timer();
        timer.schedule(
                task,
                date.getTime(),
                1000 * 60 * 60 * 24 //* 7  every day
        );
        System.out.println("timer scheduled for : " + date.getTime());
    }

    public void stopTimer() {
        timer.cancel();
    }

    public void check() {
        try {
            f = new File("sync.lock");
            // Check if the lock exist
            if (f.exists()) {
                // if exist try to delete it
                f.delete();
            }
            // Try to get the lock
            channel = new RandomAccessFile(f, "rw").getChannel();
            lock = channel.tryLock();
            if (lock == null) {
                // File is lock by other application

            }
        } catch (IOException ex) {

        }
    }

    public void unlockFile() {
        // release and delete file lock
        try {
            if (lock != null) {
                lock.release();
                channel.close();
                f.delete();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (!evt.getPropertyName().equals(
                XBookConfiguration.PROPERTY_PROGRESS) || !(evt.getNewValue() instanceof SynchronisationProgress)) {
            return;
        }
        final EntriesSynchronisationProgress progress = (EntriesSynchronisationProgress) evt.getNewValue();
        switch (progress.getType()) {
            case STARTED:
                System.out.println("Sync started");
                break;
            case WARNING:

                break;
            case ERROR:
            case COMPLETE:
            case INTERRUPTED:
                unlockFile();
                System.out.println("Sync ended");
                break;
            default:

                break;
        }
    }
}
