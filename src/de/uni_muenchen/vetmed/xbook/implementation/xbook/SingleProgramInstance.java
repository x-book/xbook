package de.uni_muenchen.vetmed.xbook.implementation.xbook;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;

import javax.net.ServerSocketFactory;
import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.uni_muenchen.vetmed.xbook.api.Loc;

/**
 * Class used to make sure there's only one instance of the program using it at
 * a time.
 *
 * @author fnuecke
 */
public final class SingleProgramInstance {

    /**
     * Logger to log errors (e.g. if the program is already running).
     */
    private static final Log log = LogFactory.getLog(SingleProgramInstance.class);
    /**
     * Local socket used to block a port for the program.
     */
    private static ServerSocket serverSocket;

    /**
     * No need for instances of this class.
     */
    private SingleProgramInstance() {
    }

    /**
     * This is a convenience function for calling {@link #check(int)}, taking
     * care of the integer parsing of the given string.
     *
     * <p>
     * If the parsing fails, the program exits with code 2.
     * </p>
     *
     * @param lockPort the port to use for locking this program.
     */
    public static void check(String lockPort) {
        try {
            check(Integer.parseInt(lockPort));
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, Loc.get("XBOOK_ALREADY_RUNNING_MESSAGE"), Loc.get("XBOOK_ALREADY_RUNNING"), JOptionPane.ERROR_MESSAGE);
            System.exit(2);
        }
    }

    /**
     * Checks if a program with the same locking port is already running.
     *
     * <p>
     * If not, a serversocket on the given port will be installed, which does
     * not accept connections, but just blocks the port.
     * </p>
     *
     * <p>
     * If yes, the program will exit with code 1.
     * </p>
     *
     * @param lockPort the port to use for locking this program.
     */
    private static void check(int lockPort) {
        synchronized (SingleProgramInstance.class) {
            if (serverSocket != null) {
                return;
            }
            try {
                serverSocket = ServerSocketFactory.getDefault().
                        createServerSocket(lockPort, 0, InetAddress.getByAddress(new byte[]{127, 0, 0, 1}));
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, Loc.get("XBOOK_ALREADY_RUNNING_MESSAGE"), Loc.get("XBOOK_ALREADY_RUNNING"), JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            }
        }
    }
}
