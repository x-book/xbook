package de.uni_muenchen.vetmed.xbook.implementation;

/**
 * Global variables to hide functionalities for Book-specific features.
 * <p>
 * Please note that it does not make any sense to activate these features without the feature being implemented.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@cesonia.io>
 */
public class FeatureConfiguration {

    /**
     * Returns the status if the functionalities for a plausibility check should be loaded.
     *
     * @return The status if the functionalities for a plausibility check should be loaded.
     */
    public boolean isPlausibilityCheckAvailable() {
        return false;
    }

}
