package de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.project;

import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.InputElement;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawTwoDatesChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputTwoDatesChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputYesNoBox;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputTextField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectEdit;

import java.util.ArrayList;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DBProjectEdit extends AbstractProjectEdit {

    public DBProjectEdit(ProjectDataSet project) {
        super(project);
    }

    public DBProjectEdit() {

    }

    @Override
    protected ArrayList<AbstractInputElement> addCustomContentBelow() {
        ArrayList<AbstractInputElement> list = new ArrayList<>();

        InputYesNoBox i1 = new InputYesNoBox(null);
        list.add(i1);

        InputTextField i2 = new InputTextField(null);
        list.add(i2);

        InputTwoDatesChooser i3 = new InputTwoDatesChooser(null, null);
        list.add(i3);

        return list;
    }

}
