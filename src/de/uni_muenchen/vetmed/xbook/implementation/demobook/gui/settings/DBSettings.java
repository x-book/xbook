package de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.settings;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.AbstractSettings;

/**
 * The setting panel of OssoBook. Extends the basic elements of xBook.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class DBSettings extends AbstractSettings {

    @Override
    protected void addCustomContent() {
        setting.addNavigationElement(Loc.get("INPUT_FIELDS"), new DBSettingInputFieldVisibility(mainFrame));
    }
}
