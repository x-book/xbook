package de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.content.entry;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.demobook.IDBInputUnitManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.Unit;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.InputElement;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawTextField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawTwoDatesChooser;
import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractInputUnitManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputTwoDatesChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.numbers.*;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputFileChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputFileNameChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputTextArea;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputTextField;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.ArrayList;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DBEntry extends AbstractEntry {

    private ArrayList<AbstractInputElement> section1;
    private ArrayList<AbstractInputElement> section2;
    private ArrayList<AbstractInputElement> section3;

    public DBEntry(GeneralInputMaskMode m) {
        super(m);
    }

    @Override
    public String getTableName() {
        return AbstractInputUnitManager.TABLENAME_INPUT_UNIT;
    }

    @Override
    public void composeSections() {
        section1 = new ArrayList<>();

        InputElement i = new InputElement("TextField new");
        i.setRawContent(new RawTextField(IDBInputUnitManager.TEXT_FIELD));

        section1.add(new InputTextField(IDBInputUnitManager.TEXT_FIELD));

        section1.add(new InputTextArea(IDBInputUnitManager.TEXT_AREA));
        ArrayList<FileNameExtensionFilter> filter = new ArrayList<>();
        filter.add(new FileNameExtensionFilter(Loc.get("IMAGES") + " (jpg, jpeg, png, gif)", "jpg", "jpeg", "gif",
                "png"));
        section1.add(new InputFileNameChooser(IDBInputUnitManager.FILE_NAME_CHOOSER, filter));

        addSection(Loc.get("TEXT_FIELDS"), section1);

        section2 = new ArrayList<>();
        section2.add(new InputIntegerField(IDBInputUnitManager.INTEGER_FIELD));
        section2.add(new InputFloatField(IDBInputUnitManager.FLOAT_FIELD));
        section2.add(new InputIntegerSpinner(IDBInputUnitManager.INTEGER_SPINNER));
        ArrayList<Unit> units = new ArrayList<>();
        units.add(new Unit("mg", 1));
        units.add(new Unit("g", 1000));
        units.add(new Unit("kg", 100000));
        section2.add(new InputIntegerUnitField(IDBInputUnitManager.INTEGER_UNIT_FIELD, units));
        section2.add(new InputLabeledFloatField(IDBInputUnitManager.LABELED_FLOAT_FIELD, "km"));
        section2.add(new InputLabeledIntegerField(IDBInputUnitManager.LABELED_INTEGER_FIELD, "cm"));
        section2.add(new InputMinMaxFloatField(IDBInputUnitManager.MINMAX_FLOAT_FIELD_MIN,
                IDBInputUnitManager.MINMAX_FLOAT_FIELD_MAX, Loc.get("LABEL_MINMAX_FLOAT_TITLE"), "cm"));
        section2.add(new InputNumberAsStringField(IDBInputUnitManager.NUMBER_AS_STRING_FIELD));
        section2.add(new InputNumberAsStringUnitField(IDBInputUnitManager.NUMBER_AS_STRING_UNIT_FIELD, units));

        addSection(Loc.get("NUMERIC_FIELDS"), section2);

        //other
        section3 = new ArrayList<>();
        FileNameExtensionFilter PDFfilter = new FileNameExtensionFilter(Loc.get("PDF") + " (pdf)", "pdf", "jpg");
        section3.add(new InputFileChooser(IDBInputUnitManager.FILE_DATA_FIELD, IDBInputUnitManager.FILE_NAME_FIELD,
                PDFfilter));
        section3.add(new InputTwoDatesChooser(IDBInputUnitManager.TWO_DATES_FIELD_1,
                IDBInputUnitManager.TWO_DATES_FIELD_2, RawTwoDatesChooser.LabelType.FROM_TO));

        addSection(Loc.get("OTHER"), section3);

    }

}
