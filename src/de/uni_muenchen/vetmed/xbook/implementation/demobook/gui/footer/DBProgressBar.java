package de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.footer;

import de.uni_muenchen.vetmed.xbook.api.gui.footer.AbstractProgressBar;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.ImagesDB;

import javax.swing.Icon;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class DBProgressBar extends AbstractProgressBar {

	@Override
	protected Icon getBorderElement() {
		return ImagesDB.FOOTER_PROGRESS_ICON_BIG;
	}

	@Override
	protected Icon getInnerBarElement() {
		return ImagesDB.FOOTER_PROGRESS_ICON_SMALL;
	}

	@Override
	protected Icon getInnerDisabledBarElement() {
		return ImagesDB.FOOTER_PROGRESS_ICON_SMALL_GRAY;
	}
}
