package de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.project;

import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectOverview;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DBProjectOverview extends AbstractProjectOverview {

    @Override
    protected void addCustomRows(HashMap<Key, ArrayList<CustomRow>> projectRows) {
        addEntriesCustomRow(projectRows);
        addSharedWithCustomRow(projectRows);
    }

    @Override
    protected void addCustomRowsAfterProjectName(HashMap<Key, ArrayList<CustomRow>> projectRows) {

    }

    @Override
    public ArrayList<CustomRow> getCustomRowsAfterProjectName(ProjectDataSet thisProject, int index) {
        return null;
    }

    @Override
    public ArrayList<CustomRow> getCustomRows() {
        ArrayList<CustomRow> rows = new ArrayList<>();
//        rows.add(addSharedWithCustomRow(thisProject, index));
        rows.add(getEntriesCustomRow());
        return rows;
    }
}
