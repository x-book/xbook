package de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.settings;

import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.AbstractSettingInputFieldVisibility;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class DBSettingInputFieldVisibility extends AbstractSettingInputFieldVisibility {

    public DBSettingInputFieldVisibility(IMainFrame mainFrame) {
        super(mainFrame);
    }

}
