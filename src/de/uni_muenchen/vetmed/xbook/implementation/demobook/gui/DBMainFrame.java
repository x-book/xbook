package de.uni_muenchen.vetmed.xbook.implementation.demobook.gui;

import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.gui.AbstractSplashScreen;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.AbstractProgressBar;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.ImagesDB;
import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import de.uni_muenchen.vetmed.xbook.implementation.demobook.controller.DBController;
import de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.content.entry.DBEntry;
import de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.footer.DBProgressBar;
import de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.navigation.DBNavigation;
import de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.project.DBProjectEdit;
import de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.project.DBProjectOverview;
import de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.settings.DBSettings;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectEdit;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectOverview;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.navigation.AbstractNavigation;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DBMainFrame extends AbstractMainFrame<DBController> {

    /**
     * @param controller
     */
    public DBMainFrame(DBController controller) {
        super(controller);

    }

    @Override
    protected void setFavicon() {
        setIconImages(Arrays.asList(
                ImagesDB.FAVICON_16.getImage(),
                ImagesDB.FAVICON_24.getImage(),
                ImagesDB.FAVICON_32.getImage(),
                ImagesDB.FAVICON_48.getImage(),
                ImagesDB.FAVICON_64.getImage(),
                ImagesDB.FAVICON_96.getImage(),
                ImagesDB.FAVICON_128.getImage()));
    }

    @Override
    public AbstractSplashScreen getSplashScreen() {
        return new DBSplashScreen(true);
    }

    @Override
    protected AbstractContent getSettingsScreen() {
        return new DBSettings();
    }

    @Override
    public AbstractEntry getEntryInputScreen(GeneralInputMaskMode m) {
        if (entry == null) {
            entry = new DBEntry(m);
        }
        return entry;
    }

    @Override
    public AbstractProjectOverview getProjectOverviewScreen() {
        return new DBProjectOverview();
    }

    @Override
    public AbstractProjectEdit getProjectEditScreen(ProjectDataSet project) {
        return new DBProjectEdit(project);
    }

    @Override
    public AbstractProjectEdit getNewProjectScreen() {
        return new DBProjectEdit();
    }

    @Override
    public AbstractContent getImportScreen() {
        return null;
    }

    @Override
    public AbstractNavigation createNavigation() {
        return new DBNavigation(this);
    }

    @Override
    public AbstractProgressBar getProgressBar() {
        return new DBProgressBar();
    }

    @Override
    public void displayEntryForManager(AbstractBaseEntryManager entryManager, EntryDataSet data, boolean isEditMode) {
        // not in use
    }

    @Override
    public AbstractEntry getEntryForManager(AbstractBaseEntryManager entryManager) {
        return null;
        // not in use
    }

    @Override
    public ArrayList<Icon> getWorkingIcons() {//TODO own

        ArrayList<Icon> icons = new ArrayList<>();
        icons.add(ImagesDB.FOOTER_LOADING_ICON_01);
        icons.add(ImagesDB.FOOTER_LOADING_ICON_02);
        icons.add(ImagesDB.FOOTER_LOADING_ICON_03);
        icons.add(ImagesDB.FOOTER_LOADING_ICON_04);
        icons.add(ImagesDB.FOOTER_LOADING_ICON_05);
        icons.add(ImagesDB.FOOTER_LOADING_ICON_06);
        icons.add(ImagesDB.FOOTER_LOADING_ICON_07);
        icons.add(ImagesDB.FOOTER_LOADING_ICON_08);
        icons.add(ImagesDB.FOOTER_LOADING_ICON_09);
        icons.add(ImagesDB.FOOTER_LOADING_ICON_10);
        icons.add(ImagesDB.FOOTER_LOADING_ICON_11);
        icons.add(ImagesDB.FOOTER_LOADING_ICON_12);
        return icons;
    }
}
