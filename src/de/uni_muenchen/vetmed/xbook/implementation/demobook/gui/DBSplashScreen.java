package de.uni_muenchen.vetmed.xbook.implementation.demobook.gui;

import de.uni_muenchen.vetmed.xbook.api.gui.AbstractSplashScreen;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.ImagesDB;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DBSplashScreen extends AbstractSplashScreen {

	public DBSplashScreen(boolean manualClose) {
		super(manualClose, ImagesDB.SPLASH_BACKGROUND);
	}

	@Override
	protected void setFavicon() {
		frame.setIconImages(Arrays.asList(
				ImagesDB.FAVICON_16.getImage(),
				ImagesDB.FAVICON_24.getImage(),
				ImagesDB.FAVICON_32.getImage(),
				ImagesDB.FAVICON_48.getImage(),
				ImagesDB.FAVICON_64.getImage(),
				ImagesDB.FAVICON_96.getImage(),
				ImagesDB.FAVICON_128.getImage()));
	}

	@Override
	protected String getApplicationVersion() {
		String stringVersion = XBookConfiguration.getProgramVersion();
		if (!(XBookConfiguration.STATUS).equals("")) {
			stringVersion += " " + XBookConfiguration.STATUS;
		}
		return stringVersion;
	}

	@Override
	protected ArrayList<Person> get1stLevelDevelopers() {
		ArrayList<Person> persons = new ArrayList<>();
		persons.add(new Person("Threepwood", "Guybrush", 10));
		return persons;
	}

	@Override
	protected ArrayList<Person> get2ndLevelDevelopers() {
		ArrayList<Person> persons = new ArrayList<>();
		return persons;
	}

	@Override
	protected ArrayList<Person> get1stLevelContributors() {
		ArrayList<Person> persons = new ArrayList<>();
		return persons;
	}

	@Override
	protected ArrayList<Person> get2ndLevelContributors() {
		ArrayList<Person> persons = new ArrayList<>();
		return persons;
	}

	@Override
	protected ArrayList<Person> getTranslators() {
		ArrayList<Person> persons = new ArrayList<>();
		return persons;
	}

	@Override
	protected ArrayList<Person> getInstitutions() {
		return null;
	}

	@Override
	protected String getURL() {
		return "http://xbook.vetmed.uni-muenchen.de/";
	}

	@Override
	protected String getBookName() {
		return "DemoBook";
	}

	@Override
	protected ArrayList<String> getDevelopmentPlaces() {
		ArrayList<String> places = new ArrayList<>();
		places.add("Melee Island");
		return places;
	}
}
