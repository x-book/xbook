package de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.navigation;

import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.ImagesDB;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.navigation.AbstractNavigation;

import javax.swing.*;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DBNavigation extends AbstractNavigation {

    /**
     * Constructor
     * <p/>
     * Sets the basic layout of the navigation bar.
     *
     * @param mainFrame
     */
    public DBNavigation(IMainFrame mainFrame) {
        super(mainFrame);
    }

    @Override
    protected Icon getApplicationLogo() {
        return ImagesDB.NAVIGATION_APPLICATION_LOGO_DEMOBOOK;
    }

    @Override
    protected String getBookSpecificIssueTrackerURL() {
        return null;
    }
}
