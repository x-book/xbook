package de.uni_muenchen.vetmed.xbook.implementation.demobook;

import de.uni_muenchen.vetmed.xbook.api.Loc;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DBLoc extends Loc {
    private static final String DB_BUNDLE_NAME = "de.uni_muenchen.vetmed.xbook.implementation.demobook.DB_localisation";
    private static ResourceBundle DEMOBOOK_BUNDLE = null;

    public static void init() {
        new DBLoc();
    }

    @Override
    protected String translate(String key) {
        synchronized (DB_BUNDLE_NAME) {
            // Check if the current resource bundle is valid (i.e. it is set and the default locale has not changed since it was created).
            if (DEMOBOOK_BUNDLE == null || !DEMOBOOK_BUNDLE.getLocale().getLanguage().equals(Locale.getDefault().getLanguage())) {
                DEMOBOOK_BUNDLE = Loc.Utf8ResourceBundle.getBundle(DB_BUNDLE_NAME, Locale.getDefault());
                DEMOBOOK_BUNDLE.getLocale();
            }
            try {
                return DEMOBOOK_BUNDLE.getString(key);
            } catch (MissingResourceException e) {
                return super.translate(key);
            }
        }
    }
}
