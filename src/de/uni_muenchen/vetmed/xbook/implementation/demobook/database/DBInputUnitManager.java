package de.uni_muenchen.vetmed.xbook.implementation.demobook.database;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.demobook.IDBInputUnitManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractInputUnitManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractSynchronisationManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.LanguageManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.UserManager;

import java.sql.Connection;
import java.util.ArrayList;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DBInputUnitManager extends AbstractInputUnitManager implements IDBInputUnitManager {
    static {
        FILE_DATA_FIELD.setMaxInputLength(1000000);
    }
    public DBInputUnitManager(int dataBaseNumber, Connection connection, String databaseName, UserManager userManager, LanguageManager languageManager, AbstractSynchronisationManager... manager) {
        super(Loc.get("INPUT_UNIT"), dataBaseNumber, connection, databaseName, userManager, languageManager, manager);
        dataColumns.add(TEXT_FIELD);
        dataColumns.add(TEXT_AREA);
        dataColumns.add(INTEGER_FIELD);
        dataColumns.add(FILE_NAME_CHOOSER);
        dataColumns.add(FLOAT_FIELD);
        dataColumns.add(INTEGER_SPINNER);
        dataColumns.add(INTEGER_UNIT_FIELD);
        dataColumns.add(LABELED_FLOAT_FIELD);
        dataColumns.add(LABELED_INTEGER_FIELD);
        dataColumns.add(MINMAX_FLOAT_FIELD_MIN);
        dataColumns.add(MINMAX_FLOAT_FIELD_MAX);
        dataColumns.add(NUMBER_AS_STRING_FIELD);
        dataColumns.add(NUMBER_AS_STRING_UNIT_FIELD);
        dataColumns.add(FILE_DATA_FIELD);
        dataColumns.add(FILE_NAME_FIELD);
    }

    @Override
    public String toString(Key projectKey, Key entryKey, boolean html) throws StatementNotExecutedException {
        return null;
    }

    @Override
    public ArrayList<ColumnType> getConflictDisplayColumns() {
        return null;
    }
}
