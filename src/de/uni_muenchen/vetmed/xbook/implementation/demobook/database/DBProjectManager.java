package de.uni_muenchen.vetmed.xbook.implementation.demobook.database;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractExtendedProjectEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractProjectManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.UserManager;

import java.sql.Connection;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DBProjectManager extends AbstractProjectManager {

    public DBProjectManager(Connection con, UserManager user, int databaseNumber,
                            String databaseName, AbstractExtendedProjectEntryManager... manager) {
        super(TABLENAME_PROJECT, Loc.get("PROJECT"), databaseNumber, con, databaseName, user, manager);
    }

    @Override
    public String toString(DataSetOld dataSet) throws StatementNotExecutedException {
        return null;
    }
}
