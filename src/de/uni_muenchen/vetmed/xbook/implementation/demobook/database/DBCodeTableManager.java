package de.uni_muenchen.vetmed.xbook.implementation.demobook.database;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractCodeTableManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.LanguageManager;

import java.sql.Connection;

public class DBCodeTableManager extends AbstractCodeTableManager {
    public DBCodeTableManager(LanguageManager languageManager, Connection conn, String dbName) {
        super(languageManager,conn,dbName);
    }
}
