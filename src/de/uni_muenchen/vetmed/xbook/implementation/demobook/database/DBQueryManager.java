package de.uni_muenchen.vetmed.xbook.implementation.demobook.database;

import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.GroupManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractQueryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.ProjectRightManager;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DBQueryManager extends AbstractQueryManager<DBProjectManager,DBInputUnitManager> {

    private final ProjectRightManager prManager;
    private final GroupManager groupManager;


    public DBQueryManager(String name, String password, String databaseName) throws NotLoggedInException {
        super(name, password, databaseName);

        int dataBaseNumber = -1;
        try {
            dataBaseNumber = databaseManager.getDatabaseNumber();
        } catch (Exception ex) {
            //not initialized yet but no problem ;)
        }
        if (dataBaseNumber != XBookConfiguration.getInt(XBookConfiguration.DATABASENUMBER)) {
            dataBaseNumber = -1;
        }
        prManager = new ProjectRightManager(userManager, conn, dbName);
        groupManager = new GroupManager(userManager, conn, dbName);
        codeTableManager = new DBCodeTableManager(languageManager, conn, dbName);

        projectManager = new DBProjectManager(conn, userManager, dataBaseNumber, dbName);

        inputUnitManager = new DBInputUnitManager(dataBaseNumber, conn, dbName, userManager, languageManager);

        syncTables.add(inputUnitManager);
        syncTables.add(projectManager);
    }

    @Override
    public AbstractQueryManager createDBUpdateLocalManager() throws StatementNotExecutedException, NotLoggedInException {
        return new DBQueryManager("root", "", dbName);
    }

    @Override
    public AbstractQueryManager createNewManager(String userName, String password) throws NotLoggedInException, StatementNotExecutedException {
        return new DBQueryManager(name, password, dbName);
    }

    @Override
    public ProjectRightManager getProjectRightManager() {
        return prManager;
    }

    @Override
    public GroupManager getGroupManager() {
        return groupManager;
    }
}
