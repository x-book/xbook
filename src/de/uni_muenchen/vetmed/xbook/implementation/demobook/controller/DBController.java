package de.uni_muenchen.vetmed.xbook.implementation.demobook.controller;

import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.helper.DatabaseType;
import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import de.uni_muenchen.vetmed.xbook.api.network.Commands;
import de.uni_muenchen.vetmed.xbook.api.plugin.PluginInformation;
import de.uni_muenchen.vetmed.xbook.implementation.demobook.DBExport;
import de.uni_muenchen.vetmed.xbook.implementation.demobook.database.DBQueryManager;
import de.uni_muenchen.vetmed.xbook.implementation.demobook.gui.DBMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractInputUnitManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.synchronisation.DatabaseScheme;
import org.h2.engine.Database;

import java.util.ArrayList;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DBController extends AbstractSynchronisationController<DBQueryManager,DBExport> {

    public DBController(String databaseName) {
        super(new DBExport(), "1.01");
        this.databaseName = databaseName;
        onlyLocal = false;
        mainFrame = new DBMainFrame(this);
    }

    @Override
    protected DBQueryManager createLocalManager(String name, String password) throws NotLoggedInException {
        return new DBQueryManager(name, password, databaseName);
    }

    @Override
    protected ArrayList<String> getAccountableMail() {
        return null;
    }

    @Override
    protected Commands getType() {
        return Commands.DEMOBOOK;
    }

    @Override
    public String getBookName() {
        return "DemoBook";
    }

    @Override
    public ArrayList<ColumnType> getImportColumnLabels() throws StatementNotExecutedException, NotLoggedInException {
        return null;
    }

    @Override
    public AbstractEntry getEntryForManager(GeneralInputMaskMode m, BaseEntryManager manager) {
        if (manager instanceof AbstractInputUnitManager) {
            return mainFrame.getEntryInputScreen(m);
        }
        return null;
    }

    @Override
    protected boolean isPluginValidForBook(PluginInformation.Book book) {
        return book == PluginInformation.Book.ALL || book == PluginInformation.Book.DEMOBOOK;
    }

    @Override
    public DatabaseType getDatabaseType() {
        return DatabaseType.MYSQL;
    }

}
