package de.uni_muenchen.vetmed.xbook.implementation;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;

/**
 * Global options for OssoBook.
 *
 * @author ali
 * @author fnuecke
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class XBookConfiguration extends AbstractConfiguration {

  private static DatabaseMode databaseMode;
  public static final String PLANNED_SYNC = "planned_sync";
  public static final String PLANNED_SYNC_HOUR = "planned_sync_hour";
  public static final String PLANNED_SYNC_MINUTE = "planned_sync_minute";
  public static final String PLANNED_SYNC_SYNC_ALL = "planned_sync_sync_all";
  public static final boolean DISPLAY_NETWORK_INFORMATION = false;
  public static Path TMP_DOWNLOAD;

  static {
    try {
      TMP_DOWNLOAD = Files.createTempDirectory("xbook");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  protected static XBookConfiguration instance;

  static {
    /**
     * **********
     */
//    MODE = Mode.DEVELOPMENT;
    /**
     * ******************
     */
    GENERAL_DATABASE_VERSION = "4.01";
    STATUS =
        MODE == Mode.RELEASE || MODE == Mode.RELEASE_EB ? "" : (MODE == Mode.TEST ? "TEST" : "DEV");

    switch (MODE) {
      case TEST:
        PROGRAM_VERSION = "18.1.0";
        break;
      case DEVELOPMENT:
        PROGRAM_VERSION = "5.08.00";
        break;
      case RELEASE:
        PROGRAM_VERSION = "5.06.02";
        break;
      default:


    }
    DISPLAY_SOUTS = false;
    LOG_SYNCHRONISATION_INFORMATION =  false;
    IMAGES_DIR = DIR_PREFIX + "assets/images.zip";
    USE_WEBAPP = MODE != Mode.RELEASE;
    serverLocation = "https://xbook.vetmed.uni-muenchen.de";

  }

  public static String EXTENSION = MODE == Mode.RELEASE ? ""
      : (MODE == Mode.DEVELOPMENT ? "_dev" : (MODE == Mode.RELEASE_EB ? "_live" : "_test"));

  /**
   * The standard xBook folder in the registy.
   */
  static {
    configXBook = Preferences.userRoot().node("xbook");
  }

  public static final int PACKETSIZE = 16;//the maximal number of packet size. WARNING is not set on server so this might lead to bugs if used in books that require a sync.!!!

  /**
   * Holds the status if the program is in development.
   *
   * If it is set to true, some useful elements for development will be
   * changed. e.g. set User ossobook / ossobook into the login screen, etc...
   */
  /**
   * Returns the path to the webapp.
   *
   * @return The URL to the webapp as a String.
   */
  public static String getWebAppURL() {
    if (instance == null) {
      instance = new XBookConfiguration() {

      };
    }
    return instance.getWebAppURLInstance();
  }

  protected String getWebAppURLInstance() {
    String postfix = (MODE == Mode.RELEASE_EB) ? "" : EXTENSION;

    return serverLocation + "/webapp" + postfix + "/functions/";
  }

  private static int MYSQL_PORT = -1;

  public static String getDatabasePort() {
    return getDatabasePort(false, true);
  }

  public static String getDatabasePort(boolean forceNew, boolean autoConfigure) {
    if (MYSQL_PORT == -1 || forceNew) {
      if (MODE == Mode.DEVELOPMENT) {
        return "53309";
      } else if (MODE == Mode.TEST) {
        return "53308";
      }
      LogFactory.getLog(XBookConfiguration.class).info("auto? " + autoConfigure);
      int basePort = 53307;
      if (!autoConfigure) {
        MYSQL_PORT = basePort;
        return MYSQL_PORT + "";
      }
      if (MYSQL_PORT != -1) {
        basePort = MYSQL_PORT + 1;
        LogFactory.getLog(XBookConfiguration.class).info("increase? ");
      }

      ServerSocket serverSocket = null;
      try {
        while (true) {
          try {

            serverSocket = new ServerSocket(basePort);
            break;
          } catch (IOException ex) {
            basePort++;
          }
        }
      } finally {
        if (serverSocket != null) {
          try {
            serverSocket.close();
          } catch (IOException ex) {
            LogFactory.getLog(XBookConfiguration.class).error(null, ex);
          }
        }
      }
      LogFactory.getLog(XBookConfiguration.class).debug("port: " + basePort);
      MYSQL_PORT = basePort;
    }
    return MYSQL_PORT + "";
  }

  public enum DatabaseMode {

    NONE, MYSQL, H2, SQLITE
  }


  /**
   * Initializes the configuration by loading the configuration files.
   */
  public static void init() {
    sharedUserTable = true;
    standardConfig.put(LOCKPORT, "50505");
    standardConfig.put(LANGUAGE, Locale.getDefault().getLanguage());
//        standardConfig.put("mysqld.enabled", "true");
//        standardConfig.put("forwarder.enabled", "true");
//        standardConfig.put("forwarder.0.backendport", "5001");
//        standardConfig.put("forwarder.0.listenport", "25");
//        standardConfig.put("installer.enabled", "true");
//        standardConfig.put(SERVER_ADDRESS, "localhost");// "ossobook.vetmed.uni-muenchen.de");
//        standardConfig.put(MYSQL_PORT, MODE == Mode.RELEASE ? "53307" : (MODE == Mode.DEVELOPMENT ? "53309" : "53308"));
//        standardConfig.put("local.database", "ossobook");
//        standardConfig.put("mail.server", "localhost");
//        standardConfig.put("mail.username", "ossobook");
//        standardConfig.put("mail.password", "Test-123");
//        standardConfig.put("mail.sender", "xbook-admin@xbook.vetmed.uni-muenchen.de");

    URL rootFolder = XBookConfiguration.class.getProtectionDomain().getCodeSource().getLocation();
    File rootfile = new File(rootFolder.getPath());

    System.setProperty("app.root", rootfile.getParent());
    setLanguage(configXBook.get(LANGUAGE, standardConfig.getProperty(LANGUAGE)));

    // Set proper logging configuration.
    PropertyConfigurator.configure(CONFIG_DIR + "logging.config");
    // Then translate the logger.
    Log log = LogFactory.getLog(XBookConfiguration.class);

    // OK, final sysout with more exciting informations.
    log.info("Standard language: " + Locale.getDefault().getLanguage());
    log.info("Client Java version: " + System.getProperty("java.version"));
    log.info("********************************************************************");
    log.info("Successfully parsed settings, switching to configured logging.");

    // Add shutdown hook that will save the settings.
    Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          configXBook.flush();
          recursiveDeleteOnExit(TMP_DOWNLOAD);

        } catch (BackingStoreException | IOException ex) {
          Logger.getLogger(XBookConfiguration.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    ));
    CURRENT_LANGUAGE = getProperty(LANGUAGE)
        .substring(0, (getProperty(LANGUAGE) + "_").indexOf("_"));


  }

  public static void recursiveDeleteOnExit(Path path) throws IOException {
    Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(Path file,
          @SuppressWarnings("unused") BasicFileAttributes attrs) {
        file.toFile().deleteOnExit();
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult preVisitDirectory(Path dir,
          @SuppressWarnings("unused") BasicFileAttributes attrs) {
        dir.toFile().deleteOnExit();
        return FileVisitResult.CONTINUE;
      }
    });
  }

  public static void setBookType(String databaseName) {
    AbstractConfiguration.databaseName = databaseName + EXTENSION;
    bookConfig = Preferences.userRoot().node(databaseName + EXTENSION);
  }

  public static void setLanguage(String language) {
    configXBook.put(LANGUAGE, language);
    language += "_";
    String end = "";
    if (language.indexOf("_") != language.lastIndexOf("_")) {
      end = language.substring(language.indexOf("_") + 1, language.lastIndexOf("_"));
    }
    Locale.setDefault(new Locale(language.substring(0, language.indexOf("_")), end));
  }

  public static void setProperty(String key, String value) {
    configXBook.put(key, value);
  }

  public static void setDefaultProperty(String key, String value) {
    bookConfig.put(key, value);
  }

  public static void setProperty(String key, int value) {
    configXBook.putInt(key, value);
  }

  public static void setDefaultProperty(String key, int value) {
    bookConfig.putInt(key, value);
  }

  /**
   * Set a property to the registry for showing db info in the listing.
   *
   * @param preferences The Preferences object of the specific book type (use the static variables
   * in the Configuration class.
   * @param value The status if the db info is set visible or false.
   */
  public static void setListingShowDBInfoProperty(Preferences preferences, boolean value) {
    preferences.node(LISTING_SHOW_DB_INFO).putBoolean("display", value);
  }

  public static boolean getListingShowDBInfoProperty(Preferences preferences) {
    return preferences.node(LISTING_SHOW_DB_INFO).getBoolean("display", false);
  }

  public static int getColumnPosition(String column, int defaultValue) {

    return bookConfig.node("columns").getInt(column.toLowerCase(), defaultValue);
  }

  public static void setColumnPosition(String column, int value) {

    bookConfig.node("columns").putInt(column.toLowerCase(), value);
  }

  public static String cmdToString(String[] cmd) {
    String ret = "";
    for (String s : cmd) {
      ret += s + " ";
    }
    return ret;
  }

  public static void setDatabaseMode(DatabaseMode databaseMode) {
    XBookConfiguration.databaseMode = databaseMode;
  }

  public static DatabaseMode getDatabaseMode() {
    return databaseMode;
  }

}
