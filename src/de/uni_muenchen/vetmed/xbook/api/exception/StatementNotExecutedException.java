package de.uni_muenchen.vetmed.xbook.api.exception;

import java.sql.SQLException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * exception for the case that a DML statement could not be executed
 *
 * @author j.lamprecht
 *
 */
public class StatementNotExecutedException extends SQLException {

    private static final long serialVersionUID = 2076681914417747674L;
    private static final Log log = LogFactory.getLog(StatementNotExecutedException.class);
    private final String query;

    /**
     * inform user about DML fault
     *
     * @param query - statement that could not be executed
     */
    public StatementNotExecutedException(String query) {
        this.query = query;
        log.error(query, this);
    }

    public StatementNotExecutedException(String query, SQLException description) {
        this(description.getLocalizedMessage() + "\n" + query);
    }

    @Override
    public String toString() {
        return query;
    }
}
