package de.uni_muenchen.vetmed.xbook.api.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class NotLoggedInException extends Exception {

    private static final Log log = LogFactory.getLog(NotLoggedInException.class);
    private final String query;
    private ErrorType type;

    public enum ErrorType {

        AccessDenied, ConnectionFailed, DBNotInitialized
    };

    public NotLoggedInException(boolean writeToLog) {
        query = "";
        type = ErrorType.ConnectionFailed;
        if (writeToLog) {
            log.error("NOT LOGGED IN", this);
        }
    }

    public NotLoggedInException(ErrorType type) {
        query = "";
        this.type = type;
        log.error("NOT CONNECTED", this);
    }

    /**
     * inform user about DML fault
     *
     * @param query - statement that could not be executed
     */
    public NotLoggedInException(String query) {
        super(query);
        this.query = query;
        type = ErrorType.ConnectionFailed;
        log.error(query, this);
    }

    public NotLoggedInException(String query, ErrorType type) {
        super(query);
        this.query = query;
        this.type = type;
        log.error(query, this);
    }

    public NotLoggedInException() {
        this(true);
    }

    @Override
    public String toString() {
        return query;
    }

    public ErrorType getErrorType() {
        return type;
    }
}
