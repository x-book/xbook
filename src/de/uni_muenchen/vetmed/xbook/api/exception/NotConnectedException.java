package de.uni_muenchen.vetmed.xbook.api.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Exception that is thrown when trying to send message to server but no
 * connection is available
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class NotConnectedException extends Exception {

    private static final Log log = LogFactory.getLog(NotConnectedException.class);
    private final String query;

    public NotConnectedException() {
        super();

        query = "";
        log.error("NO CONNECTION TO THE SERVER", this);
    }

    /**
     * inform user about DML fault
     *
     * @param query - statement that could not be executed
     */
    public NotConnectedException(String query) {
        super(query);
        this.query = query;
        log.error(query, this);
    }

    @Override
    public String toString() {
        return query;
    }
}
