package de.uni_muenchen.vetmed.xbook.api.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class NotLoadedException extends Exception {

    private static final Log log = LogFactory.getLog(NotLoadedException.class);

    public NotLoadedException() {
        log.error("NOT LOADED", this);
    }
}
