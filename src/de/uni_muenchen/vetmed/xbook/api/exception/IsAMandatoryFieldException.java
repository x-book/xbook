package de.uni_muenchen.vetmed.xbook.api.exception;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

public class IsAMandatoryFieldException extends Exception {

    private ColumnType columnType;

    public IsAMandatoryFieldException() {
    }

    public IsAMandatoryFieldException(ColumnType columnType) {
        this.columnType = columnType;
    }

    public ColumnType getColumnType() {
        return columnType;
    }

}
