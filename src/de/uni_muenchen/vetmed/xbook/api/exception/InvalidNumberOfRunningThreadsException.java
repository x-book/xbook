package de.uni_muenchen.vetmed.xbook.api.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class InvalidNumberOfRunningThreadsException extends Exception {

    private static final Log log = LogFactory.getLog(InvalidNumberOfRunningThreadsException.class);

    public InvalidNumberOfRunningThreadsException() {
        this("");
    }

    public InvalidNumberOfRunningThreadsException(String msg) {
        log.debug(msg, this);
    }

}
