package de.uni_muenchen.vetmed.xbook.api.exception;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class FileTooLargeException extends Throwable {
}
