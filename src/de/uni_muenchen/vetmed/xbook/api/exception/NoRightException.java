package de.uni_muenchen.vetmed.xbook.api.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class NoRightException extends Exception {

    public enum Rights {

        NOREAD, NOWRITE, NOADMIN, NOPROJECTOWNER
    }
    private final Rights right;
    private static final Log log = LogFactory.getLog(NoRightException.class);

    public NoRightException(Rights right) {
        this.right = right;
        log.error("NO RIGHT", this);
    }

    public Rights getType() {
        return right;
    }
}
