package de.uni_muenchen.vetmed.xbook.api.exception;

import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class WrongModeException extends Exception {

    private static final Log log = LogFactory.getLog(WrongModeException.class);

    public WrongModeException(GeneralInputMaskMode correctMode, GeneralInputMaskMode foundMode) {
        log.error("Wrong mode. '" + foundMode + "' found, but '" + correctMode + " expected.", this);
    }

}
