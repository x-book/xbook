package de.uni_muenchen.vetmed.xbook.api.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@cesonia.io>
 */
public class ValueNotAvailableException extends Exception {

    private static final Log log = LogFactory.getLog(NoRightException.class);

    public ValueNotAvailableException(Object value) {
        log.error("Value '" + value.toString() + "' is not available in list/array.", this);
    }

}
