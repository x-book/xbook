package de.uni_muenchen.vetmed.xbook.api.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class InvalidDateException extends Exception {

    private static final Log log = LogFactory.getLog(InvalidDateException.class);

    private String wrongDate;

    public InvalidDateException(String wrongDate) {
        this(wrongDate, false);
    }

    public InvalidDateException(String wrongDate, boolean printMsg) {
        this.wrongDate = wrongDate;
        if (printMsg) {
            log.error("The date '" + wrongDate + "' is invalid.");
        }
    }

    public String getWrongDate() {
        return wrongDate;
    }

}
