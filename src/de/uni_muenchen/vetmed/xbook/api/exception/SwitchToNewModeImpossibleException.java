package de.uni_muenchen.vetmed.xbook.api.exception;

import de.uni_muenchen.vetmed.xbook.api.helper.InputMaskModes;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class SwitchToNewModeImpossibleException extends Exception {

    private static final Log log = LogFactory.getLog(SwitchToNewModeImpossibleException.class);

    public SwitchToNewModeImpossibleException(InputMaskModes modeOld, InputMaskModes modeNew) {
        log.error("Cannot switch from Input Mask Mode '" + modeOld + "' to '" + modeNew + ".", this);
    }
}
