package de.uni_muenchen.vetmed.xbook.api.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class EntriesException extends Exception {

    private String message;
    private static final Log log = LogFactory.getLog(EntriesException.class);

    /**
     * Creates a new EntriesException with the given type and message
     *
     * @param message The message to be displayed
     */
    public EntriesException(String message) {
        this(message, true);
    }

    /**
     *
     * @param message
     * @param print
     */
    public EntriesException(String message, boolean print) {
        super(message);
        this.message = message;
        if (print) {
            log.error(message, this);
        }
    }

    /**
     * Creates a new Entries Exception with the option to print the exception
     *
     * @param print
     */
    public EntriesException(boolean print) {
        super();
        this.message = "EntriesException ";
        if (print) {
            log.error("", this);
        }
    }

    /**
     * Creates a new EntriesException
     */
    public EntriesException() {
        this(true);
    }

    @Override
    public String toString() {
        return message;
    }
}
