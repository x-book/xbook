package de.uni_muenchen.vetmed.xbook.api;

import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Global options for OssoBook.
 *
 * @author ali
 * @author fnuecke
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class AbstractConfiguration {

  public static String CURRENT_LANGUAGE;
  public static String databaseName;
  public static Preferences configXBook;
  protected static String serverLocation;

  public enum Mode {

    RELEASE, TEST, DEVELOPMENT, RELEASE_EB
  }

  /**
   * The prefix to find the current ossobook folder.
   */
  private static final File thisFile = new File(
      AbstractConfiguration.class.getProtectionDomain().getCodeSource().getLocation().getPath()
          .replaceAll("%20", " "));
  public static final String DIR_PREFIX =
      thisFile.isFile() ? thisFile.getParent() + "/" : System.getProperty("user.dir") + "/";
  public static final String ABSOLUT_DIR = thisFile.getAbsolutePath();
  /**
   * Holds the status if the program is in development.
   * <p/>
   * If it is set to true, some useful elements for development will be changed. e.g. set User
   * ossobook / ossobook into the login screen, etc...
   */
  public static final Mode MODE = Mode.DEVELOPMENT;
  /**
   * Force using the web app.
   */
  public static boolean USE_WEBAPP;
  /**
   * Holds the current database version.
   */
  public static String GENERAL_DATABASE_VERSION;
  /**
   * Holds the current status of the OssoBook. (e.g. alpha, beta, dev, rc, ...)
   */
  public static String STATUS;
  /**
   * Holds the current OssoBook version.
   */
  public static String PROGRAM_VERSION;
  /**
   * Status if the System.out's should be displayed or not.
   */
  public static boolean DISPLAY_SOUTS;
  /**
   * Status if the System.out's for the synchroniser should be displayed or not.
   */
  public static boolean LOG_SYNCHRONISATION_INFORMATION;
  /**
   * Directory containing graphics (general)
   */
  public static String IMAGES_DIR;

  public static String DATABASENUMBER = "databasenumber";
  public static String AUTOSYNC = "autosync";
  public static String SIDEBAR_VISIBLE = "sidebar_visible";

  public static String FULLSCREEN_MODE = "fullscreen";
  public static final String LOCKPORT = "lockport";
  public static final String CONFLICT = "conflict";
  public static final String WIDTH = "width";
  public static final String HEIGTH = "heigth";
  public static String USE_PROXY = "useproxy";
  public static String PROXY_HOST = "proxyhost";
  public static String PROXY_PORT = "proxyport";

  public static String TMP_UPLOAD_DIRECTORY = DIR_PREFIX + "tmp_upload";

  //    public static String MYSQL_PORT = "sqlport";
  public static final String PROPERTY_PROGRESS = "progress";

  public static Preferences bookConfig;

  public static boolean sharedUserTable;
  /**
   * Standard Ossobook client Settings
   */
  protected static final SortedProperties standardConfig = new SortedProperties();
  /**
   * Directory containing further configuration files.
   */
  public static String CONFIG_DIR = DIR_PREFIX + "assets/config/";

  /**
   * Directory containing documents.
   */
  public static String DOCUMENTS_DIR = DIR_PREFIX + "assets/documents";
  public static String FIRST_DB_UPDATE_DONE = "first_db_update_done";
  public static String LANGUAGE = "language";
  public static String BOOK_TYPE = "booktype";
  public static String DISPLAY_LAUNCHER = "displaylauncher";
  public static String LISTING_SHOW_DB_INFO = "listing_show_db_info";
  public static final int NO_BOOK = -1;
  public static final int OSSOBOOK = 0;
  public static final int ARCHAEOBOOK = 1;
  public static final int ANTHROBOOK = 2;
  public static final int EXCABOOK = 3;
  public static final int INBOOK = 4;
  public static final int DEMOBOOK = 5;
  public static final int PALAEODEPOT = 6;
  public static final int ANTHRODEPOT = 7;
  public static final int GALAKTOBOOK = 8;

  /**
   * Directory containing graphics (AnthroBook)
   */
  public static String IMAGES_OB_DIR = DIR_PREFIX + "assets/imagesOB.zip";
  public static String IMAGES_AB_DIR = DIR_PREFIX + "assets/imagesAB.zip";
  public static String IMAGES_AH_DIR = DIR_PREFIX + "assets/imagesAH.zip";
  public static String IMAGES_IB_DIR = DIR_PREFIX + "assets/imagesIB.zip";
  public static String IMAGES_PD_DIR = DIR_PREFIX + "assets/imagesPD.zip";
  public static String IMAGES_AD_DIR = DIR_PREFIX + "assets/imagesAD.zip";
  public static String IMAGES_DB_DIR = DIR_PREFIX + "assets/imagesDB.zip";

  public static String INPUT_FIELDS = "input_fields";
  public static String INPUT_FIELD_CHECKBOXES = "input_field_checkboxes";
  public static String MISC = "misc";
  public static String BUTTON_BAR_ON_TOP = "button_bar_on_top";
  public static String AUTO_SKIP_UPDATE_WHEN_FINISHED = "auto_skip_update_when_finished";
  public static String REMEMBER_LOGIN_USER = "remember_login_user";
  public static String REMEMBER_LOGIN_PASSWORD = "remember_login_password";


  //50 MB
  public static int MAX_UPLOAD_BATCH_SIZE = 1024 * 1024 * 50;

  /**
   * Converts the programm version into a more readable String.
   * <p/>
   * Removes unnecessary zeros. Example 1: Instead of "4.00.05" it will return "4.0.5". Example 2:
   * Instead of "4.10.0005.1" it will return "4.10.5".
   *
   * @return The converted String holding the program version.
   */
  public static String getProgramVersion() {
    String[] s = PROGRAM_VERSION.split("\\.");
    Integer mainVersion = Integer.parseInt(s[0]);
    Integer subVersion = Integer.parseInt(s[1]);

    String updateVersion = "";
      if (s.length > 2) {
          updateVersion = "." + Integer.parseInt(s[2]);
      }
    return mainVersion + "." + subVersion + updateVersion;
  }

  public static String getProgramVersionExtended() {
    String stringVersion = getProgramVersion();
    if (!(STATUS).equals("")) {
      stringVersion += " " + STATUS;
    }
    return stringVersion;
  }

  /**
   * Set a property to the registry for a specific input field.
   *
   * @param preferences The Preferences object of the specific book type (use the static variables
   * in the Configuration class.
   * @param projectKey The key of the project which input field settings should be saved.
   * @param inputFieldName The name of the specific input field.
   * @param value The status if it is set visible or false.
   */
  public static void setInputFieldProperty(Preferences preferences, Key projectKey,
      String inputFieldName, boolean value) {
    preferences.node(INPUT_FIELDS).node(projectKey.toString()).putBoolean(inputFieldName, value);
  }

  public static boolean getInputFieldProperty(Preferences preferences, Key projectKey,
      String inputFieldName) {
    return preferences.node(INPUT_FIELDS).node(projectKey.toString())
        .getBoolean(inputFieldName, true);
  }

  /**
   * Set a property to the registry for a specific input field checkbox.
   *
   * @param preferences The Preferences object of the specific book type (use the static variables
   * in the Configuration class.
   * @param projectKey The key of the project which input field settings should be saved.
   * @param inputFieldName The name of the specific input field.
   * @param value The status if it is set visible or false.
   */
  public static void setInputFieldCheckboxProperty(Preferences preferences, Key projectKey,
      String inputFieldName, boolean value) {
    preferences.node(INPUT_FIELD_CHECKBOXES).node(projectKey.toString())
        .putBoolean(inputFieldName, value);
  }

  public static boolean getInputFieldCheckboxProperty(Preferences preferences, Key projectKey,
      String inputFieldName) {
    return preferences.node(INPUT_FIELD_CHECKBOXES).node(projectKey.toString())
        .getBoolean(inputFieldName, false);
  }

  public static void setMiscPropertyBoolean(Preferences preferences, Key projectKey,
      String propertyName, boolean value) {
    preferences.node(MISC).node(projectKey.toString()).putBoolean(propertyName, value);
  }

  public static boolean getMiscPropertyBoolean(Preferences preferences, Key projectKey,
      String propertyName, boolean defaultValue) {
    return preferences.node(MISC).node(projectKey.toString())
        .getBoolean(propertyName, defaultValue);
  }

  public static class SortedProperties extends Properties {

    /**
     * Overrides, called by the store method.
     */
    @SuppressWarnings("unchecked")
    @Override
    public synchronized Enumeration keys() {
      Enumeration keysEnum = super.keys();
      ArrayList keyList = new ArrayList();
      while (keysEnum.hasMoreElements()) {
        keyList.add(keysEnum.nextElement());
      }
      Collections.sort(keyList);
      return Collections.enumeration(keyList);
    }

  }

  public static String getProperty(String key) {
    String property = configXBook.get(key, standardConfig.getProperty(key));
    return property;
  }

  public static Preferences getBookConfig() {
    return bookConfig;
  }

  public static void setConflict(Key Project, boolean isConflicted) {
    bookConfig.node(CONFLICT).putBoolean(Project.toString(), isConflicted);
  }

  public static boolean isConflicted(Key Project) {
    return bookConfig.node(CONFLICT).getBoolean(Project.toString(), false);
  }

  /**
   * Returns the saved integer value for the xBook configuration.
   *
   * @param name The name of the parameter
   * @return the value of the property. -1 as default.
   */
  public static int getInt(String name) {
    return configXBook.getInt(name, -1);
  }

  public static int getInt(String name, int defaultInt) {
    return configXBook.getInt(name, defaultInt);
  }

  /**
   * Returns the saved integer value for the current book.
   *
   * @param name The name of the parameter
   * @return the value of the property. -1 as default.
   */
  public static int getBookInt(String name) {
    return bookConfig.getInt(name, -1);
  }

  public static int getBookInt(String name, int defaultInt) {
    return bookConfig.getInt(name, defaultInt);
  }

  public static String getBookString(String name, String defaultString) {
    return bookConfig.get(name, defaultString);
  }


  /**
   * Returns the saved boolean value for the xBook configuration.
   *
   * @param name The name of the parameter
   * @return the value of the property. true as default.
   */
  public static boolean getBoolean(String name) {
    return configXBook.getBoolean(name, true);
  }

  public static boolean getBoolean(String name, boolean def) {
    return configXBook.getBoolean(name, def);
  }

  /**
   * Returns the saved boolean value for the current book.
   *
   * @param name The name of the parameter
   * @return the value of the property. false as default.
   */
  public static boolean getBookBoolean(String name) {
    return bookConfig.getBoolean(name, false);
  }

  public static void setProperty(String key, boolean value) {
    configXBook.putBoolean(key, value);
  }

  public static void setBookProperty(String key, boolean value) {
    bookConfig.putBoolean(key, value);
  }

  public static void setBookProperty(String key, String value) {
    bookConfig.put(key, value);
  }

  public static void setBookProperty(String key, int value) {
    bookConfig.putInt(key, value);
  }

  public static String getDefaultProperty(String key) {
    return standardConfig.getProperty(key);
  }

  public static Preferences getPreferences() {
    return configXBook;
  }

  public static Boolean setPasswordProperty(String key, char[] plainPassword) {
    return setPasswordProperty(key, StringHelper.charArrayToString(plainPassword));
  }

  public static Boolean setPasswordProperty(String key, String plainPassword) {
    String encoded;
    try {
      encoded = StringHelper.encodeString(plainPassword);
    } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
      Logger.getLogger(AbstractConfiguration.class.getName()).log(Level.SEVERE, null, ex);
      return null;
    }
    XBookConfiguration.setProperty(key, encoded);
    return true;
  }

  public static String getPasswordProperty(String key) {
    String decoded;
    try {
      decoded = StringHelper
          .decodeString(XBookConfiguration.getProperty(XBookConfiguration.REMEMBER_LOGIN_PASSWORD));
    } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
      Logger.getLogger(AbstractConfiguration.class.getName()).log(Level.SEVERE, null, ex);
      return null;
    }
    return decoded;
  }
}
