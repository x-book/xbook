package de.uni_muenchen.vetmed.xbook.api.event;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class EntryEvent {

    private Key projectKey;
    private Key entryKey;
    private DataRow data;

    public EntryEvent(Key projectKey, DataRow entryData) {
        this.projectKey = projectKey;
        this.data = entryData;
    }

    public EntryEvent(Key projectKey, Key key) {
        this.projectKey = projectKey;
        this.entryKey = key;
    }

    public Key getProjectKey() {
        return projectKey;
    }

    public Key getEntryKey() {
        return entryKey;
    }

    public DataRow getData() {
        return data;
    }
}
