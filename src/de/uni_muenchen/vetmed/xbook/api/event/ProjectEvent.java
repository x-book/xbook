package de.uni_muenchen.vetmed.xbook.api.event;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class ProjectEvent {

    private final DataSetOld project;

    public ProjectEvent(DataSetOld project) {
        this.project = project;
    }

    public DataSetOld getProject() {
        return project;
    }
}
