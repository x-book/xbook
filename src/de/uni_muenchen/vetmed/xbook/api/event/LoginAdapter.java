package de.uni_muenchen.vetmed.xbook.api.event;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class LoginAdapter implements LoginListener {

    @Override
    public void onLogin() {
    }

    @Override
    public void onLogout() {
    }
}
