package de.uni_muenchen.vetmed.xbook.api.event;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class ProjectListenerAdapter implements ProjectListener {

    @Override
    public void onProjectLoaded(ProjectEvent evt) {
    }

    @Override
    public void onProjectUnloaded(ProjectEvent evt) {
    }

    @Override
    public void onProjectAdded(ProjectEvent evt) {

    }

    @Override
    public void onProjectChanged(ProjectEvent evt) {

    }

    @Override
    public void onProjectDeleted(ProjectEvent evt) {

    }
}
