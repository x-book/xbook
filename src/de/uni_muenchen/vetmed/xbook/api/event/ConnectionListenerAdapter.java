package de.uni_muenchen.vetmed.xbook.api.event;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ConnectionListenerAdapter implements ConnectionListener {

    @Override
    public void onConnected() {
    }

    @Override
    public void onDisconnected() {
    }
}
