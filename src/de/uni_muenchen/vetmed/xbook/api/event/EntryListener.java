package de.uni_muenchen.vetmed.xbook.api.event;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface EntryListener {

    public void onEntryDeleted(EntryEvent e);

    public void onEntryAdded(EntryEvent e);

    public void onEntryUpdated(EntryEvent e);
}
