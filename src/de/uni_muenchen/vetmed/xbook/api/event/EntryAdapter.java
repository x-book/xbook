package de.uni_muenchen.vetmed.xbook.api.event;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class EntryAdapter implements EntryListener {

    @Override
    public void onEntryAdded(EntryEvent e) {
    }

    @Override
    public void onEntryDeleted(EntryEvent e) {
    }

    @Override
    public void onEntryUpdated(EntryEvent e) {
    }
}
