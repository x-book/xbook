package de.uni_muenchen.vetmed.xbook.api.event;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface ProjectListener {

     void onProjectLoaded(ProjectEvent evt);

     void onProjectUnloaded(ProjectEvent evt);

     void onProjectAdded(ProjectEvent evt);

     void onProjectChanged(ProjectEvent evt);

     void onProjectDeleted(ProjectEvent evt);
}
