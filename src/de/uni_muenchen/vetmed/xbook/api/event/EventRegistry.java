package de.uni_muenchen.vetmed.xbook.api.event;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

import java.util.ArrayList;
import java.util.HashMap;

public class EventRegistry {

    private static HashMap<String, ArrayList<ValueEventListener>> valueListener = new HashMap<>();

    private static ArrayList<ValueEventListener> allListener = new ArrayList<>();

    public static void addValueListener(ColumnType columnType, ValueEventListener listener) {
        addValueListener(columnType.getColumnName(), listener);
    }

    public static void addValueListener(String identifier, ValueEventListener listener) {
        ArrayList<ValueEventListener> list = valueListener.get(identifier);
        if (list == null) {
            list = new ArrayList<>();
            valueListener.put(identifier, list);
        }
        if (!list.contains(listener)) {
            list.add(listener);
        }
    }

    public static void addValueListener(ValueEventListener listener){
        allListener.add(listener);
    }

    public static void removeValueListener(ColumnType columnType, ValueEventListener listener) {
        removeValueListener(columnType.getColumnName(), listener);
    }

    public static void resetListeners() {
        valueListener = new HashMap<>();
    }

    public static void removeValueListener(String identifier, ValueEventListener listener) {
        ArrayList<ValueEventListener> list = valueListener.get(identifier);
        if (list == null) {
            return;
        }
        list.remove(listener);
    }

    public static void notifyListener(ColumnType columnType) {
        notifyListener(columnType.getColumnName());
    }

    public static void notifyListener(String identifier) {
        for (ValueEventListener valueEventListener : allListener) {
            valueEventListener.onValueUpdated();
        }
        ArrayList<ValueEventListener> list = valueListener.get(identifier);
        if (list == null) {
            return;
        }

        for (ValueEventListener listener : (ArrayList<ValueEventListener>) list.clone()) {
            listener.onValueUpdated();

        }
    }
}
