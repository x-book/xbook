package de.uni_muenchen.vetmed.xbook.api.event;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface CodeTableListener {

    void onCodeTablesUpdated();
}
