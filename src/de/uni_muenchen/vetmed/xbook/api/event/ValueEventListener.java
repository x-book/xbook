package de.uni_muenchen.vetmed.xbook.api.event;

public interface ValueEventListener {

    void onValueUpdated();
}
