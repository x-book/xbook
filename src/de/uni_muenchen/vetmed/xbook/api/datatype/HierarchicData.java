package de.uni_muenchen.vetmed.xbook.api.datatype;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class HierarchicData {

    String value;
    String parentId;

    public HierarchicData(String value, String parentId) {
        this.value = value;
        this.parentId = parentId;
    }

    public String getParentId() {
        return parentId;
    }

    public String getValue() {
        return value;
    }

}
