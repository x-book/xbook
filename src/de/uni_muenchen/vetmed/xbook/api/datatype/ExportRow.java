package de.uni_muenchen.vetmed.xbook.api.datatype;

import java.util.TreeMap;

public class ExportRow extends TreeMap<ColumnType, ExportColumn> {

    private final TreeMap<ColumnType, String> headlines;

    public ExportRow(TreeMap<ColumnType, String> headlines) {
        this.headlines = headlines;
    }

    public ExportColumn getExportColumn(ColumnType columnName) {
        ExportColumn rowData;
        if (containsKey(columnName)) {
            rowData = get(columnName);
        } else {
            rowData = new ExportColumn(columnName.isUnique());
            put(columnName, rowData);
            String displayName = columnName.getDisplayName();
            displayName = displayName.replaceAll("<html>","" ).replaceAll("<br>","" ).replaceAll("</html>","" );


            headlines.put(columnName, displayName);
        }
        return rowData;
    }

    public boolean existsExportColumn(ColumnType columnType){
        return containsKey(columnType);
    }

    public String getValue(ColumnType type) {
        ExportColumn c = get(type);
        if (c != null) {
            return c.getStringData();
        }
        return "";
    }
}
