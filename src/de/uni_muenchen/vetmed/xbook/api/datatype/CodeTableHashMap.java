package de.uni_muenchen.vetmed.xbook.api.datatype;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class CodeTableHashMap extends LinkedHashMap<String, String> {

    /**
     * Converts a list of values to a list of IDs (as String).
     *
     * Values are ignored if they are not found.
     *
     * @param values The values that should be converted.
     * @return A list of IDs as String.
     */
    public ArrayList<String> convertStringToId(ArrayList<String> values) {
        ArrayList<String> ids = new ArrayList<>();
        for (String set : values) {
            for (Map.Entry<String, String> d : entrySet()) {
                String iterId = d.getKey();
                String iterValue = d.getValue();
                if (iterValue.equals(set)) {
                    ids.add(iterId);
                    break;
                }
            }
        }
        return ids;
    }

    /**
     * Converts a single value to ID as String.
     *
     * @param value The value to convert.
     * @return The converted value as ID. null if value is not found.
     */
    public String convertStringToIdAsString(String value) {
        String id = null;
        if (containsValue(value)) {
            for (Map.Entry<String, String> d : entrySet()) {
                if (d.getValue().equals(value)) {
                    id = d.getKey();
                    break;
                }
            }
        }
        return id;
    }

    /**
     * Converts a single value to ID as Integer.
     *
     * @param value The value to convert.
     * @return The converted value as ID. null if value is not found.
     */
    public Integer convertStringToIdAsInteger(String value) {
        String id = convertStringToIdAsString(value);
        if (id == null) {
            return null;
        }
        try {
            return Integer.parseInt(id);
        } catch (NumberFormatException ex) {
            return -1;
        }
    }

    /**
     * Converts a single ID to the value as String.
     *
     * @param id The ID to convert.
     * @return The converted value. Empty String if no value is available.
     */
    public String convertIdAsStringToString(String id) {
        if (get(id) != null) {
            return get(id);
        }
        return "";
    }

    /**
     * Converts a single ID to the value as String.
     *
     * @param id The ID to convert.
     * @return The converted value. Empty String if no value is available.
     */
    public String convertIdAsIntegerToString(Integer id) {
        return convertIdAsStringToString(id + "");
    }

    /**
     * Merges another CodeTableHashMap object with the current one.
     *
     * @param other The other CodeTableHashMap object that should be merged.
     * @return The merged CodeTableHashMap object.
     */
    public CodeTableHashMap merge(CodeTableHashMap other) {
        CodeTableHashMap merged = new CodeTableHashMap();
        if (other.isEmpty()) {
            return (CodeTableHashMap) this.clone();
        } else if (isEmpty()) {
            return (CodeTableHashMap) other.clone();
        } else {
            for (Entry<String, String> t : other.entrySet()) {
                if (containsKey(t.getKey())) {
                    merged.put(t.getKey(), t.getValue());
                }
            }
        }
        return merged;
    }

    /**
     * Adds a new DataColumn.
     *
     * @param data The DataColumn to add.
     * @return the previous value associated with <tt>key</tt>, or
     * <tt>null</tt> if there was no mapping for <tt>key</tt>. (A <tt>null</tt>
     * return can also indicate that the map previously associated <tt>null</tt>
     * with <tt>key</tt>.)
     */
    public String put(DataColumn data) {
        return super.put(data.getColumnName(), data.getValue());
    }

    /**
     * Return all values as an Object[] (actual a String[]).
     *
     * @param addEmptyItem true if an ampty String should be added.
     * @return All values as a list.
     */
    public Object[] getValuesAsArray(boolean addEmptyItem) {
        ArrayList<String> list = new ArrayList<>();
        if (addEmptyItem) {
            list.add("");
        }
        list.addAll(values());
        return list.toArray();
    }

    public Collection<DataColumn> getMapAsCollection() {
        ArrayList<DataColumn> dataColumns = new ArrayList<>();
        for (Entry<String, String> e : entrySet()) {
            dataColumns.add(new DataColumn(e.getValue(), e.getKey()));
        }
        return dataColumns;
    }

}
