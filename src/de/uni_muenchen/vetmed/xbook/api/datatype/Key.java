package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.network.ISerializableTypes;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author jojo
 */
public final class Key extends Serialisable implements Comparable<Key> {

    private int id;
    private int databaseId;

    /**
     * Constructor.
     *
     * Initialises the ID and the database ID.
     *
     * @param id The ID.
     * @param databaseId The database ID.
     */
    public Key(int id, int databaseId) {
        this.id = id;
        this.databaseId = databaseId;
    }

    /**
     * Constructor.
     *
     * Initialises the ID and the database ID.
     *
     * @param id The ID.
     * @param databaseId The database ID.
     */
    public Key(String id, String databaseId) {
        this.id = Integer.valueOf(id);
        this.databaseId = Integer.valueOf(databaseId);
    }

    /**
     * Constructor.
     *
     * Initialises the key with serialized data.
     *
     * @param is An object implementing an input interface to be able to handle
     * the serialized data.
     * @throws IOException Thrown on errors while deserializing.
     */
    public Key(SerialisableInputInterface is) throws IOException {
        this(is.readInt(), is.readInt());
    }

    public Key(xResultSet rs, ColumnType projectId, ColumnType projectDatabaseId) throws SQLException {
        this(rs.getInt(projectId),rs.getInt(projectDatabaseId));
    }

    /**
     * Get the ID.
     *
     * @return The ID.
     */
    public int getID() {
        return id;
    }

    /**
     * Get the database ID.
     *
     * @return The database ID.
     */
    public int getDBID() {
        return databaseId;
    }

    /**
     * Checks if the key is a valid ley, that means: Is there a ID >= 0.
     *
     * @return true if it is a valid key, else false.
     */
    public boolean isValidKey() {
        return (id >= 0);
    }

    @Override
    public String toString() {
        return "id:" + id + "|dbid:" + databaseId;
    }

    @Override
    public boolean equals(Object anObject) {
        if (this == anObject) {
            return true;
        }
        if (!(anObject instanceof Key)) {
            return false;
        }
        Key other = (Key) anObject;
        return id == other.id && databaseId == other.databaseId;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.id;
        hash = 67 * hash + this.databaseId;
        return hash;
    }

    @Override
    public int compareTo(Key o) {
        if (o == null) {
            throw new NullPointerException();
        }
        if (o.getID() != getID()) {
            return getID() - o.getID();
        }
        return getDBID() - o.getDBID();
    }

    @Override
    protected int getClassID() {
        return ISerializableTypes.KEY_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface os) throws IOException {
        os.writeInt(id);
        os.writeInt(databaseId);
    }
}
