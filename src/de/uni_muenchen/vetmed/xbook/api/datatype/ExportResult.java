package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.*;
import java.util.Map.Entry;

/**
 * Class to save all Results from the CSVExport that contains the name of the
 * Column and the Values mapped to the name of the Column, also ordered to order
 * in which the rows shall be displayed
 *
 * @author Johannes Lohrer
 */
public class ExportResult implements Iterable<ArrayList<String>> {

    private final Log LOGGER = LogFactory.getLog(ExportResult.class);

    private TreeMap<ColumnType, String> headlines;
    private LinkedHashMap<EntryKey, ExportRow> entries;
    // private HashMap<ColumnType, Integer> columnSorting = new HashMap<>();
    private int maxSort = 0;
    Comparator<ColumnType> comp = new Comparator<ColumnType>() {
        @Override
        public int compare(ColumnType t, ColumnType t1) {
            return t.compareTo(t1);
        }
    };

    public ExportResult() {
        headlines = new TreeMap<>(comp);
        entries = new LinkedHashMap<>();
        //  keyMapping = new HashMap<>();
    }

    public ExportResult(TreeMap<ColumnType, String> headlines, LinkedHashMap<EntryKey, ExportRow> entries) {//, HashMap<EntryKey, Integer> keyMapping) {
        this.headlines = headlines;
        this.entries = entries;
        //this.keyMapping = keyMapping;
    }

    public void addAll(ExportResult other) {
        headlines.putAll(other.headlines);
        entries.putAll(other.entries);
        // keyMapping.putAll(other.keyMapping);
    }
    public void addEntry(EntryKey key, ExportRow row) {
        entries.put(key,row);
    }
    public void addEntry(Entry<EntryKey,ExportRow> data) {
        entries.put(data.getKey(),data.getValue());
    }

    public void setHeadlines(
        TreeMap<ColumnType, String> headlines) {
        this.headlines = headlines;
    }

    public ArrayList<EntryKey> getEntryKeyList() {
        ArrayList<EntryKey> keys = new ArrayList<>();
        for (EntryKey entryKey : entries.keySet()) {
            keys.add(entryKey);
        }
        return keys;
    }
    public HashMap<Key, ArrayList<Key>> getKeysSortedByProject(){
        HashMap<Key,ArrayList<Key>> map = new HashMap<>();
        for (Entry<EntryKey, ExportRow> entryKeyExportRowEntry : entries.entrySet()) {
            ArrayList<Key> projectMap = map.get(entryKeyExportRowEntry.getKey().getProjectKey());
            if(projectMap == null){
                projectMap = new ArrayList<>();
                map.put(entryKeyExportRowEntry.getKey().getProjectKey(),projectMap);
            }
            projectMap.add(entryKeyExportRowEntry.getKey().getEntryKey());
        }
        return map;

    }
    public ArrayList<Key> getKeyList() {
        ArrayList<Key> keys = new ArrayList<>();
        for (EntryKey entryKey : entries.keySet()) {
            keys.add(entryKey.getEntryKey());
        }
        return keys;
    }
    public ArrayList<Key> getProjectKeyList() {
        ArrayList<Key> keys = new ArrayList<>();
        for (EntryKey entryKey : entries.keySet()) {
            keys.add(entryKey.getProjectKey());
        }
        return keys;
    }
    public void addHeadline(ColumnType columnType){
        String displayName = columnType.getDisplayName();
        displayName = displayName.replaceAll("<html>","" ).replaceAll("<br>","" ).replaceAll("</html>","" );
        headlines.put(columnType, displayName);
    }

    public ExportRow getExportRow(EntryKey entryKey) {
        ExportRow exportData;
        if (entries.containsKey(entryKey)) {
            exportData = entries.get(entryKey);
        } else {
            exportData = new ExportRow(headlines);
            entries.put(entryKey, exportData);
        }
        return exportData;
    }

    /**
     * Returns the Headlines
     *
     * @return
     */
    public TreeMap<ColumnType, String> getHeadlines() {
        return headlines;

    }

    /**
     * REturns the Highest sort number
     *
     * @return
     */
    public int getMaxSort() {
        return maxSort;
    }

    public LinkedHashMap<EntryKey, ExportRow> getEntries() {
        return entries;
    }

    /**
     * Returns the number of Entries
     *
     * @return
     */
    public int getNumberOfEntrys() {
        return entries.size();
    }

    @Override
    public Iterator<ArrayList<String>> iterator() {
        return new Iterator<ArrayList<String>>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < entries.size();
            }

            @Override
            public ArrayList<String> next() {
                ArrayList<String> list = new ArrayList<>();
                ExportRow entry = entries.values().toArray(new ExportRow[0])[index++];

                for (Entry<ColumnType, String> headline : headlines.entrySet()) {
                    ColumnType key = headline.getKey();
                    if (entry.containsKey(key)) {
                        list.add(entry.get(key).getStringData());
                    } else {
                        list.add("");
                    }
                }

                return list;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
    }

    /**
     * Returns the Key at the given Position from the Data
     *
     * @param index The index of the Entry
     * @return The Key of the Entry
     * @throws NumberFormatException when something strange happens
     */
    public Key getKeyAt(int index) throws NumberFormatException {
        return entries.keySet().toArray(new EntryKey[0])[index].getEntryKey();
    }
    public Key getProjectKeyAt(int index) throws NumberFormatException {
        return entries.keySet().toArray(new EntryKey[0])[index].getProjectKey();
    }


    public ArrayList<Key> getKeysAt(ArrayList<Integer> indices) throws NumberFormatException {
        final EntryKey[] entryKeys = entries.keySet().toArray(new EntryKey[0]);
        ArrayList<Key> returnList = new ArrayList<>();
        for (Integer index : indices) {
            returnList.add(entryKeys[index].getEntryKey());
        }
        return returnList;
    }

    public EntryKey getEntryKeyAt(int index) throws NumberFormatException {
        return entries.keySet().toArray(new EntryKey[0])[index];
    }

//    public HashMap<EntryKey, Integer> getKeyMapping() {
//        return keyMapping;
//    }
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ExportResult)) {
            return false;
        }
        ExportResult er = (ExportResult) obj;
        return entries.equals(er.entries);
    }

    @Override
    public String toString() {
        String sep = " | ";
        StringBuilder sb = new StringBuilder();
        for (Entry<EntryKey, ExportRow> entry : entries.entrySet()) {
            for (Entry<ColumnType, ExportColumn> e : entry.getValue().entrySet()) {
                if (e.getKey().equals(IStandardColumnTypes.ID)
                        || e.getKey().equals(IStandardColumnTypes.DATABASE_ID)
                        || e.getKey().equals(IStandardColumnTypes.PROJECT_ID)
                        || e.getKey().equals(IStandardColumnTypes.PROJECT_DATABASE_ID)) {
                    continue;
                }
                if (e.getValue() == null || e.getValue().getStringData().equals("-1") || e.getValue().getStringData().equals("-1.0") || e.getValue().getStringData().equals("null")) {
                    continue;
                }
                sb.append(e.getValue()).append(sep);
            }
        }
        return sb.substring(0, (sb.length() < sep.length()) ? sb.length() : sb.length() - sep.length());
    }

    public ExportResult concat(ExportResult temp) {
        ExportResult result = new ExportResult();
        if (entries.isEmpty()) {
            result = temp;
        } else if (temp.entries.isEmpty()) {
            result = this;
        } else {
            for (Entry<EntryKey, ExportRow> entry : entries.entrySet()) {
                if (temp.entries.containsKey(entry.getKey())) {
                    ExportRow row = result.getExportRow(entry.getKey());
                    row.putAll(entry.getValue());
                    row.putAll(temp.getExportRow(entry.getKey()));
                }
            }
        }
        return result;
    }

    public void filter(Map<ColumnType, SearchEntryInfo> filter) {
        final Iterator<ExportRow> iterator = entries.values().iterator();
        while (iterator.hasNext()) {
            ExportRow row = iterator.next();

            for (Entry<ColumnType, SearchEntryInfo> filterValues : filter.entrySet()) {
                ExportColumn column = row.getExportColumn(filterValues.getKey());
                if (column == null) {
                    continue;
                }
                SearchEntryInfo value = filterValues.getValue();
                if(value.getCurrentSearchMode()== SearchEntryInfo.SearchMode.EQUALS) {
                    if (!column.containsValue(value.getText())) {
                        iterator.remove();
                        break;
                    }
                }
                else{
                    LOGGER.debug("SEARCH MODE CONTAINS!! NO FILTER DONE!!");
                }
            }
        }
    }

}
