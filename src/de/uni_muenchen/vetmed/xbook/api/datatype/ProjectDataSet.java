package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.network.ISerializableTypes;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardProjectColumnTypes;

import java.io.IOException;

/**
 * A ProjectDataSet is the complete data set of a project.
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class ProjectDataSet extends DataSetOld implements Comparable<ProjectDataSet> {

    private final String projectName;
    private int projectOwnerId;
    private boolean deleted;

    private boolean isHidden = false;

    /**
     * Constructor.
     *
     * Initialises the project key, the database name, the project name and the
     * project owner ID. The tables list is empty.
     *
     * @param projectKey The unique project key of the data set.
     * @param databaseName The database name.
     * @param projectName The name of the project.
     * @param projectOwnerId The ID of the project owner.
     */
    public ProjectDataSet(Key projectKey, String databaseName, String projectName, int projectOwnerId, boolean hidden) {
        super(projectKey, databaseName, IStandardProjectColumnTypes.TABLENAME_PROJECT);
        this.projectName = projectName;
        this.projectOwnerId = projectOwnerId;
        isHidden = hidden;
    }

    /**
     * Constructor.
     *
     * Initialises the project data set with serialized data.
     *
     * @param is An object implementing an input interface to be able to handle
     * the serialized data.
     * @throws IOException Thrown on errors while deserializing.
     */
    public ProjectDataSet(SerialisableInputInterface is) throws IOException {
        super(is);
        projectName = is.readString();
        projectOwnerId = is.readInt();
        deleted = is.readBoolean();
    }

    /**
     * Search for the data table for the given project and return the first data
     * row. Created a new one if no data table is available for the given table
     * name.
     *
     * @return The data row for the given table name or a new one if there is no
     * data available for the given table.
     */
    public DataRow getProjectRow() {
        return super.getDataRow(IStandardProjectColumnTypes.TABLENAME_PROJECT);
    }

    /**
     * Get the project name.
     *
     * @return The project name.
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * Set a new project owner ID.
     *
     * @param projectOwnerId The new project owner id.
     */
    public void setProjectOwnerId(int projectOwnerId) {
        this.projectOwnerId = projectOwnerId;
    }

    /**
     * Get the project owner ID.
     *
     * @return The project owner ID.
     */
    public int getProjectOwnerId() {
        return projectOwnerId;
    }

    /**
     * Get the project ID.
     *
     * @return The project ID.
     */
    public int getProjectId() {
        return projectKey.getID();
    }

    /**
     * Get the project database ID.
     *
     * @return The project database ID.
     */
    public int getProjectDatabaseId() {
        return projectKey.getDBID();
    }

    /**
     * Set the deleted status.
     *
     * @param deleted The new deleted status.
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * Check if the dataset is deleted or not.
     *
     * @return true if the dataset is deleted, else false.
     */
    public boolean isDeleted() {
        return deleted;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        super.serialiseMe(outputStream);
        outputStream.writeString(projectName);
        outputStream.writeInt(projectOwnerId);
        outputStream.writeBoolean(deleted);
    }

    @Override
    public String toString() {
        return projectName;
    }

    @Override
    protected int getClassID() {
        return ISerializableTypes.PROJECT_DATA_ID;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ProjectDataSet) {
            return projectKey.equals(((ProjectDataSet) obj).getProjectKey());
        }
        return false;
    }

    @Override
    public int compareTo(ProjectDataSet o) {
        if (o.getProjectName().equals(projectName)) {
            return projectKey.compareTo(o.projectKey);
        }
        return projectName.toUpperCase().compareTo(o.projectName.toUpperCase());
    }

    public void setHidden(boolean hidden) {
        isHidden = hidden;
    }

    public boolean isHidden() {
        return isHidden;
    }
}
