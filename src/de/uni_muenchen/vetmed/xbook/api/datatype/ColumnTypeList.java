package de.uni_muenchen.vetmed.xbook.api.datatype;

import java.util.ArrayList;

public final class ColumnTypeList extends ArrayList<ColumnType> {

    private final String tableName;

    public ColumnTypeList(String tableName) {
        this.tableName = tableName;
    }

    public String getTableName() {
        return tableName;
    }

    @Override
    public boolean add(ColumnType columnType) {
        return super.add(columnType);
    }
}
