package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.network.ISerializableTypes;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;

import java.io.IOException;

/**
 * A DataSetOld is the complete data set of a entry.
 *
 * Each dataset is unique by the project key and the database name. Each entry
 * can hold several data rows out of several tables.
 * 
 * The EntryDataSet also holds the Key of the entry and the base table name.
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class EntryDataSet extends DataSetOld {

    /**
     * The key for the entry.
     */
    protected Key entryKey;


    /**
     * Constructor.
     *
     * Initialises the object. Tge input unit is the base table name.
     *
     * @param entryKey The Key of the entry.
     * @param projectKey The Key of the project,
     * @param databaseName The name of the database.
     * @param baseTableName The name of the base table name.
     */
    public EntryDataSet(Key entryKey, Key projectKey, String databaseName, String baseTableName) {
        super(projectKey, databaseName,baseTableName);
        this.entryKey = entryKey;
    }

    /**
     * Constructor.
     *
     * Initialises the entry data set with serialized data.
     *
     * @param is An object implementing an input interface to be able to handle
     * the serialized data.
     * @throws IOException Thrown on errors while deserializing.
     */
    public EntryDataSet(SerialisableInputInterface is) throws IOException {
        super(is);
        entryKey = (Key) is.deserialize();
    }

    @Override
    protected int getClassID() {
        return ISerializableTypes.ENTRYDATA_ID;
    }

    /**
     * Get the Key of the entry.
     *
     * @return The entry key.
     */
    public Key getEntryKey() {
        return entryKey;
    }

    /**
     * Set a new entry key to the entry data set.
     *
     * @param entryKey The new entry key.
     */
    public void setEntryKey(Key entryKey) {
        this.entryKey = entryKey;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        super.serialiseMe(outputStream);
        entryKey.serialize(outputStream);
    }


}
