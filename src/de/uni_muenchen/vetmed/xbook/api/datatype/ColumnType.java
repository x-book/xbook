package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IDefaultSectionAssignments;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;

import java.util.*;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 * @author Alex
 */
public final class ColumnType implements Comparable<ColumnType> {

    public static final ColumnType NULL = new ColumnType("should_not_be_used", Type.VALUE, ExportType.NONE);
    public static final ColumnType ITTABLE = new ColumnType("should_not_be_used_in_ittable", Type.VALUE,
            ExportType.NONE);

    /**
     * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
     */
    public static enum SortedBy {

        ID, ALPHABETICAL, ORDER_IN_DATABASE
    }

    /**
     * Defines the different options of the type.
     */
    public enum Type {

        VALUE,
        DATE,
        VALUE_THESAURUS, //extra parameter?
        ID,
        HIERARCHIC, //extra parameter?
        ID_SPECIFIC, //remove if possible
        SUBQUERY_SPECIFIC, //remove if possible
        BOOLEAN,
        YES_NO_NONE,
        UNKNOWN,
        EXTERN_VALUE,
        EXTERN_ID,
        EXTERN_MULTILAYER,//remove if possible
        TIME,
        FILE
    }

    public enum ExportType {

        ALL, GENERAL, SPECIFIC, NONE, CROSS_LINKED
    }

    private boolean isSort = false;
    /**
     * Defines if the values that are mapped are translated.
     */
    private boolean isLanguage = false;
    /**
     * The column name, as it is saved in the database
     */
    private String columnName;
    /**
     * Defines in which Section in the GUI this element shall be displayed.
     */
    private SectionAssignment sectionProperty = IDefaultSectionAssignments.SECTION_NOT_ASSIGNED;
    /**
     * The type of the column e.g. a String or Integer value, or a ID for a value saved in another table
     */
    private Type type;
    /**
     * Defines for which export settings this column shall be exported.
     */
    private ExportType exportType;
    /**
     * If the column is a ID, this value defines the name of the table where the mapping is stored.
     */
    private String connectedTableName;
    /**
     * List of columns that shall also be exported or listed if this column is retrieved.
     */
    private ColumnType[] additionalColumns = {};
    /**
     * The display name of the column.
     */
    private String displayName;

    private String displayNameGeneral = null;
    /**
     * The display name in HTML
     */
    private String htmlDisplayName;
    private String htmlDisplayNameGeneral;
    /**
     * For complex queries to retrieve the value this can be defined here.
     */
    private String whereQuery;
    /**
     * The order in which the columns shall be shown. Higher priority means further right, lower means left.
     */
    private int priority = 999;
    /**
     * If multiple groups with the same priority exist, this can be used to further specify the order.
     */
    private int groupNumber = 0;
    /**
     * If group number cannot be used, a string id can be used additionally to distinguish different groups.
     */
    private int grouptID = 0;
    /**
     * If multiple elements inside a group exist, with this the order inside the groups can be defined.
     */
    private int inSubgroupPriority = 0;
    /**
     * Defines the maximum characters that can be entered inside the input fields.
     */
    private Integer maxInputLength = null;
//    /**
//     * The column header of the column type. USed for analysis.
//     */
//    private final ColumnHeader columnHeader;
    /**
     * Defines if this column type is used for the listing only.
     */
    private boolean forListing = false;

    private boolean unique = false;

    /**
     * Defines if the input into this field is mandatory.
     */
    private boolean isMandatory = false;

    /**
     * Defines the way in which the entries of this column are sorted.
     */
    private SortedBy sortedBy = SortedBy.ALPHABETICAL;

    private boolean hiddenInListing = false;

    private String selectQuery;

    private String specificWhereQuery =
            " AND " + getConnectedTableName() + "." + IStandardColumnTypes.ID + "=" + getColumnName();

    /**
     * Defines if the column shall be shown in the project search.
     */
    private boolean showInProjectSearch = false;
    /**
     * Defines if this column can be edited with the multi edit option.
     */
    private boolean isMultiEditAllowed = true;

    private boolean hideInVisibilityList = false;
    /**
     * Indicates if the entry should be shortened in the listing (with amount of entries or place holder for long
     * texts.
     */
    private boolean shortenedInListing = false;
    /**
     * Indicates if the entry should be shortened in the export (with amount of entries or place holder for long texts.
     */
    private boolean shortenedInExport = false;
    /**
     * Indicated if the display of a hierarchic value is displayed with the full hierarchy from root to leave, or only
     * the leave value itself.
     */
    private boolean hierarchyDisplayed = false;

    private String tableSidebarText = null;

    /**
     * Indicates if the input field is used in a default input mask panel or in the project edit mask. The value can
     * then be used to individually handle special characteristics in the behaviour of the field.
     */
    private boolean isProjectEditMask = false;

//    /**
//     * Creates a ColumnType object without any data.
//     * <p/>
//     * <b>IMPORTANT:</b> Every ColumnType object MUST have at least a column
//     * name, a type and an export type!
//     *
//     * @deprecated
//     */
//    @Deprecated
//    public ColumnType() {
//        columnHeader = new ColumnHeader(displayName, columnName, NominalScale.class, DataType.String, this);
//    }


    /**
     * Creates a ColumnType object without any data.
     * <p/>
     * <b>IMPORTANT:</b> Every ColumnType object MUST have at least a column
     * name, a type and an export type!
     *
     * @deprecated
     */
    @Deprecated
    public ColumnType() {
//        columnHeader = new ColumnHeader(displayName, columnName, NominalScale.class, DataType.String, this);
    }

    /**
     * Creates a ColumnType object with basic data.
     *
     * @param columnName The column name.
     * @param type       The type.
     * @param exportType The export type.
     */
    public ColumnType(String columnName, Type type, ExportType exportType) {
        this.columnName = columnName;
        this.displayName = columnName;
        this.type = type;
        this.exportType = exportType;
//        columnHeader = new ColumnHeader(displayName, columnName, NominalScale.class, DataType.String, this);

    }

    /**
     * <b>SHOULD NOT BE USED ANYMORE</b>
     */
    @Deprecated
    public ColumnType(String columnName, String displayName, SectionAssignment sectionProperty, Type type,
                      ExportType exportType, int priority) {
        this.columnName = columnName;
        this.displayName = displayName;
        this.sectionProperty = sectionProperty;
        this.type = type;
        this.exportType = exportType;
        this.priority = priority;

//        columnHeader = new ColumnHeader(displayName, columnName, NominalScale.class, DataType.String, this);
    }

    /**
     * <b>SHOULD NOT BE USED ANYMORE</b>
     */
    @Deprecated
    public ColumnType(String columnName, String displayName, SectionAssignment sectionProperty, Type type,
                      ExportType exportType, String tableName, int priority) {
        this.columnName = columnName;
        this.displayName = displayName;
        this.sectionProperty = sectionProperty;
        this.type = type;
        this.exportType = exportType;
        this.connectedTableName = tableName;
        this.priority = priority;

//        columnHeader = new ColumnHeader(displayName, columnName, NominalScale.class, DataType.String, this);
    }

    /**
     * Creates a new ColumnType object with the data of an existing one, but adds an additional table name for the
     * column.
     *
     * @param oldColumnType                The old ColumnType object.
     * @param additionalTableNameForColumn The additional table name for the column
     */
    public ColumnType(ColumnType oldColumnType, String additionalTableNameForColumn) {
        columnName = additionalTableNameForColumn + "." + ColumnHelper.removeDatabaseName(oldColumnType.columnName);

        sectionProperty = oldColumnType.sectionProperty;
        type = oldColumnType.type;
        exportType = oldColumnType.exportType;
        connectedTableName = oldColumnType.connectedTableName;
        additionalColumns = oldColumnType.additionalColumns;
        displayName = oldColumnType.displayName;
        whereQuery = oldColumnType.whereQuery;
        priority = oldColumnType.priority;
        unique = oldColumnType.unique;
        isMandatory = oldColumnType.isMandatory;
        maxInputLength = oldColumnType.maxInputLength;


//        if (columnName.toLowerCase().equals("projectid")
//                || columnName.toLowerCase().contains("databasenumber")
//                || columnName.toLowerCase().contains("projectdatabasenumber")
//                || columnName.toLowerCase().contains("id")) {
//
//            columnHeader = new ColumnHeader(displayName, columnName, IntervalScale.class, DataType.Integer, this);
//
//        } else {
//
//            columnHeader = new ColumnHeader(displayName, columnName, NominalScale.class, DataType.String, this);
//        }
    }

    private ColumnType(ColumnType copy) {
        columnName = copy.columnName;

        sectionProperty = copy.sectionProperty;
        type = copy.type;
        exportType = copy.exportType;
        connectedTableName = copy.connectedTableName;
        additionalColumns = copy.additionalColumns;
        displayName = copy.displayName;
        whereQuery = copy.whereQuery;
        priority = copy.priority;
        unique = copy.unique;
        isMandatory = copy.isMandatory;
        maxInputLength = copy.maxInputLength;


//        if (columnName.toLowerCase().equals("projectid")
//                || columnName.toLowerCase().contains("databasenumber")
//                || columnName.toLowerCase().contains("projectdatabasenumber")
//                || columnName.toLowerCase().contains("id")) {
//
//            columnHeader = new ColumnHeader(displayName, columnName, IntervalScale.class, DataType.Integer, this);
//
//        } else {
//
//            columnHeader = new ColumnHeader(displayName, columnName, NominalScale.class, DataType.String, this);
//        }
    }

    private Collection<? extends String> additionalTables = new ArrayList<>();


    public ColumnType copy() {
        return new ColumnType(this);
    }

    /*
     * ***************************************************************
     * Setter *******************************************************
     * **************************************************************
     */
    public final ColumnType setColumnName(String columnName) {
        this.columnName = columnName;
        if (displayName == null) {
            this.displayName = columnName;
//            columnHeader.setDisplayName(displayName);

        }
        return this;
    }

    public final ColumnType setDisplayName(String displayName) {
        this.displayName = displayName;
//        columnHeader.setDisplayName(displayName);
        return this;
    }

    public final ColumnType setDisplayNameGeneral(String displayNameGeneral) {
        this.displayNameGeneral = displayNameGeneral;
        return this;
    }

    public final ColumnType setHtmlDisplayName(String htmlDisplayName) {
        this.htmlDisplayName = htmlDisplayName;
        return this;
    }

    public final ColumnType setHtmlDisplayNameGeneral(String htmlDisplayNameGeneral) {
        this.htmlDisplayNameGeneral = htmlDisplayNameGeneral;
        return this;
    }

    public final ColumnType setSectionProperty(SectionAssignment sectionProperty) {
        this.sectionProperty = sectionProperty;
        return this;
    }

    public final ColumnType setMandatory(boolean isMandatory) {
        this.isMandatory = isMandatory;
        return this;
    }

    public final ColumnType setMandatory() {
        return setMandatory(true);
    }

    public final ColumnType setAdditionalTables(Collection<? extends String> additionalTables) {
        this.additionalTables = additionalTables;
        return this;
    }

    public final ColumnType setType(Type type) {
        this.type = type;
        return this;
    }

    public final ColumnType setSelectQuery(String selectQuery) {
        this.selectQuery = selectQuery;
        return this;
    }

    public final ColumnType setExportType(ExportType validExport) {
        this.exportType = validExport;
        return this;
    }

    public final ColumnType setForListing(boolean forListing) {
        this.forListing = forListing;
        return this;
    }

    public final ColumnType setConnectedTableName(String connectedTableName) {
        this.connectedTableName = connectedTableName;
        return this;
    }

    public final ColumnType setPriority(int priority) {
        this.priority = priority;
        return this;
    }

    public final ColumnType setPriority(int priority, int groupID, int groupNumber, int inSubgroupPriority) {
        this.priority = priority;
        this.groupNumber = groupNumber;
        this.grouptID = groupID;
        this.inSubgroupPriority = inSubgroupPriority;
        return this;
    }

    public final ColumnType setPriority(int priority, int groupID, int inSubgroupPriority) {
        this.priority = priority;
        this.grouptID = groupID;
        this.inSubgroupPriority = inSubgroupPriority;
        return this;
    }

    public final ColumnType setWhereQuery(String whereQuery) {
        this.whereQuery = whereQuery;
        return this;
    }

    public final ColumnType setUnique(boolean unique) {
        this.unique = unique;
        return this;
    }

    public final ColumnType setAdditionalColumns(ColumnType... additionalColumns) {
        this.additionalColumns = additionalColumns;
        return this;
    }

    public final ColumnType setSortedBy(SortedBy sortedBy) {
        this.sortedBy = sortedBy;
        return this;
    }

    public final ColumnType setMaxInputLength(int maxInputLength) {
        this.maxInputLength = maxInputLength;
        return this;
    }

    public final ColumnType setHiddenInListing(boolean hiddenInListing) {
        this.hiddenInListing = hiddenInListing;
        return this;
    }

    public final ColumnType setSpecificWhereQuery(String specificWhereQuery) {
        this.specificWhereQuery = specificWhereQuery;
        return this;
    }

//    public ColumnType setScale(Class<? extends NominalScale> scale) {
//        columnHeader.setScale(scale);
//        return this;
//    }
//
//    public ColumnType setDataType(DataType dataType) {
//        columnHeader.setDataType(dataType);
//        return this;
//    }

    public ColumnType setIsSort(boolean isSort) {
        this.isSort = isSort;
        return this;
    }

    public ColumnType setIsLanguage(boolean isLanguage) {
        this.isLanguage = isLanguage;
        return this;
    }

    public ColumnType setShowInProjectSearch(boolean showInProjectSearch) {
        this.showInProjectSearch = showInProjectSearch;
        return this;
    }

    public ColumnType setHideInVisibilityList(boolean hideInVisibilityList) {
        this.hideInVisibilityList = hideInVisibilityList;
        return this;
    }

    /*
     * ***************************************************************
     * Getter *******************************************************
     * **************************************************************
     */
//    public ColumnHeader getColumnHeader() {
//
//        return columnHeader;
//    }

    public final ColumnType[] getAdditionalColumns() {
        return additionalColumns;
    }

    public final String getColumnName() {
        return columnName;
    }

    public final String getDisplayName() {
        if (displayName == null) {
            return getColumnName();
        }
        return displayName;
    }

    public final String getDisplayNameGeneral() {
        if (displayNameGeneral == null) {
            return getDisplayName();
        }
        return displayNameGeneral;
    }

    public final String getHtmlDisplayName() {
        if (htmlDisplayName == null || htmlDisplayName.isEmpty()) {
            return getDisplayName();
        }
        return htmlDisplayName;
    }

    public final String getHtmlDisplayNameGeneral() {
        if (htmlDisplayNameGeneral == null || htmlDisplayNameGeneral.isEmpty()) {
            return getDisplayNameGeneral();
        }
        return htmlDisplayNameGeneral;
    }

    public final int getPriority() {
        return priority;
    }

    public final String getConnectedTableName() {
        return connectedTableName;
    }

    public final SectionAssignment getSectionProperty() {
        return sectionProperty;
    }

    public final Type getType() {
        return type;
    }

    public final boolean isUnique() {
        return unique;
    }

    public final boolean isMandatory() {
        return isMandatory;
    }

    public final ExportType getExportType() {
        return exportType;
    }

    public final boolean isForListing() {
        return forListing;
    }

    public final String getWhereQuery() {
        return whereQuery;
    }

    public final String getSpecificWhere() {
        return specificWhereQuery;
    }

    public final SortedBy getSortedBy() {
        return sortedBy;
    }

    public final String getSelectQuery() {
        return selectQuery;
    }

    public final Integer getMaxInputLength() {
        return maxInputLength;
    }

    public final boolean isNonDefaultField() {
        return hiddenInListing;
    }

    public boolean isSort() {
        return isSort;
    }

    public boolean isLanguage() {
        return isLanguage;
    }

    public boolean showInProjectSearch() {
        return showInProjectSearch;
    }

    /*
     * ***************************************************************
     * Other methods ************************************************
     * **************************************************************
     */
    @Override
    public final String toString() {
        return columnName;
    }

    public static Comparator<ColumnType> comp = new Comparator<ColumnType>() {
        @Override
        public int compare(ColumnType t, ColumnType t1) {
            return t.compareTo(t1);
        }
    };

    public static Comparator<ColumnType> displayComp = new Comparator<ColumnType>() {
        @Override
        public int compare(ColumnType t, ColumnType t1) {
            return t.compareToName(t1);
        }
    };

    public final boolean isIdentical(ColumnType other) {
        return (sectionProperty != null || other.sectionProperty == null)
                && !(sectionProperty != null && other.sectionProperty == null)
                && !(sectionProperty != null && !sectionProperty.equals(other.sectionProperty))
                && !(type != null && other.type != null && !type.equals(other.type))
                && !(exportType != null && other.exportType != null && !exportType.equals(other.exportType))
                && !(connectedTableName == null && other.connectedTableName != null)
                && !(connectedTableName != null && other.connectedTableName == null)
                && !(connectedTableName != null && !connectedTableName.equals(other.connectedTableName))
                && Arrays.equals(additionalColumns, other.additionalColumns)
                && displayName.equals(other.displayName)
                && displayNameGeneral.equals(other.displayNameGeneral)
                && htmlDisplayName.equals(other.htmlDisplayName)
                && htmlDisplayNameGeneral.equals(other.htmlDisplayNameGeneral)
                && !(whereQuery == null && other.whereQuery != null)
                && !(whereQuery != null && other.whereQuery == null)
                && !(whereQuery != null && !whereQuery.equals(other.whereQuery))
                && priority == other.priority
                && unique == other.unique
                && isMandatory == other.isMandatory
                && shortenedInExport == other.shortenedInExport
                && shortenedInListing == other.shortenedInListing
                && hierarchyDisplayed == other.hierarchyDisplayed
                && showInProjectSearch == other.showInProjectSearch
                && hiddenInListing == other.hiddenInListing;
    }

    @Override
    public int compareTo(ColumnType t) {
        if (t.priority == priority) {
            if (grouptID == (t.grouptID)) {

                if (groupNumber == t.groupNumber) {
                    if (inSubgroupPriority == t.inSubgroupPriority) {
                        if (ColumnHelper.getTableName(columnName).isEmpty() || ColumnHelper.getTableName(t.columnName).isEmpty()) {
                            return ColumnHelper.removeDatabaseName(columnName).toUpperCase().compareTo(ColumnHelper.removeDatabaseName(t.columnName).toUpperCase());
                        }
                        return columnName.toUpperCase().compareTo(t.columnName.toUpperCase());
                    }
                    return inSubgroupPriority - t.inSubgroupPriority;
                }
                return groupNumber - t.groupNumber;
            }
            return grouptID - (t.grouptID);
        }

        return priority - t.priority;
    }

    public int compareToName(ColumnType t) {
        return displayName.toUpperCase().compareTo(t.displayName.toUpperCase());
    }

    @Override
    public final boolean equals(Object obj) {
        if (obj instanceof String) {
            return columnName.equals(obj);
        } else if (obj instanceof ColumnType) {
            ColumnType other = (ColumnType) obj;
            //only compare columNames without tableNames if one of them is empty. Otherwise they might be different
            if (ColumnHelper.getTableName(columnName).isEmpty() || ColumnHelper.getTableName(other.columnName).isEmpty()) {
                return ColumnHelper.removeDatabaseName(columnName).equals(ColumnHelper.removeDatabaseName(other.columnName));
            } else {
                return columnName.equals(other.columnName);
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.columnName);
        hash = 83 * hash + Objects.hashCode(this.connectedTableName);
        return hash;
    }

    public static class SectionAssignment {

        private final int sortedId;
        private final String displayName;

        public SectionAssignment(int sortedId, String displayName) {
            this.sortedId = sortedId;
            this.displayName = displayName;
        }

        public String getDisplayName() {
            return displayName;
        }

        public Integer getSortedId() {
            return sortedId;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof SectionAssignment)) {
                return false;
            }
            SectionAssignment other = (SectionAssignment) obj;
            return other.displayName.equals(displayName) && sortedId == other.sortedId;
        }
    }

    public final String getDefaultValue() {
        switch (type) {
            case BOOLEAN:
                return "0";
            case YES_NO_NONE:
            case ID:
            case ID_SPECIFIC:
            case EXTERN_ID:
            case EXTERN_MULTILAYER:
            case HIERARCHIC:
                return "-1";
            case DATE:
                return "0001-01-01";
            case TIME:
                return "00:00:00";
            case EXTERN_VALUE:
            case VALUE:
            case VALUE_THESAURUS:
                return "";
            case UNKNOWN:
            case SUBQUERY_SPECIFIC:
            default:
                return "";
        }
    }

    public Collection<? extends String> getAdditionalTables() {
        return additionalTables;
    }

    /**
     * Set the displayed text in the export to shortened.
     * <p>
     * That means: Either the text displays how many entries are available, or the text is shortened with a place
     * holder.
     *
     * @param shortened Whether the text should be shortened in the export, or not.
     * @return The column type itself.
     */
    public ColumnType setShortenedInExport(boolean shortened) {
        this.shortenedInExport = shortened;
        return this;
    }

    /**
     * Set the displayed text in the listing to shortened.
     * <p>
     * That means: Either the text displays how many entries are available, or the text is shortened with a place
     * holder.
     *
     * @param shortened Whether the text should be shortened in the listing, or not.
     * @return The column type itself.
     */
    public ColumnType setShortenedInListing(boolean shortened) {
        this.shortenedInListing = shortened;
        return this;
    }

    /**
     * Set the displayed text in the listing and in the export to shortened.
     * <p>
     * That means: Either the text displays how many entries are available, or the text is shortened with a place
     * holder.
     *
     * @param shortened Whether the text should be shortened in the listing and in the export, or not.
     * @return The column type itself.
     */
    public ColumnType setShortened(boolean shortened) {
        setShortenedInExport(shortened);
        setShortenedInListing(shortened);
        return this;
    }

    /**
     * Returns if the displayed text should be shortened in the export.
     * <p>
     * That means: Either the text displays how many entries are available, or the text is shortened with a place
     * holder.
     *
     * @return <code>true</code> if the text should be shortened in the export.
     */
    public boolean isShortenedInExport() {
        return shortenedInExport;
    }

    /**
     * Returns if the displayed text should be shortened in the listing.
     * <p>
     * That means: Either the text displays how many entries are available, or the text is shortened with a place
     * holder.
     *
     * @return <code>true</code> if the text should be shortened in the listing.
     */
    public boolean isShortenedInListing() {
        return shortenedInListing;
    }

    /**
     * Set if the display of a hierarchic value is displayed with the full hierarchy from root to leave, or only the
     * leave value itself.
     *
     * @param hierarchyDisplayed <code>true</code> to display the full path from
     *                           root to leave of the value, <code>false</code> to display only the leave value.
     * @return The column type itself.
     */
    public ColumnType setHierarchyDisplayed(boolean hierarchyDisplayed) {
        this.hierarchyDisplayed = hierarchyDisplayed;
        return this;
    }

    /**
     * Returns if the display of a hierarchic value is displayed with the full hierarchy from root to leave, or only the
     * leave value itself.
     *
     * @return <code>true</code> to display the full path from root to leave.
     */
    public boolean isHierarchyDisplayed() {
        return hierarchyDisplayed;
    }

    public boolean isMultiEditAllowed() {
        return isMultiEditAllowed;
    }

    public ColumnType setMultiEditAllowed(boolean isMultiEditAllowed) {
        this.isMultiEditAllowed = isMultiEditAllowed;
        return this;
    }

    public boolean isHideInVisibilityList() {
        return hideInVisibilityList;
    }

    public String getTableSidebarText() {
        return tableSidebarText;
    }

    public ColumnType setTableSidebarText(String tableSidebarText) {
        this.tableSidebarText = tableSidebarText;
        return this;
    }

    /**
     * Sets the indicator if the input field is used in a default input mask panel or in the project edit mask.
     *
     * @param projectEditMask The information if the input field is used in the project edit mask.
     */
    public ColumnType setProjectEditMask(boolean projectEditMask) {
        isProjectEditMask = projectEditMask;
        return this;
    }

    /**
     * Get the indicator if the input field is used in a default input mask panel or in the project edit mask.
     *
     * @return The information if the input field is used in the project edit mask.
     */
    public boolean isProjectEditMask() {
        return isProjectEditMask;
    }
}
