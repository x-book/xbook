package de.uni_muenchen.vetmed.xbook.api.datatype.right;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class Group {

    private final String name;
    private final int id;

    public Group(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
