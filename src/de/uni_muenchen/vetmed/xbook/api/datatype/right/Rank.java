package de.uni_muenchen.vetmed.xbook.api.datatype.right;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Rank {

    private final String name;
    private final int id;

    public Rank(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
