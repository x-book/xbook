package de.uni_muenchen.vetmed.xbook.api.datatype.right;

import de.uni_muenchen.vetmed.xbook.api.network.ISerializableTypes;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class RightsInformation extends Serialisable {

    /**
     * The displayed name of the user/group/etc.
     */
    private final String name;
    /**
     * The id of the user/group/etc.
     */
    private final int id;
    /**
     * A list of rights for the user/group/etc.
     */
    private final ArrayList<Rights> rights;
    /**
     * Indicated if a row was changed or not.
     */
    // private boolean changed = false;

    /**
     * Constructor.
     *
     * @param name   The displayed name of the user/group/etc.
     * @param id     The id of the user/group/etc.
     * @param rights A list of rights for the user/group/etc.
     */
    public RightsInformation(String name, int id, ArrayList<Rights> rights) {
        this.name = name;
        this.rights = rights;
        this.id = id;
    }

    public RightsInformation(SerialisableInputInterface is) throws IOException {
        id = is.readInt();
        name = is.readString();

        int numberOfEntries = is.readInt();
        rights = new ArrayList<>();
        for (int i = 0; i < numberOfEntries; i++) {
            rights.add(((Rights) is.deserialize()));
        }
    }

    /**
     * Returns the rights for the user/group/etc.
     *
     * @return The rights.
     */
    public ArrayList<Rights> getRights() {
        return rights;
    }

    /**
     * Returns the name for the user/group/etc.
     *
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the id for the user/group/etc.
     *
     * @return The id.
     */
    public int getID() {
        return id;
    }

    @Override
    protected int getClassID() {
        return ISerializableTypes.RIGHTS_INFORMATION;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeString("" + id);
        outputStream.writeInt(rights.size());
        for (Rights right : rights) {
            right.serialize(outputStream);
        }
    }

    /**
     * Returns if the user/group/etc has any rights, or not.
     *
     * @return The status if the user/group/etc. has any right.
     */
    public boolean hasAnyRight() {
        return hasAnyRight(rights);
    }

    /**
     * Returns if the user/group/etc. has has any of the specific rights, or not.
     *
     * @return The status if the user/group/etc. has any of the the specific rights.
     */
    public static boolean hasAnyRight(ArrayList<Rights> rightList) {
        for (Rights right : rightList) {
            if (right.hasRight()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the status if the row has been changed.
     *
     * @return The status if the row has been changed.
     */
//    public boolean isChanged() {
//        return changed;
//    }

    /**
     * Set the information if the row has changed.
     *
     * @param changed The status if the row has changed.
     */
//    public void setChanged(boolean changed) {
//        this.changed = changed;
//    }
}
