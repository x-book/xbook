package de.uni_muenchen.vetmed.xbook.api.datatype.right;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.api.network.ISerializableTypes;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.TableManager;

import java.io.IOException;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Rights extends Serialisable {

    /**
     * The ColumnType of the right.
     */
    private final ColumnType column;
    /**
     * Status, if the right is granted or not.
     */
    private final boolean hasRight;

    /**
     * Constructor.
     *
     * @param column   The ColumnType of the right.
     * @param hasRight Status, if the right is granted or not
     */
    public Rights(ColumnType column, boolean hasRight) {
        this.column = column;
        this.hasRight = hasRight;
    }

    /**
     * Constructor.
     *
     * @param columnName The ColumnType of the right.
     * @param hasRight   Status, if the right is granted or not
     */
    public Rights(String columnName, boolean hasRight) {
        column = new ColumnType(columnName, ColumnType.Type.VALUE, ColumnType.ExportType.NONE);
        column.setDisplayName(Loc.get(columnName.toUpperCase()));
        this.hasRight = hasRight;
    }


    public Rights(SerialisableInputInterface is) throws IOException {
        this(ColumnHelper.removeDatabaseName(is.readString()), is.readBoolean());
    }


    /**
     * Return the ColumnType of the right.
     *
     * @return The ColumnType of the right.
     */
    public ColumnType getColumnType() {
        return column;
    }

    /**
     * Returns the status if the right is granted or not.
     *
     * @return The status if the right is granted or not.
     */
    public boolean hasRight() {
        return hasRight;
    }

    @Override
    protected int getClassID() {
        return ISerializableTypes.RIGHTS_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeString(column.getColumnName());
        outputStream.writeBoolean(hasRight);
    }
}
