package de.uni_muenchen.vetmed.xbook.api.datatype;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class SearchEntryInfo {


    /**
     * The logger object of this class.
     */
    private static final Log LOGGER = LogFactory.getLog(SearchEntryInfo.class);

    /**
     * An enumeration that holds the available search modes.
     */
    public enum SearchMode {
        CONTAINS, EQUALS
    }

    private final String text;
    private final SearchMode currentSearchMode;

    public SearchEntryInfo(String s) {
        this.text = s;
        this.currentSearchMode = SearchMode.EQUALS;
    }

    public SearchEntryInfo(String text, SearchMode currentSearchMode) {

        this.text = text;
        this.currentSearchMode = currentSearchMode;
    }

    public SearchMode getCurrentSearchMode() {
        return currentSearchMode;
    }

    public String getText() {
        return text;
    }
}
