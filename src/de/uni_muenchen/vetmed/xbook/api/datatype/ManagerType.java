package de.uni_muenchen.vetmed.xbook.api.datatype;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public enum ManagerType {

    ENTRY, PROJECT

}
