package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractCrossLinkedManager;

/**
 * @author Johannes Lohrer
 */
public class CrossLinkedEntry {

    private final Key entry;
    private final Key deletedMatch;
    private Key projectKey;
    private final AbstractBaseEntryManager managerOfEntry;
    private final AbstractCrossLinkedManager abstractCrossLinkedManager;
    private final String displayOfOrignalEntry;
    private final String displayOfDeletedEntry;

    public CrossLinkedEntry(Key entry, Key deletedMatch, Key projectKey, AbstractBaseEntryManager managerOfEntry,
                            AbstractCrossLinkedManager abstractCrossLinkedManager,
                            String displayOfOrignalEntry, String displayOfDeletedEntry) {

        this.entry = entry;
        this.deletedMatch = deletedMatch;
        this.projectKey = projectKey;
        this.managerOfEntry = managerOfEntry;
        this.abstractCrossLinkedManager = abstractCrossLinkedManager;
        this.displayOfOrignalEntry = displayOfOrignalEntry;
        this.displayOfDeletedEntry = displayOfDeletedEntry;
    }

    public AbstractBaseEntryManager getManagerOfEntry() {
        return managerOfEntry;
    }

    public Key getDeletedMatch() {
        return deletedMatch;
    }

    public String getDisplayOfDeletedEntry() {
        return displayOfDeletedEntry;
    }

    public String getDisplayOfOrignalEntry() {
        return displayOfOrignalEntry;
    }

    public AbstractCrossLinkedManager getAbstractCrossLinkedManager() {
        return abstractCrossLinkedManager;
    }

    public Key getEntry() {
        return entry;
    }

    public Key getProjectKey() {
        return projectKey;
    }
}
