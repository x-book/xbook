package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType.Type;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * A DataRow holds a single row out of the database.
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DataRow extends HashMap<String, String> {

  /**
   * The name of the table.
   */
  private final String tableName;

  /**
   * Indicator if a image is uploaded.
   */
  private boolean image = false;

  public DataRow(String tableName) {
    this.tableName = tableName;
  }


  /**
   * Adds a given DataColumn to the DataRow.
   *
   * @param dataColumn The DataColumn to add.
   * @deprecated use put instead
   */
  @Deprecated
  public void add(DataColumn dataColumn) {
    put(ColumnHelper.removeDatabaseName(dataColumn.getColumnName()), dataColumn.getValue());
  }

  /**
   * Returns whether the current row and the given row have the same primaryKeys e.g. define the
   * same entry.
   *
   * @param other The DataRow that should be compared.
   * @param primaryKeys The primary values.
   * @return true if the primary keys are equal, else false.
   */
  public boolean isPrimaryKeyEqual(DataRow other, ArrayList<String> primaryKeys) {
    if (primaryKeys == null) {
      return false;
    }
    for (String type : primaryKeys) {
      String thisValue = get(type);
      String otherValue = other.get(type);

      if (thisValue == null || otherValue == null || !thisValue.equals(otherValue)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Returns if the data row is deleted.
   *
   * @return true if the data row is deleted, else false.
   */
  public boolean isDeleted() {
    String value = get(tableName + "." + IStandardColumnTypes.DELETED.getColumnName());
    return value == null || value.equals(IStandardColumnTypes.DELETED_YES);
  }


  public String put(ColumnType columnType, int value) {
    return put(columnType, value + "");
  }


  /**
   * Puts a new values for a specific ColumnType.
   *
   * @param columnType The ColumnType.
   * @param value The value.
   * @return the previous value associated with <tt>key</tt>, or
   * <tt>null</tt> if there was no mapping for <tt>key</tt>. (A <tt>null</tt>
   * return can also indicate that the map previously associated <tt>null</tt> with <tt>key</tt>.)
   */
  public String put(ColumnType columnType, String value) {
    if (columnType.getType() == ColumnType.Type.DATE) {
      if ((value == null || value.isEmpty()) && !columnType.isMandatory()) {
        //lets try to not input data...
        value = columnType.getDefaultValue();
      }
    } else if (value == null && (columnType.getType() == Type.ID
        || columnType.getType() == Type.HIERARCHIC)) {

      value = columnType.getDefaultValue();
    }
    return super.put(ColumnHelper.removeDatabaseName(columnType.getColumnName()), value);
  }

  @Override
  public String put(String key, String value) {
    return super.put(ColumnHelper.removeDatabaseName(key), value);
  }

  /**
   * Get the column name of a fiven ColumnType.
   *
   * @param columnType The ColumnType.
   * @return The column name.
   */
  public String get(ColumnType columnType) {
    String columnName = ColumnHelper.removeDatabaseName(columnType.getColumnName());

    return get(columnName);
  }

  @Override
  public String get(Object key) {
    return super.get(ColumnHelper.removeDatabaseName(key.toString()));
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof DataRow)) {
      return false;
    }

    DataRow other = (DataRow) o;

    //since we have some columns that we don'T care about, at least check if there is ANY data available...
    if (size() == 0 && other.size() != 0) {
      return false;
    }
    for (String key : keySet()) {
      if (key.equals("" + IStandardColumnTypes.STATUS) || key
          .equals("" + IStandardColumnTypes.MESSAGE_NUMBER)) {
        continue;
      }
      if (key.equals("" + "zusatzFeld1") || key.equals("" + "zusatzFeld2") || key.equals(
          "" + "zusatzFeld3") || key.equals("" + "zusatzFeld4")) {
        continue;
      }
      String thisValue = get(key);
      String otherValue = other.get(key);

      if (thisValue == null || otherValue == null) {
        if (thisValue == null && otherValue == null) {
          continue;
        }
        if (thisValue == null && (otherValue.equals("") || otherValue.equals("-1"))) {
          continue;
        }
        if (otherValue == null && (thisValue.equals("") || thisValue.equals("-1"))) {
          continue;
        }
        return false;
      }

      if (!thisValue.equals(otherValue)) {
        if ((thisValue.equals("0000-00-00") && otherValue.equals("")) || (otherValue.equals(
            "0000-00-00") && thisValue.equals(""))) {
          continue;
        }
        try {
          Double dV = Double.valueOf(thisValue);
          Double dVo = Double.valueOf(otherValue);
          if (dV.equals(dVo)) {
            continue;
          }
        } catch (NumberFormatException ex) {
        }
        return false;
      }
    }

    return true;
  }

  @Override
  public int hashCode() {
    return super.hashCode();

  }


  public DataRowIterator iterator() {
    return new DataRowIterator();
  }

  public boolean valuesEqual(DataRow row) {
    for (Map.Entry<String, String> entrySet : entrySet()) {
      String key = ColumnHelper.removeDatabaseName(entrySet.getKey());
      if (IStandardColumnTypes.ID.equals(key) || IStandardColumnTypes.DATABASE_ID.equals(key) ||
          IStandardColumnTypes.PROJECT_ID.equals(key) || IStandardColumnTypes.PROJECT_DATABASE_ID
          .equals(
              key) ||
          IStandardColumnTypes.STATUS.equals(key) || IStandardColumnTypes.DELETED.equals(
          key) || IStandardColumnTypes.MESSAGE_NUMBER.equals(key)) {
        continue;
      }
      if (!entrySet.getValue().equals(row.get(entrySet.getKey()))) {
        return false;
      }
    }
    return true;
  }

  /**
   * Indicates if this data row has an image
   */
  public void setImage(boolean image) {
    this.image = image;
  }

  public boolean isImage() {
    return image;
  }

  public boolean containsAll(DataRow upData) {
    for (Entry<String, String> entry : upData.entrySet()) {
      String value = entry.getValue();
      String anObject = get(entry.getKey());
      if ((value == null && anObject != null) || (value != null && !value.equals(anObject))) {
        return false;
      }
    }
    return true;
  }

  /**
   * A specific Iterator for DataRow objects.
   */
  public class DataRowIterator implements Iterator<DataColumn>, Iterable<DataColumn> {

    private final Iterator<Map.Entry<String, String>> entryIt;

    /**
     * Constructor.
     */
    public DataRowIterator() {
      entryIt = entrySet().iterator();
    }

    @Override
    public boolean hasNext() {
      return entryIt.hasNext();
    }

    @Override
    public DataColumn next() {
      Map.Entry<String, String> entry = entryIt.next();
      return new DataColumn(entry.getValue(), entry.getKey());
    }

    @Override
    public void remove() {
      entryIt.remove();
    }

    @Override
    public Iterator<DataColumn> iterator() {
      return this;
    }
  }
}
