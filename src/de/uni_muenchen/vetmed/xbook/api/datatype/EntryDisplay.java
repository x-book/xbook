package de.uni_muenchen.vetmed.xbook.api.datatype;

/**
 * A helper class that holds the the id, database id and any representing text
 * for the data.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class EntryDisplay {

    private final String id;
    private final String databaseId;
    private final String displayText;

    /**
     * Constructor.
     *
     * Initialised the id, database id and the display text.
     *
     * @param id The ID of the entry.
     * @param databaseId The database ID of the entry.
     * @param displayText The display text of the entry.
     */
    public EntryDisplay(String id, String databaseId, String displayText) {
        this.id = id;
        this.databaseId = databaseId;
        this.displayText = displayText;
    }

    /**
     * Get the ID of the entry.
     *
     * @return The ID.
     */
    public String getID() {
        return id;
    }

    /**
     * Get the database ID of the entry.
     *
     * @return The database ID.
     */
    public String getDatabaseId() {
        return databaseId;
    }

    /**
     * Get the display text of the entry.
     *
     * @return The display text.
     */
    public String getDisplayText() {
        return displayText;
    }

    @Override
    public String toString() {
        return displayText;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EntryDisplay) {
            EntryDisplay other = (EntryDisplay) obj;
            return other.id.equals(id) && other.databaseId.equals(databaseId);
        }
        return false;
    }

}
