package de.uni_muenchen.vetmed.xbook.api.datatype;

import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class ExportColumn {

    private ArrayList<String> data;
    private String sep = " | ";

    ExportColumn(boolean unique) {
        if (unique) {
            data = new ArrayList<>();
        } else {
            data = new UniqueArrayList<>();
        }
    }

    public void setSeperator(String sep) {
        this.sep = sep;
    }

    ExportColumn() {
        this.data = new UniqueArrayList<>();
    }

    public void addData(ExportColumn other) {

        data.addAll(other.data);

    }

    public void setData(String value) {
        this.data.clear();
        data.add(value);
    }

    public void addData(String newData) {
        data.add(newData);
    }

    public ArrayList<String> getData() {
        return data;
    }

    public String getStringData() {
        String string = "";
        for (String string1 : data) {
            if (string1 != null && !string1.isEmpty()) {
                string += string1 + sep;
            }
        }
        if (string.endsWith(sep)) {
            return string.substring(0, string.length() - sep.length());
        }
        return string;
    }

    @Override
    public String toString() {
        return getStringData();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof String) {
            return getStringData().equals(obj);
        }
        if (!(obj instanceof ExportColumn)) {
            return false;
        }
        ExportColumn other = (ExportColumn) obj;
        return data.equals(other.data);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.data);
        return hash;
    }

    public boolean containsValue(String value) {
        return data.contains(value);
    }
}
