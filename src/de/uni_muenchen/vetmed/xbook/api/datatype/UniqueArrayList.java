package de.uni_muenchen.vetmed.xbook.api.datatype;

import java.util.ArrayList;
import java.util.Collection;

/**
 * An ArrayList that only hold items once. There won't be Two identical items in
 * the list.
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 * @param <T> The type of the items in the list.
 */
public class UniqueArrayList<T> extends ArrayList<T> {

    @Override
    public boolean addAll(Collection<? extends T> c) {
        if (c == null) {
            return true;
        }
        for (T obj : c) {
            if (!this.contains(obj)) {
                add(obj);
            }
        }

        return true;
    }

    @Override
    public boolean add(T e) {
        if (!contains(e)) {
            return super.add(e);
        }
        return false;
    }
}
