package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.network.ISerializableTypes;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;

import java.io.IOException;

/**
 * Class that holds specific information of user profiles.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class User extends Serialisable {

    private final int uid;
    private final String userName;
    private final String displayName;
    private final String organisation;
    private final boolean allowPublicSearch;

    /**
     * Constructor.
     * <p>
     * Initialises the UserProfile object and assign the values.
     *
     * @param uid               The user id
     * @param userName          The user name.
     * @param displayName       The display name.
     * @param organisation      The organisation name.
     * @param allowPublicSearch Whether the user is allowed to be found in
     *                          public search or not.
     */
    public User(int uid, String userName, String displayName, String organisation, boolean allowPublicSearch) {
        this.uid = uid;
        this.userName = userName;
        this.displayName = displayName;
        this.organisation = organisation;
        this.allowPublicSearch = allowPublicSearch;
    }

    /**
     * Get the id of the user.
     *
     * @return The user id.
     */
    public int getId() {
        return uid;
    }

    /**
     * Get the name of the user.
     *
     * @return The user name.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Get the display name of the user.
     *
     * @return The user display name.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Get the organisation of the user.
     *
     * @return The organisation.
     */
    public String getOrganisation() {
        return organisation;
    }

    /**
     * Get the status if the user want to be searchable or not.
     *
     * @return Whether the user is allowed to be found in public search or not.
     */
    public boolean isAllowPublicSearch() {
        return allowPublicSearch;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof User)) {
            return false;
        }
        User u = (User) obj;
        return (u.getId() == uid
                && u.getUserName().equals(userName)
                && u.getDisplayName().equals(displayName)
                && u.getOrganisation().equals(organisation)
                && u.isAllowPublicSearch() == allowPublicSearch);
    }

    @Override
    protected int getClassID() {
        return ISerializableTypes.USER_PROFILE_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeString(uid + "");
        outputStream.writeString(displayName);
        outputStream.writeString(organisation);
        outputStream.writeBoolean(allowPublicSearch);
    }
}
