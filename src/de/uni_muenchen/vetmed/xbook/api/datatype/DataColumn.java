package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.ISerializableTypes;
import java.io.IOException;
import java.util.Objects;

/**
 * The DataColumn class makes it possible to assign any data to a specific
 * database column.
 *
 * In the first instance objects of this class are used to communicate between
 * GUI and logic of the application.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class DataColumn extends Serialisable {

    /**
     * The column name in the database.
     */
    private String columnName;
    /**
     * The object holding the data.
     */
    private String value;

    /**
     * Constructor.
     *
     * @param value The data as an Object.
     * @param id The id, eg the column name or the id the value has
     */
    public DataColumn(String value, String id) {
        this.value = value == null ? "" : value;
        this.columnName = ColumnHelper.removeDatabaseName(id);
    }

    /**
     * Constructor.
     *
     * @param value The data as an Object.
     * @param id The id, eg the column name or the id the value has
     */
    public DataColumn(String value, ColumnType id) {
        this.value = value == null ? "" : value;
        this.columnName = ColumnHelper.removeDatabaseName(id.getColumnName());
    }

    /**
     * Constructor.
     *
     * @param value The data as an Object.
     * @param id The id, eg the column name or the id the value has
     */
    public DataColumn(int value, String id) {
        this.value = "" + value;
        this.columnName = ColumnHelper.removeDatabaseName(id);
    }

    /**
     * Constructor.
     *
     * @param value The data as an Object.
     * @param id The id, eg the column name or the id the value has
     */
    public DataColumn(int value, ColumnType id) {
        this.value = "" + value;
        this.columnName = ColumnHelper.removeDatabaseName(id.getColumnName());
    }

    /**
     * Constructor.
     *
     * @param is An object implementing an input interface to be able to handle
     * the serialized data.
     */
    public DataColumn(SerialisableInputInterface is) throws IOException {
        value = is.readString();
        columnName = ColumnHelper.removeDatabaseName(is.readString());
    }

    /**
     * Returns the column name in the database..
     *
     * @return The column name.
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * Returns the data.
     *
     * @return The data as an Vector of Objects.
     */
    public String getValue() {
        return value;
    }

    /**
     * Set the column name for the value.
     *
     * @param columnName The column name for the value
     */
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    @Override
    public String toString() {
        return value;
    }

    /**
     * Alternative to the toString() Method, that prints the column name and the
     * value.
     *
     * @return A String formatted [columnName]|[value]
     */
    public String print() {
        return columnName + " | " + value;
    }

    /**
     * Set new data to the column.
     *
     * @param value The data to set.
     */
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    protected int getClassID() {
        return ISerializableTypes.DATAHASH_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeString(value != null ? value : "");
        outputStream.writeString(columnName);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof DataColumn)) {
            return false;
        }
        DataColumn other = ((DataColumn) obj);
        return columnName.equals(other.columnName) && value.equals(other.value);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.columnName);
        hash = 79 * hash + Objects.hashCode(this.value);
        return hash;
    }
}
