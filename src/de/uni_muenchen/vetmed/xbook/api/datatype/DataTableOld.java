package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Johannes Lohrer <johanneslohrer@msn.com>
 */
public class DataTableOld extends ArrayList<DataRow> {

  private String tableName;

  private boolean whereAllEntriesLoaded = true;

  /**
   * Creates a new DataTable with the given tableName
   *
   * @param tableName the name of the table to be represented inside this object
   */
  public DataTableOld(String tableName) {
    if (tableName == null) {
      throw new RuntimeException("TABLENAME NULL ");
    }

    this.tableName = tableName;
  }

  /**
   * Returns the name of the Table
   *
   * @return The name of the Table
   */
  public String getTableName() {
    return tableName;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof DataTableOld)) {
      return false;
    }
    return ((DataTableOld) o).tableName.equals(tableName);
  }

  public DataTableOld getEqualContent(DataTableOld other) {
    DataTableOld tableOld = new DataTableOld(tableName);
    if (other.size() != size()) {
      return tableOld;
    }
    for (DataRow dataRow : this) {
      boolean match = false;
      for (DataRow row : other) {
        if (dataRow.valuesEqual(row)) {
          tableOld.add(row);
          match = true;
          break;
        }

      }
      if (!match) {
        return new DataTableOld(tableName);
      }
    }
    return tableOld;
  }

  public boolean datasetsEqual(DataTableOld other, ArrayList<String> prim) {
    if (!other.tableName.equals(tableName)) {
      return false;
    }
    DataTableOld thisCopy = copy();
    DataTableOld otherCopy = other.copy();

    for (DataRow row : this) {
      boolean foundOther = false;
      for (DataRow otherRow : other) {
        if (row.isPrimaryKeyEqual(otherRow, prim)) {
          foundOther = true;
          if (row.equals(otherRow)) {
            thisCopy.remove(row);
            otherCopy.remove(otherRow);
          } else {
            if (AbstractConfiguration.DISPLAY_SOUTS) {
              System.out.println("not equal: " + row);
              System.out.println("other: " + otherRow);
            }
            return false;
          }
          break;
        }
      }
      if (!foundOther) {
        if (row.isDeleted()) {
          thisCopy.remove(row);
        } else {
          if (AbstractConfiguration.DISPLAY_SOUTS) {
            System.out.println("Corresponding row not found and not deleted");
          }
          return false;
        }
      }
    }

    for (DataRow d : otherCopy) {
      if (!d.isDeleted()) {
        if (AbstractConfiguration.DISPLAY_SOUTS) {
          System.out.println("Corresponding local row not found and not deleted");
        }
        return false;
      }
    }
    for (DataRow d : thisCopy) {
      if (!d.isDeleted()) {
        if (AbstractConfiguration.DISPLAY_SOUTS) {
          System.out.println("Corresponding global row not found and not deleted");
        }
        return false;
      }
    }
    if (AbstractConfiguration.DISPLAY_SOUTS) {
      System.out.println("dataset equals true");
    }
    return true;
  }

  public boolean isWhereAllEntriesLoaded() {
    return whereAllEntriesLoaded;
  }

  public void setWhereAllEntriesLoaded(boolean whereAllEntriesLoaded) {
    this.whereAllEntriesLoaded = whereAllEntriesLoaded;
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 23 * hash + Objects.hashCode(this.tableName);
    return hash;
  }

  @Override
  public boolean isEmpty() {
    return (super.isEmpty()
        //either it has no entries or it has only one entry and this entry is empty
        || (size() == 1 && get(0).isEmpty()));
  }

  @Override
  public String toString() {
    return tableName;
  }

  public DataTableOld copy() {
    DataTableOld copy = new DataTableOld(tableName);
    for (DataRow b : this) {
      copy.add((DataRow) b.clone());
    }
    return copy;
  }

  public DataRow addRow() {
    DataRow row = new DataRow(tableName);
    add(row);
    return row;
  }

  @Override
  public boolean add(DataRow e) {
    return super.add(e);
  }

  public boolean add(ArrayList<DataColumn> e) {
    DataRow row = new DataRow(tableName);
    for (DataColumn d : e) {
      row.add(d);

    }
    return add(row);
  }

  public DataRow getRow(DataRow upData) {

    for (DataRow dataRow : this) {
      if (dataRow.containsAll(upData)) {
        return dataRow;
      }
    }
    return null;
  }
}
