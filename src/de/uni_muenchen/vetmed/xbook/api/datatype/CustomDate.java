package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.exception.InvalidDateException;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class CustomDate {

    private final Integer year;
    private final Integer month;
    private final Integer day;

    /**
     * Creates a new Date object.
     *
     * @param year The year as Integer.
     * @param month The month as Integer (null allowed)
     * @param day The day as Integer (null allowed)
     */
    public CustomDate(Integer year, Integer month, Integer day) throws InvalidDateException {
        this.year = year;
        this.month = month;
        this.day = day;

        if (!isValidDate()) {
            throw new InvalidDateException(parseToDatebaseDate());
        }
    }

    /**
     * Creates a new Date object.
     *
     * @param year The year as a String.
     * @param month The month as String (null allowed)
     * @param day The day as String (null allowed)
     */
    public CustomDate(String year, String month, String day) throws InvalidDateException {
        this(
                (year == null || year.isEmpty()) ? 0 : Integer.parseInt(year),
                (month == null || month.isEmpty()) ? 0 : Integer.parseInt(month),
                (day == null || day.isEmpty()) ? 0 : Integer.parseInt(day)
        );
    }

    /**
     * Creates a new Date object from the date format YYYY-MM-DD.
     *
     * @param yyyymmdd The date to be parsed from the formal YYYY-MM-DD.
     */
    public CustomDate(String yyyymmdd) throws InvalidDateException {
        String[] dateArray = yyyymmdd.split("-");

        try {
            if (dateArray[0].equals("0000") || dateArray[0].equals("000") || dateArray[0].equals("00") || dateArray[0].equals("0")) {
                year = null;
            } else {
                year = Integer.parseInt(dateArray[0]);
            }

            if (dateArray[1].equals("00") || dateArray[1].equals("0")) {
                month = null;
            } else {
                month = Integer.parseInt(dateArray[1]);
            }

            if (dateArray[2].equals("00") || dateArray[2].equals("0")) {
                day = null;
            } else {
                day = Integer.parseInt(dateArray[2]);
            }

            if (!isValidDate()) {
                throw new InvalidDateException(parseToDatebaseDate());
            }
        } catch (NumberFormatException nfe) {
            throw new InvalidDateException(parseToDatebaseDate());
        }
    }

    public static CustomDate tryParseDate(String string) throws InvalidDateException {
        String[] parts = string.split("\\.");
        String year;
        String month = "0";
        String day = "0";
        if (parts.length == 3) {
            year = parts[2];
            month = parts[1];
            day = parts[0];
        } else if (parts.length == 2) {
            year = parts[1];
            month = parts[0];
        } else if (parts.length == 1) {
            year = parts[0];
        } else {
            throw new InvalidDateException(string);
        }
        return new CustomDate(year.trim() + "-" + month.trim() + "-" + day.trim());
    }

    public Integer getYear() {
        return year;
    }

    public Integer getMonth() {
        return month;
    }

    public Integer getDay() {
        return day;
    }

    /**
     * Converts the date to a string of the format DD.MM.YYYY.
     *
     * @return The date in the format DD.MM.YYYY.
     */
    public String parseToReadableDate() {
        String s = "Invalid Date";

        if (year == null) {
            return s;
        }
        s = StringHelper.addLeadingZeros(year, 4);

        if (month == null || month == 0) {
            return s;
        }
        s = StringHelper.addLeadingZeros(month, 2) + "." + s;

        if (day == null || day == 0) {
            return s;
        }
        s = StringHelper.addLeadingZeros(day, 2) + "." + s;

        return s;
    }

    /**
     * Converts the date to a string of the format YYYY-MM-DD.
     *
     * @return The date in the format YYYY-MM-DD.
     */
    public String parseToDatebaseDate() {
        String s = "";
        if (year == null) {
            s = "0000";
        } else {
            s = StringHelper.addLeadingZeros(year, 4);
        }

        if (month == null) {
            s += "-00";
        } else {
            s += "-" + StringHelper.addLeadingZeros(month, 2);
        }

        if (day == null) {
            s += "-00";
        } else {
            s += "-" + StringHelper.addLeadingZeros(day, 2);
        }

        return s;
    }

    /**
     * Checks if the date is a valid one. The format "YYYY", "YYYY-MM" and
     * "YYYY-MM-DD" (without month and/or day) is also a valid date to support
     * only the setting of a year and year/month.
     *
     * @param year The year as Integer.
     * @param month The month as Integer.
     * @param day The day as Integer.
     * @return true if it is a valid date.
     */
    public static boolean isValidDate(Integer year, Integer month, Integer day) {
        if (year == null || year == 0) {
            return false;
        }
        // change the parameters if they are null...
        // to allow "YYYY", "YYYY-MM" and "YYYY-MM-DD" being valid dates
        if (month == null || month == 0) {
            month = 1;
        }
        if (day == null || day == 0) {
            day = 1;
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setLenient(false);
        try {
            String s = StringHelper.addLeadingZeros(year, 4) + "-" + StringHelper.addLeadingZeros(month, 2) + "-" + StringHelper.addLeadingZeros(day, 2);
            Date d = format.parse(s);
        } catch (ParseException ex) {
            return false;
        }

        return true;
    }

    /**
     * Checks if the date is a valid one. The format "YYYY", "YYYY-MM" and
     * "YYYY-MM-DD" (without month and/or day) is also a valid date to support
     * only the setting of a year and year/month.
     *
     * @param year The year as String.
     * @param month The month as String.
     * @param day The day as String.
     * @return true if it is a valid date.
     */
    public static boolean isValidDate(String year, String month, String day) {
        return isValidDate(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day));
    }

    /**
     * Checks if the date is a valid one. The format "YYYY", "YYYY-MM" and
     * "YYYY-MM-DD" (without month and/or day) is also a valid date to support
     * only the setting of a year and year/month.
     *
     * @return true if it is a valid date.
     */
    public boolean isValidDate() {
        return isValidDate(year, month, day);
    }

    public Integer toIntegerValue() {
        String d;
        if (year == null) {
            d = "-1";
        } else if (month == null) {
            d = StringHelper.addLeadingZeros(year, 4) + "00" + "00";
        } else if (day == null) {
            d = StringHelper.addLeadingZeros(year, 4) + StringHelper.addLeadingZeros(month, 2) + "00";
        } else {
            d = StringHelper.addLeadingZeros(year, 4) + StringHelper.addLeadingZeros(month, 2) + StringHelper.addLeadingZeros(day, 2);
        }
        return Integer.parseInt(d);
    }

    public int compareTo(CustomDate begin) {
        return toIntegerValue() - begin.toIntegerValue();
    }

    /**
     * Checks if any data is inserted, that means no year is inserted at all.
     *
     * @return
     */
    public boolean isEmpty() {
        return (year == null || year == 0) && (month == null || month == 0) && (day == null || day == 0);
    }

}
