package de.uni_muenchen.vetmed.xbook.api.datatype;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;

/**
 * A variation of a HashMap where it is possible to assign several values to one
 * single key.
 *
 * All values of a specific key are saved in one ArrayList that can be returned
 * when getting the values.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 * @param <K> The tyoe of the key.
 * @param <V> The type of the values.
 */
public class TreeListMap<K, V> {

    /**
     * The HashMap that holds all data.
     */
    private final TreeMap<K, ArrayList<V>> map;

    /**
     * Constructor.
     *
     * Creates an empty HashMap.
     */
    public TreeListMap() {
        map = new TreeMap<>();
    }

    /**
     * Adds a value to the list of values of a specific key.
     *
     * If no key is available a new ArrayList is created and the value is added.
     *
     * @param key The key that should link to the added value.
     * @param value The value to add.
     */
    public void add(K key, V value) {
        if (!map.containsKey(key)) {
            map.put(key, new ArrayList<V>());
        }
        map.get(key).add(value);
    }

    /**
     * Adds several values to the list of values of a specific key.
     *
     * If no key is available a new ArrayList is created and the value is added.
     *
     * @param key The key that should link to the added value.
     * @param values The values to add.
     */
    public void add(K key, ArrayList<V> values) {
        for (V value : values) {
            add(key, value);
        }
    }

    /**
     * Returns the values to which the specified key is mapped, or null if this
     * map contains no mapping for the key.
     *
     * @param key The key whose associated value is to be returned
     * @return The values to which the specified key is mapped, or null if this
     * map contains no mapping for the key
     */
    public ArrayList<V> getValues(K key) {
        return map.get(key);
    }

    /**
     * Returns <code>true</code> if this map contains a mapping for the
     * specified key.
     *
     * @param key The key whose presence in this map is to be tested
     * @return <code>true</code> if this map contains a mapping for the
     * specified key.
     */
    public boolean containsKey(K key) {
        return map.containsKey(key);
    }

    /**
     * Returns a Collection view of the values contained in this map.
     *
     * The collection's iterator returns the values in ascending order of the
     * corresponding keys.
     *
     * @return A collection view of the values contained in this map, sorted in
     * ascending key order
     */
    public Collection<ArrayList<V>> values() {
        return map.values();
    }

    /**
     * Removes all of the mappings from this map. The map will be empty after
     * this call returns.
     */
    public void clear() {
        map.clear();
    }

}
