package de.uni_muenchen.vetmed.xbook.api.datatype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A variation of a HashMap where it is possible to assign several values to one
 * single key.
 * <p>
 * All values of a specific key are saved in one ArrayList that can be returned
 * when getting the values.
 *
 * @param <K> The tyoe of the key.
 * @param <V> The type of the values.
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class HashListMap<K, V> {

    /**
     * The HashMap that holds all data.
     */
    private final HashMap<K, ArrayList<V>> list;

    /**
     * Constructor.
     * <p>
     * Creates an empty HashMap.
     */
    public HashListMap() {
        list = new HashMap<>();
    }

    /**
     * Adds a value to the list of values of a specific key.
     * <p>
     * If no key is available a new ArrayList is created and the value is added.
     *
     * @param key   The key that should link to the added value.
     * @param value The value to add.
     */
    public void add(K key, V value) {
        if (!list.containsKey(key)) {
            list.put(key, new ArrayList<V>());
        }
        list.get(key).add(value);
    }

    /**
     * Adds several values to the list of values of a specific key.
     * <p>
     * If no key is available a new ArrayList is created and the value is added.
     *
     * @param key    The key that should link to the added value.
     * @param values The values to add.
     */
    public void add(K key, ArrayList<V> values) {
        for (V value : values) {
            add(key, value);
        }
    }

    /**
     * Returns the values to which the specified key is mapped, or null if this
     * map contains no mapping for the key.
     *
     * @param key The key whose associated value is to be returned
     * @return The values to which the specified key is mapped, or null if this
     * map contains no mapping for the key
     */
    public ArrayList<V> getValues(K key) {
        return list.get(key);
    }

    public Set<Map.Entry<K, ArrayList<V>>> entrySet() {
        return list.entrySet();
    }

    /**
     * Returns <code>true</code> if this map contains a mapping for the
     * specified key.
     *
     * @param key The key whose presence in this map is to be tested
     * @return <code>true</code> if this map contains a mapping for the
     * specified key.
     */
    public boolean containsKey(K key) {
        return list.containsKey(key);
    }

}
