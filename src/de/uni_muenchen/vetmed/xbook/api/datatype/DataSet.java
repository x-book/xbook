package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.network.ISerializableTypes;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

/**
 * A DataSet is the complete data set.
 *
 * Each dataset is unique by the project key and the database name. Each entry
 * can hold several data rows out of several tables.
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DataSet extends Serialisable {

    /**
     * The Key of the project.
     */
    protected Key projectKey;
    /**
     * The name of the database.
     */
    protected String databaseName;
    /**
     * The necessary tables for the data set.
     */
    protected ArrayList<DataTable> tablesList;

    /**
     * Constructor.
     *
     * Initialises the project key and the database name. The tables list is
     * empty.
     *
     * @param projectKey The unique project key of the data set.
     * @param databaseName The database name.
     */
    public DataSet(Key projectKey, String databaseName) {
        this.projectKey = projectKey;
        this.databaseName = databaseName;
        tablesList = new ArrayList<>();
    }

    /**
     * Constructor.
     *
     * Initialises the data set with serialized data.
     *
     * @param is An object implementing an input interface to be able to handle
     * the serialized data.
     * @throws IOException Thrown on errors while deserializing.
     */
    public DataSet(SerialisableInputInterface is) throws IOException {
        projectKey = (Key) is.deserialize();
        databaseName = is.readString();
        tablesList = new ArrayList<>();
        int size = is.readInt();
        for (int i = 0; i < size; i++) {
            DataTable table = new DataTable(is);
            tablesList.add(table);
        }
    }

    /**
     * Get the DataTable for a given table name.
     *
     * @param tableName The table name which corresponding DataTable should be
     * returned.
     * @return The DataTable for the given table name.
     */
    public DataTable getDataTable(String tableName) {
        for (DataTable entry : tablesList) {
            if (entry.getTableName().equals(tableName) || entry.getTableName().equals(databaseName + "." + tableName)) {
                return entry;
            }
        }
        return addDataTable(new DataTable(tableName));
    }

    /**
     * Get the ProjectRow for a given table name.
     *
     * @param tableName The table name which corresponding ProjectRow should be
     * returned.
     * @return The ProjectRow for the given table name.
     */
    public DataRow getProjectRow(String tableName) {
        for (DataTable entry : tablesList) {
            if (entry.getTableName().equals(tableName) || entry.getTableName().equals(databaseName + "." + tableName)) {
                return entry.get(0);
            }
        }
        DataTable table = addDataTable(new DataTable(tableName));
        table.add(new DataRow(tableName));
        return table.get(0);
    }

    /**
     * Adds a new EntryTable to the table list.
     *
     * @param table The DataTableOld to add.
     * @return The DataTableOld object that was added.
     */
    public DataTable addDataTable(DataTable table) {
        if (table == null) {
            throw new RuntimeException("TABLE NULLL!!!");
        }
        if (tablesList.contains(table)) {
            tablesList.remove(table);
        }
        tablesList.add(table);
        return table;
    }

    /**
     * Get the key of the project.
     *
     * @return The Key of the project.
     */
    public Key getProjectKey() {
        return projectKey;
    }

    /**
     * Get the name of the databse.
     *
     * @return The name of the database.
     */
    public String getDatabaseName() {
        return databaseName;
    }

    /**
     * Set a new project key to the DataSet.
     *
     * @param projectKey The Key of the project to add.
     */
    public void setProjectKey(Key projectKey) {
        this.projectKey = projectKey;
    }

    /**
     * Get the table list.
     *
     * @return The table list.
     */
    public ArrayList<DataTable> getTablesList() {
        return tablesList;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        projectKey.serialize(outputStream);
//        outputStream.writeString(databaseName);
//        outputStream.writeInt(tablesList.size());
//        for (DataTable table : tablesList) {
//            outputStream.writeString(table.getTableName());
//            outputStream.writeInt(table.size());
//
//            for (DataRow row : table) {
//                outputStream.writeInt(row.size());
//                for (DataColumn entry : row) {
//                    entry.serialize(outputStream);
//                }
//            }
//        }
    }

    @Override
    protected int getClassID() {
        return ISerializableTypes.DATA_SET_NEW_ID;
    }

    @Override
    public String toString() {
        String string = "CURRENT DATA:\n";
        for (DataTable table : tablesList) {
            string += table.getTableName() + "\n";
            for (DataRow column : table) {
                for (DataColumn hash : column.iterator()) {
                    string += hash + "\n";
                }
                string += "\n";
            }
        }
        return string;
    }

    /**
     * Checks if the DataTableOld has Entries.
     *
     * @return true if at least one entry exists, false otherwise.
     */
    public boolean hasEntries() {
        for (DataTable t : tablesList) {
            if (!t.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the current DataSetOld is equal to the given DataSetOld with
     * the given IBaseManager.
     *
     * @param other The other DataSetOld object that should be compared
     * with the current one.
     * @param iBase The manager that is responsible for the data.
     * @return true if the DataSetOld objects are equal, else false.
     * @throws NotLoggedInException Thrown when the user is not logged in.
     */
    public boolean isDatasetEqual(DataSet other, IBaseManager iBase) throws NotLoggedInException {
        for (DataTable table : tablesList) {
            if (!table.datasetsEqual(other.getDataTable(table.getTableName()), iBase.getPrimaryColumnsForTable(table.getTableName()))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.projectKey);
        hash = 11 * hash + Objects.hashCode(this.databaseName);
        hash = 11 * hash + Objects.hashCode(this.tablesList);
        return hash;
    }

    /**
     * Get a list of all table names.
     *
     * @return A list of all table names.
     */
    public ArrayList<String> getTableNames() {
        ArrayList<String> list = new ArrayList<>();
        for (DataTable table : tablesList) {
            list.add(table.getTableName());
        }
        return list;
    }

}
