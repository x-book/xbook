package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.network.ISerializableTypes;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;

import java.io.IOException;
import java.util.Objects;

/**
 * An EntryKey is a combination of a key of the entry and a key of the project.
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class EntryKey extends Serialisable {

    private Key entryKey;
    private final Key projectKey;

    /**
     * Constructor.
     * <p>
     * Initialises the entry key and project key.
     *
     * @param entryKey   The Key for the entry.
     * @param projectKey The Key for the project.
     */
    public EntryKey(Key entryKey, Key projectKey) {
        this.entryKey = entryKey;
        this.projectKey = projectKey;
    }

    /**
     * Constructor.
     * <p>
     * Initialises the entry key with serialized data.
     *
     * @param is An object implementing an input interface to be able to handle
     *           the serialized data.
     * @throws IOException Thrown on errors while deserializing.
     */
    public EntryKey(SerialisableInputInterface is) throws IOException {
        entryKey = (Key) is.deserialize();
        projectKey = (Key) is.deserialize();
    }

    /**
     * Get the entry key.
     *
     * @return The entry key.
     */
    public Key getEntryKey() {
        return entryKey;
    }

    /**
     * Get the project key.
     *
     * @return The project key.
     */
    public Key getProjectKey() {
        return projectKey;
    }

    /**
     * Set a new entry key to the EntryKey object.
     *
     * @param entryKey The new entry key.
     */
    public void setEntryKey(Key entryKey) {
        this.entryKey = entryKey;
    }

    /**
     * Creates a copy of the EntryKey object and returns it.
     *
     * @return The copy of the EntryKey object.
     */
    public EntryKey copy() {
        return new EntryKey(entryKey, projectKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof EntryKey)) {
            return false;
        }
        EntryKey other = (EntryKey) obj;

        boolean equals = Objects.equals(other.entryKey, entryKey);
        return equals && Objects.equals(other.projectKey, projectKey);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.entryKey);
        hash = 97 * hash + Objects.hashCode(this.projectKey);
        return hash;
    }

    @Override
    protected int getClassID() {
        return ISerializableTypes.ENTRYKEY_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        entryKey.serialize(outputStream);
        projectKey.serialize(outputStream);
    }

    @Override
    public String toString() {
        return entryKey + "|" + projectKey;
    }

}
