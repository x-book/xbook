package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.manager.ISynchronisationManager;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.network.ISerializableTypes;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

/**
 * A DataSet is the complete data set.
 * <p/>
 * Each dataset is unique by the project key and the database name. Each entry can hold several data
 * rows out of several tables.
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DataSetOld extends Serialisable {

  /**
   * The unique project key of the data set.
   */
  protected Key projectKey;
  /**
   * The database name.
   */
  protected String databaseName;
  /**
   * The necessary tables for the data set.
   */
  protected ArrayList<DataTableOld> tablesList;

  /**
   * The base table name where the entry is saved. E.g. input unit.
   */
  protected String baseTableName;


  protected boolean whereAllEntriesLoaded = true;

  /**
   * Constructor.
   * <p/>
   * Initialises the project key and the database name. The tables list is empty.
   *
   * @param projectKey The unique project key of the data set.
   * @param databaseName The database name.
   */
  public DataSetOld(Key projectKey, String databaseName, String baseTableName) {
    this.projectKey = projectKey;
    this.databaseName = databaseName;
    this.baseTableName = baseTableName;
    tablesList = new ArrayList<>();
  }

  /**
   * Constructor.
   * <p/>
   * Initialises the data set with serialized data.
   *
   * @param is An object implementing an input interface to be able to handle the serialized data.
   * @throws IOException Thrown on errors while deserializing.
   */
  public DataSetOld(SerialisableInputInterface is) throws IOException {
    projectKey = (Key) is.deserialize();
    databaseName = is.readString();
    tablesList = new ArrayList<>();
    int size = is.readInt();
    for (int i = 0; i < size; i++) {
      String name = is.readString();
      int tableSize = is.readInt();

      DataTableOld table = new DataTableOld(name);
      for (int j = 0; j < tableSize; j++) {
        DataRow data = new DataRow(name);
        int rowsize = is.readInt();
        for (int k = 0; k < rowsize; k++) {
          data.add((DataColumn) is.deserialize());
        }
        table.add(data);
      }
      tablesList.add(table);
    }
  }

  /**
   * Checks if the table list contains data for the given table name.
   *
   * @param tableName The table name to check.
   * @return true if data was found for the table name, else false.
   */
  public boolean hasDataTable(String tableName) {
    for (DataTableOld entry : tablesList) {
      if (entry.getTableName().equals(tableName) || entry.getTableName()
          .equals(databaseName + "." + tableName)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Search for the data table for the given table name and return it. Created a new one if no data
   * table is available for the given table name.
   *
   * @param tableName The table name to check.
   * @return The data table for the given table name or a new one if there is no data available for
   * the given table.
   */
  public DataTableOld getOrCreateDataTable(String tableName) {
    for (DataTableOld entry : tablesList) {
      if (entry.getTableName().equals(tableName) || entry.getTableName()
          .equals(databaseName + "." + tableName)) {
        return entry;
      }
    }
    return addDataTable(new DataTableOld(tableName));
  }


  /**
   * Adds a new EntryTable to the table list.
   *
   * @param table The DataTableOld to add.
   * @return The DataTableOld object that was added.
   */
  public DataTableOld addDataTable(DataTableOld table) {
    if (table == null) {
      throw new RuntimeException("TABLE NULLL!!!");
    }
    if (tablesList.contains(table)) {
      tablesList.remove(table);
    }
    tablesList.add(table);
    whereAllEntriesLoaded &= table.isWhereAllEntriesLoaded();
    return table;
  }

  /**
   * Get the project key.
   *
   * @return The project key.
   */
  public Key getProjectKey() {
    return projectKey;
  }

  /**
   * Get the name of the database.
   *
   * @return The database name.
   */
  public String getDatabaseName() {
    return databaseName;
  }

  /**
   * Set a new project key for the data set.
   *
   * @param projectKey The new project key.
   */
  public void setProjectKey(Key projectKey) {
    this.projectKey = projectKey;
  }

  /**
   * Get the table list.
   *
   * @return The table list.
   */
  public ArrayList<DataTableOld> getTablesList() {
    return tablesList;
  }

  @Override
  protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
    projectKey.serialize(outputStream);
    outputStream.writeString(databaseName);
    outputStream.writeInt(tablesList.size());
    for (DataTableOld table : tablesList) {
      outputStream.writeString(table.getTableName());
      outputStream.writeInt(table.size());

      for (DataRow row : table) {
        outputStream.writeInt(row.size());
        for (DataColumn entry : row.iterator()) {
          entry.serialize(outputStream);
        }
        outputStream.writeBoolean(row.isImage());
      }
    }
  }

  @Override
  protected int getClassID() {
    return ISerializableTypes.DATA_SET_ID;
  }

  @Override
  public String toString() {
    String string = "CURRENT DATA:\n";
    for (DataTableOld table : tablesList) {
      string += "Table: " + table.getTableName() + " ";
      for (DataRow column : table) {
        for (DataColumn hash : column.iterator()) {
          string += hash.print() + ",";
        }
        string += "; ";
      }
    }
    return string;
  }

  /**
   * Checks if the DataTableOld has Entries.
   *
   * @return true if at least one entry exists, false otherwise.
   */
  public boolean hasEntries() {
    for (DataTableOld t : tablesList) {
      if (!t.isEmpty()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Returns the number of entries saved in the data set object.
   *
   * @return The number of entries.
   */
  public int size() {
    // TODO
    return -1;
  }

  /**
   * Get the base table name.
   *
   * @return The base table name.
   */
  public String getBaseTableName() {
    return baseTableName;
  }

  /**
   * Checks if the current DataSetOld is equal to the given DataSetOld with the given IBaseManager.
   *
   * @param otherDataSet The other DataSetOld object that should be compared with the current one.
   * @param iBase The manager that is responsible for the data.
   * @return true if the DataSetOld objects are equal, else false.
   * @throws NotLoggedInException Thrown when the user is not logged in.
   */
  public boolean isDatasetEqual(DataSetOld otherDataSet, IBaseManager iBase)
      throws NotLoggedInException {
    for (DataTableOld table : tablesList) {
      if (!table.datasetsEqual(otherDataSet.getOrCreateDataTable(table.getTableName()),
          iBase.getPrimaryColumnsForTable(table.getTableName()))) {
        for (ISynchronisationManager abstractSynchronisationManager : iBase.getManagers()) {
          if (abstractSynchronisationManager.getTableName().equals(table.getTableName())) {
            if (abstractSynchronisationManager instanceof IBaseManager) {
              break;
            } else {
              return false;
            }
          }
        }

      }
    }
    return true;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 11 * hash + Objects.hashCode(this.projectKey);
    hash = 11 * hash + Objects.hashCode(this.databaseName);
    hash = 11 * hash + Objects.hashCode(this.tablesList);
    return hash;
  }

  /**
   * Get a list of all table names.
   *
   * @return A list of all table names.
   */
  public ArrayList<String> getTableNames() {
    ArrayList<String> list = new ArrayList<>();
    for (DataTableOld table : tablesList) {
      list.add(table.getTableName());
    }
    return list;
  }

  /**
   * Get a custom input unit as a data row.
   *
   * @param tableName The table name of the input unit.
   * @return A data row for the custom input unit.
   */
  public DataRow getDataRowForTable(String tableName) {
    DataTableOld table = getOrCreateDataTable(tableName);
    if (table == null) {
      table = new DataTableOld(tableName);
      table.add(new DataRow(tableName));
      tablesList.add(table);
    }
    if (table.isEmpty()) {
      table.add(new DataRow(tableName));
    }
    return table.get(0);
  }

  /**
   * Search for the data table for the given table name and return the first data row. Created a new
   * one if no data table is available for the given table name.
   *
   * @param tableName The table name to check.
   * @return The data row for the given table name or a new one if there is no data available for
   * the given table.
   */
  public DataRow getDataRow(String tableName) {
    for (DataTableOld entry : tablesList) {
      if (entry.getTableName().equals(tableName) || entry.getTableName()
          .equals(databaseName + "." + tableName)) {
        return entry.get(0);
      }
    }
    DataTableOld table = addDataTable(new DataTableOld(tableName));
    table.add(new DataRow(tableName));
    return table.get(0);
  }


  /**
   * Get a new input unit as a data row.
   *
   * @return A data row for the input unit.
   */
  public DataRow getInputUnit() {
    return getDataRowForTable(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT);
  }

  static public DataSetOld getCommonValues(ArrayList<? extends DataSetOld> dataSetOlds) {
    if (dataSetOlds.isEmpty()) {
      return null;
    }
    DataSetOld ref = dataSetOlds.get(0);
    DataSetOld returnValue = new DataSetOld(ref.getProjectKey(), ref.getDatabaseName(),
        ref.baseTableName);
//        returnValue.
    for (DataSetOld dataSetOld : dataSetOlds) {

    }

    return returnValue;
  }

  static public DataSetOld intersect(DataSetOld one, DataSetOld two) {
    String baseTableName = one.baseTableName;
    DataSetOld returnValue = new DataSetOld(one.getProjectKey(), one.getDatabaseName(),
        baseTableName);
    ArrayList<String> sharedNames = new ArrayList<>();
    for (String s : one.getTableNames()) {
      if (s.equals(baseTableName)) {
        continue;
      }
      if (two.getTableNames().contains(s)) {
        sharedNames.add(s);
      }
    }
    for (String sharedName : sharedNames) {
      DataTableOld dataTableOne = one.getOrCreateDataTable(sharedName);
      DataTableOld dataTableTwo = two.getOrCreateDataTable(sharedName);
      DataTableOld equalContent = dataTableOne.getEqualContent(dataTableTwo);
      if (!equalContent.isEmpty()) {
        returnValue.addDataTable(equalContent);
      }
    }
    DataRow inputUnitOne = one.getDataRowForTable(baseTableName);
    DataRow inputUnitTwo = two.getDataRowForTable(baseTableName);

    for (Map.Entry<String, String> values : inputUnitOne.entrySet()) {
      String valueTwo = inputUnitTwo.get(values.getKey());
      if (values.getValue().equals(valueTwo)) {
        returnValue.getDataRowForTable(baseTableName).put(values.getKey(), valueTwo);
      }
    }
    return returnValue;

  }

  public boolean isWhereAllEntriesLoaded() {
    return whereAllEntriesLoaded;
  }

  public void setWhereAllEntriesLoaded(boolean whereAllEntriesLoaded) {
    this.whereAllEntriesLoaded = whereAllEntriesLoaded;
  }
}
