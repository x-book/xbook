package de.uni_muenchen.vetmed.xbook.api.datatype;

/**
 * Class to calulate between units.
 *
 * Example: In general in databases values are saved in the smallest unit. To be
 * able to display converted units it's necessary to define the multiplicator to
 * the next unit: - 1000 g are 1 kg - 24 hours are 1 day - etc...
 *
 * This class can be used to save the unit as string ("g", "hour") and assign a
 * specific multiplicator value (1000, 24).
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Unit {

    private final String unit;
    private final int multiplicator;

    /**
     * Created a new unit object.
     *
     * @param unit The unit name as String
     * @param multiplicator The multiplicator value to the base unit.
     */
    public Unit(String unit, int multiplicator) {
        this.unit = unit;
        this.multiplicator = multiplicator;
    }

    /**
     * Gets the unit name as string.
     *
     * @return The unit name as string.
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Gets the multiplicator to the base unit.
     *
     * @return The multiplicator.
     */
    public int getMultiplicator() {
        return multiplicator;
    }

}
