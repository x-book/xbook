package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.network.ISerializableTypes;
import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;
import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * @author Johannes Lohrer <lohrer@cesonia.io>
 */
public class ProjectInformation extends Serialisable {

  private static final Log LOGGER = LogFactory.getLog(ProjectInformation.class);


  private ProjectDataSet project;
  private int numberOfEntries;
  private String lastUpdate;

  public ProjectInformation(SerialisableInputInterface is) throws IOException {
    project = is.deserialize();
    numberOfEntries = is.readInt();
    lastUpdate = is.readString();
  }


  public ProjectDataSet getProject() {
    return project;
  }

  public int getNumberOfEntries() {
    return numberOfEntries;
  }

  public String getLastUpdate() {
    return lastUpdate;
  }

  @Override
  protected int getClassID() {
    return ISerializableTypes.PROJECT_INFORMATION_ID;
  }

  @Override
  protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
    project.serialize(outputStream);
    outputStream.writeInt(numberOfEntries);
    outputStream.writeString(lastUpdate);
  }
}
