package de.uni_muenchen.vetmed.xbook.api.datatype;

import de.uni_muenchen.vetmed.xbook.api.network.Serialisable;
import de.uni_muenchen.vetmed.xbook.api.network.SerialisableInputInterface;
import de.uni_muenchen.vetmed.xbook.api.network.SerializableOutputInterface;

import java.io.IOException;
import java.util.*;

/**
 * @author Johannes Lohrer <johanneslohrer@msn.com>
 */
public class DataTable extends Serialisable implements List<DataRow> {

    private String tableName;
    private ArrayList<DataRow> rows = new ArrayList<>();

    /**
     * @param inputStream     */
    public DataTable(SerialisableInputInterface inputStream) throws IOException {
        this(inputStream, null);
    }

    /**
     *
     * @param inputStream
     * @param columnNames
     * @throws IOException
     */
    public DataTable(SerialisableInputInterface inputStream, ArrayList<String> columnNames) throws IOException {
        tableName = inputStream.readString();
        boolean updateColumnNames = inputStream.readBoolean();
        if (updateColumnNames) {
            if (columnNames == null) {
                columnNames = new ArrayList<>();
            }
            columnNames.clear();
            int nameSize = inputStream.readInt();
            for (int i = 0; i < nameSize; i++) {
                columnNames.add(inputStream.readString());
            }
        }
        int tableSize = inputStream.readInt();
        for (int j = 0; j < tableSize; j++) {
            DataRow data = new DataRow(tableName);
            int rowsize = inputStream.readInt();
            for (int k = 0; k < rowsize; k++) {
                data.put(columnNames.get(k), inputStream.readString());
            }
            add(data);
        }
        if (tableName == null) {
            throw new RuntimeException("TABLENAME NULL ");
        }

    }

    /**
     *
     * @param tableName
     */
    public DataTable(String tableName) {
        if (tableName == null) {
            throw new RuntimeException("TABLENAME NULL ");
        }

        this.tableName = tableName;
    }

    /**
     * Returns the name of the Table
     *
     * @return The name of the Table
     */
    public String getTableName() {
        return tableName;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof DataTable)) {
            return false;
        }
        return ((DataTable) o).tableName.equals(tableName);
    }

    public boolean datasetsEqual(DataTable other, ArrayList<String> prim) {
        if (!other.tableName.equals(tableName)) {
            return false;
        }
        DataTable thisCopy = copy();
        DataTable otherCopy = other.copy();

        for (DataRow row : this) {
            boolean foundOther = false;
            for (DataRow otherRow : other) {
                if (row.isPrimaryKeyEqual(otherRow, prim)) {
                    foundOther = true;
                    if (row.equals(otherRow)) {
                        thisCopy.remove(row);
                        otherCopy.remove(otherRow);
                    } else {
                        return false;
                    }
                    break;
                }
            }
            if (!foundOther) {
                if (row.isDeleted()) {
                    thisCopy.remove(row);
                } else {
                    return false;
                }
            }
        }

        for (DataRow d : otherCopy) {
            if (!d.isDeleted()) {
                return false;
            }
        }
        for (DataRow d : thisCopy) {
            if (!d.isDeleted()) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.tableName);
        return hash;
    }

    @Override
    public boolean isEmpty() {
        return (rows.isEmpty() //either it has no entries or it has only one entry and this entry is empty
                || (size() == 1 && get(0).isEmpty()));
    }

    @Override
    public String toString() {
        return tableName;
    }

    public DataTable copy() {
        DataTable copy = new DataTable(tableName);
        for (DataRow b : this) {

            copy.add((DataRow) b.clone());

        }
        return copy;
    }

    @Override
    public boolean add(DataRow e) {
        return rows.add(e); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean add(ArrayList<DataColumn> e) {
        DataRow row = new DataRow(tableName);
        for (DataColumn d : e) {
            row.add(d);

        }
        return add(row);
    }

    @Override
    protected int getClassID() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int size() {
        return rows.size();
    }

    @Override
    public boolean contains(Object o) {
        return rows.contains(o);
    }

    @Override
    public Iterator<DataRow> iterator() {
        return rows.iterator();
    }

    @Override
    public Object[] toArray() {
        return rows.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return rows.toArray(a);
    }

    @Override
    public boolean remove(Object o) {
        return rows.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return rows.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends DataRow> c) {
        return rows.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends DataRow> c) {
        return rows.addAll(index, c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return rows.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return rows.removeAll(c);
    }

    @Override
    public void clear() {
        rows.clear();
    }

    @Override
    public DataRow get(int index) {
        return rows.get(index);
    }

    @Override
    public DataRow set(int index, DataRow element) {
        return rows.set(index, element);
    }

    @Override
    public void add(int index, DataRow element) {
        rows.add(index, element);
    }

    @Override
    public DataRow remove(int index) {
        return rows.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return rows.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return rows.lastIndexOf(o);
    }

    @Override
    public ListIterator<DataRow> listIterator() {
        return rows.listIterator();
    }

    @Override
    public ListIterator<DataRow> listIterator(int index) {
        return rows.listIterator(index);
    }

    @Override
    public List<DataRow> subList(int fromIndex, int toIndex) {
        return rows.subList(fromIndex, toIndex);
    }
}

