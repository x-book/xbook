package de.uni_muenchen.vetmed.xbook.api.plugin;

/**
 *
 * @author Jojo
 */
public class PluginInformation {

    public enum PluginType {

        ACTIVATE,
        PASSIVE
    };

    /**
     * The Book this plung is made for
     */
    public enum Book {

        ALL,
        OSSOBOOK,
        ARCHAEOBOOK,
        ANTHROBOOK,
        EXCABOOK,
        INBOOK,
        DEMOBOOK,
        PALAEODEPOT,
        ANTHRODEPOT
    }

    private final String name;
    private final String tooltip;
    private final String path;
    private String actionCommand;
    private final double version;
    private final PluginType type;
    private final Book book;

    /**
     * Creates the Plugin Information for the Plugin. This should only be done
     * inside the Plugin as a return type.
     *
     * @param name The name of the Plugin as it is shown in the menu.
     * @param tooltip The Tooltip of the Plugin.
     * @param path The path to the plugin as it should be displayed in the
     * toolbar. Sub menues can be denoted with "/"
     * @param version The version of the Plugin.
     * @param type The type of the plugin. 
     * @param book The books the Plugin is valid for.
     */
    public PluginInformation(String name, String tooltip, String path, double version, PluginType type, Book book) {
        this.actionCommand = "";
        this.name = name;
        this.tooltip = tooltip;
        this.path = path;
        this.version = version;
        this.type = type;
        this.book = book;
    }

    /**
     * Returns the name of the Plugin
     *
     * @return the name of the Plugin
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the Tooltip Description of the Plugin
     *
     * @return the Tooltip Description of the Plugin
     */
    public String getTooltip() {
        return tooltip;
    }

    /**
     * Returns the desired path for the plugin
     *
     * @return
     */
    public String getPath() {
        return path;
    }

    public String getActionCommand() {
        return actionCommand;
    }

    public void setActionCommand(String command) {
        actionCommand = command;
    }

    public PluginType getType() {
        return type;
    }

    public double getVersion() {
        return version;
    }

    /**
     * Returns the Book this Plugin is valid for
     *
     * @return
     */
    public Book getBook() {
        return book;
    }

}
