package de.uni_muenchen.vetmed.xbook.api.plugin;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import de.uni_muenchen.vetmed.xbook.api.plugin.IPlugin;


public class PluginLoader {

    /**
     * Loads a plugin of the given name.
     * 
     * @param pluginName
     *            the name of the plugin to load.
     * @param queryManager
     *            the queryManager manager reference to pass to the plugin.
     */
    @SuppressWarnings("unchecked")
    public static Class<IPlugin> loadPlugin(String pluginName) {
        String jarName = System.getProperty("user.dir")
                + System.getProperty("file.separator") + "plugins"
                + System.getProperty("file.separator") + pluginName;
       return loadPlugin(jarName, false); 
    }
         public static Class<IPlugin> loadPlugin(String jarName,boolean debug) {
             
         
        // Build path to where the JAR file for the given plugin must be
        // located.
        // We will store the main class in this variable when we find it, to use
        // it for creating a new instance later on. This seems to be necessary
        // because for some reasons I can't figure out Class.forName does not
        // find the loaded class.
        Class<IPlugin> main = null;
        try {
            // Create class loader for the JAR file.
            URLClassLoader ucl = new URLClassLoader(new URL[]{new URL("file",
                        null, jarName)});

            // And a reader.
            JarInputStream jis = new JarInputStream(
                    new FileInputStream(jarName));
            JarEntry je = jis.getNextJarEntry();

            // Parse all records in the JAR file.
            while (je != null) {
                String name = je.getName();

                if (!je.isDirectory() && je.getName().endsWith(".class")) {
                    // Got a compiled class, replace /s with .s and load it.
                    name = name.substring(0, name.length() - 6).replace("/",
                            ".");
                    // org.apache.log4j.Logger.getLogger(PluginLoader.class).info(String.format("Loading class %s",
                    // name));
                    Class<?> clazz = ucl.loadClass(name);
                    // Check if it's the main class.
                    if (IPlugin.class.isAssignableFrom(clazz)) {
                        main = (Class<IPlugin>) clazz;
                    }
                }

                je = jis.getNextJarEntry();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new PluginLoaderException("Invalid plugin name given.", e);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new PluginLoaderException("Plugin not found.", e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new PluginLoaderException("Plugin could not be opened.", e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new PluginLoaderException("Plugin could not be loaded.", e);
        }

        // Check if we found the main class.
        if (main == null) {
            throw new PluginLoaderException("Main class not found.", null);
        }
        return main;
//        try {
//            // @SuppressWarnings("unchecked")
//            // Class<IPlugin> clazz = (Class<IPlugin>)
//            // Class.forName("ossobook2010.plugins." + pluginName + ".Main",
//            // true, ucl);
//
//            // OK, try to get a new instance, and initialize it.
//            //main.newInstance().initialize(queryManager);
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//            throw new PluginLoaderException("Failed instantiating Main class.",
//                    e);
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//            throw new PluginLoaderException("Failed instantiating Main class.",
//                    e);
//        }
    }
}
