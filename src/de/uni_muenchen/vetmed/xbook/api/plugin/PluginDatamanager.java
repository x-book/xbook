package de.uni_muenchen.vetmed.xbook.api.plugin;

import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;

/**
 *
 * @author jojo
 */
public interface PluginDatamanager {
    public UniqueArrayList<String> getData(String name);
}
