package de.uni_muenchen.vetmed.xbook.api.plugin;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;

/**
 * Interface that has to be implemented by plugins for the OssoBook client.
 *
 * <p>
 * Plugins must contain a main class (named <code>Main</code>), in a subpackage
 * of the <code>ossobook.plugins</code>package, named like their containing JAR
 * file. For example, a plugin named <code>example</code> must have a main class
 * <code>ossobook.plugins.example.Main</code>, and the JAR file must be named
 * <code>example.jar</code>.
 * </p>
 *
 * <p>
 * This main class <em>must implement</em> the {@link IPlugin} interface.
 * </p>
 *
 * <p>
 * Upon user request, this JAR file is loaded during runtime, and the plugin's
 * <code>initialize()</code> function is called.
 * </p>
 *
 * @author fnuecke
 */
public interface IPlugin {

    /**
     * This function will be called by the OssoBook client when the plugin was
     * loaded successfully.
     *
     * @param apiControllerAccess
     */
    public void initialize(ApiControllerAccess apiControllerAccess);

    /**
     * Returns the Plugin Information
     *
     * @return
     */
    public PluginInformation getPluginInformation();

    /**
     * Deactivates this Plugin. Only used if plugin changes Data and is
     * therefore an passive plugin as defined under PluginInformation
     */
    public void deactivate();
}
