package de.uni_muenchen.vetmed.xbook.api.plugin.example;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.plugin.IPlugin;
import de.uni_muenchen.vetmed.xbook.api.plugin.PluginDatamanager;
import de.uni_muenchen.vetmed.xbook.api.plugin.PluginInformation;

/**
 * Example plugin for the OssoBook client.
 *
 * <p>
 * Queries the current version from the database on initialization.
 * </p>
 *
 * @author fnuecke
 */
public class Main implements IPlugin, PluginDatamanager {

    private ApiControllerAccess queryInterface;

    @Override
    public PluginInformation getPluginInformation() {
        return new PluginInformation("asdtest", "test", "values", 1, PluginInformation.PluginType.PASSIVE, PluginInformation.Book.ALL);
    }

    @Override
    public UniqueArrayList<String> getData(String name) {
        UniqueArrayList<String> asd = new UniqueArrayList<String>();
        asd.add("dollertest1");
        asd.add("dollertest2");
        asd.add("dollertest3");
        asd.add("dollertest4");
        asd.add("dollertest5");
        return asd;

    }

    @Override
    public void deactivate() {
        queryInterface.removeDatamanager(this);
    }

    @Override
    public void initialize(ApiControllerAccess pluginController) {
    }
}
