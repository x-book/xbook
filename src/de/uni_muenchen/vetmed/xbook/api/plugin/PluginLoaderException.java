package de.uni_muenchen.vetmed.xbook.api.plugin;

public class PluginLoaderException extends RuntimeException {
	
	public PluginLoaderException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
