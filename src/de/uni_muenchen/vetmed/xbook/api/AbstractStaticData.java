package de.uni_muenchen.vetmed.xbook.api;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;



/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractStaticData {

    protected static ApiControllerAccess controller;

    public abstract void update();

    public static void setController(ApiControllerAccess controller) {
        AbstractStaticData.controller = controller;
    }
}
