package de.uni_muenchen.vetmed.xbook.api;

import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.*;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Loc {

    private static final Log LOGGER = LogFactory.getLog(Loc.class);

    private static final String BUNDLE_NAME = "de.uni_muenchen.vetmed.xbook.api.localisation";
    private static ResourceBundle RESOURCE_BUNDLE = null;
    private static Loc thisElement;

    /**
     * Singleton.
     */
    protected Loc() {
        this.thisElement = this;
    }

    /**
     * Get a localized string of the given ID. If the string cannot be found in the current localization, an error
     * String is returned (ID of the string encapsulated by '!'s).
     *
     * @param key the ID of the localized string to translate.
     * @return localized string or error string if not found.
     */
    public static String get(String key) {
        if (thisElement == null) {
            new Loc();
        }
        return thisElement.customModification(thisElement.translate(key));
    }

    /**
     * Get a formatted string.
     *
     * <p>
     * This is a convenience function for
     * <code>String.format(Messages.get(key), args)</code>. </p>
     *
     * @param key  the ID of the localized string to translate.
     * @param args arguments to use when formatting the string.
     * @return localized string or error string if not found.
     */
    public static String get(String key, Object... args) {
        return String.format(get(key), args);
    }

    protected String translate(String key) {
        synchronized (BUNDLE_NAME) {
            // Check if the current resource bundle is valid (i.e. it is set and the default locale has not changed
            // since it was created).
            if (RESOURCE_BUNDLE == null) {
                RESOURCE_BUNDLE = Utf8ResourceBundle.getBundle(BUNDLE_NAME, Locale.getDefault());
            }
            try {
                return RESOURCE_BUNDLE.getString(key);
            } catch (MissingResourceException e) {
                if (XBookConfiguration.MODE == AbstractConfiguration.Mode.DEVELOPMENT) {
                    LOGGER.warn("█ Value of key '" + key + "' not found in properties!");
                }
                return '!' + key + '!';
            }
        }
    }

    /**
     * Support for UTF-8 in localization property files.
     *
     * <p>
     * Original implementation slightly modified from
     * <a href="http://www.thoughtsabout.net/blog/archives/000044.html">here</a>.
     * </p>
     */
    protected abstract static class Utf8ResourceBundle {

        public static ResourceBundle getBundle(String baseName, Locale locale) {
            ResourceBundle bundle = ResourceBundle.getBundle(baseName, locale);
            return createUtf8PropertyResourceBundle(bundle);
        }

        private static ResourceBundle createUtf8PropertyResourceBundle(ResourceBundle bundle) {
            if (!(bundle instanceof PropertyResourceBundle)) {
                return bundle;
            }
            return new Utf8PropertyResourceBundle((PropertyResourceBundle) bundle);
        }

        private static class Utf8PropertyResourceBundle extends ResourceBundle {

            @Override
            public Locale getLocale() {
                return bundle.getLocale();
            }

            @Override
            public boolean containsKey(String key) {
                return bundle.containsKey(key);
            }

            @Override
            public Set<String> keySet() {
                return bundle.keySet();
            }

            PropertyResourceBundle bundle;

            private Utf8PropertyResourceBundle(PropertyResourceBundle bundle) {
                this.bundle = bundle;
            }

            /* (non-Javadoc)
             * @see java.util.ResourceBundle#getKeys()
             */
            @SuppressWarnings("unchecked")
            @Override
            public Enumeration getKeys() {
                return bundle.getKeys();
            }

            /* (non-Javadoc)
             * @see java.util.ResourceBundle#handleGetObject(java.lang.String)
             */
            @Override
            protected Object handleGetObject(String key) {
                String value = bundle.getString(key);
                if (value == null) {
                    return null;
                }
                return value;
            }
        }
    }

    /**
     * By default returns the string itself. Can be overwritten to customize specific properties. (e.g. replace
     * "Projekt" with "Maßnahme" in EB.
     *
     * @param modify The string that should be modified.
     * @return The modified string.
     */
    protected String customModification(String modify) {
        return modify;
    }

    /**
     * Replaces the word "Projekt" with "Maßnahme".
     *
     * @param modify The string that should be modified.
     * @return The modified string.
     */
    protected String replaceProjektWithMassnahme(String modify) {
        if (modify.contains("Projekt")) {
            if (modify.equals("Projektdaten")) {
            } else {
                modify = modify.replace("ein neues Projekt", "eine neue Maßnahme");
                modify = modify.replace("Neues Projekt", "Neue Maßnahme");
                modify = modify.replace("des ausgewählten Projekts", "der ausgewählten Maßnahme");
                modify = modify.replace("des ausgewählten Projektes", "der ausgewählten Maßnahme");
                modify = modify.replace("Projektsuche", "Maßnahmensuche");
                modify = modify.replace("des<br>Projektes", "der<br>Maßnahme");
                modify = modify.replace("Das Projekt", "Die Maßnahme");
                modify = modify.replace("ein Projekt", "eine Maßnahme");
                modify = modify.replace("Ein neues Projekt", "Eine neue Maßnahme");
                modify = modify.replace("ein neues Projekt", "eine neue Maßnahme");
                modify = modify.replace("Globale Projekte", "globale Maßnahmen");
                modify = modify.replace("Globale Projekte", "globale Maßnahmen");
                modify = modify.replace("Lokale Projekte", "lokale Maßnahmen");
                modify = modify.replace("Projekteigentümer", "Maßnahmeneigentümer");

                modify = modify.replace("Projekte", "Maßnahmen");
                modify = modify.replace("Projekt", "Maßnahme");

                // fix errors while modifying
                modify = modify.replace("Maßnahmename", "Maßnahmenname");

            }
        }
        return modify;
    }

}
