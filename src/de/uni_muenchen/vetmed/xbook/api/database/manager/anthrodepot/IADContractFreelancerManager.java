package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IADContractFreelancerManager {

    String TABLE_NAME_CONTRACT_FREELANCER = "contract_freelancer";

    ColumnType CONTRACT_FREELANCER_HASH = new ColumnType(TABLE_NAME_CONTRACT_FREELANCER + ".ID" ,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    ColumnType CONTRACT_FREELANCER_NAME = new ColumnType(TABLE_NAME_CONTRACT_FREELANCER + ".Name",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    ColumnType CONTRACT_FREELANCER_VALUE = new ColumnType(TABLE_NAME_CONTRACT_FREELANCER + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE)
            .setMaxInputLength(4*1024*1024)
            .setDisplayName(Loc.get("CONTRACT_FREELANCER"));
}
