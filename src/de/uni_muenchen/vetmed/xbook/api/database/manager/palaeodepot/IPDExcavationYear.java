package de.uni_muenchen.vetmed.xbook.api.database.manager.palaeodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * Created by lohrer on 18.10.2016.
 */
public interface IPDExcavationYear {
    String TABLENAME_EXCAVATION_YEAR = "excavation_year";

    ColumnType EXCAVATION_YEAR_BEGIN = new ColumnType(TABLENAME_EXCAVATION_YEAR+".YearBegin", ColumnType.Type.VALUE, ColumnType.ExportType.ALL).setDisplayName(Loc.get("EXCAVATION_YEAR"));
    ColumnType EXCAVATION_YEAR_END = new ColumnType(TABLENAME_EXCAVATION_YEAR+".YearEnd", ColumnType.Type.VALUE, ColumnType.ExportType.ALL);
}

