package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

public interface IADReportingManager {
    /**
     * The name of the "animal" table, this is the lookup table for animals.
     */
    String TABLE_NAME_REPORTING_VALUES = "reporting_values";

    /**
     * The
     */
    ColumnType REPORTING_VALUES_VALUE = new ColumnType(TABLE_NAME_REPORTING_VALUES + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.ID,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("REPORTING"))
            .setShowInProjectSearch(true);
}
