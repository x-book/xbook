package de.uni_muenchen.vetmed.xbook.api.database.manager.palaeodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

public interface IPDFindSheetBagManager extends IStandardColumnTypes {

    String TABLENAME_FIND_SHEET_BAG = "find_sheet_bag";

    /* ======================================================================
     * GENERAL
     * ====================================================================== */
    ColumnType CARTON_ID = new ColumnType(
            TABLENAME_FIND_SHEET_BAG + ".CartonID",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("CARTON"))
            .setHiddenInListing(true)
            .setMandatory(true)
            .setPriority(10);
    ColumnType CARTON_DATABASE_NUMBER = new ColumnType(
            TABLENAME_FIND_SHEET_BAG + ".CartonDatabaseNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE)
            .setHiddenInListing(true)
            .setDisplayName(Loc.get("CARTON_DATABASE_NUMBER"));

    /* ======================================================================
     * ARCHEOLOGICAL DATA
     * ====================================================================== */
    ColumnType FINDS_SHEET_NUMBER = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".FindsSheetNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(100)
            .setDisplayName(Loc.get("FINDS_SHEET_NUMBER"))
            .setMandatory(true)
            .setShowInProjectSearch(true)
            .setPriority(11);
    ColumnType FINDS_SHEET_NUMBER_NOTE = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".FindsSheetNumberNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(100)
            .setDisplayName(Loc.get("FINDS_SHEET_NUMBER_NOTE"))
            .setPriority(12);
    ColumnType INVENTORY_NUMBER = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".InventoryNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(100)
            .setDisplayName(Loc.get("INVENTORY_NUMBER"))
            .setPriority(13);

    ColumnType FEATURE_NUMBER = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".FeatureNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(100)
            .setDisplayName(Loc.get("FEATURE_NUMBER"))
            .setShowInProjectSearch(true)
            .setPriority(14);
    ColumnType FEATURE_NAME = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".FeatureName",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(100)
            .setDisplayName(Loc.get("FEATURE_NAME"))
            .setShowInProjectSearch(true)
            .setPriority(15);
    ColumnType FEATURE_NAME_DETAILED = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".FeatureNameDetailed",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(100)
            .setDisplayName(Loc.get("FEATURE_NAME_DETAILED"))
            .setShowInProjectSearch(true)
            .setPriority(16);
    ColumnType FEATURE_NOTE = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".FeatureNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("FEATURE_NOTE"))
            .setPriority(17);
    ColumnType VERTICAL1 = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".Vertical1",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(100)
            .setDisplayName(Loc.get("VERTICAL1"))
            .setPriority(18);
    ColumnType VERTICAL2 = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".Vertical2",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(100)
            .setDisplayName(Loc.get("VERTICAL2"))
            .setPriority(18);
    ColumnType VERTICAL3 = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".Vertical3",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(100)
            .setDisplayName(Loc.get("VERTICAL3"))
            .setPriority(19);
    ColumnType HORIZONTAL1 = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".Horizontal1",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(100)
            .setDisplayName(Loc.get("HORIZONTAL1"))
            .setPriority(20);
    ColumnType HORIZONTAL2 = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".Horizontal2",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(100)
            .setDisplayName(Loc.get("HORIZONTAL2"))
            .setPriority(21);
    ColumnType ADDITIONAL_ARCHAEOLOGICAL_INFO = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".AdditionalArchaeologicalInfo",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("ADDITIONAL_ARCHAEOLOGICAL_INFO"))
            .setPriority(22);

    ColumnType FIND_DATE = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".FindDate",
            ColumnType.Type.DATE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("FIND_DATE"))
            .setPriority(23);
    ColumnType COORDINATE_X = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".CoordinateX",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(16)
            .setDisplayName(Loc.get("COORDINATE_X"))
            .setPriority(24);
    ColumnType COORDINATE_Y = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".CoordinateY",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(16)
            .setDisplayName(Loc.get("COORDINATE_Y"))
            .setPriority(25);
    ColumnType COORDINATE_Z = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".CoordinateZ",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(16)
            .setDisplayName(Loc.get("COORDINATE_Z"))
            .setPriority(26);
    ColumnType RELATIVE_DATING = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".RelativeDating",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(50)
            .setDisplayName(Loc.get("RELATIVE_DATING"))
            .setShowInProjectSearch(true)
            .setPriority(30);
    ColumnType ABSOLUTE_DATING = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".AbsoluteDating",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(50)
            .setDisplayName(Loc.get("ABSOLUTE_DATING"))
            .setPriority(31);
    ColumnType EXCAVATION_METHOD = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".ExcavationMethod",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(50)
            .setDisplayName(Loc.get("EXCAVATION_METHOD"))
            .setPriority(32);
    ColumnType MESH_SIZE = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".MeshSize",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("MESH_SIZE"))
            .setMaxInputLength(50)
            .setPriority(33);
    ColumnType VOLUME = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".Volume",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMaxInputLength(50)
            .setDisplayName(Loc.get("VOLUME"))
            .setPriority(34);
    ColumnType NOTE = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".Note",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("NOTE"))
            .setPriority(35);

    /* ======================================================================
     * LOAN
     * ====================================================================== */
    ColumnType LOAN_TO_CONTACT = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".LoanToContact",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_TO_CONTACT"))
            .setShowInProjectSearch(true)
            .setPriority(36);
    ColumnType LOAN_FROM = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".LoanFrom",
            ColumnType.Type.DATE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_FROM"))
            .setPriority(37);
    ColumnType LOAN_UNTIL = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".LoanUntil",
            ColumnType.Type.DATE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_UNTIL"))
            .setPriority(38);
    ColumnType LOAN_NOTE = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".LoanNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("LOAN_NOTE"))
            .setPriority(39);
    ColumnType LOAN_CLARIFICATION_NEEDED = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".LoanClarificationNeeded",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_CLARIFICATION_NEEDED"))
            .setPriority(40);
    ColumnType SAMPLE_TAKEN = new ColumnType(TABLENAME_FIND_SHEET_BAG + ".SampleTaken",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("SAMPLE_TAKEN"))
            .setPriority(41);
}
