package de.uni_muenchen.vetmed.xbook.api.database.manager.ossobook;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * Created by lohrer on 14.09.2015.
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>, Alex
 */
public interface IOBInputUnitManager extends IStandardInputUnitColumnTypes {

    ColumnType.SectionAssignment PROPERTY_SECTION1 = new ColumnType.SectionAssignment(1, Loc.get("SECTION_1"));
    ColumnType.SectionAssignment PROPERTY_SECTION2 = new ColumnType.SectionAssignment(2, Loc.get("SECTION_2"));
    ColumnType.SectionAssignment PROPERTY_SECTION3 = new ColumnType.SectionAssignment(3, Loc.get("SECTION_3"));
    ColumnType.SectionAssignment PROPERTY_SECTION4 = new ColumnType.SectionAssignment(4, Loc.get("SECTION_4"));
    ColumnType.SectionAssignment PROPERTY_SECTION5 = new ColumnType.SectionAssignment(5, Loc.get("SECTION_5"));

    /* =====================================================================
     * ARCHAEOLOGY
     * ===================================================================== */
    ColumnType EXCAVATION_NUMBER = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".ExcavationNr",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType ARCHAEOLOGICAL_UNIT = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".ArchaeologicalUnit",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType INVENTORY_NUMBER = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".InventoryNo",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType FEATURE_NAME = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".FeatureName",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType FEATURE_NAME_DETAILED = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".FeatureNameDetailed",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType FEATURE_NUMBER = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".FeatureNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType VERTICAL_STRATIGRAPHY_1 = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".VerticalStratigraphy1",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType VERTICAL_STRATIGRAPHY_2 = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".VerticalStratigraphy2",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType VERTICAL_STRATIGRAPHY_3 = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".VerticalStratigraphy3",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType HORIZONTAL_STRATIGRAPHY_1 = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".HorizontalStratigraphy1",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType HORIZONTAL_STRATIGRAPHY_2 = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".HorizontalStratigraphy2",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType COORD_X = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".CoordX",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
        //.setDataType(DataType.Double).setScale(IntervalScale.class)
            .setShowInProjectSearch(true);
    ColumnType COORD_Y = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".CoordY",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)//.setDataType(DataType.Double).setScale(IntervalScale.class)
            .setShowInProjectSearch(true);
    ColumnType COORD_Z = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".CoordZ",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)//.setDataType(DataType.Double).setScale(IntervalScale.class)
            .setShowInProjectSearch(true);
    ColumnType ADDITIONAL_ARCHAEOLOGICAL_INFO = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".AddInfoArch",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType FEATURE_NOTE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".FeatureNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType DATE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Date",
            ColumnType.Type.DATE,
            ColumnType.ExportType.ALL)//.setDataType(DataType.Date).setScale(OrdinalScale.class)
            .setShowInProjectSearch(true);
    ColumnType REFERENCE_NOTE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".ReferenceNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType ABSOLUTE_DATING = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".AbsoluteDating",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType RELATIVE_DATING = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".RelativeDating",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType EXCAVATION_METHOD = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".ExcavationMethod",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);

    ColumnType FRACTION = new ColumnType(TABLENAME_INPUT_UNIT + ".Fracture",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType MESH_SIZE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".MeshSize",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)//.setDataType(DataType.Double).setScale(IntervalScale.class)
            .setShowInProjectSearch(true);
    ColumnType VOLUME = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Volume",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)//.setDataType(DataType.Double).setScale(IntervalScale.class)
            .setShowInProjectSearch(true);

    /* =====================================================================
     * ARCHAEOZOOLOGY
     * ===================================================================== */
    ColumnType CF = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".CF",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.ALL);//.setDataType(DataType.Boolean);
    ColumnType SKELETON_ELEMENT_ID = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".SkeletonElement",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.SPECIFIC);
    ColumnType SKELETON_ELEMENT = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".SkeletonElement",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType AGE_GROUP_DESCRIPT = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".AgeGroupDescript",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)//.setScale(OrdinalScale.class)
            .setShowInProjectSearch(true);
    ColumnType WEARSTAGE_ALTERNATIVE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".WearstageAlternative",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)//.setScale(OrdinalScale.class)
            .setShowInProjectSearch(true);
    ColumnType AGE_1_BASEL = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Age1Basel",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)//.setScale(OrdinalScale.class)
            .setShowInProjectSearch(true);
    ColumnType AGE_2_BASEL = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Age2Basel",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)//.setScale(OrdinalScale.class)
            .setShowInProjectSearch(true);
    ColumnType BONE_FUSION = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".BoneFusion",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType SEX = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Sex",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType BODY_SIDE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".BodySide",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType WEIGHT = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Weight",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)//.setDataType(DataType.Double).setScale(RatioScale.class)
            .setShowInProjectSearch(true);
    ColumnType PATHOLOGY = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Pathology",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType INDIVIDUAL_NUMBER = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".IndividualNo",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);
    ColumnType INDIVIDUAL_NUMBER_UNCERTAIN = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".IndividalNumberNotSure",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType FUSED = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Fused",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.GENERAL);
    ColumnType SINTERED = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Sintered",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.GENERAL);
    ColumnType OBJECT_NOTE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".ObjectNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType MUMMIFIED = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Mummified",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.GENERAL);
    ColumnType NUMBER = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Number",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)//.setDataType(DataType.Integer)
            .setShowInProjectSearch(true);
    ColumnType MINIMUM_NUMBER_OF_ELEMENTS = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".MinimalNumberOfElements",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType DNA_ANALYSIS = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".DNAAnalysis",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.GENERAL);
    ColumnType ISOTOP_ANALYSIS = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".IsotopAnalysis",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.GENERAL);
    ColumnType MEASUREMENTS_NOTE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".MeasurementsNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setShowInProjectSearch(true);
    ColumnType ESTIMATED_SIZE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".EstimatedSize",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)//.setScale(OrdinalScale.class)
            .setShowInProjectSearch(true);
    ColumnType ESTIMATED_LENGTH_MIN = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".EstimatedLengthMin",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)//.setDataType(DataType.Double).setScale(RatioScale.class)
            .setShowInProjectSearch(true);
    ColumnType ESTIMATED_LENGTH_MAX = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".EstimatedLengthMax",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)//.setDataType(DataType.Double).setScale(RatioScale.class)
            .setShowInProjectSearch(true)
            .setHideInVisibilityList(true);
    ColumnType SEASON = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Season",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType GROWTH_RING_OTOLITH = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".GrowthRingOtolith",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType GROWTH_RING_VERTEBRA = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".GrowthRingVertebra",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);

    /* =====================================================================
     * TAPHONOMY
     * ===================================================================== */
    ColumnType FRACTURE_EDGE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".FractureEdge",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)//.setScale(OrdinalScale.class)
            .setShowInProjectSearch(true);
    ColumnType FRACTURE_EDGE_2 = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".FractureEdge2",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)//.setScale(OrdinalScale.class)
            .setShowInProjectSearch(true);
    ColumnType SURFACE_PRESERVATION = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".SurfacePreservation",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)//.setScale(OrdinalScale.class)
            .setShowInProjectSearch(true);
    ColumnType SURFACE_PRESERVATION_2 = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".SurfacePreservation2",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)//.setScale(OrdinalScale.class)
            .setShowInProjectSearch(true);
    ColumnType BUTCHERING_MARK_1 = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".ButcheringMark1",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType BUTCHERING_MARK_2 = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".ButcheringMark2",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType GNAWING = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Gnawing",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType DIGESTED = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Digested",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType TRACES_OF_BURNING = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".TracesOfBurning",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType DECOLOURATION_PATINA = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".DecolourationPatina",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType ENCRUSTATION = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Encrustation",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType ROOT_EDGING = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".RootEdging",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)//.setScale(OrdinalScale.class)
            .setShowInProjectSearch(true);
    ColumnType FATTY_GLOSS = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".FattyGloss",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)//.setScale(OrdinalScale.class)
            .setShowInProjectSearch(true);
    ColumnType WATERLOGGED_DEPOSIT = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".WaterloggedDeposit",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
    ColumnType TAPHONOMY_NOTE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".TaphonomyNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);

    /* =====================================================================
     * MOLECULAR ANALYSIS
     * ===================================================================== */
    ColumnType SAMPLE_NR_14C = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".SampleNr14C",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);
    ColumnType M_14C_BP = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".14CBP",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);
    ColumnType M_14C_CAL_BC = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".14CcalBC",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);
    ColumnType M_2_SIGMA_14C = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".2Sigma14C",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);
    ColumnType SAMPLE_NR_ISO = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".SampleNrIso",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);
    ColumnType COLLAGEN_CN = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".CollagenCN",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_2H_1H = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".2H1H",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_18O_16O = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".18O16O",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_13C_12C = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".13C12C",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_15N_14N = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".15N14N",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_34S_32S = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".34S32S",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_87SR_86SR = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".87Sr86Sr",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_206PB_204PB = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".206Pb204Pb",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_206PB_207PB = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".206Pb207Pb",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_207PB_204PB = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".207Pb204Pb",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_208PB_204PB = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".208Pb204Pb",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_208PB_207PB = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".208Pb207Pb",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_18O_16O_HYDROXYLAPATITE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".18O16OHydroxylapatite",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_18O_16O_PHOSPHATE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".18O16OPhosphate",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_13C_12C_COLLAGEN = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".13C12CCollagen",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType M_13C_12C_HYDROXYLAPATITE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".13C12CHydroxylapatite",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType DNA_LAB_NUMBER = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".DNALabNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);//.setDataType(DataType.Integer).setScale(OrdinalScale.class);
    ColumnType SAMPLE_QUALITY = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".SampleQuality",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);//.setScale(OrdinalScale.class);
    ColumnType SAMPLE_DEPLETED = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".SampleDepleted",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.ALL);//.setDataType(DataType.Boolean);
    ColumnType MTDNA_HAPLOGROUP = new ColumnType(TABLENAME_INPUT_UNIT + ".mtDNAhaplogroup",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.ALL);
    ColumnType MTDNA_HAPLOGROUP_EXAMPLE = new ColumnType(TABLENAME_INPUT_UNIT + ".mtDNAhaplogroupExample",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType DNA_LIBRARY_PREPARATION = new ColumnType(TABLENAME_INPUT_UNIT + ".dnaLibraryPreparation",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.ALL);
    ColumnType DNA_LIBRARY_PREPARATION_SUCCESSFUL = new ColumnType(TABLENAME_INPUT_UNIT +
            ".dnaLibraryPreparationSuccessful",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.ALL);
    ColumnType MOLECULAR_ANALYSIS_NOTE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".MolecularAnalysisNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);

    /* =====================================================================
     * ARTEFACTS
     * ===================================================================== */
    ColumnType ARTEFACT_NOTE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT +
            ".InventoryNumberNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setShowInProjectSearch(true);
}
