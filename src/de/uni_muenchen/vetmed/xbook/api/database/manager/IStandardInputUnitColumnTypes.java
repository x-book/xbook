package de.uni_muenchen.vetmed.xbook.api.database.manager;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.database.manager.xbook.IUserManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType.ExportType;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface IStandardInputUnitColumnTypes extends IStandardColumnTypes {

    String TABLENAME_INPUT_UNIT = "inputunit";

    ColumnType ID_INPUTUNIT = new ColumnType(TABLENAME_INPUT_UNIT + "." + ID, ColumnType.Type.VALUE, ExportType.GENERAL)
            .setDisplayName(Loc.get("ID"))
            .setPriority(1)
            .setSectionProperty(IDefaultSectionAssignments.SECTION_SYSTEM);
    ColumnType DATABASE_ID_INPUTUNIT = new ColumnType(TABLENAME_INPUT_UNIT + "." + DATABASE_ID, ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("DBNUMBER"))
            .setPriority(2)
            .setSectionProperty(IDefaultSectionAssignments.SECTION_SYSTEM);
    ColumnType PROJECT_ID_INPUTUNIT = new ColumnType(TABLENAME_INPUT_UNIT + "." + PROJECT_ID, ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("PROJECT_ID"))
            .setPriority(3)
            .setSectionProperty(IDefaultSectionAssignments.SECTION_SYSTEM);
    ColumnType PROJECT_DATABASE_ID_INPUTUNIT = new ColumnType(TABLENAME_INPUT_UNIT + "." + PROJECT_DATABASE_ID, ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("PROJECT_DATABASENUMBER"))
            .setPriority(4)
            .setSectionProperty(IDefaultSectionAssignments.SECTION_SYSTEM);
    ColumnType USER_ID_INPUTUNIT = new ColumnType(TABLENAME_INPUT_UNIT + ".UserID", ColumnType.Type.ID, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("USER"))
            .setConnectedTableName(IUserManager.TABLENAME_USER)
            .setPriority(5)
            .setSectionProperty(IDefaultSectionAssignments.SECTION_SYSTEM);

}
