package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface IADFindingDate {

	String TABLENAME_FINDING_DATE = "finding_date";

	ColumnType FINDING_DATE_BEGIN = new ColumnType(
			TABLENAME_FINDING_DATE + ".DateBegin",
			ColumnType.Type.VALUE,
			ColumnType.ExportType.ALL)
			.setDisplayName(Loc.get("FINDING_DATE"));
	ColumnType FINDING_DATE_END = new ColumnType(
			TABLENAME_FINDING_DATE + ".DateEnd",
			ColumnType.Type.VALUE,
			ColumnType.ExportType.ALL);
}
