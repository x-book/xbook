package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

public interface IADBurialManager {

    /**
     * The name of the "animal" table, this is the lookup table for animals.
     */
    String TABLE_NAME_BURIAL_VALUES = "burial_values";

    /**
     * The
     */
    ColumnType BURIAL_VALUES_VALUE = new ColumnType(TABLE_NAME_BURIAL_VALUES + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("BURIAL"))
            .setShowInProjectSearch(true);
}
