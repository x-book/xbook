package de.uni_muenchen.vetmed.xbook.api.database.manager.palaeodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

public interface IPDSpeciesCartonManager {
    String TABLENAME_SPECIES_CARTON_VALUES = "species_carton_values";

    ColumnType SPECIES_VALUES_VALUE = new ColumnType(TABLENAME_SPECIES_CARTON_VALUES + ".Value",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("SPECIES"))
			.setShowInProjectSearch(true);
}
