package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IADFindingsSheetManager {

    String TABLE_NAME_FINDINGS_SHEET = "findings_sheet";

    ColumnType FINDINGS_SHEET_HASH = new ColumnType(TABLE_NAME_FINDINGS_SHEET + ".Hash",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    ColumnType FINDINGS_SHEET_NAME = new ColumnType(TABLE_NAME_FINDINGS_SHEET + ".Name",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    ColumnType FINDINGS_SHEET_VALUE = new ColumnType(TABLE_NAME_FINDINGS_SHEET + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("FINDINGS_SHEET"));
}
