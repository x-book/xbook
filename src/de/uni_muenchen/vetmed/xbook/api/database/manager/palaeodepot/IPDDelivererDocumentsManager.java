package de.uni_muenchen.vetmed.xbook.api.database.manager.palaeodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IPDDelivererDocumentsManager {

	String TABLE_NAME_DELIVERER_DOCUMENTS = "deliverer_documents";

	ColumnType DELIVERER_DOCUMENTS_HASH = new ColumnType(TABLE_NAME_DELIVERER_DOCUMENTS + ".Hash",
			ColumnType.Type.VALUE,
			ColumnType.ExportType.NONE);
	ColumnType DELIVERER_DOCUMENTS_NAME = new ColumnType(TABLE_NAME_DELIVERER_DOCUMENTS + ".Name",
			ColumnType.Type.VALUE,
			ColumnType.ExportType.NONE);
	ColumnType DELIVERER_DOCUMENTS_VALUE = new ColumnType(TABLE_NAME_DELIVERER_DOCUMENTS + "." + IStandardColumnTypes.VALUE,
			ColumnType.Type.VALUE,
			ColumnType.ExportType.NONE)
			.setDisplayName(Loc.get("DELIVERER_DOCUMENTS"))
                        .setMaxInputLength(2*1024*1024);
}
