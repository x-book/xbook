package de.uni_muenchen.vetmed.xbook.api.database.manager;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType.ExportType;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface IStandardColumnTypes {

    String DATABASE_NAME_GENERAL = (AbstractConfiguration.sharedUserTable ? "general"

            + XBookConfiguration.EXTENSION
            : AbstractConfiguration.databaseName);
    String DELETED_NO = "N";
    String DELETED_YES = "Y";
    String CONFLICTED = "-1";
    String SYNCHRONIZED = "0";
    String CHANGED = "1";
    String DELETED_LOCAL = "L";

    /**
     * The default ID Column. This is not bound to a specific table
     */
    ColumnType ID = new ColumnType("ID", ColumnType.Type.VALUE, ExportType.GENERAL)
            .setForListing(true)
            .setPriority(1);
    /**
     * The default Database ID Column. This is not bound to a specific table
     */
    ColumnType DATABASE_ID = new ColumnType("DatabaseNumber", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("DBNUMBER"))
            .setForListing(true)
            .setPriority(2);
    /**
     * The default Project ID Column. This is not bound to a specific table
     */
    ColumnType PROJECT_ID = new ColumnType("ProjectID", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("PROJECT_ID"))
            .setPriority(3);
    /**
     * The default Project Database ID Column. This is not bound to a specific
     * table
     */
    ColumnType PROJECT_DATABASE_ID = new ColumnType("ProjectDatabaseNumber", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("PROJECT_DATABASENUMBER"))
            .setPriority(4);
    /**
     * The default Status Column. This holds the last time the entry was
     * synchronized to the server. This is not bound to a specific table.
     */
    ColumnType STATUS = new ColumnType("Status", ColumnType.Type.VALUE, ColumnType.ExportType.NONE)
            .setSectionProperty(IDefaultSectionAssignments.SECTION_SYSTEM);

    /**
     * The default Value Column. This is not bound to a specific table
     */
    ColumnType VALUE = new ColumnType("Value", ColumnType.Type.VALUE, ExportType.ALL)
            .setDisplayName(Loc.get("ALPHABETICAL"));
    /**
     * The default Message Number Column. This holds the information, if the
     * entry is {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#SYNCHRONIZED Synchronized},
     * {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#CONFLICTED Conflicted}
     * or
     * {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#CHANGED Changed}.
     * This is not bound to a specific table.
     */
    ColumnType MESSAGE_NUMBER = new ColumnType("MessageNumber", ColumnType.Type.VALUE, ColumnType.ExportType.NONE)
            .setSectionProperty(IDefaultSectionAssignments.SECTION_SYSTEM);


    /**
     * The default Deleted Column. This can be
     * {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#DELETED_YES Deleted}
     * or
     * {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#DELETED_NO not}.
     * This is not bound to a specific table
     */
    ColumnType DELETED = new ColumnType("Deleted", ColumnType.Type.VALUE, ColumnType.ExportType.NONE)
            .setSectionProperty(IDefaultSectionAssignments.SECTION_SYSTEM);

    /**
     * The path or name of the file to upload
     */
    ColumnType FILE_NAME = new ColumnType("FileName",
            ColumnType.Type.FILE,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("FILE"))
            .setHiddenInListing(true);
    /**
     * The path or name of the file to upload
     */
    ColumnType FILE_EXTENSION = new ColumnType("FileExtension",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("FILE_NAME_EXTENSION"))
            .setHiddenInListing(true)
            .setExportType(ColumnType.ExportType.NONE);
    /**
     * Indicates if the file is new and has to be updated
     */
    ColumnType FILE_MUST_BE_UPDATED = new ColumnType("FileMustBeSynced",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("FILE_MUST_BE_UPDATED"))
            .setHiddenInListing(true)
            .setExportType(ColumnType.ExportType.NONE);

}
