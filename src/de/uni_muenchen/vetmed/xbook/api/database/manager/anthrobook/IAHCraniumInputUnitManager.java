/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrobook;

import static de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 *
 * @author Anja Mösch
 */
public interface IAHCraniumInputUnitManager {
    
    	public static final ColumnType CRANIUM_BASIS = new ColumnType(TABLENAME_INPUT_UNIT + ".cranium_basis", 
                ColumnType.Type.VALUE, 
                ColumnType.ExportType.GENERAL);
        
	public static final ColumnType CRANIUM_FRONTAL = new ColumnType(TABLENAME_INPUT_UNIT + ".cranium_frontal", 
                ColumnType.Type.VALUE, 
                ColumnType.ExportType.GENERAL);
        
	public static final ColumnType CRANIUM_LATERAL_LEFT = new ColumnType(TABLENAME_INPUT_UNIT + ".cranium_lateral_left",
                ColumnType.Type.VALUE, 
                ColumnType.ExportType.GENERAL);
        
	public static final ColumnType CRANIUM_LATERAL_RIGHT = new ColumnType(TABLENAME_INPUT_UNIT + ".cranium_lateral_right", 
                ColumnType.Type.VALUE, 
                ColumnType.ExportType.GENERAL);

    
}
