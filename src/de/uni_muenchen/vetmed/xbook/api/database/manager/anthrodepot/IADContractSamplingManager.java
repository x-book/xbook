package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IADContractSamplingManager {

    String TABLE_NAME_CONTRACT_SAMPLING = "contract_sampling";

    ColumnType CONTRACT_SAMPLING_HASH = new ColumnType(TABLE_NAME_CONTRACT_SAMPLING + ".ID" ,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    ColumnType CONTRACT_SAMPLING_NAME = new ColumnType(TABLE_NAME_CONTRACT_SAMPLING + ".Name",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    ColumnType CONTRACT_SAMPLING_VALUE = new ColumnType(TABLE_NAME_CONTRACT_SAMPLING + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("CONTRACT_SAMPLING"));
}
