package de.uni_muenchen.vetmed.xbook.api.database.manager;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnTypeList;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 18.06.2014.
 */
public interface IBaseManager {

  DataSetOld getNextUncomittedEntry(DataSetOld project)
      throws NotLoggedInException, StatementNotExecutedException, NoRightException, IOException;

  void deletePermanent(ProjectDataSet project, int event)
      throws StatementNotExecutedException;

  void updateDatabaseNumber(int newNumber)
      throws StatementNotExecutedException;

  int getNumberOfUncommittedEntries(Key projectKey)
      throws StatementNotExecutedException;

  void setConflicted(DataSetOld dataSet)
      throws StatementNotExecutedException;

  void updateEntries(DataSetOld data) throws StatementNotExecutedException;

  void setSynchronised(DataSetOld entryData, DataSetOld returnData)
      throws StatementNotExecutedException;

  boolean isConflicted(Key projectKey)
      throws StatementNotExecutedException;

  void solveConflict(DataSetOld entryData)
      throws StatementNotExecutedException;

  String getTableName();

  ArrayList<String> getPrimaryColumns();

  String getLastSynchronisation(Key projectKey)
      throws StatementNotExecutedException;

  boolean isSynchronised(DataSetOld entry)
      throws StatementNotExecutedException;


  ColumnTypeList getDataColumns();

  /**
   * Returns the primary columns for the given table. If the manager has multiple sub managers it
   * should queryManager them all
   *
   * @param tableName The name of the table to return the primary columns for
   * @return The Primary columns for the given table name, or null if not found
   */
  ArrayList<String> getPrimaryColumnsForTable(String tableName);

  ISynchronisationManager[] getManagers();

  int getNumberofEntrys(Key projectKey) throws StatementNotExecutedException;

  int getNumberOfConflictedEntries(Key projectKey) throws StatementNotExecutedException;

  void updateUnsyncedEntries(ProjectDataSet project) throws StatementNotExecutedException;

  HashMap<Key, Integer> getNumberofEntrysForAllProject() throws StatementNotExecutedException;

  HashMap<Key, Integer> getNumberofUnsynchronizedEntrysForAllProject()
      throws StatementNotExecutedException;

  HashMap<Key, Integer> getNumberofConflictedEntrysForAllProject()
      throws StatementNotExecutedException;

  String getLocalisedTableName();
}
