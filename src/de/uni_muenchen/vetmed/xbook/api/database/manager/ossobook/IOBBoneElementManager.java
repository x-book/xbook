package de.uni_muenchen.vetmed.xbook.api.database.manager.ossobook;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * Created by lohrer on 14.09.2015.
 */
public interface IOBBoneElementManager extends IStandardColumnTypes {

    /**
     * The name for the bone element values table, this is the table the bone
     * element values the user enters are saved.
     */
    String TABLENAME_BONEELEMENT_VALUES = "boneelementvalues";
    /**
     * The name fot the bonelement table, this is the lookup table for the
     * boneelements.
     */
    String TABLENAME_BONEELEMENT = "boneelement";
    //Boneelements

    /**
     * The Column for the Sceleton element ID of the boneelements
     */
    ColumnType BONEELEMENT_SCELETID = new ColumnType(TABLENAME_BONEELEMENT + ".sceletID",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Boneelement ID of the boneelements
     */
    ColumnType BONEELEMENT_BONEELEMENTID = new ColumnType(TABLENAME_BONEELEMENT + ".boneelementID",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Bonelement Name of the Bonelements. The language the
     * name is in, is defined in the Language Column
     */
    ColumnType BONEELEMENT_VALUE = new ColumnType(TABLENAME_BONEELEMENT + ".value",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Language, the name of the boneelement is in. 0 means
     * international, 1 german, 2 englisch, 3 french, 4 spanish.
     */
    ColumnType BONEELEMENT_LANGUAGE = new ColumnType(TABLENAME_BONEELEMENT + ".language",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    //Boneelement Values
    /**
     * The Column for the ID of the Bonelement Values.
     */
    ColumnType BONEELEMENT_VALUES_ID = new ColumnType(TABLENAME_BONEELEMENT_VALUES + "." + IStandardColumnTypes.ID,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setPriority(1);
    /**
     * The Column for the Database ID of the Bonelement Values.
     */
    ColumnType BONEELEMENT_VALUES_DATABASE_ID = new ColumnType(TABLENAME_BONEELEMENT_VALUES + "." + IStandardColumnTypes.DATABASE_ID, ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("DBNUMBER"))
            .setPriority(2);
    /**
     * The Column for the Project ID of the Bonelement Values.
     */
    ColumnType BONEELEMENT_VALUES_PROJECT_ID = new ColumnType(TABLENAME_BONEELEMENT_VALUES + "." + IStandardColumnTypes.PROJECT_ID,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("PROJECT_ID"))
            .setPriority(3);
    /**
     * The Column for the Project Database ID of the Bonelement Values.
     */
    ColumnType BONEELEMENT_VALUES_PROJECT_DATABASE_ID = new ColumnType(TABLENAME_BONEELEMENT_VALUES + "." + IStandardColumnTypes.PROJECT_DATABASE_ID,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("PROJECT_DATABASENUMBER"))
            .setPriority(4);
    /**
     * The column for the Value of the Bonelement Values. This is where the
     * value of the bonelement the user enters is saved.
     */
    ColumnType BONEELEMENT_VALUES_BONEELEMENT = new ColumnType(TABLENAME_BONEELEMENT_VALUES + "." + VALUE,
            ColumnType.Type.ID_SPECIFIC,
            ColumnType.ExportType.GENERAL);

    /**
     * The Column for the Status of the Animal Values. This holds the last time
     * the entry was synchronized to the server.
     */
    ColumnType BONEELEMENT_VALUES_STATUS = new ColumnType(TABLENAME_BONEELEMENT_VALUES + "." + IStandardColumnTypes.STATUS,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Bonelement Values Message Number. This holds the
     * information, if the entry is {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#SYNCHRONIZED Synchronized},
     * {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#CONFLICTED Conflicted}
     * or
     * {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#CHANGED Changed}.
     */
    ColumnType BONEELEMENT_VALUES_MESSAGESNUMBER = new ColumnType(TABLENAME_BONEELEMENT_VALUES + "." + IStandardColumnTypes.MESSAGE_NUMBER,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Bonelement Values Deleted. This can be
     * {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#DELETED_YES Deleted}
     * or
     * {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#DELETED_NO not}.
     */
    ColumnType BONEELEMENT_VALUES_DELETED = new ColumnType(TABLENAME_BONEELEMENT_VALUES + "." + IStandardColumnTypes.DELETED,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
}
