package de.uni_muenchen.vetmed.xbook.api.database.manager.ossobook;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * This interface holds all Information over the columns and tables used for the
 * animals used in OssoBook
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 * 14.09.2015.
 */
public interface IOBAnimalManager {

    /**
     * The name of the "animal" table, this is the lookup table for animals.
     */
    String TABLE_NAME_ANIMAL = "animal";

    /**
     * The name of the "animalclass" table, this is the lookup table for animal
     * class.
     */
    String TABLE_NAME_CLASS = "animalclass";

    /**
     * The name of the "animalvalues" table, this is the table the animal values
     * the user enters are saved.
     */
    String TABLE_NAME_ANIMAL_VALUES = "animalvalues";

    //**********animals***********
    /**
     * The Column for the ID of the anmials. This is used to identify the
     * animal. Only true unique with a language. eg. A Singular ID can exist in
     * different Languages
     */
    ColumnType ANIMAL_ID = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL + "." + IStandardColumnTypes.ID,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Label of the Animal. This is a classification of the
     * different classes of the animals
     */
    ColumnType ANIMAL_LABEL = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL + ".LB",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);

    /**
     * The Column for the Name of the Animal. The language the name is in is
     * defined in the Language Column
     */
    ColumnType ANIMAL_NAME = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL + ".value",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Language, the Name of the animal is in. 0 means
     * international, 1 german, 2 englisch, 3 french, 4 spanish.
     */
    ColumnType ANIMAL_LANGUAGE = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL + ".language",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);

    //**********animals class***********//  
    /**
     * The Column for the ID of the class of animals.
     */
    ColumnType CLASS_ID = new ColumnType(IOBAnimalManager.TABLE_NAME_CLASS + "." + IStandardColumnTypes.ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Name of the animal Class of the animal values.
     */
    ColumnType CLASS_NAME = new ColumnType(IOBAnimalManager.TABLE_NAME_CLASS + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.ID_SPECIFIC,
            ColumnType.ExportType.ALL);
    /**
     * The Column for the Label of the Class. A Class can have multiple labels.
     */
    ColumnType CLASS_LABEL = new ColumnType(IOBAnimalManager.TABLE_NAME_CLASS + ".AnimalLB",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Language of the class.
     */
    ColumnType CLASS_LANGUAGE = new ColumnType(IOBAnimalManager.TABLE_NAME_CLASS + ".Language",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);

    //**********animal values***********//  
    /**
     * The Column for the ID of the animal values.
     */
    ColumnType ANIMAL_VALUES_ID = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL_VALUES + "." + IStandardColumnTypes.ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Database ID of the animal values.
     */
    ColumnType ANIMAL_VALUES_DATABASE_ID = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL_VALUES + "." + IStandardColumnTypes.DATABASE_ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Project ID of the animal values.
     */
    ColumnType ANIMAL_VALUES_PROJECT_ID = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL_VALUES + "." + IStandardColumnTypes.PROJECT_ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Project Database ID of the animal values.
     */
    ColumnType ANIMAL_VALUES_PROJECT_DATABASE_ID = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL_VALUES + "." + IStandardColumnTypes.PROJECT_DATABASE_ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);

    /**
     * The Column for the ID of the Animal saved in the animal values table.
     * This one returns the id as a integer in the export. For the translated
     * name use {@link IOBAnimalManager#ANIMAL_VALUES_ANIMALS}
     */
    ColumnType ANIMAL_VALUES_ANIMALS_ID = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL_VALUES + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.SPECIFIC);
    /**
     * The Column for the ID of the Animal saved in the animal values table.
     * This returns the translated name in the export. For the id as a integer
     * use {@link IOBAnimalManager#ANIMAL_VALUES_ANIMALS_ID} bla
     */
    ColumnType ANIMAL_VALUES_ANIMALS = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL_VALUES + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL);
    /**
     * The Column for the Status of the Animal Values. This holds the last time
     * the entry was synchronized to the server.
     */
    ColumnType ANIMAL_VALUES_STATUS = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL_VALUES + "." + IStandardColumnTypes.STATUS,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column for the Message Number of the Animal Values. This holds the
     * information, if the entry is {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#SYNCHRONIZED Synchronized},
     * {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#CONFLICTED Conflicted}
     * or
     * {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#CHANGED Changed}
     */
    ColumnType ANIMAL_VALUES_MESSAGESNUMBER = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL_VALUES + "." + IStandardColumnTypes.MESSAGE_NUMBER,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * The Column that holds if the Animal Values entry is 
     * {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#DELETED_YES Deleted}
     * or {@link de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes#DELETED_NO not}
     */
    ColumnType ANIMAL_VALUES_DELETED = new ColumnType(IOBAnimalManager.TABLE_NAME_ANIMAL_VALUES + "." + IStandardColumnTypes.DELETED,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    /**
     * Animal mapping used to translate the animals for the current entry
     */
    public static final String MAPPING_ANIMAL_TO_INPUT_UNIT = ANIMAL_VALUES_ID + "=" + IOBInputUnitManager.ID + " AND "
            + ANIMAL_VALUES_DATABASE_ID + "=" + IOBInputUnitManager.DATABASE_ID
            + " AND " + ANIMAL_VALUES_PROJECT_ID + "=" + IOBInputUnitManager.PROJECT_ID
            + " AND " + ANIMAL_VALUES_PROJECT_DATABASE_ID + "=" + IOBInputUnitManager.PROJECT_DATABASE_ID;
}
