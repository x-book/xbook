package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardProjectColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IADProjectManager extends IStandardProjectColumnTypes {

    ColumnType INPUT_NUMBER = new ColumnType(TABLENAME_PROJECT + ".InputNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("INPUT_NUMBER"))
            .setShowInProjectSearch(true);
    ColumnType ACTIVITY_NUMBER = new ColumnType(TABLENAME_PROJECT + ".ActivityNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("ACTIVITY_NUMBER"))
            .setShowInProjectSearch(true);
    ColumnType INPUT_NUMBER_OLD = new ColumnType(TABLENAME_PROJECT + ".InputNumberOld",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("INPUT_NUMBER_OLD"))
            .setShowInProjectSearch(true);
    ColumnType COUNTRY = new ColumnType(TABLENAME_PROJECT + ".Country",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("COUNTRY"))
            .setShowInProjectSearch(true);
    ColumnType STATE = new ColumnType(TABLENAME_PROJECT + ".State",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("STATE"))
            .setShowInProjectSearch(true);
    ColumnType COUNTY = new ColumnType(TABLENAME_PROJECT + ".County",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("COUNTY"))
            .setShowInProjectSearch(true);
    ColumnType PARISH = new ColumnType(TABLENAME_PROJECT + ".Parish",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("PARISH"))
            .setShowInProjectSearch(true);
    ColumnType BOUNDS = new ColumnType(TABLENAME_PROJECT + ".Bounds",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("BOUNDS"))
            .setShowInProjectSearch(true);
    ColumnType FINDPLACE = new ColumnType(TABLENAME_PROJECT + ".FindPlace",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("FINDPLACE"))
            .setShowInProjectSearch(true);
//    ColumnType FIND_DATE_FROM = new ColumnType(TABLENAME_PROJECT + ".FindDateFrom",
//            ColumnType.Type.VALUE, 
//            ColumnType.ExportType.GENERAL)
//            .setDisplayName(Loc.get("FIND_DATE_FROM"))
//            .setDisplayNameGeneral(Loc.get("FIND_DATE"))
//            .setShowInProjectSearch(true);
//    ColumnType FIND_DATE_TO = new ColumnType(TABLENAME_PROJECT + ".FindDateTo",
//            ColumnType.Type.VALUE,
//            ColumnType.ExportType.GENERAL)
//            .setDisplayName(Loc.get("FIND_DATE_TO"))
//            .setDisplayNameGeneral(Loc.get("FIND_DATE"))
//            .setShowInProjectSearch(true);
    ColumnType FINDING_DATE_UNSURE = new ColumnType(TABLENAME_PROJECT + ".FindingDateUnsure",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("FINDING_DATE_UNSURE"));
    ColumnType PERIOD = new ColumnType(TABLENAME_PROJECT + ".Period",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("PERIOD"))
            .setShowInProjectSearch(true);
    ColumnType CONDITION = new ColumnType(TABLENAME_PROJECT + ".Condition",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("CONDITION"))
            .setShowInProjectSearch(true);
    ColumnType EXCAVATION_COMPANY = new ColumnType(TABLENAME_PROJECT + ".ExcavationCompany",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("EXCAVATION_COMPANY"))
            .setShowInProjectSearch(true);
    ColumnType ANTHROPOLOGIST = new ColumnType(TABLENAME_PROJECT + ".Anthropologist",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("ANTHROPOLOGIST"))
            .setShowInProjectSearch(true);
    ColumnType OWNERSHIP = new ColumnType(TABLENAME_PROJECT + ".Ownership",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("OWNERSHIP"))
            .setShowInProjectSearch(true);
    ColumnType LOAN_UNTIL = new ColumnType(TABLENAME_PROJECT + ".LoanUntil",
            ColumnType.Type.DATE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_UNTIL"))
            .setShowInProjectSearch(true);
    ColumnType CURRENT_STOCKROOM = new ColumnType(TABLENAME_PROJECT + ".CurrentStockroom",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("CURRENT_STOCKROOM"))
            .setShowInProjectSearch(true);
    ColumnType COMMENTS = new ColumnType(TABLENAME_PROJECT + ".Comments",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("COMMENTS"))
            .setShowInProjectSearch(true);

    ColumnType ADMISSION_FORM_BOOLEAN = new ColumnType(TABLENAME_PROJECT + ".AdmissionForm",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.GENERAL);
    ColumnType CONTRACT_FREELANCER_BOOLEAN = new ColumnType(TABLENAME_PROJECT + ".ContractFreelancer",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.GENERAL);
    ColumnType CONTRACT_SAMPLING_BOOLEAN = new ColumnType(TABLENAME_PROJECT + ".ContractSampling",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.GENERAL);

}
