package de.uni_muenchen.vetmed.xbook.api.database.manager.ossobook;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * Created by lohrer on 14.09.2015.
 */
public interface IOBDiagnosticZonesManager {

    String TABLENAME_DIAGNOSTICZONES_VALUES = "diagnosticzones";
    ColumnType DIAGNOSTICZONES_VALUES_DIAGNOSTICZONES = new ColumnType(IOBDiagnosticZonesManager.TABLENAME_DIAGNOSTICZONES_VALUES + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("DIAGNOSTIC_ZONES"))
            .setPriority(53)
            .setSectionProperty(IOBInputUnitManager.PROPERTY_SECTION2)
            .setMultiEditAllowed(false);
}
