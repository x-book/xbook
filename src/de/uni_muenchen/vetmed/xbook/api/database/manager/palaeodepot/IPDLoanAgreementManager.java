package de.uni_muenchen.vetmed.xbook.api.database.manager.palaeodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IPDLoanAgreementManager {

	String TABLE_NAME_LOAN_AGREEMENT = "loan_agreement";

	ColumnType LOAN_AGREEMENT_HASH = new ColumnType(TABLE_NAME_LOAN_AGREEMENT + ".Hash",
			ColumnType.Type.VALUE,
			ColumnType.ExportType.NONE);
	ColumnType LOAN_AGREEMENT_NAME = new ColumnType(TABLE_NAME_LOAN_AGREEMENT + ".Name",
			ColumnType.Type.VALUE,
			ColumnType.ExportType.NONE);
	ColumnType LOAN_AGREEMENT_VALUE = new ColumnType(TABLE_NAME_LOAN_AGREEMENT + "." + IStandardColumnTypes.VALUE,
			ColumnType.Type.VALUE,
			ColumnType.ExportType.NONE)
			.setDisplayName(Loc.get("LOAN_AGREEMENT"))
                        .setMaxInputLength(2*1024*1024);
}
