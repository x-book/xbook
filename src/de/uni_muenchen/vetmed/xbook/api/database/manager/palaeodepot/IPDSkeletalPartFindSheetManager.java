package de.uni_muenchen.vetmed.xbook.api.database.manager.palaeodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

public interface IPDSkeletalPartFindSheetManager {
    String TABLENAME_SKELETAL_PARTS_FIND_SHEETS_VALUES = "skeletal_parts_find_sheets_values";

    ColumnType SKELETAL_PARTS_VALUES_VALUE = new ColumnType(TABLENAME_SKELETAL_PARTS_FIND_SHEETS_VALUES + ".Value",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("SKELETAL_PARTS"))
			.setShowInProjectSearch(true);
}
