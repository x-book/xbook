package de.uni_muenchen.vetmed.xbook.api.database.manager.demobook;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IDBInputUnitManager extends IStandardInputUnitColumnTypes {

    ColumnType TEXT_FIELD = new ColumnType(TABLENAME_INPUT_UNIT + ".TextField",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType TEXT_AREA = new ColumnType(TABLENAME_INPUT_UNIT + ".TextArea",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType INTEGER_FIELD = new ColumnType(TABLENAME_INPUT_UNIT + ".IntegerField",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType FILE_NAME_CHOOSER = new ColumnType(TABLENAME_INPUT_UNIT + ".FileNameChooser",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setMaxInputLength(128);
    ColumnType FLOAT_FIELD = new ColumnType(TABLENAME_INPUT_UNIT + ".FloatField",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType INTEGER_SPINNER = new ColumnType(TABLENAME_INPUT_UNIT + ".IntegerSpinner",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType INTEGER_UNIT_FIELD = new ColumnType(TABLENAME_INPUT_UNIT + ".IntegerUnitField",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType LABELED_FLOAT_FIELD = new ColumnType(TABLENAME_INPUT_UNIT + ".LabeledFloatField",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType LABELED_INTEGER_FIELD = new ColumnType(TABLENAME_INPUT_UNIT + ".LabeledIntegerField",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType MINMAX_FLOAT_FIELD_MIN = new ColumnType(TABLENAME_INPUT_UNIT + ".MinMaxFloatFieldMin",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType MINMAX_FLOAT_FIELD_MAX = new ColumnType(TABLENAME_INPUT_UNIT + ".MinMaxFloatFieldMax",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType NUMBER_AS_STRING_FIELD = new ColumnType(TABLENAME_INPUT_UNIT + ".NumberAsStringField",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType NUMBER_AS_STRING_UNIT_FIELD = new ColumnType(TABLENAME_INPUT_UNIT + ".NumberAsStringUnitField",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType FILE_DATA_FIELD = new ColumnType(TABLENAME_INPUT_UNIT + ".FileDataField",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    ColumnType FILE_NAME_FIELD = new ColumnType(TABLENAME_INPUT_UNIT + ".FileNameField",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType TWO_DATES_FIELD_1 = new ColumnType(TABLENAME_INPUT_UNIT + ".TwoDatesField1",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
    ColumnType TWO_DATES_FIELD_2 = new ColumnType(TABLENAME_INPUT_UNIT + ".TwoDatesField2",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL);
}
