package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrobook;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 30.05.2017.
 */
public interface IAHCodeTableManager {

    // Table Skeleton Identification
    String TABLE_NAME_GRAVE_DESCRIPTION = "grave_description";
    String TABLE_NAME_TAPHONOMY_COMPLETENESS = "taphonomy_completeness";
    String TABLE_NAME_TAPHONOMY_SURFACE = "taphonomy_surface";
    String TABLE_NAME_TAPHONOMY_FRAGMENTATION = "taphonomy_fragmentation";

    // Table Dying Age
    String TABLE_NAME_APEX = "apex";
    String TABLE_NAME_FACIES_SYMPHYSIALIS = "facies_symphysialis";
    String TABLE_NAME_MACROPOROSITY = "macroporosity";
    String TABLE_NAME_MICROPOROSITY = "microporosity";
    String TABLE_NAME_SURFACE_ORGANISATION = "surface_organisation";
    String TABLE_NAME_SURFACE_TEXTURE = "surface_texture";
    String TABLE_NAME_OS = "os_frontale";
    String TABLE_NAME_EST_AGE_GROUP = "est_age_group";

    // Table Teeth
    String TABLENAME_TEETH_INVENTORY = "teeth_status";
    String TABLENAME_TEETH_CARIES = "teeth_caries";
    String TABLENAME_TEETH_CALCULUS = "teeth_calculus";
    String TABLENAME_TEETH_ABRASION = "teeth_abrasion";
    String TABLENAME_TEETH_LEH_POWER = "teeth_leh_power";

    // Table Age Sex
    String TABLENAME_EPIAPO_CONDITION = "agesex_epiapo_condition";

    // unsorted
    String TABLE_NAME_TOOTH_ERUPTION = "tooth_eruption";

    //
    String TABLE_NAME_VERTEBRA_BODY = "vertebra_body";
    String TABLE_NAME_VERTEBRA_DEFINITION = "vertebra_definition";
    String TABLE_NAME_VERTEBRA_JOINT = "vertebra_joint";
    String TABLE_NAME_VERTEBRA_SCHMORLS_NODES_POSITION = "vertebra_schmorls_nodes_position";
    String TABLE_NAME_VERTEBRA_SCHMORLS_NODES_SIDE = "vertebra_schmorls_nodes_side";
    String TABLE_NAME_SEX = "sex";
    String TABLE_NAME_SEX_SULCUS_PRAEAURICULARIS = "sex_sulcus_praeauricularis";

    // Table Osteometrie
    String TABLE_NAME_JOINT_STATUS = "joint_status";

    // Table Characteristics
    String TABLE_NAME_CHARACTERISTICS_OPTIONS = "characteristics_option";
    String TABLE_NAME_CRIBA_ORBITALIA = "cribra_orbitalia";


}
