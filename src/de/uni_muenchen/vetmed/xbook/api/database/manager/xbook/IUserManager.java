/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.uni_muenchen.vetmed.xbook.api.database.manager.xbook;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;

/**
 *
 * @author lohrer
 */
public interface IUserManager {

    String TABLENAME_USER = IStandardColumnTypes.DATABASE_NAME_GENERAL + ".user";
    String DISPLAYNAME = TABLENAME_USER + ".Value";
    String USER_UID = TABLENAME_USER + ".ID";
    String USERNAME = TABLENAME_USER + ".UserName";
    String ORGANISATION = TABLENAME_USER + ".Organisation";
    String ADMIN = TABLENAME_USER + ".Admin";
    String SEARCHABLE = TABLENAME_USER + ".Searchable";
    int ISDEVELOPER = 2;
    int ISADMIN = 1;
    int ISUSER = 0;
}
