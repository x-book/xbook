/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrobook;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractExtendedEntryManager;

/**
 *
 * @author Anja Mösch
 */
public interface IAHBoneElementManager {

	/**
     * the Name of the Bone Definition Table
     */
    public static final String TABLE_NAME_BONE_DEFINITION = "bone_definition";
    /**
     * the Name of the Bone Table
     */
    public static final String TABLE_NAME_BONE = "bone";
    
    /**
     * the Name of the Bone description Column; This is the description
     * of the Bone
     */
    ColumnType BONE_DEFINITION_ID = new ColumnType(TABLE_NAME_BONE_DEFINITION + "." + IStandardColumnTypes.ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    
    /* the Name of the bone ID Column; This is the id of the bone
     * as defined in the bone_defintion
     */
    ColumnType BONE_ID = new ColumnType(TABLE_NAME_BONE + ".Bone",
            ColumnType.Type.ID,
            ColumnType.ExportType.SPECIFIC)
            .setDisplayName(Loc.get("Bone_ID"))
            .setConnectedTableName(TABLE_NAME_BONE_DEFINITION)
            .setSectionProperty(AbstractExtendedEntryManager.SECTION_NO_DISPLAY);
    
    /**
     * the Name of the Bone ALPHABETICAL Column; This is the value of the
     * bone which is entered by the user
     */
    ColumnType BONE_VALUE = new ColumnType(TABLE_NAME_BONE + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.ID,
            ColumnType.ExportType.SPECIFIC)
            .setDisplayName(Loc.get("BONE"))
            .setSectionProperty(IAHInputUnitManager.PROPERTY_SECTION3)
            .setUnique(true);
    
    ColumnType BONE_DEFINITION_DESCRIPTION = new ColumnType(TABLE_NAME_BONE_DEFINITION + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL); 
    
    ColumnType BONE_STATUS = new ColumnType(TABLE_NAME_BONE + "." + IStandardColumnTypes.STATUS,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    
    ColumnType BONE_NACHRICHTENNUMMER = new ColumnType(TABLE_NAME_BONE + "." + IStandardColumnTypes.MESSAGE_NUMBER,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    
    ColumnType MEASUREMENTS_DELETED = new ColumnType(TABLE_NAME_BONE + "." + IStandardColumnTypes.DELETED,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
}
