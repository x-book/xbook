package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IADBoxManager extends IStandardInputUnitColumnTypes {

    /* ======================================================================
     * GENERAL
     * ====================================================================== */
    ColumnType BOX_NUMBER = new ColumnType(TABLENAME_INPUT_UNIT + ".BoxNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("BOX_NUMBER"))
            .setShowInProjectSearch(true)
            .setMaxInputLength(100)
            .setPriority(11)
            .setMandatory();
    ColumnType BOX_NUMBER_EXTERN = new ColumnType(
            TABLENAME_INPUT_UNIT + ".BoxNumber",
            ColumnType.Type.EXTERN_VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("BOX_NUMBER"))
            .setForListing(true)
            .setPriority(18);
    ColumnType STICKER = new ColumnType(TABLENAME_INPUT_UNIT + ".Sticker",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("STICKER"))
            .setShowInProjectSearch(true)
            .setMaxInputLength(100);

    /* ======================================================================
     * LOCATION
     * ====================================================================== */
    ColumnType ROOM_EXTERN = new ColumnType(TABLENAME_INPUT_UNIT + ".Room",
            ColumnType.Type.EXTERN_VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("ROOM"))
              .setMaxInputLength(50)
            .setPriority(14)
            .setShowInProjectSearch(true);
    ColumnType SHELF_EXTERN = new ColumnType(TABLENAME_INPUT_UNIT + ".Shelf",
            ColumnType.Type.EXTERN_VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("SHELF"))

            .setMaxInputLength(50)
            .setPriority(15)
            .setShowInProjectSearch(true);
    ColumnType TRAY_EXTERN = new ColumnType(TABLENAME_INPUT_UNIT + ".Tray",
            ColumnType.Type.EXTERN_VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("TRAY"))
            .setMaxInputLength(50)
            .setPriority(16)
            .setShowInProjectSearch(true);

    /* ======================================================================
    * LOCATION_EXTERN
    * ====================================================================== */
    ColumnType ROOM = new ColumnType(TABLENAME_INPUT_UNIT + ".Room",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("ROOM"))
            .setMandatory(true)
            .setMaxInputLength(50)
            .setPriority(14)
            .setShowInProjectSearch(true);
    ColumnType SHELF = new ColumnType(TABLENAME_INPUT_UNIT + ".Shelf",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("SHELF"))
            .setMandatory(true)
            .setMaxInputLength(50)
            .setPriority(15)
            .setShowInProjectSearch(true);
    ColumnType TRAY = new ColumnType(TABLENAME_INPUT_UNIT + ".Tray",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("TRAY"))
            .setMaxInputLength(50)
            .setPriority(16)
            .setShowInProjectSearch(true);

    /* ======================================================================
     * LOAN
     * ====================================================================== */
    ColumnType LOAN = new ColumnType(TABLENAME_INPUT_UNIT + ".Loan",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN"));
    ColumnType LOAN_TO = new ColumnType(TABLENAME_INPUT_UNIT + ".LoanTo",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_TO"))
            .setShowInProjectSearch(true)
            .setMaxInputLength(100);
    ColumnType LOAN_TO_CONTACT = new ColumnType(TABLENAME_INPUT_UNIT + ".LoanToContact",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_TO_CONTACT"))
            .setShowInProjectSearch(true);
    ColumnType LOAN_FROM = new ColumnType(TABLENAME_INPUT_UNIT + ".LoanFrom",
            ColumnType.Type.DATE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_FROM"))
            .setShowInProjectSearch(true);
    ColumnType LOAN_UNTIL = new ColumnType(TABLENAME_INPUT_UNIT + ".LoanUntil",
            ColumnType.Type.DATE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_UNTIL"))
            .setShowInProjectSearch(true);
    ColumnType LOAN_CLARIFICATION_NEEDED = new ColumnType(TABLENAME_INPUT_UNIT + ".LoanClarificationNeeded",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_CLARIFICATION_NEEDED"));
    ColumnType LOAN_NOTE = new ColumnType(TABLENAME_INPUT_UNIT + ".LoanNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_NOTE"))
            .setShowInProjectSearch(true);

}
