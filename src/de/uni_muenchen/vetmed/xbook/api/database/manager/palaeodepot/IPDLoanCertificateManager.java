package de.uni_muenchen.vetmed.xbook.api.database.manager.palaeodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IPDLoanCertificateManager {

    String TABLE_NAME_LOAN_CERTIFICATE = "loan_certificate";

    ColumnType LOAN_CERTIFICATE_HASH = new ColumnType(TABLE_NAME_LOAN_CERTIFICATE + ".Hash",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    ColumnType LOAN_CERTIFICATE_NAME = new ColumnType(TABLE_NAME_LOAN_CERTIFICATE + ".Name",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    ColumnType LOAN_CERTIFICATE_VALUE = new ColumnType(TABLE_NAME_LOAN_CERTIFICATE + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE)
            .setDisplayName(Loc.get("LOAN_CERTIFICATE"))
            .setMaxInputLength(2 * 1024 * 1024);
}
