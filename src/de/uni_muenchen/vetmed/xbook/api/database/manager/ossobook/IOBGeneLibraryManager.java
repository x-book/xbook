package de.uni_muenchen.vetmed.xbook.api.database.manager.ossobook;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * Created by lohrer on 14.09.2015.
 */
public interface IOBGeneLibraryManager {

    String TABLE_NAME_GENE_LIBARY = "genbank";
    ColumnType GENE_LIBRARY_ID = new ColumnType(IOBGeneLibraryManager.TABLE_NAME_GENE_LIBARY + "." + IStandardColumnTypes.ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType GENE_LIBRARY_DBNUMBER = new ColumnType(TABLE_NAME_GENE_LIBARY + "." + IStandardColumnTypes.DATABASE_ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType GENE_LIBRARY_PROJECT_ID = new ColumnType(TABLE_NAME_GENE_LIBARY + "." + IStandardColumnTypes.PROJECT_ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType GENE_LIBRARY_PROJECT_DATABASENUMBER = new ColumnType(TABLE_NAME_GENE_LIBARY + "." + IStandardColumnTypes.PROJECT_DATABASE_ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType GENE_LIBRARY_VALUE = new ColumnType(TABLE_NAME_GENE_LIBARY + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL);
    ColumnType GENE_LIBRARY_STATUS = new ColumnType(TABLE_NAME_GENE_LIBARY + "." + IStandardColumnTypes.STATUS,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType GENE_LIBRARY_MESSAGESNUMBER = new ColumnType(TABLE_NAME_GENE_LIBARY + "." + IStandardColumnTypes.MESSAGE_NUMBER,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType GENE_LIBRARY_DELETED = new ColumnType(TABLE_NAME_GENE_LIBARY + "." + IStandardColumnTypes.DELETED,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
}
