package de.uni_muenchen.vetmed.xbook.api.database.manager;

import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by lohrer on 01.06.2015.
 */
public interface BaseEntryManager extends IBaseManager {

    void loadBase(DataSetOld entryData) throws StatementNotExecutedException;
    void loadBase(DataSetOld entryData, String[] tableFilter) throws StatementNotExecutedException;

    String getLocalisedTableName();

    void deleteValue(Key key, Key projectKey) throws StatementNotExecutedException;

    ExportResult getEntriesForListing(DataSetOld project, ArrayList<ColumnType> list, ColumnType.ExportType type, boolean conflict, int offset, int count, HashMap<ColumnType, SearchEntryInfo> filter) throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException;

    ExportResult getEntriesForDisplay(Key projectKey, Key entryKey) throws StatementNotExecutedException;

    /**
     * Checks if the current input for a specific column type is already
     * existing in the local database.
     *
     * @param columnType The column type for that the value should be checked.
     * @param value      The value to check.
     * @param projectKey The key of the project that is loaded.
     * @return <code>true</code> if the value is already saved in the database,
     * <code>false</code> else.
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException
     */
    ArrayList<Key> getExistingEntriesForValue(ColumnType columnType, String value, Key projectKey) throws StatementNotExecutedException;

    ExportResult getEntries(DataSetOld project, ArrayList<ColumnType> list, ColumnType.ExportType type, boolean conflict, int offset, int count, HashMap<ColumnType, SearchEntryInfo> filter, boolean hideNonDefaultFields)
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException;

    ExportResult getEntries(DataSetOld project, ArrayList<ColumnType> list, ColumnType.ExportType type, boolean conflict, int offset, int count, ArrayList<Key> entries, HashMap<ColumnType, SearchEntryInfo> filter, boolean hideNonDefaultFields)
            throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException;
}
