package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IADLoanAgreementManager {

    String TABLE_NAME_LOAN_AGREEMENT = "loan_agreement";

    ColumnType LOAN_AGREEMENT_HASH = new ColumnType(TABLE_NAME_LOAN_AGREEMENT + ".ID",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    
    ColumnType LOAN_AGREEMENT_NAME = new ColumnType(TABLE_NAME_LOAN_AGREEMENT + ".Name",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    
    ColumnType LOAN_AGREEMENT_VALUE = new ColumnType(TABLE_NAME_LOAN_AGREEMENT + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE)
            .setMaxInputLength(4*1024*1024)//TODO war das nicht mal drin?
            .setDisplayName(Loc.get("LOAN_AGREEMENT"));
}
