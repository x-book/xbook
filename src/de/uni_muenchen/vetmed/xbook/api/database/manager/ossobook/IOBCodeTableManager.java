package de.uni_muenchen.vetmed.xbook.api.database.manager.ossobook;

/**
 * Created by lohrer on 14.09.2015.
 */
public interface IOBCodeTableManager {
    String TABLENAME_AGE_1_BASEL = "age1basel";
    String TABLENAME_AGE_2_BASEL = "age2basel";
    String TABLENAME_AGE_GROUP_DESCRIPT = "agegroupdescript";
    String TABLENAME_WEARSTAGE_ALTERNATIVE = "wearstage_alternative";
    String TABLENAME_WEARSTAGE_GPM = "wearstage";
    String TABLENAME_BODY_SIDE = "bodyside";
    String TABLENAME_BONE_FUSION = "bonefusion";
    String TABLENAME_SEX = "sex";
    String TABLENAME_ESTIMATED_SIZE = "estimatedsize";
    String TABLENAME_FRACTURE_EDGE = "fractureedge";
    String TABLENAME_FRACTURE_EDGE_2 = "fractureedge2";
    String TABLENAME_SURFACE_PRESERVATION = "surfacepreservation";
    String TABLENAME_SURFACE_PRESERVATION_2 = "surfacepreservation2";
    String TABLENAME_ROOT_EDGING = "rootedging";
    String TABLENAME_ENCRUSTATION = "encrustation";
    String TABLENAME_FATTY_GLOSS = "fattygloss";
    String TABLENAME_SAMPLE_QUALITY = "samplequality";
    String TABLENAME_DECOLOURATION_PATINA = "decolourationpatina";
    String TABLENAME_TRACES_OF_BURNING = "tracesofburning";
    String TABLENAME_GNAWING = "gnawing";
    String TABLENAME_BUTCHERING_MARK_1 = "butcheringmark1";
    String TABLENAME_BUTCHERING_MARK_2 = "butcheringmark2";
    String TABLENAME_SEASON = "season";
    String TABLENAME_AGEDETERMINATIONPERIOD = "agedeterminationperiod";
    String ANIMAL = "animal";
    String WEARSTAGE = "wearstage";
    String FRACTION = "fraction";
}
