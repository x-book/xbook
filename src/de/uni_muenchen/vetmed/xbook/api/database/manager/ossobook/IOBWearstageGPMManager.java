package de.uni_muenchen.vetmed.xbook.api.database.manager.ossobook;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IDefaultSectionAssignments;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * Created by lohrer on 14.09.2015.
 */
public interface IOBWearstageGPMManager extends IStandardColumnTypes, IDefaultSectionAssignments {

    String TABLENAME_WEARSTAGE_GPM = "wearstage";
    String TABLENAME_WEARSTAGE_GPM_VALUES = "wearstagevalues";
    ColumnType WEARSTAGE_GPM_ID = new ColumnType(IOBWearstageGPMManager.TABLENAME_WEARSTAGE_GPM + "." + ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType WEARSTAGE_GPM_ANIMAL = new ColumnType(TABLENAME_WEARSTAGE_GPM + ".animal",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType WEARSTAGE_GPM_SKELETONELEMENT = new ColumnType(TABLENAME_WEARSTAGE_GPM + ".skeletonelement",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType WEARSTAGE_GPM_LANGUAGE = new ColumnType(TABLENAME_WEARSTAGE_GPM + ".language",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType WEARSTAGE_GPM_VALUE = new ColumnType(TABLENAME_WEARSTAGE_GPM + ".value",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType WEARSTAGE_GPM_VALUES_ID = new ColumnType(TABLENAME_WEARSTAGE_GPM_VALUES + "." + ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType WEARSTAGE_GPM_VALUES_DBNUMBER = new ColumnType(TABLENAME_WEARSTAGE_GPM_VALUES + "." + DATABASE_ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType WEARSTAGE_GPM_VALUES_PROJECT_ID = new ColumnType(TABLENAME_WEARSTAGE_GPM_VALUES + "." + PROJECT_ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType WEARSTAGE_GPM_VALUES_PROJECT_DATABASENUMBER = new ColumnType(TABLENAME_WEARSTAGE_GPM_VALUES + "." + PROJECT_DATABASE_ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType WEARSTAGE_GPM_VALUES_WEARSTAGE = new ColumnType(TABLENAME_WEARSTAGE_GPM_VALUES + ".Wearstage",
            ColumnType.Type.SUBQUERY_SPECIFIC,
            ColumnType.ExportType.GENERAL);
}
