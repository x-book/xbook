package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

public interface IADFindsLabelNumberManager {

    String TABLENAME_FINDSLABELNUMBER = "findslabelnumbers";

    ColumnType FINDSLABELNUMBER_VALUE = new ColumnType(TABLENAME_FINDSLABELNUMBER + ".Value", ColumnType.Type.VALUE, ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("FINDSLABELNUMBER"))
            .setPriority(12)
			.setShowInProjectSearch(true);
}
