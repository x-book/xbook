package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IADAdmissionFromManager {

    String TABLE_NAME_ADMISSION_FORM = "admission_form";

    ColumnType ADMISSION_FORM_HASH = new ColumnType(TABLE_NAME_ADMISSION_FORM + ".ID" ,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    ColumnType ADMISSION_FORM_NAME = new ColumnType(TABLE_NAME_ADMISSION_FORM + ".Name",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE);
    ColumnType ADMISSION_FORM_VALUE = new ColumnType(TABLE_NAME_ADMISSION_FORM + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE)

            .setMaxInputLength(4*1024*1024)
            .setDisplayName(Loc.get("ADMISSION_FORM"));
}
