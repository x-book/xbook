package de.uni_muenchen.vetmed.xbook.api.database.manager;

import de.uni_muenchen.vetmed.xbook.api.database.manager.xbook.IUserManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractExtendedEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.UserManager;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface IStandardProjectColumnTypes {

    String TABLENAME_PROJECT = "project";
    String PROJECT_DELETED = TABLENAME_PROJECT + "." + IStandardColumnTypes.DELETED;
    String PROJECT_LAST_SYNCRONISATION = TABLENAME_PROJECT + ".LastSynchronized";

    ColumnType PROJECT_DATABASENUMBER = new ColumnType(TABLENAME_PROJECT + ".ProjectDatabaseNumber", ColumnType.Type.VALUE, ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("PROJECT_DATABASENUMBER"))
            .setPriority(4)
            .setSectionProperty(IDefaultSectionAssignments.SECTION_SYSTEM);
    ;

    ColumnType PROJECT_ID_PROJECT = new ColumnType(TABLENAME_PROJECT + ".ProjectID", ColumnType.Type.VALUE, ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("PROJECT_ID"))
            .setPriority(3)
            .setSectionProperty(IDefaultSectionAssignments.SECTION_SYSTEM);
    ;

    /* Important: Some settings are overwritten in the inherited classes, e.g. the max input length is overwritten in OssoBook and ExcaBook. */
    ColumnType PROJECT_PROJECTNAME = new ColumnType(TABLENAME_PROJECT + ".ProjectName", ColumnType.Type.VALUE, ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("PROJECT_NAME"))
            .setPriority(1)
            .setMaxInputLength(50)
            .setMandatory(true)
            .setShowInProjectSearch(true);

    ColumnType PROJECT_PROJECTOWNER = new ColumnType(TABLENAME_PROJECT + ".ProjectOwner", ColumnType.Type.ID, ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("PROJECT_OWNER"))
            .setConnectedTableName(IUserManager.TABLENAME_USER)
            .setPriority(2)
            .setMaxInputLength(128)
            .setConnectedTableName(UserManager.TABLENAME_USER);



}
