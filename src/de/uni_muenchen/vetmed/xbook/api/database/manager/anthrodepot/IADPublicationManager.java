package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

public interface IADPublicationManager {
    /**
     * The name of the "animal" table, this is the lookup table for animals.
     */
    String TABLE_NAME_PUBLICATION_VALUES = "publication";

    /**
     * The
     */
    ColumnType PUBLICATION_VALUES_VALUE = new ColumnType(TABLE_NAME_PUBLICATION_VALUES + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("PUBLICATION"));
}
