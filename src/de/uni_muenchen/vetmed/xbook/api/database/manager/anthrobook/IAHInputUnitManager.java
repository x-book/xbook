package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrobook;

import de.uni_muenchen.vetmed.xbook.api.Loc;

import static de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

import static de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractExtendedEntryManager.PRIORITY;

/**
 * @author Anja Mösch
 */
public interface IAHInputUnitManager {

    public static final ColumnType.SectionAssignment PROPERTY_SECTION1 = new ColumnType.SectionAssignment(1, "SECTION 1");
    public static final ColumnType.SectionAssignment PROPERTY_SECTION2 = new ColumnType.SectionAssignment(1, "SECTION 2");
    public static final ColumnType.SectionAssignment PROPERTY_SECTION3 = new ColumnType.SectionAssignment(1, "SECTION 3");

    // Entry Database column names (1st section)
    ColumnType GRAVE_NUMBER = new ColumnType(TABLENAME_INPUT_UNIT + ".grave_number", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setSectionProperty(PROPERTY_SECTION1)
            .setPriority(PRIORITY++)
            .setMaxInputLength(50)
            .setDisplayName(Loc.get("GRAVE_NUMBER"))
            .setMandatory(true)
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>GRAVE_NUMBER"));
    ColumnType GRAVE_NUMBER_EXTERN = new ColumnType(TABLENAME_INPUT_UNIT + ".grave_number", ColumnType.Type.EXTERN_VALUE, ColumnType.ExportType.GENERAL)
            .setSectionProperty(PROPERTY_SECTION1)
            .setPriority(PRIORITY++)
            .setMaxInputLength(50)
            .setDisplayName(Loc.get("GRAVE_NUMBER"))
            .setMandatory(true)
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>GRAVE_NUMBER"));
    ColumnType FEATURE_NUMBER = new ColumnType(TABLENAME_INPUT_UNIT + ".feature_number", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("FEATURE_NUMBER"))
            .setSectionProperty(PROPERTY_SECTION1)
            .setMandatory(true)
            .setPriority(PRIORITY++)
            .setMaxInputLength(50)
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>FEATURE_NUMBER"));
    ColumnType FINDSHEET_NUMBER = new ColumnType(TABLENAME_INPUT_UNIT + ".findsheet_number", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("FINDSHEET_NUMBER"))
            .setSectionProperty(PROPERTY_SECTION1)
            .setPriority(PRIORITY++)
            .setMaxInputLength(50)
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>FINDSHEET_NUMBER"));
    ColumnType GRAVE_TYPE = new ColumnType(TABLENAME_INPUT_UNIT + ".grave_type", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("GRAVE_TYPE"))
            .setSectionProperty(PROPERTY_SECTION1)
            .setPriority(PRIORITY++)
            .setMaxInputLength(50)
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>GRAVE_TYPE"));
    ColumnType GRAVE_DESCRIPTION = new ColumnType(TABLENAME_INPUT_UNIT + ".grave_description", ColumnType.Type.ID, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("GRAVE_DESCRIPTION"))
            .setSectionProperty(PROPERTY_SECTION1)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_GRAVE_DESCRIPTION)
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>GRAVE_DESCRIPTION"));
    ColumnType AGE_DETERMINATION = new ColumnType(TABLENAME_INPUT_UNIT + ".age_determination", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("AGE_DETERMINATION"))
            .setSectionProperty(PROPERTY_SECTION1)
            .setPriority(PRIORITY++)
            .setMaxInputLength(Integer.MAX_VALUE)
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>AGE_DETERMINATION"));
    ColumnType DEPOT_NUMBER = new ColumnType(TABLENAME_INPUT_UNIT + ".depot_number", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("DEPOT_NUMBER"))
            .setSectionProperty(PROPERTY_SECTION1)
            .setPriority(PRIORITY++)
            .setMaxInputLength(50)
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>LOCATION_MAGAZIN"));
    ColumnType HIRE_OUT = new ColumnType(TABLENAME_INPUT_UNIT + ".hire_out", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("HIRE_OUT"))
            .setSectionProperty(PROPERTY_SECTION1)
            .setPriority(PRIORITY++)
            .setMaxInputLength(50)
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>HIRE_OUT"));

    ColumnType SKULL_COMPLETENESS = new ColumnType(TABLENAME_INPUT_UNIT + ".SkullCompleteness",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_COMPLETENESS)
            .setDisplayName(Loc.get("SKULL") + " " + Loc.get("COMPLETENESS"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>COMPLETENESS"));
    ColumnType TEETH_COMPLETENESS = new ColumnType(TABLENAME_INPUT_UNIT + ".TeethCompleteness",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_COMPLETENESS)
            .setDisplayName(Loc.get("TEETH") + " " + Loc.get("COMPLETENESS"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>COMPLETENESS"));
    ColumnType MANDIBULA_COMPLETENESS = new ColumnType(TABLENAME_INPUT_UNIT + ".MandibulaCompleteness",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_COMPLETENESS)
            .setDisplayName(Loc.get("MANDIBULA") + " " + Loc.get("COMPLETENESS"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>COMPLETENESS"));
    ColumnType MAXILLA_COMPLETENESS = new ColumnType(TABLENAME_INPUT_UNIT + ".MaxillaCompleteness",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_COMPLETENESS)
            .setDisplayName(Loc.get("MAXILLA") + " " + Loc.get("COMPLETENESS"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>COMPLETENESS"));
    ColumnType POSTCRANIUM_COMPLETENESS = new ColumnType(TABLENAME_INPUT_UNIT + ".PostcraniumCompleteness",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_COMPLETENESS)
            .setDisplayName(Loc.get("POSTCRANIUM") + " " + Loc.get("COMPLETENESS"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>COMPLETENESS"));

    ColumnType SKULL_SURFACE = new ColumnType(TABLENAME_INPUT_UNIT + ".SkullSurface",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_SURFACE)
            .setDisplayName(Loc.get("SKULL") + " " + Loc.get("SURFACE"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>SURFACE"));
    ColumnType TEETH_SURFACE = new ColumnType(TABLENAME_INPUT_UNIT + ".TeethSurface",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_SURFACE)
            .setDisplayName(Loc.get("TEETH") + " " + Loc.get("SURFACE"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>SURFACE"));
    ColumnType MANDIBULA_SURFACE = new ColumnType(TABLENAME_INPUT_UNIT + ".MandibulaSurface",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_SURFACE)
            .setDisplayName(Loc.get("MANDIBULA") + " " + Loc.get("SURFACE"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>SURFACE"));
    ColumnType MAXILLA_SURFACE = new ColumnType(TABLENAME_INPUT_UNIT + ".MaxillaSurface",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_SURFACE)
            .setDisplayName(Loc.get("MAXILLA") + " " + Loc.get("SURFACE"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>SURFACE"));
    ColumnType POSTCRANIUM_SURFACE = new ColumnType(TABLENAME_INPUT_UNIT + ".PostcraniumSurface",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_SURFACE)
            .setDisplayName(Loc.get("POSTCRANIUM") + " " + Loc.get("SURFACE"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>SURFACE"));

    ColumnType SKULL_FRAGMENTATION = new ColumnType(TABLENAME_INPUT_UNIT + ".SkullFragmentation",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_FRAGMENTATION)
            .setDisplayName(Loc.get("SKULL") + " " + Loc.get("FRAGMENTATION"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>FRAGMENTATION"));
    ColumnType TEETH_FRAGMENTATION = new ColumnType(TABLENAME_INPUT_UNIT + ".TeethFragmentation",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_FRAGMENTATION)
            .setDisplayName(Loc.get("TEETH") + " " + Loc.get("FRAGMENTATION"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>FRAGMENTATION"));
    ColumnType MANDIBULA_FRAGMENTATION = new ColumnType(TABLENAME_INPUT_UNIT + ".MandibulaFragmentation",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_FRAGMENTATION)
            .setDisplayName(Loc.get("MANDIBULA") + " " + Loc.get("FRAGMENTATION"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>FRAGMENTATION"));
    ColumnType MAXILLA_FRAGMENTATION = new ColumnType(TABLENAME_INPUT_UNIT + ".MaxillaFragmentation",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_FRAGMENTATION)
            .setDisplayName(Loc.get("MAXILLA") + " " + Loc.get("FRAGMENTATION"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>FRAGMENTATION"));
    ColumnType POSTCRANIUM_FRAGMENTATION = new ColumnType(TABLENAME_INPUT_UNIT + ".PostcraniumFragmentation",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setConnectedTableName(IAHCodeTableManager.TABLE_NAME_TAPHONOMY_FRAGMENTATION)
            .setDisplayName(Loc.get("POSTCRANIUM") + " " + Loc.get("FRAGMENTATION"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>FRAGMENTATION"));

    ColumnType DISCOLORATION = new ColumnType(TABLENAME_INPUT_UNIT + ".Discoloration",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setDisplayName(Loc.get("DISCOLORATIONS"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>DISCOLORATIONS"));
    ColumnType TAPHONOMIC_CHARACTERISTICS = new ColumnType(TABLENAME_INPUT_UNIT + ".TaphonomicCharacteristics",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setPriority(PRIORITY++)
            .setDisplayName(Loc.get("TAPHONOMIC_CHARACTERISTICS"))
            .setTableSidebarText(Loc.get("SIDEBAR_ENTRY>SKELETON_IDENTIFICATION>TAPHONOMIC_CHARACTERISTICS"));
}