package de.uni_muenchen.vetmed.xbook.api.database.manager.palaeodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardProjectColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IPDProjectManager extends IStandardProjectColumnTypes {


    ColumnType INPUT_NUMBER = new ColumnType(TABLENAME_PROJECT + ".InputNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMandatory(true)
			.setShowInProjectSearch(true)
			.setDisplayName(Loc.get("INPUT_NUMBER"));
    ColumnType EXCAVATION_NUMBER = new ColumnType(TABLENAME_PROJECT + ".ExcavationNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setMandatory(true)
			.setDisplayName(Loc.get("EXCAVATION_NUMBER"))
			.setShowInProjectSearch(true);
    ColumnType DATE_OF_RECEIPT = new ColumnType(TABLENAME_PROJECT + ".DateOfReceipt",
            ColumnType.Type.DATE,
            ColumnType.ExportType.GENERAL)
			.setDisplayName(Loc.get("DATE_OF_RECEIPT"));
    ColumnType LAND_PARCEL_NUMBER = new ColumnType(TABLENAME_PROJECT + ".LandParcelNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
			.setDisplayName(Loc.get("LAND_PARCEL_NUMBER"));
    ColumnType COUNTRY = new ColumnType(TABLENAME_PROJECT + ".Country",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
			.setDisplayName(Loc.get("COUNTRY"))
			.setShowInProjectSearch(true);
    ColumnType STATE = new ColumnType(TABLENAME_PROJECT + ".State",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
			.setDisplayName(Loc.get("STATE"));
    ColumnType PROVINCE = new ColumnType(TABLENAME_PROJECT + ".Province",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
			.setDisplayName(Loc.get("PROVINCE"));
    ColumnType COUNTY = new ColumnType(TABLENAME_PROJECT + ".County",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
			.setDisplayName(Loc.get("COUNTY"));
//    ColumnType EXCAVATION_YEAR = new ColumnType(TABLENAME_PROJECT + ".ExcavationYear",
//            ColumnType.Type.VALUE,
//            ColumnType.ExportType.GENERAL)
//			.setDisplayName(Loc.get("EXCAVATION_YEAR"));
    ColumnType CONDITION = new ColumnType(TABLENAME_PROJECT + ".Condition",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
			.setDisplayName(Loc.get("CONDITION"));
    ColumnType OWNER = new ColumnType(TABLENAME_PROJECT + ".Owner",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
			.setDisplayName(Loc.get("OWNER"));
    ColumnType DELIVERER = new ColumnType(TABLENAME_PROJECT + ".Deliverer",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
			.setDisplayName(Loc.get("DELIVERER"));
    ColumnType EXCAVATOR = new ColumnType(TABLENAME_PROJECT + ".Excavator",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
			.setDisplayName(Loc.get("EXCAVATOR"))
			.setShowInProjectSearch(true);
    ColumnType PERMANENT_LOCATION = new ColumnType(TABLENAME_PROJECT + ".PermanentLocation",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
			.setDisplayName(Loc.get("PERMANENT_LOCATION"));
    ColumnType PUBLICATIONS = new ColumnType(TABLENAME_PROJECT + ".Publications",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
			.setDisplayName(Loc.get("PUBLICATIONS"));

}
