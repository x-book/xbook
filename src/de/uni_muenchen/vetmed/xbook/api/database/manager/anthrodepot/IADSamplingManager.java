package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

public interface IADSamplingManager {

    String TABLE_NAME_SAMPLING = "sampling";

    ColumnType SAMPLING_VALUE = new ColumnType(TABLE_NAME_SAMPLING + "." + IStandardColumnTypes.VALUE,
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("SAMPLING"));
}
