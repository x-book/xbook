package de.uni_muenchen.vetmed.xbook.api.database.manager.inbook;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 *
 * @author ciaran.harrington@extern.lrz-muenchen.de
 */
//public interface IOBInputUnitManager extends IStandardInputUnitColumnTypes {
public interface IIBInputUnitManager extends IStandardInputUnitColumnTypes {
    ColumnType.SectionAssignment PROPERTY_SECTION1 = new ColumnType.SectionAssignment(1, Loc.get("SECTION_1"));
    ColumnType.SectionAssignment PROPERTY_SECTION2 = new ColumnType.SectionAssignment(2, Loc.get("SECTION_2"));
    ColumnType.SectionAssignment PROPERTY_SECTION3 = new ColumnType.SectionAssignment(3, Loc.get("SECTION_3"));
    ColumnType.SectionAssignment PROPERTY_SECTION4 = new ColumnType.SectionAssignment(4, Loc.get("SECTION_4"));    

    ColumnType E_NUMBER = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".ENumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("E_NUMBER"));
    ColumnType COLLECTION = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Collection",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("COLLECTION"));   
    ColumnType REGISTRATION_NUMBER = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".RegistrationNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("REGISTRATION_NUMBER"));
    ColumnType ACQUISITION_DATE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".AcquisitionDate",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("ACQUISITION_DATE"));
    ColumnType RESTORATION_NUMBER = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".RestorationNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("RESTORATION_NUMBER"));
    ColumnType NOTES = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Notes",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("NOTES"));    
    ColumnType FIND_SPOT = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".FindSpot",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("FIND_SPOT"));
    ColumnType FIND_CIRCUMSTANCES = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".FindCircumstances",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("FIND_CIRCUMSTANCES"));
    ColumnType OBJECT_DESCRIPTION = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".ObjectDescription",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("OBJECT_DESCRIPTION"));
    ColumnType NUMBER_OF_OBJECTS = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".NumberOfObjects",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("NUMBER_OF_OBJECTS"));
    ColumnType ASM_NUMBER = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".ASMnumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("ASM_NUMBER"));
    ColumnType BLFD_NUMBER = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".BLFDnumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("BLFD_NUMBER"));
    ColumnType ACQUIRED_FROM = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".AcquiredFrom",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("ACQUIRED_FROM"));
    ColumnType ACQUIRED_FROM_INSTITUTE_ID = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".AcquiredFromInstituteID",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("ACQUIRED_FROM_INSTITUTE_ID"));
    ColumnType ACQUIRED_FROM_NAME = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".AcquiredFromName",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("ACQUIRED_FROM_NAME"));
    ColumnType ACQUIRED_FROM_ADDRESS1 = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".AcquiredFromAddress1",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("ACQUIRED_FROM_ADDRESS1"));
    ColumnType ACQUISITION_TYPE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".AcquisitionType",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("ACQUISITION_TYPE"));
    ColumnType ACQUISITION_NOTE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".AcquisitionNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("ACQUISITION_NOTE"));
        ColumnType STATUS_OF_OWNERSHIP = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".StatusOfOwnership",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("STATUS_OF_OWNERSHIP"));
        ColumnType NOTE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Note",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("NOTE"));
        ColumnType COMPLETE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Complete",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("COMPLETE"));        
        ColumnType VERIFIED = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".Verified",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("VERIFIED"));
        ColumnType CHECKED = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".ObjectsChecked",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("OBJECT_CLARIFICATION_NEEDED"));
        ColumnType OBJECTS_IMPORTANTCHANGE = new ColumnType(IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT + ".ImportantChanges",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL).setDisplayName(Loc.get("IMPORTANT_CHANGES"));   

}
