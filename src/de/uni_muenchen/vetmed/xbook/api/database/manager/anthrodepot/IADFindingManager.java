package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.manager.xbook.IUserManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IADFindingManager extends IStandardInputUnitColumnTypes {

    String TABLENAME_FINDING = "finding";

    /* ======================================================================
     * GENERAL
     * ====================================================================== */
    ColumnType BOX_ID = new ColumnType(
            TABLENAME_FINDING + ".BoxID",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("BOX"))
            .setHiddenInListing(true)
            .setMandatory(true);
    ColumnType BOX_DATABASE_NUMBER = new ColumnType(
            TABLENAME_FINDING + ".BoxDatabaseNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.NONE)
            .setHiddenInListing(true)
            .setDisplayName(Loc.get("BOX_DATABASE_NUMBER"));
    ColumnType NOTE = new ColumnType(TABLENAME_FINDING + ".Note",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("NOTE"))
            .setShowInProjectSearch(true)
            .setPriority(19)
            .setMaxInputLength(100);
    ColumnType GRAVENUMBER = new ColumnType(TABLENAME_FINDING + ".GraveNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("GRAVENUMBER"))
            .setShowInProjectSearch(true)
            .setPriority(20)
            .setMaxInputLength(100);
    ColumnType FEATURE_NUMBER = new ColumnType(TABLENAME_FINDING + ".FeatureNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("FEATURE_NUMBER"))
            .setShowInProjectSearch(true)
            .setPriority(30)
            .setMaxInputLength(100);
    ColumnType BURIAL = new ColumnType(TABLENAME_FINDING + ".Burial",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("BURIAL"))
            .setPriority(40)
            .setShowInProjectSearch(true);

    /* ======================================================================
     * LOAN
     * ====================================================================== */
    ColumnType LOAN = new ColumnType(TABLENAME_FINDING + ".Loan",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN"));
    ColumnType LOAN_TO = new ColumnType(TABLENAME_FINDING + ".LoanTo",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_TO"))
            .setShowInProjectSearch(true)
            .setMaxInputLength(100);
    ColumnType LOAN_TO_CONTACT = new ColumnType(TABLENAME_FINDING + ".LoanToContact",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_TO_CONTACT"))
            .setShowInProjectSearch(true);
    ColumnType LOAN_FROM = new ColumnType(TABLENAME_FINDING + ".LoanFrom",
            ColumnType.Type.DATE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_FROM"))
            .setShowInProjectSearch(true);
    ColumnType LOAN_UNTIL = new ColumnType(TABLENAME_FINDING + ".LoanUntil",
            ColumnType.Type.DATE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_UNTIL"))
            .setShowInProjectSearch(true);
    ColumnType LOAN_CLARIFICATION_NEEDED = new ColumnType(TABLENAME_FINDING + ".LoanClarificationNeeded",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_CLARIFICATION_NEEDED"));
    ColumnType LOAN_NOTE = new ColumnType(TABLENAME_FINDING + ".LoanNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_NOTE"))
            .setShowInProjectSearch(true);

    /* ======================================================================
     * DIAGNOSED
     * ====================================================================== */
    ColumnType DIAGNOSED = new ColumnType(TABLENAME_FINDING + ".Diagnosed",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("DIAGNOSED"))
            .setShowInProjectSearch(true);
    ColumnType DIAGNOSED_NAME = new ColumnType(TABLENAME_FINDING + ".DiagnosedName",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("DIAGNOSED_NAME"))
            .setShowInProjectSearch(true)
            .setMaxInputLength(100);
    ColumnType DIAGNOSED_CONTACT = new ColumnType(TABLENAME_FINDING + ".DiagnosedContact",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("DIAGNOSED_CONTACT"))
            .setShowInProjectSearch(true);
    ColumnType FINDINGS_SHEET_BOOLEAN = new ColumnType(TABLENAME_FINDING + ".FindingsSheet",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("FINDINGS_SHEET_BOOLEAN"));

    /* ======================================================================
     * PUBLICATION
     * ====================================================================== */
    ColumnType PUBLICATION_RIGHTS = new ColumnType(TABLENAME_FINDING + ".PublicationRights",
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("PUBLICATION_RIGHTS"))
            .setHtmlDisplayName(Loc.get("PUBLICATION_RIGHTS_HTML"))
            .setShowInProjectSearch(true);
    ColumnType PUBLICATION_RIGHTS_DATE = new ColumnType(TABLENAME_FINDING + ".PublicationRightsDate",
            ColumnType.Type.DATE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("PUBLICATION_RIGHTS_DATE"))
            .setShowInProjectSearch(true);

    /* ======================================================================
     * DIVERSE
     * ====================================================================== */
    ColumnType USER_ID_FINDING = new ColumnType(TABLENAME_FINDING + ".UserID", ColumnType.Type.ID, ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("USER"))
            .setConnectedTableName(IUserManager.TABLENAME_USER)
            .setPriority(5);

}
