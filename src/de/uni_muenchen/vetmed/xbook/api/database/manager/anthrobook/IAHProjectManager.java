package de.uni_muenchen.vetmed.xbook.api.database.manager.anthrobook;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

import static de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardProjectColumnTypes.TABLENAME_PROJECT;

/**
 * @author Anja Mösch
 */
public interface IAHProjectManager {

    ColumnType PROJECT_PROJECT_NUMBER = new ColumnType(TABLENAME_PROJECT + ".ProjectNumber", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("PROJECT_NUMBER")).setMandatory(true);
    ColumnType PROJECT_NUMBER_EXTERN = new ColumnType(TABLENAME_PROJECT + ".ProjectNumber", ColumnType.Type.EXTERN_VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("PROJECT_NUMBER")).setMandatory(true);
    ColumnType PROJECT_EMAIL = new ColumnType(TABLENAME_PROJECT + ".Email", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("EMAIL")).setMandatory(true);
    ColumnType PROJECT_FIND_SITE = new ColumnType(TABLENAME_PROJECT + ".FindSite", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("FIND_SITE"));
    ColumnType PROJECT_FIND_SITE_EXTERN = new ColumnType(TABLENAME_PROJECT + ".FindSite", ColumnType.Type.EXTERN_VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("FIND_SITE"));
    ColumnType PROJECT_TOMB_TYPE = new ColumnType(TABLENAME_PROJECT + ".TombType", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("TOMB_TYPE"));
    ColumnType PROJECT_COUNTRY = new ColumnType(TABLENAME_PROJECT + ".Country", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("COUNTRY")).setMandatory(true);
    ColumnType PROJECT_ROUGH_DATATION = new ColumnType(TABLENAME_PROJECT + ".RoughDatation", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("ROUGH_DATATION"));
    ColumnType PROJECT_DISTRICT = new ColumnType(TABLENAME_PROJECT + ".District", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("DISTRICT"));
    ColumnType PROJECT_DATE = new ColumnType(TABLENAME_PROJECT + ".Date", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("PROJECT_DATE"));
    ColumnType PROJECT_PRESERVATION = new ColumnType(TABLENAME_PROJECT + ".Preservation", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("PRESERVATION"));
    ColumnType PROJECT_PRESERVATION_PLACE = new ColumnType(TABLENAME_PROJECT + ".PreservationPlace", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("PRESERVATION_PLACE"));
    ColumnType PROJECT_PERSON_RESPONSIBLE = new ColumnType(TABLENAME_PROJECT + ".PersonResponsibleName", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("PERSON_RESPONSIBLE"));
    ColumnType PROJECT_PERSON_RESPONSIBLE_CONTACT = new ColumnType(TABLENAME_PROJECT + ".PersonResponsibleContact", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("PERSON_RESPONSIBLE_CONTACT")).setMandatory(true);
    ColumnType PROCESSING_NOTES = new ColumnType(TABLENAME_PROJECT + ".ProcessingNote", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("PROCESSING_NOTES"));
    ColumnType PROJECT_ARCHAEO_CONTACT_PERSON = new ColumnType(TABLENAME_PROJECT + ".ArchaeoContactPerson", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("ARCHAEO_CONTACT_PERSON"));
    ColumnType PROJECT_ARCHAEO_CONTACT_PERSON_CONTACT = new ColumnType(TABLENAME_PROJECT + ".ArchaeoContactPersonContact", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("ARCHAEO_CONTACT_PERSON_CONTACT"));
    ColumnType FEATURING_DATE = new ColumnType(TABLENAME_PROJECT + ".FeaturingDate", ColumnType.Type.VALUE, ColumnType.ExportType.GENERAL).setDisplayName(Loc.get("FEATURING_DATE"));

}
