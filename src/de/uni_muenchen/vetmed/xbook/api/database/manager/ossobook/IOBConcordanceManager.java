package de.uni_muenchen.vetmed.xbook.api.database.manager.ossobook;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * Created by lohrer on 14.09.2015.
 */
public interface IOBConcordanceManager extends IStandardColumnTypes, IBaseManager {

    String TABLENAME_CONCORDANCE = "concordance";
    ColumnType CONCORDANCE_PROJECTID = new ColumnType(IOBConcordanceManager.TABLENAME_CONCORDANCE + "." + PROJECT_ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType CONCORDANCE_PROJECT_DATABASE_NUMBER = new ColumnType(TABLENAME_CONCORDANCE + "." + PROJECT_DATABASE_ID,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType CONCORDANCE_ARCHEOLOGICALUNIT = new ColumnType(TABLENAME_CONCORDANCE + ".ArcheologicalUnit",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("ARCHAEOLOGICAL_UNIT"));
    ColumnType CONCORDANCE_CHRONOLOGY1 = new ColumnType(TABLENAME_CONCORDANCE + ".Chronology1",
            ColumnType.Type.EXTERN_VALUE,
            ColumnType.ExportType.ALL);
    ColumnType CONCORDANCE_CHRONOLOGY2 = new ColumnType(TABLENAME_CONCORDANCE + ".Chronology2",
            ColumnType.Type.EXTERN_VALUE,
            ColumnType.ExportType.ALL);
    ColumnType CONCORDANCE_CHRONOLOGY3 = new ColumnType(TABLENAME_CONCORDANCE + ".Chronology3",
            ColumnType.Type.EXTERN_VALUE, ColumnType.ExportType.ALL);
    ColumnType CONCORDANCE_CHRONOLOGY4 = new ColumnType(TABLENAME_CONCORDANCE + ".Chronology4",
            ColumnType.Type.EXTERN_VALUE, ColumnType.ExportType.ALL);
    ColumnType CONCORDANCE_ZUSTAND = new ColumnType(TABLENAME_CONCORDANCE + "." + STATUS,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType CONCORDANCE_MESSAGENUMBER = new ColumnType(TABLENAME_CONCORDANCE + "." + MESSAGE_NUMBER,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
    ColumnType CONCORDANCE_DELETED = new ColumnType(TABLENAME_CONCORDANCE + "." + DELETED,
            ColumnType.Type.ID,
            ColumnType.ExportType.GENERAL);
}
