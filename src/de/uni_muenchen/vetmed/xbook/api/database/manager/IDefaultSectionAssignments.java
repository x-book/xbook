package de.uni_muenchen.vetmed.xbook.api.database.manager;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface IDefaultSectionAssignments {

    public static final int COLUMN_NOTASSIGNED = -1;
    public static final int COLUMN_SYSTEM = -2;
    public static final int COLUMN_NODISPLAY = -3;

    /**
     * Describes an element that is not assigned to any section (yet).
     */
    ColumnType.SectionAssignment SECTION_NOT_ASSIGNED = new ColumnType.SectionAssignment(COLUMN_NOTASSIGNED, "NOT ASSIGNED TO SECTION");
    /**
     * Describes an element that should not be displayed in specific panels,
     * e.g. export, settings, ...
     */
    ColumnType.SectionAssignment SECTION_NO_DISPLAY = new ColumnType.SectionAssignment(COLUMN_NODISPLAY, "NO_DISPLAY");
    /**
     * Describes an element that is a system field and that is not displayed in
     * general.
     */
    ColumnType.SectionAssignment SECTION_SYSTEM = new ColumnType.SectionAssignment(COLUMN_SYSTEM, "SYSTEM_COLUMN");

}
