package de.uni_muenchen.vetmed.xbook.api.database.manager.palaeodepot;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import java.util.ArrayList;

public interface IPDCartonManager extends IStandardInputUnitColumnTypes {

    /* ======================================================================
     * LOCATION
     * ====================================================================== */
    ColumnType CARTON_NUMBER = new ColumnType(TABLENAME_INPUT_UNIT + ".CartonNumber",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("CARTON_NUMBER"))
            .setMandatory(true)
            .setMaxInputLength(6);

    ColumnType CARTON_NUMBER_EXTERN = new ColumnType(
            TABLENAME_INPUT_UNIT + ".CartonNumber",
            ColumnType.Type.EXTERN_VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("CARTON_NUMBER"))
            .setForListing(true)
            .setPriority(9);
    ColumnType CARTON_SIZE = new ColumnType(TABLENAME_INPUT_UNIT + ".CartonSize",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("CARTON_SIZE"))
            .setMandatory(true);
    ColumnType FLOOR = new ColumnType(TABLENAME_INPUT_UNIT + ".Floor",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("FLOOR"))
            .setMaxInputLength(2);
    ColumnType CORRIDOR = new ColumnType(TABLENAME_INPUT_UNIT + ".Corridor",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("CORRIDOR"))
            .setMaxInputLength(2);
    ColumnType SIDE = new ColumnType(TABLENAME_INPUT_UNIT + ".Side",
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("SIDE"));
    ColumnType SHELF = new ColumnType(TABLENAME_INPUT_UNIT + ".Shelf",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("SHELF"))
            .setMaxInputLength(8);
    ColumnType EDITED = new ColumnType(TABLENAME_INPUT_UNIT + ".Edited",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("EDITED"));
    ColumnType EDITED_FROM = new ColumnType(TABLENAME_INPUT_UNIT + ".EditedFrom",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("EDITED_FROM"));

    /* ======================================================================
     * CONTENT
     * ====================================================================== */
    ColumnType PHOTOGRAPED_PIECES = new ColumnType(TABLENAME_INPUT_UNIT + ".PhotographedPieces",
            ColumnType.Type.YES_NO_NONE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("PHOTOGRAPED_PIECES"));
    ColumnType NOTE = new ColumnType(TABLENAME_INPUT_UNIT + ".Note",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("NOTE"));

    /* ======================================================================
     * LOAN
     * ====================================================================== */
    ColumnType LOAN_TO_CONTACT = new ColumnType(TABLENAME_INPUT_UNIT + ".LoanToContact",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("LOAN_TO_CONTACT"));
    ColumnType LOAN_FROM = new ColumnType(TABLENAME_INPUT_UNIT + ".LoanFrom",
            ColumnType.Type.DATE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("LOAN_FROM"));
    ColumnType LOAN_UNTIL = new ColumnType(TABLENAME_INPUT_UNIT + ".LoanUntil",
            ColumnType.Type.DATE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("LOAN_UNTIL"));
    ColumnType LOAN_NOTE = new ColumnType(TABLENAME_INPUT_UNIT + ".LoanNote",
            ColumnType.Type.VALUE,
            ColumnType.ExportType.ALL)
            .setDisplayName(Loc.get("LOAN_NOTE"));
    ColumnType LOAN_CLARIFICATION_NEEDED = new ColumnType(TABLENAME_INPUT_UNIT + ".LoanClarificationNeeded",
            ColumnType.Type.BOOLEAN,
            ColumnType.ExportType.GENERAL)
            .setDisplayName(Loc.get("LOAN_CLARIFICATION_NEEDED"));

    ArrayList<Integer> getAllCartonNumbers(Key projectKey) throws StatementNotExecutedException;

}

