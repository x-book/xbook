/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.uni_muenchen.vetmed.xbook.api.database.manager;

import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.exception.EntriesException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLineGroup;
import java.util.ArrayList;

/**
 *
 * @author lohrer
 */
public interface ISynchronisationManager {

    ColumnTypeList getDataColumns();
    /**
     * records definitely deleted from database for given project
     *
     * @param project
     * @param event   - QueryManager.DELETE_DELETED, QueryManager.DELETE_ALL
     * @throws StatementNotExecutedException
     * @precondition method is executed on local database during synchronization
     */
    void deletePermanent(ProjectDataSet project, int event) throws StatementNotExecutedException;

    /**
     * Deletes the entry permanently. Does nothing if this is not overriden by a
     * manager...
     *
     * @param project
     * @param entry
     * @param event
     * @throws StatementNotExecutedException
     */
    void deletePermanent(Key project, Key entry, int event) throws StatementNotExecutedException;

    /**
     * Deletes the values for the given Record Key
     *
     * @param entryKey
     * @param projectKey
     * @throws StatementNotExecutedException
     */
    void deleteValue(Key entryKey, Key projectKey) throws StatementNotExecutedException;

    /**
     * Filters the List of solve Conflict lines. This can be used to only
     * display Conflicted lines or mandatory lines.
     *
     * @param lines
     * @return
     */
    ArrayList<SolveConflictLineGroup> filterLines(ArrayList<SolveConflictLineGroup> lines);

    /**
     * Returns a list of SolveConflictLineGroups for the given Entry. this is
     * done by getting the corresponding database entrys local and global. then
     * a SolveConflictLineGroup is created and finally the Groups are filtered.
     *
     * @param localData
     * @param serverData
     * @return
     * @throws StatementNotExecutedException
     * @throws EntriesException
     */
    ArrayList<SolveConflictLineGroup> getConflictLines(DataSetOld localData, DataSetOld serverData) throws StatementNotExecutedException, EntriesException;

    ArrayList<DataSetOld> getConflicts(ProjectDataSet project) throws StatementNotExecutedException;

    /**
     * Returns the last synchronisation time for the current table
     *
     * @return
     * @throws StatementNotExecutedException
     */
    String getLastSynchronisation(Key projectKey) throws StatementNotExecutedException;

    /**
     * Returns the SolveConflictLines for the given entry with the given local
     * Data and the given server data.
     *
     * @param localAll  The local available data for this entry. If no data
     *                  exists this list is empty.
     * @param globalRows The global available data for this entry. If no data
     *                  exists this list is empty.
     * @return The SolveConflictGroup for this entry.
     * @throws StatementNotExecutedException
     * @throws EntriesException
     */
    SolveConflictLineGroup getLinesForEntry(DataRow localAll, DataRow globalRows) throws StatementNotExecutedException, EntriesException;

    /**
     * Returns the localised table name.
     *
     * @return
     */
    String getLocalisedTableName();

    /**
     * Returns the number of conflicted Entries for the given Project
     *
     * @param projectKey The Key of the Project
     * @return The number of entries conflicted
     * @throws StatementNotExecutedException
     */
    int getNumberOfConflictedEntries(Key projectKey) throws StatementNotExecutedException;

    /**
     * Returns the number of Uncommitted Entries for the given Project
     *
     * @param projectKey The Key of the Project
     * @return The number of entries uncommitted
     * @throws StatementNotExecutedException
     */
    int getNumberOfUncommittedEntries(Key projectKey) throws StatementNotExecutedException;

    /**
     * Returns the number of entries for the given AbstractProjectOverviewScreen in the DB
     *
     * @param projectKey The Key of the AbstractProjectOverviewScreen including Id and DATABASE_ID
     * @return The number of entries
     * @throws StatementNotExecutedException
     */
    int getNumberofEntrys(Key projectKey) throws StatementNotExecutedException;

    ArrayList<String> getPrimaryColumns();

    String getTableName();

    /**
     * Inserts the given data into the current table
     *
     * @param data   The data of the record
     * @param ignore whether the insert shall ignore errors
     * @throws StatementNotExecutedException
     */
    void insertData(DataRow data, boolean ignore) throws StatementNotExecutedException;

    /**
     * Inserts the given data into the current table
     *
     * @param data The data
     * @throws StatementNotExecutedException
     */
    void insertData(DataRow data) throws StatementNotExecutedException;

    boolean isConflicted(Key projectKey) throws StatementNotExecutedException;

    boolean isSynchronised(ArrayList<DataColumn> entry) throws StatementNotExecutedException;

    /**
     * Loads the entry for the current table in the EntryData object,
     * informations about the entry can be found in the object itself. This only
     * works with plain string entrys. More komplex tables must override the
     * methode
     *
     * @param data
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException
     */
    void load(DataSetOld data) throws StatementNotExecutedException;

    void save(DataSetOld data) throws StatementNotExecutedException;

    /**
     * Sets the given data to conflicted
     *
     * @param dataSet
     * @throws StatementNotExecutedException
     */
    void setConflicted(DataSetOld dataSet) throws StatementNotExecutedException;

    /**
     * Sets the status of the entry given by the given primary keys to
     * synchronise
     *
     * @param primaryKeyValues
     * @throws StatementNotExecutedException
     */
    void setStatusToSynchronise(ArrayList<DataColumn> primaryKeyValues) throws StatementNotExecutedException;

    /**
     * Sets the given data to synchronised.
     *
     * @param entryData
     * @param returnData
     * @throws StatementNotExecutedException
     */
    void setSynchronised(DataSetOld entryData, DataSetOld returnData) throws StatementNotExecutedException;

    /**
     * Updates the data for the current table with the given data and the given
     * key
     *
     * @param data The data to be updated
     * @param key  The key of the Entry
     * @return The number of rows affected
     * @throws StatementNotExecutedException
     */
    int updateData(DataRow data, ArrayList<String> key) throws StatementNotExecutedException;

    int updateDataWithKey(ArrayList<DataColumn> data, ArrayList<DataColumn> key) throws StatementNotExecutedException;

    /**
     * Updates the database number if (for some reason) no database number was
     * set
     *
     * @param newNumber The new number to be set
     * @throws StatementNotExecutedException
     */
    void updateDatabaseNumber(int newNumber) throws StatementNotExecutedException;

    void updateEntries(DataSetOld data) throws StatementNotExecutedException;

    void updateUnsyncedEntries(ProjectDataSet project) throws StatementNotExecutedException;
    
}
