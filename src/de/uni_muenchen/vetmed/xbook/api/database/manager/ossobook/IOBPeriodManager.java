package de.uni_muenchen.vetmed.xbook.api.database.manager.ossobook;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public interface IOBPeriodManager extends IStandardColumnTypes {

    String TABLENAME_PERIOD_VALUES = "period_values";

    ColumnType PERIOD_VALUE = new ColumnType(TABLENAME_PERIOD_VALUES + "." + VALUE,
            ColumnType.Type.ID,
            ColumnType.ExportType.ALL);
}
