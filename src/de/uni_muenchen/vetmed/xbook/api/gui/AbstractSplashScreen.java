package de.uni_muenchen.vetmed.xbook.api.gui;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractSplashScreen {

    /**
     * Hold the time in ms how long to display the splash screen at least
     */
    protected final static int MIN_DISPLAY_TIME = 3000; // standard: 3000
    /**
     * Hold the time in ms how long to display the splash screen at least in
     * dev-MODE
     */
    protected final static int MIN_DISPLAY_TIME_DEV_MODE = 1000; // standard: 1000
    /**
     * The background image object of the splash screen.
     */
    private static ImageIcon bgImage;
    /**
     * The JFrame object holding all graphical elements of the splash screen.
     */
    protected JFrame frame;
    /**
     * Holds the time in ms when the splash screen was displayed.
     */
    protected long startTime;
    /**
     * Holds the status wheather the manual or the automatical MODE is
     * activated.
     */
    private boolean manualClose;
    /**
     * The static object of this class.
     */
    private static AbstractSplashScreen runningInstace;

    /**
     * Constructor of the SplashScreen class.
     * <p/>
     * Display all the items of the splash screen and saves the time when the
     * splash screen was displayed, in the class variable.
     *
     * @param manualClose <code>true</code> for manual close (e.g. for about
     *                    panel), <code>false</code> for auto-close (e.g. for startup)
     */
    public AbstractSplashScreen(boolean manualClose, ImageIcon backgroundImage) {
        this.manualClose = manualClose;
        this.runningInstace = this;
        frame = new JFrame();

        // set the favicon
        setFavicon();

        // Get the background image of the splash screen.
        bgImage = backgroundImage;

        // build together the graphical elements and display the splash screen
//        frame.setAlwaysOnTop(true);
        frame.setUndecorated(true);
        frame.setSize(bgImage.getIconWidth(), bgImage.getIconHeight());
        frame.setContentPane(new JLabel(bgImage));
        addComponentsToPane();
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
        frame.requestFocus();
        // save the current time when the splash screen is displayed
        startTime = Calendar.getInstance().getTimeInMillis();
    }

    /**
     * Creates, initialises and displays all elements of the splash screen
     * without the background image.
     * <p/>
     * Is only be called by the constructor in general.
     */
    private void addComponentsToPane() {
        Container pane = frame.getContentPane();
        pane.setLayout(null);
        Dimension size;

        // Initialise the label holding the version of OssoBook
        JLabel labelVersion = new JLabel(Loc.get("VERSION") + " " + getApplicationVersion());
        labelVersion.setForeground(getTextColor());
        labelVersion.setFont(new Font("Verdana", Font.PLAIN, 12));
        size = labelVersion.getPreferredSize();
        Dimension additionalOffset = getVersionLabelOffset();
        labelVersion.setBounds(bgImage.getIconWidth() - size.width / 2 - 100 + additionalOffset.width, 111 + additionalOffset.height, size.width, size.height);

        // Initialise the label holding the clickable URL to the homepage
        String url = "<html><b><u>" + getURL() + "</u></b></html>";
        JLabel labelUrl = new JLabel(url);
        labelUrl.setForeground(new Color(247, 215, 192));
        labelUrl.setFont(new Font("Verdana", Font.PLAIN, 10));
        size = labelUrl.getPreferredSize();
        labelUrl.setBounds(20, 258, size.width, size.height);
        labelUrl.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                try {
                    Desktop.getDesktop().browse(new URI(getURL()));
                } catch (URISyntaxException | IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        // Initialise the label holding the credit list
        JEditorPane labelCredits = new JEditorPane();
        labelCredits.setEditorKit(JEditorPane.createEditorKitForContentType("text/html"));
        labelCredits.setEditable(false);
        labelCredits.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            Desktop.getDesktop().browse(e.getURL().toURI());
                        } catch (IOException | URISyntaxException ex) {
                            Logger.getLogger(SidebarPanel.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        });
        // add a HTMLEditorKit to the editor pane
        HTMLEditorKit kit = new HTMLEditorKit();
        labelCredits.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule("body #splash { font-family: Verdana, SansSerif; font-size: 8px; color: white; line-height: 9px; }");

        labelCredits.setText("<html><body><div id='splash'>" + getCredits() + "</div></body></html>");
        labelCredits.setForeground(new Color(237, 237, 237));
        labelCredits.setFont(new Font("Verdana", Font.PLAIN, 9));
        labelCredits.setOpaque(false);
        labelCredits.setBounds(20, 11, 450, 224);

        // Initialise the Close button
        JButton closeButton = new JButton(Loc.get("CLOSE"));
        closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.setVisible(false);
            }
        });
        size = closeButton.getPreferredSize();
        closeButton.setBounds(bgImage.getIconWidth() - size.width - 62, 30, size.width, size.height);
        closeButton.setBackground(new Color(125, 99, 91));

        // display everything
        pane.add(labelVersion);
        pane.add(labelUrl);
        pane.add(labelCredits);
        if (manualClose) {
            pane.add(closeButton);
        }
    }

    public static void hide() {
        hide(false);
    }

    /**
     * Hides the splash screen.
     */
    public static void hide(boolean forceClose) {
        // check if the minimum display time is already expired
        int sleepTime = MIN_DISPLAY_TIME;
        if (AbstractConfiguration.MODE == AbstractConfiguration.Mode.DEVELOPMENT) {
            sleepTime = MIN_DISPLAY_TIME_DEV_MODE;
        }
        if (runningInstace == null) {
            return;
        }

        if (!forceClose) {
            while (Calendar.getInstance().getTimeInMillis() - runningInstace.startTime < sleepTime) {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
        // if it is expired, close the splash screen
        runningInstace.frame.dispose();
    }

    /**
     * Compose and return the text that will be displayed in the credit list.
     *
     * @return The credit list.
     */
    private String getCredits() {
        ArrayList<Person> xBookDeveloppers = getXBookDevelopers();
        ArrayList<Person> xBookContributors = getXBookContributors();
        ArrayList<Person> xBook1stLevelDeveloppers = get1stLevelDevelopers();
        ArrayList<Person> xBook2ndLevelDeveloppers = get2ndLevelDevelopers();
        ArrayList<Person> xBook1stLevelContributors = get1stLevelContributors();
        ArrayList<Person> xBook2ndLevelContributors = get2ndLevelContributors();
        ArrayList<Person> translators = getTranslators();
        ArrayList<Person> institutions = getInstitutions();
        String credits = "";
        String lineWrapText = ""; // "<br />"

        if (xBookDeveloppers != null && !xBookDeveloppers.isEmpty()) {
            credits += "<b><u>" + Loc.get("DEVELOPMENT") + " xBook:" + "</b></u> " + lineWrapText;
            credits += createNameList(xBookDeveloppers) + "<br />";
        }
        if (xBookContributors != null && !xBookContributors.isEmpty()) {
            credits += "<b><u>" + Loc.get("CONTRIBUTION") + " xBook:" + "</b></u> " + lineWrapText;
            credits += createNameList(xBookContributors) + "<br /><br />";
        }
        if ((xBook1stLevelDeveloppers != null && !xBook1stLevelDeveloppers.isEmpty()) || (xBook2ndLevelDeveloppers != null && !xBook2ndLevelDeveloppers.isEmpty())) {
            credits += "<b><u>" + Loc.get("DEVELOPMENT") + " " + getBookName() + ":" + "</b></u> " + lineWrapText;
            if (xBook1stLevelDeveloppers != null && !xBook1stLevelDeveloppers.isEmpty()) {
                credits += createNameList(xBook1stLevelDeveloppers) + lineWrapText;
            }
            if ((xBook1stLevelDeveloppers != null && !xBook1stLevelDeveloppers.isEmpty()) && (xBook2ndLevelDeveloppers != null && !xBook2ndLevelDeveloppers.isEmpty())) {
                credits += ", ";
            }
            if (xBook2ndLevelDeveloppers != null && !xBook2ndLevelDeveloppers.isEmpty()) {
                credits += createNameList(xBook2ndLevelDeveloppers) + lineWrapText;
            }
        }
        if ((xBook1stLevelContributors != null && !xBook1stLevelContributors.isEmpty()) || (xBook2ndLevelContributors != null && !xBook2ndLevelContributors.isEmpty())) {
            credits += "<br />";
            credits += "<b><u>" + Loc.get("CONTRIBUTION") + " " + getBookName() + ":" + "</b></u> " + lineWrapText;
            if (xBook1stLevelContributors != null && !xBook1stLevelContributors.isEmpty()) {
                credits += createNameList(xBook1stLevelContributors) + lineWrapText;
            }
            if ((xBook1stLevelContributors != null && !xBook1stLevelContributors.isEmpty()) && (xBook2ndLevelContributors != null && !xBook2ndLevelContributors.isEmpty())) {
                credits += ", ";
            }
            if (xBook2ndLevelContributors != null && !xBook2ndLevelContributors.isEmpty()) {
                credits += createNameList(xBook2ndLevelContributors) + lineWrapText;
            }
        }

        if ((translators != null && !translators.isEmpty()) || (institutions != null && !institutions.isEmpty())) {
            credits += "<br /><br />";
        }

        if (translators != null && !translators.isEmpty()) {
            credits += "<b><u>" + Loc.get("TRANSLATION") + ":" + "</b></u> " + lineWrapText;
            credits += createNameList(translators);
            credits += "<br />";
        }
        if (institutions != null && !institutions.isEmpty()) {
            credits += "<b><u>" + Loc.get("INSTITUTIONS") + ":" + "</b></u> " + lineWrapText;
            credits += createNameList(institutions) + "<br />";
            credits += "<br />";
        }

        while (credits.endsWith("<br />")) {
            credits = credits.substring(0, credits.length() - "<br />".length());
        }
        return credits;
    }

    /**
     * Creates a string of names that lists all names separated with an comma.
     * <p/>
     * The names will be sorted for the last name.
     *
     * @param personList An arraylist of all persons that should be converted to
     *                   a String.
     * @return The final string holdin all persons in alphabetical order.
     */
    private String createNameList(ArrayList<Person> personList) {
        Collections.sort(personList, new PersonSortByNameComparator());
        String list = "";
        if (personList == null || personList.isEmpty()) {
            return "";
        }
        for (Person p : personList) {
            if (p.firstName != null && !p.firstName.isEmpty() && p.lastName != null && !p.lastName.isEmpty()) {
                if (p.title != null) {
                    list += p.title + " ";
                }
                list += p.firstName + " " + p.lastName + ", ";
            } else if (p.firstName != null && !p.firstName.isEmpty()) {
                if (p.title != null) {
                    list += p.title + " ";
                }
                list += p.firstName + ", ";
            } else if (p.lastName != null && !p.lastName.isEmpty()) {
                if (p.title != null) {
                    list += p.title + " ";
                }
                list += p.lastName + ", ";
            }
        }
        if (list.endsWith(", ")) {
            list = list.substring(0, list.length() - ", ".length());
        }
        return list;
    }

    public String getQuoteText() {
        // first collect all persons that should be quoted
        UniqueArrayList<Person> allPersonsToQuote = new UniqueArrayList<>();

        allPersonsToQuote.addAll(getXBookDevelopers());
        allPersonsToQuote.addAll(getXBookContributors());
        allPersonsToQuote.addAll(get1stLevelDevelopers());
        allPersonsToQuote.addAll(get2ndLevelDevelopers());
        allPersonsToQuote.addAll(get1stLevelContributors());
        allPersonsToQuote.addAll(get2ndLevelContributors());
        allPersonsToQuote.addAll(getTranslators());


        Collections.sort(allPersonsToQuote, new PersonSortyByPriorityComparator());

        String quoteString = "";

        // list of all persons
        for (Person p : allPersonsToQuote) {
            if (p.priority != 0) {
                quoteString += p.lastName;
                if (p.firstName != null && !p.firstName.isEmpty()) {
                    quoteString += ", " + p.firstName.substring(0, 1) + ".";
                }
                quoteString += "; ";
            }
        }
        if (quoteString.endsWith("; ")) {
            quoteString = quoteString.substring(0, quoteString.length() - 2);
        }
        quoteString += ": ";

        // book name and version
        quoteString += getBookName() + " v" + AbstractConfiguration.getProgramVersion() + ", ";

        // places and year
        for (String place : getDevelopmentPlaces()) {
            quoteString += place + ", ";
        }
        quoteString += Calendar.getInstance().get(Calendar.YEAR) + ". (" + getURL() + ")";

        return quoteString;
    }

    /**
     * Returns a list of all xBook developers.
     *
     * @return A list of all xBook developers.
     */
    private ArrayList<Person> getXBookDevelopers() {
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(new Person("Dr.", "Kaltenthaler", "Daniel", 1));
        persons.add(new Person("Dr.", "Lohrer", "Johannes-Y.", 1));
        return persons;
    }

    /**
     * Returns a list of all xBook contributors.
     *
     * @return A list of all xBook contributors.
     */
    private ArrayList<Person> getXBookContributors() {
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(new Person("Prof. Dr.", "Kröger", "Peer", 1));
        persons.add(new Person("van der Meijden", "Chris", 1));
        return persons;
    }

    /**
     * Returns a list of all 1st level developers of the specific book. They
     * will be displayed above the list of the 2nd level developers.
     *
     * @return A list of all xBook developers.
     */
    protected abstract ArrayList<Person> get1stLevelDevelopers();

    /**
     * Returns a list of all 2nd level developers of the specific book. They
     * will be displayed below the list of the 1st level developers.
     *
     * @return A list of all xBook developers.
     */
    protected abstract ArrayList<Person> get2ndLevelDevelopers();

    /**
     * Returns a list of all 1st level contributors of the specific book. They
     * will be displayed above the list of the 2nd level contributors.
     *
     * @return A list of all xBook contributors.
     */
    protected abstract ArrayList<Person> get1stLevelContributors();

    /**
     * Returns a list of all 2nd level contributors of the specific book. They
     * will be displayed below the list of the 1st level contributors.
     *
     * @return A list of all xBook contributors.
     */
    protected abstract ArrayList<Person> get2ndLevelContributors();

    /**
     * Returns a list of all translators of the specific book.
     *
     * @return A list of all translators.
     */
    protected abstract ArrayList<Person> getTranslators();

    /**
     * Returns a list of all institutions of the specific book.
     *
     * @return A list of all institutionss.
     */
    protected abstract ArrayList<Person> getInstitutions();

    /**
     * Set the favicon for the frame.
     */
    protected abstract void setFavicon();

    /**
     * Returns the current application version.
     *
     * @return The current application version;
     */
    protected abstract String getApplicationVersion();

    /**
     * Returns the name of the book.
     *
     * @return The name of the book.
     */
    protected abstract String getBookName();

    /**
     * Returns the places of development of the book.
     *
     * @return The places of development of the book.
     */
    protected abstract ArrayList<String> getDevelopmentPlaces();

    /**
     * Returns the text color for the text labels.
     *
     * @return The text color for the text labels.
     */
    protected Color getTextColor() {
        return Color.WHITE;
    }

    /**
     * Returns the additional x offset of the version label. Necessary if the
     * label should not be displayed exactly centered.
     *
     * @return The additional x offset of the version label.
     */
    protected Dimension getVersionLabelOffset() {
        return new Dimension(0, 0);
    }

    /**
     * Returns the URL that should be displayed in the splash screen.
     *
     * @return The URL to be displayed.
     */
    protected abstract String getURL();

    /**
     * A helper class to display the single persons in the credits list.
     */
    protected class Person {

        private String title;
        private String lastName;
        private String firstName;
        private Integer priority = 0;

        public Person(String lastName, String firstName) {
            this.lastName = lastName;
            this.firstName = firstName;
        }

        public Person(String lastName, String firstName, int priority) {
            this(lastName, firstName);
            this.priority = priority;
        }

        public Person(String name) {
            this.lastName = name;
            this.firstName = null;
        }

        public Person(String name, int priority) {
            this(name);
            this.priority = priority;
        }

        public Person(String title, String lastName, String firstName) {
            this.title = title;
            this.lastName = lastName;
            this.firstName = firstName;
        }

        public Person(String title, String lastName, String firstName, int priority) {
            this(title, lastName, firstName);
            this.priority = priority;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Person)) {
                return false;
            }
            Person object = (Person) obj;
            return lastName.equals(object.lastName) && firstName.equals(object.firstName);
        }

    }

    public static AbstractSplashScreen getSplashScreen() {

        return runningInstace;
    }

    /**
     * A comparator to be able to sort the Person objects by person name.
     */
    public class PersonSortByNameComparator implements Comparator<Person> {

        @Override
        public int compare(Person p1, Person p2) {
            if (p1.lastName == null && p2.lastName == null) {
                return 0;
            }
            if (p1.lastName == null) {
                return 1;
            }
            if (p2.lastName == null) {
                return -1;
            }
            return p1.lastName.compareTo(p2.lastName);
        }
    }

    /**
     * A comparator to be able to sort the Person objects by priority.
     */
    public class PersonSortyByPriorityComparator implements Comparator<Person> {

        @Override
        public int compare(Person p1, Person p2) {
            return p1.priority.compareTo(p2.priority);
        }
    }

}
