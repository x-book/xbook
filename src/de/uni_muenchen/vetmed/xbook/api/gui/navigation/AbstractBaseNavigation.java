package de.uni_muenchen.vetmed.xbook.api.gui.navigation;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.TreeListMap;
import de.uni_muenchen.vetmed.xbook.api.event.LoginListener;
import de.uni_muenchen.vetmed.xbook.api.event.ProjectEvent;
import de.uni_muenchen.vetmed.xbook.api.event.ProjectListener;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.RepeatBackgroundPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractBaseNavigation<T extends IMainFrame> extends RepeatBackgroundPanel implements LoginListener, ProjectListener, INavigation {

    /**
     * The basic main frame object.
     */
    protected T mainFrame;

    /**
     * The basic controller object.
     */
    protected ApiControllerAccess controller;

    /**
     * The panel where the single main navigation elements are displayed.
     */
    private final RepeatBackgroundPanel elements;

    /**
     * The objects of the basic main navigation elements.
     */
    protected MainNavigationElement project, dataEntry, listing, sync, rights, tools, help, dev, logout;

    /**
     * Holds the current mode that determines which menu elements are enabled and which are disabled.
     */
    private INavigation.Mode mode;

    /**
     * Holds all available main navigation elements.
     */
    private final TreeListMap<Integer, INavigationElement> allNavigationElements = new TreeListMap<>();

    /**
     * Holds all available sub navigation elements.
     */
    private final HashMap<MainNavigationElement, TreeListMap<Integer, INavigationElement>> allNavigationPopupElements = new HashMap<>();

    /**
     * Holds all custom added navigation elements and their position, where to display them-
     */
    private final ArrayList<MainNavigationElement> customNavigationElements = new ArrayList<>();

    /**
     * Constructor
     * <p>
     * Sets the basic layout of the navigation bar.
     *
     * @param mainFrame The basic main frame object.
     */
    public AbstractBaseNavigation(final T mainFrame) {
        super(Images.NAVIGATION_BACKGROUND);

        this.mode = INavigation.Mode.NOT_LOGGED_IN;

        mainFrame.getController().addLoginListener(this);
        mainFrame.getController().addProjectListener(this);
        this.mainFrame = mainFrame;
        this.controller = mainFrame.getController();

        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(63, 63));
        setBackground(Colors.CONTENT_BACKGROUND);

        // set the navigation
        elements = new RepeatBackgroundPanel(Images.NAVIGATION_BACKGROUND);
        elements.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 5));
        createMenuItems();

        // set the application icon
        if (getApplicationLogo() != null) {
            JLabel l = new JLabel();
            l.setIcon(getApplicationLogo());
            add(BorderLayout.EAST, l);
        }
    }

    /**
     * Basic method to create all menu items from scatch.
     */
    private void createMenuItems() {
        // initialise all elements
        loadElements();

        // display all main navigation elements
        for (ArrayList<INavigationElement> e1 : allNavigationElements.values()) {
            for (INavigationElement e : e1) {
                addMainNavigationElement(e);
            }
        }
        for (MainNavigationElement e : customNavigationElements) {
            addMainNavigationElement(e);
        }

        // display all popup elements
        for (MainNavigationElement main : allNavigationPopupElements.keySet()) {
            TreeListMap<Integer, INavigationElement> t = allNavigationPopupElements.get(main);
            for (ArrayList<INavigationElement> l : t.values()) {
                for (INavigationElement e : l) {
                    addMainNavigationPopupElement(main, e);
                }
            }
        }
        add(BorderLayout.WEST, elements);
    }

    /**
     * Returns the application logo that is inserted in the right upper corner.
     *
     * @return The application logo.
     */
    protected abstract Icon getApplicationLogo();

    /**
     * Returns the color of the separator color.
     *
     * @return The color of the separator color.
     */
    protected Color getSeparatorColor() {
        return Colors.BOOK_COLOR;
    }

    /**
     * Adds a separator in the main navigation element bar.
     *
     * @param priority The priority of the main navigation element.
     */
    protected void addSeparator(int priority) {
        allNavigationElements.add(priority, new MainNavigationSeparator(priority, getSeparatorColor()));
    }

    /**
     * Adds a separator in the main navigation popup element.
     *
     * @param mainNavigationElement The main navigation element where to add the separator.
     * @param priority              The priority of the main navigation element.
     */
    protected MainNavigationPopupSeparator addPopupSeparator(MainNavigationElement mainNavigationElement, int priority) {
        if (!allNavigationPopupElements.containsKey(mainNavigationElement)) {
            allNavigationPopupElements.put(mainNavigationElement, new TreeListMap<Integer, INavigationElement>());
        }
        MainNavigationPopupSeparator sep = new MainNavigationPopupSeparator(priority);
        allNavigationPopupElements.get(mainNavigationElement).add(priority, sep);
        return sep;
    }

    /**
     * Overwrite this method to add custom listing items that are attached in the entry main menu element.
     * <p>
     * Please make use of createMainNavigationElement() or createNavigationPopupElement().
     */
    protected void addCustomItems() {
    }

    /**
     * Enables or disables the synchronisation button.
     *
     * @param b <code>true</code> to enable, <code>false</code> to disable.
     */
    public void setSyncButtonActive(boolean b) {
        sync.setEnabled(b);
    }

    /**
     * Get the displayed String for the menu item "projects". Use this method to rename the word "project".
     *
     * @return The label to be displayed.
     */
    protected String getProjectsLabel() {
        return Loc.get("PROJECTS");
    }

    /**
     * Get the displayed String for the menu item "projects overview". Use this method to rename the word "project".
     *
     * @return The label to be displayed.
     */
    protected String getProjectsOverviewLabel() {
        return Loc.get("PROJECT_OVERVIEW");
    }

    /**
     * Get the displayed String for the menu item "create new project". Use this method to rename the word "project".
     *
     * @return The label to be displayed.
     */
    protected String getCreateNewProjectLabel() {
        return Loc.get("CREATE_NEW_PROJECT");
    }

    /**
     * Get the displayed String for the menu item "project right". Use this method to rename the word "project".
     *
     * @return The label to be displayed.
     */
    protected String getProjectRightsLabel() {
        return Loc.get("PROJECT_RIGHTS");
    }

    @Override
    public void onLogin() {
        mainFrame.updateNavigation();
    }

    @Override
    public void onLogout() {
        mainFrame.updateNavigation();
    }

    @Override
    public void onProjectLoaded(ProjectEvent evt) {
        mainFrame.updateNavigation();
    }

    @Override
    public void onProjectUnloaded(ProjectEvent evt) {
        mainFrame.updateNavigation();
    }

    @Override
    public void onProjectAdded(ProjectEvent evt) {
    }

    @Override
    public void onProjectChanged(ProjectEvent evt) {
    }

    @Override
    public void onProjectDeleted(ProjectEvent evt) {
    }

    /**
     * Overwrite to add custom navigation elements.
     * <p>
     * Should be used for main navigation elements as well as for navigation popup elements.
     */
    public abstract void loadElements();

    /**
     * Creates and adds a new main navigation element.
     * <p>
     * Use this method to create a main navigation element without any function.
     *
     * @param priority     The priority of the main navigation element.
     * @param label        The label of the main navigation element.
     * @param icon         The icon of the main navigation element.
     * @param setEnabledOn The modes when this main navigation element is enabled.
     * @return The main navigation element object that was created and added.
     */
    protected MainNavigationElement createMainNavigationElement(int priority, String label, ImageIcon icon,
                                                                INavigation.Mode... setEnabledOn) {
        return createMainNavigationElement(priority, label, icon, (Runnable) null, setEnabledOn);
    }

    /**
     * Creates and adds a new main navigation element.
     *
     * @param priority     The priority of the main navigation element.
     * @param label        The label of the main navigation element.
     * @param icon         The icon of the main navigation element.
     * @param action       The action that is called when the main navigation element was clicked.
     * @param setEnabledOn The modes when this main navigation element is enabled.
     * @return The main navigation element object that was created and added.
     */
    protected MainNavigationElement createMainNavigationElement(int priority, String label, ImageIcon icon,
                                                                final Runnable action,
                                                                INavigation.Mode... setEnabledOn) {
        MainNavigationElement e = new MainNavigationElement(priority, label, icon, setEnabledOn) {
            @Override
            protected void actionOnClick() {
                if (action != null) {
                    new Thread(action).start();
                }
            }
        };
        allNavigationElements.add(e.getPriority(), e);
        return e;
    }

    /**
     * Adds a main navigation element to the navigation bar.
     *
     * @param e The navigation element to add.
     */
    private void addMainNavigationElement(INavigationElement e) {
        if (e instanceof MainNavigationElement) {
            MainNavigationElement c = (MainNavigationElement) e;
            c.setEnabled(mode);
            elements.add(c);
        } else if (e instanceof MainNavigationSeparator) {
            MainNavigationSeparator c = (MainNavigationSeparator) e;
            elements.add(c);
        }
    }

    /**
     * Creates and adds a new navigation popup element to an existing main navigation element.
     * <p>
     * Use this method to create an navigation popup element without an icon.
     *
     * @param mainNavigationElement The main navigation element where to add the new navigation popup element.
     * @param priority              The priority of the main navigation element.
     * @param label                 The label of the navigation popup element.
     * @param action                The action that is called when the navigation popup element was clicked.
     * @param setEnabledOn          The modes when this navigation popup element is enabled.
     */
    protected MainNavigationPopupElement createNavigationPopupElement(MainNavigationElement mainNavigationElement,
                                                                      int priority, String label,
                                                                      final Runnable action,
                                                                      INavigation.Mode... setEnabledOn) {
        return createNavigationPopupElement(mainNavigationElement, priority, label, (ImageIcon) null, action,
                setEnabledOn);
    }

    /**
     * Creates and adds a new navigation popup element to an existing main navigation element.
     *
     * @param mainNavigationElement The main navigation element where to add the new navigation popup element.
     * @param priority              The priority of the main navigation element.
     * @param label                 The label of the navigation popup element.
     * @param icon                  The icon of the navigation popup element.
     * @param action                The action that is called when the navigation popup element was clicked.
     * @param setEnabledOn          The modes when this navigation popup element is enabled.
     */
    protected MainNavigationPopupElement createNavigationPopupElement(MainNavigationElement mainNavigationElement,
                                                                      int priority, String label, ImageIcon icon,
                                                                      final Runnable action,
                                                                      INavigation.Mode... setEnabledOn) {
        if (!allNavigationPopupElements.containsKey(mainNavigationElement)) {
            allNavigationPopupElements.put(mainNavigationElement, new TreeListMap<Integer, INavigationElement>());
        }
        MainNavigationPopupElement element = new MainNavigationPopupElement(priority, label, icon, action,
                setEnabledOn);
        allNavigationPopupElements.get(mainNavigationElement).add(priority, element);
        return element;
    }

    /**
     * Adds a sub navigation element to a given main navigation element.
     *
     * @param mainNavigationElement  The main navigation element where to add the navigation popup element.
     * @param popupNavigationElement The navigation popup element to add.
     */
    private void addMainNavigationPopupElement(MainNavigationElement mainNavigationElement,
                                               INavigationElement popupNavigationElement) {
        mainNavigationElement.addPopupItem(popupNavigationElement);
        popupNavigationElement.setEnabled(mode);
    }

    /**
     * Set the navigation to a new mode and updates the enability of each main and sub navigation element.
     * <p>
     * Use setMode to determine whether the navigation should save the new mode in the object or not. Not overwriting
     * the mode is Necesserary when enable all fields again after disabling all fields.
     *
     * @param mode    The new mode to set.
     * @param setMode <code>true</code> to set the new mode to the navigation
     *                object.
     */
    private void setMode(INavigation.Mode mode, boolean setMode) {

        // iterate all main navigation elements and adjust the mode
        for (ArrayList<INavigationElement> n1 : allNavigationElements.values()) {
            for (INavigationElement n2 : n1) {
                n2.setEnabled(mode);
            }
        }

        // iterate all popup elements and adjust the mode
        for (TreeListMap<Integer, INavigationElement> n1 : allNavigationPopupElements.values()) {
            for (ArrayList<INavigationElement> n2 : n1.values()) {
                for (INavigationElement n3 : n2) {
                    n3.setEnabled(mode);
                }
            }
        }

        // set the new mode to the class variable.
        if (setMode) {
            this.mode = mode;
        }
    }

    /**
     * Set the navigation to a new mode and updates the enability of each main and sub navigation element.
     *
     * @param mode The new mode to set.
     */
    public void setMode(INavigation.Mode mode) {
        setMode(mode, true);
    }

    /**
     * Enables or disables all navigation elements and its popups elements. The current mode of the navigation is not
     * overwritten and can be restored by calling the method again with <code>false</code> as a parameter.
     *
     * @param state Whether to set the navigation enables or not.
     */
    public void setEnableAll(boolean state) {
        if (!state) {
            setMode(INavigation.Mode.DEACTIVATE_ALL, false);
        } else {
            setMode(mode, false);
        }
    }

    /**
     * Removes all elements of the navigation, recreates them and rearrange them again. Escpecially Necessary if new
     * custom elements were added.
     * <p>
     * If you only want to change the mode, this method is not necessary. Please use setMode(INavigation.Mode) instead.
     */
    public void reloadNavigation() {
        elements.removeAll();
        allNavigationElements.clear();
        allNavigationPopupElements.clear();

        createMenuItems();
        setMode(mode);
    }

    /**
     * Inserts a new navigation element to the navigation. Navigation will be reloaded after insertion.
     *
     * @param element The navigation element to add.
     */
    public void insertNavigationElement(MainNavigationElement element) {
        customNavigationElements.add(element);
        reloadNavigation();
    }

    /**
     * Inserts new navigation elements to the navigation. Navigation will be reloaded after insertion.
     *
     * @param elements The navigation elements to add.
     */
    public void insertNavigationElement(ArrayList<MainNavigationElement> elements) {
        customNavigationElements.addAll(elements);
        reloadNavigation();
    }

}
