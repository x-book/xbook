package de.uni_muenchen.vetmed.xbook.api.gui.navigation;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * A separator that can be inserted beween single main navigation elements to
 * group the elements.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class MainNavigationSeparator extends JLabel implements INavigationElement {

    /**
     * The priority of the main navigation element.
     */
    private final int priority;

    /**
     * Constructor.
     * <p>
     * Initianaise the separator.
     *
     * @param priority       The priority of the main navigation element.
     * @param separatorColor The color of the separator. In general the Book
     *                       specific color.
     */
    public MainNavigationSeparator(int priority, Color separatorColor) {
        this.priority = priority;

        setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        setBackground(separatorColor);
        setPreferredSize(new Dimension(10, 51));
        setOpaque(true);
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public void setEnabled(INavigation.Mode mode) {
        // not necessary
    }

    @Override
    public void forceEnabled(boolean b) {
        // not necessary
    }
}
