package de.uni_muenchen.vetmed.xbook.api.gui.navigation;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.WrapLayout;
import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author kaltenthaler
 */
public abstract class AbstractSubNavigation extends JPanel {

    protected JPanel elements;
    protected IMainFrame mainFrame;

    public AbstractSubNavigation(IMainFrame mainFrame) {
        this.mainFrame = mainFrame;

        setBackground(Colors.CONTENT_BACKGROUND);
        setLayout(new BorderLayout());

        elements = new JPanel();
        elements.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
        updateElements();

        add(BorderLayout.NORTH, elements);
    }

    protected void updateElements() {
        elements.removeAll();
        elements.setLayout(new WrapLayout(WrapLayout.LEFT, 0, 2));
        addSeparator();

        for (SubNavigationElement sub : createContent()) {
            elements.add(sub);
            addSeparator();
        }
        repaint();
    }

    protected abstract ArrayList<SubNavigationElement> createContent();

    private JLabel addSeparator() {
        JLabel sep = new JLabel();
        sep.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        sep.setBackground(getSeparatorColor());
        sep.setPreferredSize(new Dimension(10, 24));
        sep.setOpaque(true);
        elements.add(sep);
        return sep;
    }

    protected Color getSeparatorColor() {
        return Colors.BOOK_COLOR;
    }

}
