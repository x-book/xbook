package de.uni_muenchen.vetmed.xbook.api.gui.navigation;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.RepeatBackgroundPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Daniel Kaltenthaler
 */
public class SubNavigationElement extends RepeatBackgroundPanel {

    private final Color HOVER_BORDER = new Color(39, 39, 39);
    private final Color HOVER_BACKGROUND = new Color(55, 55, 55);
    private JPanel outerWrapper;
    private JLabel title;
    private JPanel wrapperButton;
    private boolean selected = false;
    private String label;

    /**
     * Constructor.
     * <p>
     * Creates a sub navigation element and a specific label. The element will
     * not be colored.
     *
     * @param label The label of the sub navigation element.
     */
    public SubNavigationElement(String label) {
        this(label, false);
    }

    /**
     * Constructor.
     * <p>
     * Creates a sub navigation element and a specific label. If the 'selected'
     * parameter is set to <code>true</code> the element will be colored.
     *
     * @param label    The label of the sub navigation element.
     * @param selected Set the sub navigation element to selected or not.
     */
    public SubNavigationElement(String label, boolean selected) {
        super(Images.SUB_NAVIGATION_BACKGROUND_SELECTED);
        this.label = label;

        setLayout(new BorderLayout());
        setOpaque(false);

        setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

        outerWrapper = new JPanel(new BorderLayout());
        outerWrapper.setBorder(BorderFactory.createEmptyBorder(1, 1, 0, 1));
        outerWrapper.setOpaque(false);

        // Standard button
        wrapperButton = new JPanel(new BorderLayout());
        wrapperButton.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));
        wrapperButton.setOpaque(false);

        title = new JLabel("<html><body><center>" + label + "</center></body></html>");
        title.setForeground(Color.WHITE);
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setBorder(BorderFactory.createEmptyBorder(3, 0, 5, 0));
        wrapperButton.add(BorderLayout.NORTH, title);

        // arrange everything
        outerWrapper.add(BorderLayout.CENTER, wrapperButton);
        add(BorderLayout.CENTER, outerWrapper);

        setSelected(selected);

        addMouseListener(this);
        addMouseListener(title);
        addMouseListener(wrapperButton);
        addMouseListener(outerWrapper);

        addButtonClickListener(wrapperButton);
        addButtonClickListener(title);

        updateSize();
    }

    private void addMouseListener(JComponent comp) {
        comp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (isEnabled()) {
                    if (!selected) {
                        outerWrapper.setBackground(HOVER_BACKGROUND);
                        outerWrapper.setOpaque(true);
                        outerWrapper.setBorder(BorderFactory.createLineBorder(HOVER_BORDER, 1));
                    }
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                outerWrapper.setOpaque(false);
                outerWrapper.setBorder(BorderFactory.createEmptyBorder(1, 1, 0, 1));
            }
        });
    }

    private void addButtonClickListener(final JComponent comp) {
        comp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (isEnabled()) {
                    outerWrapper.setOpaque(false);
                    outerWrapper.setBorder(BorderFactory.createEmptyBorder(1, 1, 0, 1));
                    actionOnClick();
                }
            }
        });
    }

    @Override
    public void setEnabled(boolean state) {
        super.setEnabled(state);
        outerWrapper.setEnabled(state);
        title.setEnabled(state);
        wrapperButton.setEnabled(state);
    }

    protected void actionOnClick() {
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        setImage(getImage().getImage());

        if (title != null && label != null) {
            if (selected) {
                title.setForeground(Color.BLACK);
                title.setText("<html><body><center><b>" + label + "</b></center></body></html>");
            } else {
                title.setForeground(Color.WHITE);
                title.setText("<html><body><center>" + label + "</center></body></html>");
            }
            updateSize();
        }
    }

    private ImageIcon getImage() {
        if (selected) {
            return Images.SUB_NAVIGATION_BACKGROUND_SELECTED;
        } else {
            return Images.SUB_NAVIGATION_BACKGROUND;
        }
    }

    private void updateSize() {
        if (selected) {
            title.setPreferredSize(new Dimension(title.getPreferredSize().width + 20, 24));
            setPreferredSize(new Dimension(getPreferredSize().width + 20, 24));
        } else {
            title.setPreferredSize(new Dimension(title.getPreferredSize().width + 5, 24));
            setPreferredSize(new Dimension(getPreferredSize().width + 5, 24));
        }
    }

}
