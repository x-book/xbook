package de.uni_muenchen.vetmed.xbook.api.gui.navigation;

import javax.swing.JPopupMenu;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class MainNavigationPopupSeparator extends JPopupMenu.Separator implements INavigationElement {

    private final int priority;

    public MainNavigationPopupSeparator(int priority) {
        this.priority = priority;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public void setEnabled(INavigation.Mode mode) {
        // not necessary
    }

    @Override
    public void forceEnabled(boolean b) {
        // not necessary
    }
}
