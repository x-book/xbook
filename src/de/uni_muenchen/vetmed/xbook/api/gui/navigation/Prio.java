package de.uni_muenchen.vetmed.xbook.api.gui.navigation;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Prio {

    public static int M_PROJECT = 1000;
    public static int M_DATA_ENTRY = 2000;
    public static int M_LISTING = 3000;
    public static int M_SEARCH = 4000;
    public static int M_SEP1 = 4500;
    public static int M_SYNC = 5000;
    public static int M_RIGHTS = 6000;
    public static int M_TOOLS = 7000;
    public static int M_SEP2 = 7500;
    public static int M_HELP = 8000;
    public static int M_ADMIN = 8500;
    public static int M_LOGOUT = 9000;

    public static int M_SEP3 = 100000;
    public static int M_DEV = 101000;

    public static int P_PROJECT_OVERVIEW = 1000;
    public static int P_PROJECT_NEW = 2000;

    public static int P_LISTING_BACK = 1000;
    public static int P_LISTING_CUSTOM = 2000;

    public static int P_DATA_ENTRY_BACK = 1000;
    public static int P_DATA_ENTRY_NEW = 2000;
    public static int P_DATA_ENTRY_CUSTOMENTRY = 3000;

    public static int P_RIGHTS_GROUPS = 1000;
    public static int P_RIGHTS_USER_RIGHTS = 2000;
    public static int P_RIGHTS_GROUP_RIGHTS = 3000;

    public static int P_TOOLS_EXPORT = 1000;
    public static int P_TOOLS_IMPORT = 2000;
    public static int P_TOOLS_CODETABLES = 3000;
    public static int P_TOOLS_SETTINGS = 4000;

    public static int P_HELP_REPORT = 1000;
    public static int P_HELP_WIKI = 2000;
    public static int P_HELP_ABOUT = 3000;

    public static int P_ADMIN_GROUPS = 1000;
}
