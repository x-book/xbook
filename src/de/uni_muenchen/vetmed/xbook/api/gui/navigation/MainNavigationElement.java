package de.uni_muenchen.vetmed.xbook.api.gui.navigation;

import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

/**
 * A object that represent a main navigation element.
 *
 * @author Daniel Kaltenthaler
 */
public class MainNavigationElement extends JPanel implements INavigationElement {

    /**
     * The default background color.
     */
    private final Color HOVER_BACKGROUND = new Color(55, 55, 55);
    /**
     * The background color when the item is hovered with the mouse.
     */
    private final Color HOVER_BORDER = new Color(39, 39, 39);
    /**
     * JPanels that are used to assemble the main navigation element.
     */
    private final JPanel outerWrapper, wrapperCollapseable, wrapperButton;
    /**
     * JLabels that are used to assemble the main navigation element.
     */
    private final JLabel title, imageButton, imageCollapseable;
    /**
     * The popup menu that is displayed when clicking the navigation element.
     */
    private final JPopupMenu popup = new JPopupMenu();
    /**
     * The state if the popup is currently visible or not.
     */
    private boolean isPopupVisible = false;
    /**
     * The state wheather the popup should be displayed on a click or not.
     */
    private boolean openPopupOnClick = false;
    /**
     * Holds all states when the navigation element should be set to enabled.
     */
    private final ArrayList<INavigation.Mode> setEnabledOn = new ArrayList<>();
    /**
     * The priority of the main navigation element.
     */
    private final int priority;
    private String label;

    public String getLabel() {
        return label;
    }

    /**
     * Constructor.
     *
     * Creates the main navigation element and put all elements together.
     *
     * @param priority The priority of the main navigation element.
     * @param label The text label that should be displayed.
     * @param icon The icon that should be used for the element.
     * @param setEnabledOn All states when the navigation element should be set
     * to enabled
     */
    public MainNavigationElement(int priority, String label, Icon icon, INavigation.Mode... setEnabledOn) {
        this.priority = priority;
        this.label = label;

        setLayout(new BorderLayout());
        setOpaque(false);

        setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

        outerWrapper = new JPanel(new BorderLayout());
        outerWrapper.setBorder(BorderFactory.createEmptyBorder(1, 1, 0, 1));
        outerWrapper.setOpaque(false);

        // Standard button
        wrapperButton = new JPanel(new BorderLayout());
        wrapperButton.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));
        wrapperButton.setOpaque(false);

        title = new JLabel("<html><body><center>" + label + "</center></body></html>");
        title.setForeground(Color.WHITE);
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setBorder(BorderFactory.createEmptyBorder(2, 0, 5, 0));
        wrapperButton.add(BorderLayout.NORTH, title);

        imageButton = new JLabel();
        imageButton.setHorizontalAlignment(JLabel.CENTER);
        imageButton.setBorder(BorderFactory.createEmptyBorder(0, 0, 3, 0));
        imageButton.setOpaque(false);
        imageButton.setIcon(icon);
        wrapperButton.add(BorderLayout.SOUTH, imageButton);

        // Collapseable button
        wrapperCollapseable = new JPanel(new BorderLayout());
        wrapperCollapseable.setBorder(BorderFactory.createEmptyBorder(0, 1, 0, 0));
        wrapperCollapseable.setOpaque(false);

        imageCollapseable = new JLabel();
        imageCollapseable.setHorizontalAlignment(JLabel.CENTER);
        imageCollapseable.setOpaque(false);
        imageCollapseable.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));
        imageCollapseable.setSize(new Dimension(Images.NAVIGATION_COLLAPSABLE_ICON_DOWN.getIconWidth(), Images.NAVIGATION_COLLAPSABLE_ICON_DOWN.getIconHeight()));
        imageCollapseable.setIcon(Images.NAVIGATION_COLLAPSABLE_ICON_DOWN);
        wrapperCollapseable.add(BorderLayout.CENTER, imageCollapseable);

        // arrange everything
        outerWrapper.add(BorderLayout.EAST, wrapperCollapseable);
        outerWrapper.add(BorderLayout.CENTER, wrapperButton);
        add(BorderLayout.CENTER, outerWrapper);

        addMouseOverListener(this);
        addMouseOverListener(title);
        addMouseOverListener(imageButton);
        addMouseOverListener(imageCollapseable);
        addMouseOverListener(wrapperButton);
        addMouseOverListener(wrapperCollapseable);
        addMouseOverListener(outerWrapper);

        addButtonClickListener(wrapperButton);
        addButtonClickListener(imageButton);
        addButtonClickListener(title);

        addCollapsableClickListener(imageCollapseable);
        addCollapsableClickListener(wrapperCollapseable);

        wrapperCollapseable.setVisible(false);

        for (INavigation.Mode d : setEnabledOn) {
            this.setEnabledOn.add(d);
        }
    }

    /**
     * Adds a mouse listener to a component that opens the popup menu on a
     * click.
     *
     * Only useful for the elements of the main navigation element.
     *
     * @param comp The component the listener should be added.
     */
    private void addButtonClickListener(final JComponent comp) {
        comp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (isEnabled()) {
                    if (openPopupOnClick) {
                        togglePopup(e);
                    } else {
                        actionOnClick();
                    }
                }
            }
        });
    }

    /**
     * Adds a collapsable listener to a component that opens the popup menu on a
     * click.
     *
     * Only useful for the elements of the main navigation element.
     *
     * @param comp The component the listener should be added.
     */
    private void addCollapsableClickListener(JComponent comp) {
        comp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                togglePopup(e);
            }
        });
    }

    /**
     * Toggles the visibility of the popup and their corresponding states of the
     * class.
     *
     * Only working when popup is supported by this main navigation element.
     *
     * @param e The mouse event that is passed.
     */
    private void togglePopup(MouseEvent e) {
        if (isEnabled()) {
            if (isPopupVisible) {
                popup.setVisible(false);
                isPopupVisible = false;
            } else {
                popup.show(e.getComponent(), e.getX() - 55, 55);
                isPopupVisible = true;
            }
        }
    }

    /**
     * Adds a mouse listener to a component that changes the look of the main
     * navigation element when the mouse is hovered or leaves the element again.
     *
     * Only useful for the elements of the main navigation element.
     *
     * @param comp The component the listener should be added.
     */
    private void addMouseOverListener(JComponent comp) {
        comp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (isEnabled()) {
                    outerWrapper.setBackground(HOVER_BACKGROUND);
                    outerWrapper.setOpaque(true);
                    outerWrapper.setBorder(BorderFactory.createLineBorder(HOVER_BORDER, 1));
                    wrapperCollapseable.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, HOVER_BORDER));
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                outerWrapper.setOpaque(false);
                outerWrapper.setBorder(BorderFactory.createEmptyBorder(1, 1, 0, 1));
                wrapperCollapseable.setBorder(BorderFactory.createEmptyBorder(0, 1, 0, 0));
            }
        });
    }

    @Override
    public void setEnabled(INavigation.Mode mode) {
        boolean state = setEnabledOn.contains(mode);
        forceEnabled(state);
    }

    @Override
    public void forceEnabled(boolean b) {
        super.setEnabled(b);
        imageCollapseable.setEnabled(b);
        outerWrapper.setEnabled(b);
        wrapperCollapseable.setEnabled(b);
        imageButton.setEnabled(b);
        title.setEnabled(b);
        wrapperButton.setEnabled(b);
    }

    /**
     * Overwrite to define the action on click.
     */
    protected void actionOnClick() {
    }

    /**
     * Set whether the popup should be displayed on click or not.
     *
     * @param state <code>true</code> if the popup should be displayed on click.
     */
    protected void setDisplayPopupOnClick(boolean state) {
        openPopupOnClick = state;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    protected void addPopupItem(INavigationElement n) {
        if (n instanceof MainNavigationPopupElement) {
            MainNavigationPopupElement e = (MainNavigationPopupElement) n;
            popup.add(e);
        } else if (n instanceof MainNavigationPopupSeparator) {
            popup.addSeparator();
        }
        wrapperCollapseable.setVisible(popup.getSubElements().length > 0);
    }
}
