package de.uni_muenchen.vetmed.xbook.api.gui.navigation;

/**
 * An interface that only has the use to define that a object is a part of the
 * main navigiaton (that means: either a navigation element or a separator)
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface INavigationElement {

    /**
     * Returns the priority of this navigation element.
     *
     * @return The priority.
     */
    int getPriority();

    /**
     * Depending on the current mode the main navigation element is enabled or
     * disabled.
     *
     * @param mode The current mode.
     */
    void setEnabled(INavigation.Mode mode);

    /**
     * Force the main navigation element to be enabled or disabled.
     *
     * @param b True to set the element enables, false else.
     */
    void forceEnabled(boolean b);

}
