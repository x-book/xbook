package de.uni_muenchen.vetmed.xbook.api.gui.navigation;

/**
 *
 * @author Daniel Kaltenthaler  <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface INavigation {

    /**
     * An enumeration for all available modes for the main and sub navigation.
     */
    public enum Mode {

        NOT_LOGGED_IN, NO_PROJECT_LOADED, STANDARD, DEACTIVATE_ALL
    }
}
