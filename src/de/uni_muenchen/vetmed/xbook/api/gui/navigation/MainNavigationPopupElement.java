package de.uni_muenchen.vetmed.xbook.api.gui.navigation;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class MainNavigationPopupElement extends JMenuItem implements INavigationElement {

    /**
     * Holds all states when the navigation element should be set to enabled.
     */
    private final ArrayList<INavigation.Mode> setEnabledOn = new ArrayList<>();
    /**
     * The priority of the main navigation element.
     */
    private final int priority;
    private String label;

    @Override
    public String getLabel() {
        return label;
    }

    /**
     * Constructor.
     * Creates the main navigation element and put all elements together.
     *
     * @param priority     The priority of the main navigation element.
     * @param label        The text label that should be displayed.
     * @param icon         The icon that should be used for the element.
     * @param action       The action that is run when clicking the sub navigation
     *                     element.
     * @param setEnabledOn All states when the navigation element should be set
     *                     to enabled
     */
    public MainNavigationPopupElement(int priority, String label, ImageIcon icon, final Runnable action, INavigation.Mode... setEnabledOn) {
        super(label, icon);
        this.priority = priority;
        this.label = label;

        if (action != null) {
            addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SwingUtilities.invokeLater(action);
                }
            });
        }
        for (INavigation.Mode d : setEnabledOn) {
            this.setEnabledOn.add(d);
        }
    }

    /**
     * Constructor.
     * Creates the main navigation element and put all elements together.
     * This constructor does not add an icon.
     *
     * @param priority     The priority of the main navigation element.
     * @param label        The text label that should be displayed.
     * @param action       The action that is run when clicking the sub navigation
     *                     element.
     * @param setEnabledOn All states when the navigation element should be set
     *                     to enabled
     */
    public MainNavigationPopupElement(int priority, String label, final Runnable action, INavigation.Mode... setEnabledOn) {
        this(priority, label, (ImageIcon) null, action, setEnabledOn);
    }

    @Override
    public void setEnabled(INavigation.Mode mode) {
        boolean state = setEnabledOn.contains(mode);
        super.setEnabled(state);
    }

    @Override
    public void forceEnabled(boolean b) {
        super.setEnabled(b);
    }

    @Override
    public int getPriority() {
        return priority;
    }

}
