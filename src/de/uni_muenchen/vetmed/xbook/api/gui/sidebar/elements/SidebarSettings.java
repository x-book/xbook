package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarSettings extends SidebarPanel {

	public SidebarSettings() {
		addTitle(Loc.get("SETTINGS"));
		addTextBlock("");
	}
	
}
