package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarEntry extends SidebarCustomEntry {

	public SidebarEntry() {
        super(Loc.get("SIDEBAR_ENTRY"), Loc.get("FURTHER_INFORMATION_BY_CLICKING_THE_FIELD"));
	}
	
}
