package de.uni_muenchen.vetmed.xbook.api.gui.sidebar;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.CodeTableHashMap;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XSubTitle;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTextBlock;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A JPanel object that is used for displaying the button panel of the content
 * area.
 *
 * It holds several methods to dynamic addition of components. The design of
 * these components are styled in this class.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class SidebarPanel extends JPanel {

    private static ApiControllerAccess apiControllerAccess;
    private ArrayList<Component> focusableItems;

    /**
     * Constructor
     */
    public SidebarPanel() {
        setLayout(new StackLayout());
        setBackground(Colors.SIDEBAR_BACKGROUND);
        focusableItems = new ArrayList<>();
    }

    /**
     * Constructor.
     *
     * Allows adding a dynamic number of text blocks.
     *
     * @param s The text blocks.
     */
    public SidebarPanel(String... s) {
        this();
        for (int i = 0; i < s.length; i++) {
            addTextBlock(s[i]);
        }
    }

    /**
     * Add a title to the sidebar.
     *
     * @param text The title.
     */
    public void addTitle(String text) {
        XTitle title = new XTitle(text);
        title.setBackground(Colors.SIDEBAR_BACKGROUND);
        add(title);
        focusableItems.add(title);
    }

    /**
     * Add a title to the sidebar.
     *
     * @param text The title.
     */
    public void addSubTitle(String text) {
        XSubTitle subTitle = new XSubTitle(text);
        subTitle.setBackground(Colors.SIDEBAR_BACKGROUND);
        add(subTitle);
        focusableItems.add(subTitle);
    }

    /**
     * Add a text block to the sidebar.
     *
     * The text will be splitted by the String <code>\n\n</code>. Each splitted
     * element will be an own text block, so the line wrapping looks nicer.
     *
     * @param text The text.
     */
    public void addTextBlock(String text) {
        if (text != null && !text.isEmpty()) {
            String[] elements = text.split("\n\n");
            for (String s : elements) {
                JPanel wrapper = new JPanel();
                wrapper.setBackground(Colors.SIDEBAR_BACKGROUND);

                XTextBlock block = new XTextBlock(s);
                block.setBackground(Colors.SIDEBAR_BACKGROUND);
                block.setFont(Fonts.FONT_STANDARD_PLAIN);
                wrapper.add(block);
                focusableItems.add(block);

                add(wrapper);
            }
        }
    }

    /**
     * Add a JPanel to the sidebar.
     *
     * @param panel The JPanel.
     */
    public void addPanel(JPanel panel) {
        add(panel);
        focusableItems.add(panel);
    }

    /**
     * Add a button that will create a PDF file of the code table values that
     * will be opened when you click on the button.
     *
     * @param columnType The correspondenting ColumnType object that holds the
     * information how to get the code tables.
     * @param printIds <code>true</code> to print the ids of the values as well.
     */
    public XButton addCreateInformationButton(ColumnType columnType, boolean printIds) {
        return addCreateInformationButton(Loc.get("SHOW_AVAILABLE_VALUES"), columnType, printIds);
    }

    /**
     * Add a button that will create a PDF file of the code table values that
     * will be opened when you click on the button.
     *
     * @param columnType The correspondenting ColumnType object that holds the
     * information how to get the code tables.
     * @param printIds <code>true</code> to print the ids of the values as well.
     * @param label The button label.
     */
    public XButton addCreateInformationButton(String label, final ColumnType columnType, final boolean printIds) {
        JPanel wrapper = new JPanel(new BorderLayout());
        wrapper.setBackground(Colors.SIDEBAR_BACKGROUND);
        wrapper.setBorder(BorderFactory.createEmptyBorder(6, 10, 6, 10));
        XButton button = new XButton(label);
        button.setStyle(XButton.Style.DARKER);
        button.setBackgroundButton(Color.WHITE);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("pressed button for" + columnType);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            apiControllerAccess.createPdfOfCodeTableValues(columnType, printIds);
                        } catch (NotLoggedInException | StatementNotExecutedException ex) {
                            Logger.getLogger(SidebarPanel.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }).start();
            }
        });
        wrapper.add(BorderLayout.CENTER, button);
        add(wrapper);

        focusableItems.add(button.getButton());
        return button;
    }

    /**
     * Add a button that will create a PDF file of the code table values that
     * will be opened when you click on the button. The IDs of the values won't
     * be printed.
     *
     * @param columnType The correspondenting ColumnType object that holds the
     * information how to get the code tables.
     */
    public XButton addCreateInformationButton(final ColumnType columnType) {
        return addCreateInformationButton(columnType, false);
    }

    /**
     * Add a button that will open a specific file when clicking on it.
     *
     * @param text The button label.
     * @param pathToFile The path of the file that should be opened.
     */
    public XButton addOpenFileButton(String text, final String pathToFile) {
        return addOpenFileButton(text, pathToFile, null);
    }

    /**
     * Add a button that will open a specific file when clicking on it.
     *
     * @param text The button label.
     * @param pathToFile The path of the file that should be opened.
     * @param icon An icon that is displayed on the button.
     */
    public XButton addOpenFileButton(String text, final String pathToFile, ImageIcon icon) {
        JPanel wrapper = new JPanel(new BorderLayout());
        wrapper.setBackground(Colors.SIDEBAR_BACKGROUND);
        wrapper.setBorder(BorderFactory.createEmptyBorder(6, 10, 6, 10));
        XButton button = new XButton(text);
        button.setBackgroundButton(Color.WHITE);
        if (icon != null) {
            button.setIcon(icon);
        }
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Desktop.getDesktop().open(new File(pathToFile));
                } catch (IOException ignored) {
                    Logger.getLogger(SidebarPanel.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        });
        wrapper.add(BorderLayout.CENTER, button);
        add(wrapper);

        focusableItems.add(button.getButton());
        return button;
    }

    /**
     * Add an image to the sidebar.
     *
     * @param imageUrl The path of the image.
     */
    public void addImage(ImageIcon imageUrl) {
        JPanel p = new JPanel();
        p.setBackground(Colors.SIDEBAR_BACKGROUND);
        JLabel image = new JLabel();
        image.setBackground(Colors.SIDEBAR_BACKGROUND);
        image.setIcon(imageUrl);
        p.add(image);
        focusableItems.add(image);
        add(p);
    }

    /**
     * Adds an HTML pane to the sidebar.
     *
     * Use this panel to display any HTML content. Not all HTML tags are
     * supported in Java (e.g. iframe elements won't work), but the basic
     * elements like text formatting, images, etc... will work.
     *
     * @param htmlCode The html code (without <html><body>...</body></html>)
     */
    public void addHtmlPane(String htmlCode) {
        addHtmlPane(htmlCode, true);
    }
    /**
     * Adds an HTML pane to the sidebar.
     *
     * Use this panel to display any HTML content. Not all HTML tags are
     * supported in Java (e.g. iframe elements won't work), but the basic
     * elements like text formatting, images, etc... will work.
     *
     * @param htmlCode The html code (without <html><body>...</body></html>)
     * @param replaceBreakLineWithHtmlTag Wether to replace a \n with a <br />
     * tag, or not
     */
    public void addHtmlPane(String htmlCode, boolean replaceBreakLineWithHtmlTag) {
        if (replaceBreakLineWithHtmlTag) {
            htmlCode = htmlCode.replace("\n", "<br />");
        }

        JEditorPane jEditorPane = new JEditorPane();
        jEditorPane.setBackground(Colors.SIDEBAR_BACKGROUND);
        jEditorPane.setEditorKit(JEditorPane.createEditorKitForContentType("text/html"));
        jEditorPane.setEditable(false);
        jEditorPane.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    if (Desktop.isDesktopSupported()) {
                        try {
                            Desktop.getDesktop().browse(e.getURL().toURI());
                        } catch (IOException | URISyntaxException ex) {
                            Logger.getLogger(SidebarPanel.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        });
        // add a HTMLEditorKit to the editor pane
        HTMLEditorKit kit = new HTMLEditorKit();
        jEditorPane.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule("body #motd { font-family: SansSerif; font-size: 9px;}");
        styleSheet.addRule("#motd h1 { font-size: 12px; }");
        styleSheet.addRule("#motd h2 { font-size: 10px; }");
        styleSheet.addRule("#motd h3 { font-size: 9px; }");
        styleSheet.addRule("#motd div { padding-bottom: 8px; }");

        jEditorPane.setText("<html><body><div id='motd'>" + htmlCode + "</div></body></html>");

        // now add it to a scroll pane
        add(jEditorPane);
        focusableItems.add(jEditorPane);
    }

    public void addCodeTableListing(ColumnType columnType) {
        try {
            addTextBlock(Loc.get("VALID_VALUES") + ":");
            CodeTableHashMap items = apiControllerAccess.getHashedCodeTableEntries(columnType);
            String listing = "";
            for (String values : items.values()) {
                listing += "- " + values + "\n";
            }
            if (listing.endsWith("\n")) {
                listing = listing.substring(0, listing.length() - "\n".length());
            }
            addTextBlock(listing);
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(SidebarPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void setApiControllerAccess(ApiControllerAccess apiControllerAccess) {
        SidebarPanel.apiControllerAccess = apiControllerAccess;
    }

    public ArrayList<Component> getFocusableItems() {
        return focusableItems;
    }

}
