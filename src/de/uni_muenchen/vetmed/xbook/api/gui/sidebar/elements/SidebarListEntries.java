package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarListEntries extends SidebarPanel {

	public SidebarListEntries() {
		addTitle(Loc.get("LIST_ENTRIES"));
		addTextBlock(Loc.get("SIDEBAR_LIST_ENTRIES"));
		addTextBlock(Loc.get("SIDEBAR_LIST_ENTRIES2"));
		addTextBlock(Loc.get("SIDEBAR_LIST_ENTRIES3"));
		addTitle(Loc.get("EDITING_OF_MULTIPLE_ENTRIES"));
		addTextBlock(Loc.get("SIDEBAR_LIST_ENTRIES_MULTI_EDIT"));
	}
}
