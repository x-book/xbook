package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarSolveConflict extends SidebarPanel {

    public SidebarSolveConflict() {
        addTitle(Loc.get("SOLVE_CONFLICT"));
        addTextBlock(Loc.get("SIDEBAR_SOLVE_CONFICT", Loc.get("SOLVE_CONFLICT"), Loc.get("NOTIFY_PROJECT_OWNER")));
    }

}
