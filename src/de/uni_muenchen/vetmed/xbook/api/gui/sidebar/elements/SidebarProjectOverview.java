package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarProjectOverview extends SidebarPanel {

    public SidebarProjectOverview() {
        addTitle(getProjectOverviewLabel());
        addTextBlock(Loc.get("SIDEBAR_PROJECT"));
    }

    protected String getProjectOverviewLabel() {
        return Loc.get("PROJECT_OVERVIEW");
    }
}
