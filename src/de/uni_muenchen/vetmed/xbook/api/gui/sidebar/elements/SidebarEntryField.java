package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarEntryField extends SidebarCustomEntryField {

    public SidebarEntryField(ColumnType columnType, String sidebarText) {
        super(columnType.getDisplayName(), sidebarText, new String[]{Loc.get("SIDEBAR_ENTRY"), Loc.get("FURTHER_INFORMATION_BY_CLICKING_THE_FIELD")});
    }

    public SidebarEntryField(String displayName, String sidebarText) {
        super(displayName, sidebarText, new String[]{Loc.get("SIDEBAR_ENTRY"), Loc.get("FURTHER_INFORMATION_BY_CLICKING_THE_FIELD")});
    }
}
