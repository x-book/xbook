package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarConflicts extends SidebarPanel {

	public SidebarConflicts() {
		addTitle(Loc.get("CONFLICT_OVERVIEW"));
		addTextBlock(Loc.get("SIDEBAR_CONFLICT_OVERVIEW"));
	}
	
}
