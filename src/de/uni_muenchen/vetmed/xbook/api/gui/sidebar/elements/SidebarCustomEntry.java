package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarCustomEntry extends SidebarPanel {

    public SidebarCustomEntry(String... text) {
        addTitle(Loc.get("INPUT_FIELDS"));
        if (text.length > 0) {
            addSubTitle(Loc.get("GENERAL"));
            for (String t : text) {
                addTextBlock(t);
            }
        }
    }
    
    public SidebarCustomEntry(boolean HtmlIndicator, String... text) {
        addTitle(Loc.get("INPUT_FIELDS"));
        if (text.length > 0) {
            addSubTitle(Loc.get("GENERAL"));
            for (String t : text) {
                addHtmlPane(t);
            }
        }
    }

}
