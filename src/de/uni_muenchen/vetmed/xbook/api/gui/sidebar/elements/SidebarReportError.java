package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarReportError extends SidebarPanel {

	public SidebarReportError() {
		addTitle(Loc.get("REPORT_ERROR"));
		addTextBlock("");
	}
	
}
