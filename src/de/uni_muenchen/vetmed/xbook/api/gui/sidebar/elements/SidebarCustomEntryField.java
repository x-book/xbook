package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.helper.EntryModeHelper;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarCustomEntryField extends SidebarPanel {

    public SidebarCustomEntryField(ColumnType columnType, String sidebarText, String... generalText) {
        this(columnType.getDisplayName(), sidebarText, generalText);
    }

    public SidebarCustomEntryField(ColumnType columnType, String sidebarText) {
        this(columnType.getDisplayName(), sidebarText, new String[]{});
    }

    public SidebarCustomEntryField(String displayName, String sidebarText) {
        this(displayName, sidebarText, new String[]{});
    }

    public SidebarCustomEntryField(String displayName, String sidebarText, String... generalText) {
        addTitle(Loc.get("INPUT_FIELDS"));
        addSubTitle(displayName);
        if (sidebarText != null && !sidebarText.isEmpty()) {
            addHtmlPane(sidebarText);
        }

        if (EntryModeHelper.isMultiEntryMode()) {
            addHtmlPane("");
            addSubTitle(Loc.get("GENERAL"));
            addHtmlPane("<div style='padding: 6px; background-color: yellow;'><b>" + Loc.get("MULTIPLE_ENTRIES_ARE_LOADED") + "</b></div>");
            addHtmlPane(Loc.get("SIDEBAR_MULTI_ENTRY"));
        } else {
            if (generalText.length > 0) {
                addHtmlPane("");
                addSubTitle(Loc.get("GENERAL"));
                for (String t : generalText) {
                    addHtmlPane(t);
                }
            }
        }
    }
}
