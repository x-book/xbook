package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarUpdate extends SidebarPanel {

	public SidebarUpdate() {
		addTitle(Loc.get("APPLICATION_UPDATE"));
		addTextBlock(Loc.get("SIDEBAR_APPLICATION_UPDATE"));
	}
}
