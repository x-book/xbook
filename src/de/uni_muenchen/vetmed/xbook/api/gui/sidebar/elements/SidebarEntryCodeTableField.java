package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import java.util.ArrayList;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarEntryCodeTableField extends SidebarPanel {

    public SidebarEntryCodeTableField(ColumnType columnType, String sidebarText, boolean displayButton, boolean printIds) {
        ArrayList<EntryOverview> items = new ArrayList<>();
        items.add(new EntryOverview(columnType, sidebarText, displayButton, printIds));
        createContent(items);
    }

    public SidebarEntryCodeTableField(ArrayList<EntryOverview> entry) {
        createContent(entry);
    }

    public SidebarEntryCodeTableField(ColumnType columnType, String sidebarText) {
        this(columnType, sidebarText, true, false);
    }

    private void createContent(ArrayList<EntryOverview> entry) {
        addTitle(Loc.get("INPUT_FIELDS"));
        for (EntryOverview overview : entry) {
            addSubTitle(overview.columnType.getDisplayName());
            if (overview.sidebarText != null && !overview.sidebarText.isEmpty()) {
                addHtmlPane(overview.sidebarText);
            }
            if (overview.displayButton) {
                addCreateInformationButton(overview.columnType, overview.printIDs);
            }
            addTextBlock("");
        }
        addSubTitle(Loc.get("GENERAL"));
        addTextBlock(Loc.get("SIDEBAR_ENTRY"));
        addTextBlock(Loc.get("FURTHER_INFORMATION_BY_CLICKING_THE_FIELD"));
    }

    public static class EntryOverview {

        public String sidebarText;
        public ColumnType columnType;
        public boolean displayButton;
        public boolean printIDs;

        public EntryOverview(ColumnType columnType, String sidebarText) {
            this(columnType, sidebarText, true, false);
        }

        public EntryOverview(ColumnType columnType, String sidebarText, boolean displayButton, boolean printIDs) {
            this.sidebarText = sidebarText;
            this.columnType = columnType;
            this.printIDs = printIDs;
            this.displayButton = displayButton;
        }

    }
}
