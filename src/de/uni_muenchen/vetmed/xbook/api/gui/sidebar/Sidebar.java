package de.uni_muenchen.vetmed.xbook.api.gui.sidebar;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.ScrollablePanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Sidebar extends JPanel {

    /**
     * The object itself. Necessary for static methods.
     */
    private static Sidebar thisElement;
    /**
     * The (outer) top and bottom padding around the sidebar in pixel.
     */
    private final int PADDING_TOP_BOTTOM = 16;
    /**
     * The (outer) left padding around the sidebar in pixel.
     */
    private final int PADDING_LEFT = 8;
    /**
     * The (inner) margin inside of the sidebar in pixel.
     */
    private final int MARGIN = 5;
    /**
     * The content panel of the sidebar.
     */
    private final JPanel sidebarWrapper;
    /**
     * The JScrollPane around the sidebar.
     */
    private JScrollPane scroll;

    /**
     * Constructor
     */
    public Sidebar() {
        this.thisElement = this;

        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(300 + PADDING_TOP_BOTTOM, 300));
        setBackground(Colors.CONTENT_BACKGROUND);

        JPanel borderRight = new JPanel(new BorderLayout());
        borderRight.setBackground(Colors.SIDEBAR_BACKGROUND);
        borderRight.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Colors.INPUT_FIELD_INPUT_BORDER));
        borderRight.setPreferredSize(new Dimension(300 + MARGIN, 300));

        JPanel p = new JPanel(new BorderLayout());
        p.setLayout(new BorderLayout());
        p.setPreferredSize(new Dimension(300, 300));
        p.setBackground(Colors.SIDEBAR_BACKGROUND);
        p.setBorder(new EmptyBorder(MARGIN, MARGIN, MARGIN, MARGIN));

        sidebarWrapper = new ScrollablePanel(new BorderLayout());
        sidebarWrapper.setBackground(Colors.SIDEBAR_BACKGROUND);
        scroll = new JScrollPane(sidebarWrapper);
        scroll.getVerticalScrollBar().setUnitIncrement(16);
        scroll.setBackground(Colors.SIDEBAR_BACKGROUND);
        scroll.setBorder(new EmptyBorder(0, 0, 0, 0));
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        p.add(BorderLayout.CENTER, scroll);

        borderRight.add(BorderLayout.EAST, p);

        add(BorderLayout.EAST, borderRight);
    }

    /**
     * Toggles the visiblity of the sidebar.
     */
//    public static void toggleSidebar() {
//        if (thisElement.isVisible()) {
//            SettingView.setSidebarVisibilityProperty(false);
//            hideSidebar();
//        } else {
//            SettingView.setSidebarVisibilityProperty(true);
//            showSidebar();
//        }
//    }

    /**
     * Display the sidebar.
     */
    public static void showSidebar() {
        thisElement.setVisible(true);
    }

    /**
     * Hide the sidebar.
     */
    public static void hideSidebar() {
        thisElement.setVisible(false);
    }

    /**
     * Display a specific sidebar element.
     *
     * @param content The sidebar element to display.
     */
    public static void setSidebarContent(JComponent content) {
        if (thisElement != null) {

            // clear the sidebar and set the new content to the sidebar
            thisElement.sidebarWrapper.removeAll();
            if (content != null) {
                thisElement.sidebarWrapper.add(BorderLayout.NORTH, content);
            }

            // refresh the GUI
            thisElement.revalidate();
            thisElement.repaint();
            thisElement.sidebarWrapper.revalidate();
            thisElement.sidebarWrapper.repaint();

            // automatically scroll to the top after adding the content
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    thisElement.scroll.getVerticalScrollBar().setValue(0);
                }
            });
        }

    }

    public static void clearSidebarContent() {
        setSidebarContent(null);
    }

    public static boolean isSidebarVisible() {
        return thisElement.isVisible();
    }
}
