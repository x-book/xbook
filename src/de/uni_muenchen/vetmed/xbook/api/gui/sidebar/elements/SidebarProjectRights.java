package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarProjectRights extends SidebarPanel {

	public SidebarProjectRights(String title) {
		addTitle(title);
		addTextBlock(Loc.get("SIDEBAR_USER_RIGHTS"));
	}
}
