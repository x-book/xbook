package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;

/**
 * @author Daniel Kaltenthaler
 */
public class SidebarProjectEditField extends SidebarPanel {

    public SidebarProjectEditField(String displayName, String sidebarText, boolean isEditMode) {
        if (isEditMode) {
            addTitle(Loc.get("EDIT_PROJECT"));
        } else {
            addTitle(Loc.get("CREATE_NEW_PROJECT"));
        }
        addSubTitle(displayName);
        if (sidebarText != null && !sidebarText.isEmpty()) {
            addTextBlock(sidebarText);
        }
        addTextBlock("");
        addSubTitle(Loc.get("GENERAL"));
        if (isEditMode) {
            addTextBlock(Loc.get("SIDEBAR_EDIT_PROJECT"));
        } else {
            addTextBlock(Loc.get("SIDEBAR_CREATE_NEW_PROJECT"));
        }
        addTextBlock(Loc.get("FURTHER_INFORMATION_BY_CLICKING_THE_FIELD"));
    }

}
