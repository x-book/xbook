package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarCodeTableListing extends SidebarPanel {

    public SidebarCodeTableListing(ColumnType columnType, String sidebarText) {
        this(columnType, sidebarText, true);
    }

    public SidebarCodeTableListing(ColumnType columnType, String sidebarText, boolean bShowText) {
        addTitle(Loc.get("INPUT_FIELDS"));
        addSubTitle(columnType.getDisplayName());
        if (sidebarText != null && !sidebarText.isEmpty()) {
            addTextBlock(sidebarText);
        }
        if (bShowText) {
            addCodeTableListing(columnType);
        }
        addCreateInformationButton(columnType);
        addTextBlock("");
        addSubTitle(Loc.get("GENERAL"));
        addTextBlock(Loc.get("SIDEBAR_ENTRY"));
        addTextBlock(Loc.get("FURTHER_INFORMATION_BY_CLICKING_THE_FIELD"));
    }
}
