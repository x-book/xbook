package de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements;

import de.uni_muenchen.vetmed.xbook.api.Loc;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class SidebarMultiEntry extends SidebarCustomEntry {

    public SidebarMultiEntry() {
        super(true, "<div style='padding: 6px; background-color: yellow;'><b>" + Loc.get("MULTIPLE_ENTRIES_ARE_LOADED") + "</b></div>", Loc.get("SIDEBAR_MULTI_ENTRY"), Loc.get("FURTHER_INFORMATION_BY_CLICKING_THE_FIELD"));
    }

}
