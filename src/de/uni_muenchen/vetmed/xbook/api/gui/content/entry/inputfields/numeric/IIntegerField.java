package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface IIntegerField extends IInputElement {

	/**
	 * Set a new number as value.
	 *
	 * @param number The new value as String.
	 */
	void setInteger(String number);

	/**
	 * Set a new number as value.
	 *
	 * @param number The new value as Float.
	 */
	void setInteger(Integer number);

	/**
	 * Get the current value as a String value. Return
	 * <code>null</code> if no value is entered.
	 *
	 * @return The current value as a String value.
	 */
	String getIntegerAsString();

	/**
	 * Get the current value as a Integer value. Return
	 * <code>null</code> if no value is entered.
	 *
	 * @return The current value as a Integer value.
	 */
	Integer getInteger();
}
