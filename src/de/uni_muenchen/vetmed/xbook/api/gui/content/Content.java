package de.uni_muenchen.vetmed.xbook.api.gui.content;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

/**
 * The basic content object.
 *
 * It holds several static methods to set it up.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Content extends JPanel {

    /**
     * The object itself. Necessary for static methods.
     */
    private static Content thisElement;
    /**
     * The content panel that is displayed.
     */
    private static AbstractContent currentContent;
    /**
     * The content panel that was displayed before the current content was load.
     */
    private static AbstractContent previousContent;

    /**
     * Constructor
     *
     * Sets the basic layout of the content.
     */
    public Content() {
        this.thisElement = this;
        setLayout(new BorderLayout());
        setBackground(Colors.CONTENT_BACKGROUND);
    }

    /**
     * Display a specific content.
     *
     * @param content The content to display.
     */
    public static void setContent(final AbstractContent content) {
        if (content != null) {
            // ony save the current conent panel as previous panel if the conent is allowed to be returned
            // leave the old content else!
            if (!(currentContent instanceof CannotBeReturnedOnPressingBackButton)) {
                thisElement.previousContent = thisElement.currentContent;
            }

            thisElement.currentContent = content;
            // rebuild the content panel
            thisElement.removeAll();
            thisElement.add(BorderLayout.CENTER, content);
            thisElement.revalidate();
            thisElement.repaint();
            thisElement.currentContent.actionOnDisplay();
            thisElement.currentContent.updateContent();

//            if (content.forceSidebar()) {
//                Sidebar.showSidebar();
//            } else {
//                Sidebar.hideSidebar();
//            }
            Footer.updateToggleSidebarButtonImages();

            thisElement.currentContent.setFocus();
        }
    }

    /**
     * Returns the current content that is display to the content area.
     *
     * @return The current content.
     */
    public static AbstractContent getCurrentContent() {
        return thisElement.currentContent;
    }

    public static void setPreviousContent() {
        setContent(thisElement.previousContent);
    }
}
