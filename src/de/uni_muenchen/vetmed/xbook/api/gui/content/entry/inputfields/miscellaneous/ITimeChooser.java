package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface ITimeChooser extends IInputElement {

    /**
     * Set a new time as a String to the input field. The String should have the
     * format "HH:MM" or "HH:MM:SS". Due the database only saves values in the
     * format "HH:MM" the input field should not display the seconds.
     *
     * @param time The time as String in the format "HH:MM" or "HH:MM:SS".
     */
    void setTime(String time);

    /**
     * Returns the input value for the time in the format "HH:MM".
     *
     * @return The time in the format "HH:MM".
     */
    String getTime();

}
