package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import java.text.ParseException;

public interface IDateChooser extends IInputElement {

    /**
     * Set a date as String in the format "yyyy-MM-dd" as the selected date.
     *
     * @param date The date to set.
     * @throws ParseException if the date has an invalid format.
     */
    void setDate(String date) throws ParseException;

    /**
     * Get the selected date as a String in the format "yyyy-MM-dd".
     *
     * @return The selected date.
     */
    String getDate();
}
