package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.UpdateableEntry;

import java.awt.*;
import java.util.ArrayList;

public interface IInputElement {

    /**
     * Load the data and fills it to the input field.
     *
     * @param data The EntryData object that holds all data (the data of other
     * input fields as well!)
     */
    void load(DataSetOld data);

    /**
     * Load the multi edit data and fills it to the input field.
     *
     * @param data The EntryData object that holds all data of the selected
     * entries for the multi edit. Identical values are added as values,
     * different values are added as empty values, no data is not included at
     * all.
     */
    void loadMultiEdit(DataSetOld data);

    /**
     * Save the data.
     *
     * @param data The EntryData object where to save the data.
     */
    void save(DataSetOld data) throws IsAMandatoryFieldException;

    /**
     * Clear the input field.
     */
    void clear();

    /**
     * Converts the current input into an string.
     *
     * @return The input as a String.
     */
    String toString();

    /**
     * Set the focus to this input field.
     */
    void setFocus();

    /**
     * Returns if the input field is a mandatory field or not.
     *
     * @return The status if the input field is a mandatory field or not.
     */
    boolean isMandatory();

    /**
     * Check if the entered input of the input element is valid or not.
     *
     * @return <code>true</code> if it is a valid input, <code>false</code>
     * else.
     */
    boolean isValidInput();

    /**
     * Method is run when the input element gains the focus.
     */
    void actionOnFocusGain();

    /**
     * Method is run when the input element loses the focus.
     */
    void actionOnFocusLost();

    /**
     * Set the focus to the input field.
     */
    void requestFocus();

    /**
     * Returns a list of all components that are focusable. Input fields should
     * override this method to define all specific components.
     *
     * This method SHOULD NOT BE CALLED from other methods than
     * getFocusableComponents() !
     *
     * @return A list of all focusable components.
     */
    ArrayList<Component> getMyFocusableComponents();

    /**
     * Sets the parent container that holds all input fields.
     *
     * E.g. Entry, ProjectEdit, ...
     *
     * @param entry The parent container .
     */
    void setEntry(UpdateableEntry entry);
    
    String getStringRepresentation();
}
