package de.uni_muenchen.vetmed.xbook.api.gui.content.entry;

import de.uni_muenchen.vetmed.xbook.api.exception.WrongModeException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.StatusLabel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;

/**
 * Created by lohrer on 01.06.2015.
 */
public interface UpdateableEntry {

    void updateUpdateableInputFields();

    void updateUpdateableInputFieldsSingle();

    void updateUpdateableInputFieldsMultiEdit();

    SidebarPanel getSideBar();

    /**
     * Removes the focus from the current focused field.
     */
    public void removeCurrentFocus();

    /**
     * Sets a specific input field focused.
     *
     * @param input The input field to set focused.
     */
    void setInputFieldFocused(IInputElement input);

    /**
     * Updates the visibility of all input fields.
     * <p/>
     * Check the properties for the correct values, iterates all input fields
     * and set the fields visible or not.
     */
    void updateInputFieldVisibility();

    void revertMultiEditInputField(AbstractInputElement fieldToReload) throws WrongModeException;
}
