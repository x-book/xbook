package de.uni_muenchen.vetmed.xbook.api.gui.content;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import javax.swing.JPanel;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XIconButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

/**
 * A JPanel object that is used for displaying the button panel of the content
 * area.
 * <p>
 * It holds several methods to dynamic addition of components. The design of
 * these components are styled in this class.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ButtonPanel extends JPanel {

    /**
     * A panel that hold all components that are displayed on the north-left
     * side of the button panel.
     */
    private final JPanel buttonsLeftNorth;
    /**
     * A panel that hold all components that are displayed on the south-left
     * side of the button panel.
     */
    private final JPanel buttonsLeftSouth;
    /**
     * A panel that hold all components that are displayed on the north-center
     * of the button panel.
     */
    private final JPanel buttonsCenterNorth;
    /**
     * A panel that hold all components that are displayed on the south-center
     * of the button panel.
     */
    private final JPanel buttonsCenterSouth;
    /**
     * A panel that hold all components that are displayed on the north-right
     * side of the button panel.
     */
    private final JPanel buttonsRightNorth;
    /**
     * A panel that hold all components that are displayed on the south-right
     * side of the button panel.
     */
    private final JPanel buttonsRightSouth;

    /**
     * Constructor
     */
    public ButtonPanel() {
        setLayout(new StackLayout());
        setBackground(Colors.BUTTON_PANEL_BACKGROUND);

        // northern button line
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBackground(Colors.BUTTON_PANEL_BACKGROUND);

        buttonsLeftNorth = new JPanel(new FlowLayout());
        buttonsLeftNorth.setBackground(Colors.BUTTON_PANEL_BACKGROUND);
        panel.add(BorderLayout.WEST, buttonsLeftNorth);

        buttonsCenterNorth = new JPanel(new FlowLayout());
        buttonsCenterNorth.setBackground(Colors.BUTTON_PANEL_BACKGROUND);
        panel.add(BorderLayout.CENTER, buttonsCenterNorth);

        buttonsRightNorth = new JPanel(new FlowLayout());
        buttonsRightNorth.setBackground(Colors.BUTTON_PANEL_BACKGROUND);
        panel.add(BorderLayout.EAST, buttonsRightNorth);

        add(panel);

        // southern button line
        JPanel q = new JPanel(new BorderLayout());
        q.setBackground(Colors.BUTTON_PANEL_BACKGROUND);

        buttonsLeftSouth = new JPanel(new FlowLayout());
        buttonsLeftSouth.setBackground(Colors.BUTTON_PANEL_BACKGROUND);
        q.add(BorderLayout.WEST, buttonsLeftSouth);

        buttonsCenterSouth = new JPanel(new FlowLayout());
        buttonsCenterSouth.setBackground(Colors.BUTTON_PANEL_BACKGROUND);
        q.add(BorderLayout.CENTER, buttonsCenterSouth);

        buttonsRightSouth = new JPanel(new FlowLayout());
        buttonsRightSouth.setBackground(Colors.BUTTON_PANEL_BACKGROUND);
        q.add(BorderLayout.EAST, buttonsRightSouth);

        add(q);
    }

    /**
     * Creates and adds a XButton to the north-right side of the button panel.
     *
     * @param label           The label of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XButton object itself.
     */
    public XButton addButtonToNorthWest(String label, double widthPercentage, ActionListener listener) {
        return (XButton) buttonsLeftNorth.add(createButton(label, widthPercentage, listener));
    }

    /**
     * Creates and adds a XButton to the north-right side of the button panel.
     *
     * @param label    The label of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XButton object itself.
     */
    public XButton addButtonToNorthWest(String label, ActionListener listener) {
        return (XButton) buttonsLeftNorth.add(createButton(label, 1.0, listener));
    }

    /**
     * Creates and adds a XButton to the south-right side of the button panel.
     *
     * @param label           The label of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XButton object itself.
     */
    public XButton addButtonToSouthWest(String label, double widthPercentage, ActionListener listener) {
        return (XButton) buttonsLeftSouth.add(createButton(label, widthPercentage, listener));
    }

    /**
     * Creates and adds a XButton to the south-right side of the button panel.
     *
     * @param label    The label of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XButton object itself.
     */
    public XButton addButtonToSouthWest(String label, ActionListener listener) {
        return (XButton) buttonsLeftSouth.add(createButton(label, 1.0, listener));
    }

    /**
     * Creates and adds a XButton to the north-center side of the button panel.
     *
     * @param label           The label of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XButton object itself.
     */
    public XButton addButtonToNorthCenter(String label, double widthPercentage, ActionListener listener) {
        return (XButton) buttonsCenterNorth.add(createButton(label, widthPercentage, listener));
    }

    /**
     * Creates and adds a XButton to the north-center side of the button panel.
     *
     * @param label    The label of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XButton object itself.
     */
    public XButton addButtonToNorthCenter(String label, ActionListener listener) {
        return (XButton) buttonsCenterNorth.add(createButton(label, 1.0, listener));
    }

    /**
     * Creates and adds a XButton to the south-center side of the button panel.
     *
     * @param label           The label of the button.
     * @param listener        An ActionListener when clicking the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @return The XButton object itself.
     */
    public XButton addButtonToSouthCenter(String label, double widthPercentage, ActionListener listener) {
        return (XButton) buttonsCenterSouth.add(createButton(label, widthPercentage, listener));
    }

    /**
     * Creates and adds a XButton to the south-center side of the button panel.
     *
     * @param label    The label of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XButton object itself.
     */
    public XButton addButtonToSouthCenter(String label, ActionListener listener) {
        return (XButton) buttonsCenterSouth.add(createButton(label, 1.0, listener));
    }

    /**
     * Creates and adds a XButton to the north-left side of the button panel.
     *
     * @param label           The label of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XButton object itself.
     */
    public XButton addButtonToNorthEast(String label, double widthPercentage, ActionListener listener) {
        return (XButton) buttonsRightNorth.add(createButton(label, widthPercentage, listener));
    }

    /**
     * Creates and adds a XButton to the north-left side of the button panel.
     *
     * @param label    The label of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XButton object itself.
     */
    public XButton addButtonToNorthEast(String label, ActionListener listener) {
        return (XButton) buttonsRightNorth.add(createButton(label, 1.0, listener));
    }

    /**
     * Creates and adds a XButton to the south-left side of the button panel.
     *
     * @param label           The label of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XButton object itself.
     */
    public XButton addButtonToSouthEast(String label, double widthPercentage, ActionListener listener) {
        return (XButton) buttonsRightSouth.add(createButton(label, widthPercentage, listener));
    }

    /**
     * Creates and adds a XButton to the south-left side of the button panel.
     *
     * @param label    The label of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XButton object itself.
     */
    public XButton addButtonToSouthEast(String label, ActionListener listener) {
        return (XButton) buttonsRightSouth.add(createButton(label, 1.0, listener));
    }

    /**
     * Creates a JButton object and do its setup.
     *
     * @param label           The label of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XButton object itself.
     */
    private XButton createButton(String label, double widthPercentage, ActionListener listener) {
        XButton button = new XButton(label);
        button.setButtonSizeToDefault();
        if (listener != null) {
            button.addActionListener(listener);
        }
        return button;
    }

    /**
     * Creates and adds a XIconButton to the north-right side of the button
     * panel.
     *
     * @param icon     The icon to display.
     * @param tooltip  The tooltip of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XIconButton addIconButtonToNorthEast(ImageIcon icon, String tooltip, ActionListener listener) {
        return (XIconButton) buttonsRightNorth.add(createIconButton(icon, tooltip, 1.0, listener));
    }

    /**
     * Creates and adds a XIconButton to the north-right side of the button
     * panel.
     *
     * @param icon            The icon to display.
     * @param tooltip         The tooltip of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XIconButton addIconButtonToNorthEast(ImageIcon icon, String tooltip, double widthPercentage, ActionListener listener) {
        return (XIconButton) buttonsRightNorth.add(createIconButton(icon, tooltip, widthPercentage, listener));
    }

    /**
     * Creates and adds a XIconButton to the north-center side of the button
     * panel.
     *
     * @param icon     The icon to display.
     * @param tooltip  The tooltip of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XIconButton addIconButtonToNorthCenter(ImageIcon icon, String tooltip, ActionListener listener) {
        return (XIconButton) buttonsCenterNorth.add(createIconButton(icon, tooltip, 1.0, listener));
    }

    /**
     * Creates and adds a XIconButton to the north-center side of the button
     * panel.
     *
     * @param icon            The icon to display.
     * @param tooltip         The tooltip of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XIconButton addIconButtonToNorthCenter(ImageIcon icon, String tooltip, double widthPercentage, ActionListener listener) {
        return (XIconButton) buttonsCenterNorth.add(createIconButton(icon, tooltip, widthPercentage, listener));
    }

    /**
     * Creates and adds a XIconButton to the north-left side of the button
     * panel.
     *
     * @param icon     The icon to display.
     * @param tooltip  The tooltip of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XIconButton addIconButtonToNorthWest(ImageIcon icon, String tooltip, ActionListener listener) {
        return (XIconButton) buttonsLeftNorth.add(createIconButton(icon, tooltip, 1.0, listener));
    }

    /**
     * Creates and adds a XIconButton to the north-left side of the button
     * panel.
     *
     * @param icon            The icon to display.
     * @param tooltip         The tooltip of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XIconButton addIconButtonToNorthWest(ImageIcon icon, String tooltip, double widthPercentage, ActionListener listener) {
        return (XIconButton) buttonsLeftNorth.add(createIconButton(icon, tooltip, widthPercentage, listener));
    }

    /**
     * Creates and adds a XIconButton to the south-east side of the button
     * panel.
     *
     * @param icon     The icon to display.
     * @param tooltip  The tooltip of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XIconButton addIconButtonToSouthEast(ImageIcon icon, String tooltip, ActionListener listener) {
        return (XIconButton) buttonsRightSouth.add(createIconButton(icon, tooltip, 1.0, listener));
    }

    /**
     * Creates and adds a XIconButton to the south-east side of the button
     * panel.
     *
     * @param icon            The icon to display.
     * @param tooltip         The tooltip of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XIconButton addIconButtonToSouthEast(ImageIcon icon, String tooltip, double widthPercentage, ActionListener listener) {
        return (XIconButton) buttonsRightSouth.add(createIconButton(icon, tooltip, widthPercentage, listener));
    }

    /**
     * Creates and adds a XIconButton to the south-center side of the button
     * panel.
     *
     * @param icon     The icon to display.
     * @param tooltip  The tooltip of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XIconButton addIconButtonToSouthCenter(ImageIcon icon, String tooltip, ActionListener listener) {
        return (XIconButton) buttonsCenterSouth.add(createIconButton(icon, tooltip, 1.0, listener));
    }

    /**
     * Creates and adds a XIconButton to the south-center side of the button
     * panel.
     *
     * @param icon            The icon to display.
     * @param tooltip         The tooltip of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XIconButton addIconButtonToSouthCenter(ImageIcon icon, String tooltip, double widthPercentage, ActionListener listener) {
        return (XIconButton) buttonsCenterSouth.add(createIconButton(icon, tooltip, widthPercentage, listener));
    }

    /**
     * Creates and adds a XIconButton to the south-left side of the button
     * panel.
     *
     * @param icon     The icon to display.
     * @param tooltip  The tooltip of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XIconButton addIconButtonToSouthWest(ImageIcon icon, String tooltip, ActionListener listener) {
        return (XIconButton) buttonsLeftSouth.add(createIconButton(icon, tooltip, 1.0, listener));
    }

    /**
     * Creates and adds a XIconButton to the south-left side of the button
     * panel.
     *
     * @param icon            The icon to display.
     * @param tooltip         The tooltip of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XIconButton addIconButtonToSouthWest(ImageIcon icon, String tooltip, double widthPercentage, ActionListener listener) {
        return (XIconButton) buttonsLeftSouth.add(createIconButton(icon, tooltip, widthPercentage, listener));
    }

    /**
     *
     */
    public enum Position {

        NORTH_WEST, NORTH_CENTER, NORTH_EAST, SOUTH_WEST, SOUTH_CENTER, SOUTH_EAST
    }

    /**
     * Creates and adds a XIconButton to the south-left side of the button
     * panel.
     *
     * @param position The position where to inser the button.
     * @param icon     The icon to display.
     * @param tooltip  The tooltip of the button.
     * @param listener An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XImageButton addImageButton(Position position, ImageIcon icon, String tooltip, ActionListener listener) {
        return addImageButton(position, icon, tooltip, 1.0, listener);
    }
    /**
     * Creates and adds a XIconButton to the south-left side of the button
     * panel.
     *
     * @param position The position where to inser the button.
     * @param icon     The icon to display.
     * @param tooltip  The tooltip of the button.
     * @param listener An ActionListener when clicking the button.
     * @param index    The index to add the element
     * @return The XIconButton object itself.
     */
    public XImageButton addImageButton(Position position, ImageIcon icon, String tooltip, int index, ActionListener listener) {
        return addImageButton(position, icon, tooltip, 1.0,index, listener);
    }

    /**
     * Creates and adds a XIconButton to the south-left side of the button
     * panel.
     *
     * @param position        The position where to inser the button.
     * @param icon            The icon to display.
     * @param tooltip         The tooltip of the button.
     * @param percentageWidth Modifier to resize the button. 1.0 is 100%.
     * @param listener        An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XImageButton addImageButton(Position position, ImageIcon icon, String tooltip, double percentageWidth, ActionListener listener) {
        return addImageButton(position, icon, tooltip, percentageWidth, -1, listener);

    }

    /**
     * Creates and adds a XIconButton to the south-left side of the button
     * panel.
     *
     * @param position        The position where to inser the button.
     * @param icon            The icon to display.
     * @param tooltip         The tooltip of the button.
     * @param percentageWidth Modifier to resize the button. 1.0 is 100%.
     * @param listener        An ActionListener when clicking the button.
     * @param index           The index to add the element
     * @return The XIconButton object itself.
     */
    public XImageButton addImageButton(Position position, ImageIcon icon, String tooltip, double percentageWidth, int index, ActionListener listener) {
        JPanel target = null;
        switch (position) {
            case NORTH_WEST:
                target = buttonsLeftNorth;
                break;
            case NORTH_CENTER:
                target = buttonsCenterNorth;
                break;
            case NORTH_EAST:
                target = buttonsRightNorth;
                break;
            case SOUTH_WEST:
                target = buttonsLeftSouth;
                break;
            case SOUTH_CENTER:
                target = buttonsCenterSouth;
                break;
            case SOUTH_EAST:
                target = buttonsRightSouth;
                break;
        }
        return (XImageButton) target.add(createImageButton(icon, tooltip, percentageWidth, listener), index);
    }

    /**
     * Creates and adds a XIconButton to the south-left side of the button
     * panel.
     *
     * @param icon            The icon to display.
     * @param tooltip         The tooltip of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    public XImageButton addImageButtonToNorthWest(ImageIcon icon, String tooltip, double widthPercentage, ActionListener listener) {
        return (XImageButton) buttonsLeftNorth.add(createImageButton(icon, tooltip, widthPercentage, listener));
    }

    /**
     * Creates an icon button and do its setup.
     *
     * @param icon            The icon to display.
     * @param tooltip         The tooltip of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    private XIconButton createIconButton(ImageIcon icon, String tooltip, double widthPercentage, ActionListener listener) {
        XIconButton button = new XIconButton(icon, tooltip, widthPercentage);
        if (listener != null) {
            button.addActionListener(listener);
        }
        return button;
    }

    /**
     * Creates an icon button and do its setup.
     *
     * @param icon            The icon to display.
     * @param tooltip         The tooltip of the button.
     * @param widthPercentage A modifier to adjust the width of the button. Use
     *                        1.0 for the default size.
     * @param listener        An ActionListener when clicking the button.
     * @return The XIconButton object itself.
     */
    private XImageButton createImageButton(ImageIcon icon, String tooltip, double widthPercentage, ActionListener listener) {
        XImageButton button;
        button = new XImageButton(icon, widthPercentage);
        button.setToolTipText(tooltip);
        if (listener != null) {
            button.addActionListener(listener);
        }
        return button;
    }

    public JPanel getButtonsCenterNorth() {
        return buttonsCenterNorth;
    }

    public JPanel getButtonsCenterSouth() {
        return buttonsCenterSouth;
    }

    public JPanel getButtonsLeftNorth() {
        return buttonsLeftNorth;
    }

    public JPanel getButtonsLeftSouth() {
        return buttonsLeftSouth;
    }

    public JPanel getButtonsRightNorth() {
        return buttonsRightNorth;
    }

    public JPanel getButtonsRightSouth() {
        return buttonsRightSouth;
    }

    /**
     * Adds a component to a specific position of the button panel.
     *
     * @param position  The position where to inser the component.
     * @param component The component to add.
     */
    public void addTo(Position position, JComponent component) {
        switch (position) {
            case NORTH_WEST:
                buttonsLeftNorth.add(component);
                return;
            case NORTH_CENTER:
                buttonsCenterNorth.add(component);
                return;
            case NORTH_EAST:
                buttonsRightNorth.add(component);
                return;
            case SOUTH_WEST:
                buttonsLeftSouth.add(component);
                return;
            case SOUTH_CENTER:
                buttonsCenterSouth.add(component);
                return;
            case SOUTH_EAST:
                buttonsRightSouth.add(component);
                return;
        }
    }

    /**
     * Removes a component from a specific position of the button panel.
     *
     * @param position  The position where to remove the component.
     * @param component The component to remove.
     */
    public void removeFrom(Position position, JComponent component) {
        switch (position) {
            case NORTH_WEST:
                buttonsLeftNorth.remove(component);
                return;
            case NORTH_CENTER:
                buttonsCenterNorth.remove(component);
                return;
            case NORTH_EAST:
                buttonsRightNorth.remove(component);
                return;
            case SOUTH_WEST:
                buttonsLeftSouth.remove(component);
                return;
            case SOUTH_CENTER:
                buttonsCenterSouth.remove(component);
                return;
            case SOUTH_EAST:
                buttonsRightSouth.remove(component);
                return;
        }
    }
}
