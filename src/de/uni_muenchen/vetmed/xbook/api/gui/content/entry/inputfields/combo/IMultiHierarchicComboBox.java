package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import java.util.List;

public interface IMultiHierarchicComboBox extends IInputElement {

    void setSelectedValue(String id);

    void setSelectedValue(List<String> ids);

    List<DataColumn> getSelectedInputs();

    void setOrderByValue(boolean orderByValue);

    void setLabels(String[] labels);
}
