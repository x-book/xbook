package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface IButton<C extends AbstractContent> extends IInputElement ,de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.IButton{

    /**
     * Set the content that shoud be opened when pressing the button.
     *
     * @param contentToOpen The content to open.
     */
    void setContentToOpen(C contentToOpen);
}
