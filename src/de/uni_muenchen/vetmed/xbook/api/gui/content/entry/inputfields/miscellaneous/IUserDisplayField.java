package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface IUserDisplayField extends IInputElement {

    /**
     * Returns the display name of the user.
     *
     * @return The display name of the user.
     */
    String getUserDisplayName();

    /**
     * Returns the ID of the user.
     *
     * @return The ID of the user.
     */
    String getUserId();

}
