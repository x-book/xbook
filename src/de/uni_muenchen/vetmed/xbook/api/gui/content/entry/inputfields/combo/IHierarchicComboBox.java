package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface IHierarchicComboBox extends IInputElement {

    void setSelectedValue(String id);

    DataColumn getSelectedInput();

    void setOrderByValue(boolean orderByValue);

    void setLabels(String[] labels);

    void setComboHeight(int comboHeight);
}
