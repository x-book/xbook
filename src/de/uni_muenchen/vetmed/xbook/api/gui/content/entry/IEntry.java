package de.uni_muenchen.vetmed.xbook.api.gui.content.entry;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface IEntry extends UpdateableEntry {

}
