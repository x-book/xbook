package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import java.util.List;

public interface IMultiComboTextBox extends IInputElement {

    void addToSelection(String value);

    void addToSelection(List<String> values);

    void removeAllSelectedItems();

    void setSelectedData(String value);

    void setSelectedData(List<String> values);

    List<String> getSelectedData();

}
