package de.uni_muenchen.vetmed.xbook.api.gui.content;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.ScrollablePanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.navigation.AbstractSubNavigation;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.SettingView;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * An abstract class that offers the necessary functions to display content
 * elements.
 * <p>
 * To create a content panel extend this class to implement the content panel
 * and the button bar.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractContent extends JPanel {

    /**
     * The basic main frame object as a static object.
     */
    protected static IMainFrame mainFrame;
    protected static ApiControllerAccess apiControllerAccess;
    /**
     * The (inner) margin inside of the content in pixel.
     */
    private final int MARGIN = 5;
    /**
     * The scroll pane around the content.
     */
    protected JScrollPane scroll;
    /**
     * The panel holding the content.
     */
    protected ScrollablePanel content;
    /**
     * Status if the content is scrollable or not.
     */
    private boolean isScrollable;
    private JPanel topWrapper;

    protected ButtonPanel buttonBar;

    /**
     * Constructor.
     */
    public AbstractContent() {
        this.isScrollable = getIsScrollable();
    }

    /**
     * Constructor.
     *
     * @param isScrollable Status if the content is scrollable or not.
     */
    public AbstractContent(boolean isScrollable) {
        super();
        this.isScrollable = isScrollable;
    }

    /**
     * Method to initialise the content element.
     * <p/>
     * !!! MUST !!! BE CALLED IN THE CONSTRUCOR OF INHERITTED OBJECTS
     */
    public void init() {
        setLayout(new BorderLayout());
        setBackground(Colors.CONTENT_BACKGROUND);

        topWrapper = new JPanel(new StackLayout());
        topWrapper.setBackground(Colors.CONTENT_BACKGROUND);
        add(BorderLayout.NORTH, topWrapper);

        AbstractSubNavigation subNavigation = getSubNavigation();
        if (subNavigation != null) {
            topWrapper.add(subNavigation);
        }

        // setup and display the button bar
        if (buttonBar == null) {
            buttonBar = getButtonBar();
        }
        if (buttonBar != null) {
            if (AbstractConfiguration.getBoolean(AbstractConfiguration.BUTTON_BAR_ON_TOP)) {
                topWrapper.add(buttonBar);
            } else {
                add(BorderLayout.SOUTH, buttonBar);
            }
        }

        scroll = new JScrollPane();
        scroll.getVerticalScrollBar().setUnitIncrement(16);

        // setup and display the content
        content = new ScrollablePanel(new BorderLayout());
        content.add(BorderLayout.CENTER, getContent());

        // setup and display the content
        if (content != null) {
            content.setBorder(new EmptyBorder(MARGIN, MARGIN, MARGIN, MARGIN));
            content.setBackground(Colors.CONTENT_BACKGROUND);

            scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
            scroll.setBackground(Colors.CONTENT_BACKGROUND);
            scroll.setBorder(new EmptyBorder(MARGIN, MARGIN, MARGIN, MARGIN));

            if (isScrollable) {
                scroll.setViewportView(content);
                add(BorderLayout.CENTER, scroll);
            } else {
                add(BorderLayout.CENTER, content);
            }

            // setup and display the button bar
        }
    }

    /**
     * Implement the method to define the elements that are displayed in the
     * content panel.
     *
     * @return The content elements. Must be a <code>JPanel</code> object.
     * <code>null</code> if no content is available (but that would not make any
     * sense?!)
     */
    protected abstract JPanel getContent();

    /**
     * Implement the method to define the elements that are displayed in the
     * button bar.
     *
     * @return The button bar. <code>null</code> if no button bar is available.
     */
    public abstract ButtonPanel getButtonBar();

    /**
     * Implement the method to define the elements that are displayed in the
     * sidebar.
     *
     * @return The sidebar bar. <code>null</code> if no sidebar is available.
     */
    public abstract SidebarPanel getSideBar();

    /**
     * Forces that the sidebar will always be displayed for this content,
     * independent of the user settings.
     *
     * @return Should the sidebar be displayed or not.
     */
    public abstract boolean forceSidebar();

    /**
     * Set the component that is focused when loading the screen.
     */
    public void setFocus() {
    }

    /**
     * Save the apiControllerAccess object as a static variable that is visible
     * within all content panels.
     *
     * @param mainFrame The basic mainframe object.
     */
    public static void setMainFrame(IMainFrame mainFrame) {
        AbstractContent.mainFrame = mainFrame;
    }

    public static void setApiControllerAccess(ApiControllerAccess apiControllerAccess) {
        AbstractContent.apiControllerAccess = apiControllerAccess;
    }

    /**
     * Returns the scrollpane around the content.
     *
     * @return The scrollpane.
     */
    public JScrollPane getScroll() {
        return scroll;
    }

    /**
     * Returns the panel that holds the content.
     *
     * @return The content panel.
     */
    public ScrollablePanel getContentPanel() {
        return content;
    }

    /**
     * Updates the content elements. Can be used for resetting items,
     * translating strings, etc...
     */
    public void updateContent() {
    }

    /**
     * Action that is run everytime the content panel is displayed.
     */
    public void actionOnDisplay() {
        SidebarPanel sideBar = getSideBar();
        if (sideBar == null) {
            Sidebar.hideSidebar();
        } else if (forceSidebar()) {
            Sidebar.showSidebar();
            Sidebar.setSidebarContent(sideBar);
        } else if (SettingView.getSidebarVisibilityProperty()) {
            Sidebar.showSidebar();
            Sidebar.setSidebarContent(sideBar);
        } else {
            Sidebar.hideSidebar();
        }
        Footer.updateToggleSidebarButtonImages();
    }

    /**
     * Defines the action that is done when the application is suddenly
     * disconnected from the server.
     */
    public void actionOnDisconnect() {
    }

    /**
     * Adds any component to the top wrapper that is above the content panel.
     * This component will not be scrollable.
     *
     * @param comp The component to add.
     */
    public void addContentToTopWrapper(JComponent comp) {
        topWrapper.add(comp);
    }

    public AbstractSubNavigation getSubNavigation() {
        return null;
    }

    protected void scrollToTop() {
        scroll.getVerticalScrollBar().setValue(0);
    }

    /**
     * Override this method to set a content panel scrollable or non-scrollable.
     * Default: scrollable.
     *
     * @return
     */
    public boolean getIsScrollable() {
        return true;
    }
}
