package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface ITextField extends IInputElement {

	/**
	 * Sets the display text to the given text.
	 *
	 * @param text
	 */
	void setText(String text);

	/**
	 * Get the current entered text of the text field.
	 *
	 * @return The current entered text of the text field.
	 */
	String getText();
}
