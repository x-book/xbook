package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface IPlaceHolder extends IInputElement {

}
