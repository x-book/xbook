package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiYearElement;

import java.util.List;

/**
 *
 */
public interface IMultiYearChooser extends IInputElement {

    boolean addElement(MultiYearElement element);

    boolean addElements(List<MultiYearElement> element);

    List<MultiYearElement> getElementsFromList();

    MultiYearElement getElementFromInput();

    void removeElement(MultiYearElement elements);

    void removeElements(List<MultiYearElement> elements);

    void removeElementAt(int index);
}
