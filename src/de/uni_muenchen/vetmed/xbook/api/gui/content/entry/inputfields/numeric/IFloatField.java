package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface IFloatField extends IInputElement {

	/**
	 * Set a new number as value.
	 *
	 * @param number The new value as String.
	 */
	void setFloat(String number);

	/**
	 * Set a new number as value.
	 *
	 * @param number The new value as Float.
	 */
	void setFloat(Float number);

	/**
	 * Get the current value as a String value. Return
	 * <code>null</code> if no value is entered.
	 *
	 * @return The current value as a String value.
	 */
	String getFloatAsString();

	/**
	 * Get the current value as a Float value. Return
	 * <code>null</code> if no value is entered.
	 *
	 * @return The current value as a Float value.
	 */
	Float getFloat();
}
