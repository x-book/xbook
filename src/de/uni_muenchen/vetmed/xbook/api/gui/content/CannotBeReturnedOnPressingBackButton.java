package de.uni_muenchen.vetmed.xbook.api.gui.content;

/**
 * An empty interface that is used to specify any abstract content elements that
 * they are not able to be called when clicking on the back button of a content.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface CannotBeReturnedOnPressingBackButton {
}
