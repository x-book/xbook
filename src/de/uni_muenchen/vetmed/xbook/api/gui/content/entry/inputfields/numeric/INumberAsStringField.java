package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface INumberAsStringField extends IInputElement {

	/**
	 * Set a new number as value to the "minimum" field.
	 *
	 * @param number The new minimum value as String. <code>null</code> for no value.
	 */
	void setNumber(String number);

	/**
	 * Get the current minimum value as a String value. Return
	 * <code>null</code> if no value is entered.
	 *
	 * @return The current minimum value as a String value.
	 */
	String getNumberAsString();
}
