package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import java.text.ParseException;

public interface ITwoDatesChooser extends IInputElement {

    /**
     * Set the first date as String in the format "yyyy-MM-dd" as the selected
     * date.
     *
     * @param date The date to set.
     * @throws ParseException if the date has an invalid format.
     */
    void setDate1(String date) throws ParseException;

    /**
     * Get the first selected date as a String in the format "yyyy-MM-dd".
     *
     * @return The first selected date.
     */
    String getDate1();

    /**
     * Set the second date as String in the format "yyyy-MM-dd" as the selected
     * date.
     *
     * @param date The date to set.
     * @throws ParseException if the date has an invalid format.
     */
    void setDate2(String date) throws ParseException;

    /**
     * Get the second selected date as a String in the format "yyyy-MM-dd".
     *
     * @return The second selected date.
     */
    String getDate2();
}
