package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.Unit;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.IButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.IEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboIdBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboIdSearchBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboTextBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboTextBoxWithProjectData;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IComboTextBoxWithThesaurus;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IHierarchicComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo.IMultiComboIdBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.ICheckBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IDateChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IIconButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IOpenURLButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IPlaceHolder;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IPlaceholderButton;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.ITimeChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IUserDisplayField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IYesNoField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IFloatField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IIntegerField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IIntegerSpinner;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IIntegerSpinnerWithAutoRaise;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IIntegerUnitField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.ILabeledFloatField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.ILabeledIntegerField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.IMinMaxFloatField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.INumberAsStringField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric.INumberAsStringUnitField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.IFileNameChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.IMultiTextField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.ITextArea;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text.ITextField;
import java.util.ArrayList;

import javax.swing.filechooser.FileNameExtensionFilter;

public abstract class AbstractInputFieldFactory {

    protected static AbstractInputFieldFactory factory;

    public static AbstractInputFieldFactory getFactory() {
        return factory;
    }

	/////////////////////////////////////////////////////////////////////////
    // TEXTS
    /////////////////////////////////////////////////////////////////////////
    public abstract ITextField createTextField(ColumnType columnType);

    public abstract ITextArea createTextArea(ColumnType columnType);

    public abstract IMultiTextField createMultiTextField(ColumnType columnType, String table);

    public abstract IFileNameChooser createFileNameChooser(ColumnType columnType, ArrayList<FileNameExtensionFilter> fileFilter);

	/////////////////////////////////////////////////////////////////////////
    // NUMBERS
    /////////////////////////////////////////////////////////////////////////
    public abstract IFloatField createFloatField(ColumnType columnType);

    public abstract IIntegerField createIntegerField(ColumnType columnType);

    public abstract IIntegerSpinner createIntegerSpinner(ColumnType columnType);

    public abstract IIntegerSpinnerWithAutoRaise createIntegerSpinnerWithAutoRaise(ColumnType columnType, String propertyName);

    public abstract IIntegerUnitField createIntegerUnitField(ColumnType columnType, ArrayList<Unit> units);

    public abstract ILabeledFloatField createLabeledFloatField(ColumnType columnType, String labelText);

    public abstract ILabeledIntegerField createLabeledIntegerField(ColumnType columnType, String labelText);

    public abstract IMinMaxFloatField createMinMaxFloatField(ColumnType columnTypeMin, ColumnType columnTypeMax, String title, String labelText);

    public abstract INumberAsStringField createNumberAsStringField(ColumnType columnType);

    public abstract INumberAsStringUnitField createNumberAsStringUnitField(ColumnType columnType, ArrayList<Unit> units);

    /////////////////////////////////////////////////////////////////////////
    // MISCELLANEOUS
    /////////////////////////////////////////////////////////////////////////
    public abstract IComboBox createComboBox(ColumnType columnType, ArrayList<String> values);

    public abstract IComboIdBox createComboIdBox(ColumnType columnType);

    public abstract IComboIdSearchBox createComboIdSearchBox(ColumnType columnType);

    public abstract IComboTextBox createComboTextBox(ColumnType columnType);

    public abstract IComboTextBoxWithThesaurus createComboTextBoxWithThesaurus(ColumnType columnType);

    public abstract IComboTextBoxWithProjectData createComboTextBoxWithProjectData(ColumnType columnTypeToSave, ColumnType columnTypeToLoad);

    public abstract IHierarchicComboBox createHierarchicComboBox(ColumnType columnType);

    public abstract IMultiComboIdBox createMultiComboIdBox(ColumnType columnType, String table);

    /////////////////////////////////////////////////////////////////////////
    // MISCELLANEOUS
    /////////////////////////////////////////////////////////////////////////
    public abstract IButton createButton(ColumnType columnType, IEntry entry);

    public abstract ICheckBox createCheckBox(ColumnType columnType);

    public abstract IDateChooser createDateChooser(ColumnType columnType);

    public abstract IIconButton createIconButton(ColumnType columnType, IEntry entry);

    public abstract IOpenURLButton createOpenURLButton(ColumnType columnType);

    public abstract IPlaceHolder createPlaceHolder(String title);

    public abstract IPlaceholderButton createPlaceHolderButton(String title, String buttonLabel);

    public abstract ITimeChooser createTimeChooser(ColumnType columnType);

    public abstract IUserDisplayField createUserDisplayField(ColumnType columnType);

    public abstract IYesNoField createYesNoField(ColumnType columnType);

}
