package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo;

public interface IComboIdSearchBox extends IComboIdBox {

    void setIsEditable(boolean isEditable);
    
}
