package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiFileChooserElement;

import java.util.List;

/**
 * Created by lohrer on 01.07.2015.
 */
public interface IMultiFileChooser extends IInputElement {

    boolean addElement(MultiFileChooserElement element);

    boolean addElements(List<MultiFileChooserElement> element);

    List<MultiFileChooserElement> getElementsFromList();

    MultiFileChooserElement getElementFromInput();

    void removeElement(MultiFileChooserElement elements);

    void removeElements(List<MultiFileChooserElement> elements);

    void removeElementAt(int index);
}
