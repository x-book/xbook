package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.text;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

import java.util.List;

/**
 * Created by lohrer on 01.07.2015.
 */
public interface IMultiTextField extends IInputElement {

	boolean addText(String text);

	boolean addText(List<String> text);

	List<String> getTextFromList();

	String getTextFromInput();

	void removeText(String text);

	void removeText(List<String> text);

	void removeText(int index);
}