package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.numeric;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface IMinMaxFloatField extends IInputElement {

	/**
	 * Set a new number as value to the "minimum" field.
	 *
	 * @param number The new minimum value as String. <code>null</code> for no value.
	 */
	void setMinimumFloat(Float number);

	/**
	 * Set a new number as value to the "maximum" field.
	 *
	 * @param number The new maximum value as String. <code>null</code> for no value.
	 */
	void setMaximumFloat(Float number);

	/**
	 * Set a new number as String as value to the "minimum" field.
	 *
	 * @param number The new minimum value as String. <code>null</code> for no value.
	 */
	void setMinimumFloat(String number);

	/**
	 * Set a new number as String as value to the "maximum" field.
	 *
	 * @param number The new maximum value as String. <code>null</code> for no value.
	 */
	void setMaximumFloat(String number);

	/**
	 * Get the current minimum value as a String value. Return
	 * <code>null</code> if no value is entered.
	 *
	 * @return The current minimum value as a String value.
	 */
	String getMinimumFloatAsString();

	/**
	 * Get the current maximum value as a String value. Return
	 * <code>null</code> if no value is entered.
	 *
	 * @return The current maximum value as a String value.
	 */
	String getMaximumFloatAsString();

	/**
	 * Get the current minimum value as a Float value. Return
	 * <code>null</code> if no value is entered.
	 *
	 * @return The current minimum value as a String value.
	 */
	Float getMinimumFloat();

	/**
	 * Get the current maximum value as a Float value. Return
	 * <code>null</code> if no value is entered.
	 *
	 * @return The current maximum value as a String value.
	 */
	Float getMaximumFloat();
}
