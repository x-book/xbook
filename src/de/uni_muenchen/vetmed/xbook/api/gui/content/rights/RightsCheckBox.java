package de.uni_muenchen.vetmed.xbook.api.gui.content.rights;

import de.uni_muenchen.vetmed.xbook.api.exception.InvalidSelectionException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.modernCheckbox.ModernCheckbox;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.security.auth.login.Configuration;
import java.util.logging.Logger;

/**
 * An extension of the ModernCheckbox class to be able to additional
 * hold an status whether the selected status has been changed or not.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class RightsCheckBox extends ModernCheckbox {
    /**
     * Holds the status if the value has changed, or not.
     */
    private boolean valueHasChanged = false;
    /**
     * Stores the original value, before the user has entered any changed.
     */
    private Selection originalValue;

    /**
     * Constructor.
     *
     * @param mode     The selected mode.
     */
    public RightsCheckBox(Mode mode) {
        this(mode, "");
    }

    /**
     * Constructor.
     *
     * @param mode     The selected mode.
     * @param text     The text of the label that is dislayed in the checkbox.
     */
    public RightsCheckBox(Mode mode, String text) {
        super(mode, text);
        originalValue = getDefaultSelection();
    }

    /**
     * Returns if the value has changed, or not.
     *
     * @return The status if the value has changed, or not.
     */
    public boolean hasValueChanged() {
        return valueHasChanged;
    }

    /**
     * Resets the valueHasChanged information, that means the current value is the new origin value.
     */
    private void resetValueHasChangedInformation() {
        valueHasChanged = false;
        originalValue = getCurrentSelection();
        LogFactory.getLog(RightsCheckBox.class).debug("Value of valueHasChanged in '" + getText() + "' was reset to '" + valueHasChanged + "'.");
    }

    /**
     * Updates the status if the value has changed, or not.
     * <p>
     * If the original value is identical to the current selected
     * value, the value has not changed. Remember to use
     * resetValueChangedInformation() when setting a new value, e.g.
     * on setSelected().
     */
    public void updateValueChangedInformation() {
        boolean originValue = valueHasChanged;
        valueHasChanged = getCurrentSelection() != originalValue;
        boolean newValue = valueHasChanged;
        LogFactory.getLog(RightsCheckBox.class).debug("Value of valueHasChanged in '" + getText() + "' was '" + originValue + "' and is now '" + newValue + "'.");
    }

    @Override
    public void setSelection(Selection selection) throws InvalidSelectionException {
        setSelection(selection, false);
    }

    /**
     * Sets the selection to a new value. Dependent on the parameter resetValueHasChangedStatus either the
     * valueHasChanged information is updated, or reseted.
     *
     * @param selection                  The new selection the checkbox should be set to.
     * @param resetValueHasChangedStatus If value is <code>true</code> the checkbox value is set to a "new origin"
     *                                   value. If value is set to <code>false</code> the origin value is unchanged
     *                                   and only the valueHasChanged status is updated.
     */
    public void setSelection(Selection selection, boolean resetValueHasChangedStatus) throws InvalidSelectionException {
        super.setSelection(selection);
        if (resetValueHasChangedStatus) {
            resetValueHasChangedInformation();
        } else {
            updateValueChangedInformation();
        }
    }

    @Override
    protected void actionOnChanged() {
        super.actionOnChanged();
        updateValueChangedInformation();
    }
}

