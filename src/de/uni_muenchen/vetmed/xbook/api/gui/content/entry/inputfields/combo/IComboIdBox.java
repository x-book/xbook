package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface IComboIdBox extends IInputElement {

    /**
     * Return the selected value of the combo box.
     *
     * @return The selected value.
     */
    String getSelectedValue();

    /**
     * Return the selected value of the combo box.
     *
     * @return The selected value.
     */
    String getSelectedId();

    /**
     * Set the selected value of the combo box.
     *
     * @param value The new selected value.
     */
    void setSelectedValue(String value);

    /**
     * Set the selected id of the combo box.
     *
     * @param id The new selected value.
     */
    void setSelectedId(String id);

    /**
     * Set the status whether to add an empty entry to the combo box or not.
     *
     * @param addEmptyEntry <code>true</code> to add an empty item,
     * <code>false</code> else.
     */
    void setAddEmptyEntry(boolean addEmptyEntry);
}
