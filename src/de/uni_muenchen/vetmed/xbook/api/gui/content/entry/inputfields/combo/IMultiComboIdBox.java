package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import java.util.List;

public interface IMultiComboIdBox extends IInputElement {

    void addToSelection(String id);

    void addToSelection(List<String> id);

    void removeAllSelectedItems();

    void setSelectedData(String id);

    void setSelectedData(List<String> id);

    List<String> getSelectedData();

    void setAddEmptyEntry(boolean addEmptyEntry);
}
