package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous;

import javax.swing.Icon;

public interface IIconButton extends de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.IButton {

    /**
     * Set the default icon to the button.
     */
    void setDefaultIcon();

    /**
     * Set a new icon to the button.
     *
     * @param icon The icon to display.
     */
    void setIcon(Icon icon);

    /**
     * Removes the icon from the button.
     */
    void removeIcon();
}
