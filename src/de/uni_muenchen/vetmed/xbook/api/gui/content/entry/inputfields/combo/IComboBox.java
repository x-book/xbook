package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.combo;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import java.util.Collection;

public interface IComboBox extends IInputElement {

	/**
	 * Return the selected value of the combo box.
	 *
	 * @return The selected value.
	 */
	String getSelectedValue();

	/**
	 * Set the selected value of the combo box.
	 *
	 * @param value The new selected value.
	 */
	void setSelectedValue(String value);

	/**
	 * Add an item to the combo box.
	 *
	 * @param items The item to add.
	 */
	void addItemToCombo(String items);

	/**
	 * Add the elements of a list to the combo box.
	 *
	 * @param items The items to add.
	 */
	void addItemsToCombo(Collection<String> items);

	/**
	 * Removes all items from the combo.
	 */
	void clearItemsFromCombo();

	/**
	 * Set the combobox editable (to enable inserting custom text) or not.
	 *
	 * @param b <code>true</code> to set it editable, <code>false</code> else.
	 */
	void setEditable(boolean b);
}
