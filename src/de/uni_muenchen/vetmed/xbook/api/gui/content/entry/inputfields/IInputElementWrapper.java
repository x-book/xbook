package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields;

import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import java.awt.Color;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface IInputElementWrapper {

    /**
     * Returns whether the remember value checkbox is selected or not.
     *
     * @return <code>true</code> if the remember value checkbox is selected,
     * <code>false</code> else.
     */
    boolean isRememberValueSelected();

    /**
     * Colorise the background of the input field.
     *
     * @param color The background color of the content.
     */
    void colorizeBackground(Color color);

    /**
     * Set a specific style that displays that there occured an error within the
     * field.
     */
    void setErrorStyle();

    /**
     * Set the default style.
     *
     * @param isFocused <code>true</code> to set the field to focued style.
     */
    void setDefaultStyle(boolean isFocused);

    /**
     * Set the remember value field disabled, so that is cannot be selected
     * anymore by clicking on it.
     */
    void setRememberValueFieldDisabled();

    /**
     * Set the hide button to invisible.
     */
    void hideHideButton();

    /**
     * Set the delete button to visible or invisible.
     * @param b The status whether to set the delete button to visible or not.
     */
    void setDeleteButtonVisible(boolean b);

    /**
     * Get the horizontal grid size. (Default: 1)
     *
     * @return The horizontal grid size.
     */
    int getGridX();

    /**
     * Get the vertical grid size. (Default: 1)
     *
     * @return The vertical grid size.
     */
    int getGridY();

    /**
     * Set the horizontal grid size.
     *
     * @param gridX The horizontal grid size,
     */
    void setGridX(int gridX);

    /**
     * Set the vertical grid size.
     *
     * @param gridY The vertical grid size,
     */
    void setGridY(int gridY);

    /**
     * Set the custom sidebar for this input element. <code>null</code> if no
     * custom sidebar is available..
     *
     * @param sidebar The custom sidebar of this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    void setSidebar(SidebarPanel sidebar);

    /**
     * Set the custom sidebar for this input element. <code>null</code> if no
     * custom sidebar is available..
     *
     * @param sidebarText The sidebar text of this input element.
     */
    void setSidebar(String sidebarText);

}
