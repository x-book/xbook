package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiDateElement;

import java.util.List;

/**
 *
 */
public interface IMultiDateChooser extends IInputElement {

    boolean addElement(MultiDateElement element);

    boolean addElements(List<MultiDateElement> element);

    List<MultiDateElement> getElementsFromList();

    MultiDateElement getElementFromInput();

    void removeElement(MultiDateElement elements);

    void removeElements(List<MultiDateElement> elements);

    void removeElementAt(int index);
}
