package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface IYesNoField extends IInputElement {

    /**
     * Enumeration of the possible states "Yes", "No" and "None".
     */
    enum State {

        YES("1"), NO("0"), NONE("-1");

        String id;

        /**
         * Constructor. Assigns the ID.
         *
         * @param id The assigned ID.
         */
        State(String id) {
            this.id = id;
        }

        /**
         * Get the ID of the state.
         *
         * @return The ID of the state.
         */
        public String getId() {
            return id;
        }

    }

    /**
     * Set the "Yes" field selected, deselect the "No" field.
     */
    void setSelectedYes();

    /**
     * Set the "No" field selected, deselect the "Yes" field.
     */
    void setSelectedNo();

    /**
     * Set the "Yes" and the "No" field deselected.
     */
    void setSelectedNone();

    /**
     * Set the field to a new state.
     *
     * @param state The state to set.
     */
    void setSelected(State state);

    /**
     * Set the field to a new state. "1" for Yes, "0" for No, "-1" for None.
     *
     * @param id The state to set. "1" for Yes, "0" for No, "-1" for None.
     */
    void setSelected(String id);

    /**
     * Returns the current state.
     *
     * @return The current state.
     */
    State getSelectedValue();

    /**
     * Returns the current state as ID: "1" for Yes, "0" for No, "-1" for None.
     *
     * @return The current state as ID: "1" for Yes, "0" for No, "-1" for None.
     */
    String getSelectedID();

    /**
     * Returns whether the "Yes" option is selected or not.
     *
     * @return <code>true</code> if the "Yes" option is selected,
     * <code>false</code> else.
     */
    boolean isSelectedYes();

    /**
     * Returns whether the "No" option is selected or not.
     *
     * @return <code>true</code> if the "No" option is selected,
     * <code>false</code> else.
     */
    boolean isSelectedNo();

    /**
     * Returns whether the "No" or the "Yes" option is selected or not.
     *
     * @return <code>true</code> if the "Yes" or "No" option is selected,
     * <code>false</code> if none of them is selected.
     */
    boolean isSelectedNone();

}
