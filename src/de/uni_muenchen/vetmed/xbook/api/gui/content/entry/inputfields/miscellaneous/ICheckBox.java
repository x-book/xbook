package de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous;

import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;

public interface ICheckBox extends IInputElement {

    /**
     * Set the checkbox selection to the given status.
     *
     * @param status The status whether the checkbox should be selected or not.
     */
    void setSelected(boolean status);

    /**
     * Return the selection status of the checkbox.
     *
     * @return The status whether the checkbox is selected or not.
     */
    boolean isSelected();

}
