package de.uni_muenchen.vetmed.xbook.api.gui.footer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Icon;

/**
 * A progress bar that can display a progress af anything from 0% to 100%.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractProgressBar extends JPanel {

    /**
     * Holds all icons ordered by value.
     */
    private final ArrayList<JLabel> boneLabels;
    /**
     * A label where the current value is displayed.
     */
    private final JLabel valueLabel;

    /**
     * Constructor
     */
    public AbstractProgressBar() {
        boneLabels = new ArrayList<>();

        setLayout(null);
        setSize(200, 32);
        setPreferredSize(new Dimension(200, 32));
        setOpaque(false);

        // create the label for the procentual value
        valueLabel = new JLabel();
        valueLabel.setOpaque(true);
        valueLabel.setBackground(Color.BLACK);
        valueLabel.setForeground(Color.WHITE);
        valueLabel.setSize(46, 16);
        valueLabel.setLocation(82, 8);
        valueLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(valueLabel);

        int xPos = 0;

        // create the left border
        add(getBorderElement(xPos));
        xPos += 10 + 1;

        // create the progress bar elements
        for (int i = 0; i < 20; i++) {
            JLabel bone = getInnerElement(xPos);
            add(bone);
            boneLabels.add(bone);
            xPos += 9 + 1;
        }

        // create the right border
        add(getBorderElement(xPos));
        xPos += 10 + 1;

        // set the size of the panel
        setSize(xPos, 32);
        setPreferredSize(new Dimension(xPos, 32));
    }

    /**
     * Creates a JLabel with the border element graphic of the progress bar.
     *
     * @param xPos The horizontal position of the element.
     * @return The final JLabel with the graphic.
     */
    private JLabel getBorderElement(int xPos) {
        JLabel panel = new JLabel();
        panel.setSize(10, 32);
        panel.setIcon(getBorderElement());
        panel.setBounds(0, 0, 10, 32);
        panel.add(new JLabel(""));
        panel.setLocation(xPos, 0);
        return panel;
    }

    /**
     * Creates a JLabel with a progress bar element graphic.
     *
     * @param xPos The horizontal position of the element.
     * @return The final JLabel with the graphic.
     */
    private JLabel getInnerElement(int xPos) {
        JLabel panel = new JLabel();
        panel.setSize(9, 28);
        panel.setIcon(getInnerBarElement());
        panel.setBounds(0, 0, 9, 28);
        panel.add(new JLabel(""));
        panel.setLocation(xPos, 2);
        return panel;
    }

    /**
     * Set a new (numeric) value to the progress bar.
     *
     * Valid inputs are 0.0 to 100.0 which represent procentual values. Negative
     * values are interpreted as 0.0, values bigger than 100 are interpreted as
     * 100.0
     *
     * @param value A numeric value that should be displayed.
     */
    public void setValue(double value) {
        if (value < 100.0) {
            for (JLabel bone : boneLabels) {
                bone.setIcon(getInnerDisabledBarElement());
            }
            if (value <= 0) {
                valueLabel.setText("0 %");
                return;
            }
        } else {
            for (JLabel bone : boneLabels) {
                bone.setIcon(getInnerBarElement());
            }
            valueLabel.setText("100 %");
            return;
        }

        double count = 0.0;
        try {
            count = (value / 5.0);
            valueLabel.setText((int) (count * 5) + " %");
            for (int i = 0; i <= count - 1; i++) {
                boneLabels.get(i).setIcon(getInnerBarElement());
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
    }

    protected abstract Icon getBorderElement();

    protected abstract Icon getInnerBarElement();

    protected abstract Icon getInnerDisabledBarElement();
}
