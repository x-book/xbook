package de.uni_muenchen.vetmed.xbook.api.gui.footer;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.RepeatBackgroundPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.Content;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.SettingView;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The basic main navigation object.
 * <p/>
 * It holds several static methods to set it up.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Footer extends JPanel {
    static private final Log LOGGER = LogFactory.getLog(Footer.class);
    /**
     * A state whether an item is currently clicked or not.
     */
    public static boolean anItemIsClicked = false;
    /**
     * The object itself. Necessary for static methods.
     */
    protected static Footer thisElement;
    /**
     * The standard footer object.
     */
    private static RepeatBackgroundPanel standardFooter;

    protected final Thread t1;

    // ========================================================================
    /**
     * The label where the current project is displayed.
     */
    protected JLabel userProjectLabel;
    /**
     * The progress bar object.
     */
    private AbstractProgressBar progressBar;
    /**
     * The working icon object.
     */
    private WorkingIcon workingIcon;
    /**
     * The current user that is displayed in the footer bar.
     */
    protected String currentUser = "";
    /**
     * The current project that is displayed in the footer bar.
     */
    protected ProjectDataSet currentProject = null;
    /**
     * The current connection status.
     */
    private Connection connectionStatus;
    /**
     * The label and button that displays the connection status.
     */
    private FooterButton connectionButton;
    private FooterButton timerButton;
    private FooterButton toggleSidebar;
    private FooterButton conflictedButton;
    protected JPanel leftButtons;

    private boolean isProgressBarVisible = false;
    protected IMainFrame mainFrame;

    private JPanel messageWrapper;
    private JLabel messageLabel;

    private JPanel messagePane;
    private JPanel standardPane;
    private JPanel currentPane;

    private Stack<TimedRunnable> threads = new Stack<>();

    /**
     * The different modes for the connection.
     */
    public enum Connection {

        /**
         * The user is disconnected from the server and is working offline.
         */
        DISCONNECTED,
        /**
         * The user is connected to the server and auto synchronisation is available.
         */
        CONNECTED_AUTO_SYNC,
        /**
         * The user is connected to the server and auto synchronisation is not available.
         */
        CONNECTED_MANUAL_SYNC,
        /**
         * The connection label if no status is available.
         */
        NONE
    }

    /**
     * Constructor
     * <p/>
     * Sets the basic layout of the navigation bar.
     */
    public Footer(IMainFrame mainFrame) {
        super();
        this.thisElement = this;
        this.mainFrame = mainFrame;

        this.standardFooter = createFooter();

        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(50, 50));
        setBackground(Colors.CONTENT_BACKGROUND);

        add(BorderLayout.CENTER, standardFooter);

        // Test settings
        updateProjectLabel(null);
        updateUserLabel("");

        // display connection button
        ApiControllerAccess c = mainFrame.getController();
        if (c instanceof AbstractSynchronisationController) {
            //only do status changes if we have the corect controller
            if (((AbstractSynchronisationController) c).isServerConnected()) {
                if (AbstractConfiguration.getBookBoolean(AbstractConfiguration.AUTOSYNC)) {
                    setConnectionStatus(Connection.CONNECTED_AUTO_SYNC);
                } else {
                    setConnectionStatus(Connection.CONNECTED_MANUAL_SYNC);
                }
            } else {
                setConnectionStatus(Connection.NONE);
            }
        }

        // display timed sync button
        updateTimedButton();

        t1 = new Thread(new Runnable() {

            @Override
            public void run() {

                while (true) {

                    try {
                        synchronized (t1) {
                            while (threads.empty()) {
                                displayStandardPane();
                                t1.wait();
                            }
                        }
                        TimedRunnable r = threads.pop();
                        SwingUtilities.invokeLater(r);

                        Thread.sleep(r.getTime());
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Footer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });
        t1.start();
    }

    private static interface TimedRunnable extends Runnable {

        /**
         * Returns how long the message box should be displayed [in ms].
         *
         * @return The length of display duration in ms.
         */
        public int getTime();
    }

    /**
     * Set a new project label.
     *
     * @param project
     */
    public static void updateProjectLabel(ProjectDataSet project) {
        if (thisElement != null) {
            thisElement.setProjectLabel(project);
        }
    }

    public static void updateTimedButton() {
        boolean b = XBookConfiguration.getBookBoolean(XBookConfiguration.PLANNED_SYNC);
        if (thisElement.timerButton != null) {
            thisElement.timerButton.setVisible(b);
            if (b) {
                thisElement.timerButton.setToolTipText(thisElement.getTimerButtonTooltipText());
            }
        }
    }

    /**
     * Set a new user label.
     *
     * @param userName The new user label.
     */
    public static void updateUserLabel(String userName) {
        if (thisElement != null) {
            thisElement.setUserLabel(userName);
        }
    }

    /**
     * Set a new connection status. The method edits the displayed status icon.
     *
     * @param c The new connection status.
     */
    public static void setConnectionStatus(Connection c) {
        if (thisElement != null && thisElement.standardFooter != null) {
            thisElement.connectionStatus = c;
            switch (c) {
                case CONNECTED_AUTO_SYNC:
                    thisElement.connectionButton.setImages(Images.FOOTER_CONNECTED_AUTO,
                            Images.FOOTER_CONNECTED_AUTO_HOVERED);
                    thisElement.connectionButton.setToolTipText("<html>" + Loc.get("CONNECTION_TO_SERVER_ESTABLISHED") + "<br />"
                            + Loc.get("AUTOMATICAL_SYNCHRONISATION_IS_ACTIVATED") + "<br /> "
                            + Loc.get("DATA_IS_SYNCHRONISING_IN_THE_BACKGROUND") + "</html>");
                    break;
                case CONNECTED_MANUAL_SYNC:
                    thisElement.connectionButton.setImages(Images.FOOTER_CONNECTED_MANUAL,
                            Images.FOOTER_CONNECTED_MANUAL_HOVERED);
                    thisElement.connectionButton.setToolTipText("<html>" + Loc.get("CONNECTION_TO_SERVER_ESTABLISHED") + "<br />"
                            + Loc.get("AUTOMATICAL_SYNCHRONISATION_IS_NOT_ACTIVATED") + "<br /> "
                            + Loc.get("REMEMBER_TO_SYNCHRONISE_DATA_MANUALLY") + "</html>");
                    break;
                case DISCONNECTED:
                    thisElement.connectionButton.setImages(Images.FOOTER_DISCONNECTED,
                            Images.FOOTER_DISCONNECTED_HOVERED);
                    thisElement.connectionButton.setToolTipText("<html>" + Loc.get("NO_CONNECTION_TO_SERVER") + "<br />"
                            + Loc.get("CLICK_HERE_TO_RECONNECT") + "</html>");
                    break;
                case NONE:
                    thisElement.connectionButton.setImages(Images.FOOTER_PLACEHOLDER,
                            Images.FOOTER_PLACEHOLDER_HOVERED);
                    break;
            }
        }
    }

    /**
     * Sets the connectivity button to auto sync or manual sync. If the application is not connected to the server it
     * automaticaly displays the "disconnected" icon.
     *
     * @param bool <code>true</code> for auto-sync, <code>false</code> for
     *             manual sync.
     */
    public static void setAutoSync(boolean bool) {
        if (thisElement != null && thisElement.standardFooter != null && thisElement.mainFrame.getController() instanceof AbstractSynchronisationController) {
            if (((AbstractSynchronisationController) thisElement.mainFrame.getController()).isServerConnected()) {
                if (bool) {
                    setConnectionStatus(Connection.CONNECTED_AUTO_SYNC);
                } else {
                    setConnectionStatus(Connection.CONNECTED_MANUAL_SYNC);
                }
            } else {
                setConnectionStatus(Connection.DISCONNECTED);
            }
        }
    }

    /**
     * Display a specific text to the message box in the footer.
     *
     * @param text       The text to display.
     * @param background The background color of the message box.
     * @param textColor  The text color of the message box.
     * @param opaque     Is the background opaque or not.
     */
    public void displayText(final String text, final Color background, final Color textColor, final boolean opaque) {
        displayText(text, background, textColor, opaque, 3000);
    }

    /**
     * Display a specific text to the message box in the footer.
     *
     * @param text       The text to display.
     * @param background The background color of the message box.
     * @param textColor  The text color of the message box.
     * @param opaque     Is the background opaque or not.
     * @param time       Duration [in ms] how long the message box should be displayed.
     */
    public void displayText(final String text, final Color background, final Color textColor, final boolean opaque,
                            final int time) {
        final int ttime = 3000;
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                synchronized (t1) {
                    TimedRunnable r = new TimedRunnable() {
                        @Override
                        public void run() {
                            displayMessagePane(text, background, textColor, opaque);
                        }

                        @Override
                        public int getTime() {
                            return ttime;
                        }

                    };
                    threads.add(r);
                    t1.notifyAll();
                }
            }
        });
    }

    /**
     * Display a specific text to the message box in the footer.
     *
     * @param text       The text to display.
     * @param background The background color of the message box.
     * @param textColor  The text color of the message box.
     * @param opaque     Is the background opaque or not.
     */
    public void forceText(final String text, final Color background, final Color textColor, final boolean opaque) {
        forceText(text, background, textColor, opaque, 3000);
    }

    /**
     * Display a specific text to the message box in the footer.
     *
     * @param text       The text to display.
     * @param background The background color of the message box.
     * @param textColor  The text color of the message box.
     * @param opaque     Is the background opaque or not.
     * @param time       Duration [in ms] how long the message box should be displayed.
     */
    public void forceText(final String text, final Color background, final Color textColor, final boolean opaque,
                          final int time) {
        synchronized (t1) {
            threads.removeAllElements();
        }
        displayText(text, background, textColor, opaque, time);
    }

    /**
     * Display a warning text to the message box in the footer.
     *
     * @param text The text to display.
     */
    public static void displayWarning(String text) {
        if (thisElement != null) {
            thisElement.displayText(text, Colors.HIGHLIGHT_YELLOW, Color.BLACK, true);
        }
    }

    /**
     * Display a warning text to the message box in the footer.
     *
     * @param text The text to display.
     * @param time Duration [in ms] how long the message box should be displayed.
     */
    public static void displayWarning(String text, int time) {
        thisElement.displayText(text, Colors.HIGHLIGHT_YELLOW, Color.BLACK, true, time);
    }

    /**
     * Display a warning text to the message box in the footer.
     *
     * @param text The text to display.
     */
    public static void forceWarning(String text) {
        thisElement.forceText(text, Colors.HIGHLIGHT_YELLOW, Color.BLACK, true);
    }

    /**
     * Display a warning text to the message box in the footer.
     *
     * @param text The text to display.
     * @param time Duration [in ms] how long the message box should be displayed.
     */
    public static void forceWarning(String text, int time) {
        thisElement.forceText(text, Colors.HIGHLIGHT_YELLOW, Color.BLACK, true, time);
    }

    /**
     * Display an error text to the message box in the footer.
     *
     * @param text The text to display.
     */
    public static void displayError(String text) {
        thisElement.displayText(text, Colors.HIGHLIGHT_RED, Color.WHITE, true);
    }

    /**
     * Display an error text to the message box in the footer.
     *
     * @param text The text to display.
     * @param time Duration [in ms] how long the message box should be displayed.
     */
    public static void displayError(String text, int time) {
        thisElement.displayText(text, Colors.HIGHLIGHT_RED, Color.WHITE, true, time);
    }

    /**
     * Display a confirmation text to the message box in the footer.
     *
     * @param text The text to display.
     */
    public static void displayConfirmation(String text) {
        thisElement.displayText(text, Colors.HIGHLIGHT_GREEN, Color.WHITE, true);
    }

    /**
     * Display a confirmation text to the message box in the footer.
     *
     * @param text The text to display.
     * @param time Duration [in ms] how long the message box should be displayed.
     */
    public static void displayConfirmation(String text, int time) {
        thisElement.displayText(text, Colors.HIGHLIGHT_GREEN, Color.WHITE, true, time);
    }

    /**
     * Display a text without background to the message box in the footer.
     *
     * @param text The text to display.
     */
    public static void displayMessage(String text) {
        thisElement.displayText(text, Color.BLACK, Color.WHITE, false);
    }

    /**
     * Display a text without background to the message box in the footer.
     *
     * @param text The text to display.
     * @param time Duration [in ms] how long the message box should be displayed.
     */
    public static void displayMessage(String text, int time) {
        thisElement.displayText(text, Color.BLACK, Color.WHITE, false, time);
    }

    static ArrayList<String> starting = new ArrayList<>();
    static ArrayList<String> stopping = new ArrayList<>();

    /**
     * Display the working icon to the standard footer and run the animation.
     */
    public static void startWorking() {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        starting.add(elements[2].getFileName() + elements[2].getMethodName());
        if (thisElement != null) {
            thisElement.getWorkingIcons().startWorking();
        }
    }

    /**
     * Hide the working icon from the standard footer.
     */
    public static void stopWorking() {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        if (!starting.remove(elements[2].getFileName() + elements[2].getMethodName())) {
            stopping.add(elements[2].getFileName() + elements[2].getMethodName());
        }
        if (thisElement != null) {
            thisElement.getWorkingIcons().stopWorking();
        }
    }

    /**
     * Show the progress bar in the standard footer.
     */
    public static void showProgressBar() {
        if (thisElement != null) {
            thisElement.progressBar.setValue(0.0);
            thisElement.progressBar.setVisible(true);
            thisElement.workingIcon.startWorking();
            thisElement.isProgressBarVisible = true;
        }
    }

    /**
     * Hide the progress bar from the standard footer.
     */
    public static void hideProgressBar() {
        if (thisElement != null) {
            thisElement.progressBar.setVisible(false);
            thisElement.workingIcon.stopWorking();
            thisElement.isProgressBarVisible = false;
        }
    }

    /**
     * Set a new value to the progress bar. Values can be between 0 and 100
     */
    public static void setProgressBarValue(double value) {
        if (thisElement != null) {
            thisElement.progressBar.setValue(value);
        }
    }

    public static boolean isProgressBarVisible() {
        if (thisElement != null) {
            return thisElement.isProgressBarVisible;
        } else {
            return false;
        }
    }

    public static void updateToggleSidebarButtonImages() {
        AbstractContent content = Content.getCurrentContent();
        if (content == null) {
            return;
        }
        if (Content.getCurrentContent().getSideBar() == null) {
            thisElement.toggleSidebar.setImages(Images.FOOTER_DISABLEDSIDEBAR, Images.FOOTER_DISABLEDSIDEBAR_HOVERED);
            thisElement.toggleSidebar.setToolTipText(Loc.get("SIDEBAR_NOT_AVAILABLE_HERE"));
        } else if (content.forceSidebar()) {
            thisElement.toggleSidebar.setImages(Images.FOOTER_DISABLEDSIDEBAR, Images.FOOTER_DISABLEDSIDEBAR_HOVERED);
            thisElement.toggleSidebar.setToolTipText(Loc.get("SIDEBAR_CANNOT_BE_HIDDEN"));
        } else if (SettingView.getSidebarVisibilityProperty()) {
            thisElement.toggleSidebar.setImages(Images.FOOTER_HIDESIDEBAR, Images.FOOTER_HIDESIDEBAR_HOVERED);
            thisElement.toggleSidebar.setToolTipText(Loc.get("COLLAPSE_SIDEBAR"));
        } else {
            thisElement.toggleSidebar.setImages(Images.FOOTER_SHOWSIDEBAR, Images.FOOTER_SHOWSIDEBAR_HOVERED);
            thisElement.toggleSidebar.setToolTipText(Loc.get("SHOW_SIDEBAR"));
        }
    }

    /**
     * Sets the conflict button visible.
     */
    public static void showConflictButton() {
        thisElement.conflictedButton.setVisible(true);
    }

    /**
     * Hides the conflict button visible.
     */
    public static void hideConflictButton() {
        thisElement.conflictedButton.setVisible(false);
    }

    public static void setConflictedButtonVisible(boolean isConflicted) {
        if (AbstractConfiguration.MODE == AbstractConfiguration.Mode.DEVELOPMENT) {
            // isConflicted = true;
        }
        thisElement.conflictedButton.setVisible(isConflicted);
    }

    private RepeatBackgroundPanel createFooter() {
        RepeatBackgroundPanel f = new RepeatBackgroundPanel(Images.NAVIGATION_BACKGROUND);

        workingIcon = new WorkingIcon(mainFrame);

        f.setLayout(new BorderLayout());
        f.setBackground(Colors.CONTENT_BACKGROUND);

        ///////////////////////////////////////////////
        // left elements
        ///////////////////////////////////////////////
        leftButtons = new JPanel(new FlowLayout());
        leftButtons.setOpaque(false);
        leftButtons.setBorder(new EmptyBorder(4, 0, 0, 0));

        if (mainFrame.getController() instanceof AbstractSynchronisationController) {

            conflictedButton = new FooterButton(Images.FOOTER_CONFLICTED, Images.FOOTER_CONFLICTED_HOVERED);
            conflictedButton.setBorder(new EmptyBorder(0, 0, 0, 6));
            conflictedButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    mainFrame.displayConflictScreen();
                }
            });
            conflictedButton.setVisible(false);
            leftButtons.add(conflictedButton);

            connectionButton = new FooterButton(Images.FOOTER_DISCONNECTED, Images.FOOTER_DISCONNECTED_HOVERED);
            connectionButton.setBorder(new EmptyBorder(0, 0, 0, 6));
            connectionButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    switch (connectionStatus) {
                        case CONNECTED_AUTO_SYNC:
                            return;
                        case CONNECTED_MANUAL_SYNC:
                            return;
                        case DISCONNECTED:
                            ((AbstractSynchronisationController) mainFrame.getController()).update(null,
                                    ApiControllerAccess.Event.RECONNECT);
                            return;
                        case NONE:
                            return;
                    }
                }
            });
            leftButtons.add(connectionButton);

            addElementsBetweenTimerAndConnectionButton();

            timerButton = new FooterButton(Images.FOOTER_TIMER, Images.FOOTER_TIMER_HOVERED);
            timerButton.setBorder(new EmptyBorder(0, 0, 0, 6));
            timerButton.setToolTipText(getTimerButtonTooltipText());
            leftButtons.add(timerButton);
        }
        f.add(BorderLayout.WEST, leftButtons);

        ///////////////////////////////////////////////
        // Progress bar elements
        ///////////////////////////////////////////////
        standardPane = new JPanel(new BorderLayout());
        standardPane.setBackground(Color.red);
        standardPane.setBorder(new EmptyBorder(4, 0, 0, 0));
        standardPane.setOpaque(false);

        userProjectLabel = new JLabel();
        updateUserProjectInformation();
        userProjectLabel.setForeground(Color.WHITE);
        userProjectLabel.setBorder(new EmptyBorder(0, 0, 4, 0));
        standardPane.add(BorderLayout.WEST, userProjectLabel);

        JPanel progressBarWrapper = new JPanel();
        progressBarWrapper.setOpaque(false);
        progressBar = mainFrame.getProgressBar();
        progressBarWrapper.add(progressBar);
        standardPane.add(BorderLayout.CENTER, progressBarWrapper);

        showProgressBar(); // necessary to update working icon counter
        hideProgressBar();
        f.add(BorderLayout.CENTER, currentPane = standardPane);

        ///////////////////////////////////////////////
        // right elements
        ///////////////////////////////////////////////
        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.setOpaque(false);
        buttonPanel.setBorder(new EmptyBorder(4, 0, 0, 0));

        // working icon
        buttonPanel.add(workingIcon);

        // spacer
        JLabel spacer1 = new JLabel();
        spacer1.setPreferredSize(new Dimension(10, 1));
        spacer1.setOpaque(false);
        buttonPanel.add(spacer1);

        // temporary button for testing something
        if (AbstractConfiguration.MODE == AbstractConfiguration.Mode.DEVELOPMENT) {
            FooterButton devSomething = new FooterButton(Images.FOOTER_PLACEHOLDER, Images.FOOTER_PLACEHOLDER_HOVERED);
            devSomething.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                }
            });
            buttonPanel.add(devSomething);

            FooterButton devSomething2 = new FooterButton(Images.FOOTER_PLACEHOLDER, Images.FOOTER_PLACEHOLDER_HOVERED);
            devSomething2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    test3();
                }
            });
            buttonPanel.add(devSomething2);
        }

        // toggle Sidebar button
        toggleSidebar = new FooterButton(Images.FOOTER_HIDESIDEBAR, Images.FOOTER_HIDESIDEBAR_HOVERED);
        updateToggleSidebarButtonImages();
        toggleSidebar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!Content.getCurrentContent().forceSidebar()) {
                    SettingView.toggleSidebarVisibilityProperty();
                }
            }
        });
        buttonPanel.add(toggleSidebar);

        f.add(BorderLayout.EAST, buttonPanel);

        return f;
    }

    protected void addElementsBetweenTimerAndConnectionButton() {
    }

    private String getTimerButtonTooltipText() {
        String s = "<html>" + Loc.get("PLANNED_SYNC_PLANNED_AT") + ":<br><b>";

        String time = "";
        int hr = XBookConfiguration.getBookInt(XBookConfiguration.PLANNED_SYNC_HOUR);
        if (hr < 10) {
            time += "0";
        }
        time += hr + ":";
        int min = XBookConfiguration.getBookInt(XBookConfiguration.PLANNED_SYNC_MINUTE);
        if (min < 10) {
            time += "0";
        }
        time += min;

        s += time + "</b><br>(";
        if (XBookConfiguration.getBookBoolean(XBookConfiguration.PLANNED_SYNC_SYNC_ALL)) {
            s += Loc.get("ALL_PROJECTS");
        } else {
            s += Loc.get("ONLY_LOCAL_PROJECTS");
        }
        s += ")</html>";
        return s;
    }

    /**
     * Set a new user label.
     *
     * @param username The new user label.
     */
    public void setUserLabel(String username) {
        currentUser = username;
        updateUserProjectInformation();
    }

    /**
     * Set a new project label.
     *
     * @param projectname The new project label.
     */
    public void setProjectLabel(ProjectDataSet projectname) {
        currentProject = projectname;
        updateUserProjectInformation();
    }

    /**
     * Updates the label where the username and the project name is displayed.
     */
    protected void updateUserProjectInformation() {
        String text = "<html>";
        if (!currentUser.isEmpty()) {
            text += "<b>" + Loc.get("USER") + ":</b> " + currentUser;
        }
        text += "<br />";
        if (currentProject != null) {
            text += "<b>" + Loc.get("PROJECT") + ":</b> " + currentProject;
        }
        text += "</html>";
        userProjectLabel.setText(text);
    }

    /**
     * Get the working icon object.
     *
     * @return The working icon object.
     */
    public WorkingIcon getWorkingIcons() {
        return workingIcon;
    }

    boolean b = true;

    private void test3() {
        if (b) {
            showProgressBar();
        } else {
            hideProgressBar();
        }
        b = !b;
    }

    public void displayMessagePane(String text, Color backgroundColor, Color textColor, boolean opaque) {
        if (messagePane == null) {
            messagePane = new JPanel(new BorderLayout());
            messagePane.setOpaque(false);

            messageWrapper = new JPanel(new BorderLayout());

            messageLabel = new JLabel();
            messageLabel.setHorizontalAlignment(SwingConstants.CENTER);
            messageLabel.setVerticalAlignment(SwingConstants.CENTER);
            messageLabel.setOpaque(false);

            messageWrapper.add(BorderLayout.CENTER, messageLabel);

            JComponent c = ComponentHelper.wrapComponent(messageWrapper, 7, 50, 7,
                    50 + Images.FOOTER_LOADING_TRANSPARENT.getIconWidth());
            c.setOpaque(false);
            messagePane.add(BorderLayout.CENTER, c);
        }
        LOGGER.info(text);

        messageWrapper.setBackground(backgroundColor);
        messageWrapper.setOpaque(opaque);
        messageLabel.setText("<html>" + text + "</html>");
        messageLabel.setForeground(textColor);

        if (currentPane != null) {
            standardFooter.remove(currentPane);
        }
        standardFooter.add(BorderLayout.CENTER, currentPane = messagePane);

        repaint();
        revalidate();
    }

    private void displayStandardPane() {
        if (currentPane != null) {
            standardFooter.remove(currentPane);
        }
        standardFooter.add(BorderLayout.CENTER, currentPane = standardPane);

        repaint();
        revalidate();
    }
}
