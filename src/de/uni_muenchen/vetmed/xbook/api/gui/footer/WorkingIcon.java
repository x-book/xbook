package de.uni_muenchen.vetmed.xbook.api.gui.footer;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.exception.InvalidNumberOfRunningThreadsException;
import de.uni_muenchen.vetmed.xbook.api.gui.IMainFrame;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;

import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.JLabel;

/**
 * An icon that is rotating. Can be used to display that the application is
 * working, loading, or whatever.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class WorkingIcon extends JLabel {

    /**
     * An array that holds all displayed icons in sorted order.
     */
    private ArrayList<Icon> icons = new ArrayList<>();
    /**
     * The thread which is managing the icons.
     */
    private WorkingThread thread;
    /**
     * Necessary for the inner class.
     */
    private WorkingIcon thisElement;

    /**
     * Constructor
     *
     * @param mainFrame
     */
    public WorkingIcon(IMainFrame mainFrame) {
        this.thisElement = this;

        // add all icons in sorted order to the array list
        icons = mainFrame.getWorkingIcons();

        setSize(32, 32);
        setIcon(icons.get(0));

        thread = new WorkingThread();
        thread.start();
    }

    /**
     * Starts the thread working, so the icons are rotating.
     */
    public void startWorking() {
        thread.startWorking();
    }

    /**
     * Starts the thread working, so the icons will stop rotating.
     */
    public void stopWorking() {
        thread.stopWorking();
    }

    /**
     * A thread that handles the icons.
     */
    private class WorkingThread extends Thread {

        /**
         * The state if the thread is running or not.
         */
        int countRunningThreads = 0;

        int counter = 0;

        @Override
        public void run() {
            try {

                while (true) {
                    if (countRunningThreads <= 0) {
                        synchronized (this) {
                            // if the thread is not running hide the panel and wait until the thread is continued.
                            // thisElement.setVisible(false);
                            setIcon(Images.FOOTER_LOADING_TRANSPARENT);
                            wait();
                            // thisElement.setVisible(true);
                        }
                    }
                    // change the icon and sleep a while
                    counter++;
                    setIcon(icons.get((counter + 1) % icons.size()));
                    Thread.sleep(100);
                }

            } catch (InterruptedException ignored) {
            }
        }

        /**
         * Start rotating the icons.
         */
        public synchronized void startWorking() {
            countRunningThreads++;
            notify();
        }

        /**
         * Stop rotating the icons.
         */
        public synchronized void stopWorking() {
            if (countRunningThreads > 0) {
                countRunningThreads--;
            } else {
                try {
                    if (XBookConfiguration.MODE == AbstractConfiguration.Mode.DEVELOPMENT) {
                        throw new InvalidNumberOfRunningThreadsException("Removed one thread from Working Icon Thrad though no thread is running.");
                    }
                } catch (InvalidNumberOfRunningThreadsException e) {
                    // do nothing, only print stack trace to lcg
                }
                countRunningThreads = 0;
            }
        }
    }

}
