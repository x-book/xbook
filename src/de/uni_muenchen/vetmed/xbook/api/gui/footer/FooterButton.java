package de.uni_muenchen.vetmed.xbook.api.gui.footer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.JLabel;

/**
 * A single button that can be used in the footer bar.
 * 
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class FooterButton extends JLabel implements MouseListener {

    /**
     * The standard image for the background.
     */
    private Icon image;
    /**
     * The hover image for the background.
     */
    private Icon imageHovered;
    /**
     * Necessary for the action listeners.
     */
    private ArrayList<ActionListener> list = new ArrayList<>();
    /**
     * A state whether an item is currently clicked or not.
     */
    public static boolean itemIsClicked = false;
    
    /**
     * Constructor 
     * 
     * @param image The standard image for the background.
     * @param imageHovered The hover image for the background.
     */
    public FooterButton(Icon image, Icon imageHovered) {
        setImages(image, imageHovered);
        
        addMouseListener(this);       
        setIcon(image);
        setSize(32, 32);
    }
    
    @Deprecated
    public void mouseClicked(MouseEvent ignored) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        itemIsClicked = true;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (itemIsClicked) {
            for (ActionListener actionListener : list) {
                actionListener.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_FIRST, "blubb"));
            }
        }
        itemIsClicked = false;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        setIcon(imageHovered);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        setIcon(image);
        itemIsClicked = false;
    }
    
    /**
     * Set new images to the button.
     * 
     * @param image The standard image for the background.
     * @param imageHovered The hover image for the background.
     */
    public void setImages(Icon image, Icon imageHovered) {
        this.image = image;
        this.imageHovered = imageHovered;
        setIcon(image);
    }
    
    /**
     * Add a new action listener to the button that is run when clicking on the button.
     * 
     * @param al The action listener.
     */
    public void addActionListener(ActionListener al) {
        list.add(al);
    }
    
}
