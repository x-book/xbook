package de.uni_muenchen.vetmed.xbook.api.gui.documentFilter;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class TimeDocumentFilter extends IntegerDocumentFilter {

    @Override
    protected boolean test(String text) {
        try {
            if (text.isEmpty()) {
                return true;
            }
            Integer.parseInt(text.replace(":", ""));
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    protected String getErrorMessage() {
        return Loc.get("NO_VALID_INPUT_ONLY_TIME_VALUES");
    }

}
