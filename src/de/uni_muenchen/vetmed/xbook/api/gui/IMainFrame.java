package de.uni_muenchen.vetmed.xbook.api.gui;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.ISynchronisationManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Group;
import de.uni_muenchen.vetmed.xbook.api.gui.content.AbstractContent;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.IEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.AbstractProgressBar;
import de.uni_muenchen.vetmed.xbook.api.gui.navigation.INavigation;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.Synchronisation;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.synchronisation.filter.SyncFilter;

import javax.swing.*;
import java.util.ArrayList;

/**
 * @author Johannes Lohrer<lohrer@dbs.ifi.lmu.de>
 */
public interface IMainFrame {

    /**
     * Display the auto synchronisation message screen.
     *
     * @param previousContent
     * @param loadedEntryKey
     */
    void displayAutoSynchronisationMessageScreen(AbstractContent previousContent, Key loadedEntryKey);

    /**
     * Display the synchronisation conflict screen.
     */
    void displayConflictScreen();

//    /**
//     * Display the analysis panel screen.
//     */
//
//    void displayTestFxPanelScreen3();

    /**
     * Display the development panel screen.
     */
    void displayDevPanelScreen();

    /**
     * Display the export screen.
     */
    void displayExportScreen();

    /**
     * Display the project group rights screen.
     */
    void displayGroupManagementScreen();

    /**
     * Display the importer screen.
     */
    void displayImportScreen();

    /**
     * Display the multi edit entry screen for the input unit.
     */
    void displayMultiEditEntryScreenInputUnit();

    /**
     * Display the entry screen for the input unit.
     *
     * @param isNewEntry           Defines if the new entry mode should be load or the
     *                             edit mode.
     * @param ignoreRememberValues Defines if the remember values should be
     *                             ignored, or not.
     */
    void displayEntryScreenInputUnit(boolean isNewEntry, boolean ignoreRememberValues);

    /**
     * Display the list entries screen.
     */
    void displayListingScreen();

    /**
     * Display the list entries screen.
     *
     * @param manager The manager of which the entries should be listed.
     */
    void displayListingScreen(BaseEntryManager manager);

    /* ********************************************************************** */
    /*                      DISPLAY SPECIFIC CONTENT PANELS
     /* ********************************************************************** */

    /**
     * Display the login screen.
     */
    void displayLoginScreen();

    /**
     * Display the project overview screen.
     */
    void displayNewProjectScreen();

    /**
     * Display search project screen.
     */
    void displayProjectSearchScreen();

    /**
     * Display search server screen.
     */
    void displayServerSearchScreen();

    /**
     * Display barcode screen.
     */
    void displayBarcodeSearchScreen();

    /**
     * Display the project edit screen.
     *
     * @param project
     */
    void displayProjectEditScreen(ProjectDataSet project);

    /**
     * Display the project group rights screen.
     */
    void displayProjectGroupRightsScreen();

    /**
     * Display the project overview screen.
     */
    void displayProjectOverviewScreen();

    void displayProjectUserRightsAddGroupScreen();

    void displayProjectUserRightsAddUserScreen();

    void displayProjectUserRightsCreateGroupScreen();

    /**
     * Display the project user rights screen.
     */
    void displayProjectUserRightsScreen();

    /**
     * Display the register screen.
     */
    void displayRegisterScreen();

    /**
     * Display the settings screen.
     */
    void displaySettingsScreen();

    void displaySplashScreen();

    /**
     * Display the synchronisation screen.
     */
    void displaySynchronisationScreen();

    void displaySynchronisationFilterScreen(Synchronisation syncPanel);

    public SyncFilter getSynchronisationFilterScreen(Synchronisation syncPanel);

    int getContentSize();

    ApiControllerAccess getController();

    ArrayList<Icon> getWorkingIcons();

    AbstractProgressBar getProgressBar();

    Iterable<? extends IEntry> getAllAvailableEntryPanels();

    void displaySolveConflictScreen(DataSetOld get, DataSetOld dataSetOld, ISynchronisationManager syncInterface, ManagerType managerType);

    void displayGroupManagementScreen(Group group);

    void updateNavigation();

    void updateNavigation(INavigation.Mode mode);

    void displayProjectListingScreen();

    void displayEntryForManager(AbstractBaseEntryManager entryManager, EntryDataSet data, boolean isEditMode);

    AbstractEntry getEntryForManager(AbstractBaseEntryManager entryManager);
}
