package de.uni_muenchen.vetmed.xbook.api.gui.stylesheet;


import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.BUTTON_PANEL_BUTTON_CENTER;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.BUTTON_PANEL_BUTTON_LEFT;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.BUTTON_PANEL_BUTTON_RIGHT;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.BUTTON_PANEL_BUTTON_HOVERED_CENTER;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.BUTTON_PANEL_BUTTON_HOVERED_LEFT;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.BUTTON_PANEL_BUTTON_HOVERED_RIGHT;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.SEP;
import javax.swing.*;

/**
 * A class holding the paths of all icons which are used in the navigation in
 * the header of the application.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ImagesDB extends Images {

    //////////////////////
    // APPLICATION FAVICON
    //////////////////////
    public final static ImageIcon FAVICON_16 = createImageIcon("favicon" + SEP + "16.png");
    public final static ImageIcon FAVICON_24 = createImageIcon("favicon" + SEP + "24.png");
    public final static ImageIcon FAVICON_32 = createImageIcon("favicon" + SEP + "32.png");
    public final static ImageIcon FAVICON_48 = createImageIcon("favicon" + SEP + "48.png");
    public final static ImageIcon FAVICON_64 = createImageIcon("favicon" + SEP + "64.png");
    public final static ImageIcon FAVICON_96 = createImageIcon("favicon" + SEP + "96.png");
    public final static ImageIcon FAVICON_128 = createImageIcon("favicon" + SEP + "128.png");
    ///////////////////////////
    // FOOTER BUTTON GRAPHICS
    ///////////////////////////
    public final static ImageIcon FOOTER_LOADING_ICON_01 = Images.FOOTER_LOADING_PACMAN_01;
    public final static ImageIcon FOOTER_LOADING_ICON_02 = Images.FOOTER_LOADING_PACMAN_02;
    public final static ImageIcon FOOTER_LOADING_ICON_03 = Images.FOOTER_LOADING_PACMAN_03;
    public final static ImageIcon FOOTER_LOADING_ICON_04 = Images.FOOTER_LOADING_PACMAN_03;
    public final static ImageIcon FOOTER_LOADING_ICON_05 = Images.FOOTER_LOADING_PACMAN_02;
    public final static ImageIcon FOOTER_LOADING_ICON_06 = Images.FOOTER_LOADING_PACMAN_01;
    public final static ImageIcon FOOTER_LOADING_ICON_07 = Images.FOOTER_LOADING_PACMAN_01;
    public final static ImageIcon FOOTER_LOADING_ICON_08 = Images.FOOTER_LOADING_PACMAN_02;
    public final static ImageIcon FOOTER_LOADING_ICON_09 = Images.FOOTER_LOADING_PACMAN_03;
    public final static ImageIcon FOOTER_LOADING_ICON_10 = Images.FOOTER_LOADING_PACMAN_03;
    public final static ImageIcon FOOTER_LOADING_ICON_11 = Images.FOOTER_LOADING_PACMAN_02;
    public final static ImageIcon FOOTER_LOADING_ICON_12 = Images.FOOTER_LOADING_PACMAN_01;
	
    public final static ImageIcon FOOTER_PROGRESS_ICON_BIG = Images.FOOTER_PROGRESS_BONE_BIG;
    public final static ImageIcon FOOTER_PROGRESS_ICON_SMALL = Images.FOOTER_PROGRESS_GUYBRUSH_SMALL;
    public final static ImageIcon FOOTER_PROGRESS_ICON_SMALL_GRAY = Images.FOOTER_PROGRESS_GUYBRUSH_SMALL_GRAY;
    ///////////////////////////
    // NAVIGATION GRAPHICS
    ///////////////////////////
    public final static ImageIcon NAVIGATION_APPLICATION_LOGO_DEMOBOOK = createImageIcon("navigation" + SEP + "logo.png");
    ////////////////
    // SPLASH SCREEN
    ////////////////
    public final static ImageIcon SPLASH_BACKGROUND = createImageIcon("general" + SEP + "splash_screen_background.png");

    ////////////////////////
    // BUTTON PANEL GRAPHICS
    ////////////////////////

    static {
        BUTTON_PANEL_BUTTON_LEFT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_border.png");
        BUTTON_PANEL_BUTTON_CENTER = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_center.png");
        BUTTON_PANEL_BUTTON_RIGHT = createMirroredImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_border.png");
        BUTTON_PANEL_BUTTON_HOVERED_LEFT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_hover_border.png");
        BUTTON_PANEL_BUTTON_HOVERED_CENTER = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_hover_center.png");
        BUTTON_PANEL_BUTTON_HOVERED_RIGHT = createMirroredImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_hover_border.png");
    }

    public final static String PATH = "assets/imagesDB/";

	/**
     * Creates a new ImageIcon object from a graphic out of the images zip file.
     *
     * @param url The relative path to the image in the zip archive.
     * @return The ImageIcon object of the image.
     */
    public static ImageIcon createImageIcon(String url) {
        return createImageIcon(url, AbstractConfiguration.IMAGES_DB_DIR);
    }

    public static MirroredImageIcon createMirroredImageIcon(String url) {
        return createMirroredImageIcon(url, AbstractConfiguration.IMAGES_DB_DIR);
    }
}
