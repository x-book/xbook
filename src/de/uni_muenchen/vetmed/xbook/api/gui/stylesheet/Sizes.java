package de.uni_muenchen.vetmed.xbook.api.gui.stylesheet;

import java.awt.Dimension;

/**
 * Class which holds static final variables which contain indication of size for
 * several elements of the gui.
 *
 * By adjusting these numbers several components of the GUI can be resized.
 *
 * @author Daniel Kaltenthaler
 */
public class Sizes {

    /* *********************************************** */
    /* *************** GENERAL ELEMENTS *************** */
    /* *********************************************** */
    /**
     * The size of one input field grid in pixel.
     */
    public final static Dimension INPUT_GRID_SIZE = new Dimension(160, 75);
    /**
     * The height of a JComponent object that is displayed within one input
     * field.
     *
     * Will be only be used on items those size has been set manually. (e.g. if
     * there are more than one components in one input field.
     */
    public final static int INPUT_FIELD_HEIGHT = 32;
    /**
     * A standard modifier hat is used when creating a small button for the
     * button panel. 1.0 means that the button has 100 percent of its size.
     */
    public final static double MODIFIER_SMALL_BUTTON = 0.7;
    /**
     * A standard modifier hat is used when creating a small button for the
     * button panel. 1.0 means that the button has 100 percent of its size.
     */
    public final static double MODIFIER_BIG_BUTTON = 1.7;
    /* *********************************************** */
    /* **************** STACK ELEMENTS *************** */
    /* *********************************************** */
    /**
     * The absolute width of the stack elements.
     */
    public static final int STACK_DEFAULT_WIDTH = 600;
    /**
     * The default height of the stack elements.
     */
    @Deprecated
    public static final int STACK_DEFAULT_HEIGHT = 32;
    @Deprecated
    public static final int STACK_DEFAULT_HEIGHT_NEW = 48;
    @Deprecated
    public static final int STACK_QUADRUPLE_HEIGHT = 4 * STACK_DEFAULT_HEIGHT;
    @Deprecated
    public static final int STACK_THREEQUARTER_HEIGHT = 3 * STACK_DEFAULT_HEIGHT;
    /**
     * The default size of the label of a stack element.
     */
    @Deprecated
    public static final Dimension STACK_DEFAULT_SIZE = new Dimension(STACK_DEFAULT_WIDTH, STACK_DEFAULT_HEIGHT_NEW);
    /**
     * The default width of a stack element.
     */
    @Deprecated
    public static final int STACK_LABEL_WIDTH_DEFAULT = 150;
    /**
     * The default size of the label of a stack element.
     */
    @Deprecated
    public static final Dimension STACK_LABEL_SIZE_DEFAULT = new Dimension(STACK_LABEL_WIDTH_DEFAULT, STACK_DEFAULT_HEIGHT);
    /**
     * The default small width of a stack element.
     */
    @Deprecated
    public static final int STACK_LABEL_WIDTH_SMALL = 100;
    /**
     * The default height of a button.
     */
    public static final int BUTTON_HEIGHT = 40;
    /**
     * The default width of a button.
     */
    public static final int BUTTON_WIDTH = 150;
}
