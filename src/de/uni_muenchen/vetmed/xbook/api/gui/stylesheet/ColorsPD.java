package de.uni_muenchen.vetmed.xbook.api.gui.stylesheet;

import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors.BOOK_COLOR;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors.BOOK_COLOR_DARKER;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors.INPUT_FIELD_FOCUSED_MANDATORY_BACKGROUND;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors.INPUT_FIELD_MANDATORY_BACKGROUND;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors.SIDEBAR_BACKGROUND;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors.TABLE_LINE_HOVERED_COLOR;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors.TABLE_LINE_SELECTED_COLOR_DARK;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors.TABLE_LINE_SELECTED_COLOR_LIGHT;
import java.awt.Color;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class ColorsPD extends Colors {

	static {
		/* ******************************************************************* */
		/* ************************** BOOK COLORS **************************** */
		/* ******************************************************************* */
		BOOK_COLOR = new Color(98, 77, 71);
		BOOK_COLOR_DARKER = new Color(68, 54, 50);
		/* ******************************************************************* */
		/* ************************ MANDATORY FIELDS ************************* */
		/* ******************************************************************* */
		INPUT_FIELD_MANDATORY_BACKGROUND = new Color(232, 232, 196);
		INPUT_FIELD_FOCUSED_MANDATORY_BACKGROUND = new Color(236, 236, 191); // new Color(213, 206, 202);
		/* ******************************************************************* */
		/* ************************** TABLE LINES **************************** */
		/* ******************************************************************* */
		TABLE_LINE_SELECTED_COLOR_DARK = new Color(234, 206, 195);
		TABLE_LINE_SELECTED_COLOR_LIGHT = new Color(234, 206, 195);
		TABLE_LINE_HOVERED_COLOR = new Color(230, 208, 199);;
		/* ******************************************************************* */
		/* ************************ GENERAL BACKGROUND *********************** */
		/* ******************************************************************* */
		SIDEBAR_BACKGROUND = new Color(213, 206, 202);
	}

	public static Color LISTING_ROW_LOAN_ENDS_NOW = new Color(234,178,178);
	public static Color LISTING_ROW_LOAN_ENDS_SOON = new Color(252,238,183);

	public static void init() {
		// only necessary for initialisation of book-specific Color classes
	}
}
