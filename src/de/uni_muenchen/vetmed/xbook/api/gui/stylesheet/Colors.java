package de.uni_muenchen.vetmed.xbook.api.gui.stylesheet;

import java.awt.*;

/**
 * A stylesheet object which makes it possible to change some color settings of the design of the GUI.
 * <p>
 * By changing the variables in this class it is possible to present the application in different colors
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Colors {

    /* ******************************************************************* */
    /* ************************ GENERAL BACKGROUND *********************** */
    /* ******************************************************************* */
    public static Color SIDEBAR_BACKGROUND;
    public final static Color CONTENT_BACKGROUND = new Color(245, 245, 245);
    public final static Color CONTENT_FOREGROUND = new Color(0, 0, 0);
    /* ******************************************************************* */
    /* ************************** INPUT FIELDS ************************** */
    /* ******************************************************************* */
    public final static Color INPUT_FIELD_BACKGROUND = new Color(235, 235, 235);
    public final static Color INPUT_FIELD_BORDER = new Color(225, 225, 225);
    public final static Color INPUT_FIELD_FOCUSED_BACKGROUND = new Color(234, 234, 234);
    public final static Color INPUT_FIELD_FOCUSED_BORDER = new Color(110, 110, 110);
    public static Color INPUT_FIELD_MANDATORY_BACKGROUND;
    public static Color INPUT_FIELD_FOCUSED_MANDATORY_BACKGROUND;
    public final static Color INPUT_FIELD_INPUT_BACKGROUND = new Color(255, 255, 255);
    public final static Color INPUT_FIELD_INPUT_BORDER = new Color(230, 230, 230);
    public final static Color INPUT_FIELD_ERROR_BACKGROUND = new Color(235, 153, 153);
    public final static Color BUTTON_PANEL_BACKGROUND = CONTENT_BACKGROUND;
    public final static Color NAVIGATION_BACKGROUND = new Color(0, 0, 0);
    public final static Color NAVIGATION_FOREGROUND = new Color(255, 255, 255);
    /* ******************************************************************* */
    /* ************************** TABLE LINES **************************** */
    /* ******************************************************************* */
    public final static Color TABLE_LINE_DARK = new Color(240, 240, 240);
    public final static Color TABLE_LINE_LIGHT = new Color(255, 255, 255);
    public static Color TABLE_LINE_SELECTED_COLOR_DARK;
    public static Color TABLE_LINE_SELECTED_COLOR_LIGHT;
    public static Color TABLE_LINE_HOVERED_COLOR;
    /* ******************************************************************* */
    /* ***************************** SIDEBAR ***************************** */
    /* ******************************************************************* */
    public final static Color UPDATE_ACTIVE = new Color(255, 204, 0);
    public final static Color UPDATE_INACTIVE = new Color(192, 192, 192);
    public final static Color UPDATE_UNKNOWN = new Color(128, 128, 128);
    /* ******************************************************************* */
    /* ************************* BUTTON_COLORS *************************** */
    /* ******************************************************************* */
    public final static Color HIGHLIGHT_YELLOW = new Color(255, 204, 0);
    public final static Color HIGHLIGHT_YELLOW_BACKGROUND = new Color(255, 250, 227);
    public final static Color HIGHLIGHT_YELLOW_BACKGROUND_HOVER = new Color(255, 252, 240);
    public final static Color HIGHLIGHT_RED = new Color(255, 53, 53);
    public final static Color HIGHLIGHT_RED_BACKGROUND = new Color(255, 210, 210);
    public final static Color HIGHLIGHT_RED_BACKGROUND_HOVERED = new Color(255, 232, 232);
    public final static Color HIGHLIGHT_GREEN = new Color(76, 136, 74);
    public final static Color HIGHLIGHT_GREEN_BACKGROUND = new Color(224, 240, 217);
    public final static Color HIGHLIGHT_GREEN_BACKGROUND_HOVERED = new Color(239, 247, 236);
    public final static Color HIGHLIGHT_GRAY = new Color(154, 154, 154);
    public final static Color HIGHLIGHT_GRAY_BACKGROUND = new Color(241, 241, 241);
    public final static Color HIGHLIGHT_GRAY_BACKGROUND_HOVERED = new Color(248, 248, 248);
    public final static Color HIGHLIGHT_DARK_GRAY_BACKGROUND = new Color(165, 165, 165);
    /* ******************************************************************* */
    /* ************************** BOOK COLORS **************************** */
    /* ******************************************************************* */
    public static Color BOOK_COLOR;
    public static Color BOOK_COLOR_DARKER;
    public static Color BOOK_COLOR_SUBTITLE_BG;
    /**
     * The background color of the teeth table.
     */
    public final static Color TEETH_TOTAL_BACKGROUND = new Color(198, 255, 198);

    public final static javafx.scene.paint.Color toFxColor(Color c) {
        return new javafx.scene.paint.Color(c.getRed() / 255.0, c.getBlue() / 255.0, c.getGreen() / 255.0,
                c.getAlpha() / 255.0);
    }
}
