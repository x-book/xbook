package de.uni_muenchen.vetmed.xbook.api.gui.stylesheet;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A class holding the paths of all files that can be opened within the application.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Files {

    /**
     * The language shortcut of the loaded language in the config files.
     */
    private final static String language = Locale.getDefault().getLanguage();
    /**
     * An own variable for the system specific file separator ('cause it's shorter)
     */
    private final static String SEP = "/";
    ///////////////////
    // Input Field PDFs
    ///////////////////
    /**
     * The file path to the age 1 basel PDF file.
     */
    public static final String PDF_AGE_X_BASEL = "assets" + SEP + "documents" + SEP + "age_basel.pdf";
    /**
     * The file path to the animal PDF file.
     */
    public static final String PDF_ANIMAL = "assets" + SEP + "documents" + SEP + "animal_" + language + ".pdf";
    /**
     * The file path to the animal PDF file.
     */
    public static final String PDF_SKELETONELEMENT = "assets" + SEP + "documents" + SEP + "skeletonelement.pdf";
    /**
     * The file path to the bone element longbone codes PDF file.
     */
    public static final String PDF_BONEELEMENT_LONGBONECONES = "assets" + SEP + "documents" + SEP + "bone_element_longbone_codes.pdf";
    /**
     * The file path to the diagnostic zones PDF file.
     */
    public static final String PDF_DIAGNOSTIC_ZONES = "assets" + SEP + "documents" + SEP + "diagnostic_zones.pdf";
    

    /**
     * Opens a document out of the documents.zip file with the path from the parameters.
     *
     * @param url The path of the file to open.
     */
    public static void openFile(String url) {
        try {
            Desktop.getDesktop().open(new File(url));
        } catch (IOException ex) {
            Logger.getLogger(Files.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
