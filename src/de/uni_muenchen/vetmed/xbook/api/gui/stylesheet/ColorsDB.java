package de.uni_muenchen.vetmed.xbook.api.gui.stylesheet;

import java.awt.*;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class ColorsDB extends Colors {
    static {
        BOOK_COLOR = new Color(66, 35, 86);
        BOOK_COLOR_DARKER = new Color(66, 35, 86);
    }

    public static void init() {
        // only necessary for initialisation of book-specific Color classes
    }
}
