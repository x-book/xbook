package de.uni_muenchen.vetmed.xbook.api.gui.stylesheet;

import java.awt.Font;

/**
 * A stylesheet object which makes it possible to change some size settings of the design of the GUI.
 *
 * By changing the variables in this class it is possible to adjust the layout in the application.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class Fonts {

    /**
     * The bottom-padding size below a text paragraph.
     */
    public final static int TEXT_PARAGRAPH_PADDING = 8;
    
    public final static Font FONT_SMALL_PLAIN = new Font("SansSerif", Font.PLAIN, 11);
    public final static Font FONT_SMALL_BOLD = new Font("SansSerif", Font.BOLD, 11);
    public final static Font FONT_STANDARD_PLAIN = new Font("SansSerif", Font.PLAIN, 12);
    public final static Font FONT_STANDARD_BOLD = new Font("SansSerif", Font.BOLD, 12);
    public final static Font FONT_BIG_PLAIN = new Font("SansSerif", Font.PLAIN, 14);
    public final static Font FONT_BIG_BOLD = new Font("SansSerif", Font.BOLD, 14);
    public final static Font FONT_VERYBIG_PLAIN = new Font("SansSerif", Font.PLAIN, 16);
    public final static Font FONT_VERYBIG_BOLD = new Font("SansSerif", Font.BOLD, 16);
    
}
