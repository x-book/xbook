package de.uni_muenchen.vetmed.xbook.api.gui.stylesheet;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.api.framework.JarResources;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * A class holding the paths of all icons which are used in the navigation in the header of the
 * application.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class AbstractImages {

  private static final Log LOGGER = LogFactory.getLog(AbstractImages.class);
  /**
   * Holds a reference to the JarResources object.
   */
  private static HashMap<String, JarResources> images = new HashMap<>();
  /**
   * An own variable for the system specific file separator ('cause it's shorter)
   */
  public final static String SEP = "/";

  /**
   * Creates a new ImageIcon object from a graphic out of the images zip file.
   *
   * @param url The relative path to the image in the zip archive.
   * @param dir The path to the zip archive that contains the images.
   * @return The ImageIcon object of the image.
   */
  public static ImageIcon createImageIcon(String url, String dir) {
    ImageIcon image;
    try {
      image = new ImageIcon(getJarResources(dir).getResource(url));
    } catch (NullPointerException ex) {
      try {
        url = url.replace("_" + AbstractConfiguration.CURRENT_LANGUAGE + "_", "_en_");
        image = new ImageIcon(getJarResources(dir).getResource(url));

      } catch (Exception e) {
        image = null;
        LOGGER.error("> Loading image '" + dir + "/" + url + "' FAILED!");
      }
    }
    return image;
  }

  public static ImageIcon createImageIconResource(String path, String dir, Class clasz) {
    ImageIcon image;

    URL url = clasz.getResource(dir + path);
    if (url == null) {
      path = path.replace("_" + AbstractConfiguration.CURRENT_LANGUAGE + "_", "_en_");
      url = clasz.getResource(dir + path);
      if (url == null) {
        LOGGER.error("> Loading image '" + dir + "/" + path + "' FAILED!");
        image = null;
      } else {
        image = new ImageIcon(url);
      }
    } else {
      image = new ImageIcon(url);
    }
    return image;
  }

  public static MirroredImageIcon createMirroredImageIconResource(String path, String dir,
      Class clasz) {
    MirroredImageIcon image;

    URL url = clasz.getResource(dir + path);
    if (url == null) {
      path = path.replace("_" + AbstractConfiguration.CURRENT_LANGUAGE + "_", "_en_");
      url = clasz.getResource(dir + path);
      if (url == null) {
        LOGGER.error("> Loading image '" + dir + "/" + path + "' FAILED!");
        image = null;
      } else {
        image = new MirroredImageIcon(url);
      }
    } else {
      image = new MirroredImageIcon(url);
    }
    return image;
  }

  public static BufferedImage createBufferedImage(String url, String dir) {
    BufferedImage image;
    try {
      image = ImageIO.read(new ByteArrayInputStream(getJarResources(dir).getResource(url)));

    } catch (IOException ex) {
      try {
        url = url.replace("_" + AbstractConfiguration.CURRENT_LANGUAGE + "_", "_en_");
        image = ImageIO.read(new ByteArrayInputStream(getJarResources(dir).getResource(url)));


      } catch (Exception e) {
        image = null;
        LOGGER.error("> Loading image '" + dir + "/" + url + "' FAILED!");
      }
    }
    return image;
  }

  /**
   * Creates a new ImageIcon object from a graphic out of the images zip file.
   *
   * @param url The relative path to the image in the zip archive.
   * @return The ImageIcon object of the image.
   */
  public static ImageIcon createImageIcon(String url) {
    return createImageIcon(url, AbstractConfiguration.IMAGES_DIR);
  }

  /**
   * Creates a new ImageIcon object from a graphic out of the images zip file.
   *
   * @param url The relative path to the image in the zip archive.
   * @param dir The path to the zip archive that contains the images.
   * @return The ImageIcon object of the image.
   */
  public static MirroredImageIcon createMirroredImageIcon(String url, String dir) {
    MirroredImageIcon image;
    try {
      image = new MirroredImageIcon(getJarResources(dir).getResource(url));
    } catch (NullPointerException ex) {
      try {
        url = url.replace("_" + AbstractConfiguration.CURRENT_LANGUAGE + "_", "_en_");
        image = new MirroredImageIcon(getJarResources(dir).getResource(url));
      } catch (Exception e) {
        image = null;
        LOGGER.error("> Loading mirrored image '" + dir + "/" + url + "' FAILED!");
      }
    }
    return image;
  }

  /**
   * Creates a new ImageIcon object from a graphic out of the images zip file.
   *
   * @param url The relative path to the image in the zip archive.
   * @return The ImageIcon object of the image.
   */
  public static MirroredImageIcon createMirroredImageIcon(String url) {
    return createMirroredImageIcon(url, AbstractConfiguration.IMAGES_DIR);
  }

  /**
   * Returns the JarResources object holding the image files.
   *
   * @param dir The path to the images zip.
   * @return The JarResources object.e
   */
  public static JarResources getJarResources(String dir) {
    if (!images.containsKey(dir)) {
      images.put(dir, new JarResources(dir));
    }
    return images.get(dir);
  }

  public static class MirroredImageIcon extends ImageIcon {

    public MirroredImageIcon(String filename) {
      super(filename);
    }

    public MirroredImageIcon(URL location) {
      super(location);
    }

    public MirroredImageIcon(byte[] imageData) {
      super(imageData);
    }

    @Override
    public synchronized void paintIcon(Component c, Graphics g, int x, int y) {
      Graphics2D g2 = (Graphics2D) g.create();
      g2.translate(getIconWidth(), 0);
      g2.scale(-1, 1);
      super.paintIcon(c, g2, x, y);
    }


  }

  ////////////////////////
  // BUTTON PANEL GRAPHICS
  ////////////////////////
  public static ImageIcon BUTTON_PANEL_BUTTON_LEFT;
  public static ImageIcon BUTTON_PANEL_BUTTON_CENTER;
  public static ImageIcon BUTTON_PANEL_BUTTON_RIGHT;
  public static ImageIcon BUTTON_PANEL_BUTTON_HOVERED_LEFT;
  public static ImageIcon BUTTON_PANEL_BUTTON_HOVERED_CENTER;
  public static ImageIcon BUTTON_PANEL_BUTTON_HOVERED_RIGHT;

}
