package de.uni_muenchen.vetmed.xbook.api.gui.stylesheet;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.BUTTON_PANEL_BUTTON_CENTER;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.BUTTON_PANEL_BUTTON_LEFT;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.BUTTON_PANEL_BUTTON_RIGHT;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.BUTTON_PANEL_BUTTON_HOVERED_CENTER;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.BUTTON_PANEL_BUTTON_HOVERED_LEFT;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.BUTTON_PANEL_BUTTON_HOVERED_RIGHT;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.SEP;

import javax.swing.*;

/**
 * A class holding the paths of all icons which are used in the navigation in the header of the application.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ImagesOB extends AbstractImages {

    //////////////////////
	// APPLICATION FAVICON
	//////////////////////
	public final static ImageIcon FAVICON_16 = createImageIcon("favicon" + SEP + "16.png");
	public final static ImageIcon FAVICON_24 = createImageIcon("favicon" + SEP + "24.png");
	public final static ImageIcon FAVICON_32 = createImageIcon("favicon" + SEP + "32.png");
	public final static ImageIcon FAVICON_48 = createImageIcon("favicon" + SEP + "48.png");
	public final static ImageIcon FAVICON_64 = createImageIcon("favicon" + SEP + "64.png");
	public final static ImageIcon FAVICON_96 = createImageIcon("favicon" + SEP + "96.png");
	public final static ImageIcon FAVICON_128 = createImageIcon("favicon" + SEP + "128.png");
    ///////////////////////////
	// NAVIGATION GRAPHICS
	///////////////////////////
	public final static ImageIcon NAVIGATION_APPLICATION_LOGO = createImageIcon("navigation" + SEP + "logo.png");
    ////////////////
	// SPLASH SCREEN
	////////////////
	public final static ImageIcon SPLASH_BACKGROUND = createImageIcon("general" + SEP + "splash_screen_background.png");
    ////////////////////////
	// BUTTON PANEL GRAPHICS
	////////////////////////

	static {
		BUTTON_PANEL_BUTTON_LEFT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_border.png");
		BUTTON_PANEL_BUTTON_CENTER = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_center.png");
		BUTTON_PANEL_BUTTON_RIGHT = createMirroredImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_border.png");
        BUTTON_PANEL_BUTTON_HOVERED_LEFT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_hover_border.png");
        BUTTON_PANEL_BUTTON_HOVERED_CENTER = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_hover_center.png");
        BUTTON_PANEL_BUTTON_HOVERED_RIGHT = createMirroredImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_hover_border.png");
	}
    ///////////////////////////
	// FOOTER BUTTON GRAPHICS
	///////////////////////////
	public final static ImageIcon FOOTER_LOADING_ICON_01 = Images.FOOTER_LOADING_BONES_01;
	public final static ImageIcon FOOTER_LOADING_ICON_02 = Images.FOOTER_LOADING_BONES_02;
	public final static ImageIcon FOOTER_LOADING_ICON_03 = Images.FOOTER_LOADING_BONES_03;
	public final static ImageIcon FOOTER_LOADING_ICON_04 = Images.FOOTER_LOADING_BONES_04;
	public final static ImageIcon FOOTER_LOADING_ICON_05 = Images.FOOTER_LOADING_BONES_05;
	public final static ImageIcon FOOTER_LOADING_ICON_06 = Images.FOOTER_LOADING_BONES_06;
	public final static ImageIcon FOOTER_LOADING_ICON_07 = Images.FOOTER_LOADING_BONES_07;
	public final static ImageIcon FOOTER_LOADING_ICON_08 = Images.FOOTER_LOADING_BONES_08;
	public final static ImageIcon FOOTER_LOADING_ICON_09 = Images.FOOTER_LOADING_BONES_09;
	public final static ImageIcon FOOTER_LOADING_ICON_10 = Images.FOOTER_LOADING_BONES_10;
	public final static ImageIcon FOOTER_LOADING_ICON_11 = Images.FOOTER_LOADING_BONES_11;
	public final static ImageIcon FOOTER_LOADING_ICON_12 = Images.FOOTER_LOADING_BONES_12;
	
	public final static ImageIcon FOOTER_PROGRESS_ICON_BIG = Images.FOOTER_PROGRESS_BONE_BIG;
	public final static ImageIcon FOOTER_PROGRESS_ICON_SMALL = Images.FOOTER_PROGRESS_BONE_SMALL;
	public final static ImageIcon FOOTER_PROGRESS_ICON_SMALL_GRAY = Images.FOOTER_PROGRESS_BONE_SMALL_GRAY;
    ////////////////////////////////
	// BONE ELEMENT GRAPHICS
	////////////////////////////////
	public final static ImageIcon LONGBONE_BG = createImageIcon("boneElement" + SEP + "longbone.png");
	public final static ImageIcon LONGBONE_A = createImageIcon("boneElement" + SEP + "longbone_a.png");
	public final static ImageIcon LONGBONE_B = createImageIcon("boneElement" + SEP + "longbone_b.png");
	public final static ImageIcon LONGBONE_C = createImageIcon("boneElement" + SEP + "longbone_c.png");
	public final static ImageIcon LONGBONE_D = createImageIcon("boneElement" + SEP + "longbone_d.png");
	public final static ImageIcon MANDIBULA_BG = createImageIcon("boneElement" + SEP + "mandibula.png");
	public final static ImageIcon SCAPULA_BG = createImageIcon("boneElement" + SEP + "scapula.png");
	public final static ImageIcon PELVIS_BG = createImageIcon("boneElement" + SEP + "pelvis.png");

    ////////////////////////////////
	// DIAGNOSTIC ZONES GRAPHICS
	////////////////////////////////
	public final static ImageIcon DIAGNOSTICZONES_TEST1 = createImageIcon("yorkSystem" + SEP + "test1.png");
	public final static ImageIcon DIAGNOSTICZONES_TEST2 = createImageIcon("yorkSystem" + SEP + "test2.png");
	public final static ImageIcon DIAGNOSTICZONES_TEST3 = createImageIcon("yorkSystem" + SEP + "test3.png");
	public final static ImageIcon DIAGNOSTICZONES_TEST4 = createImageIcon("yorkSystem" + SEP + "test4.png");
	public final static ImageIcon DIAGNOSTICZONES_44_OviEquSusCamBosCer = createImageIcon("yorkSystem" + SEP + "Zones-img_44_OviEquSusCamBosCer.png");
	public final static ImageIcon DIAGNOSTICZONES_3_5_Bos = createImageIcon("yorkSystem" + SEP + "Zones-img_3_5_Bos.png");
	public final static ImageIcon DIAGNOSTICZONES_5_Sus = createImageIcon("yorkSystem" + SEP + "Zones-img_5_Sus.png");
	public final static ImageIcon DIAGNOSTICZONES_22_Ave = createImageIcon("yorkSystem" + SEP + "Zones-img_22_Ave.png");
	public final static ImageIcon DIAGNOSTICZONES_23_Ave = createImageIcon("yorkSystem" + SEP + "Zones-img_23_Ave.png");
	public final static ImageIcon DIAGNOSTICZONES_23_CanLepRodInsUrsFel = createImageIcon("yorkSystem" + SEP + "Zones-img_23_CanLepRodInsUrsFel.png");
	public final static ImageIcon DIAGNOSTICZONES_23_OviEquSusCamBosCer = createImageIcon("yorkSystem" + SEP + "Zones-img_23_OviEquSusCamBosCer.png");
	public final static ImageIcon DIAGNOSTICZONES_24_Ave = createImageIcon("yorkSystem" + SEP + "Zones-img_24_Ave.png");
	public final static ImageIcon DIAGNOSTICZONES_24_CanLepRodInsUrsFel = createImageIcon("yorkSystem" + SEP + "Zones-img_24_CanLepRodInsUrsFel.png");
	public final static ImageIcon DIAGNOSTICZONES_24_OviEquSusCamBosCer = createImageIcon("yorkSystem" + SEP + "Zones-img_24_OviEquSusCamBosCer.png");
	public final static ImageIcon DIAGNOSTICZONES_25_CanLepRodInsUrsFel = createImageIcon("yorkSystem" + SEP + "Zones-img_25_CanLepRodInsUrsFel.png");
	public final static ImageIcon DIAGNOSTICZONES_25_26_OviEquSusCamBosCer = createImageIcon("yorkSystem" + SEP + "Zones-img_25_26_OviEquSusCamBosCer.png");
	public final static ImageIcon DIAGNOSTICZONES_26_Ave = createImageIcon("yorkSystem" + SEP + "Zones-img_26_Ave.png");
	public final static ImageIcon DIAGNOSTICZONES_26_CanLepRodInsUrsFel = createImageIcon("yorkSystem" + SEP + "Zones-img_26_CanLepRodInsUrsFel.png");
	public final static ImageIcon DIAGNOSTICZONES_29_Ave = createImageIcon("yorkSystem" + SEP + "Zones-img_29_Ave.png");
	public final static ImageIcon DIAGNOSTICZONES_30_50_100_OviCamBosCer = createImageIcon("yorkSystem" + SEP + "Zones-img_30_50_100_OviCamBosCer.png");
	public final static ImageIcon DIAGNOSTICZONES_31_51_60_Equ = createImageIcon("yorkSystem" + SEP + "Zones-img_31_51_60_Equ.png");
	public final static ImageIcon DIAGNOSTICZONES_41_OviEquSusCamBosCerUrs = createImageIcon("yorkSystem" + SEP + "Zones-img_41_OviEquSusCamBosCerUrs.png");
	public final static ImageIcon DIAGNOSTICZONES_41_CanLepRodInsFel = createImageIcon("yorkSystem" + SEP + "Zones-img_41_CanLepRodInsFel.png");
	public final static ImageIcon DIAGNOSTICZONES_42_Ave = createImageIcon("yorkSystem" + SEP + "Zones-img_42_Ave.png");
	public final static ImageIcon DIAGNOSTICZONES_42_CanLepRodInsFel = createImageIcon("yorkSystem" + SEP + "Zones-img_42_CanLepRodInsFel.png");
	public final static ImageIcon DIAGNOSTICZONES_42_Equ = createImageIcon("yorkSystem" + SEP + "Zones-img_42_Equ.png");
	public final static ImageIcon DIAGNOSTICZONES_42_OviSusCamBosCerUrs = createImageIcon("yorkSystem" + SEP + "Zones-img_42_OviSusCamBosCerUrs.png");
	public final static ImageIcon DIAGNOSTICZONES_44_Ave = createImageIcon("yorkSystem" + SEP + "Zones-img_44_Ave.png");
	public final static ImageIcon DIAGNOSTICZONES_44_CanLepRodInsUrsFel = createImageIcon("yorkSystem" + SEP + "Zones-img_44_CanLepRodInsUrsFel.png");
	public final static ImageIcon DIAGNOSTICZONES_46_OviEquSusCamBosCer = createImageIcon("yorkSystem" + SEP + "Zones-img_46_OviEquSusCamBosCer.png");
	public final static ImageIcon DIAGNOSTICZONES_47_OviEquSusCamBosCer = createImageIcon("yorkSystem" + SEP + "Zones-img_47_OviEquSusCamBosCer.png");
	public final static ImageIcon DIAGNOSTICZONES_54_Ave = createImageIcon("yorkSystem" + SEP + "Zones-img_54_Ave.png");
	public final static ImageIcon DIAGNOSTICZONES_201_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_201_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_202_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_202_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_11_LepRodIns = createImageIcon("yorkSystem" + SEP + "Zones-img_11_LepRodIns.png");
	public final static ImageIcon DIAGNOSTICZONES_205_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_205_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_206_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_206_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_208_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_208_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_209_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_209_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_210_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_210_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_211_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_211_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_215_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_215_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_224_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_224_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_225_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_225_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_226_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_226_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_242_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_242_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_247_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_247_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_252_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_252_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_265_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_265_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_1065_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_1065_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_221_Pis = createImageIcon("yorkSystem" + SEP + "Zones-img_221_Pis.png");
	public final static ImageIcon DIAGNOSTICZONES_MpSchwein_Sus = createImageIcon("yorkSystem" + SEP + "Zones-img_MpSchwein_Sus.png");
	public final static ImageIcon DIAGNOSTICZONES_11_13_Sus = createImageIcon("yorkSystem" + SEP + "Zones-img_11_Sus.png");
	public final static ImageIcon DIAGNOSTICZONES_11_13_OviBosCer = createImageIcon("yorkSystem" + SEP + "Zones-img_11_13_OviBosCer.png");
	public final static ImageIcon DIAGNOSTICZONES_11_13_Cam = createImageIcon("yorkSystem" + SEP + "Zones-img_11_13_Cam.png");
	public final static ImageIcon DIAGNOSTICZONES_11_13_Equ = createImageIcon("yorkSystem" + SEP + "Zones-img_11_13_Equ.png");
	public final static ImageIcon DIAGNOSTICZONES_11_13_Can = createImageIcon("yorkSystem" + SEP + "Zones-img_11_13_Can.png");
	public final static ImageIcon DIAGNOSTICZONES_11_13_Fel = createImageIcon("yorkSystem" + SEP + "Zones-img_11_13_Fel.png");
	public final static ImageIcon DIAGNOSTICZONES_MpHund_CanLepRodInsUrsFel = createImageIcon("yorkSystem" + SEP + "Zones-img_MpHund_CanLepRodInsUrsFel.png");
	public final static ImageIcon DIAGNOSTICZONES_Pha1_2_OviEquSusCamBosCer = createImageIcon("yorkSystem" + SEP + "Zones-img_Pha1_2_OviEquSusCamBosCer.png");
	public final static ImageIcon DIAGNOSTICZONES_Pha3_OviEquSusCamBosCer = createImageIcon("yorkSystem" + SEP + "Zones-img_Pha3_OviEquSusCamBosCer.png");
	public final static ImageIcon DIAGNOSTICZONES_3_5_Ovi = createImageIcon("yorkSystem" + SEP + "Zones-img_3_5_Ovi.png");
	public final static ImageIcon DIAGNOSTICZONES_5_CanLepRodInsUrsFel = createImageIcon("yorkSystem" + SEP + "Zones-img_5_CanLepRodInsUrsFel.png");

    //////////////////////////////
	// WEARSTAGE 2 SIDEBAR SPECIAL
	//////////////////////////////
	// The ImageIcon holding the graphic for the sidebar of WearStage 2 Gazelle Pd4 (image #1)
	public final static ImageIcon WEARSTAGE_2_GAZELLE_PD4_1 = createImageIcon("wearstage2" + SEP + "gazelle_pd4_1.jpg");
	// The ImageIcon holding the graphic for the sidebar of WearStage 2 Gazelle Pd4 (image #2)
	public final static ImageIcon WEARSTAGE_2_GAZELLE_PD4_2 = createImageIcon("wearstage2" + SEP + "gazelle_pd4_2.jpg");

	/**
	 * Creates a new ImageIcon object from a graphic out of the images zip file.
	 *
	 * @param url The relative path to the image in the zip archive.
	 * @return The ImageIcon object of the image.
	 */
	public static ImageIcon createImageIcon(String url) {
		return createImageIcon(url, AbstractConfiguration.IMAGES_OB_DIR);
	}

	public static MirroredImageIcon createMirroredImageIcon(String url) {
		return createMirroredImageIcon(url, AbstractConfiguration.IMAGES_OB_DIR);
	}

	/**
	 * Creates a wearstage2 icon with a specific image prefix and the code.
	 *
	 * @param imagePrefix The image prefix.
	 * @param code The code.
	 * @return The final wearstage2 icon.
	 */
	public static ImageIcon getWearstage2Icon(String imagePrefix, String code) {
		return createImageIcon("wearstage2" + SEP + imagePrefix + "_" + code + ".jpg");
	}

	/**
	 * Creates a measurement sidebar image.
	 *
	 * @param skeletonElement The skeleton element ID.
	 * @param key The key.
	 * @param postFix The postfix.
	 * @return The final measurement sidebar image.
	 */
	public static ImageIcon getMeasurementSidebarImage(String skeletonElement, String key, String postFix) {
		return createImageIcon("measurementImages" + SEP + "measurements-img_" + skeletonElement + "_" + key + "_" + postFix + ".jpg");
	}

}
