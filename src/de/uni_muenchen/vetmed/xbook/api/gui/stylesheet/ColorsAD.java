package de.uni_muenchen.vetmed.xbook.api.gui.stylesheet;

import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors.BOOK_COLOR;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors.BOOK_COLOR_DARKER;
import java.awt.Color;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class ColorsAD extends Colors {

    static {
        /* ******************************************************************* */
        /* ************************** BOOK COLORS **************************** */
        /* ******************************************************************* */
        BOOK_COLOR = new Color(140, 116, 95); // original: 202, 172, 135
        BOOK_COLOR_DARKER = new Color(140, 116, 95);
        /* ******************************************************************* */
        /* ************************ MANDATORY FIELDS ************************* */
        /* ******************************************************************* */
        INPUT_FIELD_MANDATORY_BACKGROUND = new Color(218, 203, 185);
        INPUT_FIELD_FOCUSED_MANDATORY_BACKGROUND = new Color(225, 210, 192);
        /* ******************************************************************* */
        /* ************************** TABLE LINES **************************** */
        /* ******************************************************************* */
        TABLE_LINE_SELECTED_COLOR_DARK = new Color(234, 231, 222); // not changed yet
        TABLE_LINE_SELECTED_COLOR_LIGHT = new Color(234, 206, 195); // not changed yet
        TABLE_LINE_HOVERED_COLOR = new Color(234, 231, 222);;
        /* ******************************************************************* */
        /* ************************ GENERAL BACKGROUND *********************** */
        /* ******************************************************************* */
        SIDEBAR_BACKGROUND = new Color(234, 231, 222);
    }

    public static Color LISTING_ROW_LOAN_ENDS_NOW = new Color(234,178,178);
    public static Color LISTING_ROW_LOAN_ENDS_SOON = new Color(252,238,183);


    public static void init() {
        // only necessary for initialisation of book-specific Color classes
    }
}
