package de.uni_muenchen.vetmed.xbook.api.gui.stylesheet;

import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.AbstractImages.SEP;
import static de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.ImagesOB.createImageIcon;

import javax.swing.*;

/**
 * A class holding the paths of all icons which are used in the navigation in
 * the header of the application.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class Images extends AbstractImages {

    //////////////////////
    // APPLICATION FAVICON
    //////////////////////
    public final static ImageIcon FAVICON_XBOOK_16 = createImageIcon("favicon" + SEP + "xbook_16.png");
    public final static ImageIcon FAVICON_XBOOK_24 = createImageIcon("favicon" + SEP + "xbook_24.png");
    public final static ImageIcon FAVICON_XBOOK_32 = createImageIcon("favicon" + SEP + "xbook_32.png");
    public final static ImageIcon FAVICON_XBOOK_48 = createImageIcon("favicon" + SEP + "xbook_48.png");
    public final static ImageIcon FAVICON_XBOOK_64 = createImageIcon("favicon" + SEP + "xbook_64.png");
    public final static ImageIcon FAVICON_XBOOK_96 = createImageIcon("favicon" + SEP + "xbook_96.png");
    public final static ImageIcon FAVICON_XBOOK_128 = createImageIcon("favicon" + SEP + "xbook_128.png");
    ///////////////////////////
    // NEW NAVIGATION GRAPHICS
    ///////////////////////////
    public final static ImageIcon NAVIGATION_BACKGROUND = createImageIcon("navigation" + SEP + "main_background.png");
    public final static ImageIcon NAVIGATION_COLLAPSABLE_ICON_DOWN = createImageIcon("navigation" + SEP + "collapsable_icon.png");
    public final static ImageIcon NAVIGATION_COLLAPSABLE_ICON_DOWN_BLACK = createImageIcon("navigation" + SEP + "collapsable_icon_black.png");
    public final static ImageIcon SUB_NAVIGATION_BACKGROUND = createImageIcon("navigation" + SEP + "sub_navigation_background.png");
    public final static ImageIcon SUB_NAVIGATION_BACKGROUND_SELECTED = createImageIcon("navigation" + SEP + "sub_navigation_background_selected.png");
    ///////////////////////////
    // MAIN NAVIGATION ICONS
    ///////////////////////////
    public final static ImageIcon NAVIGATION_ICON_PACMAN = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "pacman.png");
    public final static ImageIcon NAVIGATION_ICON_PACMAN_GHOST = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "pacman_ghost.png");
    public final static ImageIcon NAVIGATION_ICON_DATA_ENTRY = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "dataentry.png");
    public final static ImageIcon NAVIGATION_ICON_EXPORT = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "export.png");
    public final static ImageIcon NAVIGATION_ICON_EXPORT_FILTERED = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "export_filtered.png");
    public final static ImageIcon NAVIGATION_ICON_IMPORT = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "import.png");
    public final static ImageIcon NAVIGATION_ICON_LOGOUT = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "logout.png");
    public final static ImageIcon NAVIGATION_ICON_PROJECTS = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "projects.png");
    public final static ImageIcon NAVIGATION_ICON_REPORT = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "report.png");
    public final static ImageIcon NAVIGATION_ICON_REPORT_2 = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "report2.png");
    public final static ImageIcon NAVIGATION_ICON_GROUP = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "group.png");
    public final static ImageIcon NAVIGATION_ICON_SETTINGS = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "settings.png");
    public final static ImageIcon NAVIGATION_ICON_SYNCHRONISATION = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "synchronisation.png");
    public final static ImageIcon NAVIGATION_ICON_LISTING = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "listing.png");
    public final static ImageIcon NAVIGATION_ICON_HELP = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "questionmark.png");
    public final static ImageIcon NAVIGATION_ICON_PERSON = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "person.png");
    public final static ImageIcon NAVIGATION_ICON_ADMIN = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "admin.png");
    public final static ImageIcon NAVIGATION_ICON_SEARCH = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "search.png");
    public final static ImageIcon NAVIGATION_ICON_BARCODE = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "barcode.png");
    public final static ImageIcon NAVIGATION_ICON_BOXLABEL = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "boxlabel.png");
    public final static ImageIcon NAVIGATION_ICON_DOCUMENT_LISTING = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "document_listing.png");
    public final static ImageIcon NAVIGATION_ICON_KEY = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "key.png");
    public final static ImageIcon NAVIGATION_ICON_ANALYSIS = createImageIcon("navigation" + SEP + "navigation-icons" + SEP + "analysis.png");

    ///////////////////////////
    // MAIN NAVIGATION SMALL ICONS
    ///////////////////////////
    public final static ImageIcon NAVIGATION_ICON_SMALL_EXPORT = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "export.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_IMPORT = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "import.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_LOCK = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "lock.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_PERSON = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "person.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_GROUP = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "group.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_GROUP2 = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "group2.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_GROUP3 = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "group3.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_PLUS = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "plus.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_PROJECT = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "project.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_SETTINGS = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "settings.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_INFO = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "info.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_LINK = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "link.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_PAPER = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "paper.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_OPEN = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "open.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_AT = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "at.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_EDIT = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "edit.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_DELETE = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "delete.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_RELOAD = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "reload.png");
    public final static ImageIcon NAVIGATION_ICON_SMALL_CHECK = createImageIcon("navigation" + SEP + "navigation-small-icons" + SEP + "check.png");
    ///////////////////////////
    // FOOTER BUTTON GRAPHICS
    ///////////////////////////
    public final static ImageIcon FOOTER_LOADING_TRANSPARENT = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loading_transparent.png");
    public final static ImageIcon FOOTER_CONFIGURATION = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "configuration.png");
    public final static ImageIcon FOOTER_CONFIGURATION_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "configuration_hovered.png");
    public final static ImageIcon FOOTER_SYNCHRONISATION = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "synchronisation.png");
    public final static ImageIcon FOOTER_SYNCHRONISATION_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "synchronisation_hovered.png");
    public final static ImageIcon FOOTER_SHOWSIDEBAR = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "showsidebar.png");
    public final static ImageIcon FOOTER_SHOWSIDEBAR_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "showsidebar_hovered.png");
    public final static ImageIcon FOOTER_HIDESIDEBAR = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "hidesidebar.png");
    public final static ImageIcon FOOTER_HIDESIDEBAR_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "hidesidebar_hovered.png");
    public final static ImageIcon FOOTER_DISABLEDSIDEBAR = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "disabledsidebar.png");
    public final static ImageIcon FOOTER_DISABLEDSIDEBAR_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "disabledsidebar_hovered.png");
    public final static ImageIcon FOOTER_PLACEHOLDER = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "placeholder.png");
    public final static ImageIcon FOOTER_PLACEHOLDER_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "placeholder_hovered.png");
    public final static ImageIcon FOOTER_TIMER = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "timer.png");
    public final static ImageIcon FOOTER_TIMER_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "timer_hovered.png");
    public final static ImageIcon FOOTER_DISCONNECTED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "disconnected.png");
    public final static ImageIcon FOOTER_DISCONNECTED_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "disconnected_hovered.png");
    public final static ImageIcon FOOTER_CONNECTED_AUTO = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "connected_auto.png");
    public final static ImageIcon FOOTER_CONNECTED_AUTO_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "connected_auto_hovered.png");
    public final static ImageIcon FOOTER_CONNECTED_MANUAL = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "connected_manual.png");
    public final static ImageIcon FOOTER_CONNECTED_MANUAL_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "connected_manual_hovered.png");
    public final static ImageIcon FOOTER_EXPORT = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "export.png");
    public final static ImageIcon FOOTER_EXPORT_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "export_hovered.png");
    public final static ImageIcon FOOTER_IMPORT = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "import.png");
    public final static ImageIcon FOOTER_IMPORT_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "import_hovered.png");
    public final static ImageIcon FOOTER_CONFLICTED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "conflict.png");
    public final static ImageIcon FOOTER_CONFLICTED_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "conflict_hovered.png");
    public final static ImageIcon FOOTER_CONTACT = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "contact.png");
    public final static ImageIcon FOOTER_CONTACT_HOVERED = createImageIcon("navigation" + SEP + "footer-icons" + SEP + "contact_hovered.png");
    public final static ImageIcon PROJECT_FOLDER = createImageIcon("navigation" + SEP + "project-icons" + SEP + "project_folder.png");
    public final static ImageIcon PROJECT_USER = createImageIcon("navigation" + SEP + "project-icons" + SEP + "project_user.png");
    public final static ImageIcon PROJECT_OWNER = createImageIcon("navigation" + SEP + "project-icons" + SEP + "project_owner.png");
    public final static ImageIcon PROJECT_READ_RIGHTS = createImageIcon("navigation" + SEP + "project-icons" + SEP + "read_rights.png");
    public final static ImageIcon PROJECT_WRITE_RIGHTS = createImageIcon("navigation" + SEP + "project-icons" + SEP + "write_rights.png");
    public final static ImageIcon PROJECT_CONFLICTED = createImageIcon("navigation" + SEP + "project-icons" + SEP + "project_conflicted2.png");
    public final static ImageIcon FOOTER_BUTTON = createImageIcon("footer" + SEP + "button_dummy.png");
    public final static ImageIcon FOOTER_BUTTON_HOVER = createImageIcon("footer" + SEP + "button_dummy_hover.png");
    public final static ImageIcon TEMP_STATUS_GREEN = createImageIcon("navigation2012" + SEP + "temp_status_green.png");
    public final static ImageIcon TEMP_STATUS_GREEN_HOVER = createImageIcon("navigation2012" + SEP + "temp_status_green_hover.png");
    public final static ImageIcon TEMP_STATUS_YELLOW = createImageIcon("navigation2012" + SEP + "temp_status_yellow.png");
    public final static ImageIcon TEMP_STATUS_YELLOW_HOVER = createImageIcon("navigation2012" + SEP + "temp_status_yellow_hover.png");
    public final static ImageIcon TEMP_STATUS_RED = createImageIcon("navigation2012" + SEP + "temp_status_red.png");
    public final static ImageIcon TEMP_STATUS_RED_HOVER = createImageIcon("navigation2012" + SEP + "temp_status_red_hover.png");
    public final static ImageIcon TEMP_STATUS_NONE = createImageIcon("navigation2012" + SEP + "temp_status_none.png");
    public final static ImageIcon TEMP_STATUS_NONE_HOVER = createImageIcon("navigation2012" + SEP + "temp_status_none_hover.png");
    /////////
    // SEARCH
    /////////
    public final static ImageIcon SEARCH_SWITCH_INPUT = createImageIcon("navigation" + SEP + "search" + SEP + "search_switch_input.png");
    public final static ImageIcon SEARCH_SWITCH_ANY = createImageIcon("navigation" + SEP + "search" + SEP + "search_switch_any.png");
    public final static ImageIcon SEARCH_SWITCH_EMPTY = createImageIcon("navigation" + SEP + "search" + SEP + "search_switch_empty.png");
    ////////////////////////
    // BUTTON PANEL GRAPHICS
    ////////////////////////
    public final static ImageIcon BUTTONPANEL_ADD = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_add.png");
    public final static ImageIcon BUTTONPANEL_BACK = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_back.png");
    public final static ImageIcon BUTTONPANEL_CANCEL = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_cancel.png");
    public final static ImageIcon BUTTONPANEL_CLEAR = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_clear.png");
    public final static ImageIcon BUTTONPANEL_CLOSE = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_close.png");
    public final static ImageIcon BUTTONPANEL_CONFIRM = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_confirm.png");
    public final static ImageIcon BUTTONPANEL_DEFAULT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_default.png");
    public final static ImageIcon BUTTONPANEL_DELETE = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_delete.png");
    public final static ImageIcon BUTTONPANEL_DELETE_GLOBAL = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_deleteglobal.png");
    public final static ImageIcon BUTTONPANEL_DELETE_LOCAL = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_deletelocal.png");
    public final static ImageIcon BUTTONPANEL_DOWNLOAD = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_download.png");
    public final static ImageIcon BUTTONPANEL_EDIT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_edit.png");
    public final static ImageIcon BUTTONPANEL_EMPTYHELPER = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_emptyhelper.png");
    public final static ImageIcon BUTTONPANEL_EXECUTE = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_execute.png");
    public final static ImageIcon BUTTONPANEL_EXPORT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_export.png");
    public final static ImageIcon BUTTONPANEL_EXPORT_MULTI = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_export_multi.png");
    public final static ImageIcon BUTTONPANEL_FORWARD = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_forward.png");
    public final static ImageIcon BUTTONPANEL_LOAD = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_load.png");
    public final static ImageIcon BUTTONPANEL_HIDEUNHIDE = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_hideunhide.png");
    public final static ImageIcon BUTTONPANEL_IMPORT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_import.png");
    public final static ImageIcon BUTTONPANEL_MAIL = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_mail.png");
    public final static ImageIcon BUTTONPANEL_MULTISAVE = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_multisave.png");
    public final static ImageIcon BUTTONPANEL_NEWFILE = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_newfile.png");
    public final static ImageIcon BUTTONPANEL_NEXTENTRY = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_nextentry.png");
    public final static ImageIcon BUTTONPANEL_PDFEXPORT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_pdfexport.png");
    public final static ImageIcon BUTTONPANEL_PLACEHOLDER = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_spaceholder.png");
    public final static ImageIcon BUTTONPANEL_PREVENTRY = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_preventry.png");
    public final static ImageIcon BUTTONPANEL_REVERT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_revert.png");
    public final static ImageIcon BUTTONPANEL_SAVE = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_save.png");
    public final static ImageIcon BUTTONPANEL_SAVE_NEXT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_save-next.png");
    public final static ImageIcon BUTTONPANEL_SAVENEW = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_save-new.png");
    public final static ImageIcon BUTTONPANEL_SEARCH = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_search.png");
    public final static ImageIcon BUTTONPANEL_SELECTALL = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_selectall.png");
    public final static ImageIcon BUTTONPANEL_SELECTNONE = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_selectnone.png");
    public final static ImageIcon BUTTONPANEL_SKIP = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_skip.png");
    public final static ImageIcon BUTTONPANEL_SYNC = createImageIcon("navigation" + SEP + "button-panel" + SEP + "icon_sync.png");
    //////////////////////////
    // BOOK SELECTION GRAPHICS
    //////////////////////////
    public final static ImageIcon BOOK_SELECTION_BUTTON_OSSOBOOK = createImageIcon("book_selection" + SEP + "button_ossobook.png");
    public final static ImageIcon BOOK_SELECTION_BUTTON_ARCHAEOBOOK = createImageIcon("book_selection" + SEP + "button_archaeobook.png");
    public final static ImageIcon BOOK_SELECTION_BUTTON_ANTHROBOOK = createImageIcon("book_selection" + SEP + "button_anthrobook.png");
    public final static ImageIcon BOOK_SELECTION_BUTTON_EXCABOOK = createImageIcon("book_selection" + SEP + "button_excabook.png");
    public final static ImageIcon BOOK_SELECTION_BUTTON_DEMOBOOK = createImageIcon("book_selection" + SEP + "button_demobook.png");
    public final static ImageIcon BOOK_SELECTION_BUTTON_INBOOK = createImageIcon("book_selection" + SEP + "button_inbook.png");
    public final static ImageIcon BOOK_SELECTION_BUTTON_PALAEODEPOT = createImageIcon("book_selection" + SEP + "button_palaeodepot.png");
    public final static ImageIcon BOOK_SELECTION_BUTTON_ANTHRODEPOT = createImageIcon("book_selection" + SEP + "button_anthrodepot.png");
    public final static ImageIcon COLLAPSABLE_ICON_DOWN_WHITE = createImageIcon("swingElements" + SEP + "icons" + SEP + "collapsable_icon1_down_white.png");
    public final static ImageIcon COLLAPSABLE_ICON_RIGHT_WHITE = createImageIcon("swingElements" + SEP + "icons" + SEP + "collapsable_icon1_right_white.png");
    public final static ImageIcon COLLAPSABLE_ICON_DOWN_BLACK = createImageIcon("swingElements" + SEP + "icons" + SEP + "collapsable_icon1_down_black.png");
    public final static ImageIcon COLLAPSABLE_ICON_RIGHT_BLACK = createImageIcon("swingElements" + SEP + "icons" + SEP + "collapsable_icon1_right_black.png");
    ///////////////////////////
    // PLAUSIBILITY CHECK ICONS
    ///////////////////////////
    public final static ImageIcon PLAUSIBILITY_CHECK_ERROR = createImageIcon("general" + SEP + "plausibilityCheckIcons" + SEP + "plausibility_check_icon_error.png");
    public final static ImageIcon PLAUSIBILITY_CHECK_CHECK = createImageIcon("general" + SEP + "plausibilityCheckIcons" + SEP + "plausibility_check_icon_check.png");
    ///////////////////////////
    // OTHER SWING/GUI GRAPHICS
    ///////////////////////////
    public final static ImageIcon DELETE_SQUARE = createImageIcon("swingElements" + SEP + "icons" + SEP + "icon_square_delete.png");
    public final static ImageIcon DELETE_SQUARE_HOVERED = createImageIcon("swingElements" + SEP + "icons" + SEP + "icon_square_delete_hover.png");
    public final static ImageIcon DELETE_CIRCLE_RED = createImageIcon("swingElements" + SEP + "icons" + SEP + "delete_cross_circle_red.png");
    public final static ImageIcon DELETE_CIRCLE_GRAY = createImageIcon("swingElements" + SEP + "icons" + SEP + "delete_cross_circle_gray.png");
    public final static ImageIcon EDIT_SQUARE = createImageIcon("swingElements" + SEP + "icons" + SEP + "icon_square_edit.png");
    public final static ImageIcon EDIT_SQUARE_HOVERED = createImageIcon("swingElements" + SEP + "icons" + SEP + "icon_square_edit_hover.png");
    public final static ImageIcon DELETE_CROSS_GRAY = createImageIcon("swingElements" + SEP + "icons" + SEP + "delete_cross_gray.png");
    public final static ImageIcon DELETE_CROSS_BLACK = createImageIcon("swingElements" + SEP + "icons" + SEP + "delete_cross_black.png");
    ////////////////////////////////////
    // NAVIGATION_ICON_SETTINGS GRAPHICS
    ////////////////////////////////////
    /**
     * The ImageIcon holding the graphic of the default setting navigation
     * element.
     */
    public final static ImageIcon SETTINGS_NAV_DEFAULT = createImageIcon("settings" + SEP + "navigation_default.png");
    public final static ImageIcon BUTTON_SELECTED_ICON = createImageIcon("navigation" + SEP + "misc" + SEP + "buttonSelectedIcon.png");
    ////////////////////////////
    // CUSTOM CHECK BOX GRAPHICS
    ////////////////////////////
    // The ImageIcon holding the graphic of the default icon for checkbox
    public final static ImageIcon CHECKBOX_DEFAULT_ICON = createImageIcon("swingElements" + SEP + "checkbox" + SEP + "default_icon.png");
    // The ImageIcon holding the graphic of the double sized checkbox icon when checkbox is disabled
    public final static ImageIcon CHECKBOX_DISABLED = createImageIcon("swingElements" + SEP + "checkbox" + SEP + "disabled.png");
    // The ImageIcon holding the graphic of the double sized checkbox icon when checkbox is disabled and selected
    public final static ImageIcon CHECKBOX_DISABLED_SELECTED = createImageIcon("swingElements" + SEP + "checkbox" + SEP + "disabled_selected.png");
    //////////////////////////////////
    // DOUBLED SIZE CHECK BOX GRAPHICS
    //////////////////////////////////
    // The ImageIcon holding the graphic of the default icon for checkbox
    public final static ImageIcon CHECKBOX_X2_DEFAULT_ICON = createImageIcon("swingElements" + SEP + "checkbox_x2" + SEP + "default_icon.png");
    // The ImageIcon holding the graphic of the double sized checkbox icon when checkbox state is selected
    public final static ImageIcon CHECKBOX_X2_SELECTED = createImageIcon("swingElements" + SEP + "checkbox_x2" + SEP + "selected.png");
    // The ImageIcon holding the graphic of the double sized checkbox icon when checkbox is disabled
    public final static ImageIcon CHECKBOX_X2_DISABLED = createImageIcon("swingElements" + SEP + "checkbox_x2" + SEP + "disabled.png");
    // The ImageIcon holding the graphic of the double sized checkbox icon when checkbox is disabled and selected
    public final static ImageIcon CHECKBOX_X2_DISABLED_SELECTED = createImageIcon("swingElements" + SEP + "checkbox_x2" + SEP + "disabled_selected.png");
    // The ImageIcon holding the graphic of the double sized checkbox icon when checkbox is pressed
    public final static ImageIcon CHECKBOX_X2_PRESSED = createImageIcon("swingElements" + SEP + "checkbox_x2" + SEP + "pressed.png");
    // The ImageIcon holding the graphic of the double sized checkbox icon when checkbox is rollevered
    public final static ImageIcon CHECKBOX_X2_ROLLOVER = createImageIcon("swingElements" + SEP + "checkbox_x2" + SEP + "rollover.png");
    // The ImageIcon holding the graphic of the double sized checkbox icon when checkbox is rollevered and selected
    public final static ImageIcon CHECKBOX_X2_ROLLOVER_SELECTED = createImageIcon("swingElements" + SEP + "checkbox_x2" + SEP + "rollover_selected.png");
    /////////////////////
    // PROJECT MANAGEMENT
    /////////////////////
    public final static ImageIcon PROJECT_BUTTON_NEW = createImageIcon("document-new.png");
    public final static ImageIcon PROJECT_BUTTON_EDIT = createImageIcon("edit-find.png");
    public final static ImageIcon PROJECT_BUTTON_DELETE = createImageIcon("edit-delete.png");
    public final static ImageIcon PROJECT_BUTTON_LOAD = createImageIcon("go-next.png");

    ////////////////////////
    // BUTTON PANEL GRAPHICS
    ////////////////////////
    public static ImageIcon BUTTON_PANEL_BUTTON_GRAY_LEFT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_gray_border.png");
    public static ImageIcon BUTTON_PANEL_BUTTON_GRAY_CENTER = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_gray_center.png");
    public static ImageIcon BUTTON_PANEL_BUTTON_GRAY_RIGHT = createMirroredImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_gray_border.png");
    public static ImageIcon BUTTON_PANEL_BUTTON_GREEN_LEFT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_green_border.png");
    public static ImageIcon BUTTON_PANEL_BUTTON_GREEN_CENTER = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_green_center.png");
    public static ImageIcon BUTTON_PANEL_BUTTON_GREEN_RIGHT = createMirroredImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_green_border.png");
    public static ImageIcon BUTTON_PANEL_BUTTON_RED_LEFT = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_red_border.png");
    public static ImageIcon BUTTON_PANEL_BUTTON_RED_CENTER = createImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_red_center.png");
    public static ImageIcon BUTTON_PANEL_BUTTON_RED_RIGHT = createMirroredImageIcon("navigation" + SEP + "button-panel" + SEP + "button_bg_red_border.png");
    ///////////////////////////
    // FOOTER BUTTON GRAPHICS
    ///////////////////////////
    public final static ImageIcon FOOTER_LOADING_BONES_01 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingbones_01.png");
    public final static ImageIcon FOOTER_LOADING_BONES_02 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingbones_02.png");
    public final static ImageIcon FOOTER_LOADING_BONES_03 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingbones_03.png");
    public final static ImageIcon FOOTER_LOADING_BONES_04 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingbones_04.png");
    public final static ImageIcon FOOTER_LOADING_BONES_05 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingbones_05.png");
    public final static ImageIcon FOOTER_LOADING_BONES_06 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingbones_06.png");
    public final static ImageIcon FOOTER_LOADING_BONES_07 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingbones_07.png");
    public final static ImageIcon FOOTER_LOADING_BONES_08 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingbones_08.png");
    public final static ImageIcon FOOTER_LOADING_BONES_09 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingbones_09.png");
    public final static ImageIcon FOOTER_LOADING_BONES_10 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingbones_10.png");
    public final static ImageIcon FOOTER_LOADING_BONES_11 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingbones_11.png");
    public final static ImageIcon FOOTER_LOADING_BONES_12 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingbones_12.png");
    public final static ImageIcon FOOTER_LOADING_TROWEL_01 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingtrowel_01.png");
    public final static ImageIcon FOOTER_LOADING_TROWEL_02 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingtrowel_02.png");
    public final static ImageIcon FOOTER_LOADING_TROWEL_03 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingtrowel_03.png");
    public final static ImageIcon FOOTER_LOADING_TROWEL_04 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingtrowel_04.png");
    public final static ImageIcon FOOTER_LOADING_TROWEL_05 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingtrowel_05.png");
    public final static ImageIcon FOOTER_LOADING_TROWEL_06 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingtrowel_06.png");
    public final static ImageIcon FOOTER_LOADING_TROWEL_07 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingtrowel_07.png");
    public final static ImageIcon FOOTER_LOADING_TROWEL_08 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingtrowel_08.png");
    public final static ImageIcon FOOTER_LOADING_TROWEL_09 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingtrowel_09.png");
    public final static ImageIcon FOOTER_LOADING_TROWEL_10 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingtrowel_10.png");
    public final static ImageIcon FOOTER_LOADING_TROWEL_11 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingtrowel_11.png");
    public final static ImageIcon FOOTER_LOADING_TROWEL_12 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingtrowel_12.png");
    public final static ImageIcon FOOTER_LOADING_PACMAN_01 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingpacman_01.png");
    public final static ImageIcon FOOTER_LOADING_PACMAN_02 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingpacman_02.png");
    public final static ImageIcon FOOTER_LOADING_PACMAN_03 = createImageIcon("footer" + SEP + "loading_icon" + SEP + "loadingpacman_03.png");

    public final static ImageIcon FOOTER_PROGRESS_BONE_BIG = createImageIcon("footer" + SEP + "progess_bar" + SEP + "bone_big.png");
    public final static ImageIcon FOOTER_PROGRESS_BONE_SMALL = createImageIcon("footer" + SEP + "progess_bar" + SEP + "bone_small.png");
    public final static ImageIcon FOOTER_PROGRESS_BONE_SMALL_GRAY = createImageIcon("footer" + SEP + "progess_bar" + SEP + "bone_small_gray.png");
    public final static ImageIcon FOOTER_PROGRESS_TROWEL_BIG = createImageIcon("footer" + SEP + "progess_bar" + SEP + "trowel_big.png");
    public final static ImageIcon FOOTER_PROGRESS_TROWEL_SMALL = createImageIcon("footer" + SEP + "progess_bar" + SEP + "trowel_small.png");
    public final static ImageIcon FOOTER_PROGRESS_TROWEL_SMALL_GRAY = createImageIcon("footer" + SEP + "progess_bar" + SEP + "trowel_small_gray.png");
    public final static ImageIcon FOOTER_PROGRESS_GUYBRUSH_SMALL = createImageIcon("footer" + SEP + "progess_bar" + SEP + "guybrush_small.png");
    public final static ImageIcon FOOTER_PROGRESS_GUYBRUSH_SMALL_GRAY = createImageIcon("footer" + SEP + "progess_bar" + SEP + "guybrush_small_gray.png");

}
