package de.uni_muenchen.vetmed.xbook.api.gui.stylesheet;

import java.awt.Color;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class ColorsIB extends Colors {

	static {
		/* ******************************************************************* */
		/* ************************** BOOK COLORS **************************** */
		/* ******************************************************************* */
		BOOK_COLOR = new Color(117, 11, 0);
		BOOK_COLOR_DARKER = new Color(82, 8, 0);
		/* ******************************************************************* */
		/* ************************ MANDATORY FIELDS ************************* */
		/* ******************************************************************* */
		INPUT_FIELD_MANDATORY_BACKGROUND = new Color(227, 220, 219);
		INPUT_FIELD_FOCUSED_MANDATORY_BACKGROUND = new Color(220, 209, 208);
		/* ******************************************************************* */
		/* ************************** TABLE LINES **************************** */
		/* ******************************************************************* */
		TABLE_LINE_SELECTED_COLOR_DARK = INPUT_FIELD_FOCUSED_MANDATORY_BACKGROUND;
		TABLE_LINE_SELECTED_COLOR_LIGHT = INPUT_FIELD_FOCUSED_MANDATORY_BACKGROUND;
		TABLE_LINE_HOVERED_COLOR = INPUT_FIELD_MANDATORY_BACKGROUND;
		/* ******************************************************************* */
		/* ************************ GENERAL BACKGROUND *********************** */
		/* ******************************************************************* */
		SIDEBAR_BACKGROUND = TABLE_LINE_SELECTED_COLOR_DARK;
	}

	public static void init() {
		// only necessary for initialisation of book-specific Color classes
	}
}
