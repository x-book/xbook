package de.uni_muenchen.vetmed.xbook.api.framework;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class PDFWriter {

    /**
     * The document of this writer
     */
    private PDDocument doc;
    /**
     * The current page. This can change!
     */
    private PDPage page;
    private PDPageContentStream contentStream;
    /**
     * The current y position in the document
     */
    private int yPos;
    private int indentLeft = 50;
    private int indentBottom = 20;
    private int indentRight = 50;
    private int indentTop = 50;
    private int fontSize = 1;
    private PDFont font = PDType1Font.HELVETICA_BOLD;
    private PDRectangle mediaBox = PDPage.PAGE_SIZE_A4;
    int row = 1;

    /**
     * Creates a new PDF Writer. this just creates a new PAge but doesn't save the file yet. Add content by using the corresponding methods.
     */
    public PDFWriter() {
        try {
            doc = new PDDocument();
        } catch (IOException e) {
            e.printStackTrace();
        }
        yPos = indentTop;
    }

    /**
     * Write standard text to the current page.
     *
     * @param text The text to be written
     * @throws IOException
     */
    public void writeText(String text) throws IOException {
        fontSize = 8;
        write(text, false);
    }

    /**
     * Writes the given text to the current page. Auto creates new pages when page full.
     *
     * @param text The text to be written in the pdf
     * @throws IOException
     */
    private void write(String text, boolean centered) throws IOException {
        text = text.replaceAll("–", "-");
        if (page == null) {
            newPage();
        }
        String[] texts = text.split(" ");
        String line = "";
        float width = page.getMediaBox().getWidth();
        float lineLenght = 0;
        contentStream.setFont(font, fontSize);
        for (String word : texts) {
            while (word.contains("\n")) {
                int indexOfNewLine = word.indexOf("\n");
                String subword = word.substring(0, indexOfNewLine);
                float textwidth = font.getStringWidth(subword) * fontSize / 1000;
                if (lineLenght + textwidth < width - indentRight) {//only add if 
                    line += subword;
                    word = word.substring(indexOfNewLine + 1);

                }
                printLine(line, lineLenght, centered);
                lineLenght = 0;
                line = "";
                row++;

            }
            float textwidth = font.getStringWidth(word) * fontSize / 1000;
            if (lineLenght + textwidth < width - indentRight) {
                line += word + " ";
                lineLenght += textwidth + font.getAverageFontWidth() * fontSize / 1000;

            } else {
                printLine(line, lineLenght, centered);
                line = word + " ";
                row++;
                lineLenght = textwidth + textwidth + font.getAverageFontWidth() * fontSize / 1000;
            }
        }
        printLine(line, lineLenght, centered);


    }

    public void writeList(ArrayList<String> data, boolean centered) throws IOException {
        if (page == null) {
            newPage();
        }

        fontSize = 8;
        float top = yPos + fontSize;
        float bottom;
        float width = page.getMediaBox().getWidth();
        float height = page.getMediaBox().getHeight();
        for (String text : data) {
            String[] texts = text.split(" ");
            String line = "";
            boolean newPage = false;
            float lineLenght = 0;
            contentStream.setFont(font, fontSize);
            boolean first = true;
            for (String word : texts) {
                while (word.contains("\n")) {
                    int indexOfNewLine = word.indexOf("\n");
                    String subword = word.substring(0, indexOfNewLine);
                    float textwidth = font.getStringWidth(subword) * fontSize / 1000;
                    if (lineLenght + textwidth < width - indentRight) {//only add if 
                        line += subword;

                        word = word.substring(indexOfNewLine + 1);

                    }
                    newPage |= printLine(line, lineLenght, centered);

                    bottom = yPos + fontSize;
                    if (first) {
                        halfboxDown(indentLeft, width - indentRight, height - yPos + fontSize, height - bottom);
                        first = false;
                        top = bottom;
                    } else {
                        drawSideLines(indentLeft, width - indentRight, height - yPos + fontSize, height - bottom);
                    }
                    lineLenght = 0;
                    line = "";
                    row++;

                }
                float textwidth = font.getStringWidth(word) * fontSize / 1000;
                if (lineLenght + textwidth < width - indentRight) {

                    line += word + " ";
                    lineLenght += textwidth + font.getAverageFontWidth() * fontSize / 1000;

                } else {
                    newPage |= printLine(line, lineLenght, centered);
                    bottom = yPos + fontSize;
                    if (first) {
                        halfboxDown(indentLeft, width - indentRight, height - yPos + fontSize, height - bottom);
                        first = false;
                    } else {
                        drawSideLines(indentLeft, width - indentRight, height - yPos + fontSize, height - bottom);
                    }
                    line = word + " ";
                    row++;
                    lineLenght = textwidth + font.getAverageFontWidth() * fontSize / 1000;
                    top = bottom;
                }

            }
            newPage |= printLine(line, lineLenght, centered);
            bottom = yPos + fontSize;
            if (first) {
                halfboxDown(indentLeft, width - indentRight, height - yPos + fontSize, height - bottom);
            } else {
                drawSideLines(indentLeft, width - indentRight, height - yPos + fontSize, height - bottom);
            }

            halfboxup(indentLeft, width - indentRight, height - yPos + fontSize, height - bottom);
            top = bottom;

        }

    }

    public void writeHeadline(String headline) throws IOException {
        writeHeadline(headline, true);
    }

    public void writeHeadline(String headline, boolean forceNewPage) throws IOException {
        if (forceNewPage) {
            newPage();
        } else {
            newLine();
        }
        fontSize = 20;
        write(headline, true);
        newLine();
    }

    public boolean printLine(String line, float length, boolean centered) throws IOException {

        PDPage old = page;
        Float y = newLine();

        contentStream.beginText();
        if (centered) {
            float availableSpace = +page.getMediaBox().getWidth() - indentRight - indentLeft;
            float center = indentLeft + availableSpace / 2;
            float begin = center - length / 2;
            contentStream.moveTextPositionByAmount(begin, y);
        } else {
            contentStream.moveTextPositionByAmount(indentLeft, y);
        }

        contentStream.drawString(line);
        contentStream.endText();
        return old != page;
    }

    public float newLine() throws IOException {
        yPos = yPos + (fontSize * 2);
        float y = page.getMediaBox().getHeight() - yPos;
        if (y < indentBottom) {
            newPage();
            yPos += fontSize;
            row = 1;
            y = page.getMediaBox().getHeight() - yPos;
        }
        return y;
    }

    public void newPage() throws IOException {
        if (page == null) {
            page = new PDPage();
            page.setMediaBox(mediaBox);
            doc.addPage(page);

        } else {

            PDRectangle m = page.getMediaBox();
            page = new PDPage();
            page.setMediaBox(m);
            doc.addPage(page);
            contentStream.close();
        }
        contentStream = new PDPageContentStream(doc, page);
        contentStream.setFont(font, fontSize);
        yPos = indentTop;
    }

    public void save(String filename) throws IOException, COSVisitorException {
        // Make sure that the content stream is closed:
        contentStream.close();
        doc.save(filename);
        doc.close();
    }

    public void box(float xLeft, float xRight, float yTop, float yBottom) throws IOException {

        contentStream.drawLine(xLeft, yBottom, xLeft, yTop);
        contentStream.drawLine(xRight, yBottom, xRight, yTop);
        contentStream.drawLine(xRight, yBottom, xLeft, yBottom);
        contentStream.drawLine(xRight, yTop, xLeft, yTop);
    }

    public void halfboxDown(float xLeft, float xRight, float yTop, float yBottom) throws IOException {

        contentStream.drawLine(xLeft, yBottom, xLeft, yTop);
        contentStream.drawLine(xRight, yBottom, xRight, yTop);
        contentStream.drawLine(xRight, yTop, xLeft, yTop);
    }

    public void halfboxup(float xLeft, float xRight, float yTop, float yBottom) throws IOException {

        contentStream.drawLine(xLeft, yBottom, xLeft, yTop);
        contentStream.drawLine(xRight, yBottom, xRight, yTop);

        contentStream.drawLine(xRight, yBottom, xLeft, yBottom);
    }

    public void drawHorizontalLine(float xLeft, float xRight, float y) throws IOException {
        contentStream.drawLine(xRight, y, xLeft, y);
    }

    public void drawSideLines(float xLeft, float xRight, float yTop, float yBottom) throws IOException {

        contentStream.drawLine(xLeft, yTop, xLeft, yBottom);
        contentStream.drawLine(xRight, yTop, xRight, yBottom);

    }

    public static void main(String[] args) {
        try {
            PDFWriter pdf = new PDFWriter();
            ArrayList<String> data = new ArrayList<>();
            for (int i = 0; i < 1; i++) {
                data.add("zabababa das das winas niasd niasd maosdn knaso nom e n gen mansd asin aer dsa erm asd ner das wber asle eros sd war ar a");
            }

            pdf.writeList(data, false);
//            pdf.writeHeadline("Lorem Ipsum", false);
//            String text = " Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
//
//            pdf.writeText(text);
//            pdf.writeHeadline("Neue erfindungen");
//            pdf.writeText("noch so ein text...");
            pdf.save("Test.pdf");


        } catch (IOException | COSVisitorException ex) {
            Logger.getLogger(PDFWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
