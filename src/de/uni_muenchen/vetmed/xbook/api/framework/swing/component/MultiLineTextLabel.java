package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

/**
 * A JTextArea which looks like a normal JLabel but allows an automatical line
 * wrap.
 *
 * @author Daniel Kaltenthaler
 */
public class MultiLineTextLabel extends JTextPane {

    private boolean useCustomSizeCalculation;
    private String textRaw;
    private StyleSheet styleSheet;

    /**
     * Constructor of the MultiLineTexelLabel class.
     *
     * Creates a JTextArea which looks like a normal JLabel but allows an
     * automatical line wrap.
     *
     * @param text The text which should be displayed in the JTextArea.
     * @param useCustomSizeCalculation Deactive the custom size calculation when
     * parameter is <code>true</code>. The default value should be
     * <code>false</code>.
     */
    public MultiLineTextLabel(String text, boolean useCustomSizeCalculation) {
        super();
        HTMLEditorKit kit = new HTMLEditorKit();
        setEditorKit(kit);
        setContentType("text/html");

        styleSheet = kit.getStyleSheet();
        styleSheet.addRule("body {color: #000000; font-family: sans-serif; font-size: 9.5px;}");
        Document doc = kit.createDefaultDocument();
        setDocument(doc);

        setText(text);
        this.useCustomSizeCalculation = useCustomSizeCalculation;

        setEditable(false);
        setEnabled(false);
        setBackground(new Color(240, 240, 240));
        setDisabledTextColor(Color.BLACK);
        setBorder(new EmptyBorder(0, 0, 0, 0));
    }

    /**
     * Constructor of the MultiLineTexelLabel class.
     *
     * Creates a JTextArea which looks like a normal JLabel but allows an
     * automatical line wrap.
     *
     * @param text The text which should be displayed in the JTextArea.
     */
    public MultiLineTextLabel(String text) {
        this(text, false);
    }

    @Override
    public Dimension getPreferredSize() {
        if (useCustomSizeCalculation) {
            return super.getPreferredSize();
        } else {
            Rectangle r;
            try {
                r = modelToView(getDocument().getLength());
            } catch (BadLocationException ex) {
                return super.getPreferredSize();
            }
            if (getParent() == null || getParent().getSize() == null || r == null) {
                return super.getPreferredSize();
            }
            return new Dimension(getParent().getSize().width - 10, r.y + r.height);
        }
    }

    @Override
    public void setForeground(Color fg) {
        super.setForeground(fg);
        setDisabledTextColor(fg);
    }

    /**
     * StyleConstants.ALIGN_CENTER
     *
     * @param alignment
     */
    public void setHorizontalAlignment(int alignment) {
        StyledDocument doc = getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, alignment);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
        setStyledDocument(doc);
    }

    @Override
    public void setText(String text) {
        if (text == null) {
            super.setText("");
            return;
        }
        if (text.startsWith("<html><body>")) {
            super.setText(text);
        } else {
            super.setText("<html><body>" + text + "</body></html>");
        }
        this.textRaw = text;
    }

    public void setStyleSheet(String cssRule) {
        styleSheet.addRule(cssRule);
    }
    
    @Override
    public String getText() {
        return textRaw;
    }

    public String getTextWithHTMLTags() {
        return super.getText();
    }
}
