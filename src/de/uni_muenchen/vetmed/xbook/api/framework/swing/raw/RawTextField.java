package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class RawTextField extends AbstractInputElementFunctions {

    /**
     * The text field object.
     */
    protected JTextField textField;

    public RawTextField(ColumnType columnType) {
        super(columnType);
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        final String s = list.get(columnType);
        textField.setText(s);
    }

    @Override
    public void clear() {
        textField.setText("");
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        list.add(new DataColumn(getText() + "", columnType.getColumnName()));
    }

    @Override
    public String toString() {
        return getText();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textField);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !textField.getText().isEmpty();
    }

    public JTextField getTextField() {
        return textField;
    }

    public String getText() {
        return textField.getText();
    }

    public void setText(String text) {
        textField.setText(text);
    }

    @Override
    public void createField() {
        setLayout(new BorderLayout());
        textField = new JTextField();
        textField.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
        textField.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        textField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (textField.getText().isEmpty()) {
                    textField.setToolTipText(null);
                } else {
                    textField.setToolTipText(textField.getText());
                }
            }
        });

        ComponentHelper.setMaximumInputLength(textField, columnType);
        add(BorderLayout.CENTER, textField);
    }

    @Override
    public void setFocus() {
        textField.requestFocusInWindow();
    }

    @Override
    public String getStringRepresentation() {
        return getText();
    }


}
