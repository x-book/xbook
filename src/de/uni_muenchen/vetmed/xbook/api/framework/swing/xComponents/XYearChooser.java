package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import java.util.Calendar;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class XYearChooser extends JSpinner {

    public XYearChooser() {
        super(new SpinnerNumberModel(Calendar.getInstance().get(Calendar.YEAR), 1000, 3000, 1));
        setEditor(new JSpinner.NumberEditor(this, "#"));
    }

    public void clear() {
        setYear(Calendar.getInstance().get(Calendar.YEAR));
    }

    public String getYear() {
        return ((JSpinner.DefaultEditor) getEditor()).getTextField().getText();
    }

    public void setYear(int year) {
        ((JSpinner.DefaultEditor) getEditor()).getTextField().setText("" + year);
    }
}
