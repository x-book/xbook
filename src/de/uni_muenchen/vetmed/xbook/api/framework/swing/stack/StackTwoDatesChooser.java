package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyListener;
import javax.swing.JComponent;

import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawTwoDatesChooser;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawTwoDatesChooser.LabelType;

/**
 * A stack element that holds a text field.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackTwoDatesChooser extends AbstractStackElement {

    /**
     * The two date chooser object.
     */
    private RawTwoDatesChooser raw;

    private final ColumnType columnTypeDate1;
    private final ColumnType columnTypeDate2;
    private LabelType type;

    /**
     * Constructor.
     *
     * @param columnTypeDate1 The correspondenting column type for first date
     * field.
     * @param columnTypeDate2 The correspondenting column type for second date
     * field.
     * @param labelText The label of the stack element.
     * @param labelWidth The width of the label.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    public StackTwoDatesChooser(ColumnType columnTypeDate1, ColumnType columnTypeDate2, String labelText, LabelType type, Integer labelWidth, SidebarPanel sidebar) {
        super(columnTypeDate1, labelText, labelWidth, sidebar);
        this.columnTypeDate1 = columnTypeDate1;
        this.columnTypeDate2 = columnTypeDate2;
        this.type = type;
        raw.getDateChooser1().addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                setDefaultStyle();
            }
        });
        addCustomSidebarFunctionality(raw.getDateChooser1());
        raw.getDateChooser2().addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                setDefaultStyle();
            }
        });
        addCustomSidebarFunctionality(raw.getDateChooser2());
        setMandatory(columnTypeDate1.isMandatory());
    }

    @Override
    public JComponent getElement() {
        if (raw == null) {
            raw = new RawTwoDatesChooser(columnTypeDate1, columnTypeDate2, LabelType.NONE);
        }
        return raw;
    }

    @Override
    public String getValue() throws IsAMandatoryFieldException {
        return "getValue() not in use";
    }

    @Override
    public void clearInput() {
        raw.clear();
    }

    @Override
    public void setMandatoryStyle() {
    }

    @Override
    public void setErrorStyle() {
    }

    @Override
    public void setDefaultStyle() {
    }

    @Override
    public void setEnabled(boolean b) {
        if (raw != null) {
            raw.getDateChooser1().setEnabled(b);
            raw.getDateChooser2().setEnabled(b);
        }
    }

    @Override
    public void addKeyListener(KeyListener l) {
    }

    @Override
    public void requestFocus() {
        raw.getDateChooser1().requestFocusInWindow();
    }

    @Override
    public void setValue(String value) {
    }

//    @Override
//    public void load(ProjectDataSet data) {
//        raw.load(data);
//    }

    @Override
    public void save(ProjectDataSet data) throws IsAMandatoryFieldException {
        raw.save(data);
    }

}
