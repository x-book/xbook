package de.uni_muenchen.vetmed.xbook.api.framework.swing.table;

import javax.swing.table.DefaultTableModel;

/**
 * The CustomTableModel which extends from the DefaultTableModel class.
 *
 * Garanteed that the table in general and the cells of the table are not
 * editable.
 *
 * @author Daniel Kaltenthaler
 */
public class CustomTableModel extends DefaultTableModel {

    /**
     * Constructor of the CustomTableModel class.
     *
     * Calls the contructor of the DefaultTableModel class and assign the
     * titles of the columns and the numer of elements which should be displayed.
     *
     * @param titles
	 *		The titles of the columns of the table.
     * @param n
	 *		The number of rows which are added by default to the table.
     */
    public CustomTableModel(String[] titles, int n) {
        super(titles, n);
    }

    public CustomTableModel() {
    }


    /**
     * Get the status whether the table is editable or not.
     *
     * @return
	 *		<code>true</code> if the table is editable, <code>false</code> if not.
     */
    public boolean isEditable(){
        return false;
    }

    /**
     * Get the status whether a specific cell of the table is editable or not.
     *
     * @param row
	 *		The number of the row where to check if the cell is editable.
     * @param column
	 *		The number of the column where to check if the cell is editable.
     *
     * @return
	 *		<code>true</code> if the table is editable, <code>false</code> if not.
     */
    public boolean isCellEditable(int row, int column){
        return false;
    }

}
