package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.ITimeChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.documentFilter.TimeDocumentFilter;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;

import javax.swing.*;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.util.ArrayList;

/**
 * A time field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class RawTimeField extends RawTextField implements ITimeChooser {

    public RawTimeField(ColumnType columnType) {
        super(columnType);
    }

    @Override
    public void clear() {
        textField.setText("");
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        list.add(new DataColumn(getText(), columnType.getColumnName()));
    }

    private String convert(int number, int digit) {
        String buffer = String.valueOf(number);
        if (buffer.length() > digit) {
            return buffer;
        }
        while (buffer.length() != digit) {
            buffer = "0" + buffer;
        }
        return buffer;
    }

    @Override
    public String toString() {
        return getText();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textField);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !textField.getText().isEmpty();
    }

    public JTextField getTextField() {
        return textField;
    }

    @Override
    public String getStringRepresentation() {
        return getText();
    }

    /**
     * Check if the time has the correct format "HH:MM". Necessary because values are saved as "HH:MM:SS" in the
     * database!
     *
     * @return <code>true</code> if the time has the correct format,
     * <code>false</code> else.
     */
    public boolean checkInput() {
        String[] akk = textField.getText().split(":", 3);

        String input = "";
        for (String akk1 : akk) {
            input += akk1;
        }
        if (input.isEmpty()) {
            return false;
        }

        // If the input is a number, try to convert the number to a time
        // Examples:
        // - "1234" will be converted to 12:34
        // - "123" will be converted to 1:23
        // - "12" will be converted to 12:00
        // - "1" will be converted to 01:00
        // - invalid time values will be removed
        try {
            int number = Integer.parseInt(input);
            if (number < 0 || number > 2359) {
                throw new NumberFormatException();
            } else {
                String hours = "";
                String minutes = "";
                String numberAsString = number + "";
                if (numberAsString.length() == 1 || numberAsString.length() == 2) {
                    String num = convert(number, 2);
                    hours = num;
                    minutes = "00";
                } else if (numberAsString.length() == 3 || numberAsString.length() == 4) {
                    String num = convert(number, 4);
                    hours = num.substring(0, 2);
                    minutes = num.substring(2, 4);
                }
                if (Integer.parseInt(hours) < 0 || Integer.parseInt(hours) > 23
                        || Integer.parseInt(minutes) < 0 || Integer.parseInt(minutes) > 59) {
                    throw new NumberFormatException();
                }
                textField.setText(hours + ":" + minutes);
            }
        } catch (NumberFormatException nfe) {
            // input values (without the double points) is not a valid time! invalid input!
            textField.setText("");
            setErrorStyle();
            Footer.displayWarning(Loc.get("INVALID_INPUT_IS_NO_TIME", columnType.getDisplayName()));
            return false;
        }
        return true;
    }

    public void setErrorStyle() {
        ComponentHelper.colorAllChildren(this, Colors.INPUT_FIELD_ERROR_BACKGROUND);
    }

    @Override
    public void setTime(String time) {
        String[] s = time.split(":");
        if (s.length < 2) {
            return;
        }
        textField.setText(s[0] + ":" + s[1]);

        // Leave the time field empty if the time is "00:00"
        if (textField.getText().equals("00:00")) {
            textField.setText("");
        }
    }

    @Override
    public String getTime() {
        if (textField.getText().isEmpty()) {
            return "00:00";
        }
        return textField.getText() + ":00";
    }

    @Override
    public void createField() {
        super.createField();
        PlainDocument document = (PlainDocument) textField.getDocument();
        document.setDocumentFilter(new TimeDocumentFilter());
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        final String s = list.get(columnType);
        // since the value is saved different in the database (00:00:00) it must be converted to the correct format
        // (00:00)
        textField.setText(s.substring(0, 5));
    }


}
