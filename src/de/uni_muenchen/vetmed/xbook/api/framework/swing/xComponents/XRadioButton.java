package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import javax.swing.JRadioButton;

import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

/**
 * 
 * @author Eduardo Granado <eduardo.granado@unibas.ch>
 * 
 */
public class XRadioButton extends JRadioButton {
	public XRadioButton(String label) {
		super(label);
		setBackground(Colors.CONTENT_BACKGROUND);
	}
}
