package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import javax.swing.JLabel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

/**
 * An object that inherits from the JLabel class.
 *
 * It has the same functions than a standard JLabel object, but is modified for
 * a default style for xBook.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class XLabel extends JLabel {

    /**
     * Constructor.
     *
     * Set the design for the object.
     *
     * @param text The text that is displayed in the object.
     */
    public XLabel(String text) {
        super(text);

        setBackground(Colors.CONTENT_BACKGROUND);
    }

    /**
     * Constructor.
     *
     * Set the design for the object.
     *
     * @param text The text that is displayed in the object.
     * @param horizontalAlignment The text alignment.
     */
    public XLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);

        setBackground(Colors.CONTENT_BACKGROUND);
    }

}
