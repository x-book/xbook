package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import com.toedter.calendar.JDateChooser;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

import javax.swing.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * A stack element that holds a date chooser.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackDateChooser extends AbstractStackElement {

    /**
     * The date chooser object.
     */
    private JDateChooser dateChooser;
    /**
     * Object to format the date.
     */
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public StackDateChooser(ColumnType columnType, SidebarPanel sidebar) {
        this(columnType, columnType.getDisplayName(), null, sidebar);
    }

    /**
     * Constructor.
     *
     * @param columnType The correspondenting column type.
     * @param labelText  The label of the stack element.
     * @param labelWidth The width of the label.
     * @param sidebar    The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
     */
    public StackDateChooser(ColumnType columnType, String labelText, Integer labelWidth, SidebarPanel sidebar) {
        super(columnType, labelText, labelWidth, sidebar);

        dateChooser.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                setDefaultStyle();
            }
        });
        addCustomSidebarFunctionality(dateChooser);
    }

    /**
     * Constructor.
     *
     * @param labelText  The label of the stack element.
     * @param labelWidth The width of the label.
     * @param sidebar    The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
     */
    public StackDateChooser(String labelText, Integer labelWidth, SidebarPanel sidebar) {
        this(null, labelText, labelWidth, sidebar);
    }

    /**
     * Constructor.
     *
     * @param labelText   The label of the stack element.
     * @param labelWidth  The width of the label.
     * @param sidebarText The text that will be displayed into the sidebar.
     */
    public StackDateChooser(String labelText, Integer labelWidth, String... sidebarText) {
        this(labelText, labelWidth, new SidebarPanel(sidebarText));
    }

    /**
     * Constructor.
     *
     * @param labelText  The label of the stack element.
     * @param labelWidth The width of the label.
     */
    public StackDateChooser(String labelText, Integer labelWidth) {
        this(labelText, labelWidth, (SidebarPanel) null);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param sidebar   The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
     */
    public StackDateChooser(String labelText, SidebarPanel sidebar) {
        this(labelText, null, sidebar);
    }

    /**
     * Constructor.
     *
     * @param labelText   The label of the stack element.
     * @param sidebarText The text that will be displayed into the sidebar.
     */
    public StackDateChooser(String labelText, String... sidebarText) {
        this(labelText, null, sidebarText);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     */
    public StackDateChooser(String labelText) {
        this(labelText, null, (SidebarPanel) null);
    }

    @Override
    public JComponent getElement() {
        dateChooser = new JDateChooser();
        return dateChooser;
    }

    @Override
    public String getValue() {
        if (dateChooser.getDate() == null) {
            return "0000-00-00";
        } else {
            return dateFormat.format(dateChooser.getDate());
        }
    }


    @Override
    public void clearInput() {
        dateChooser.setDate(null);
    }

    @Override
    public void setMandatoryStyle() {
        if (dateChooser != null) {
            dateChooser.setBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        }
    }

    @Override
    public void setErrorStyle() {
    }

    @Override
    public void setDefaultStyle() {
    }

    @Override
    public void setEnabled(boolean b) {
        if (dateChooser != null) {
            dateChooser.setEnabled(b);
        }
    }

    @Override
    public void addKeyListener(KeyListener l) {
        dateChooser.addKeyListener(l);
    }

    @Override
    public void requestFocus() {
        dateChooser.requestFocusInWindow();
    }

    @Override
    public void setValue(String value)  {
        try {
            setDate(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    public void setDate(String date) throws ParseException {
        dateChooser.setDate(dateFormat.parse(date));
    }
}
