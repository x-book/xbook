package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.settingPanel;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.languageChooser.LanguageChooser;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XSubTitle;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.fields.TimeSelection;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.settings.fields.TwoRadioButtons;

import javax.swing.*;
import java.awt.*;

/**
 * An abstract class that inherits from a JPanel which that is used to display the content of a setting panel. It
 * garantees that neccessary or important methods are available.
 *
 * @author Daniel Kaltenthaler
 */
public abstract class AbstractSettingPanel extends JPanel {

    /**
     * The default background color.
     */
    private final Color DEFAULT_BACKGROUND_COLOR = Color.WHITE;

    /**
     * Saves the entered values to the properties.
     */
    public abstract void save();

    /**
     * Resets the input fields with the current data before loading the setting screen.
     */
    public abstract void reset();

    /**
     * Resets the input fields with the default data of the properties.
     */
    public abstract void setDefaultValues();

    /**
     * Adds a title bar to the setting panel.
     *
     * @param text           The text to display in the title bar.
     * @param addBorderToTop The status if a border should be add the the top of the element. Use <code>false</code> to
     *                       use no border on top, e.g. for the first line.
     * @return The JLabel object.
     */
    protected XSubTitle addTitleBar(String text, boolean addBorderToTop) {
        XSubTitle label = createTitleBar(text);
        if (addBorderToTop) {
            add(ComponentHelper.wrapComponent(label, 10, 10, 10, 10));
        } else {
            add(ComponentHelper.wrapComponent(label, 0, 10, 10, 10));
        }
        return label;
    }

    /**
     * Adds a title bar to the setting panel.
     *
     * @param text The text to display in the title bar.
     * @returns The JLabel object.
     */
    protected XSubTitle addTitleBar(String text) {
        return addTitleBar(text, true);
    }

    protected XSubTitle createTitleBar(String text) {
        return new XSubTitle(text);
    }

    /**
     * Adds a text block to the setting panel.
     *
     * @param background The background color.
     * @param text       The text to display in the text block.
     * @returns The MultiLineTextLabel object.
     */
    protected MultiLineTextLabel addTextBlock(Color background, String text) {
        MultiLineTextLabel label = createTextBlock(background, text);
        label.setFont(Fonts.FONT_STANDARD_PLAIN);
        add(ComponentHelper.wrapComponent(label, 0, 10, 10, 10));
        return label;
    }

    /**
     * Adds a title bar to the setting panel.
     *
     * @param text The text to display in the text block.
     * @return The MultiLineTextLabel object.
     */
    protected MultiLineTextLabel addTextBlock(String text) {
        return addTextBlock(DEFAULT_BACKGROUND_COLOR, text);
    }

    protected MultiLineTextLabel createTextBlock(Color background, String text) {
        MultiLineTextLabel textLabel = new MultiLineTextLabel(text);
        textLabel.setFont(Fonts.FONT_STANDARD_PLAIN);
        textLabel.setBackground(background);
        return textLabel;
    }

    protected MultiLineTextLabel createTextBlock(String text) {
        return createTextBlock(DEFAULT_BACKGROUND_COLOR, text);
    }

    /**
     * Adds a text field to the setting panel.
     *
     * @param text The text to display in the text field.
     * @returns The MultiLineTextLabel object.
     */
    protected JTextField addTextField(String text) {
        return addTextField(DEFAULT_BACKGROUND_COLOR, text);
    }

    /**
     * Adds a text field to the setting panel.
     *
     * @param background The background color.
     * @param text       The text to display in the text field.
     * @returns The JTextField object.
     */
    protected JTextField addTextField(Color background, String text) {
        JTextField textField = createTextField(background, text);
        add(ComponentHelper.wrapComponent(textField, 0, 10, 10, 10));
        return textField;
    }

    protected JTextField createTextField(Color background, String text) {
        JTextField textField = new JTextField(text);
        textField.setBackground(background);
        return textField;
    }

    protected JTextField createTextField(String text) {
        return createTextField(DEFAULT_BACKGROUND_COLOR, text);
    }

    protected JPasswordField addPasswordField(Color background) {
        JPasswordField label = createPasswordField(background);
        add(ComponentHelper.wrapComponent(label, 0, 10, 10, 10));
        return label;
    }

    protected JPasswordField addPasswordField() {
        return addPasswordField(DEFAULT_BACKGROUND_COLOR);
    }

    protected JPasswordField createPasswordField(Color background) {
        JPasswordField field = new JPasswordField();
        field.setBackground(background);
        return field;
    }

    protected JPasswordField createPasswordField() {
        return createPasswordField(DEFAULT_BACKGROUND_COLOR);
    }

    /**
     * Adds a checkbox to the setting panel.
     *
     * @param background The background color.
     * @param text       The text to display in the text block.
     * @returns The JCheckBox object.
     */
    protected JCheckBox addCheckBox(Color background, String text) {
        JCheckBox check = new JCheckBox(text);
        check.setBackground(background);
        add(ComponentHelper.wrapComponent(check, 0, 10, 10, 10));
        return check;
    }

    /**
     * Adds a checkbox to the setting panel.
     *
     * @param text The text to display in the text block.
     * @returns The JCheckBox object.
     */
    protected JCheckBox addCheckBox(String text) {
        return addCheckBox(DEFAULT_BACKGROUND_COLOR, text);
    }

    /**
     * Adds a language chooser to the setting panel.
     *
     * @param background The background color.
     * @return The language chooser object.
     */
    protected LanguageChooser addLanguageSelector(Color background) {
        JPanel wrapper = new JPanel(new BorderLayout());
        wrapper.setBackground(background);
        LanguageChooser languageChooser = new LanguageChooser(true);
        languageChooser.setSize(300, 40);
        languageChooser.setPreferredSize(new Dimension(300, 40));
        wrapper.add(BorderLayout.CENTER, languageChooser);
        add(ComponentHelper.wrapComponent(wrapper, 0, 10, 10, 10));
        return languageChooser;
    }

    /**
     * Adds a language chooser to the setting panel.
     *
     * @return The language chooser object.
     */
    protected LanguageChooser addLanguageSelector() {
        return addLanguageSelector(DEFAULT_BACKGROUND_COLOR);
    }

    /**
     * Adds a language chooser to the setting panel.
     *
     * @param background The background color.
     * @return The combo box object.
     */
    protected JComboBox addComboBox(Color background) {
        JPanel wrapper = new JPanel(new BorderLayout());
        wrapper.setBackground(background);
        JComboBox combo = new JComboBox();
        combo.setSize(300, 40);
        combo.setPreferredSize(new Dimension(300, 40));
        wrapper.add(BorderLayout.CENTER, combo);
        add(ComponentHelper.wrapComponent(wrapper, 0, 10, 10, 10));
        return combo;
    }

    /**
     * Adds a language chooser to the setting panel.
     *
     * @return The combo box object.
     */
    protected JComboBox addComboBox() {
        return addComboBox(DEFAULT_BACKGROUND_COLOR);
    }

    protected TwoRadioButtons createTwoRadioButtons(Color background, String labelYes, String labelNo) {
        return new TwoRadioButtons(background, labelNo, labelYes);
    }

    /**
     * Adds a language chooser to the setting panel.
     *
     * @param background The background color.
     * @param labelYes   The label for the "yes" option.
     * @param labelNo    The label for the "no" option.
     * @return The TwoRadioButtons object.
     */
    protected TwoRadioButtons addTwoRadioButtons(Color background, String labelYes, String labelNo) {
        TwoRadioButtons twoButtons = new TwoRadioButtons(background, labelNo, labelYes);
        add(ComponentHelper.wrapComponent(twoButtons, 0, 10, 10, 10));
        return twoButtons;
    }

    /**
     * Adds a language chooser to the setting panel.
     *
     * @param labelYes The label for the "yes" option.
     * @param labelNo  The label for the "no" option.
     * @return The TwoRadioButtons object.
     */
    protected TwoRadioButtons addTwoRadioButtons(String labelYes, String labelNo) {
        return addTwoRadioButtons(DEFAULT_BACKGROUND_COLOR, labelYes, labelNo);
    }

    /**
     * Adds a language chooser to the setting panel.
     *
     * @return The TwoRadioButtons object.
     */
    protected TwoRadioButtons addTwoRadioButtons() {
        return addTwoRadioButtons(DEFAULT_BACKGROUND_COLOR, "Yes", "No");
    }


    protected TimeSelection addTimeSelection() {
        return addTimeSelection(DEFAULT_BACKGROUND_COLOR);
    }

    /**
     * Adds a time selection to the setting panel.
     *
     * @return The TimeSelection object.
     */
    protected TimeSelection addTimeSelection(Color background) {
        TimeSelection time = new TimeSelection(background);
        add(ComponentHelper.wrapComponent(time, 0, 10, 10, 10));
        return time;
    }

}
