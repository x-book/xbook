package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton.Style;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;

import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;

/**
 * A stack element that allows adding two buttons.
 */
public class StackTwoButtons extends AbstractStackElement {

    /**
     * The object of the first button.
     */
    private XImageButton firstButton;
    /**
     * The object of the second button.
     */
    private XImageButton secondButton;

    /**
     * Constructor.
     *
     * @param iconFirst
     * @param labelFirst           The label of the first button.
     * @param listenerFirstButton  The action listener for the first button.
     * @param iconSecond
     * @param labelSecond          The label of the second button.
     * @param listenerSecondButton The action listener for the second button.
     * @param labelWidth           The width of the label.
     */
    public StackTwoButtons(ImageIcon iconFirst, String labelFirst, ActionListener listenerFirstButton, ImageIcon iconSecond, String labelSecond, ActionListener listenerSecondButton, Integer labelWidth) {
        super("", labelWidth);
        if (iconFirst != null) {
            firstButton.setIcon(iconFirst);
        }
        if (labelFirst != null && !labelFirst.isEmpty()) {
            firstButton.setLabel(labelFirst);
        }
        firstButton.addActionListener(listenerFirstButton);
        if (iconFirst != null) {
            secondButton.setIcon(iconFirst);
        }
        if (labelSecond != null && !labelSecond.isEmpty()) {
            secondButton.setLabel(labelSecond);
        }
        secondButton.addActionListener(listenerSecondButton);

        setHeight(Images.BUTTON_PANEL_BUTTON_LEFT.getIconHeight());

        revalidate();
        repaint();
    }

    /**
     * Constructor.
     *
     * @param labelFirst           The label of the first button.
     * @param listenerFirstButton  The action listener for the first button.
     * @param labelSecond          The label of the second button.
     * @param listenerSecondButton The action listener for the second button.
     */
    public StackTwoButtons(String labelFirst, ActionListener listenerFirstButton, String labelSecond, ActionListener listenerSecondButton) {
        this(null, labelFirst, listenerFirstButton, null, labelSecond, listenerSecondButton, null);
    }

    /**
     * Constructor.
     *
     * @param iconFirst           The label of the first button.
     * @param listenerFirstButton  The action listener for the first button.
     * @param iconSecond          The label of the second button.
     * @param listenerSecondButton The action listener for the second button.
     */
    public StackTwoButtons(ImageIcon iconFirst, ActionListener listenerFirstButton, ImageIcon iconSecond, ActionListener listenerSecondButton) {
        this(iconFirst, null, listenerFirstButton, iconSecond, null, listenerSecondButton, null);
    }

    @Override
    public String getValue() {
        return null;
    }


    @Override
    public JComponent getElement() {
        JPanel panel = new JPanel(new BorderLayout());

        firstButton = new XImageButton();
        secondButton = new XImageButton();

        firstButton.setWidth(200);
        secondButton.setWidth(140);

        panel.add(BorderLayout.CENTER, ComponentHelper.wrapComponent(firstButton, 0, 5, 0, 0));
        panel.add(BorderLayout.EAST, secondButton);

        return panel;
    }

    @Override
    public void clearInput() {
        // not in use
    }

    @Override
    public void setMandatoryStyle() {
    }

    @Override
    public void setErrorStyle() {
    }

    @Override
    public void setDefaultStyle() {
    }

    @Override
    public void setEnabled(boolean b) {
    }

    @Override
    public void addKeyListener(KeyListener l) {
    }

    @Override
    public void setValue(String value) {
        // not in use
    }

    public void setStyleFirstButton(Style style) {
        firstButton.setStyle(style);
    }

    public void setStyleSecondButton(Style style) {
        secondButton.setStyle(style);
    }

    public XImageButton getFirstButton() {
        return firstButton;
    }

    public XImageButton getSecondButton() {
        return secondButton;
    }
    
}
