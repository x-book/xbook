package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.NotColorableJLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractSynchronisationController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * An input field that is used to upload files to the server of xBook. This field uses a defined "category" that defines
 * the categoryId of the folder structure.
 * <p>
 * Therefore there are three states of the field.
 * <p>
 * State 1 means that a file has been chosen, but the data set has not been save. In this case, the field displays the
 * selected categoryId and file name. This is always the state if a file has been selected by the file chooser.
 * <p>
 * State 2 means that a file is saved in the temporary file folder of xbook. In this case, the field displays the file
 * name (the categoryId is not necessary, because it is the general file folder). This is the state after the entry has
 * been saved and as long as the file has not been synchronized.
 * <p>
 * State 3 means that a file has been synced and is saved on the server. The saving in the temporary file is not
 * necessary any more. In this case, the field displays the unique file name that is saved on the server. This is always
 * the state after the entry has been synced and has not been changed since the synchronization.
 * <p>
 * As soon as another file has been selected from the file chooser or has inserted manually in the textfield, the field
 * is always reverted to state 1.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
// TODO Make this inherated from RawFileUploadField
public class RawFileUploadWithCategoryField extends RawFileNameChooser {
    private Log LOGGER = LogFactory.getLog(RawFileUploadWithCategoryField.class);

    /**
     * The four available status for the files.
     */
    protected enum Status {
        NOT_SET, NOT_SAVED, SAVED, SYNCED
    }

    /**
     * The available text labels.
     */
    protected enum Label {
        FILE, IMAGE
    }

    protected NotColorableJLabel labelStatus;
    protected XButton buttonClear;
    protected XButton buttonOpen;

    protected JPanel generalWrapper;
    protected JPanel eastButtonWrapper;
    protected JPanel labelWrapper;
    protected JPanel textFieldWrapper;

    protected Status currentStatus = Status.NOT_SET;
    protected Label currentLabel;

    protected ApiControllerAccess controller;

    protected int[] displayedCategoryIds;

    /**
     * The corresponding column type for the information whether the file has to be synchronized or not.
     */
    protected ColumnType columnTypeFileMustBeSynced;
    /**
     * The corresponding column type for the categoryId where the file has to be saved.
     */
    protected ColumnType columnTypeCategory;
    /**
     * The corresponding column type for the information of the file extension.
     */
    protected final ColumnType columnTypeExtension;

    /**
     * Constructor.
     *
     * @param columnTypeFileName         The corresponding column type for file name.
     * @param columnTypeExtension        The corresponding column type for file extension.
     * @param columnTypeFileMustBeSynced The corresponding column type for the information whether the file has to be
     *                                   synchronized or not.
     * @param columnTypeCategory         The corresponding column type for category information.
     * @param displayedCategoryId        The ID of the categories that should be selectable in the category selection.
     * @param fileFilter                 The file filter options.
     */
    public RawFileUploadWithCategoryField(ApiControllerAccess controller, ColumnType columnTypeFileName,
                                          ColumnType columnTypeExtension,
                                          ColumnType columnTypeFileMustBeSynced, ColumnType columnTypeCategory,
                                          int displayedCategoryId,
                                          ArrayList<FileNameExtensionFilter> fileFilter) {
        super(columnTypeFileName, fileFilter, Mode.FILE_AND_DIRECTORY);
        this.columnTypeExtension = columnTypeExtension;
        this.columnTypeFileMustBeSynced = columnTypeFileMustBeSynced;
        this.columnTypeCategory = columnTypeCategory;
        this.displayedCategoryIds = new int[]{displayedCategoryId};
        this.controller = controller;
    }

    /**
     * Constructor. Should only be used from hertitages objects.
     *
     * @param columnTypeFileName         The corresponding column type for file name.
     * @param columnTypeExtension        The corresponding column type for file extension.
     * @param columnTypeFileMustBeSynced The corresponding column type for the information whether the file has to be
     *                                   synchronized or not.
     * @param columnTypeCategory         The corresponding column type for category information.
     * @param displayedCategoryIds       The IDs of the categories that should be selectable in the category selection.
     * @param fileFilter                 The file filter options.
     */
    public RawFileUploadWithCategoryField(ApiControllerAccess controller, ColumnType columnTypeFileName,
                                          ColumnType columnTypeExtension,
                                          ColumnType columnTypeFileMustBeSynced, ColumnType columnTypeCategory,
                                          int[] displayedCategoryIds,
                                          ArrayList<FileNameExtensionFilter> fileFilter) {
        super(columnTypeFileName, fileFilter, Mode.FILE_AND_DIRECTORY);
        this.columnTypeExtension = columnTypeExtension;
        this.columnTypeFileMustBeSynced = columnTypeFileMustBeSynced;
        this.columnTypeCategory = columnTypeCategory;
        this.displayedCategoryIds = displayedCategoryIds;
        this.controller = controller;
    }

    @Override
    public void createField() {
        this.createField(false);
    }

    public void createField(boolean isCallFromInheritagesObject) {
        super.createField();

        textFieldWrapper = new JPanel(new BorderLayout());
        textField.setPreferredSize(new Dimension(100, 32));
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                onInputChanged();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                onInputChanged();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                onInputChanged();
            }
        });
        textField.setDisabledTextColor(Color.BLACK);
        textField.setEnabled(false);

        // init the clear button
        buttonClear = new XButton("X");
        buttonClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clear();
            }
        });

        // init the open button
        buttonOpen = new XButton(Loc.get("OPEN"));
        buttonOpen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                open();
            }
        });

        // init the select button
        buttonView.setText(Loc.get("SELECT"));

        // init the status label
        labelWrapper = new JPanel(new BorderLayout());
        labelStatus = new NotColorableJLabel();
        labelStatus.setOpaque(true);
        labelStatus.setVisible(false);

        // combine everything in the panel
        textFieldWrapper.add(BorderLayout.CENTER, textField);
        labelWrapper.add(BorderLayout.WEST, labelStatus);

        eastButtonWrapper = new JPanel(new FlowLayout());
        eastButtonWrapper.add(buttonClear);
        eastButtonWrapper.add(buttonOpen);
        eastButtonWrapper.add(buttonView);

        generalWrapper = new JPanel(new BorderLayout());
        generalWrapper.add(BorderLayout.WEST, labelWrapper);
        generalWrapper.add(BorderLayout.CENTER, textFieldWrapper);
        generalWrapper.add(BorderLayout.EAST, eastButtonWrapper);
        add(BorderLayout.CENTER, generalWrapper);

        if (!isCallFromInheritagesObject) {
            currentStatus = Status.NOT_SET;
            updateToStatus(currentStatus);
        }
    }

    @Override
    protected void onFileChoosen(File lastLoadedDirectory, File lastLoadedFile) throws FileTooLargeException {
        textField.setText(textField.getText().replace("\\", "/"));


        onInputChanged();
    }

    /**
     * Actions that are called after the input, that means the value of the text field has changed.
     */
    protected void onInputChanged() {
        String text = textField.getText();
        if (text.isEmpty()) {
            updateToStatus(Status.NOT_SET);
        } else {
            updateToStatus(Status.NOT_SAVED);
        }
    }

    @Override
    public void clear() {
        super.clear();

        // nothing to clear in catergory

        onInputChanged();
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());

        // save category
        list.add(new DataColumn(displayedCategoryIds[0], columnTypeCategory.getColumnName()));
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());

        // category must not be loaded, since it is always the same value
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = super.getMyFocusableComponents();
        comp.add(labelStatus);
        comp.add(buttonClear);
        comp.add(buttonClear.getButton());
        comp.add(buttonOpen);
        comp.add(buttonOpen.getButton());
        return comp;
    }

    @Override
    public void setText(String text) {
        super.setText(text);
    }

    private boolean isValidFile(String file) {
        return new File(file).exists();
    }

    private void open() {


        if (currentStatus == Status.NOT_SAVED) {
            String filePath = textField.getText();
            if (isValidFile(filePath)) {
                try {
                    Desktop.getDesktop().open(new File(filePath));
                } catch (IOException e) {
                    LOGGER.error(e, e);
                    if (currentLabel == Label.FILE) {
                        Footer.displayError(Loc.get("ERROR_WHILE_LOADING_FILE"));
                    } else {
                        Footer.displayError(Loc.get("ERROR_WHILE_LOADING_IMAGE"));
                    }
                }
            } else {
                LOGGER.debug("Errored file categoryId: " + filePath);
                Footer.displayError(Loc.get("ERROR_DEFINED_PATH_DOES_NOT_EXIST"));
            }
            return;
        }

        if (controller instanceof AbstractSynchronisationController) {
            AbstractSynchronisationController abstractSynchronisationController =
                    (AbstractSynchronisationController) controller;
            if (currentStatus == Status.SAVED) {
                try {
                    abstractSynchronisationController.openNotUploadedFile(textField.getText());
                } catch (NotLoggedInException | NoRightException | StatementNotExecutedException | NotConnectedException | IOException | NotLoadedException e) {
                    e.printStackTrace();
                }
            } else if (currentStatus == Status.SYNCED) {
                try {
                    abstractSynchronisationController.downloadAndOpenFile(textField.getText());
                } catch (NotLoadedException | NotLoggedInException | StatementNotExecutedException | NoRightException | NotConnectedException | IOException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    /**
     * Updates the status to a new value. Additinally adjusts the display of the components (buttons and labels).
     *
     * @param newStatus The new status to set.
     */
    protected void updateToStatus(Status newStatus) {
        // updates the visibility of the elements
        updateComponentVisibility(newStatus);
        // finally update the variable with the status
        currentStatus = newStatus;
    }

    protected void updateComponentVisibility(Status newStatus) {
        if (newStatus == Status.NOT_SET) {
            labelStatus.setVisible(false);
            labelStatus.setText("");
            labelStatus.setBackground(new Color(222, 222, 222));
            buttonClear.setVisible(false);
            buttonOpen.setVisible(false);
            textFieldWrapper.setVisible(true);
            textField.setVisible(true);
        } else if (newStatus == Status.NOT_SAVED) {
            labelStatus.setVisible(false);
            labelStatus.setText("");
            labelStatus.setBackground(new Color(222, 222, 222));
            buttonClear.setVisible(true);
            buttonOpen.setVisible(true);
            textFieldWrapper.setVisible(true);
            textField.setVisible(true);
        } else if (newStatus == Status.SAVED) {
            labelStatus.setVisible(true);
            if (currentLabel == Label.FILE) {
                labelStatus.setText("  " + Loc.get("FILE_SAVED_LOCALLY") + "  ");
            } else {
                labelStatus.setText("  " + Loc.get("IMAGE_SAVED_LOCALLY") + "  ");
            }
            labelStatus.setBackground(new Color(255, 204, 0));
            buttonClear.setVisible(true);
            buttonOpen.setVisible(true);
            textFieldWrapper.setVisible(false);
            textField.setVisible(false);
        } else if (newStatus == Status.SYNCED) {
            labelStatus.setVisible(true);
            if (currentLabel == Label.FILE) {
                labelStatus.setText("  " + Loc.get("FILE_SYNCED") + "  ");
            } else {
                labelStatus.setText("  " + Loc.get("IMAGE_SYNCED") + "  ");
            }
            labelStatus.setBackground(new Color(85, 194, 70));
            buttonClear.setVisible(true);
            buttonOpen.setVisible(true);
            textFieldWrapper.setVisible(false);
            textField.setVisible(false);
        }
    }

    @Override
    public void actionOnFocusLost() {
        super.actionOnFocusLost();
        if (currentStatus == Status.NOT_SAVED) {
            if (!isValidPath()) {
                clear();
                Footer.displayError(Loc.get("ERROR_DEFINED_PATH_DOES_NOT_EXIST"));
            }
        }
    }


    public NotColorableJLabel getLabelStatus() {
        return labelStatus;
    }

    public boolean isValidPath() {
        if (currentStatus == Status.NOT_SAVED) {
            return new File(textField.getText()).exists();
        }
        return true;
    }

    @Override
    public void setEnabled(boolean enabled) {
        textField.setEnabled(false);
    }

    /**
     * Checks if a specific key (an ID) is available in the available display category ids.
     *
     * @param keyToCheck The key to check.
     * @return The stuatus if the key was found in the available display category ids.
     */
    protected boolean containsKey(String keyToCheck) {
        for (int id : displayedCategoryIds) {
            if ((id + "").equals(keyToCheck)) {
                return true;
            }
        }
        return false;
    }

}
