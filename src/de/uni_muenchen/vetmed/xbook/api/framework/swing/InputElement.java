package de.uni_muenchen.vetmed.xbook.api.framework.swing;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XLabel;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.UpdateableEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElementWrapper;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import static de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement.BORDER_AROUND;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class InputElement extends JPanel implements IInputElement, IInputElementWrapper {

    /**
     * The different styles for the input element that are possible.
     */
    public enum Style {

        INPUT_FIELD, STACK
    }

    /**
     * An outter wrapper panel that holds all elements of the input element.
     */
    protected JPanel outerWrapper;
    /**
     * An inner wrapper panel that holds all elements of the input element.
     */
    private JPanel innerWrapper;
    /**
     * The current mode of this input element.
     */
    protected final Style style;
    /**
     * The displayed label to describe the input element.
     */
    private final String label;

    /**
     * The number of horizontal grid elements that are used for this input element.
     */
    private int gridX = 1;
    /**
     * The number of vertical grid elements that are used for this input element.
     */
    private int gridY = 1;

    /**
     * A JLabel that set the input field invisible when clicking on it.
     */
    private JLabel hideLabel;

    /**
     * A JLabel that set the input field to deleted when clicking on it.
     */
    private JLabel deleteLabel;
    /**
     * A JCheckBox to check when the input should not be removed from the input field when saving.
     */
    protected JCheckBox checkRememberValue;
    protected XLabel firstLineLabel;

    /**
     * The element of the input element that is currently focussed.
     */
    protected Component currentFocused;

    protected static AbstractMainFrame mainFrame;

    protected JPanel rawWrapper;
    private AbstractInputElementFunctions raw;
    /**
     * The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
     */
    protected SidebarPanel sidebar;
    /**
     * The correspondenting entry object.
     */
    protected UpdateableEntry entry;

    protected boolean isMultiEdit = false;

    public InputElement(Style style, String label) {
        this.label = label;
        this.style = style;

        createFieldWrapper();
    }

    public InputElement(String label) {
        this(Style.INPUT_FIELD, label);
    }

    public void setToMultiEditStyle() {
        isMultiEdit = true;
    }

    /**
     * Creates the basic wrapper and functionality of the input field (in general).
     */
    protected void createFieldWrapper() {
        setBorder(new EmptyBorder(BORDER_AROUND, BORDER_AROUND, BORDER_AROUND, BORDER_AROUND));
        setBackground(Colors.CONTENT_BACKGROUND);

        // wrapper
        outerWrapper = new JPanel(new BorderLayout());
        outerWrapper.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_BORDER, 1));
        outerWrapper.setBackground(Colors.INPUT_FIELD_BACKGROUND);
        calculateSize();
        add(outerWrapper);

        innerWrapper = new JPanel(new BorderLayout());
        innerWrapper.setBackground(Colors.INPUT_FIELD_BACKGROUND);
        innerWrapper.setBorder(new EmptyBorder(4, 3, 4, 3));
        outerWrapper.add(BorderLayout.CENTER, innerWrapper);

        switch (style) {
            case STACK:
                outerWrapper.add(BorderLayout.WEST, createFirstLineWithCheckBox());
                break;
            case INPUT_FIELD:
            default:
                outerWrapper.add(BorderLayout.NORTH, createFirstLineWithCheckBox());
                break;
        }

        rawWrapper = new JPanel(new BorderLayout());
        innerWrapper.add(rawWrapper);

        colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
    }

    /**
     * Creates the label wrapper.
     * <p>
     * In input field mode, it is the top line above the input field itself. It holds an possibility to remember the
     * value when clearing the field, the label of the input field and the possibility to hide the field.
     * <p>
     * In stack mode, only the label is displayed.
     *
     * @return The JPanel holding the label line.
     */
    protected JPanel createFirstLineWithCheckBox() {
        JPanel firstLine = new JPanel(new BorderLayout());
        firstLine.setBackground(Colors.INPUT_FIELD_BACKGROUND);

        if (style == Style.INPUT_FIELD) {
            checkRememberValue = new JCheckBox("");
            checkRememberValue.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                }
            });
            checkRememberValue.setBackground(Colors.INPUT_FIELD_BACKGROUND);
            checkRememberValue.setFocusable(false);
            firstLine.add(BorderLayout.WEST, checkRememberValue);
        }

        firstLineLabel = new XLabel(getFormattedLabelText());
        if (style == Style.STACK) {
            JPanel labelWrapper = new JPanel();
            firstLineLabel.setHorizontalAlignment(SwingConstants.RIGHT);
            firstLineLabel.setBorder(new EmptyBorder(0, 0, 0, 10));
            firstLineLabel.setPreferredSize(new Dimension(Sizes.STACK_LABEL_WIDTH_DEFAULT, Sizes.STACK_DEFAULT_HEIGHT));
            labelWrapper.add(BorderLayout.NORTH, firstLineLabel);
            firstLine.add(BorderLayout.CENTER, labelWrapper);
        } else if (style == Style.INPUT_FIELD) {
            firstLine.add(BorderLayout.CENTER, firstLineLabel);
        }

        if (style == Style.INPUT_FIELD) {
            JPanel crossWrapper = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
            crossWrapper.setBackground(Colors.INPUT_FIELD_BACKGROUND);
            crossWrapper.setBorder(new EmptyBorder(9, 0, 0, 0));

            hideLabel = new JLabel();
            hideLabel.setBackground(Colors.INPUT_FIELD_BACKGROUND);
            hideLabel.setIcon(Images.DELETE_CROSS_GRAY);
            hideLabel.setBorder(new EmptyBorder(0, 0, 0, 4));
            hideLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
//                setVisibleAndSaveToProperties(false);
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    hideLabel.setIcon(Images.DELETE_CROSS_BLACK);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    hideLabel.setIcon(Images.DELETE_CROSS_GRAY);
                }
            });
            crossWrapper.add(hideLabel);

            // set delete button/label for multi edit
            deleteLabel = new JLabel();
            deleteLabel.setBackground(Colors.INPUT_FIELD_BACKGROUND);
            deleteLabel.setIcon(Images.DELETE_CIRCLE_RED);
            deleteLabel.setBorder(new EmptyBorder(0, 0, 0, 4));
            deleteLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    clear();
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    deleteLabel.setIcon(Images.DELETE_CIRCLE_GRAY);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    deleteLabel.setIcon(Images.DELETE_CIRCLE_RED);
                }
            });
            crossWrapper.add(deleteLabel);
            setDeleteButtonVisible(false);

            firstLine.add(BorderLayout.EAST, crossWrapper);
        }

        return firstLine;
    }

    /**
     * Gets a formatted String of the label that should be displayed.
     * <p>
     * The format of the String can be different depending on the mode of the input field and whether the field is
     * mandatory or not.
     *
     * @return The formatted String of the label. Can be HTML.
     */
    private String getFormattedLabelText() {
        String newLabel = label;

        if (style == Style.STACK) {
            if (!newLabel.isEmpty()) {
                newLabel += ":";
            }
        }

        if (raw != null && isMandatory()) {
            return "<html><b>" + newLabel + "</b></html>";
        } else {
            return newLabel;
        }
    }

    /**
     * Sets a raw input field into the wrapper.
     *
     * @param raw The raw input field to set.
     */
    public final void setRawContent(AbstractInputElementFunctions raw) {
        this.raw = raw;
        raw.setInputElement(this);
        rawWrapper.add(BorderLayout.CENTER, raw);
        colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
    }

    /**
     * Sets the field to mandatory.
     */
    public void setMandatoryStyle(boolean b) {
        if (b) {
            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
            firstLineLabel.setText("<html><b>" + firstLineLabel.getText() + "</b></html>");
        } else {
            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
            firstLineLabel.setText(firstLineLabel.getText() + "</b></html>");
        }
        checkRememberValue.setVisible(b);
    }

    /**
     * Sets the mainframe object.
     *
     * @param mainFrame The mainframe object.
     */
    public static void setMainFrame(AbstractMainFrame mainFrame) {
        InputElement.mainFrame = mainFrame;
    }

    /**
     * Calculates the size of the input field.
     * <p>
     * Useres the horizontal and vertical number of grids and calculates the real pixel size of the input field.
     */
    public void calculateSize() {
        if (outerWrapper != null) {
            int x, y;
            switch (style) {
                case INPUT_FIELD:
                    x = (Sizes.INPUT_GRID_SIZE.width - 10) * gridX;
                    if (isMultiEdit) {
                        y = (Sizes.INPUT_GRID_SIZE.height - 10) * gridY + 200;
                    } else {
                        y = (Sizes.INPUT_GRID_SIZE.height - 10) * gridY + 200;
                    }
                case STACK:
                default:
                    x = Sizes.STACK_DEFAULT_WIDTH;
                    y = Sizes.STACK_DEFAULT_HEIGHT_NEW * gridY;
            }
            outerWrapper.setPreferredSize(new Dimension(x, y));
        }
    }

    public void colorizeBackground(Color color) {
        ComponentHelper.colorAllChildren(this, color);
        innerWrapper.setBackground(color);
    }

    public void setRememberValueFieldEnabled(boolean b) {
        checkRememberValue.setSelected(b);
        checkRememberValue.setIcon(Images.CHECKBOX_DEFAULT_ICON);
        checkRememberValue.setDisabledIcon(Images.CHECKBOX_DISABLED);
        checkRememberValue.setDisabledSelectedIcon(Images.CHECKBOX_DISABLED_SELECTED);
        checkRememberValue.setEnabled(b);
    }

    public void setRememberValueFieldVisible(boolean b) {
        setRememberValueFieldEnabled(b);
        checkRememberValue.setVisible(b);
    }

    public void setHideLabelVisible(boolean b) {
        hideLabel.setVisible(b);
    }

    @Override
    public boolean isMandatory() {
        if (raw != null) {
            return raw.isMandatory();
        }
        return false;
    }

    @Override
    public void load(DataSetOld data) {
        raw.load(data);
    }

    @Override
    public void loadMultiEdit(DataSetOld data) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void clear() {
        raw.clear();
    }

    @Override
    public void save(DataSetOld data) throws IsAMandatoryFieldException {
        System.out.println("*** SAVING OF RAW FIND IS DISABLED! ***");
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return raw.getMyFocusableComponents();
    }

    @Override
    public boolean isValidInput() {
        return raw.isValidInput();
    }

    @Override
    public void setEntry(UpdateableEntry entry) {
        this.entry = entry;
        raw.setEntry(entry);
    }

    @Override
    public void setDefaultStyle(boolean isFocused) {
        if (isMandatory()) {
            if (isFocused) {
                ComponentHelper.colorAllChildren(this, Colors.INPUT_FIELD_FOCUSED_MANDATORY_BACKGROUND);
            } else {
                ComponentHelper.colorAllChildren(this, Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
            }
        } else {
            if (isFocused) {
                ComponentHelper.colorAllChildren(this, Colors.INPUT_FIELD_FOCUSED_BACKGROUND);
            } else {
                ComponentHelper.colorAllChildren(this, Colors.INPUT_FIELD_BACKGROUND);
            }
        }
    }

    @Override
    public void setErrorStyle() {
        colorizeBackground(Colors.INPUT_FIELD_ERROR_BACKGROUND);
    }

    @Override
    public void setFocus() {
        ArrayList<Component> comp = getMyFocusableComponents();
        if (currentFocused == null) {
            if (comp.isEmpty()) {
            } else {
                comp.get(0).requestFocusInWindow();
            }
        } else {
            currentFocused.requestFocusInWindow();
        }
    }

    /**
     * Returns a focus listener that can be added to the focusable elements.
     *
     * @return The focus listener.
     */
    public FocusListener getFocusFunction() {
        return new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                ArrayList<Component> comp = getMyFocusableComponents();
                currentFocused = e.getComponent();
                if (entry != null) {
                    entry.setInputFieldFocused(InputElement.this);
                }

                if (!comp.contains(e.getOppositeComponent())) {
                    if (sidebar != null) {
                        Sidebar.setSidebarContent(sidebar);
                    }
                }
                // colorize the input element
                if (isMandatory()) {
                    colorizeBackground(Colors.INPUT_FIELD_FOCUSED_MANDATORY_BACKGROUND);
                } else {
                    colorizeBackground(Colors.INPUT_FIELD_FOCUSED_BACKGROUND);
                }
                outerWrapper.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_FOCUSED_BORDER, 1));
            }

            @Override
            public void focusLost(FocusEvent e) {
                ArrayList<Component> comp = getMyFocusableComponents();
                if (!comp.contains(sidebar)) {
                    comp.add(sidebar);
                }
                if (!comp.contains(e.getOppositeComponent())) {
                    currentFocused = null;
                    Sidebar.setSidebarContent(entry.getSideBar());
                }
                if (isMandatory()) {
                    colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
                } else {
                    colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
                }
                outerWrapper.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_BORDER, 1));
            }
        };
    }

    @Override
    public void actionOnFocusGain() {
        setDefaultStyle(true);
    }

    @Override
    public void actionOnFocusLost() {
    }

    @Override
    public boolean isRememberValueSelected() {
        return checkRememberValue.isSelected();
    }

    @Override
    public void setRememberValueFieldDisabled() {
        checkRememberValue.setSelected(false);
        checkRememberValue.setIcon(Images.CHECKBOX_DEFAULT_ICON);
        checkRememberValue.setDisabledIcon(Images.CHECKBOX_DISABLED);
        checkRememberValue.setDisabledSelectedIcon(Images.CHECKBOX_DISABLED_SELECTED);
        checkRememberValue.setEnabled(false);
    }

    @Override
    public void hideHideButton() {
        hideLabel.setVisible(false);
        hideLabel.setEnabled(false);
    }

    @Override
    public void setDeleteButtonVisible(boolean b) {
        deleteLabel.setVisible(b);
        deleteLabel.setEnabled(b);
    }

    @Override
    public int getGridX() {
        return gridX;
    }

    @Override
    public int getGridY() {
        return gridY;
    }

    @Override
    public void setGridX(int gridX) {
        this.gridX = gridX;
        revalidate();
    }

    @Override
    public void setGridY(int gridY) {
        this.gridY = gridY;
        revalidate();
    }

    @Override
    public void setSidebar(SidebarPanel sidebar) {
        this.sidebar = sidebar;
    }

    @Override
    public void setSidebar(String sidebarText) {
        this.sidebar = new SidebarEntryField(label, sidebarText);
    }

    @Override
    public String getStringRepresentation() {
        return raw.getStringRepresentation();
    }

}
