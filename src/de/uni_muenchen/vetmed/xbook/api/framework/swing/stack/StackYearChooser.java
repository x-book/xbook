package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * A stack element that holds a year chooser to enter valid years values.
 *
 * @author Daniel Kaltenthaler
 *         <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackYearChooser extends AbstractStackElement {

    final int MIN_YEAR = 0;
    final int MAX_YEAR = 3000;
    /**
     * The year chooser object.
     */
    private JSpinner yearChooser;
    /**
     * The default style of the year chooser.
     */
    private static Border defaultBorder = (new JSpinner()).getBorder();
    /**
     * Holds the custum default value.
     */
    private String defaultValue;

    /**
     * Constructor.
     *
     * @param columnType The correspondenting columnType.
     * @param labelText  The label of the stack element.
     * @param sidebar    The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
     */
    public StackYearChooser(ColumnType columnType, String labelText, SidebarPanel sidebar) {
        super(columnType, labelText, null, sidebar);

        String[] items = fillSpinnerWithData();
        SpinnerModel sm = new SpinnerListModel(items);
        yearChooser.setModel(sm);
        ((JSpinner.DefaultEditor) yearChooser.getEditor()).getTextField().addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                setDefaultStyle();
            }
        });
        addCustomSidebarFunctionality(yearChooser);
        addCustomSidebarFunctionality(((JSpinner.DefaultEditor) yearChooser.getEditor()).getTextField());
    }

    public StackYearChooser(ColumnType columnType, SidebarPanel sidebar) {
        this(columnType, columnType.getDisplayName(), sidebar);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param sidebar   The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
     */
    public StackYearChooser(String labelText, SidebarPanel sidebar) {
        super(null, labelText, null, sidebar);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     */
    public StackYearChooser(String labelText) {
        this(labelText, null);
    }

    private String[] fillSpinnerWithData() {
        ArrayList<String> dataAkk = new ArrayList<>();
        dataAkk.add("");
        for (int n = MIN_YEAR; n <= MAX_YEAR; n++) {
            dataAkk.add("" + (n));
        }
        return dataAkk.toArray(new String[dataAkk.size()]);
    }

    public void setYear(int year) {
        setYear("" + year);
    }

    public void setYear(String value) {
        int yearInt = -1;
        try {
            yearInt = Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            // yearInt is still -1
        }

        if (yearInt < MIN_YEAR || yearInt > MAX_YEAR) {
            yearChooser.setValue("");
        } else {
            yearChooser.setValue(value);
        }
    }

    public void setDefaultValue(int defaultValue) {
        setDefaultValue("" + defaultValue);
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
        setYear(defaultValue);
    }


    @Override
    public JComponent getElement() {
        yearChooser = new JSpinner();
        yearChooser.setFont(Fonts.FONT_STANDARD_PLAIN);
        return yearChooser;
    }

    @Override
    public String getValue() {
        if (yearChooser.getValue().equals("")) {
            return "-1";
        }
        return "" + yearChooser.getValue();
    }


    @Override
    public void clearInput() {
        if (defaultValue != null) {
            yearChooser.setValue("");
        } else {
            yearChooser.setValue(defaultValue);
        }
    }

    @Override
    public void setMandatoryStyle() {
        if (yearChooser != null) {
            yearChooser.setBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
            yearChooser.setFont(Fonts.FONT_STANDARD_BOLD);
        }
    }

    @Override
    public void setErrorStyle() {
        if (yearChooser != null) {
            yearChooser.setBorder(BorderFactory.createLineBorder(Color.red, 2));
        }
    }

    @Override
    public void setDefaultStyle() {
        if (yearChooser != null) {
            yearChooser.setBorder(defaultBorder);
        }
    }

    @Override
    public void setEnabled(boolean b) {
        if (yearChooser != null) {
            yearChooser.setEnabled(b);
        }
    }

    @Override
    public void addKeyListener(KeyListener l) {
        yearChooser.addKeyListener(l);
    }

    @Override
    public void setValue(String value) {
        setYear(value);
    }
}
