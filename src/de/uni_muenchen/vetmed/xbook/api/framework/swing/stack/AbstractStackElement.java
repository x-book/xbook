package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardProjectColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.abstracts.AbstractStackBase;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyListener;

/**
 * An abstract class to garantee a standard design for the stack elements.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractStackElement extends AbstractStackBase {

    /**
     * The text of the label.
     */
    protected String labelText = "";
    /**
     * Status if it is a mandatory field or not.
     */
    private boolean isMandatory = false;
    /**
     * The wrapper panel that holds all elements of the stack element.
     */
    protected JPanel wrapper;
    /**
     * The JLabel object holding the label.
     */
    private JLabel label;
    /**
     * The custom sidebar for this input element. <code>null</code> if no custom
     * sidebar is available.
     */
    protected SidebarPanel sidebar;

    protected ColumnType columnType;

    /**
     * Creates a stack element.
     * <p/>
     * The label is only be displayed if the <code>labelText</code> is not null.
     * If <code>labelText</code> is an empty String no text is displayed, but
     * the gap is visible. If <code>labelText</code> is <code>null</code> the
     * center element of the BorderLayout is wider.
     *
     * @param labelText The text of the lable. <code>""</code> if no text should
     * be displayed, <code>null</code> if no gap should be displayed
     */
    public AbstractStackElement(String labelText) {
        this(null, labelText, null, null);
    }

    /**
     * Creates a stack element.
     * <p/>
     * The label is only be displayed if the <code>labelText</code> is not null.
     * If <code>labelText</code> is an empty String no text is displayed, but
     * the gap is visible. If <code>labelText</code> is <code>null</code> the
     * center element of the BorderLayout is wider.
     *
     * @param labelText The text of the lable. <code>""</code> if no text should
     * be displayed, <code>null</code> if no gap should be displayed
     * @param sidebar The custom sidebar for this input * element.
     * <code>null</code> if no custom sidebar is available.
     */
    public AbstractStackElement(String labelText, SidebarPanel sidebar) {
        this(null, labelText, null, sidebar);
    }

    /**
     * Creates a stack element.
     * <p/>
     * The label is only be displayed if the <code>labelText</code> is not null.
     * If <code>labelText</code> is an empty String no text is displayed, but
     * the gap is visible. If <code>labelText</code> is <code>null</code> the
     * center element of the BorderLayout is wider.
     *
     * @param labelText The text of the lable. <code>""</code> if no text should
     * be displayed, <code>null</code> if no gap should be displayed
     * @param labelWidth The width of the label.
     */
    public AbstractStackElement(String labelText, Integer labelWidth) {
        this(null, labelText, labelWidth, null);
    }

    /**
     * Creates a stack element.
     * <p/>
     * The label is only be displayed if the <code>labelText</code> is not null.
     * If <code>labelText</code> is an empty String no text is displayed, but
     * the gap is visible. If <code>labelText</code> is <code>null</code> the
     * center element of the BorderLayout is wider.
     *
     * @param labelText The text of the lable. <code>""</code> if no text should
     * be displayed, <code>null</code> if no gap should be displayed
     * @param labelWidth The width of the label.
     * @param sidebar The custom sidebar for this input * element.
     * <code>null</code> if no custom sidebar is available.
     */
    public AbstractStackElement(String labelText, Integer labelWidth, SidebarPanel sidebar) {
        this(null, labelText, labelWidth, sidebar);
    }

    /**
     * Creates a stack element.
     * <p/>
     * The label is only be displayed if the <code>labelText</code> is not null.
     * If <code>labelText</code> is an empty String no text is displayed, but
     * the gap is visible. If <code>labelText</code> is <code>null</code> the
     * center element of the BorderLayout is wider.
     *
     * @param columnType The corresponding column type of the field.
     * @param labelText The text of the lable. <code>""</code> if no text should
     * be displayed, <code>null</code> if no gap should be displayed
     * @param sidebar The custom sidebar for this input * element.
     * <code>null</code> if no custom sidebar is available.
     */
    public AbstractStackElement(ColumnType columnType, String labelText, SidebarPanel sidebar) {
        this(columnType, labelText, null, sidebar);
    }

    /**
     * Creates a stack element.
     * <p/>
     * The label is only be displayed if the <code>labelText</code> is not null.
     * If <code>labelText</code> is an empty String no text is displayed, but
     * the gap is visible. If <code>labelText</code> is <code>null</code> the
     * center element of the BorderLayout is wider.
     *
     * @param columnType The corresponding column type of the field.
     * @param labelText The text of the lable. <code>""</code> if no text should
     * be displayed, <code>null</code> if no gap should be displayed
     * @param labelWidth The width of the label.
     * @param sidebar The custom sidebar for this input * element.
     * <code>null</code> if no custom sidebar is available.
     */
    public AbstractStackElement(ColumnType columnType, String labelText, Integer labelWidth, SidebarPanel sidebar) {
        if (columnType != null) {
            this.columnType = columnType;
        }
        if (labelText != null) {
            this.labelText = labelText;
        }
        this.sidebar = sidebar;

        setBackground(Colors.CONTENT_BACKGROUND);
        setBorder(new EmptyBorder(0, 0, 0, 0));

        // create a wrapper panal to make possible a static width
        wrapper = new JPanel(new BorderLayout());
        wrapper.setBackground(Colors.CONTENT_BACKGROUND);
        wrapper.setBorder(new EmptyBorder(0, 0, 0, 0));
        wrapper.setPreferredSize(new Dimension(Sizes.STACK_DEFAULT_WIDTH, Sizes.STACK_DEFAULT_HEIGHT));
        wrapper.setOpaque(true);

        // set a label.
        // display an empty string if the label is ""
        // display nothing if the label is null (the center layout is wider then.
        if (labelText != null) {
            JPanel labelWrapper = new JPanel();
            labelWrapper.setBackground(Colors.CONTENT_BACKGROUND);
            label = new JLabel(labelText);
            if (!label.getText().equals("")) {
                label.setText(label.getText() + ":");
            }
            if (labelWidth == null) {
                label.setPreferredSize(new Dimension(Sizes.STACK_LABEL_WIDTH_DEFAULT, Sizes.STACK_DEFAULT_HEIGHT - 8));
            } else {
                label.setPreferredSize(new Dimension(labelWidth, Sizes.STACK_DEFAULT_HEIGHT - 8));
            }
            label.setBackground(Colors.CONTENT_BACKGROUND);
            label.setHorizontalAlignment(SwingConstants.RIGHT);
            label.setBorder(new EmptyBorder(0, 0, 0, 10));
            labelWrapper.add(BorderLayout.NORTH, label);
            wrapper.add(BorderLayout.WEST, labelWrapper);
        }

        // add the elements to the center
        JComponent c = getElement();
        if (c != null) {
            wrapper.add(BorderLayout.CENTER, c);
        }
        add(wrapper);
    }

    /**
     * Set the stack element mandatory or not.
     *
     * @param isMandatory <code>true</code> if it is a mandatory * field,
     * <code>false</code> else.
     */
    public void setMandatory(boolean isMandatory) {
        this.isMandatory = isMandatory;
        if (isMandatory) {
            setMandatoryStyle();
        } else {
            setDefaultStyle();
        }
    }

    /**
     * Returns if the stack element is a mandatory field or not.
     *
     * @return Is the stack element a mandatory field or not.
     */
    public boolean isMandatory() {
        return isMandatory;
    }

    /**
     * Get the input of the stack element as a String object. If the stack
     * element is a mandatory field throw an exception if there was not entered
     * any valid value.
     *
     * @return The input of the stack element as a String object.
     * @throws IsAMandatoryFieldException If no valid value was entered.
     */
    public String getString() throws IsAMandatoryFieldException {
        String input = getValue();
        if (isMandatory && input.isEmpty()) {
            setErrorStyle();
            Footer.displayError(Loc.get("ENTER_A_VALUE_FOR_THE_MANDATORY_FIELD", labelText));
            throw new IsAMandatoryFieldException();
        }
        return input;
    }

    /**
     * Get the input of the stack element as an Integer object. If the stack
     * element is a mandatory field throw an exception if there was not entered
     * any valid value.
     *
     * @return The input of the stack element as an Integer object.
     * @throws IsAMandatoryFieldException If no valid value was entered.
     */
    public int getInteger() throws IsAMandatoryFieldException {
        String input = getValue();
        if (isMandatory && input.isEmpty()) {
            setErrorStyle();
            throw new IsAMandatoryFieldException();
        }
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    /**
     * Set the displayed content (without the label!).
     *
     * @return The content.
     */
    public abstract JComponent getElement();

    protected void setHeight(int height) {
        wrapper.setPreferredSize(new Dimension(wrapper.getPreferredSize().width, height));
        label.setPreferredSize(new Dimension(label.getPreferredSize().width, height));
    }

    @Override
    public abstract void addKeyListener(KeyListener l);

    /**
     * Adds the custom sidebar functionality to a specific component object.
     * <p/>
     * If an input field has a custom sidebar, every focusable element of the
     * input field should be called with this method.
     *
     * @param component The component to add the custom sidebar functionality.
     */
    protected void addCustomSidebarFunctionality(JComponent component) {
        component.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (sidebar != null) {
                    Sidebar.setSidebarContent(sidebar);
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (sidebar != null) {
                    Sidebar.clearSidebarContent();
                }
            }
        });

    }

    /**
     * Saves the current data to a ProjectDataSet.
     * <p/>
     * <strong>Important: Cannot be used if no ColumnType is available!</strong>
     *
     * @param data The ProjectDataSet object where to save the value.
     * @throws
     * de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException
     */
    public void save(ProjectDataSet data) throws IsAMandatoryFieldException {
        if (columnType == null) {
            if (labelText == null) {
                throw new RuntimeException("AbstractStackElement: save-method is only supported if a valid ColumnType is available.");
            } else {
                throw new RuntimeException("AbstractStackElement (" + labelText + "): save-method is only supported if a valid ColumnType is available.");
            }
        }
        DataRow list = data.getDataRow(IStandardProjectColumnTypes.TABLENAME_PROJECT);
        list.put(columnType.getColumnName(), getValue());
    }

    /**
     * Loades the current data from a ProjectDataSet.
     * <p/>
     * <strong>Important: Cannot be used if no ColumnType is available!</strong>
     *
     * @param data The ProjectDataSet object where is stored the data that
     * should be loaded.
     */
    public void load(ProjectDataSet data) {
        if (columnType == null) {
            if (labelText == null) {
                throw new RuntimeException("AbstractStackElement: load-method is only supported if a valid ColumnType is available.");
            } else {
                throw new RuntimeException("AbstractStackElement (" + labelText + "): load-method is only supported if a valid ColumnType is available.");
            }
        }
        DataRow list = data.getDataRow(IStandardProjectColumnTypes.TABLENAME_PROJECT);
        String value = list.get(columnType);
        if (value != null) {
            System.out.println("load: " + columnType + " | value: " + value);
            setValue(value);
        } else {
            clearInput();
        }
    }

    /**
     * Insert a value to the input field.
     *
     * @param value The value to insert.
     */
    public abstract void setValue(String value);

    /**
     * Return the input if a input is possible. <code>null</code> if no input is
     * possible.
     *
     * @return The input.
     */
    public abstract String getValue() throws IsAMandatoryFieldException;

    public boolean hasColumnType() {
        return columnType != null;
    }

}
