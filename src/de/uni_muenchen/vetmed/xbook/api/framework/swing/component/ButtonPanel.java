package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class ButtonPanel extends JPanel {

    public static enum Position {

        LEFT, CENTER, RIGHT
    }
    private final JPanel buttonsLeft;
    private final JPanel buttonsCenter;
    private final JPanel buttonsRight;

    private Color bgColor = new Color(240, 240, 240);

    public ButtonPanel() {
        setLayout(new BorderLayout());
        setBackground(bgColor);

        buttonsLeft = new JPanel(new FlowLayout());
        buttonsLeft.setBackground(bgColor);
        add(BorderLayout.WEST, buttonsLeft);

        buttonsCenter = new JPanel(new FlowLayout());
        buttonsCenter.setBackground(bgColor);
        add(BorderLayout.CENTER, buttonsCenter);

        buttonsRight = new JPanel(new FlowLayout());
        buttonsRight.setBackground(bgColor);
        add(BorderLayout.EAST, buttonsRight);
    }

    public void add(JComponent button, Position position) {
        button.setBackground(bgColor);
        button.setPreferredSize(new Dimension(150, 40));
        switch (position) {
            case LEFT:
                buttonsLeft.add(button);
                return;
            case CENTER:
                buttonsCenter.add(button);
                return;
            case RIGHT:
                buttonsRight.add(button);
                return;
        }
    }

    public void addToLeft(JComponent button) {
        add(button, Position.LEFT);
    }

    public void addToCenter(JComponent button) {
        add(button, Position.CENTER);
    }

    public void addToRight(JComponent button) {
        add(button, Position.RIGHT);
    }

    public void setBackgroundColor(Color color) {
        bgColor = color;
    }
}
