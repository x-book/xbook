package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import javax.swing.BorderFactory;

/**
 * An title object that can be used for any kind of title bars.
 *
 * @author Daniel Kaltenthaler
 * <kaltenthaler@dbs.ifi.lmu.de>
 */
public class XTitle extends JPanel {

    /**
     * The JLabel object where to display the text of the title.
     */
    protected JLabel label;
    protected JPanel labelWrapper;

    protected Color BG_COLOR = Colors.BOOK_COLOR_DARKER;

    /**
     * Constructor.
     *
     * Set the design of the title bar.
     *
     * @param title The text of the title.
     * @param borderSize
     */
    public XTitle(String title, Integer borderSize) {
        super();

        labelWrapper = new JPanel(new BorderLayout());
        setBorderColor(BG_COLOR);

        setLayout(new BorderLayout());
        setBackground(Colors.CONTENT_BACKGROUND);
        if (borderSize == null) {
            setBorder(new EmptyBorder(Fonts.TEXT_PARAGRAPH_PADDING + 4, 0, Fonts.TEXT_PARAGRAPH_PADDING - 4, 0));
        } else {
            setBorder(new EmptyBorder(borderSize, 0, borderSize, 0));
        }

        label = new JLabel(title);
        label.setSize(1000, 24);
        label.setFont(Fonts.FONT_BIG_BOLD);
        label.setPreferredSize(new Dimension(1000, 24));
        label.setMinimumSize(new Dimension(3000, 24));
        label.setBackground(BG_COLOR);
        label.setForeground(Color.WHITE);
        label.setOpaque(true);
        label.setBorder(new EmptyBorder(0, 8, 0, 8));
        labelWrapper.add(BorderLayout.CENTER, label);

        add(BorderLayout.CENTER, labelWrapper);
    }

    public XTitle(String title) {
        this(title, Fonts.TEXT_PARAGRAPH_PADDING);
    }

    /**
     * Set a new text to the title.
     *
     * @param text The text to display in the title.
     */
    public void setText(String text) {
        label.setText(text);
    }

    public void setBorderColor(Color color) {
        labelWrapper.setBorder(BorderFactory.createLineBorder(color, 1));
    }

    public void setBackgroundBar(Color bg) {
        if (labelWrapper != null) {
            labelWrapper.setBackground(bg);
        }
        if (label != null) {
            label.setBackground(bg);
        }
    }

    public void setForegroundBar(Color fg) {
        if (label != null) {
            label.setForeground(fg);
        }
    }

}
