package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

/**
 * An input field that is used to upload files to the server of xBook. This field allows to define a specfici "category"
 * from a list of available options that defines the categoryId of the folder structure.
 * <p>
 * Therefore there are three states of the field.
 * <p>
 * State 1 means that a file has been chosen, but the data set has not been save. In this case, the field displays the
 * selected categoryId and file name. This is always the state if a file has been selected by the file chooser.
 * <p>
 * State 2 means that a file is saved in the temporary file folder of xbook. In this case, the field displays the file
 * name (the categoryId is not necessary, because it is the general file folder). This is the state after the entry has
 * been saved and as long as the file has not been synchronized.
 * <p>
 * State 3 means that a file has been synced and is saved on the server. The saving in the temporary file is not
 * necessary any more. In this case, the field displays the unique file name that is saved on the server. This is always
 * the state after the entry has been synced and has not been changed since the synchronization.
 * <p>
 * As soon as another file has been selected from the file chooser or has inserted manually in the textfield, the field
 * is always reverted to state 1.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class RawFileUploadWithCategorySelectionField extends RawFileUploadWithCategoryField {

    private JPanel optionsPanel;
    private XComboBox comboCategory;
    private CodeTableHashMap categoryData;

    /**
     * Constructor.
     *
     * @param columnTypeFileName         The corresponding column type for file name.
     * @param columnTypeExtension        The corresponding column type for file extension.
     * @param columnTypeFileMustBeSynced The corresponding column type for the information whether the file has to be
     *                                   synchronized or not.
     * @param columnTypeCategory         The corresponding column type for category information.
     * @param displayedCategoryIds       The available categoryId where the files should be saved in the data
     *                                   structure.
     * @param fileFilter                 The file filter options.
     */
    public RawFileUploadWithCategorySelectionField(ApiControllerAccess controller, ColumnType columnTypeExtension,
                                                   ColumnType columnTypeFileName, ColumnType columnTypeFileMustBeSynced,
                                                   ColumnType columnTypeCategory, int[] displayedCategoryIds,
                                                   ArrayList<FileNameExtensionFilter> fileFilter) {
        super(controller, columnTypeFileName, columnTypeExtension, columnTypeFileMustBeSynced, columnTypeCategory,
                displayedCategoryIds, fileFilter);
        updateCategories();
    }

    @Override
    public void createField() {
        super.createField(true);

        // update the label status information
        labelStatus.setPreferredSize(new Dimension(100000, 32));

        // combine everything in the panel
        generalWrapper.add(BorderLayout.NORTH, textFieldWrapper);

        JPanel categorySelection = new JPanel(new BorderLayout());
        categorySelection.add(BorderLayout.NORTH, new JLabel(Loc.get("CATEGORY")));
        comboCategory = new XComboBox();
        comboCategory.setPreferredSize(new Dimension(Sizes.INPUT_GRID_SIZE.width, 32));
        categorySelection.add(BorderLayout.SOUTH, ComponentHelper.wrapComponent(comboCategory, 4, 0, 0, 0));

        optionsPanel = new JPanel(new BorderLayout());
        optionsPanel.add(BorderLayout.WEST, ComponentHelper.wrapComponent(categorySelection, 12, 16, 6, 0));
        optionsPanel.add(BorderLayout.EAST, eastButtonWrapper);
        generalWrapper.add(BorderLayout.SOUTH, optionsPanel);

        currentStatus = Status.NOT_SET;
        updateToStatus(currentStatus);
    }

    @Override
    protected void updateToStatus(Status newStatus) {
        // update the panels, if necessary
        boolean statusHasChanged = newStatus != currentStatus;
        if (statusHasChanged) {
            // elements must not be restructured if there is a change from Status.NOT_SET to
            // Status.NOT_SAVED or counterwise
            boolean mustDisplayTextField = (newStatus == Status.NOT_SET && currentStatus == Status.NOT_SAVED) ||
                    (newStatus == Status.NOT_SAVED && currentStatus == Status.NOT_SET);
            if (mustDisplayTextField) {
                // only update the text field wrapper if is really have to be reloaded.
                // if the textfield is removed from the panel and set again, there occure problems with component focus
                generalWrapper.removeAll();
                generalWrapper.add(BorderLayout.NORTH, textFieldWrapper);
            } else if (newStatus == Status.SAVED || newStatus == Status.SYNCED) {
                generalWrapper.removeAll();
                generalWrapper.add(BorderLayout.NORTH, labelStatus);
            }
        }

        // updates the visibility of the elements
        updateComponentVisibility(newStatus);
        // finally update the variable with the status
        currentStatus = newStatus;
    }

    public void setItems(CodeTableHashMap newData) {
        this.categoryData = newData;
        updateCategories();
    }

    public void updateCategories() {
        if (categoryData == null) {
            return;
        }
        ArrayList<String> allValues = new ArrayList<>();
        for (Map.Entry<String, String> hash : categoryData.entrySet()) {
            if (containsKey(hash.getKey())) {
                allValues.add(hash.getValue());
            }
        }
        Collections.sort(allValues, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.toUpperCase().compareTo(o2.toUpperCase());
            }
        });

        comboCategory.removeAllItems();
        comboCategory.addItem("");
        for (String value : allValues) {
            comboCategory.addItem(value);
        }
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());

        // save category
        if (getSelectedCategoryId() == null) {
            list.add(new DataColumn("-1", columnTypeCategory.getColumnName()));
        } else {
            list.add(new DataColumn(getSelectedCategoryId() + "", columnType.getColumnName()));
        }
    }

    public String getSelectedCategoryId() {
        if (comboCategory.getSelectedItem() == null || comboCategory.getSelectedItem().equals("")) {
            return null;
        }
        return categoryData.convertStringToIdAsString((String) comboCategory.getSelectedItem());
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());

        String value = list.get(columnTypeCategory);
        if (value != null) {
            comboCategory.setSelectedItem(categoryData.convertIdAsStringToString(value));
        }
    }

    @Override
    public void clear() {
        // category
        comboCategory.setSelectedItem("");
    }

}
