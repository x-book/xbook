package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.RepeatBackgroundPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class XImageButton extends JPanel implements IButton {

    public enum Style {

        COLOR, GRAY, DISABLED, GREEN, RED
    }

    private final JLabel left;
    private final RepeatBackgroundPanel center;
    private final JLabel right;
    private final ArrayList<ActionListener> listener = new ArrayList<>();
    private final static int DEFAULT_WIDTH = 54;
    private JLabel labelIcon;
    private JLabel labelLabel;
    private Dimension imageBorderWidth;
    private ImageIcon imageLeft = Images.BUTTON_PANEL_BUTTON_LEFT;
    private ImageIcon imageCenter = Images.BUTTON_PANEL_BUTTON_CENTER;
    private ImageIcon imageRight = Images.BUTTON_PANEL_BUTTON_RIGHT;
    private ImageIcon currentIcon = null;
    private String currentLabel = null;
    private boolean isEnabled = true;
    private Style currentStyle = Style.COLOR;

    public XImageButton() {
        this(null, null, DEFAULT_WIDTH);
    }

    public XImageButton(String label) {
        this(null, label, DEFAULT_WIDTH);
    }

    public XImageButton(String label, double widthPercentage) {
        this(null, label, (int) (DEFAULT_WIDTH * widthPercentage));
    }

    public XImageButton(String label, int width) {
        this(null, label, width);
    }

    public XImageButton(ImageIcon icon) {
        this(icon, null, DEFAULT_WIDTH);
    }

    public XImageButton(ImageIcon icon, double widthPercentage) {
        this(icon, null, (int) (DEFAULT_WIDTH * widthPercentage));
    }

    public XImageButton(ImageIcon icon, int width) {
        this(icon, null, width);
    }

    public XImageButton(ImageIcon icon, String label, int width) {
//        ImageIcon imageLeft = Images.BUTTON_PANEL_BUTTON_LEFT;
//        ImageIcon imageCenter = Images.BUTTON_PANEL_BUTTON_CENTER;
//        ImageIcon imageRight = Images.BUTTON_PANEL_BUTTON_RIGHT;

        imageBorderWidth = new Dimension(imageLeft.getIconWidth(), imageLeft.getIconHeight());

        left = new JLabel(imageLeft);
        left.setSize(imageLeft.getIconWidth(), imageLeft.getIconHeight());
        left.setPreferredSize(new Dimension(imageLeft.getIconWidth(), imageLeft.getIconHeight()));

        JPanel centerWrapper = new JPanel(new BorderLayout());
        centerWrapper.setOpaque(false);
        labelIcon = new JLabel();
        labelIcon.setOpaque(false);
        setIcon(icon);
        centerWrapper.add(BorderLayout.WEST, labelIcon);
        labelLabel = new JLabel();
        labelLabel.setForeground(Color.WHITE);
        labelLabel.setOpaque(false);
        setLabel(label);
        centerWrapper.add(BorderLayout.CENTER, labelLabel);
        center = new RepeatBackgroundPanel(imageCenter);
        setWidth(width);
        center.add(centerWrapper);

        right = new JLabel(imageRight);
        right.setSize(imageRight.getIconWidth(), imageRight.getIconHeight());
        right.setPreferredSize(new Dimension(imageRight.getIconWidth(), imageRight.getIconHeight()));

        setLayout(new BorderLayout());
        add(BorderLayout.WEST, left);
        add(BorderLayout.CENTER, center);
        add(BorderLayout.EAST, right);

        left.addMouseListener(getMouseListener());
        center.addMouseListener(getMouseListener());
        right.addMouseListener(getMouseListener());
        labelIcon.addMouseListener(getMouseListener());
        labelLabel.addMouseListener(getMouseListener());
    }

    protected void fireUpdate(ActionEvent evt) {
        for (ActionListener l : listener) {
            l.actionPerformed(evt);
        }
    }

    private MouseListener getMouseListener() {
        return new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (isEnabled()) {
                    left.setIcon(Images.BUTTON_PANEL_BUTTON_HOVERED_LEFT);
                    center.setImage(Images.BUTTON_PANEL_BUTTON_HOVERED_CENTER.getImage());
                    right.setIcon(Images.BUTTON_PANEL_BUTTON_HOVERED_RIGHT);
                    repaint();
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                left.setIcon(imageLeft);
                center.setImage(imageCenter.getImage());
                right.setIcon(imageRight);
                repaint();
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                left.setIcon(imageLeft);
                center.setImage(imageCenter.getImage());
                right.setIcon(imageRight);
                repaint();

                if (isEnabled) {
                    fireUpdate(new ActionEvent(this, 0, "awesome"));
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                requestFocus();
            }

        };
    }

    public void addActionListener(ActionListener al) {
        listener.add(al);
    }

    public void setIcon(ImageIcon icon, String tooltip) {
        setIcon(icon);
        setToolTipText(tooltip);
    }

    public void setIcon(ImageIcon icon) {
        if (labelIcon == null) {
            return;
        }
        currentIcon = icon;

        if (icon == null) {
            labelIcon.setIcon(Images.BUTTONPANEL_EMPTYHELPER);
        } else {
            labelIcon.setIcon(icon);
        }
        setLabel(currentLabel);
    }

    public void setLabel(String label) {
        if (labelLabel == null) {
            return;
        }
        currentLabel = label;
        if (label == null || label.isEmpty()) {
            labelLabel.setText("");
        } else if (currentIcon == null) {
            labelLabel.setText(label);
        } else {
            labelLabel.setText("  " + label + "  ");
        }
        revalidate();
        repaint();
    }

    @Override
    public void setToolTipText(String text) {
        left.setToolTipText(text);
        center.setToolTipText(text);
        right.setToolTipText(text);
        labelIcon.setToolTipText(text);
        labelLabel.setToolTipText(text);
    }

    public void setWidth(int width) {
        width = width - (2 * imageBorderWidth.width);
        center.setSize(width, imageBorderWidth.height);
        center.setPreferredSize(new Dimension(width, imageBorderWidth.height));
    }

    public void setStyle(Style style) {
        switch (style) {
            case COLOR:
                imageLeft = Images.BUTTON_PANEL_BUTTON_LEFT;
                imageCenter = Images.BUTTON_PANEL_BUTTON_CENTER;
                imageRight = Images.BUTTON_PANEL_BUTTON_RIGHT;
                break;
            case GREEN:
                imageLeft = Images.BUTTON_PANEL_BUTTON_GREEN_LEFT;
                imageCenter = Images.BUTTON_PANEL_BUTTON_GREEN_CENTER;
                imageRight = Images.BUTTON_PANEL_BUTTON_GREEN_RIGHT;
                break;
            case RED:
                imageLeft = Images.BUTTON_PANEL_BUTTON_RED_LEFT;
                imageCenter = Images.BUTTON_PANEL_BUTTON_RED_CENTER;
                imageRight = Images.BUTTON_PANEL_BUTTON_RED_RIGHT;
                break;
            case GRAY:
            case DISABLED:
            default:
                imageLeft = Images.BUTTON_PANEL_BUTTON_GRAY_LEFT;
                imageCenter = Images.BUTTON_PANEL_BUTTON_GRAY_CENTER;
                imageRight = Images.BUTTON_PANEL_BUTTON_GRAY_RIGHT;
                break;
        }
        left.setIcon(imageLeft);
        center.setImage(imageCenter.getImage());
        right.setIcon(imageRight);
        repaint();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        this.isEnabled = enabled;
        if (!enabled) {
            left.setIcon(Images.BUTTON_PANEL_BUTTON_GRAY_LEFT);
            center.setImage(Images.BUTTON_PANEL_BUTTON_GRAY_CENTER.getImage());
            right.setIcon(Images.BUTTON_PANEL_BUTTON_GRAY_RIGHT);
        } else {
            setStyle(currentStyle);
        }
        repaint();
    }
}
