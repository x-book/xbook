package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.LabelConstraints;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * The raw label field.
 *
 * @param <C> The corresponding input constraints for the raw input field.
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITRawLabel<C extends LabelConstraints> extends AbstractITRaw<C> {

    /**
     * The displayed label object.
     */
    private JLabel label;
    /**
     * The background color of this label.
     */
    private Color backgroundColor = Color.WHITE;

    public ITRawLabel(C constraints) {
        super(constraints);
    }

    @Override
    protected JComponent create() {
        JPanel wrapper = new JPanel(new BorderLayout());
        label = new JLabel(constraints.getLabel());
        label.setHorizontalAlignment(constraints.getHorizontalAlignment());
        label.setBackground(backgroundColor);
        label.setOpaque(true);
        label.setBorder(new EmptyBorder(0, 5, 0, 5));
        label.setMinimumSize(new Dimension(constraints.getWidth(), 30));
        label.setMaximumSize(new Dimension(constraints.getWidth(), 30));
        wrapper.add(BorderLayout.CENTER, label);
        return wrapper;
    }

    @Override
    public String getValue() {
        // not necessary
        return "n.a.";
    }

    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public void setValue(String value) {
        // not necessary
    }

    @Override
    public void clear() {
        // not necessary
    }

    @Override
    public boolean isValidInput() {
        // not necessary
        return false;
    }

    @Override
    public void actionOnFocusGain() {
        // not necessary
    }

    @Override
    public void actionOnFocusLost() {
        super.actionOnFocusLost();
        // not necessary
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return new ArrayList<>();
    }

    @Override
    public String getStringRepresentation() {
        return "n.a.";
    }

    @Override
    public void save(DataRow dataa) {
        //no please don't!
    }

    @Override
    public void setBackground(Color color) {
        backgroundColor = color;
        if (label != null) {
            label.setBackground(color);
        }
    }

    @Override
    public void addSidebarFunktionality() {
        // not necessary
    }

    public JLabel getLabel() {
        return label;
    }
}
