package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawCheckBoxSmall;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;

import javax.swing.*;
import java.awt.event.KeyListener;

/**
 * A stack element that holds a text field.
 *
 * @author Daniel Kaltenthaler
 * <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackCheckBoxSmall extends AbstractStackElement {

    /**
     * The textfield object.
     */
    private RawCheckBoxSmall raw;

    /**
     * Constructor.
     *
     * @param columnType The correspondenting column type for this input field.
     * @param labelText The label of the stack element.
     * @param labelWidth The width of the label.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    public StackCheckBoxSmall(ColumnType columnType, String labelText, Integer labelWidth, SidebarPanel sidebar) {
        super(columnType, labelText, labelWidth, sidebar);
        addCustomSidebarFunctionality(raw.getCheckBox());
        setMandatory(columnType.isMandatory());
    }

    public StackCheckBoxSmall(ColumnType columnType, String labelText, Integer labelWidth) {
        this(columnType, labelText, labelWidth, null);
    }

    /**
     * Constructor.
     *
     * @param columnType The correspondenting column type for this input field.
     * @param labelText The label of the stack element.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    public StackCheckBoxSmall(ColumnType columnType, String labelText, SidebarPanel sidebar) {
        this(columnType, labelText, null, sidebar);
    }

    /**
     * Constructor.
     *
     * @param columnType The correspondenting column type for this input field.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    public StackCheckBoxSmall(ColumnType columnType, SidebarPanel sidebar) {
        this(columnType, columnType.getDisplayName(), null, sidebar);
    }

    /**
     * Constructor.
     *
     * @param columnType The correspondenting column type for this input field.
     * @param labelText The label of the stack element.
     * @param sidebarText The text that will be displayed into the sidebar.
     */
    public StackCheckBoxSmall(ColumnType columnType, String labelText, String... sidebarText) {
        this(columnType, labelText, null, new SidebarPanel(sidebarText));
    }

    @Override
    public JComponent getElement() {
        if (raw == null) {
            raw = new RawCheckBoxSmall(columnType);
        }
        raw.getCheckBox().setFont(Fonts.FONT_STANDARD_PLAIN);
        return raw;
    }

    @Override
    public String getValue() throws IsAMandatoryFieldException {
        return raw.getValue();
    }


    @Override
    public void clearInput() {
        raw.setText("");
    }

    @Override
    public void setMandatoryStyle() {
    }

    @Override
    public void setErrorStyle() {
    }

    @Override
    public void setDefaultStyle() {
    }

    @Override
    public void setEnabled(boolean b) {
        if (raw != null) {
            raw.getCheckBox().setEnabled(b);
        }
    }

    @Override
    public void addKeyListener(KeyListener l) {
        raw.getCheckBox().addKeyListener(l);
    }

    @Override
    public void requestFocus() {
        raw.getCheckBox().requestFocusInWindow();
    }

    @Override
    public void setValue(String value) {
        raw.getCheckBox().setText(value);
    }

}
