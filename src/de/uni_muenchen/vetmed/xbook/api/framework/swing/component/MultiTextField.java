package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import javax.swing.*;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Extends the DynamicComboBox with a list that allows adding several elements
 * out of the combo box.
 *
 * @author Daniel Kaltenthaler
 * @version 1.0.0
 */
public class MultiTextField extends AbstractMultiComponent<String> {

    /**
     * The dynamic combo box that is used above the list.
     */
    private JTextField textField;

    /**
     * Constructor of the DynamicComboBoxList class.
     *
     * @param name
     */
    public MultiTextField(String name) {
        super(name);
        init();
    }

    @Override
    protected JComponent getInputComponent() {
        if (textField == null) {
            textField = new JTextField();
        }
        return textField;
    }

    @Override
    public void addInputToList() {
        if(!textField.getText().isEmpty()) {
            addItemToList(textField.getText());
        }
    }

    @Override
    public boolean addItemToList(String item) {
        if (!item.isEmpty()) {
            if (!selectedItemsOnList.contains(item)) {
                selectedItemsOnList.add(item);
                listModel.addElement(item);
                textField.setText("");
                return true;
            }
        }
        return false;
    }

    @Override
    public void removeSelectedItemsFromList() {
        int[] selected = listSelectedItems.getSelectedIndices();
        for (int i = selected.length - 1; i >= 0; i--) {
            selectedItemsOnList.remove(selected[i]);
            listModel.removeElementAt(selected[i]);
        }
    }

    @Override
    public void removeAllItemsFromList() {
        for (int i = listModel.getSize() - 1; i >= 0; i--) {
            selectedItemsOnList.remove(i);
            listModel.removeElementAt(i);
        }
    }

    public void removeItem(String text) {
        for (int i = 0; i < listModel.getSize(); i++) {
            if (text.equals(selectedItemsOnList.get(i))) {
                selectedItemsOnList.remove(i);
                listModel.removeElementAt(i);
                return;
            }
        }
    }

    public void removeItems(List<String> text) {
        for (String s : text) {
            removeItem(s);
        }
    }

    @Override
    public void addAllItems() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ArrayList<String> getListItems(boolean addComboItemToo) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < listModel.size(); i++) {
            list.add(listModel.getElementAt(i));
        }
        if (addComboItemToo) {
            list.add(textField.getText());
        }
        return list;
    }

    @Override
    public void clearInput() {
        textField.setText("");
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        textField.setVisible(b);
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        textField.setEnabled(b);
    }

    @Override
    public void addFocusListener(FocusListener fl) {
        super.addFocusListener(fl);
        textField.addFocusListener(fl);
    }

    public JTextField getTextField() {
        return textField;
    }

    @Override
    protected Comparator<String> getComparator() {
        return new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                if (o1 == o2) {
                    return 0;
                }
                if (o1 == null) {
                    return -1;
                }
                if (o2 == null) {
                    return 1;
                }
                return o1.compareTo(o2);
            }
        };
    }

    @Override
    public boolean addItemsToList(ArrayList<String> items) {
        boolean allSuccessful = true;
        for (String s : items) {
            allSuccessful = allSuccessful && addItemToList(s);
        }
        return allSuccessful;
    }

    @Override
    public String getElementFromInput() {
        return textField.getText();
    }

}
