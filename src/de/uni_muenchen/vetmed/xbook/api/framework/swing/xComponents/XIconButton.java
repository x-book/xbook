package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class XIconButton extends XButton {

    public XIconButton(ImageIcon icon) {
        this(icon, null, 1.0);
    }

    public XIconButton(ImageIcon icon, double widthModifier) {
        this(icon, null, widthModifier);
    }

    public XIconButton(ImageIcon icon, String tooltippText) {
        this(icon, tooltippText, 1.0);
    }

    public XIconButton(ImageIcon icon, String tooltippText, double widthModifier) {
        super();
        setButtonSize((int) ((Sizes.BUTTON_HEIGHT * widthModifier) + (Sizes.BUTTON_HEIGHT * 0.5)), Sizes.BUTTON_HEIGHT);
        button.setIcon(icon);
        button.setHorizontalTextPosition(SwingConstants.CENTER);
        if (tooltippText != null && !tooltippText.isEmpty()) {
            button.setToolTipText(tooltippText);
        }
    }

}
