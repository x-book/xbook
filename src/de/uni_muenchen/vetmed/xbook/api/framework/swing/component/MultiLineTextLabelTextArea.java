package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;

/**
 * A JTextArea which looks like a normal JLabel but allows an automatical line wrap.
 *
 * @author Daniel Kaltenthaler
 */
public class MultiLineTextLabelTextArea extends JTextArea {

    private boolean useCustomSizeCalculation;

    /**
     * Constructor of the MultiLineTexelLabel class.
     *
     * Creates a JTextArea which looks like a normal JLabel but allows an automatical line wrap.
     *
     * @param text The text which should be displayed in the JTextArea.
     * @param useCustomSizeCalculation Deactive the custom size calculation when parameter is <code>true</code>. The
     * default value should be <code>false</code>.
     */
    public MultiLineTextLabelTextArea(String text, boolean useCustomSizeCalculation) {
        super(text);
        this.useCustomSizeCalculation = useCustomSizeCalculation;
        setLineWrap(true);
        setWrapStyleWord(true);
        setEditable(false);
        //setEnabled(true);
        setBackground(new Color(240, 240, 240));
        setDisabledTextColor(Color.BLACK);
        setBorder(new EmptyBorder(0, 0, 0, 0));
    }

    /**
     * Constructor of the MultiLineTexelLabel class.
     *
     * Creates a JTextArea which looks like a normal JLabel but allows an automatical line wrap.
     *
     * @param text The text which should be displayed in the JTextArea.
     */
    public MultiLineTextLabelTextArea(String text) {
        this(text, false);
    }

    @Override
    public Dimension getPreferredSize() {
        if (useCustomSizeCalculation) {
            return super.getPreferredSize();
        } else {
            Rectangle r;
            try {
                r = modelToView(getDocument().getLength());
            } catch (BadLocationException ex) {
                return super.getPreferredSize();
            }
            if (getParent() == null || getParent().getSize() == null || r == null) {
                return super.getPreferredSize();
            }
            return new Dimension(getParent().getSize().width - 10, r.y + r.height);
        }
    }

    @Override
    public void setForeground(Color fg) {
        super.setForeground(fg);
        setDisabledTextColor(fg);
    }
    
}
