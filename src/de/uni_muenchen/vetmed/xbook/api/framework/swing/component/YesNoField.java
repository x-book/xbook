package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class YesNoField extends JPanel {

    private JRadioButton radioYes;
    private JRadioButton radioNo;

    public YesNoField() {
        setLayout(new GridLayout(2, 1));

        radioYes = new JRadioButton(Loc.get("YES"));
        radioYes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (radioYes.isSelected()) {
                    radioNo.setSelected(false);
                }
            }
        });
        add(radioYes);

        radioNo = new JRadioButton(Loc.get("NO"));
        radioNo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (radioNo.isSelected()) {
                    radioYes.setSelected(false);
                }
            }
        });
        add(radioNo);
    }

    public JRadioButton getYes() {
        return radioYes;
    }

    public JRadioButton getNo() {
        return radioNo;
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (radioYes != null) {
            radioYes.setBackground(bg);
        }
        if (radioNo != null) {
            radioNo.setBackground(bg);
        }
    }

    public void setSelectedYes(boolean status) {
        radioYes.setSelected(status);
        radioNo.setSelected(!status);
    }

    public void setSelectedNo(boolean status) {
        setSelectedYes(!status);
    }

    public void setSelectedNone() {
        radioYes.setSelected(false);
        radioNo.setSelected(false);
    }

    public boolean isSelectedYes() {
        return radioYes.isSelected();
    }

    public boolean isSelectedNo() {
        return radioNo.isSelected();
    }

    public boolean isSelected() {
        return isSelectedYes() || isSelectedNo();
    }

    public boolean isSelectedNone() {
        return !(isSelected());
    }

    @Override
    public void setFont(Font font) {
        if (radioNo != null && radioYes != null) {
            radioNo.setFont(font);
            radioYes.setFont(font);
        }
    }
}
