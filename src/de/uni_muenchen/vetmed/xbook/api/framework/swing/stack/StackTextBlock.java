package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import java.awt.Dimension;
import javax.swing.JPanel;

import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import java.awt.Color;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabelTextArea;

/**
 * A stack element that displays a text without any label.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackTextBlock extends JPanel {

    private MultiLineTextLabelTextArea multi;

    /**
     * Constructor.
     *
     * @param text The text to display in the textblock.
     */
    public StackTextBlock(String text) {
        multi = new MultiLineTextLabelTextArea(text, true);
        multi.setBackground(Colors.CONTENT_BACKGROUND);
        multi.setSize(new Dimension(Sizes.STACK_DEFAULT_WIDTH, 1));
        multi.setFont(Fonts.FONT_STANDARD_PLAIN);
        setBackground(Colors.CONTENT_BACKGROUND);
        add(multi);
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (multi != null) {
            multi.setBackground(bg);
        }
    }
}
