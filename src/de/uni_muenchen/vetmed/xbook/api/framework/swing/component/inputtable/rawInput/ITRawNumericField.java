package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.NumericConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.TextFieldConstraints;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;

/**
 * The raw numeric field.
 *
 * @param <C> The corresponding input constraints for the raw input field.
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITRawNumericField<C extends NumericConstraints> extends AbstractITRaw<C> {

    private JTextField textField;

    public ITRawNumericField(C constraints) {
        super(constraints);
    }

    @Override
    protected JComponent create() {
        JPanel wrapper = new JPanel(new BorderLayout());
        textField = new JTextField();
        textField.setBorder(new EmptyBorder(0, 5, 0, 5));
        wrapper.add(BorderLayout.CENTER, textField);
        return wrapper;
    }

    @Override
    public String getValue() {
        if (textField.getText().isEmpty()) {
            return "-1";
        }
        return textField.getText();
    }

    @Override
    public boolean isEmpty() {
        return textField.getText().isEmpty();
    }

    @Override
    public void setValue(String value) {
        if ("-1".equals(value) || "0".equals(value)) {
            textField.setText("");
        } else {
            textField.setText(value);
        }
    }

    @Override
    public void clear() {
        textField.setText("");
    }

    @Override
    public boolean isValidInput() {
        try {
            String value = textField.getText();
            //Default return false otherwise fields with nothing selected will return true in row
            if (value.isEmpty()) {
                return false;
            } else {
                Integer.parseInt(textField.getText());
            }
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @Override
    public void actionOnFocusGain() {
        textField.setBackground(Colors.SIDEBAR_BACKGROUND);
    }

    @Override
    public void actionOnFocusLost() {
        super.actionOnFocusLost();
        textField.setBackground(Color.WHITE);

        if (!textField.getText().isEmpty() && !isValidInput()) {
            textField.setText("");
            Footer.displayError(Loc.get("NO_VALID_INPUT_ONLY_INTEGER_VALUES"));
            textField.setBackground(Color.RED);
        }
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textField);
        return comp;
    }

    @Override
    public String getStringRepresentation() {
        return getValue();
    }

    /**
     * Returns the textfield object.
     *
     * @return The textfield object.
     */
    public JTextField getTextField() {
        return textField;
    }


    @Override
    public void addSidebarFunktionality() {
        textField.addFocusListener(getSidebarFunctionality());
    }
}
