package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.documentHelper.AutoCompletionDocument;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

public class XComboBox<T> extends JComboBox<T> {

    private final AutoCompletionDocument doc;
    private boolean autoSort = false;
    private java.util.List<T> items;

    public XComboBox() {
        super();
        doc = new AutoCompletionDocument(this);
        setTooltipsOnItemsActive();
        this.items = new ArrayList<>();
        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionOnActionPerformed();
            }
        });
        setFocusListener();
    }

    public XComboBox(T[] items) {
        super(items);
        this.items = new ArrayList<>(Arrays.asList(items));
        setTooltipsOnItemsActive();
        doc = new AutoCompletionDocument(this);
        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionOnActionPerformed();
            }
        });
        setFocusListener();
    }

    @Override
    public void addItem(T obj) {
        if (!autoSort) {
            super.addItem(obj);
            items.add(obj);
            return;
        }
        int count = getItemCount();

        if (items.isEmpty()) {
            super.addItem(obj);
            items.add(obj);
        } else {

            if (items.get(0) != null && obj.toString().toUpperCase().compareTo(items.get(0).toString().toUpperCase()) <= 0) {
                insertItemAt(obj, 0);
                items.add(0, obj);
            } else {
                int lastIndexOfHigherNum = 0;
                for (int i = 0; i < count; i++) {
                    if (items.get(0) == null || obj.toString().toUpperCase().compareTo(items.get(i).toString().toUpperCase()) > 0) {
                        lastIndexOfHigherNum = i;
                    }
                }
                insertItemAt(obj, lastIndexOfHigherNum + 1);
                items.add(lastIndexOfHigherNum + 1, obj);
            }
        }
    }


    private final void setTooltipsOnItemsActive() {
        ListCellRenderer render = getRenderer();
        if (!(renderer instanceof XListCellRenderer)) {
            setRenderer(new XListCellRenderer(render));
        }
    }

    private void setFocusListener() {
        final JTextField tf = getTextField();
        addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                actionOnFocusLost(tf);
            }

            @Override
            public void focusGained(FocusEvent e) {
                actionOnFocusGained(tf);
            }
        });
    }

    protected void actionOnFocusLost(JTextField tf) {
        if (!isCustomInputEnabled() && !isIntegerInputEnabled()) {
            String input = tf.getText();
            if (!input.isEmpty()
                    && ((DefaultComboBoxModel) getModel()).getIndexOf(input) == -1 && !input.equals(getSelectedItem().toString())) {
                tf.setText("");
                Toolkit.getDefaultToolkit().beep();
            }
        }
    }

    protected void actionOnFocusGained(JTextField tf) {
    }

    protected void actionOnActionPerformed() {
        final Object selectedItem = getSelectedItem();
        if (selectedItem != null) {
            setToolTipText(selectedItem.toString());
        } else {
            setToolTipText(null);
        }
    }

    public void setCustomInputEnabled(boolean customInputAllowed) {
        doc.setCustomInputEnabled(customInputAllowed);
    }

    public void setIntegerInputEnabled(boolean integerInputAllowed) {
        doc.setIntegerInputEnabled(integerInputAllowed);
    }

    public boolean isCustomInputEnabled() {
        return doc.isCustomInputEnabled();
    }

    public boolean isIntegerInputEnabled() {
        return doc.isIntegerInputEnabled();
    }

    public void setAutoSort(boolean autoSort) {
        this.autoSort = autoSort;
        doc.setOrdered(autoSort);
    }

    /**
     * Returns the JTextField of the combo box where the input is done.
     *
     * @return The JTextField.
     */
    public JTextField getTextField() {
        return (JTextField) getEditor().getEditorComponent();
    }

    /**
     * Removes all items from the combo box.
     */
    public void clearItems() {
        setModel(new DefaultComboBoxModel<T>());
    }

    public void setItems(LinkedHashMap<String, T> sortedItems) {
        ArrayList<T> list = new ArrayList<>(sortedItems.values());
        this.items = list;
        setModel(new DefaultComboBoxModel<>((T[]) list.toArray()));
    }

    public void setItems(List<T> sortedItems) {
        this.items = sortedItems;
        setModel(new DefaultComboBoxModel<>((T[]) sortedItems.toArray()));
    }

    public static class XListCellRenderer implements ListCellRenderer<Object> {

        private final ListCellRenderer wrapped;

        public XListCellRenderer(ListCellRenderer wrapped) {
            this.wrapped = wrapped;
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends Object> list, Object value, int index,
                                                      boolean isSelected, boolean cellHasFocus) {
            JComponent comp = (JComponent) wrapped.getListCellRendererComponent(list, value, index, isSelected,
                    cellHasFocus);
            if (value != null) {
                list.setToolTipText(value.toString());
            }
            return comp;
        }

        public ListCellRenderer getWrapped() {
            return wrapped;
        }
    }

}
