package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 23.09.2017.
 */
public class ITConstants {

    public static final int CELL_HEIGHT = 30;
    public static final int MINIMUM_CELL_WIDTH = 50;
    public static final int MAXIMUM_CELL_WIDTH = 200;


}
