package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.ITRawComboBox;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntryRoot;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.dependable.AbstractBaseDependableEntry;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class ComboBoxConstraints extends AbstractInputConstraints {

	/**
	 * The values that are selectable from the combo box.
	 */
	private String[] values = {};

	/**
	 * Constructor.
	 *
	 * Initialize the constraints.
	 *
	 * @param entry      The corresponding Entry object of the input field.
	 * @param columnType The corresponding ColumnType of the input field.
	 * @param width The width of the input fields in pixels.
	 * @param height The height of the input fields in pixels.
	 * @param borderEast The size of the border in the east.
	 * @param values The values that are selectable from the combo box.
	 */
	public ComboBoxConstraints(AbstractEntryRoot entry, ColumnType columnType, int width, int height, int borderEast, String[] values) {
		super(entry, columnType, width, height, borderEast);
		this.values = values;
	}

	/**
	 * Constructor.
	 *
	 * Initialize the constraints. Use this constructor to display no border in the east.
	 *
	 * @param entry      The corresponding Entry object of the input field.
	 * @param columnType The corresponding ColumnType of the input field.
	 * @param width The width of the input fields in pixels.
	 * @param height The height of the input fields in pixels.
	 * @param values The values that are selectable from the combo box.
	 */
	public ComboBoxConstraints(AbstractEntryRoot entry, ColumnType columnType, int width, int height, String[] values) {
		this(entry, columnType, width, height, 0, values);
	}

	/**
	 * Constructor.
	 *
	 * Initialize the constraints. Use this constructor to display no border in the east and default cell height.
	 *
	 * @param entry      The corresponding Entry object of the input field.
	 * @param columnType The corresponding ColumnType of the input field.
	 * @param width The width of the input fields in pixels.
	 * @param values The values that are selectable from the combo box.
	 */
	public ComboBoxConstraints(AbstractEntryRoot entry, ColumnType columnType, int width, String[] values) {
		this(entry, columnType, width, 1, 0, values);
	}

	@Override
	protected AbstractITRaw createInputCustom() {
		return new ITRawComboBox(this);
	}

	/**
	 * Set the values that are selectable from the combo box.
	 *
	 * @param values The values that are selectable from the combo box.
	 */
	public void setValues(String[] values) {
		this.values = values;
	}

	/**
	 * Returns the values that are selectable from the combo box.
	 *
	 * @return The values that are selectable from the combo box.
	 */
	public String[] getValues() {
		return values;
	}

}
