package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import javax.swing.BorderFactory;

/**
 * An title object that can be used for any kind of subtitle bars.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class XSubTitle extends JPanel {

    /**
     * The JLabel object where to display the text of the subtitle.
     */
    protected MultiLineTextLabel label;
    protected JPanel innerWrapper;

    /**
     * Constructor.
     *
     * Set the design of the title bar.
     *
     * @param title The text of the title.
     */
    public XSubTitle(String title) {
        super();

        setLayout(new BorderLayout());
        setBorder(new EmptyBorder(0, 0, Fonts.TEXT_PARAGRAPH_PADDING, 0));

        innerWrapper = new JPanel(new BorderLayout());
        innerWrapper.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Colors.BOOK_COLOR_DARKER));
        setOpaque(false);

        label = new MultiLineTextLabel("<html><body><b>" + title + "</b></body></html>");
        label.setStyleSheet("body {font-size: 11px;}");
        label.setFont(Fonts.FONT_BIG_PLAIN);
        label.setForeground(Color.BLACK);
        label.setOpaque(false);
        label.setBorder(new EmptyBorder(0, 5, 0, 5));
        innerWrapper.add(BorderLayout.CENTER, label);
        add(BorderLayout.CENTER, innerWrapper);
    }

    /**
     * Set a new text to the title.
     *
     * @param text The text to display in the title.
     */
    public void setText(String text) {
        label.setText(text);
    }

    @Override
    public void setOpaque(boolean isOpaque) {
        if (innerWrapper != null) {
            innerWrapper.setOpaque(isOpaque);
        }
    }
}
