package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.AbstractInputConstraints;

/**
 * A helper class to keep the input constraints and the corresponding ColumnType together.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITConstraintsHelper {

	/**
	 * The input constraints.
	 */
	private final AbstractInputConstraints constraint;
	/**
	 * The corresponding ColumnType of the input field.
	 */
	private final ColumnType columnType;

	/**
	 * Constructor.
	 *
	 * Initializes the helper class.
	 *
	 * @param constraint The inut constraints.
	 */
	public ITConstraintsHelper(AbstractInputConstraints constraint) {
		this.constraint = constraint;
		this.columnType = constraint.getColumnType();
	}

	/**
	 * Returns the ColumnType of the input field.
	 *
	 * @return The ColumnType of the input field.
	 */
	public ColumnType getColumnType() {
		return columnType;
	}

	/**
	 * Returns the input constraints.
	 *
	 * @return The input constraints.
	 */
	public AbstractInputConstraints getConstraint() {
		return constraint;
	}

}
