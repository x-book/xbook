package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.ITRawInvisibleField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntryRoot;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.dependable.AbstractBaseDependableEntry;

/**
 * @author Daniel Kaltenthaler
 */
public class InvisibleConstraints extends AbstractInputConstraints {

    /**
     * The value that should be returned by an invisible field.
     */
    private final String value;

    /**
     * Constructor.
     * <p>
     * Initialize the constraints.
     *
     * @param entry      The corresponding Entry object of the input field.
     * @param columnType The corresponding ColumnType of the input field.
     * @param value      The value that should be returned by an invisible field.
     */
    public InvisibleConstraints(AbstractEntryRoot entry, ColumnType columnType, String value) {
        // parameters "width", "height", and "borderEast" set to "0"
        // to avoid that the cell is displayed
        super(entry, columnType, 0, 0, 0);
        this.value = value;
    }

    @Override
    protected AbstractITRaw createInputCustom() {
        return new ITRawInvisibleField(this);
    }

    /**
     * Returns the value for this invisible field.
     *
     * @return The value for this invisible field.
     */
    public String getValue() {
        return value;
    }
}
