package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.ComboBoxConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XComboBox;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.metal.MetalComboBoxButton;

/**
 * The raw combobox field.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 * @param <C> The corresponding input constraints for the raw input field.
 */
public class ITRawComboBox<C extends ComboBoxConstraints> extends AbstractITRaw<C> {

	private XComboBox<String> comboBox;

	public ITRawComboBox(C constraints) {
		super(constraints);
	}

	@Override
	protected JComponent create() {
		JPanel wrapper = new JPanel(new BorderLayout());
		comboBox = new XComboBox<>();
		comboBox.setBorder(new EmptyBorder(0, 0, 0, 0));
		comboBox.setMinimumSize(new Dimension(constraints.getWidth(), 30));
		comboBox.setMaximumSize(new Dimension(constraints.getWidth(), 30));
		comboBox.setUI(new BasicComboBoxUI());
		wrapper.add(BorderLayout.CENTER, comboBox);
		return wrapper;
	}

	@Override
	public String getValue() {
		if (comboBox.getSelectedItem() == null) {
			return "-1";
		}
		return comboBox.getSelectedItem().toString();
	}

	@Override
	public boolean isEmpty() {
		return comboBox.getSelectedItem()==null;
	}

	@Override
	public void setValue(String value) {
		comboBox.setSelectedItem(value);
	}

	@Override
	public void clear() {
		comboBox.setSelectedItem("");
	}

	@Override
	public boolean isValidInput() {
		return !getValue().equals("-1");
	}

	@Override
	public void actionOnFocusGain() {
		comboBox.setBackground(Color.YELLOW);
	}

	@Override
	public void actionOnFocusLost() {
		super.actionOnFocusLost();
		comboBox.setBackground(Color.WHITE);
	}

	@Override
	public ArrayList<Component> getMyFocusableComponents() {
		ArrayList<Component> comp = new ArrayList<>();
		comp.add(comboBox);
		return comp;
	}

	@Override
	public String getStringRepresentation() {
		return getValue();
	}


	@Override
	public void addSidebarFunktionality() {
		for (Component c : comboBox.getComponents()) {
			c.addFocusListener(getSidebarFunctionality());
		}
	}

}
