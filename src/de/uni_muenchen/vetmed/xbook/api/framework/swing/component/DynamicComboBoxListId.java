package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import java.util.*;

/**
 * Extends the DynamicComboBox with a list that allows adding several elements out of the combo box.
 *
 * @param <T>
 * @author Daniel Kaltenthaler
 * @version 1.0.0
 */
public abstract class DynamicComboBoxListId<T> extends AbstractDynamicComboBoxList<T, LinkedHashMap<String, T>> {

    public DynamicComboBoxListId(LinkedHashMap<String, T> hashData, String name) {
        super(hashData, name);
    }

    public DynamicComboBoxListId(String name) {
        this(new LinkedHashMap<String, T>(), name);
    }

    public void setItems(LinkedHashMap<String, T> items) {
        setItems(items, true);
    }

    public void setItems(LinkedHashMap<String, T> items, boolean sort) {
        this.databaseData = items;

        ArrayList<T> list = new ArrayList<>(items.values());
        list.add(getEmptyItem());
        if (sort) {
            Collections.sort(list, getComparator());
        }
        setItemsToCombo(list);
    }

    @Override
    public void addInputToList() {
        T selected = getSelectedComboItem();
        String input = getComboTextFieldInput();
        if (input.isEmpty() && (selected == null || selected == getEmptyItem())) {
            return;
        }
        if (databaseData.containsValue(selected)) {
            addItemToList(selected);
            actionOnValueFound();
        } else if (databaseData.containsKey(input)) {
            addItemToList(databaseData.get(input));
            actionOnValueFound();
        } else {
            actionOnValueNotFound();
        }

        setSelectedComboItem(getEmptyItem());
    }

    @Override
    protected Object[] getValuesForCombo() {
        if (databaseData == null) {
            return null;
        }
        Collection<T> data = databaseData.values();
        if (data.isEmpty()) {
            return null;
        }
        return databaseData.values().toArray();
    }

    @Override
    protected boolean containsValue(T value) {
        return databaseData.containsValue(value);
    }

    /**
     *
     */
    public abstract static class DynamicComboBoxListIdString extends DynamicComboBoxListId<String> {

        public DynamicComboBoxListIdString(LinkedHashMap<String, String> hashData, String name) {
            super(hashData, name);
        }

        @Override
        public String getEmptyItem() {
            return "";
        }

        @Override
        public Comparator<String> getComparator() {
            return new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.toUpperCase().compareTo(o2.toUpperCase());
                }
            };
        }
    }
}
