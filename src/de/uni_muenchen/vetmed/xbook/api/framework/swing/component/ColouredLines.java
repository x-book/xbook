package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import java.awt.event.MouseListener;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class ColouredLines extends JPanel {

    /**
     * The current selected project line. -1 if no project is selected.
     */
    protected int currentSelected = -1;
    /**
     * The default height of one project line.
     */
    private final static int DEFAULT_LINE_HEIGHT = 30;
    /**
     * The outer wrappers of the single project lines. Used for scaling with a
     * border layout.
     */
    protected ArrayList<JPanel> wrapperLineOuter;
    /**
     * The inner wrappers of the single project lines where to add the single
     * project information.
     */
    protected ArrayList<JPanel> wrapperLineInner;
    /**
     * The inner wrappers of the single project lines where to add the single
     * project information.
     */
    protected ArrayList<Line> allLines;
    protected int lineHeight;

    public ColouredLines() {
        this(DEFAULT_LINE_HEIGHT);
    }

    public ColouredLines(int lineHeight) {
        super();
        setLayout(new StackLayout());
        this.lineHeight = lineHeight;
        wrapperLineOuter = new ArrayList<>();
        wrapperLineInner = new ArrayList<>();
        allLines = new ArrayList<>();
    }

    /**
     * Creates a line that is able to display all necessary information.
     *
     * The method will add the outer wrapper of this line to the JPanel and will
     * return the (inner) element wrapper. The element wrapper can be used to
     * display the specific information.
     *
     * @param index The index of the line data (used for colourize the content).
     * @return The final project line as a JPanel.
     */
    public Line addLine(final int index) {
        Line line = new Line(index);
        allLines.add(line);
        wrapperLineOuter.add(line.getOuterWrapper());
        wrapperLineInner.add(line.getElementWrapper());
        add(line);
        return line;
    }

    /**
     * Set a specific project as selected project.
     *
     * Set the current project into the class variable and colorize the active
     * project.
     *
     * @param index The index of the specific project in the ArrayLists of the
     * class variables.
     */
    private void setSelectedLine(int index) {
        if (currentSelected != -1) {
            wrapperLineInner.get(currentSelected).setBackground(allLines.get(currentSelected).getBackgroundColor());
            wrapperLineOuter.get(currentSelected).setBackground(allLines.get(currentSelected).getBackgroundColor());
            allLines.get(currentSelected).setBackground(allLines.get(currentSelected).getBackgroundColor());
        }
        currentSelected = index;
        if (index != -1) {
            wrapperLineInner.get(index).setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
            wrapperLineOuter.get(index).setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
            allLines.get(index).setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
        }
    }

    public int getLineHeight() {
        return lineHeight;
    }

    public JPopupMenu getPopupMenu() {
        return null;
    }

    public abstract void getActionOnDoubleClick(int index);

    /**
     *
     */
    public class Line extends JPanel {

        private final JPanel outerWrapper;
        private final JPanel elementWrapper;
        private int index;
        private MouseListener ml = null;

        public Line(final int index) {
            this.index = index;
            setLayout(new BorderLayout());

            // add the innner and outer wrapper to the arraylist
            this.outerWrapper = new JPanel(new BorderLayout());
            this.elementWrapper = new JPanel();

            // colorize the background
            elementWrapper.setBackground(getBackgroundColor());
            outerWrapper.setBackground(getBackgroundColor());

            // build the content
            outerWrapper.add(BorderLayout.CENTER, elementWrapper);
            // outerWrapper.setPreferredSize(new Dimension(1, lineHeight));
            add(BorderLayout.NORTH, outerWrapper);

            // now set the mouse listener
            setMouseListener(elementWrapper);
            setMouseListener(outerWrapper);
            setMouseListener(this);
        }

        public MouseListener getMouseListener() {
            if (ml == null) {
                ml = new MouseAdapter() {
                    private boolean clickedState = false;

                    @Override
                    public void mouseClicked(MouseEvent e) {
                        // on double click open the project
                        if (e.getClickCount() == 2) {
                            getActionOnDoubleClick(index);
                        }
                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {
                        if (clickedState) {
                            // on single left click select the project
                            if (e.getButton() == MouseEvent.BUTTON1) {
                                elementWrapper.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_LIGHT);
                                outerWrapper.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_LIGHT);
                                setBackground(Colors.TABLE_LINE_SELECTED_COLOR_LIGHT);
                                setSelectedLine(index);
                                clickedState = false;
                            }
                            // on single right click open the popup menu
                            if (e.getButton() == MouseEvent.BUTTON3) {
                                if (getPopupMenu() != null) {
                                    setSelectedLine(index);
                                    getPopupMenu().show(e.getComponent(), e.getX(), e.getY());
                                }
                            }
                        }
                    }

                    @Override
                    public void mousePressed(MouseEvent e) {
                        clickedState = true;
                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {
                        elementWrapper.setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                        outerWrapper.setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                        setBackground(Colors.TABLE_LINE_HOVERED_COLOR);
                    }

                    @Override
                    public void mouseExited(MouseEvent e) {
                        clickedState = false;

                        if (currentSelected == index) {
                            elementWrapper.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_LIGHT);
                            outerWrapper.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_LIGHT);
                            setBackground(Colors.TABLE_LINE_SELECTED_COLOR_LIGHT);
                            return;
                        }
                        if (index != currentSelected) {
                            elementWrapper.setBackground(getBackgroundColor());
                            outerWrapper.setBackground(getBackgroundColor());
                            setBackground(getBackgroundColor());
                        }
                    }
                };
            }
            return ml;
        }

        protected void setMouseListener(JPanel panel) {
            panel.addMouseListener(getMouseListener());
        }

        public Color getBackgroundColor() {
            if (index % 2 == 1) {
                return Colors.CONTENT_BACKGROUND;
            } else {
                return Colors.SIDEBAR_BACKGROUND;
            }
        }

        public JPanel getElementWrapper() {
            return elementWrapper;
        }

        public JPanel getOuterWrapper() {
            return outerWrapper;
        }
    }

}
