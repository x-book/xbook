package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.CustomDate;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTableOld;
import de.uni_muenchen.vetmed.xbook.api.exception.InvalidDateException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiDateChooser;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiDateElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Kaltenthaler
 */
public class RawMultiDateChooser extends AbstractInputElementFunctions {

    private final ColumnType columnTypeFrom;
    private final ColumnType columnTypeTo;
    private MultiDateChooser dateChooser;

    public RawMultiDateChooser(ColumnType columnTypeFrom, ColumnType columnTypeTo) {
        super(columnTypeFrom);
        this.columnTypeFrom = columnTypeFrom;
        this.columnTypeTo = columnTypeTo;
    }

    @Override
    public void load(DataSetOld data) {
        DataTableOld dataTable = data.getOrCreateDataTable(getTableName());
        for (DataRow list : dataTable) {
            CustomDate element1, element2;
            try {
                element1 = new CustomDate(list.get(columnTypeFrom));
                element2 = new CustomDate(list.get(columnTypeTo));
            } catch (InvalidDateException ex) {
                continue;
            }
            MultiDateElement element = new MultiDateElement(element1, element2);
            dateChooser.addItemToList(element);
        }
    }

    @Override
    public void save(DataSetOld data) {
        DataTableOld list = data.getOrCreateDataTable(getTableName());

        for (MultiDateElement mde : dateChooser.getListItems(false)) {
            if (mde != null) {
                DataRow row = new DataRow(getTableName());
                row.put(columnTypeFrom, mde.getBegin().parseToDatebaseDate());
                row.put(columnTypeTo, mde.getEnd().parseToDatebaseDate());
                list.add(row);
            }
        }
    }

    @Override
    public String getStringRepresentation() {
        String s = "";
        for (MultiDateElement mfce : dateChooser.getListItems(false)) {
            if (mfce != null) {
                s += mfce.getBegin() + "-" + mfce.getEnd() + ",";
            }
        }
        return s;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return dateChooser.getFocusableComponents();
    }

    @Override
    public void createField() {
        setLayout(new BorderLayout());

        dateChooser = new MultiDateChooser(columnType.getDisplayName());
        add(BorderLayout.CENTER, dateChooser);
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    @Override
    public void clear() {
        dateChooser.clearInput();
    }

    @Override
    public void setFocus() {
        dateChooser.requestFocusInWindow();
    }



    public boolean addElement(MultiDateElement element) {
        return dateChooser.addItemToList(element);
    }

    public boolean addElements(List<MultiDateElement> element) {
        return dateChooser.addItemsToList(element);
    }

    public List<MultiDateElement> getElementsFromList() {
        return dateChooser.getListItems(false);
    }

    public MultiDateElement getElementFromInput() {
        return dateChooser.getElementFromInput();
    }

    public void removeElement(MultiDateElement element) {
        dateChooser.removeItem(element);
    }

    public void removeElements(List<MultiDateElement> elements) {
        dateChooser.removeItems(elements);
    }

    public void removeElementAt(int index) {
        dateChooser.removeItem(index);
    }

    public MultiDateChooser getMultiDateChooser() {
        return dateChooser;
    }

}
