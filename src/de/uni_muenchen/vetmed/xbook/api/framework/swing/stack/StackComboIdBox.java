package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.CodeTableHashMap;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;

import javax.swing.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A stack element that holds a combo box.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackComboIdBox extends AbstractStackElement {

    /**
     * The combobox object.
     */
    private JComboBox combo;
    /**
     * The elements that are selectable in the combo box.
     */
    private CodeTableHashMap items;

    /**
     * Constructor.
     *
     * @param apiControllerAccess The basic main frame object.
     * @param labelText The label of the stack element.
     * @param labelWidth The width of the label.
     * @param columnType The database column data of the correspondenting input field.
     * @param addEmptyEntry State whether an empty entry should be added to the list or not.
     * @param sidebar The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
     */
    public StackComboIdBox(ApiControllerAccess apiControllerAccess, String labelText, Integer labelWidth, final ColumnType columnType, boolean addEmptyEntry, SidebarPanel sidebar) {
        super(columnType,labelText, labelWidth, sidebar);

        try {
            items = apiControllerAccess.getHashedCodeTableEntries(columnType);
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(StackComboIdBox.class.getName()).log(Level.SEVERE, null, ex);
        }

        combo.addItem("");
        for (String s : items.values()) {
            combo.addItem(s);
        }
        combo.setSelectedItem("");
        combo.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                setDefaultStyle();
            }
        });
        addCustomSidebarFunctionality(combo);
    }
    public StackComboIdBox(ApiControllerAccess apiControllerAccess, final ColumnType columnType, boolean addEmptyEntry, SidebarPanel sidebar) {
        this(apiControllerAccess,columnType.getDisplayName(),null,columnType,addEmptyEntry,sidebar);
    }

    /**
     * Constructor.
     *
     * @param apiControllerAccess The basic main frame object.
     * @param labelText The label of the stack element.
     * @param labelWidth The width of the label.
     * @param columnType The database column data of the correspondenting input field.
     * @param addEmptyEntry State whether an empty entry should be added to the list or not.
     * @param sidebarText The text that will be displayed into the sidebar.
     */
    public StackComboIdBox(ApiControllerAccess apiControllerAccess, String labelText, Integer labelWidth, final ColumnType columnType, boolean addEmptyEntry, String... sidebarText) {
        this(apiControllerAccess, labelText, labelWidth, columnType, addEmptyEntry, new SidebarPanel(sidebarText));
    }

    /**
     * Constructor.
     *
     * @param apiControllerAccess The basic main frame object.
     * @param labelText The label of the stack element.
     * @param labelWidth The width of the label.
     * @param columnType The database column data of the correspondenting input field.
     * @param addEmptyEntry State whether an empty entry should be added to the list or not.
     */
    public StackComboIdBox(ApiControllerAccess apiControllerAccess, String labelText, Integer labelWidth, final ColumnType columnType, boolean addEmptyEntry) {
        this(apiControllerAccess, labelText, labelWidth, columnType, addEmptyEntry, (SidebarPanel) null);
    }

    /**
     * Constructor.
     *
     * @param apiControllerAccess The basic main frame object.
     * @param labelText The label of the stack element.
     * @param columnType The database column data of the correspondenting input field.
     * @param addEmptyEntry State whether an empty entry should be added to the list or not.
     * @param sidebar The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
     */
    public StackComboIdBox(ApiControllerAccess apiControllerAccess, String labelText, final ColumnType columnType, boolean addEmptyEntry, SidebarPanel sidebar) {
        this(apiControllerAccess, labelText, null, columnType, addEmptyEntry, sidebar);
    }

    /**
     * Constructor.
     *
     * @param apiControllerAccess The basic main frame object.
     * @param labelText The label of the stack element.
     * @param columnType The database column data of the correspondenting input field.
     * @param addEmptyEntry State whether an empty entry should be added to the list or not.
     * @param sidebarText The text that will be displayed into the sidebar.
     */
    public StackComboIdBox(ApiControllerAccess apiControllerAccess, String labelText, final ColumnType columnType, boolean addEmptyEntry, String... sidebarText) {
        this(apiControllerAccess, labelText, null, columnType, addEmptyEntry, sidebarText);
    }

    /**
     * Constructor.
     *
     * @param apiControllerAccess The basic main frame object.
     * @param labelText The label of the stack element.
     * @param columnType The database column data of the correspondenting input field.
     * @param addEmptyEntry State whether an empty entry should be added to the list or not.
     */
    public StackComboIdBox(ApiControllerAccess apiControllerAccess, String labelText, final ColumnType columnType, boolean addEmptyEntry) {
        this(apiControllerAccess, labelText, null, columnType, addEmptyEntry, (SidebarPanel) null);
    }

    @Override
    public JComponent getElement() {
        combo = new JComboBox();
        combo.setFont(Fonts.FONT_STANDARD_PLAIN);
        return combo;
    }

    /**
     * Select a new item in the combo box.
     *
     * @param text The item that should be selected.
     */
    @Deprecated
    public void setSelectedItem(String text) {
        setValue(text);
    }

    @Override
    public String getValue() {
        if (combo.getSelectedItem() == null || combo.getSelectedItem().equals("")) {
            return "-1";
        }
        return items.convertStringToIdAsString((String) combo.getSelectedItem());
    }



    @Override
    public void clearInput() {
        combo.setSelectedItem("");
    }

    @Override
    public void setMandatoryStyle() {
        if (combo != null) {
            combo.setBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
            combo.setFont(Fonts.FONT_STANDARD_BOLD);
        }
    }

    @Override
    public void setErrorStyle() {
    }

    @Override
    public void setDefaultStyle() {
    }

    @Override
    public void setEnabled(boolean b) {
        if (combo != null) {
            combo.setEnabled(b);
        }
    }

    @Override
    public void addKeyListener(KeyListener l) {
        combo.addKeyListener(l);
    }

    @Override
    public void requestFocus() {
        combo.requestFocusInWindow();
    }

    public void setSelectedItem(Integer id) {
        if (id != null) {
            combo.setSelectedItem(items.convertIdAsStringToString(String.valueOf(id)));
        }
    }

    @Override
    public void setValue(String value) {
        combo.setSelectedItem(items.convertIdAsStringToString(value));
    }
}
