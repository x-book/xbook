package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * A JPanel that allows setting a repeatable background image.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class RepeatBackgroundPanel extends JPanel {

	/**
	 * The image to displayed.
	 */
	private Image image;

	/**
	 * Constructor of the RepeatBackgroundPanel class.
	 *
	 * @param image The image to be displayed as the background.
	 */
	public RepeatBackgroundPanel(Image image) {
		this.image = image;
	}

	/**
	 * Constructor of the RepeatBackgroundPanel class.
	 *
	 * @param image The image to be displayed as the background.
	 */
	public RepeatBackgroundPanel(ImageIcon image) {
		this(image.getImage());
	}

	/* (non-Javadoc)
	 * 
	 * Adds the functionality to repeat the background if there is enough space.
	 * Is working in horizontal and vertical direction.
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		int iw = image.getWidth(this);
		int ih = image.getHeight(this);
		if (iw > 0 && ih > 0) {
			for (int x = 0; x < getWidth(); x += iw) {
				for (int y = 0; y < getHeight(); y += ih) {
					g.drawImage(image, x, y, iw, ih, this);
				}
			}
		} else {
			g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		}
	}

	public void setImage(Image image) {
		this.image = image;
        revalidate();
        repaint();
	}
}
