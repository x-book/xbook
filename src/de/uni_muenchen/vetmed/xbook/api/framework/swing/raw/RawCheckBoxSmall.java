package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class RawCheckBoxSmall extends AbstractInputElementFunctions {

    /**
     * The text field object.
     */
    protected JCheckBox checkBox;

    public RawCheckBoxSmall(ColumnType columnType) {
        super(columnType);
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        boolean b = false;
        if (list.get(columnType).equals("1")) {
            checkBox.setSelected(true);
        }
    }

    @Override
    public void clear() {
        checkBox.setSelected(false);
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        if (checkBox.isSelected()) {
            list.add(new DataColumn("1", columnType.getColumnName()));
        } else {
            list.add(new DataColumn("0", columnType.getColumnName()));
        }
    }

    @Override
    public String getStringRepresentation() {
        if (checkBox.isSelected()) {
            return "1";
        } else {
            return "0";
        }
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(checkBox);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    public JCheckBox getCheckBox() {
        return checkBox;
    }




    public String getValue() {
        return toString();
    }

    public void setText(String b) {
        if (b.equals("1")) {
            checkBox.setSelected(true);
        } else {
            checkBox.setSelected(false);
        }
    }

    @Override
    public void createField() {
        setLayout(new BorderLayout());
        checkBox = new JCheckBox();
        checkBox.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
        add(BorderLayout.CENTER, checkBox);
    }

    @Override
    public void setFocus() {
        checkBox.requestFocusInWindow();
    }

}
