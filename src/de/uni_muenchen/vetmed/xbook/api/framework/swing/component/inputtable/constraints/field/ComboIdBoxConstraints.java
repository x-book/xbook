package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.ITRawComboIdBox;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntryRoot;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.dependable.AbstractBaseDependableEntry;

/**
 * @author Daniel Kaltenthaler
 */
public class ComboIdBoxConstraints extends AbstractInputConstraints {

    /**
     * Flag whether to addCell an empty item to the combo box, or not.
     */
    private boolean addEmptyItem = true;

    /**
     * Constructor.
     * <p>
     * Initialize the constraints.
     *
     * @param entry      The corresponding Entry object of the input field.
     * @param columnType The corresponding ColumnType of the input field.
     * @param width      The width of the input fields in pixels.
     * @param height     The height of the input fields in pixels.
     * @param borderEast The size of the border in the east.
     */
    public ComboIdBoxConstraints(AbstractEntryRoot entry, ColumnType columnType, int width, int height, int borderEast) {
        super(entry, columnType, width, height, borderEast);
    }

    /**
     * Constructor.
     * <p>
     * Initialize the constraints. Use this constructor to display no border in the east.
     *
     * @param entry      The corresponding Entry object of the input field.
     * @param columnType The corresponding ColumnType of the input field.
     * @param width      The width of the input fields in pixels.
     * @param height     The height of the input fields in pixels.
     */
    public ComboIdBoxConstraints(AbstractEntryRoot entry, ColumnType columnType, int width, int height) {
        this(entry, columnType, width, height, 0);
    }

    /**
     * Constructor.
     * <p>
     * Initialize the constraints. Use this constructor to display no border in the east and default cell height.
     *
     * @param entry      The corresponding Entry object of the input field.
     * @param columnType The corresponding ColumnType of the input field.
     * @param width      The width of the input fields in pixels.
     */
    public ComboIdBoxConstraints(AbstractEntryRoot entry, ColumnType columnType, int width) {
        this(entry, columnType, width, 1, 0);
    }

    @Override
    protected AbstractITRaw createInputCustom() {
        return new ITRawComboIdBox(this);
    }

    /**
     * Sets the flag whether to addCell an empty item to the combo box, or not.
     *
     * @param addEmptyItem The flag whether to addCell an empty item to the combo box, or not.
     */
    public void setAddEmptyItem(boolean addEmptyItem) {
        this.addEmptyItem = addEmptyItem;
    }

    /**
     * Get the flag whether to addCell an empty item to the combo box, or not.
     *
     * @return The flag whether to addCell an empty item to the combo box, or not.
     */
    public boolean getAddEmptyItem() {
        return addEmptyItem;
    }


}
