package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class XTimeField extends JTextField {

    public XTimeField() {
        super();
        addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                actionOnFocusLost();
                checkInput();
            }

            @Override
            public void focusGained(FocusEvent e) {
                actionOnFocusGained();
                checkInput();
            }
        });

        getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                actionOnInputChanged();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                actionOnInputChanged();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                actionOnInputChanged();
            }
        });
    }

    protected void actionOnFocusGained() {
    }

    protected void actionOnFocusLost() {
    }

    protected void actionOnInputChanged() {
    }

    private String convert(int number, int digit) {
        String buffer = String.valueOf(number);
        if (buffer.length() > digit) {
            return buffer;
        }
        while (buffer.length() != digit) {
            buffer = "0" + buffer;
        }
        return buffer;
    }

    /**
     * Check if the time has the correct format "HH:MM". Necessary because values are saved as "HH:MM:SS" in the
     * database!
     *
     * @return <code>true</code> if the time has the correct format,
     * <code>false</code> else.
     */
    public boolean checkInput(boolean updateText) {
        String[] akk = getText().split(":", 3);

        String input = "";
        for (String akk1 : akk) {
            input += akk1;
        }
        if (input.isEmpty()) {
            return false;
        }

        // If the input is a number, try to convert the number to a time
        // Examples:
        // - "1234" will be converted to 12:34
        // - "123" will be converted to 1:23
        // - "12" will be converted to 12:00
        // - "1" will be converted to 01:00
        // - invalid time values will be removed
        try {
            int number = Integer.parseInt(input);
            if (number < 0 || number > 2359) {
                throw new NumberFormatException();
            } else {
                String hours = "";
                String minutes = "";
                String numberAsString = number + "";
                if (numberAsString.length() == 1 || numberAsString.length() == 2) {
                    String num = convert(number, 2);
                    hours = num;
                    minutes = "00";
                } else if (numberAsString.length() == 3 || numberAsString.length() == 4) {
                    String num = convert(number, 4);
                    hours = num.substring(0, 2);
                    minutes = num.substring(2, 4);
                }
                if (Integer.parseInt(hours) < 0 || Integer.parseInt(hours) > 23
                        || Integer.parseInt(minutes) < 0 || Integer.parseInt(minutes) > 59) {
                    throw new NumberFormatException();
                }
                if (updateText) {
                    setText(hours + ":" + minutes);
                }
            }
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public boolean checkInput() {
        return checkInput(true);
    }

}
