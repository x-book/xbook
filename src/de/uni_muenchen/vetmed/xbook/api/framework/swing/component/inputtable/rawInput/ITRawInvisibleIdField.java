package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.InvisibleIdConstraints;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * The raw invisible field.
 * Should be used if a value has to be stored to the database, but not be displayed to the GUI.
 *
 * @param <C> The corresponding input constraints for the raw input field.
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITRawInvisibleIdField<C extends InvisibleIdConstraints> extends AbstractITRaw<C> {

    /**
     * Holds the value to store in the database.
     */
    private int valueToSave;
    private int dbID;
    String valueToSaveAsString;
    JLabel label;

    /**
     * Constructor.
     *
     * @param constraints The corresponding contraints for this class.
     */
    public ITRawInvisibleIdField(C constraints) {
        super(constraints);

        // add this row to the list of rows in the constraints.
        constraints.addRow(this);

        valueToSave = constraints.getNewNumber();
        dbID = constraints.getDBID();
        valueToSaveAsString = dbID + "|" + valueToSave;
        label.setText(valueToSaveAsString);
    }

    @Override
    protected JComponent create() {


        // do nothing special
        JPanel p = new JPanel(new BorderLayout());

        label = new JLabel();
        p.add(BorderLayout.CENTER, label);

        return p;
    }

    @Override
    public String getValue() {
        return valueToSaveAsString;
    }


    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public void setValue(String value) {
        if (value != null) {
            String[] split = value.split("\\|");
            dbID = Integer.parseInt(split[0]);
            valueToSave = Integer.parseInt(split[1]);
            valueToSaveAsString = dbID + "|" + valueToSave;
            label.setText(valueToSaveAsString);
        }
    }

    @Override
    public void clear() {
    }

    @Override
    public boolean isValidInput() {
        return false;
    }

    @Override
    public void actionOnFocusGain() {
    }

    @Override
    public void actionOnFocusLost() {
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return new ArrayList<>();
    }

    @Override
    public String getStringRepresentation() {
        return getValue();
    }

    @Override
    public void addSidebarFunktionality() {
        // not necessary
    }

    @Override
    public void actionOnRemoveRow() {
        constraints.removeRow(this);
    }

    public int getID() {
        return valueToSave;
    }

    public int getDBID() {
        return dbID;
    }
}
