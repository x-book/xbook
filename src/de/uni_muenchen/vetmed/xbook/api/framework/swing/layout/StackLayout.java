package de.uni_muenchen.vetmed.xbook.api.framework.swing.layout;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

/**
 * This class provides a layout quite similar to Sun's FlowLayout.
 *
 * The difference is that StackLayout realizes a vertical flow, meaning the
 * components are added top-to-bottom.
 *
 * @author unknown
 * @author Daniel Kaltenthaler
 * @author fnuecke
 * @author Johannes Lohrer
 */
public class StackLayout implements LayoutManager {

    /**
     * The horizontal gap of a component to its container's left and right
     * border.
     */
    private int hgap;
    /**
     * The vertical gap of a component to its container's left and right border.
     */
    private int vgap;

    /**
     * Sets up a layout where components are added top-to-bottom, without any
     * gaps to the container borders or between them.
     */
    public StackLayout() {
        this(0, 0);
    }

    /**
     * Sets up a layout where components are added top-to-bottom, separated from
     * each other and their container's borders by specified values.
     *
     * @param hgap the horizontal gap of a component to its container's left and
     * right border.
     * @param vgap the vertical gap between the components and their container's
     * top and bottom border.
     */
    public StackLayout(int hgap, int vgap) {
        this.hgap = hgap;
        this.vgap = vgap;
    }

    /**
     * Determines and returns the minimum size of the container, whereas the
     * height is the sum of the minimum height of all components, and the width
     * equals the minimum width of the widest component.
     */
    @Override
    public Dimension minimumLayoutSize(Container target) {
        synchronized (target.getTreeLock()) {
            Dimension dim = new Dimension(0, 0);
            int numComponents = target.getComponentCount();
            for (int i = 0; i < numComponents; i++) {
                Component c = target.getComponent(i);
                if (c.isVisible()) {
                    Dimension cDim = c.getMinimumSize();
                    dim.width = Math.max(cDim.width, dim.width);
                    dim.width += hgap;
                    dim.height += cDim.height;
                    dim.height += vgap;
                }
            }
            dim.height += vgap;
            return dim;
        }
    }

    /**
     * Determines and returns the preferred size of the container, whereas the
     * height is the sum of the preferred height of all components, and the
     * width equals the preferred width of the widest component.
     */
    @Override
    public Dimension preferredLayoutSize(Container target) {
        synchronized (target.getTreeLock()) {
            Dimension dim = new Dimension(0, 0);
            int numComponents = target.getComponentCount();
            for (int i = 0; i < numComponents; i++) {
                Component c = target.getComponent(i);
                if (c.isVisible()) {
                    Dimension cDim = c.getPreferredSize();
                    int width = Math.min(cDim.width, target.getParent().getWidth());
                    dim.width = Math.max(width, dim.width);
                    dim.width += hgap;
                    dim.height += cDim.height;
                    dim.height += vgap;
                }
            }
            dim.height += vgap;
            return dim;
        }
    }

    /**
     * Constructs the container by vertically adding all its components from
     * top-to-bottom, regarding possibly set gaps.
     */
    @Override
    public void layoutContainer(Container target) {
        synchronized (target.getTreeLock()) {
            int nmembers = target.getComponentCount();
            int y = 0;
            for (int i = 0; i < nmembers; i++) {
                Component m = target.getComponent(i);
                if (m.isVisible()) {
                    int heigth = m.getPreferredSize().height;
                    m.setBounds(0, y, target.getWidth(), heigth);
                    y += heigth;
                }
            }
        }
    }

    @Override
    public void removeLayoutComponent(Component c) {
    }

    @Override
    public void addLayoutComponent(String s, Component c) {
    }
}
