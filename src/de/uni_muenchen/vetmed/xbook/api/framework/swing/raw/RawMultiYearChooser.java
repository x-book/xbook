package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTableOld;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiYearChooser;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiYearElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Kaltenthaler
 */
public class RawMultiYearChooser extends AbstractInputElementFunctions {

    private final ColumnType columnTypeFrom;
    private final ColumnType columnTypeTo;
    private MultiYearChooser yearChooser;

    public RawMultiYearChooser(ColumnType columnTypeFrom, ColumnType columnTypeTo) {
        super(columnTypeFrom);
        this.columnTypeFrom = columnTypeFrom;
        this.columnTypeTo = columnTypeTo;
    }

    @Override
    public void load(DataSetOld data) {
        DataTableOld dataTable = data.getOrCreateDataTable(getTableName());
        for (DataRow list : dataTable) {
            MultiYearElement element = new MultiYearElement(Integer.parseInt(list.get(columnTypeFrom)), Integer.parseInt(list.get(columnTypeTo)));
            yearChooser.addItemToList(element);
        }
    }

    @Override
    public void save(DataSetOld data) {
        DataTableOld list = data.getOrCreateDataTable(getTableName());

        for (MultiYearElement mfce : yearChooser.getListItems(false)) {
            if (mfce != null) {
                DataRow row = new DataRow(getTableName());
                row.put(columnTypeFrom, mfce.getBegin() + "");
                row.put(columnTypeTo, mfce.getEnd() + "");
                list.add(row);
            }
        }
    }

    @Override
    public String getStringRepresentation() {
        String s = "";
        for (MultiYearElement mfce : yearChooser.getListItems(false)) {
            if (mfce != null) {
                s += mfce.getBegin() + "-" + mfce.getEnd() + ",";
            }
        }
        return s;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return yearChooser.getFocusableComponents();
    }

    @Override
    public void createField() {
        setLayout(new BorderLayout());

        yearChooser = new MultiYearChooser(columnType.getDisplayName());
        add(BorderLayout.CENTER, yearChooser);
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    @Override
    public void clear() {
        yearChooser.clearInput();
    }

    @Override
    public void setFocus() {
        yearChooser.requestFocusInWindow();
    }


    public boolean addElement(MultiYearElement element) {
        return yearChooser.addItemToList(element);
    }

    public boolean addElements(List<MultiYearElement> element) {
        return yearChooser.addItemsToList(element);
    }

    public List<MultiYearElement> getElementsFromList() {
        return yearChooser.getListItems(false);
    }

    public MultiYearElement getElementFromInput() {
        return yearChooser.getElementFromInput();
    }

    public void removeElement(MultiYearElement element) {
        yearChooser.removeItem(element);
    }

    public void removeElements(List<MultiYearElement> elements) {
        yearChooser.removeItems(elements);
    }

    public void removeElementAt(int index) {
        yearChooser.removeItem(index);
    }

    public MultiYearChooser getMultiYearChooser() {
        return yearChooser;
    }

}
