package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.languageChooser;

import java.awt.Component;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.*;

/**
 * The language chooser is an extended JComboBox that holds labels of languages and the correspondenting national flag.
 * 
 * The combo box elements will return the language value as the language code (ISO 639-1). In case the language code does not exist for
 * this language the language code ISO 639-2 or ISO 639-3 are used.
 * 
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class LanguageChooser extends JComboBox {

	/**
	 * An enumeration of all languages that are supported.
	 */
	public enum Language {

		GERMAN("de", "German", "Deutsch"),
		ENGLISH("en", "English", "English"),
		FRENCH("fr", "French", "Français"),
        SWISS("de_ch", "Swiss", "Schweizerisch"),
		SPANISH("es", "Spanish", "Español");
		/**
		 * The language code of the language (in general ISO 639-1. The codes ISO 639-2 or ISO 639-3 are used when no language code is available.
		 */
		private final String languageCode;
		/**
		 * The name of the language in English language.
		 */
		private final String languageNameEnglish;
		/**
		 * The name of the language in vernacular language.
		 */
		private final String languageNameVernacular;

		/**
		 * Constructor.
		 * 
		 * @param languageCode The language code of the language (in general ISO 639-1. The codes ISO 639-2 or ISO 639-3 are used when no language code is available).
		 * @param nameEnglish The name of the language in English language.
		 * @param nameLanguage The name of the language in vernacular language.
		 */
		Language(String languageCode, String nameEnglish, String nameLanguage) {
			this.languageCode = languageCode;
			this.languageNameEnglish = nameEnglish;
			this.languageNameVernacular = nameLanguage;
		}

		/**
		 * Returns the langauge code (in general ISO 639-1. The codes ISO 639-2 or ISO 639-3 are used when no language code is available).
		 * 
		 * @return The language code.
		 */
		public String getLanguageCode() {
			return languageCode;
		}

		/**
		 * Returns the name of the language in English language.
		 * 
		 * @return The name of the language in English language.
		 */
		public String getNameEnglish() {
			return languageNameEnglish;
		}

		/**
		 * Returns the name of the language in English language.
		 * @return The name of the language in vernacular language.
		 */
		public String getNameVernacular() {
			return languageNameVernacular;
		}
	}

	/**
	 * Constructor.
	 * 
	 * Creates an empty combo box.
	 */
	public LanguageChooser() {
		this(false);
	}

	/**
	 * Constructor.
	 * 
	 * Creates a combo box with the defined languages in the parameter.
	 * 
	 * @param list The languages to add.
	 */
	public LanguageChooser(ArrayList<Language> list) {
		this(false);
		for (Language lang : list) {
			addLanguage(lang);
		}
	}

	/**
	 * Constructor.
	 * 
	 * Creates a combo box with the defined languages in the parameter.
	 * 
	 * @param list The languages to add.
	 */
	public LanguageChooser(Language[] list) {
		this(false);
		for (Language lang : list) {
			addLanguage(lang);
		}
	}

	/**
	 * Constructor.
	 * 
	 * If the parameter <code>addAll</code> is <code>true</code> all available elements that are defined in the enumeration will be added
	 * to the combo box.
	 * If <code>addAll</code> is <code>false</code> the combobox is empty and have to be filled manually by using the method
	 * <code>addLanguage</code>.
	 * 
	 * @param addAll Whether all available languages should be added or not.
	 */
	public LanguageChooser(boolean addAll) {
		super();

		if (addAll) {
			addAllAvailableLanguages();
		}

		// Create the combo box.
		setRenderer(new ComboBoxRenderer());

	}

	/**
	 * Add a single language to the combo box.
	 * 
	 * @param language The language to add.
	 */
	public void addLanguage(Language language) {
		addItem(language.getLanguageCode());
	}

	/**
	 * Add all available languages that are defined in the enumeration
	 */
	public void addAllAvailableLanguages() {
		removeAllItems();
		for (Language lang : Language.values()) {
			addLanguage(lang);
		}
	}

	/**
	 * Get the English name of a specific language code.
	 * 
	 * @param languageCode The language code.
	 * @return The English name.
	 */
	public String getLanguageNameEnglish(String languageCode) {
		for (Language lang : Language.values()) {
			if (languageCode.equals(lang.getLanguageCode())) {
				return lang.getNameEnglish();
			}
		}
		return "<" + languageCode + " not available>";
	}

	/**
	 * Get the vernacular name of a specific language code.
	 * 
	 * @param languageCode The language code.
	 * @return The Vernacular name.
	 */
	public String getLanguageNameVernacular(String languageCode) {
		for (Language lang : Language.values()) {
			if (languageCode.equals(lang.getLanguageCode())) {
				return lang.getNameVernacular();
			}
		}
		return "<" + languageCode + " not available>";
	}


	/**
	 * Get the vernacular name of a specific language code.
	 * 
	 * @param languageCode The language code.
	 * @return The Vernacular name.
	 */
	public Language getLanguageObject(String languageCode) {
		for (Language lang : Language.values()) {
			if (languageCode.equals(lang.getLanguageCode())) {
				return lang;
			}
		}
		return null;
	}
	/**
	 * Returns an ImageIcon, or null if the path was invalid.
	 * 
     * @param id
	 * @param The language id.
	 * @return The ImageIcon object with the correspondenting language flag.
	 */
	protected static ImageIcon createLanguageIcon(String id) {
		String path = "/images/" + id + ".png";
		URL imgURL = LanguageChooser.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	/**
	 * The custom renderer for the combo box.
	 */
	class ComboBoxRenderer extends JLabel implements ListCellRenderer {

		/**
		 * Constructor.
		 */
		public ComboBoxRenderer() {
			setOpaque(true);
			setHorizontalAlignment(LEFT);
			setVerticalAlignment(CENTER);
		}

		/**
		 * This method finds the image and text corresponding to the selected value and returns the label,
		 * set up to display the text and image.
		 */
        @Override
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

			// colorise the element
			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}

			// set the text
			setText(getLanguageNameEnglish(value.toString()) + " (" + getLanguageNameVernacular(value.toString()) + ")");
			// set the icon
			setIcon(createLanguageIcon(value.toString()));

			return this;
		}
	}
}
