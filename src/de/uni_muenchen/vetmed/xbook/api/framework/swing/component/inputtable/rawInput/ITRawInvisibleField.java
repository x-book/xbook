package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.InvisibleConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.TextFieldConstraints;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;

/**
 * The raw invisible field.
 * Should be used if a value has to be stored to the database, but not be displayed to the GUI.
 *
 * @param <C> The corresponding input constraints for the raw input field.
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITRawInvisibleField<C extends InvisibleConstraints> extends AbstractITRaw<C> {

    /**
     * Constructor.
     *
     * @param constraints The corresponding contraints for this class.
     */
    public ITRawInvisibleField(C constraints) {
        super(constraints);
    }

    @Override
    protected JComponent create() {
        // do nothing special
        JPanel p = new JPanel(new BorderLayout());
        p.add(BorderLayout.CENTER, new JLabel("Should not be visible. Hopefully."));
        return p;
    }

    @Override
    public String getValue() {
        return constraints.getValue();
    }

    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public void setValue(String value) {
    }

    @Override
    public void clear() {
    }

    @Override
    public boolean isValidInput() {
        return false;
    }

    @Override
    public void actionOnFocusGain() {
    }

    @Override
    public void actionOnFocusLost() {
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return new ArrayList<>();
    }

    @Override
    public String getStringRepresentation() {
        return getValue();
    }

    @Override
    public void addSidebarFunktionality() {
        // not necessary
    }

}
