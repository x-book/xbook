package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;

import javax.swing.*;

/**
 * @author Eduardo Granado <eduardo.granado@unibas.ch>
 */
public class XTextField extends JTextField {

    public XTextField() {
        this(false);
    }

    public XTextField(boolean mandatory) {
        super();
        setFont(Fonts.FONT_VERYBIG_BOLD);

        if (mandatory) {
            setMandatoryStyle();
        }
    }

    public void setMandatoryStyle() {
        setBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
    }
}
