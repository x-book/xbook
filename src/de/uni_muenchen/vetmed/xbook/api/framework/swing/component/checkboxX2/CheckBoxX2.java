package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.checkboxX2;

import javax.swing.*;
import java.net.URL;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class CheckBoxX2 extends JCheckBox {

    public CheckBoxX2() {
        this("");
    }

    public CheckBoxX2(String text) {
        super(text);

        // Set default icon for checkbox
        setIcon(createIcon("default_icon.png"));
        // Set selected icon when checkbox state is selected
        setSelectedIcon(createIcon("selected.png"));
        // Set disabled icon for checkbox
        setDisabledIcon(createIcon("disabled.png"));
        // Set disabled-selected icon for checkbox
        setDisabledSelectedIcon(createIcon("disabled_selected.png"));
        // Set checkbox icon when checkbox is pressed
        setPressedIcon(createIcon("pressed.png"));
        // Set icon when a mouse is over the checkbox
        setRolloverIcon(createIcon("rollover.png"));
        // Set icon when a mouse is over a selected checkbox
        setRolloverSelectedIcon(createIcon("rollover_selected.png"));
    }

    /**
     * Returns an ImageIcon, or null if the path was invalid.
     *
     * @param fileName The file name.
     * @return The ImageIcon object with the correspondenting language flag.
     */
    protected static ImageIcon createIcon(String fileName) {
        String path = "/images/" + fileName;
        URL imgURL = CheckBoxX2.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
}
