package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.FileTooLargeException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.documentHelper.LimitInputLengthDocument;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.MathsHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiFileChooserElement;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class FileChooser extends JPanel {

    /**
     * The text field object where the file path and name is displayed.
     */
    protected JTextField textField;
    /**
     * The button to open the file chooser to select a file.
     */
    private XButton buttonLoad;
    /**
     * The button to open the selected file.
     */
    private XButton buttonOpen;

    private long maxFileSize;

    /**
     * The filter that defined which files can be selected.
     */
    private FileNameExtensionFilter filter;
    /**
     * The file that was loaded recently.
     */
    protected File lastLoadedFile = null;

    public static String ENCODING = "ISO-8859-1";
    byte[] bytes;
    Base64 encoder = new Base64();
    String lastSavedString;

    public FileChooser() {
        this(null);
    }

    public FileChooser(FileNameExtensionFilter fileFilter) {
        this.filter = fileFilter;
        setLayout(new BorderLayout());

        add(BorderLayout.WEST, getOpenButton());
        add(BorderLayout.CENTER, ComponentHelper.wrapComponent(getTextField(), 0, 3, 0, 3));
        add(BorderLayout.EAST, ComponentHelper.wrapComponent(getSelectButton(), 0, 3, 0, 3));
    }

    public FileChooser(FileNameExtensionFilter fileFilter, long maxFileSize) {
        this(fileFilter);
        setMaxFileSize(maxFileSize);
    }

    public void setMaxInputLength(Integer length) {
        if (length == null) {
            textField.setDocument(new LimitInputLengthDocument(9999));
        } else {
            textField.setDocument(new LimitInputLengthDocument(length));
        }
    }

    public JTextField getTextField() {
        if (textField == null) {
            textField = new JTextField();
            textField.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
            textField.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
            textField.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    if (textField.getText().isEmpty()) {
                        textField.setToolTipText(null);
                    } else {
                        textField.setToolTipText(textField.getText());
                    }
                }
            });
            textField.setEnabled(false);
        }
        return textField;
    }

    public XButton getSelectButton() {
        if (buttonLoad == null) {
            buttonLoad = new XButton(Loc.get("LOAD"));
            buttonLoad.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    actionOnSelectButton();
                }
            });
        }
        return buttonLoad;
    }

    protected void actionOnSelectButton() {
        JFileChooser chooser = new JFileChooser(lastLoadedFile);
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(FileChooser.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            lastLoadedFile = chooser.getSelectedFile();
            textField.setText(lastLoadedFile.getName());
            try {
                onFileChoosen(lastLoadedFile);
            } catch (FileTooLargeException e1) {
                textField.setText("");

                double fileSize = MathsHelper.round(lastLoadedFile.length() / 1024.0 / 1024, 1);
                double allowedSize = getMaxFileSizeInMB();
                Footer.displayError(Loc.get("THE_FILE_IS_TOO_LARGE", fileSize, allowedSize));
            }
        }
    }

    public XButton getOpenButton() {
        if (buttonOpen == null) {
            buttonOpen = new XButton(Loc.get("OPEN"));
            buttonOpen.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        if (bytes != null) {
                            openSelectedFile();
                        } else {
                            System.out.println("no image found");
                            //TODO print warning or stuff!
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }

            });
        }
        return buttonOpen;
    }

    private void openSelectedFile() throws IOException {
        openFile(textField.getText(), bytes);
    }

    public void openFile(String fileName, byte[] file) throws IOException {
        File directory = new File("tmp");
        directory.mkdirs();
        FileUtils.cleanDirectory(directory);
        File f = new File(directory, fileName);
        f.createNewFile();

        FileOutputStream fos = new FileOutputStream(f);
        fos.write(file);
        fos.close();

        Desktop.getDesktop().open(f);
    }

    public void openFile(MultiFileChooserElement mfce) throws IOException {
        if (mfce == null) {
            Footer.displayWarning(Loc.get("NO_FILE_SELECTED"));
            return;
        }
        openFile(mfce.getFileName(), getDecodedFile(mfce.getFileEncoded()));
    }

    protected void onFileChoosen(File lastLoadedFile) throws FileTooLargeException {

        if (lastLoadedFile.length() > maxFileSize) {
            System.out.println("File size " + lastLoadedFile.length() + " is bigger than the maximum file size of " + maxFileSize);
            throw new FileTooLargeException();
        }
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final InputStream in;
        try {
            in = new FileInputStream(lastLoadedFile);

            final byte[] buffer = new byte[500];
            int read = -1;
            while ((read = in.read(buffer)) > 0) {
                baos.write(buffer, 0, read);
            }
            in.close();
            bytes = baos.toByteArray();

        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public String getFileName() {
        return textField.getText();
    }

    public void setFileName(String fileName) {
        this.textField.setText(fileName);
    }

    public byte[] getFile() {
        return bytes;
    }

    public String getFileEncoded() {
        if (getFile() == null) {
            return null;
        }
        return encoder.encodeToString(getFile());
    }

    public void setFile(String file) {
        try {
            bytes = getDecodedFile(file);
        } catch (Exception e) {
            bytes = null;
            e.printStackTrace();
        }
    }

    public byte[] getDecodedFile(String file) {
        return encoder.decode(file);
    }

    public ArrayList<Component> getFocusableElements() {
        ArrayList<Component> list = new ArrayList<>();
        list.add(getTextField());
        list.add(getOpenButton());
        list.add(getSelectButton());
        return list;
    }

    public boolean isValidInput() {
        return getFileName() != null && !getFileName().isEmpty() && bytes != null;
    }

    public void clear() {
        bytes = null;
        getTextField().setText("");
    }

    protected void setFileFilter(FileNameExtensionFilter fileFilter) {
        this.filter = fileFilter;
    }

    public void setMaxFileSize(long maxFileSize) {
        this.maxFileSize = maxFileSize;
    }

    public double getMaxFileSizeInMB() {
        return MathsHelper.round(maxFileSize / 1024.0 / 1024, 1);
    }

}
