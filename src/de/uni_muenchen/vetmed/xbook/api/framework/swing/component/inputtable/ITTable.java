package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.CellConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.ITConstraintsHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.ITLabelConstraintsHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.EmptyRowConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntryRoot;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.dependable.AbstractBaseDependableEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITTable extends JPanel {

    /**
     * Holds all rows of the table.
     */
    private ArrayList<ITTableRow> allRows = new ArrayList<>();

    protected AbstractEntryRoot entry;

    /**
     * Constructor.
     * <p>
     * Initializes the table.
     */
    public ITTable(AbstractEntryRoot entry) {
        super(new StackLayout());
        this.entry = entry;
    }

    /**
     * Add a new row to the table.
     * Automatically calls the create() method of the row.
     *
     * @param row The row to add.
     */
    public void addRow(ITTableRow row) {
        allRows.add(row);
        add(row.create());
    }

    /**
     * Loads data to the table.
     *
     * @param data The data to load.
     */
    public void load(DataSetOld data) {
        for (ITTableRow row : allRows) {
            row.load(data);
        }
    }

    /**
     * Saves the data of a table.
     *
     * @param data The object where to save the data.
     * @throws IsAMandatoryFieldException if a mandatory field has no input.
     */
    public void save(DataSetOld data) throws IsAMandatoryFieldException {
        for (ITTableRow row : allRows) {
            row.save(data);
        }
    }

    /**
     * Clear all data from the table, that means from all rows.
     */
    public void clear() {
        for (ITTableRow row : allRows) {
            row.clear();
        }
    }

    /**
     * Check if the table contains valid input.
     *
     * @return <code>true</code> if the table contains valid input, <code>false</code> else.
     */
    public boolean isValidInput() {
        for (ITTableRow row : allRows) {
            if (!row.isValidInput()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns an ArrayList of all focusable components of the table.
     *
     * @return An ArrayList of all focusable components of the table.
     */
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> elements = new ArrayList<>();
        for (ITTableRow row : allRows) {
            elements.addAll(row.getMyFocusableComponents());
        }
        return elements;
    }

    /**
     * Returns a String representation of the table.
     *
     * @return The String representation of the table.
     */
    public String getStringRepresentation() {
        return "not in use";
    }

    /**
     * Adds an empty row. Commonly used for the final row, to create a gap
     * at the bottom of the table and the insert a border at the bottom.
     *
     * @param borderNorth The width of the north border.
     * @return The empty row.
     */
    protected ITTableRow getEmptyRow(int borderNorth) {
        return getEmptyRow(borderNorth, 0);
    }

    /**
     * Adds an empty row. Commonly used for the final row, to create a gap
     * at the bottom of the table and the insert a border at the bottom.
     *
     * @param borderNorth The width of the north border.
     * @return The empty row.
     */
    protected ITTableRow getEmptyRow(int borderNorth, int borderSouth) {
        ITTableRow row = new ITTableRow();
        CellConstraints c = new CellConstraints(borderNorth, 0, borderSouth, 0, 0);
        row.addCell(c, new ITLabelConstraintsHelper(new EmptyRowConstraints(entry)));
        return row;
    }

}
