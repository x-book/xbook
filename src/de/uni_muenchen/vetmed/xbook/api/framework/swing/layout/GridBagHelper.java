package de.uni_muenchen.vetmed.xbook.api.framework.swing.layout;

import java.awt.GridBagConstraints;

/**
 * A helper class for the GridBagLayout.
 *
 * Helps to set GridBagConstraints within one method.
 *
 * @author Daniel Kaltenthaler
 */
public class GridBagHelper {

    /**
     * Sets a new x- and y-coordinate for the grid position and reset the height
     * and width of the grid back to 1.
     *
     * @param c The constraints where to save the settings
     * @param gridy The new y-coordinate of the grid.
     * @param gridx The new x-coordinate of the grid.
     */
    public static void setConstraints(GridBagConstraints c, int gridy, int gridx) {
        setConstraints(c, gridy, gridx, 1, 1);
    }

    /**
     * Sets a new x- and y-coordinate for the grid position and the height and
     * width of the grid.
     *
     * @param c The constraints where to save the settings
     * @param gridy The new y-coordinate of the grid.
     * @param gridx The new x-coordinate of the grid.
     * @param gridwidth The width of the grid.
     * @param gridheight The height of the grid.
     */
    public static void setConstraints(GridBagConstraints c, int gridy, int gridx, int gridwidth, int gridheight) {
        c.gridx = gridx;
        c.gridy = gridy;
        c.gridheight = gridheight;
        c.gridwidth = gridwidth;
    }

}
