package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiSelection;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.AbstractMainFrame;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler
 */
public class RawMultiSelection extends AbstractInputElementFunctions {

    /**
     * The multi selection object.
     */
    private MultiSelection multiSelection;
    /**
     * The label where to displayed the number of selected elements.
     */
    private JLabel countLabel;

    private AbstractMainFrame mainFrame;
    private CodeTableHashMap loadedData;

    /**
     * Constructor.
     *
     * @param columnType The basic column type object.
     */
    public RawMultiSelection(AbstractMainFrame mainFrame, ColumnType columnType) {
        super(columnType);
        this.mainFrame = mainFrame;
        loadAvailableData();
    }

    /**
     * Load the available data that should be selectable in the selection list.
     */
    private void loadAvailableData() {
        try {
            loadedData = mainFrame.getController().getHashedCodeTableEntries(columnType);
            Object[] dataArray = loadedData.getValuesAsArray(false);
            ArrayList<String> dataArrayList = new ArrayList<>();
            for (Object o : dataArray) {
                dataArrayList.add((String) o);
            }
            multiSelection.setAvailableData(dataArrayList);
            multiSelection.updateBoxes();
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(RawMultiSelection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void save(DataSetOld data) {
        DataTableOld list = data.getOrCreateDataTable(getTableName());
        for (String s : multiSelection.getSelectedData()) {
            DataRow row = new DataRow(getTableName());
            row.put(columnType, loadedData.convertStringToIdAsString(s));
            list.add(row);
        }
    }

    @Override
    public void load(DataSetOld data) {
        DataTableOld dataTable = data.getOrCreateDataTable(getTableName());
        for (DataRow list : dataTable) {
            multiSelection.setSelected(loadedData.convertIdAsStringToString(list.get(columnType)), true);
        }
        updateCountLabel();
    }

    @Override
    public String getStringRepresentation() {
        String string = "";
        for (String s : multiSelection.getSelectedData()) {
            string += s + ",";
        }
        if (string.endsWith(",")) {
            string = string.substring(0, string.length() - 1);
        }
        return string;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return multiSelection.getMyFocusableComponents();
    }

    @Override
    public void createField() {
        setLayout(new BorderLayout());

        multiSelection = new MultiSelection() {
            @Override
            protected void actionOnUpdateAnyCheckbox() {
                super.actionOnUpdateAnyCheckbox();
                updateCountLabel();
            }
        };

//        ArrayList<String> a = new ArrayList<>();
//        a.add("Cat 1");
//        a.add("Cat 2");
//        a.add("Cat 3");
//        a.add("Cat 4");
//        a.add("Cat 5");
//        a.add("Cat 6");
//        multiSelection.setAvailableData(a);
        add(BorderLayout.CENTER, multiSelection);

        countLabel = new JLabel("", JLabel.CENTER);
        updateCountLabel();
        add(BorderLayout.SOUTH, ComponentHelper.wrapComponent(countLabel, 4, 0, 0, 0));

    }

    /**
     * Updates the count label text and displays the current count of selected and available loadedData.
     */
    private void updateCountLabel() {
        String countLabelText;
        if (multiSelection.getAvailableData() == null || multiSelection.getAvailableData().size() == 0) {
            countLabelText = "–";
        } else {
            int selectedDataCount = 0;
            if (multiSelection.getSelectedData() != null && multiSelection.getSelectedData().size() != 0) {
                selectedDataCount = multiSelection.getSelectedData().size();
            }
            countLabelText =
                    Loc.get("SELECTED") + ": " + selectedDataCount + "/" + multiSelection.getAvailableData().size();
        }
        countLabel.setText(countLabelText);
    }

    @Override
    public boolean isValidInput() {
        System.out.println("RawMultiSelection: " + multiSelection.hasSelectedItems());
        return multiSelection.hasSelectedItems();
    }

    @Override
    public void clear() {
        multiSelection.clearSelection();
        updateCountLabel();
    }

    @Override
    public void setFocus() {
        multiSelection.requestFocusInWindow();
    }

    /**
     * Returns the MultiSelection object.
     *
     * @return The MultiSelection object.
     */
    public MultiSelection getMultiSelection() {
        return multiSelection;
    }

    /**
     * Set the state if the elements are always disabled (and therefore are only selectable by extern elements).
     *
     * @param areElementsAlwaysDisabled The state if the elements are always disabled, or not.
     */
    public void setAreElementsAlwaysDisabled(boolean areElementsAlwaysDisabled) {
        multiSelection.setAreElementsAlwaysDisabled(areElementsAlwaysDisabled);
    }
}
