package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiFileChooser;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiFileChooserElement;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;
import org.apache.commons.codec.digest.DigestUtils;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class RawMultiFileChooser extends AbstractInputElementFunctions {

    private final ColumnType columnTypeFileName;
    private final ColumnType columnTypeHash;
    private MultiFileChooser fileChooser;

    public RawMultiFileChooser(ColumnType columnTypeFile, ColumnType columnTypeFileName, ColumnType columnTypeHash, FileNameExtensionFilter fileFilter) {
        super(columnTypeFile);
        this.columnTypeFileName = columnTypeFileName;
        this.columnTypeHash = columnTypeHash;
        setFileFilter(fileFilter);
    }

    @Override
    public void load(DataSetOld data) {
        DataTableOld dataTable = data.getOrCreateDataTable(getTableName());
        for (DataRow list : dataTable) {
            final String fileName = list.get(columnTypeFileName);
            final String file = list.get(columnType);
            if (fileName != null && !fileName.isEmpty() && file != null && !(file.isEmpty())) {
                fileChooser.addItemToList(new MultiFileChooserElement(fileName, file));
            }
        }
    }

    @Override
    public void save(DataSetOld data) {
        DataTableOld list = data.getOrCreateDataTable(getTableName());
        if (list == null) {
            list = data.addDataTable(new DataTableOld(getTableName()));
        }
        for (MultiFileChooserElement mfce : fileChooser.getListItems(false)) {
            if (mfce != null) {
                DataRow row = new DataRow(getTableName());
                row.add(new DataColumn(mfce.getFileName(), columnTypeFileName));
                row.add(new DataColumn(mfce.getFileEncoded(), columnType));
                row.add(new DataColumn(DigestUtils.md5Hex(mfce.getFileEncoded()), columnTypeHash));
                list.add(row);
            }
        }
    }

    @Override
    public String getStringRepresentation() {
        String s = "";
        for (MultiFileChooserElement mfce : fileChooser.getListItems(false)) {
            if (mfce != null) {
                s += mfce.getFileName() + "," + mfce.getFileEncoded() + "/";
            }
        }
        return s;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return fileChooser.getFocusableComponents();
    }

    @Override
    public void createField() {
        setLayout(new BorderLayout());

        fileChooser = new MultiFileChooser(columnType.getDisplayName());
        add(BorderLayout.CENTER, fileChooser);
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    @Override
    public void clear() {
        fileChooser.clearInput();
    }

    @Override
    public void setFocus() {
        fileChooser.requestFocusInWindow();
    }


    public boolean addElement(MultiFileChooserElement element) {
        return fileChooser.addItemToList(element);
    }

    public boolean addElements(List<MultiFileChooserElement> element) {
        return fileChooser.addItemsToList(element);
    }

    public List<MultiFileChooserElement> getElementsFromList() {
        return fileChooser.getListItems(false);
    }

    public MultiFileChooserElement getElementFromInput() {
        return fileChooser.getElementFromInput();
    }

    public void removeElement(MultiFileChooserElement elements) {
        fileChooser.removeItem(elements);
    }

    public void removeElements(List<MultiFileChooserElement> elements) {
        fileChooser.removeItems(elements);
    }

    public void removeElementAt(int index) {
        fileChooser.removeItem(index);
    }

    public MultiFileChooser getMultiFileChooser() {
        return fileChooser;
    }

    public void setFileFilter(FileNameExtensionFilter fileFilter) {
        fileChooser.setFileFilter(fileFilter);
    }

    public void setMaxFileSize(long maxFileSize) {
        fileChooser.setMaxFileSize(maxFileSize);
    }
}
