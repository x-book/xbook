package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

/**
 * In input field that lists a multiple number of Strings as a "check box list", where the user can select any number of
 * strings.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class MultiSelection extends JPanel {

    /**
     * Addtionally added action listeners that can be dynamically added by "addActionListener" and are added to each
     * single CheckBox.
     */
    private ArrayList<ActionListener> additionalActionListeners = new ArrayList<>();
    /**
     * Holds all displayed boxes.
     */
    private ArrayList<Checkie> allBoxes = new ArrayList<>();
    /**
     * A wrapper that holds all single CheckBoxes.
     */
    private NotColorableJPanel checkWrapper;
    /**
     * Holds all available data that are selectable from the list.
     */
    private ArrayList<String> availableData;
    /**
     * Determines whether the elements are always disabled (and therefore are only selectable by extern elements).
     */
    private boolean areElementsAlwaysDisabled = false;

    /**
     * Constructor.
     *
     * @param data The data to be displayed for selection.
     */
    public MultiSelection(ArrayList<String> data) {
        super(new BorderLayout());

        // init the field
        JScrollPane scroll = new JScrollPane();
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroll.getVerticalScrollBar().setUnitIncrement(16);
        checkWrapper = new NotColorableJPanel(new StackLayout());
        checkWrapper.setBackground(Color.WHITE);
        scroll.setViewportView(checkWrapper);
        add(BorderLayout.CENTER, scroll);

        // set the data
        setAvailableData(data);
    }

    /**
     * Constructor. Initialises the field without any data.
     */
    public MultiSelection() {
        this(new ArrayList<String>());
    }

    /**
     * Set new selectable data to the input field. Replaced all existing selections.
     *
     * @param data The data to be selectable.
     */
    public void setAvailableData(ArrayList<String> data) {
        this.availableData = data;
        allBoxes.clear();
        for (String s : data) {
            Checkie checkie = new Checkie(s);
            for (ActionListener al : additionalActionListeners) {
                checkie.addActionListener(al);
            }
            allBoxes.add(checkie);
        }
        updateBoxes();
    }

    /**
     * Returns the available data that is selectable.
     *
     * @return The available data that is selectable.
     */
    public ArrayList<String> getAvailableData() {
        return availableData;
    }

    /**
     * Repaint all boxes, that means all boxes are removed and the current data is added again.
     */
    public void updateBoxes() {
        checkWrapper.removeAll();
        for (Checkie checkbox : allBoxes) {
            checkWrapper.add(checkbox);
            if (areElementsAlwaysDisabled) {
                checkbox.setEnabled(false);
            }
        }
    }

    /**
     * Returns a list of the selected values.
     *
     * @return A list of the selected values.
     */
    public ArrayList<String> getSelectedData() {
        ArrayList<String> data = new ArrayList<>();
        for (Checkie c : allBoxes) {
            if (c.isSelected()) {
                data.add(c.getValue());
            }
        }
        return data;
    }

    /**
     * Selects a set of passed values.
     *
     * @param selected The values to select.
     */
    public void setSelectedData(ArrayList<String> selected) {
        for (Checkie c : allBoxes) {
            if (selected.contains(c.getValue())) {
                c.setSelected(true);
            }
        }
    }

    /**
     * Deselect all values.
     */
    public void clearSelection() {
        for (Checkie c : allBoxes) {
            c.setSelected(false);
        }
    }

    /**
     * Select a row with a specific value selected or deselected.
     *
     * @param value    The value of the row that should be selected or deselected.
     * @param selected The selected status that the row should be set to.
     */
    public void setSelected(String value, boolean selected) {
        for (Checkie c : allBoxes) {
            if (c.getValue().equals(value)) {
                c.setSelected(selected);
            }
        }
    }

    /**
     * Select a row with a specific value enabled or disabled.
     *
     * @param value   The value of the row that should be selected or deselected.
     * @param enabled The enabled status that the row should be set to.
     */
    public void setEnabled(String value, boolean enabled) {
        for (Checkie c : allBoxes) {
            if (areElementsAlwaysDisabled) {
                c.setEnabled(false);
            } else if (c.getValue().equals(value)) {
                c.setEnabled(enabled);
            }
        }
    }

    /**
     * Select all rows enabled or disabled.
     *
     * @param enabled The enabled status that the rows should be set to.
     */
    public void setAllEnabled(boolean enabled) {
        for (Checkie c : allBoxes) {
            c.setEnabled(areElementsAlwaysDisabled && enabled);
        }
    }

    /**
     * Adds additionall action listeres which will be added to each single CheckBox.
     *
     * @param al The action listener to add.
     */
    public void addActionListener(ActionListener al) {
        additionalActionListeners.add(al);
        for (Checkie c : allBoxes) {
            c.addActionListener(al);
        }
    }

    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> list = new ArrayList<>();
        list.addAll(allBoxes);
        return list;
    }

    /**
     * Set the state if the elements are always disabled (and therefore are only selectable by extern elements).
     *
     * @param areElementsAlwaysDisabled The state if the elements are always disabled, or not.
     */
    public void setAreElementsAlwaysDisabled(boolean areElementsAlwaysDisabled) {
        this.areElementsAlwaysDisabled = areElementsAlwaysDisabled;
        updateBoxes();
    }

    protected void actionOnUpdateAnyCheckbox() {
    }

    /**
     * Returns the number of selected items.
     *
     * @return The number of selected items.
     */
    public int getCountSelected() {
        return getSelectedData().size();
    }

    /**
     * Returns if there is selected any value, or not.
     *
     * @return True if there is at least one selected value, false else.
     */
    public boolean hasSelectedItems() {
        return !getSelectedData().isEmpty();
    }

    /**
     * A helper class that extends the standard JCheckBox with functionality to handle the data.
     */
    private class Checkie extends JCheckBox {

        /**
         * The corresponding value.
         */
        private String value;

        /**
         * Constructor.
         *
         * @param title The value that is also displayed as the title.
         */
        public Checkie(String title) {
            super(title);
            this.value = title;
            setBackground(Color.WHITE);
            addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    update((JCheckBox) e.getSource());
                }
            });
        }

        /**
         * Returns the string value of the checkbox.
         *
         * @return The string value of the checkbox.
         */
        public String getValue() {
            return value;
        }

        /**
         * Updates the checkbox.
         *
         * @param cb The checkbox to update.
         */
        private void update(JCheckBox cb) {
            if (cb.isSelected()) {
                cb.setText("<html><b>" + value + "</b></html>");
            } else {
                cb.setText(value);
            }
            actionOnUpdateAnyCheckbox();
        }
    }

}