package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyListener;

/**
 * A stack element that holds a password field.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackPasswordField extends AbstractStackElement {

    /**
     * The password field object.
     */
    private JPasswordField passwordField;
    /**
     * The default border design.
     */
    private static Border defaultBorder = (new JTextField()).getBorder();

    /**
     * Constructor.
     *
     * @param columnType The correspondenting columnType.
     * @param label The label of the stack element.
     * @param labelWidth The width of the label.
     */
    public StackPasswordField(ColumnType columnType, String label, Integer labelWidth) {
        super(columnType, label, labelWidth, null);

        passwordField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                setDefaultStyle();
            }
        });

        addCustomSidebarFunctionality(passwordField);
    }

    /**
     * Constructor.
     *
     * @param label The label of the stack element.
     * @param labelWidth The width of the label.
     */
    public StackPasswordField(String label, Integer labelWidth) {
        this(null, label, labelWidth);
    }

    /**
     * Constructor.
     *
     * @param label The label of the stack element.
     */
    public StackPasswordField(String label) {
        this(label, null);
    }

    @Override
    public JComponent getElement() {
        passwordField = new JPasswordField();
        passwordField.setEchoChar('▪');
        passwordField.setFont(Fonts.FONT_STANDARD_PLAIN);
        return passwordField;
    }

    @Override
    public String getValue() {
        return StringHelper.charArrayToString(passwordField.getPassword());
    }


    /**
     * Set a text to the password field.
     *
     * @param text The text that will be displayed to the password field.
     */
    public void setText(String text) {
        passwordField.setText(text);
    }

    @Override
    public void clearInput() {
        passwordField.setText("");
    }

    @Override
    public void setMandatoryStyle() {
        if (passwordField != null) {
            passwordField.setBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
            passwordField.setFont(Fonts.FONT_STANDARD_BOLD);
        }
    }

    @Override
    public void setErrorStyle() {
        if (passwordField != null) {
            passwordField.setBorder(BorderFactory.createLineBorder(Color.red, 3));
        }
    }

    @Override
    public void setDefaultStyle() {
        if (passwordField != null) {
            passwordField.setBorder(defaultBorder);
        }
    }

    @Override
    public void setEnabled(boolean b) {
        if (passwordField != null) {
            passwordField.setEnabled(b);
        }
    }

    @Override
    public void addKeyListener(KeyListener l) {
        passwordField.addKeyListener(l);
    }

    @Override
    public void setValue(String value) {
        passwordField.setText(value);
    }
}
