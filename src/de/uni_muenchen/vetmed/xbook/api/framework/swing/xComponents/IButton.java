package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

/**
 * An interface to define several differenct Button types as a button.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface IButton {
    
    public void setEnabled(boolean state);

}
