package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.table.CustomTableModel;
import javax.swing.RowFilter;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class FilterManager {

    public abstract boolean include(RowFilter.Entry<? extends CustomTableModel, ? extends Integer> entry) ;

}
