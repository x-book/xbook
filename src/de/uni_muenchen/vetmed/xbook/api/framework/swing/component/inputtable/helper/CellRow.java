package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.helper;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.HashListMap;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.ITCell;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.ITConstants;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.*;
import javax.swing.border.Border;

/**
 * An CellRow is a JPanel that is used to assemble a line with input fields within one cell.
 * <p>
 * Example: If a cell has entries of several input fields, the input fields are all added to the CellRow. If the cell
 * enables multi-entries, each entry set is represented by one CellRow each consisting of the input fields.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class CellRow extends JPanel implements LayoutManager {

    /**
     * Holds the mapping of all Raw Input Fields and the corresponding ColumnType, which is saved in the
     * ITCellRowHelper.
     */
    private final ArrayList<ITCellRowHelper> rowInfos = new ArrayList<>();

    /**
     * The list of all elements in the row
     */
    private final ArrayList<AbstractITRaw> elements = new ArrayList<>();


    private final HashMap<ColumnType, AbstractITRaw> map = new HashMap<>();
    /**
     * The current index of the CellRow.
     */
    private int index;
    private int displayGridY;
    private ITCell itCell;

    /**
     * Constructor.
     * <p>
     * Initializes the ITRow with a specific index and the specified LayoutManager.
     *
     * @param itCell
     * @param index  The current index of the row.
     */
    public CellRow(ITCell itCell, int index) {
        this.itCell = itCell;
        setLayout(this);
        this.index = index;
    }

    /**
     * Add a new row information object to the CellRow data.
     *
     * @param columnType The corresponding ColumnType of the input field.
     * @param raw        The raw input field object.
     */
    public void addRowInfo(ColumnType columnType, AbstractITRaw raw) {
        rowInfos.add(new ITCellRowHelper(columnType, raw));
        map.put(columnType, raw);
    }

    /**
     * Returns all available row infos.
     *
     * @return The available row infos.
     */
    public ArrayList<ITCellRowHelper> getRowInfos() {
        return rowInfos;
    }

    public AbstractITRaw getElement(ColumnType columnType) {
        return map.get(columnType);
    }

    /**
     * Decreased the index by 1.
     */
    public void decreaseIndex() {
        index--;
    }

    /**
     * Returns the current index.
     *
     * @return The current index.
     */
    public int getIndex() {
        return index;
    }

    public int getGridX() {
        int width = 0;
        for (ITCellRowHelper rowInfo : rowInfos) {
            width += rowInfo.getRaw().getConstraints().getWidth();
        }
        return width;
    }

    public int getGridY() {
        int heigth = 0;
        for (ITCellRowHelper rowInfo : rowInfos) {
            heigth = Math.max(rowInfo.getRaw().getConstraints().getHeight(), heigth);
        }
        return heigth;
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {

    }

    @Override
    public void removeLayoutComponent(Component comp) {

    }

    @Override
    public Dimension preferredLayoutSize(Container target) {

        synchronized (target.getTreeLock()) {
            Dimension dim = new Dimension(0, 0);
            int numComponents = target.getComponentCount();
            for (int i = 0; i < numComponents; i++) {
                Component c = target.getComponent(i);
                if (c.isVisible()) {
                    Dimension cDim = c.getPreferredSize();
                    dim.width += target.getParent().getWidth();
//                    dim.width += hgap;
                    dim.height = Math.max(cDim.height, dim.height);
                    dim.height = Math.max(ITConstants.CELL_HEIGHT, dim.height);

//                    dim.height += vgap;
                }
            }
//            dim.height += vgap;
            return dim;
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container target) {
        synchronized (target.getTreeLock()) {
            Dimension dim = new Dimension(0, 0);
            int numComponents = target.getComponentCount();
            for (int i = 0; i < numComponents; i++) {
                Component c = target.getComponent(i);
                if (c.isVisible()) {
                    Dimension cDim = c.getMinimumSize();
                    dim.width = Math.max(cDim.width, dim.width);
//                    dim.width += hgap;
                    dim.height += cDim.height;
//                    dim.height += vgap;
                }
            }
//            dim.height += vgap;
            return dim;
        }
    }

    @Override
    public void layoutContainer(Container target) {
        synchronized (target.getTreeLock()) {
            Insets insets = target.getInsets();
            int top = insets.top;
//            int bottom = target.getHeight() - insets.bottom;
            int left = insets.left;
            int right = target.getWidth() - insets.right;

            int width = right - left;
            int gridX = 0;

            for (ITCellRowHelper cell : rowInfos) {
                AbstractITRaw raw = cell.getRaw();
                if (raw.isVisible()) {
                    gridX += raw.getConstraints().getWidth();
                }
            }
            Component[] components = target.getComponents();
            int extraComponents = components.length - rowInfos.size();

            if (gridX == 0 && extraComponents == 0) {
                return;
            }
            double x = 0;

            if (gridX != 0) {
                double gridWith = (width - (50.0 * extraComponents)) / gridX;
                int lastLeft = (int) (x + left + 0.5);

                for (ITCellRowHelper cell : rowInfos) {

                    AbstractITRaw raw = cell.getRaw();
                    if (!raw.isVisible()) {
                        continue;
                    }
                    int cellWidth = (int) (raw.getConstraints().getWidth() * gridWith);
                    int newLeft = (int) ((left + x) + cellWidth + 0.5);
                    raw.setBounds(lastLeft, top, newLeft - lastLeft,
                            Math.max(raw.getConstraints().getHeight(), displayGridY) * ITConstants.CELL_HEIGHT);
                    x += cellWidth;
                    lastLeft = newLeft;

                }
            }
            for (Component component : components) {
                if (component instanceof AbstractITRaw) {
                    continue;
                }
                component.setBounds((int) Math.round(left + x), top, 50, ITConstants.CELL_HEIGHT);
                x += 50;

            }
        }
    }

    /**
     * Add a raw element to the row
     *
     * @param raw
     */
    public void add(AbstractITRaw raw) {
        super.add(raw);
        raw.setRow(this);
        elements.add(raw);
    }

    /**
     * Should be called by a element to indicate it was changed. Calculates if the row is empty or not and then calles
     * the underlying cell to decide if a new row shall be added
     *
     * @param e
     */
    public void elementUpdated(FocusEvent e) {
        List<Component> allComponents = ComponentHelper.getAllComponents(this);
        boolean hasFocusOwnerAsChild = allComponents.contains(e.getOppositeComponent());
        //only continue if focus is now inside other element
        boolean empty = isEmpty();

        if (empty && hasFocusOwnerAsChild) {
            return;
        }

        itCell.rowUpdated(this, empty);
    }

    public boolean isEmpty() {
        boolean empty = true;
        for (AbstractITRaw element : elements) {
            empty &= element.isEmpty();
        }
        return empty;
    }

    public void doActionOnRemovingARow() {
        // do action on constraints when removing a row.
        for (AbstractITRaw raw : elements) {
            raw.actionOnRemoveRow();
        }
    }

    public void setDisplayGridY(int displayGridY) {
        this.displayGridY = displayGridY;
    }

    @Override
    public void setBorder(Border border) {
        super.setBorder(border);
    }
}
