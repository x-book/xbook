package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.modernCheckbox;

import de.uni_muenchen.vetmed.xbook.api.exception.InvalidSelectionException;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ModernCheckbox extends JLabel {

    /**
     * The available modes.
     */
    public static enum Mode {
        YES_NO, YES_NO_NONE
    }

    /**
     * An enumeration that defines the available selection states.
     */
    public static enum Selection {
        NONE, SELECTED, DESELECTED
    }

    /**
     * Saves the status if the button is hovered or not.
     */
    private boolean isHovered = false;
    private Selection currentSelection;
    private Mode currentMode;

    /**
     * Constructor. Creates the button without a label.
     *
     * @param mode The selected mode.
     */
    public ModernCheckbox(Mode mode) {
        this(mode, "");
    }

    /**
     * Constructor. Creates the button.
     *
     * @param mode The selected mode.
     * @param text The label of the button.
     */
    public ModernCheckbox(Mode mode, String text) {
        super(text);
        this.currentMode = mode;

        // set the default selection
        currentSelection = getDefaultSelection();

        setOpaque(true);
        updateColor();

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                isHovered = true;
                updateColor();
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);
                isHovered = false;
                updateColor();
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                actionOnChanged();
            }
        });
    }

    protected void actionOnChanged() {
        if (currentSelection == Selection.DESELECTED) {
            currentSelection = Selection.SELECTED;
        } else if (currentSelection == Selection.SELECTED) {
            if (currentMode == Mode.YES_NO) {
                currentSelection = Selection.DESELECTED;
            } else if (currentMode == Mode.YES_NO_NONE) {
                currentSelection = Selection.NONE;
            }
        } else if (currentSelection == Selection.NONE) {
            currentSelection = Selection.DESELECTED;
        }

        updateColor();
    }

    /**
     * Returns the default selection.
     *
     * @return The default selection.
     */
    protected Selection getDefaultSelection() {
        if (currentMode == Mode.YES_NO) {
            return Selection.DESELECTED;
        } else {
            return Selection.NONE;
        }
    }

    /**
     * Returns an ImageIcon, or null if the path was invalid.
     *
     * @param fileName The file name.
     * @return The ImageIcon object with the correspondenting language flag.
     */
    protected static ImageIcon createIcon(String fileName) {
        String path = "/images/" + fileName;
        URL imgURL = ModernCheckbox.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    /**
     * Updates the background color and the icon of the checkbox to the current state.
     */
    public void updateColor() {
        if (!isEnabled()) {
            setBackground(Colors.HIGHLIGHT_DARK_GRAY_BACKGROUND);
            if (currentSelection == Selection.SELECTED) {
                setIcon(createIcon("selected_disabled.png"));
            } else if (currentSelection == Selection.DESELECTED) {
                setIcon(createIcon("deselected_disabled.png"));
            } else if (currentSelection == Selection.NONE) {
                setIcon(createIcon("none_disabled.png"));
            }
        } else if (currentSelection == Selection.DESELECTED) {
            if (isHovered) {
                setBackground(Colors.HIGHLIGHT_RED_BACKGROUND_HOVERED);
                setIcon(createIcon("deselected_hovered.png"));
            } else {
                setBackground(Colors.HIGHLIGHT_RED_BACKGROUND);
                setIcon(createIcon("deselected.png"));
            }
        } else if (currentSelection == Selection.SELECTED) {
            if (isHovered) {
                setBackground(Colors.HIGHLIGHT_GREEN_BACKGROUND_HOVERED);
                setIcon(createIcon("selected_hovered.png"));
            } else {
                setBackground(Colors.HIGHLIGHT_GREEN_BACKGROUND);
                setIcon(createIcon("selected.png"));
            }
        } else if (currentSelection == Selection.NONE) {
            if (isHovered) {
                setBackground(Colors.HIGHLIGHT_GRAY_BACKGROUND_HOVERED);
                setIcon(createIcon("none_hovered.png"));
            } else {
                setBackground(Colors.HIGHLIGHT_GRAY_BACKGROUND);
                setIcon(createIcon("none.png"));
            }
        }
    }

    /**
     * Returns the current selection.
     *
     * @return The current selection.
     */
    public Selection getCurrentSelection() {
        return currentSelection;
    }

    /**
     * Sets the selection to a new value.
     *
     * @param selection
     */
    public void setSelection(Selection selection) throws InvalidSelectionException {
        if (currentMode == Mode.YES_NO && selection == Selection.NONE) {
            throw new InvalidSelectionException();
        }
        currentSelection = selection;
        updateColor();
    }
}
