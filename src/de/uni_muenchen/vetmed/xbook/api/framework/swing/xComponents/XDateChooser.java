package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;
import de.uni_muenchen.vetmed.xbook.api.Loc;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Locale;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class XDateChooser extends JDateChooser {

    public XDateChooser() {
        super();
        // getCalendar().setLenient(false); // throws Nullpointer
        Locale l = Locale.getDefault();

        if (l.getVariant().isEmpty()) {
            if (l.getLanguage().equals("en")) {
                l = new Locale("en", "GB");
            } else {
                l = new Locale(l.getLanguage(), l.getLanguage().toUpperCase());
            }
        }
        setLocale(l);
        getJCalendar().setNullDateButtonVisible(true);
        getJCalendar().setNullDateButtonText(Loc.get("CLEAR"));
        getJCalendar().setTodayButtonVisible(true);
        getJCalendar().setTodayButtonText(Loc.get("TODAY"));
    }

    public ArrayList<Component> getFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(getDateEditor().getUiComponent());
        comp.add(getCalendarButton());
        comp.add((JTextFieldDateEditor) getDateEditor());
        comp.add(getJCalendar());
        comp.add(getJCalendar().getDayChooser());
        comp.add(getJCalendar().getMonthChooser());
        comp.add(getJCalendar().getYearChooser());
        comp.add(this);
        if (getRootPane() != null) {
            comp.add(getRootPane());
        }
        return comp;
    }
}
