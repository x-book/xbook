package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class RawTextArea extends AbstractInputElementFunctions {

    /**
     * The text field object.
     */
    protected JTextArea textArea;

    public RawTextArea(ColumnType columnType) {
        super(columnType);
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        final String s = list.get(columnType);
        textArea.setText(s);
    }

    @Override
    public void clear() {
        textArea.setText("");
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        list.add(new DataColumn(getText() + "", columnType.getColumnName()));
    }

    @Override
    public String getStringRepresentation() {
        return getText();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textArea);
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !textArea.getText().isEmpty();
    }

    public JTextArea getTextField() {
        return textArea;
    }




    public String getText() {
        return textArea.getText();
    }

    public void setText(String text) {
        textArea.setText(text);
    }

    @Override
    public void createField() {
        setLayout(new BorderLayout());
        textArea = new JTextArea();
        textArea.setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
        textArea.setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        textArea.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (textArea.getText().isEmpty()) {
                    textArea.setToolTipText(null);
                } else {
                    textArea.setToolTipText(textArea.getText());
                }
            }
        });

//        ComponentHelper.setMaximumInputLength(textArea, columnType);
//        add(BorderLayout.CENTER, ComponentHelper.wrapComponent(textArea, 10, 0, 10, 0));
        add(BorderLayout.CENTER, textArea);
    }

    @Override
    public void setFocus() {
        textArea.requestFocusInWindow();
    }

}
