package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.ITRawLabel;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntryRoot;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.dependable.AbstractBaseDependableEntry;

import javax.swing.SwingUtilities;

/**
 * @author Daniel Kaltenthaler
 */
public class LabelConstraints extends AbstractInputConstraints {

	/**
	 * The displayed label.
	 */
	private String label = "";
	private int horizontalAlignment = SwingUtilities.LEFT;

	/**
	 * Constructor.
	 *
	 * Initialize the constraints.
	 *
	 * @param entry      The corresponding Entry object of the input field.
	 * @param width The width of the input fields in pixels.
	 * @param height The height of the input fields in pixels.
	 * @param borderEast The size of the border in the east.
	 * @param label The displayed label.
	 * @param horizontalAlignment The horizontal alignment of the text label. In SwingConstants: LEFT, CENTER, RIGHT,
	 * LEADING or TRAILING.
	 */
	public LabelConstraints(AbstractEntryRoot entry, int width, int height, int borderEast, String label, Integer horizontalAlignment) {
		super(entry, null, width, height, borderEast);
		this.label = label;
		if (horizontalAlignment != null) {
			this.horizontalAlignment = horizontalAlignment;
		}
	}

	/**
	 * Constructor.
	 *
	 * Initialize the constraints.
	 *
	 * @param entry      The corresponding Entry object of the input field.
	 * @param width The width of the input fields in pixels.
	 * @param height The height of the input fields in pixels.
	 * @param borderEast The size of the border in the east.
	 * @param label The displayed label.
	 */
	public LabelConstraints(AbstractEntryRoot entry, int width, int height, int borderEast, String label) {
		this(entry, width, height, borderEast, label, null);
	}

	/**
	 * Constructor.
	 *
	 * Initialize the constraints. Use this constructor to display no border in the east.
	 *
	 * @param entry      The corresponding Entry object of the input field.
	 * @param width The width of the input fields in pixels.
	 * @param height The height of the input fields in pixels.
	 * @param label The displayed label.
	 * @param horizontalAlignment The horizontal alignment of the text label. In SwingConstants: LEFT, CENTER, RIGHT,
	 * LEADING or TRAILING.
	 */
	public LabelConstraints(AbstractEntryRoot entry, int width, int height, String label, Integer horizontalAlignment) {
		this(entry, width, height, 0, label, horizontalAlignment);
	}

	/**
	 * Constructor.
	 *
	 * Initialize the constraints. Use this constructor to display no border in the east.
	 *
	 * @param entry      The corresponding Entry object of the input field.
	 * @param width The width of the input fields in pixels.
	 * @param height The height of the input fields in pixels.
	 * @param label The displayed label.
	 */
	public LabelConstraints(AbstractEntryRoot entry, int width, int height, String label) {
		this(entry, width, height, 0, label, null);
	}

	/**
	 * Constructor.
	 *
	 * Initialize the constraints. Use this constructor to display no border in the east and default cell height.
	 *
	 * @param entry      The corresponding Entry object of the input field.
	 * @param width The width of the input fields in pixels.
	 * @param label The displayed label.
	 * @param horizontalAlignment The horizontal alignment of the text label. In SwingConstants: LEFT, CENTER, RIGHT,
	 * LEADING or TRAILING.
	 */
	public LabelConstraints(AbstractEntryRoot entry, int width, String label, Integer horizontalAlignment) {
		this(entry, width, 1, 0, label, horizontalAlignment);
	}

	/**
	 * Constructor.
	 *
	 * Initialize the constraints. Use this constructor to display no border in the east and default cell height.
	 *
	 * @param entry      The corresponding Entry object of the input field.
	 * @param width The width of the input fields in pixels.
	 * @param label The displayed label.
	 */
	public LabelConstraints(AbstractEntryRoot entry, int width, String label) {
		this(entry, width, 1, 0, label, null);
	}

	@Override
	protected AbstractITRaw createInputCustom() {
		return new ITRawLabel(this);
	}

	/**
	 * Sets the displayed label.
	 *
	 * @param label The displayed label.
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Returns the displayed label.
	 *
	 * @return The displayed label.
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Returns the horizontal alignment of the text label.
	 *
	 * @return The horizontal alignment of the text label. The horizontal alignment of the text label. In
	 * SwingConstants: LEFT, CENTER, RIGHT, LEADING or TRAILING.
	 */
	public int getHorizontalAlignment() {
		return horizontalAlignment;
	}

	/**
	 * Sets the horizontal alignment of the text label.
	 *
	 * @param horizontalAlignment The horizontal alignment of the text label. The horizontal alignment of the text
	 * label. In SwingConstants: LEFT, CENTER, RIGHT, LEADING or TRAILING.
	 */
	public void setHorizontalAlignment(int horizontalAlignment) {
		this.horizontalAlignment = horizontalAlignment;
	}

}
