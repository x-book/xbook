package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class XCollapsibleTitle extends XTitle {

    /**
     * Status if the content ist collapsed or not.
     */
    protected boolean isCollapsed = false;
    /**
     * Defines if the collapsibility function is active or not.
     */
    protected boolean isCollapsibe = true;
    /**
     * The icon that displayed if the content is collapsed or not.
     */
    private JLabel icon;
    /**
     * The element that should be hided. In general an JPanel object.
     */
    private Component elementToHide;
    private ArrayList<XCollapsibleTitle> subCollapsibleTitles;
    private boolean isSub = false;

    /**
     * Constructor.
     *
     * @param title The title that is displayed in the bar.
     * @param fatherCollapse
     */
    public XCollapsibleTitle(String title) {
        super(title);
        subCollapsibleTitles = new ArrayList<>();

        // set the borders of the text label
        label.setBorder(new EmptyBorder(0, 5, 0, 100));

        // set the icon
        icon = new JLabel();
        icon.setOpaque(true);
        icon.setBackground(BG_COLOR);
        icon.setBorder(new EmptyBorder(0, 7, 0, 0));
        reset();
        labelWrapper.add(BorderLayout.WEST, icon);

        // set the mouse listeners
        icon.addMouseListener(getMouseAdapter());
        label.addMouseListener(getMouseAdapter());
    }

    /**
     * Get the mouse adapter that is used for setting the elements collapsed or
     * not.
     *
     * @return The mouse adapter.
     */
    private MouseAdapter getMouseAdapter() {
        return (new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (isCollapsibe) {
                    isCollapsed = !isCollapsed;
                    reset();
                }
            }
        });
    }

    public void addSubCollapsibleTitle(XCollapsibleTitle sub) {
        subCollapsibleTitles.add(sub);
        Color c = Colors.CONTENT_BACKGROUND;
        Color newColor = new Color(c.getRed() - 15, c.getGreen() - 15, c.getBlue() - 15);
        sub.icon.setBackground(newColor);
        sub.label.setBackground(newColor);
        sub.label.setForeground(Color.BLACK);
        sub.setBorderColor(Color.GRAY);
        sub.setSub(true);
        sub.reset();
    }

    /**
     * Dependent on the status set the elements visible or invisible and edit
     * the graphics.
     */
    protected void reset() {
        if (isCollapsed) {
            if (isSub) {
                icon.setIcon(Images.COLLAPSABLE_ICON_RIGHT_BLACK);
            } else {
                icon.setIcon(Images.COLLAPSABLE_ICON_RIGHT_WHITE);
            }
            if (elementToHide != null) {
                elementToHide.setVisible(false);
                for (XCollapsibleTitle c : subCollapsibleTitles) {
                    c.setVisible(false);
                    c.elementToHide.setVisible(false);
                }
            }
        } else {
            if (isSub) {
                icon.setIcon(Images.COLLAPSABLE_ICON_DOWN_BLACK);
            } else {
                icon.setIcon(Images.COLLAPSABLE_ICON_DOWN_WHITE);
            }
            if (elementToHide != null) {
                elementToHide.setVisible(true);
                for (XCollapsibleTitle c : subCollapsibleTitles) {
                    c.setVisible(true);
                    c.elementToHide.setVisible(true);
                }
            }
        }
    }

    /**
     * Get the status whether the elements are collapsed or not.
     *
     * @return The status whether the elements are collapsed or not.
     */
    public boolean isCollapsed() {
        return isCollapsed;
    }

    /**
     * Set the elements collapsed or not.
     *
     * @param isCollapsed The status if the elements should be displayed or not.
     */
    public void setCollapsed(boolean isCollapsed) {
        this.isCollapsed = isCollapsed;
        reset();
    }

    /**
     * Set the elements that should be set invisible when setting the element
     * collapsed.
     *
     * @param elementToHide The objects.
     */
    public void setElementToHide(Component elementToHide) {
        this.elementToHide = elementToHide;
    }

    public void setSub(boolean b) {
        this.isSub = b;
    }

    /**
     * Set all collapsable elements invisible (to deactivate the function)
     *
     * @param b <code>true</code> to set the elements visible,
     * <code>false</code> else.
     */
    public void enableCollapsibility(boolean b) {
        this.isCollapsibe = b;
        icon.setVisible(b);
        if (b) {
            labelWrapper.add(BorderLayout.WEST, icon);
        } else {
            JLabel spacer = new JLabel(" ");
            spacer.setBackground(Color.BLACK);
            spacer.setOpaque(true);
            labelWrapper.add(BorderLayout.WEST, spacer);
        }
    }

}
