package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.gui.AbstractSplashScreen;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 * A stack element that displays a text without any label.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class PleaseQuoteBlock extends JPanel {

    private final MultiLineTextLabel firstLine;
    private final MultiLineTextLabel quoteLine;
    private final XButton copyToClipboard;
    private final JPanel wrapper;
    private final JPanel wrapperBorder;

    /**
     * Constructor.
     *
     * @param apiCtrl
     */
    public PleaseQuoteBlock(final ApiControllerAccess apiCtrl) {
        setLayout(new BorderLayout());

        wrapper = new JPanel(new BorderLayout());

        firstLine = new MultiLineTextLabel(Loc.get("PLEASE_QUOTE_BOOK_WHEN_USING_FOR_SCIENTIFIC_WORK", apiCtrl.getBookName()) + "\n", true);
        firstLine.setBackground(Colors.CONTENT_BACKGROUND);
        wrapper.add(BorderLayout.NORTH, firstLine);

        final String quoteText = AbstractSplashScreen.getSplashScreen().getQuoteText();
        quoteLine = new MultiLineTextLabel(quoteText, true);
        quoteLine.setBackground(Colors.CONTENT_BACKGROUND);
        wrapper.add(BorderLayout.CENTER, quoteLine);

        copyToClipboard = new XButton(Loc.get("COPY"));
        copyToClipboard.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StringSelection selection = new StringSelection(quoteText);
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection, selection);
                JOptionPane.showMessageDialog(PleaseQuoteBlock.this, Loc.get("TEXT_COPIED_TO_CLIPBOARD"), "", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        copyToClipboard.setStyle(XButton.Style.DARKER);
        copyToClipboard.setBorderAround(0, 0, 0, 6);

        wrapper.add(BorderLayout.EAST, copyToClipboard);

        setBackground(Colors.CONTENT_BACKGROUND);

        add(BorderLayout.CENTER, wrapperBorder = ComponentHelper.wrapComponent(wrapper, Colors.CONTENT_BACKGROUND, 20));
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (firstLine != null) {
            firstLine.setBackground(bg);
        }
        if (quoteLine != null) {
            quoteLine.setBackground(bg);
        }
        if (copyToClipboard != null) {
            copyToClipboard.setBackground(bg);
        }
        if (wrapper != null) {
            wrapper.setBackground(bg);
        }
        if (wrapperBorder != null) {
            wrapperBorder.setBackground(bg);
        }
    }
}
