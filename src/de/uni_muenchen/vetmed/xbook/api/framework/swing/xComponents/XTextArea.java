package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JTextArea;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class XTextArea extends JTextArea {

    public XTextArea() {
        this("");
    }

    public XTextArea(String text) {
        super(text);

        setBackground(Colors.INPUT_FIELD_INPUT_BACKGROUND);
        setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        setLineWrap(true);
        setWrapStyleWord(true);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                String text = getText();
                if (text.isEmpty()) {
                    setToolTipText(null);
                }
                setToolTipText(MultiLineTooltips.splitToolTip(text));

            }
        });
    }

    public static class MultiLineTooltips {

        private static int DIALOG_TOOLTIP_MAX_SIZE = 75;
        private static final int SPACE_BUFFER = 10;

        public static String splitToolTip(String tip) {
            return splitToolTip(tip, DIALOG_TOOLTIP_MAX_SIZE);
        }

        public static String splitToolTip(String tip, int length) {
            if (tip.length() <= length + SPACE_BUFFER) {
                return tip;
            }
            tip = tip.replaceAll("\n", "<br>");
            List<String> parts = new ArrayList<>();

            int currentLenght = 0;

            while (currentLenght < tip.length()) {
                if (currentLenght + length < tip.length()) {
                    String overLong = tip.substring(currentLenght, currentLenght + length).trim();

                    int lastSpace = overLong.lastIndexOf(' ');
                    if (overLong.startsWith("<br>")) {
                        currentLenght += "<br>".length();
                        continue;
                    }
                    int indexBR = overLong.indexOf("<br>");
                    if (indexBR != -1) {

                        lastSpace = Math.min(lastSpace, indexBR + "<br>".length());
                    }

                    if (lastSpace <= length&&lastSpace != -1) {
                        parts.add(tip.substring(currentLenght, currentLenght + lastSpace));
                        currentLenght += lastSpace;
                    } else {
                        parts.add(tip.substring(currentLenght, currentLenght + length));
                        currentLenght += length;
                    }
//                    parts.add(tip.substring(maxLength, maxLength + length));
//                    maxLength += maxLength + length;
                } else {
                    parts.add(tip.substring(currentLenght));
                    break;
                }
            }
            boolean hasHTML = false;
            StringBuilder sb;
            if (tip.startsWith("<html>")) {
                hasHTML = true;
                sb = new StringBuilder();
            } else {
                sb = new StringBuilder("<html>");
            }

            for (int i = 0; i < parts.size() - 1; i++) {
                sb.append(parts.get(i) + "<br>");
            }
            sb.append(parts.get(parts.size() - 1));
            if (!hasHTML) {
                sb.append(("</html>"));
            }
            return sb.toString();
        }
    }
}
