package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Extends the DynamicComboBox with a list that allows adding several elements
 * out of the combo box.
 *
 * @author Daniel Kaltenthaler
 * @version 1.0.0
 * @param <T>
 */
public abstract class DynamicComboBoxListValue<T> extends AbstractDynamicComboBoxList<T, ArrayList<T>> {

    public DynamicComboBoxListValue(ArrayList<T> data, String name) {
        super(data, name);

    }

    public void setItems(ArrayList<T> items) {
        setItems(items, true);
    }

    public void setItems(ArrayList<T> items, boolean sort) {
        this.databaseData = items;

        ArrayList<T> list = new ArrayList<>(items);
        list.add(getEmptyItem());
        if (sort) {
            Collections.sort(list, getComparator());
        }
        setItemsToCombo(list);
    }

    @Override
    protected Object[] getValuesForCombo() {
        return databaseData.toArray();
    }

    @Override
    protected boolean containsValue(T value) {
        return databaseData.contains(value);
    }

    @Override
    public T getEmptyItem() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Comparator<T> getComparator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     */
    public abstract static class DynamicComboBoxListValueString extends DynamicComboBoxListValue<String> {

        public DynamicComboBoxListValueString(ArrayList<String> data, String name) {
            super(data, name);
        }

        @Override
        public String getEmptyItem() {
            return "";
        }

        @Override
        public Comparator<String> getComparator() {
            return new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.toUpperCase().compareTo(o2.toUpperCase());
                }
            };
        }
    }
}
