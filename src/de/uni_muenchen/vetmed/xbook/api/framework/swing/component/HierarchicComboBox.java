package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardInputUnitColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoadedException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.UpdateableEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ColumnHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.DataManagementHelper;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daniel Kaltenthaler
 */
public class HierarchicComboBox extends JPanel {

    private ApiControllerAccess apiCtrlAcess;
    private ArrayList<FocusListener> focusListener;
    private FocusListener focus;
    private int alwaysShow = 1;
    private boolean userRightCheckEnabled = true;
    private SidebarPanel sidebar = null;
    private UpdateableEntry entry;

    /**
     * The default value for the height of a combo box.
     */
    private int comboHeight = 32;
    /**
     * The default value for the empty border at the bottom of a combo box.
     */
    private int borderButtom = 8;
    /**
     * Holds all combo boxes that are currently displayed.
     */
    private ArrayList<Box> allBoxes;
    /**
     * An array of labels that are displayed above the combo boxes. If there are no labels the combo box will be
     * displayed with any label.
     */
    private String[] labels;
    private int[] comboWidths;
    private boolean isVerticalAlignment = true;
    private boolean orderByValue;
    private ColumnType columnType;
    private ItemListener itemListener;
    private ItemListener additionalItemListener;
    protected HashMap<String, ArrayList<DataColumn>> values;
    private ArrayList<JPanel> allComboPanelWrapper;
    private ArrayList<JPanel> allComboPanelWrapper2;
    private boolean isMandatory;
    /**
     * A label that should display nothing but is intend to hold the focus when no focusable element is abailable (e.g.
     * while dynamically generating the single boxes, which is necessary for a MultiHierarchicComboBox).
     */
    private JTextField focusHolder;
    /**
     * A wrapper for all boxes. Intended so all elements from the box Wrapper can be removed without removing all
     * elements from the input field (e.g. the focus holder object)
     */
    private JPanel boxWrapper;

    /**
     * When set to true, the field will automatical fill the combobox if there is only available one option.
     */
    private boolean fillSingleValuesAutomatical = false;

    public HierarchicComboBox(ApiControllerAccess apiCtrlAcess, ColumnType columnType) {
        this(apiCtrlAcess, columnType, false, null);
    }

    public HierarchicComboBox(ApiControllerAccess apiCtrlAcess, ColumnType columnType, boolean orderByValue,
                              String[] labels) {
        this(apiCtrlAcess, true, columnType, orderByValue, labels, null);
    }

    public HierarchicComboBox(ApiControllerAccess apiCtrlAcess, ColumnType columnType, boolean orderByValue,
                              String[] labels, boolean userRightCheckEnabled) {
        this(apiCtrlAcess, true, columnType, orderByValue, labels, null, userRightCheckEnabled);
    }

    public HierarchicComboBox(ApiControllerAccess apiCtrlAcess, boolean isVerticalAlignment, ColumnType columnType,
                              boolean orderByValue, String[] labels, int[] comboWidths) {
        this(apiCtrlAcess, true, columnType, orderByValue, labels, null, true);
    }

    public HierarchicComboBox(ApiControllerAccess apiCtrlAcess, boolean isVerticalAlignment, ColumnType columnType,
                              boolean orderByValue, String[] labels, int[] comboWidths, boolean userRightCheckEnabled) {
        super();
        this.apiCtrlAcess = apiCtrlAcess;
        this.columnType = columnType;
        this.labels = labels;
        this.comboWidths = comboWidths;
        this.focusListener = new ArrayList<>();
        this.isVerticalAlignment = isVerticalAlignment;
        this.orderByValue = orderByValue;
        this.allComboPanelWrapper = new ArrayList<>();
        this.allComboPanelWrapper2 = new ArrayList<>();
        this.userRightCheckEnabled = userRightCheckEnabled;

        this.values = new HashMap<>();

        if (isVerticalAlignment) {
            setLayout(new StackLayout());
        } else {
            setLayout(new FlowLayout(FlowLayout.LEFT));
        }

        // Inits the label that should display nothing but is intend to hold the focus when no focusable element is
        // abailable
        // e.g. while dynamically generating the single boxes, which is necessary for a MultiHierarchicComboBox).
        focusHolder = new JTextField("");
        focusHolder.setSize(new Dimension(1, 1));
        focusHolder.setPreferredSize(new Dimension(1, 1));
        focusHolder.setMaximumSize(new Dimension(1, 1));
        focusHolder.setVisible(false);
        add(focusHolder);

        // Inits the box wrapper.
        boxWrapper = new JPanel();
        if (isVerticalAlignment) {
            boxWrapper.setLayout(new StackLayout());
        } else {
            boxWrapper.setLayout(new FlowLayout(FlowLayout.LEFT));
        }
        add(boxWrapper);

        addInput(1, "0");
    }

    public void setNumberOfAlwaysDisplayedBoxes(int alwaysShow) {
        if (alwaysShow < 1) {
            this.alwaysShow = 1;
        } else {
            this.alwaysShow = alwaysShow;
        }
        reload();
    }

    private void reload() {
        if (getSelectedInput() == null) {
            addInput(1, "0");
        } else {
            HierarchicComboBox.this.setValue(getSelectedInput().getColumnName());
        }
    }

    /**
     * Add a new, specific combobox and removes combo boxes that are already visible but are not used anymore.
     *
     * @param level      The number of level of the item. First level is 1 (one).
     * @param selectedId The selected ID of the previous element. Root is 0.
     */
    public void addInput(final int level, final String selectedId) {
        ArrayList<DataColumn> fetchedData;
        // check if the data for the selected parent Id is already available, else setValue the data and add it to
        // the data
        if (!values.containsKey(selectedId)) {
            try {
                fetchedData = apiCtrlAcess.getMultiComboBoxData(columnType.getConnectedTableName(), selectedId,
                        columnType.getSortedBy(), columnType.isLanguage());
                values.put(selectedId, fetchedData);
            } catch (StatementNotExecutedException | NotLoggedInException ex) {
                fetchedData = new ArrayList<>();
                Logger.getLogger(HierarchicComboBox.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            fetchedData = values.get(selectedId);
        }

        final Box combo;

        if (!fetchedData.isEmpty()) {

            // setup the combo box
            combo = new Box(level, fetchedData);
            combo.setSize(new Dimension(1, comboHeight));
            combo.setPreferredSize(new Dimension(1, comboHeight));
            if (additionalItemListener != null) {
                combo.addItemListener(additionalItemListener);
            }
            combo.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        // first set the focus to the focusholder object, so the input field is able to keep the focus,
                        // even if all elements are removed.
                        focusHolder.setVisible(true);
                        focusHolder.requestFocusInWindow();

                        String selected = ((DataColumn) combo.getSelectedItem()).getColumnName();
                        if (selected != null) {
                            addInput(level + 1, selected);
                        } else {
                            addInput(level, selectedId);
                        }
                        // get the focus again, because the focus of the input field is lost after
                        // dynamically adding a new input field to the focused element.
                        ((Component) e.getSource()).requestFocusInWindow();
                        focusHolder.setVisible(false);
                    }
                }
            });
            if (getCustomComboItemListener(level) != null) {
                combo.addItemListener(getCustomComboItemListener(level));
            }
            for (FocusListener l : focusListener) {
                combo.addFocusListener(l);
            }
        } else {
            combo = null;
        }

        // create a new arraylist that does not hold the items that should be removed
        ArrayList<Box> temp = new ArrayList<>();
        for (int i = 1; i < level; i++) {
            for (Box b : allBoxes) {
                if (b.level == i) {
                    temp.add(b);
                    break;
                }
            }
        }
        allBoxes = temp;
        // add the current (new) combo box to the temporal list
        if (!fetchedData.isEmpty() && combo != null) {
            allBoxes.add(combo);
        }

        // remove all items from the panel and add them again
        boxWrapper.removeAll();
        allComboPanelWrapper = new ArrayList<>();
        allComboPanelWrapper2 = new ArrayList<>();
        for (int i = 0; i < allBoxes.size(); i++) {
            JPanel panel = new JPanel(new StackLayout());
            Box b = allBoxes.get(i);
            if (labels != null && i < labels.length) {
                panel.add(ComponentHelper.wrapComponent(new JLabel(labels[i]), 0, 0, 3, 0));
            }
            JPanel comboWrapper = new JPanel(new BorderLayout());
            comboWrapper.setBackground(Colors.INPUT_FIELD_BACKGROUND);
            comboWrapper.setBorder(new EmptyBorder(0, 0, borderButtom, 0));
            comboWrapper.add(BorderLayout.CENTER, b);
            panel.add(comboWrapper);

            allComboPanelWrapper.add(panel);
            allComboPanelWrapper2.add(comboWrapper);

            boxWrapper.add(panel);
        }
        // add disabled comboboxes as long as the maximum number of comboboxes that should be displayed is reached
        if (allBoxes.size() < alwaysShow) {
            for (int i = allBoxes.size(); i < alwaysShow; i++) {
                JPanel panel = new JPanel(new StackLayout());
                if (labels != null && i < labels.length) {
                    JLabel label = new JLabel(labels[i]);
                    label.setEnabled(false);
                    panel.add(label);
                }
                JPanel comboWrapper = new JPanel(new BorderLayout());
                comboWrapper.setBackground(Colors.INPUT_FIELD_BACKGROUND);
                comboWrapper.setBorder(new EmptyBorder(0, 0, borderButtom, 0));
                JComboBox c = new JComboBox();
                c.setEnabled(false);
                comboWrapper.add(BorderLayout.CENTER, c);
                panel.add(comboWrapper);
                boxWrapper.add(panel);
            }
        }

        if (isMandatory) {
            setBackgroundToElement(0, Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
        } else {
            setBackgroundToElement(0, Colors.INPUT_FIELD_BACKGROUND);
        }

        if (fillSingleValuesAutomatical) {
            if (fetchedData.size() == 1) {
                String selected = combo.getItemAt(1).getColumnName();
                combo.setSelectedIndex(1);
                addInput(level + 1, selected);
            }
        }

        revalidate();
        repaint();
    }

    public HashMap<String, ArrayList<DataColumn>> getValues() {
        return values;
    }

    protected ItemListener getCustomComboItemListener(final int level) {
        return null;
    }

    /**
     * Set a new height of the single combo boxes and adjust the existing ones.
     *
     * @param height The new height in pixel.
     */
    public void setComboHeight(int height) {
        System.out.println("set combo height from " + this.comboHeight);
        this.comboHeight = height;
        for (Box b : allBoxes) {
            b.setSize(new Dimension(1, height));
            b.setPreferredSize(new Dimension(1, height));
            b.revalidate();
            b.repaint();
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        for (Box b : allBoxes) {
            b.setEnabled(enabled);
        }
    }

    public void setEntry(UpdateableEntry entry) {
        this.entry = entry;
    }

    /**
     * Returns all JComboBoxes.
     *
     * @return All JComboBoxes.
     */
    public ArrayList<Box> getAllBoxes() {
        return allBoxes;
    }

    /**
     * Set a new thickness of the invisible buttom border of the single combo boxes.
     * <p/>
     * These can be used to create a gap between single combo boxes and/or a gap between a label and the previous combo
     * box.
     *
     * @param thickness The new thickness in pixel.
     */
    public void setBorderBottom(int thickness) {
        this.borderButtom = thickness;
    }

    public void clear() {
        addInput(1, "0");
    }

    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        DataColumn currentValue = getSelectedInput();
        if (currentValue != null) {
            list.add(new DataColumn(currentValue.getColumnName(), columnType.getColumnName()));
        } else {
            list.add(new DataColumn("-1", columnType.getColumnName()));
        }
    }

    public void save(ArrayList<DataColumn> data) {
        DataColumn currentValue = getSelectedInput();
        if (currentValue != null) {
            data.add(new DataColumn(currentValue.getColumnName(), columnType.getColumnName()));
        } else {
            data.add(new DataColumn("-1", columnType.getColumnName()));
        }
    }

    public DataColumn getSelectedInput() {
        for (int i = allBoxes.size() - 1; i >= 0; i--) {
            DataColumn selected = (DataColumn) allBoxes.get(i).getSelectedItem();
            if (selected != null) {
                return selected;
            }
        }
        return null;
    }

    public String inputToString() {
        int highestLevel = -1;
        String value = "";
        for (Box b : allBoxes) {
            if (b.level > highestLevel) {
                highestLevel = b.level;
                DataColumn v = (DataColumn) b.getSelectedItem();
                if (v != null) {
                    return v.getValue();
                }
            }
        }
        return value;
    }

    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        load(list);
    }

    public void load(DataRow list) {
        final String s = list.get(columnType);
        if (s != null) {
            setValue(s);
        }
    }

    public void setValue(String id) {
        try {
            if (id.equals("-1")) {
                return;
            }
            ArrayList<String> dat = apiCtrlAcess.getMultiComboBoxDataParents(columnType.getConnectedTableName(), id);
            addInput(1, "0");
            for (int level = 1, i = dat.size() - 1; i >= 0; i--, level++) {
                DataColumn value = DataManagementHelper.getDataColumnForId(dat.get(i), allBoxes.get(level - 1).data);
                allBoxes.get(level - 1).setSelectedItem(value);
                addInput(level + 1, dat.get(i));
            }
        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(HierarchicComboBox.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setValue(Integer id) {
        setValue(String.valueOf(id));
    }

    @Override
    public synchronized void addFocusListener(FocusListener focusListener) {
        this.focus = focusListener;
        this.focusListener.add(focusListener);
        for (Box b : allBoxes) {
            b.addFocusListener(focusListener);
        }
    }

    public void reset() {
    }

    public void setItemListener(ItemListener itemListener) {
        this.itemListener = itemListener;
    }

    public void setBackgroundToElement(int index, Color bg) {
        if (allComboPanelWrapper != null && index < allComboPanelWrapper.size() && allComboPanelWrapper2 != null && index < allComboPanelWrapper2.size()) {
            allComboPanelWrapper.get(index).setBackground(bg);
            allComboPanelWrapper2.get(index).setBackground(bg);
        }
    }

    public void setMandatory(boolean isMandatory) {
        this.isMandatory = isMandatory;
    }

    public void addAdditionalItemListener(ItemListener itemListener) {
        this.additionalItemListener = itemListener;
    }

    protected String getTableName() {
        String tableName = ColumnHelper.getTableName(columnType.getColumnName());
        if (tableName.isEmpty()) {
            // default table name
            tableName = IStandardInputUnitColumnTypes.TABLENAME_INPUT_UNIT;
        }
        return tableName;
    }

    public void reloadAvailableValues() {
        values = new HashMap<>();
        reload();
    }

    public ArrayList<Component> getFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.addAll(allBoxes);
        return comp;
    }

    public void setUserRightCheckEnabled(boolean userRightCheckEnabled) {
        this.userRightCheckEnabled = userRightCheckEnabled;
    }

    /**
     * By setting to <code>true</code>, the field will automatical fill the combobox if there is only available one
     * option.
     *
     * @param fillSingleValuesAutomatical
     */
    public void setFillSingleValuesAutomatical(boolean fillSingleValuesAutomatical) {
        this.fillSingleValuesAutomatical = fillSingleValuesAutomatical;
    }

    public void setSidebar(SidebarPanel sidebar) {
        this.sidebar = sidebar;
    }

    /**
     * A JComboBox that holds the level, data and a label.
     */
    public class Box extends XComboBox<DataColumn> {

        /**
         * The level of the combobox.
         */
        private int level;
        /**
         * The data that is saved in this combo box.
         */
        private ArrayList<DataColumn> data;

        /**
         * Constructor.
         *
         * @param level The level of the combobox.
         * @param data  The data that is saved in this combo box.
         */
        public Box(int level, ArrayList<DataColumn> data) {
            super();
            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    if (getSelectedItem() == null || getSelectedItem().toString().isEmpty()) {
                        setToolTipText(null);
                    } else {
                        setToolTipText(getSelectedItem().toString());
                    }
                }
            });

            addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                    Sidebar.setSidebarContent(sidebar);
                }
            });

            this.level = level;
            this.data = data;

            // add all data elements to the combo box
            ArrayList<DataColumn> values = new ArrayList<>();
            values.add(null);
            values.addAll(data);
            setItems(values);

            if (itemListener != null) {
                addItemListener(itemListener);
            }

            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    if (getSelectedItem() == null) {
                        setToolTipText(null);
                    } else {
                        setToolTipText(getSelectedItem().toString());
                    }
                }
            });

            if (columnType.isProjectEditMask()) {
                setEnabled(true);
            } else if (userRightCheckEnabled) {
                try {
                    setEnabled(apiCtrlAcess.hasWriteRights());
                } catch (NotLoggedInException | NotLoadedException | StatementNotExecutedException ex) {
                    setEnabled(false);
                }
            }
        }

    }
}
