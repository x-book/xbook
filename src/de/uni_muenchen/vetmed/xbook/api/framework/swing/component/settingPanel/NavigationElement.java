package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.settingPanel;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SettingPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 * This class is an extended JPanel that displays one navigation element of the navigation.
 *
 * One navigation element has an redyeable border and background that can have different colors in standard, active and
 * mouseover mode.
 *
 * Within this border there are displayed an icon and a label.
 *
 * @author Daniel Kaltenthaler
 */
public class NavigationElement extends JPanel implements MouseListener {

    /**
     * The background color of an hovered navigation element.
     */
    private final Color MOUSEOVER_BACKGROUND_COLOR = new Color(224, 232, 246);
    /**
     * The background color of a navigation element.
     */
    private final Color BACKGROUND_COLOR = Color.WHITE;
    /**
     * The background color of an active navigation element.
     */
    private final Color ACTIVE_BACKGROUND_COLOR = new Color(193, 210, 238);
    /**
     * The border color of an navigation element.
     */
    private final Color BORDER_COLOR = Color.WHITE;
    /**
     * The border color of an hovered navigation element.
     */
    private final Color MOUSEOVER_BORDER_COLOR = new Color(152, 180, 226);
    /**
     * The border color of an active navigation element.
     */
    private final Color ACTIVE_BORDER_COLOR = new Color(149, 106, 197);
    /**
     * The state if this NavigationElement element is the active one or not.
     */
    private boolean isActiveItem = false;
    /**
     * The basic JPanel holding all elements.
     */
    private JPanel panel;
    /**
     * The JPanel which holds the label of the element.
     */
    private JPanel textPanel;
    /**
     * The JPanel which holds the image of the element.
     */
    private JPanel imagePanel;
    /**
     * The JLabel holding the label of the element.
     */
    private JLabel textLabel;
    /**
     * The JLabel holding the image of the element.
     */
    private JLabel imageLabel;
    private final SettingPanel SETTING_PANEL;
    private final AbstractSettingPanel CONTENT;
    private boolean isItemClicked = false;

    public NavigationElement(SettingPanel settingPanel, String title, AbstractSettingPanel content) {
        this(settingPanel, title, content, null);
    }

    /**
     * Constructor of the NavigationElement class.
     *
     * Initalises and set up all elements of the element.
     *
     * @param settingPanel The basic Settings object.
     * @param title The text of the label that should be displayed.
     * @param content An ISettingsPanel that holds the content that should be displayed when this element is set active.
     * @param imageIcon The image that should be displayed.
     */
    public NavigationElement(SettingPanel settingPanel, String title, AbstractSettingPanel content, ImageIcon imageIcon) {
        if (imageIcon == null) {
            imageIcon = Images.SETTINGS_NAV_DEFAULT;
        }

        setPreferredSize(new Dimension(100, 86));
        setBackground(BACKGROUND_COLOR);
        setBorder(new EmptyBorder(2, 2, 2, 2));

        this.SETTING_PANEL = settingPanel;
        this.CONTENT = content;

        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setBackground(BACKGROUND_COLOR);
        panel.setBorder(new LineBorder(BORDER_COLOR));
        panel.addMouseListener(this);

        textPanel = new JPanel();
        textPanel.setBackground(BACKGROUND_COLOR);
        textPanel.setBorder(new EmptyBorder(0, 0, 10, 0));
        textLabel = new JLabel(title);
        textLabel.setBackground(BACKGROUND_COLOR);
        textPanel.add(textLabel);
        panel.add(BorderLayout.SOUTH, textPanel);

        imagePanel = new JPanel();
        imagePanel.setBackground(BACKGROUND_COLOR);
        imagePanel.setBorder(new EmptyBorder(10, 0, 0, 0));
        imageLabel = new JLabel();
        imagePanel.setBackground(BACKGROUND_COLOR);
        imageLabel.setIcon(imageIcon);
        imagePanel.add(imageLabel);
        panel.add(BorderLayout.CENTER, imagePanel);

        setLayout(new BorderLayout());
        add(BorderLayout.CENTER, panel);
    }

    /**
     * Set the navigation element active and redye the background and the border of the element.
     *
     * @param state <code>true</code> if set active, <code>false</code> else.
     */
    public void setActive(boolean state) {
        isActiveItem = state;
        if (isActiveItem) {
            panel.setBackground(ACTIVE_BACKGROUND_COLOR);
            panel.setBorder(new LineBorder(ACTIVE_BORDER_COLOR));
            textPanel.setBackground(ACTIVE_BACKGROUND_COLOR);
            textLabel.setBackground(ACTIVE_BACKGROUND_COLOR);
            imagePanel.setBackground(ACTIVE_BACKGROUND_COLOR);
            imageLabel.setBackground(ACTIVE_BACKGROUND_COLOR);
        } else {
            panel.setBackground(BACKGROUND_COLOR);
            panel.setBorder(new LineBorder(BORDER_COLOR));
            textPanel.setBackground(BACKGROUND_COLOR);
            textLabel.setBackground(BACKGROUND_COLOR);
            imagePanel.setBackground(BACKGROUND_COLOR);
            imageLabel.setBackground(BACKGROUND_COLOR);
        }
    }

    @Override
    public void mouseClicked(MouseEvent event) {
    }

    @Override
    public void mouseEntered(MouseEvent event) {
        if (event.getSource() == panel) {
            if (!isActiveItem) {
                panel.setBackground(MOUSEOVER_BACKGROUND_COLOR);
                panel.setBorder(new LineBorder(MOUSEOVER_BORDER_COLOR));
                textPanel.setBackground(MOUSEOVER_BACKGROUND_COLOR);
                textLabel.setBackground(MOUSEOVER_BACKGROUND_COLOR);
                imagePanel.setBackground(MOUSEOVER_BACKGROUND_COLOR);
                imageLabel.setBackground(MOUSEOVER_BACKGROUND_COLOR);
            }
        }
    }

    @Override
    public void mouseExited(MouseEvent event) {
        isItemClicked = false;
        if (event.getSource() == panel) {
            if (isActiveItem) {
                panel.setBackground(ACTIVE_BACKGROUND_COLOR);
                panel.setBorder(new LineBorder(ACTIVE_BORDER_COLOR));
                textPanel.setBackground(ACTIVE_BACKGROUND_COLOR);
                textLabel.setBackground(ACTIVE_BACKGROUND_COLOR);
                imagePanel.setBackground(ACTIVE_BACKGROUND_COLOR);
                imageLabel.setBackground(ACTIVE_BACKGROUND_COLOR);
            } else {
                panel.setBackground(BACKGROUND_COLOR);
                panel.setBorder(new LineBorder(BORDER_COLOR));
                textPanel.setBackground(BACKGROUND_COLOR);
                textLabel.setBackground(BACKGROUND_COLOR);
                imagePanel.setBackground(BACKGROUND_COLOR);
                imageLabel.setBackground(BACKGROUND_COLOR);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        isItemClicked = true;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (isItemClicked) {
            if (e.getSource() == panel) {
                SETTING_PANEL.deselectAllNavigationElements();
                SETTING_PANEL.setContent(CONTENT);
                isActiveItem = true;
                setActive(true);
            }
        }
    }

    public AbstractSettingPanel getContentPanel() {
        return CONTENT;
    }
}
