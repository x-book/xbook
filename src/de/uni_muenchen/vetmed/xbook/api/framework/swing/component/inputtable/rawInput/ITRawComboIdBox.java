package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput;

import com.sun.java.swing.plaf.windows.WindowsComboBoxUI;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.ComboIdBoxConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XComboIDBox;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Method;
import java.util.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;

/**
 * The raw combobox field.
 *
 * @param <C> The corresponding input constraints for the raw input field.
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITRawComboIdBox<C extends ComboIdBoxConstraints> extends AbstractITRaw<C> {

    private XComboIDBox comboBox;

    public ITRawComboIdBox(C constraints) {
        super(constraints);
    }


    @Override
    protected JComponent create() {
        JPanel wrapper = new JPanel(new BorderLayout());
        comboBox = getCombo();
        comboBox.setEditable(true);
        comboBox.getTextField().setBackground(Color.WHITE);
        comboBox.setBackground(Color.WHITE);
        comboBox.setBorder(new EmptyBorder(0, 0, 0, 0));
        comboBox.setMinimumSize(new Dimension(constraints.getWidth(), 30));
        comboBox.setMaximumSize(new Dimension(constraints.getWidth(), 30));
        comboBox.getTextField().setOpaque(true);
        comboBox.setOpaque(true);


        ComboBoxUI ui = (ComboBoxUI) UIManager.getUI(comboBox);

        ComboBoxUI realUi;
        if (ui instanceof WindowsComboBoxUI) {
            realUi = new WindowsComboBoxUI() {
                @Override
                protected ComboPopup createPopup() {
                    return new BasicComboPopup(comboBox) {
                        @Override
                        protected Rectangle computePopupBounds(int px, int py, int pw, int ph) {
                            return super.computePopupBounds(px, py, Math.max(pw, 300), ph);
                        }
                    };
                }
            };
        } else {
            realUi = new BasicComboBoxUI() {
                @Override
                protected ComboPopup createPopup() {
                    return new BasicComboPopup(comboBox) {
                        @Override
                        protected Rectangle computePopupBounds(int px, int py, int pw, int ph) {
                            return super.computePopupBounds(px, py, Math.max(pw, 300), ph);
                        }
                    };
                }
            };
        }


        comboBox.setUI(realUi);
        comboBox.getTextField().addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                actionOnFocusGain();
            }

            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                actionOnFocusLost();
                comboBox.getTextField().setCaretPosition(0);
            }
        });

        comboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    Object item = e.getItem();
                    comboBox.getTextField().setCaretPosition(0);
                }

            }
        });

        // comboBox.setUI(new BasicComboBoxUI());
        wrapper.add(BorderLayout.CENTER, comboBox);
        return wrapper;
    }

    public XComboIDBox getCombo() {
        if (comboBox == null) {
            comboBox = new XComboIDBox(constraints.getColumnType(), apiControllerAccess);
        }
        return comboBox;
    }


    public String getSelectedId() {
        return comboBox.getSelectedId();
    }

    public String getSelectedValue() {
        return (String) comboBox.getSelectedItem();
    }

    @Override
    public String getValue() {
        String displayName = constraints.getColumnType().getDisplayName();
        if (isEmpty()) {
            return "-1";
        }
        String sss = getSelectedId();
        return sss;
    }

    @Override
    public boolean isEmpty() {
        return comboBox.getSelectedItem() == null || comboBox.getSelectedItem().equals("");
    }

    @Override
    public void setValue(String value) {
        comboBox.setSelectedId(value);
    }

    @Override
    public void clear() {
        comboBox.reloadAvailableValues();
        comboBox.setSelectedItem("");
    }

    @Override
    public boolean isValidInput() {
        return !getValue().equals("-1");
    }

    @Override
    public void actionOnFocusGain() {
        comboBox.getTextField().setBackground(Colors.SIDEBAR_BACKGROUND);
    }

    @Override
    public void actionOnFocusLost() {
        super.actionOnFocusLost();
        comboBox.getTextField().setBackground(Color.WHITE);
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(comboBox);
        return comp;
    }

    @Override
    public String getStringRepresentation() {
        return getSelectedValue();
    }


    @Override
    public void addSidebarFunktionality() {
        for (Component c : comboBox.getComponents()) {
            c.addFocusListener(getSidebarFunctionality());
        }
    }


}
