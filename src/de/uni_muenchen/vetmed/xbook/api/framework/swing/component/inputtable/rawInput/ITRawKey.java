package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.Key;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.KeyConstraints;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 11.12.2017.
 */
public class ITRawKey<C extends KeyConstraints> extends AbstractITRaw<C> {
    /**
     * Constructor.
     * <p>
     * Initializes the raw input field by enbedding the actual input field in wrapper objects.
     *
     * @param constraints The corresponding input constraints for the raw input field.
     */
    public ITRawKey(C constraints) {
        super(constraints);
    }

    @Override
    protected JComponent create() {
        return null;
    }

    @Override
    public String getValue() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void setValue(String value) {
    }

    @Override
    public void clear() {
    }

    @Override
    public boolean isValidInput() {
        return false;
    }

    @Override
    public void actionOnFocusGain() {
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return null;
    }

    @Override
    public String getStringRepresentation() {
        return null;
    }

    @Override
    public void save(DataRow dataa) {
        //tgodo
        System.out.println("todo save IT Raw Key");
    }

    @Override
    public void addSidebarFunktionality() {
        // not necessary
    }
}
