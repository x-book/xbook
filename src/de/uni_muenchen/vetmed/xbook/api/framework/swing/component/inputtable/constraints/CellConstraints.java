package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints;

/**
 * Defines any constraints for a cell.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class CellConstraints {

	/**
	 * The border size of the cell in the north.
	 */
	private int borderNorth;
	/**
	 * The border size of the cell in the east.
	 */
	private int borderEast;
	/**
	 * The border size of the cell in the south.
	 */
	private int borderSouth;
	/**
	 * The border size of the cell in the west.
	 */
	private int borderWest;
	/**
	 * The size of the separator below the cell that is displayed when the cell has multi-cells activated. Default: 0
	 * (no visible separator).
	 */
	private int separatorSize = 0;

	/**
	 * Constructor.
	 *
	 * Initialize the cell with specific border sizes. Use this constructor to display no separator between the cell
	 * rows.
	 *
	 * @param borderNorth The border size of the cell in the north.
	 * @param borderEast The border size of the cell in the east.
	 * @param borderSouth The border size of the cell in the south.
	 * @param borderWest The border size of the cell in the west.
	 */
	public CellConstraints(int borderNorth, int borderEast, int borderSouth, int borderWest) {
		this.borderNorth = borderNorth;
		this.borderEast = borderEast;
		this.borderSouth = borderSouth;
		this.borderWest = borderWest;
	}

	/**
	 * Constructor.
	 *
	 * Initialize the cell with specific border sizes.
	 *
	 * @param borderNorth The border size of the cell in the north.
	 * @param borderEast The border size of the cell in the east.
	 * @param borderSouth The border size of the cell in the south.
	 * @param borderWest The border size of the cell in the west.
	 * @param separator The size of the separator between the rows of the cell.
	 */
	public CellConstraints(int borderNorth, int borderEast, int borderSouth, int borderWest, int separator) {
		this(borderNorth, borderEast, borderSouth, borderWest);
		this.separatorSize = separator;
	}

	/**
	 * Returns the size of the border in the north.
	 *
	 * @return The size of the border in the north.
	 */
	public int getBorderNorth() {
		return borderNorth;
	}

	/**
	 * Set the size of the border in the north.
	 *
	 * @param borderNorthSize The size of the border in the north.
	 */
	public void setBorderNorth(int borderNorthSize) {
		this.borderNorth = borderNorthSize;
	}

	/**
	 * Returns the size of the border in the east.
	 *
	 * @return The size of the border in the north.
	 */
	public int getBorderEast() {
		return borderEast;
	}

	/**
	 * Set the size of the border in the east.
	 *
	 * @param borderEastSize The size of the border in the east.
	 */
	public void setBorderEast(int borderEastSize) {
		this.borderEast = borderEastSize;
	}

	/**
	 * Returns the size of the border in the south.
	 *
	 * @return The size of the border in the south.
	 */
	public int getBorderSouth() {
		return borderSouth;
	}

	/**
	 * Set the size of the border in the south.
	 *
	 * @param borderSouthSize The size of the border in the south.
	 */
	public void setBorderSouth(int borderSouthSize) {
		this.borderSouth = borderSouthSize;
	}

	/**
	 * Returns the size of the border in the west.
	 *
	 * @return The size of the border in the west.
	 */
	public int getBorderWest() {
		return borderWest;
	}

	/**
	 * Set the size of the border in the west.
	 *
	 * @param borderWestSize The size of the border in the west.
	 */
	public void setBorderWest(int borderWestSize) {
		this.borderWest = borderWestSize;
	}

	/**
	 * Returns the size of the separator.
	 *
	 * @return The size of the separator.
	 */
	public int getSeparator() {
		return separatorSize;
	}

	/**
	 * Set the size of the separator.
	 *
	 * @param separatorSize The size of the separator.
	 */
	public void setSeperator(int separatorSize) {
		this.separatorSize = separatorSize;
	}

}
