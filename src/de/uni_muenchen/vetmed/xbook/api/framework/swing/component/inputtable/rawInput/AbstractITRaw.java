package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.AbstractInputConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.helper.CellRow;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.UpdateableEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * Any raw input field that defines the necessary methods
 *
 * @param <C> The corresponding input constraints for the raw input field.
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractITRaw<C extends AbstractInputConstraints>
        extends JPanel implements IInputElement {

    /**
     * The basic entry object for this field.
     */
    protected UpdateableEntry entry;
    /**
     * The corresponding input contraints for the raw input field.
     */
    protected C constraints;
    /**
     * The basic Controller object.
     */
    protected static ApiControllerAccess apiControllerAccess;

    /**
     * The row this element belongs to
     */
    protected CellRow row;

    /**
     * Constructor.
     * <p>
     * Initializes the raw input field by enbedding the actual input field in wrapper objects.
     *
     * @param constraints The corresponding input constraints for the raw input field.
     */
    public AbstractITRaw(C constraints) {
        this.constraints = constraints;

        setLayout(new BorderLayout());
        setOpaque(true);
        ComponentHelper.colorAllChildren(this, Color.yellow);

        JComponent created = create();
        created.setMinimumSize(new Dimension(100, 30));
        created.setMaximumSize(new Dimension(100, 30));
        created.setBorder(BorderFactory.createMatteBorder(0, 0, 0, constraints.getBorderEast(), Color.BLACK));

        add(BorderLayout.CENTER, created);

        //assume that focusable components are created
        for (Component component : ComponentHelper.getAllComponents(created)) {
            component.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    if (row != null) {
                        row.elementUpdated(e);
                    }
                }
            });
        }

        for (Component component : getMyFocusableComponents()) {
            component.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                    super.focusGained(e);
                    actionOnFocusGain();
                }

                @Override
                public void focusLost(FocusEvent e) {
                    super.focusLost(e);
                    actionOnFocusLost();
                }
            });
        }

        //set Sidebar functionality
        addSidebarFunktionality();
    }

    /**
     * Initialize and created the raw input field.
     *
     * @return The raw input field.
     */
    protected abstract JComponent create();

    /**
     * Retrieve the current value of the input field.
     *
     * @return The current value of the input field.
     */
    public abstract String getValue();

    /**
     * Returns whether a value was selected.
     *
     * @return true if no value was selected, false otherwise.
     */
    public abstract boolean isEmpty();

    /**
     * Set the value of the input field to a new value.
     *
     * @param value The new value of the input field.
     */
    public abstract void setValue(String value);

    @Override
    public void setEntry(UpdateableEntry entry) {
        this.entry = entry;
    }

    @Override
    public boolean isMandatory() {
        return constraints.getColumnType().isMandatory();
    }

    @Override
    public void load(DataSetOld data) {
        throw new UnsupportedOperationException("Should not be called in ITTable.");
    }

    @Override
    public void loadMultiEdit(DataSetOld data) {
        throw new UnsupportedOperationException("Should not be called in ITTable.");
    }

    @Override
    public void save(DataSetOld data) throws IsAMandatoryFieldException {
        throw new UnsupportedOperationException("Should not be called in ITTable.");
    }

    @Override
    public void setFocus() {
        throw new UnsupportedOperationException("Should not be called in ITTable.");
    }

    /**
     * Returns the corresponding input contraints for the raw input field.
     *
     * @return The corresponding input contraints for the raw input field.
     */
    public C getConstraints() {
        return constraints;
    }

    /**
     * A static method to set the basic mainframe object.
     *
     * @param apiControllerAccess The static basic mainframe object.
     */
    public static void setApiControllerAccess(ApiControllerAccess apiControllerAccess) {
        AbstractITRaw.apiControllerAccess = apiControllerAccess;
    }

    /**
     * Sets the row this element belongs to
     *
     * @param row the row the element belongs to.
     */
    public void setRow(CellRow row) {
        this.row = row;
    }

    @Override
    public void actionOnFocusLost() {
    }

    public void save(DataRow dataa) {
        dataa.put(constraints.getColumnType(), getValue());
    }

    /**
     * Adds the sidebar functionality, that means: The functionality on focus gained and focus lost.
     */
    public abstract void addSidebarFunktionality();

    protected FocusListener getSidebarFunctionality() {
        return new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (constraints != null && constraints.getColumnType().getTableSidebarText() != null) {
                    Sidebar.setSidebarContent(new SidebarEntryField(constraints.getColumnType(), constraints.getColumnType().getTableSidebarText()));
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (constraints != null && constraints.getEntry() != null && constraints.getEntry().getSideBar() != null) {
                    Sidebar.setSidebarContent(constraints.getEntry().getSideBar());
                }
            }
        };
    }

    /**
     * Called when an existing row is removed from the cell, e.g. when removing empty rows from multiple rows.
     */
    public void actionOnRemoveRow() {
    }
}
