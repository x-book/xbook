package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

public class GhostTextField extends JTextField implements FocusListener, DocumentListener, PropertyChangeListener {

    private boolean isEmpty;
    private Color ghostColor;
    private String ghostText;
    private boolean isEntering = false;

    public GhostTextField(String ghostText) {
        this("", ghostText);
    }

    public GhostTextField(String text, String ghostText) {
        super(text);

        this.ghostText = ghostText;
        this.ghostColor = Color.LIGHT_GRAY;
        addFocusListener(this);
        registerListeners();
        updateState();
        focusLost(null);
    }

    public void delete() {
        unregisterListeners();
        removeFocusListener(this);
    }

    private void registerListeners() {
        getDocument().addDocumentListener(this);
        addPropertyChangeListener("foreground", this);
    }

    private void unregisterListeners() {
        getDocument().removeDocumentListener(this);
        removePropertyChangeListener("foreground", this);
    }

    public Color getGhostColor() {
        return ghostColor;
    }

    public void setGhostColor(Color ghostColor) {
        this.ghostColor = ghostColor;
    }

    public void clear() {
        setText("");
        updateState();
    }

    private void updateState() {
        isEmpty = super.getText().length() == 0;
        if (isEmpty) {
            setForeground(ghostColor);
        } else {
            setForeground(Color.BLACK);
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
        setForeground(Color.BLACK);
        if (isEmpty) {
            unregisterListeners();
            try {
                setText("");
            } finally {
                registerListeners();
            }
        }

    }

    @Override
    public void focusLost(FocusEvent e) {
        if (isEmpty) {
            unregisterListeners();
            try {
                isEntering = true;
                setText(ghostText);
                isEntering = false;
                setForeground(ghostColor);
            } finally {
                registerListeners();
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {

        if (!evt.getPropertyName().equals("foreground")) {
            System.out.println("evt: " + evt.getPropertyName());
            updateState();
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        updateState();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        updateState();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        updateState();
    }

    public String getGhostText() {
        return ghostText;
    }

    public void setGhostText(String ghostText) {
        this.ghostText = ghostText;
    }

    @Override
    public String getText() {
        if (isEmpty) {
            return "";
        } else {
            return super.getText();
        }
    }

    @Override
    public void setText(String t) {
        super.setText(t);
        if (!t.isEmpty()) {
            setForeground(Color.BLACK);
        }
    }

    public void setDisplayText(String t) {
        setText(t);
        updateState();
    }

    public void onlyAllowIntegers() {
        ((AbstractDocument) getDocument()).setDocumentFilter(new IntegerDocumentFilter());
    }

    private class IntegerDocumentFilter extends DocumentFilter {

        @Override
        public void insertString(DocumentFilter.FilterBypass fp, int offset, String string, AttributeSet aset)
                throws BadLocationException {

            int len = string.length();
            boolean isValidInteger = true;

            for (int i = 0; i < len; i++) {
                if (!Character.isDigit(string.charAt(i))) {
                    isValidInteger = false;
                    break;
                }
            }
            if (isValidInteger || isEntering) {
                super.insertString(fp, offset, string, aset);
            } else {
                Toolkit.getDefaultToolkit().beep();
            }
        }

        @Override
        public void replace(DocumentFilter.FilterBypass fp, int offset, int length, String string, AttributeSet aset)
                throws BadLocationException {
            int len = string.length();
            boolean isValidInteger = true;

            for (int i = 0; i < len; i++) {
                if (!Character.isDigit(string.charAt(i))) {
                    isValidInteger = false;
                    break;
                }
            }
            if (isValidInteger || isEntering) {
                super.replace(fp, offset, length, string, aset);
            } else {
                Toolkit.getDefaultToolkit().beep();
            }
        }

    }
}
