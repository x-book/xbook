package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import javax.swing.JComponent;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XImageButton;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import javax.swing.ImageIcon;

/**
 * A stack element that holds a button. By using the parameters of the constructor it is possible to assign an action listener to the
 * element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackImageButton extends AbstractStackElement {

	/**
	 * The button object.
	 */
	private XImageButton button;

	/**
	 * Constructor.
	 *
	 * @param label The label of the stack element.
	 * @param listener The action listener for the button.
	 */
	public StackImageButton(String label, ActionListener listener) {
		this(label, listener, null, null);
	}

	/**
	 * Constructor.
	 *
	 * @param label The label of the stack element.
	 * @param listener The action listener for the button.
	 * @param sidebar The sidebar that is displayed when the field is focused.
	 */
	public StackImageButton(String label, ActionListener listener, SidebarPanel sidebar) {
		this(label, listener, null, sidebar);
	}

	/**
	 * Constructor.
	 *
	 * @param label The label of the stack element.
	 * @param listener The action listener for the button.
	 * @param sidebar The sidebar that is displayed when the field is focused.
	 */
	public StackImageButton(ImageIcon icon, String label, ActionListener listener, Integer labelWidth) {
		this(icon, label, listener, labelWidth, null);
	}

	/**
	 * Constructor.
	 *
	 * @param label The label of the stack element.
	 * @param listener The action listener for the button.
	 * @param labelWidth The width of the label.
	 */
	public StackImageButton(String label, ActionListener listener, Integer labelWidth) {
		this(label, listener, labelWidth, null);
	}

	public StackImageButton(String label, ActionListener listener, Integer labelWidth, SidebarPanel sidebar) {
		this(null, label, listener, labelWidth, sidebar);
	}

	/**
	 * Constructor.
	 *
	 * @param label The label of the stack element.
	 * @param listener The action listener for the button.
	 * @param labelWidth The width of the label.
	 * @param sidebar The sidebar that is displayed when the field is focused.
	 */
	public StackImageButton(ImageIcon icon, String label, ActionListener listener, Integer labelWidth, SidebarPanel sidebar) {
		super("", labelWidth, sidebar);
		if (icon != null) {
			button.setIcon(icon);
		}
		if (label != null && !label.isEmpty()) {
			button.setLabel(label);
		}
		button.addActionListener(listener);

		setHeight(Images.BUTTON_PANEL_BUTTON_LEFT.getIconHeight());

		revalidate();
		repaint();
	}

	@Override
	public JComponent getElement() {
		button = new XImageButton();
		return button;
	}

	@Override
	public String getValue() {
		// not in use
		return null;
	}


	@Override
	public void clearInput() {
	}

	@Override
	public void setMandatoryStyle() {
	}

	@Override
	public void setErrorStyle() {
	}

	@Override
	public void setDefaultStyle() {
	}

	@Override
	public void setEnabled(boolean b) {
		if (button != null) {
			button.setEnabled(b);
		}
	}

	@Override
	public void addKeyListener(KeyListener l) {
		button.addKeyListener(l);
	}

	public void setStyle(XImageButton.Style style) {
		button.setStyle(style);
	}

	@Override
	public void setValue(String value) {
		// not in use
	}

	@Override
	public void save(ProjectDataSet data) {
	}

//	@Override
//	public void load(ProjectDataSet data) {
//	}
}
