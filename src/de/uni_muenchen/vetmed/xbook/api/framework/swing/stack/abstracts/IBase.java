package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack.abstracts;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface IBase {


    /**
     * Clears the input field.
     */
    void clearInput();


    void setEnabled(boolean b);

    /**
     * Set the design of the element if it is a mandatory field.
     */
    void setMandatoryStyle();

    /**
     * Set the design of the element if there occured an error with this element
     * (e.g. wrong input values).
     */
    void setErrorStyle();

    /**
     * Set the design of the default style. Is needed when resetting the design
     * from error style.
     */
    void setDefaultStyle();

}
