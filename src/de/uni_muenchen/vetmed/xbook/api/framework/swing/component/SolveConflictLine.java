package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.exception.MissingInputException;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;
import de.uni_muenchen.vetmed.xbook.api.helper.ConflictMessageBuilder;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class SolveConflictLine extends JPanel {

    /**
     * A counter that is necessary for coloring the lines.
     */
    private static int counter = 0;
    /**
     * Holds the current index of the line.
     */
    private int index;
    /**
     * The radio button to select the local option.
     */
    private JRadioButton radioLocal;
    /**
     * The radio button to select the server option.
     */
    private JRadioButton radioGlobal;
    /**
     * The label holding the local current value .
     */
    private MultiLineTextLabel inputLocal;
    /**
     * The label holding the server current value .
     */
    private MultiLineTextLabel inputServer;
    /**
     * The label that holds the description.
     */
    private JLabel descriptionLabel;
    /**
     * Holds all local values that are displayed in this line.
     */
    private ArrayList<HashMap<ColumnType, String>> allLocalValues;
    /**
     * Holds all local display values that are displayed in this line.
     */
    private ArrayList<String> allLocalDisplayValues;
    /**
     * Holds all server values that are displayed in this line.
     */
    private ArrayList<HashMap<ColumnType, String>> allServerValues;
    /**
     * Holds all server display values that are displayed in this line.
     */
    private ArrayList<String> allServerDisplayValues;
    /**
     * Holds the description of the values that are displayed in this line.
     */
    private ArrayList<String> description;
    /**
     * Holds all table names of the values that are displayed in this line.
     */
    private ArrayList<String> tableName;

    /**
     * The middle panel wrapper.
     */
    private JPanel middle;



    /**
     * Constructor.
     * <p/>
     * Creates a solve conflict line with the specific data.
     * @param valueLocal    The value of the local data.
     * @param displayLocal  The value of the local data that should be displayed to the line.
     * @param valueServer   The value of the server data.
     * @param displayServer The value of the server data that should be displayed to the line.
     * @param description   The description of the data that is displayed in the middle label.
     * @param tableName     The correspondenting table name.
     */
    public SolveConflictLine(HashMap<ColumnType, String> valueLocal, String displayLocal, HashMap<ColumnType, String> valueServer, String displayServer, String description, String tableName) {
        this();
        addLine(valueLocal, displayLocal, valueServer, displayServer, description, tableName);
    }

    /**
     * Constructor.
     * <p/>
     * Creates an enmpty solve conflict line with no data.
     */
    public SolveConflictLine() {
        this.index = counter++;
        this.allLocalValues = new ArrayList<>();
        this.allServerValues = new ArrayList<>();
        this.allLocalDisplayValues = new ArrayList<>();
        this.allServerDisplayValues = new ArrayList<>();
        this.description = new ArrayList<>();
        this.tableName = new ArrayList<>();

        Color backgroundColor = getColor();
        setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, Color.LIGHT_GRAY));

        setLayout(new GridLayout(1, 3));

        inputLocal = new MultiLineTextLabel("");
        inputLocal.setBackground(getColor());
        inputLocal.setBorder(new EmptyBorder(6, 6, 6, 6));
        inputLocal.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                selectLocal();
            }
        });
        inputLocal.setHorizontalAlignment(StyleConstants.ALIGN_RIGHT);
        add(inputLocal);

        middle = new JPanel(new BorderLayout());
        middle.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
        middle.setBorder(new EmptyBorder(6, 6, 6, 6));
        radioLocal = new JRadioButton();
        radioLocal.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
        radioGlobal = new JRadioButton();
        radioGlobal.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
        middle.add(BorderLayout.WEST, radioLocal);
        descriptionLabel = new JLabel("");
        descriptionLabel.setHorizontalAlignment(SwingConstants.CENTER);
        descriptionLabel.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
        middle.add(BorderLayout.CENTER, descriptionLabel);
        middle.add(BorderLayout.EAST, radioGlobal);
        ButtonGroup group = new ButtonGroup();
        group.add(radioGlobal);
        group.add(radioLocal);
        add(middle);

        inputServer = new MultiLineTextLabel("");
        inputServer.setBackground(backgroundColor);
        inputServer.setBorder(new EmptyBorder(6, 6, 6, 6));
        inputServer.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                selectServer();
            }
        });
        add(inputServer);

        ////////////////////
        radioLocal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (radioLocal.isSelected()) {
                    selectLocal();
                }
            }
        });
        radioGlobal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (radioGlobal.isSelected()) {
                    selectServer();
                }
            }
        });
    }

    /**
     * Adds a new data line to an existing solve conflict line.
     *
     * @param line An existing SolveConflictLine holding the data.
     */
    public void addLine(SolveConflictLine line) {
        this.allLocalValues.addAll(line.allLocalValues);
        this.allServerValues.addAll(line.allServerValues);
        this.allLocalDisplayValues.addAll(line.allLocalDisplayValues);
        this.allServerDisplayValues.addAll(line.allServerDisplayValues);
        this.description.addAll(line.description);
        this.tableName.addAll(line.tableName);

        if (inputServer.getText().isEmpty()) {
            inputServer.setText(line.inputServer.getText());
        } else {
            inputServer.setText(inputServer.getText() + "\n\n" + line.inputServer.getText());
        }

        if (inputLocal.getText().isEmpty()) {
            inputLocal.setText(line.inputLocal.getText());
        } else {
            inputLocal.setText(inputLocal.getText() + "\n\n" + line.inputLocal.getText());
        }

        String columnText = descriptionLabel.getText();
        if (columnText.startsWith("<html>")) {
            columnText = columnText.substring("<html>".length(), columnText.length());
        }
        if (columnText.endsWith("</html>")) {
            columnText = columnText.substring(0, columnText.length() - "</html>".length());
        }

        String columnTextOther = line.descriptionLabel.getText();
        if (columnTextOther.startsWith("<html>")) {
            columnTextOther = columnTextOther.substring("<html>".length(), columnTextOther.length());
        }
        if (columnTextOther.endsWith("</html>")) {
            columnTextOther = columnTextOther.substring(0, columnTextOther.length() - "</html>".length());
        }

        if (columnText.isEmpty()) {
            descriptionLabel.setText("<html>" + columnTextOther + "</html>");
        } else {
            descriptionLabel.setText("<html>" + columnText + "<br><br>" + columnTextOther + "</html>");
        }

        ////////////////////
        // deleselect rows that have equal values
        ////////////////////
        if (isSameValue()) {
            setDefaultBackgroundColor();
            setButtonsVisible(false);
        } else {
            setSelectedBackgroundColor();
            setButtonsVisible(true);
        }
    }

    /**
     * Adds a new data line to this solve conflict line.
     *
     * @param valueLocal    The value of the local data.
     * @param displayLocal  The value of the local data that should be displayed to the line.
     * @param valueServer   The value of the server data.
     * @param displayServer The value of the server data that should be displayed to the line.
     * @param description   The description of the data that is displayed in the middle label.
     * @param tableName     The correspondenting table name.
     */
    public void addLine(HashMap<ColumnType, String> valueLocal, String displayLocal, HashMap<ColumnType, String> valueServer, String displayServer, String description, String tableName) {
        if (valueLocal.equals("-1") && displayLocal == null) {
            displayLocal = "";
        }
        if (valueServer.equals("-1") && displayServer == null) {
            displayServer = "";
        }
        this.allLocalValues.add(valueLocal);
        this.allServerValues.add(valueServer);
        this.allLocalDisplayValues.add(displayLocal);
        this.allServerDisplayValues.add(displayServer);
        this.description.add(description);
        this.tableName.add(tableName);

        if (inputServer.getText().isEmpty()) {
            inputServer.setText(displayServer);
        } else {
            inputServer.setText(inputServer.getText() + "\n\n" + displayServer);
        }

        if (inputLocal.getText().isEmpty()) {
            inputLocal.setText(displayLocal);
        } else {
            inputLocal.setText(inputLocal.getText() + "\n\n" + displayLocal);
        }

        String columnText = descriptionLabel.getText();
        if (columnText.startsWith("<html>")) {
            columnText = columnText.substring("<html>".length(), columnText.length());
        }
        if (columnText.endsWith("</html>")) {
            columnText = columnText.substring(0, columnText.length() - "</html>".length());
        }
        if (columnText.isEmpty()) {
            descriptionLabel.setText("<html><center>" + description + "</center></html>");
        } else {
            descriptionLabel.setText("<html><center>" + columnText + "<br><br>" + description + "</center></html>");
        }
        ////////////////////
        // deleselect rows that have equal values
        ////////////////////
        if (isSameValue()) {
            setDefaultBackgroundColor();
            setButtonsVisible(false);
        } else {
            setSelectedBackgroundColor();
            setButtonsVisible(true);
        }
    }

    /**
     * Set the radio buttons to select one option invisible or not.
     *
     * @param state The new visibility of the radio buttons.
     */
    public void setButtonsVisible(boolean state) {
        radioLocal.setVisible(state);
        radioGlobal.setVisible(state);
    }

    /**
     * Select the local option.
     */
    public void selectLocal() {
        if (!isSameValue()) {
            radioLocal.setSelected(true);
            inputLocal.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
            inputServer.setBackground(getColor());
        }
    }

    /**
     * Select the server option.
     */
    public void selectServer() {
        if (!isSameValue()) {
            radioGlobal.setSelected(true);
            inputServer.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
            inputLocal.setBackground(getColor());
        }
    }

    /**
     * Get the background color of this item dependant of the index.
     *
     * @return The color.
     */
    private Color getColor() {
        return Color.WHITE;
    }

    /**
     * Check if the server and local option have the same values.
     *
     * @return
     */
    public boolean isSameValue() {
        if(allLocalValues.size()!= allServerValues.size()){
            return false;
        }
        for (int i = 0; i < allLocalValues.size(); i++) {

            if(allLocalValues.get(i).size()!= allServerValues.get(i).size()){
                return false;
            }
            if (!allLocalValues.get(i).equals(allServerValues.get(i))) {

                for (Map.Entry<ColumnType, String> entry : allLocalValues.get(i).entrySet()) {

                    try {
                        if (Double.parseDouble(entry.getValue()) != (Double.parseDouble(allServerValues.get(i).get(entry.getKey())))) {

                            return false;
                        }
                    } catch (Exception ignored) {
                        return false;
                    }


                }
                return true;
            }
        }
        return true;
    }



    /**
     * Returns the tablename of the data that has been added as first one.
     *
     * @return The tablename.
     */
    public String getFirstTableName() {
        return tableName.get(0);
    }

    /**
     * Returns a specific tablename of the data.
     *
     * @return The tablename.
     */
    public String getDescription(int i) {
        return description.get(i);
    }

    /**
     * Returns a specific tablename of the data.
     *
     * @return The tablename.
     */
    public String getTableName(int i) {
        return tableName.get(i);
    }

    /**
     * Returns the selected value of the data that has been added as first one.
     *
     * @return The selected value.
     */
    public HashMap<ColumnType, String> getFirstSelectedValue() throws MissingInputException {
        return getSelectedValue(0);
    }

    /**
     * Returns the deselected of the data that has been added as first one.
     *
     * @return The deselected value.
     */
    public HashMap<ColumnType, String> getFirstNotSelectedValue() throws MissingInputException {
        return getNotSelectedValue(0);
    }


    public HashMap<ColumnType, String> getFirstLocalValue() {
        return allLocalValues.get(0);
    }

    public HashMap<ColumnType, String> getFirstGlobalValue() {
        return allServerValues.get(0);
    }

    /**
     * Returns a specific selected value of the data.
     *
     * @return The selected value.
     */
    public HashMap<ColumnType, String> getSelectedValue(int i) throws MissingInputException {
        if (isSameValue()) {
            return allLocalValues.get(i);
        }
        if (radioLocal.isSelected()) {
            return allLocalValues.get(i);
        }
        if (radioGlobal.isSelected()) {
            return allServerValues.get(i);
        }
        throw new MissingInputException();
    }

    /**
     * Returns a despecific selected value of the data.
     *
     * @return The deselected value.
     */
    public HashMap<ColumnType, String> getNotSelectedValue(int i) throws MissingInputException {
        if (isSameValue()) {
            return allLocalValues.get(i);
        }
        if (radioLocal.isSelected()) {
            return allServerValues.get(i);
        }
        if (radioGlobal.isSelected()) {
            return allLocalValues.get(i);
        }
        throw new MissingInputException();
    }

    /**
     * Saves the current selected data to the ArrayList<DbDataHash> object.
     *
     * @param dataToSave The object where to save the data.
     * @throws MissingInputException When an necessary option was not selected.
     */
    public void save(DataRow dataToSave) throws MissingInputException {
        if (allLocalValues.size() == allServerValues.size() ) {
            for (int i = 0; i < allLocalValues.size(); i++) {
                for (Map.Entry<ColumnType, String> entry : getSelectedValue(i).entrySet()) {
                    dataToSave.put(entry.getKey(),entry.getValue());
                }
            }
        }
    }

    /**
     * Set the background to the default color.
     */
    public void setDefaultBackgroundColor() {
        middle.setBackground(getColor());
        descriptionLabel.setBackground(getColor());
        radioLocal.setBackground(getColor());
        radioGlobal.setBackground(getColor());
        inputServer.setBackground(getColor());
        inputLocal.setBackground(getColor());
    }

    /**
     * Set the background to the selected color.
     */
    public void setSelectedBackgroundColor() {
        middle.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
        descriptionLabel.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
        radioLocal.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
        radioGlobal.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
        if (radioLocal.isSelected()) {
            inputLocal.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
        } else {
            inputLocal.setBackground(getColor());
        }
        if (radioGlobal.isSelected()) {
            inputServer.setBackground(Colors.TABLE_LINE_SELECTED_COLOR_DARK);
        } else {
            inputServer.setBackground(getColor());
        }
    }

    public String toString(boolean printSelected) {
        return ConflictMessageBuilder.solveConflictLineToString(this, printSelected);
    }

    public Collection<ColumnType> getKeys(){
        UniqueArrayList<ColumnType> columnTypes = new UniqueArrayList<>();
        for (HashMap<ColumnType, String> value : allLocalValues) {
            for (ColumnType columnType : value.keySet()) {
                columnTypes.add(columnType);
            }
        }
        return columnTypes;
    }

    public ArrayList<String> getTableName() {
        return tableName;
    }

    public ArrayList<String> getAllLocalDisplayValues() {
        return allLocalDisplayValues;
    }

    public ArrayList<String> getAllServerDisplayValues() {
        return allServerDisplayValues;
    }

    public boolean isSelectedGlobal() {
        return radioGlobal.isSelected();
    }
    public boolean isSelectedLocal() {
        return radioLocal.isSelected();
    }
}
