package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import java.awt.*;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public abstract class ColorManager {
    public abstract void colorize(Component c, int row, int column);
}
