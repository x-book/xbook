package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.CheckBoxConstraints;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * The raw checkbox field.
 *
 * @param <C> The corresponding input constraints for the raw input field.
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITRawCheckBox<C extends CheckBoxConstraints> extends AbstractITRaw<C> {

    protected JCheckBox checkBox;

    /**
     * Constructor.
     *
     * @param constraints The corresponding input constraints for the raw input field.
     */
    public ITRawCheckBox(C constraints) {
        super(constraints);
    }

    @Override
    protected JComponent create() {
        JPanel wrapper = new JPanel(new BorderLayout());
        JPanel checkBoxWrapper = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        checkBox = new JCheckBox();
        checkBox.setBorder(new EmptyBorder(9, 5, 8, 5));
        checkBox.setMinimumSize(new Dimension(constraints.getWidth(), 30));
        checkBox.setMaximumSize(new Dimension(constraints.getWidth(), 30));
        checkBoxWrapper.add(checkBox);
        wrapper.add(BorderLayout.CENTER, checkBoxWrapper);
        return wrapper;
    }

    @Override
    public String getValue() {
        if (checkBox.isSelected()) {
            return "1";
        } else {
            return "-1";
        }
    }

    @Override
    public boolean isEmpty() {
        return !checkBox.isSelected();
    }

    @Override
    public void setValue(String value) {
        checkBox.setSelected("1".equals(value));
    }

    @Override
    public void clear() {
        checkBox.setSelected(false);
    }

    @Override
    public boolean isValidInput() {
        return true;
    }

    @Override
    public void actionOnFocusGain() {
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(checkBox);
        return comp;
    }

    @Override
    public String getStringRepresentation() {
        return getValue();
    }

    public void addActionListener(ActionListener listener) {
        checkBox.addActionListener(listener);
    }

    public JCheckBox getCheckBox() {
        return checkBox;
    }

    @Override
    public void addSidebarFunktionality() {
        checkBox.addFocusListener(getSidebarFunctionality());
    }
}
