package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;

/**
 * A stack element that holds a combo box.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackComboBox extends AbstractStackElement {

    /**
     * The combobox object.
     */
    private JComboBox combo;

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param data The data that is displayed in the combo box.
     * @param labelWidth The width of the label.
     * @param sidebar The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
     */
    public StackComboBox(String labelText, ArrayList<String> data, Integer labelWidth, SidebarPanel sidebar) {
        super(labelText, labelWidth, sidebar);
    }

    /**
     * Constructor.
     *
     * @param columnType The correspondenting column type for this input field.
     * @param labelText The label of the stack element.
     * @param data The data that is displayed in the combo box.
     * @param labelWidth The width of the label.
     * @param sidebar The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
     */
    public StackComboBox(ColumnType columnType, String labelText, ArrayList<String> data, Integer labelWidth, SidebarPanel sidebar) {
        super(columnType, labelText, labelWidth, sidebar);

        combo.addItem("");
        for (String s : data) {
            combo.addItem(s);
        }

        combo.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                setDefaultStyle();
            }
        });

        addCustomSidebarFunctionality(combo);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param data The data that is displayed in the combo box.
     * @param labelWidth The width of the label.
     * @param sidebarText The text that will be displayed into the sidebar.
     */
    public StackComboBox(String labelText, ArrayList<String> data, Integer labelWidth, String... sidebarText) {
        this(labelText, data, labelWidth, new SidebarPanel(sidebarText));
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param data The data that is displayed in the combo box.
     * @param labelWidth The width of the label.
     */
    public StackComboBox(String labelText, ArrayList<String> data, Integer labelWidth) {
        this(labelText, data, labelWidth, (SidebarPanel) null);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param data The data that is displayed in the combo box.
     */
    public StackComboBox(String labelText, ArrayList<String> data) {
        this(labelText, data, null, (SidebarPanel) null);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param data The data that is displayed in the combo box.
     * @param sidebarText The text that will be displayed into the sidebar.
     */
    public StackComboBox(String labelText, ArrayList<String> data, String... sidebarText) {
        this(labelText, data, null, sidebarText);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param data The data that is displayed in the combo box.
     * @param sidebar The custom sidebar for this input element. <code>null</code> if no custom sidebar is available.
     */
    public StackComboBox(String labelText, ArrayList<String> data, SidebarPanel sidebar) {
        this(labelText, data, null, sidebar);
    }

    @Override
    public JComponent getElement() {
        combo = new JComboBox();
        combo.setFont(Fonts.FONT_STANDARD_PLAIN);
        return combo;
    }

    @Override
    public String getValue() {
        return (String) combo.getSelectedItem();
    }


    @Override
    public void clearInput() {
        combo.setSelectedItem("");
    }

    @Override
    public void setMandatoryStyle() {
        if (combo != null) {
            combo.setBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
            combo.setFont(Fonts.FONT_STANDARD_BOLD);
        }
    }

    @Override
    public void setErrorStyle() {
    }

    @Override
    public void setDefaultStyle() {
    }

    @Override
    public void setEnabled(boolean b) {
        if (combo != null) {
            combo.setEnabled(b);
        }
    }

    @Override
    public void addKeyListener(KeyListener l) {
        combo.addKeyListener(l);
    }

    @Override
    public void requestFocus() {
        combo.requestFocusInWindow();
    }

    @Override
    public void setValue(String value) {
        combo.setSelectedItem(value);
    }
}
