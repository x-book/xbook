package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTableOld;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.CellConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.ITConstraintsHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.ITLabelConstraintsHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.AbstractInputConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.helper.CellRow;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.helper.ITCellRowHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.helper.IndexButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.ITRawInvisibleField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.ITRawInvisibleIdField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.UpdateableEntry;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.IInputElement;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * An abstract class that defines any cell that can be inserted to the ITTable.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITCell extends JPanel implements IInputElement, LayoutManager {

    private final ColumnType idColumn;
    private final String idColumnValue;
    private boolean isExtendedTable;
    /**
     * The constraints of this cell.
     */
    protected CellConstraints cellConstraints;
    /**
     * All input constraints of the input fields within the cell.
     */
    protected ITConstraintsHelper[] inputConstraints;
    /**
     * Definition of the maximum number of rows. Is set to default value 1 in
     * constructor.
     */
    protected int maxRows = 1;
    /**
     * Storage of all rows in this cell.
     */
    protected ArrayList<CellRow> allRows = new ArrayList<>();
    /**
     * Storage of all "minus buttons" in this cell.
     */
    protected ArrayList<IndexButton> allMinusButtons = new ArrayList<>();
    /**
     * Storage of all "plus buttons" in this cell.
     */
    protected ArrayList<IndexButton> allPlusButtons = new ArrayList<>();
    /**
     * The table name used for this cell.
     */
    protected String tableName;
    private int gridY;
    /**
     * Define whether to use buttons to addCell new rows
     */
    protected boolean useButtons;

    /**
     * Constructor.
     * <p>
     * Initalise the cell with a specific tablename, the cell constraints and
     * the individual input constraints for the input fields.
     * <p>
     * This constructor sets the maximum row size to 1.
     *
     * @param tableName        The table name where to save the data.
     * @param cellConstraints  The constraints of the cell.
     * @param labelConstraints The constraints of the input fields.
     */
    public ITCell(String tableName, CellConstraints cellConstraints, ITLabelConstraintsHelper... labelConstraints) {
        this(tableName, cellConstraints, 1, convertToITConstraintsHelper(labelConstraints));
    }

    /**
     * Constructor.
     * <p>
     * Initalise the cell with a specific tablename, the cell constraints and
     * the individual input constraints for the input fields.
     * <p>
     * This constructor sets the maximum row size to 1.
     *
     * @param tableName        The table name where to save the data.
     * @param cellConstraints  The constraints of the cell.
     * @param inputConstraints The constraints of the input fields.
     */
    public ITCell(String tableName, CellConstraints cellConstraints, ITConstraintsHelper... inputConstraints) {
        this(tableName, cellConstraints, 1, inputConstraints);
    }

    /**
     * Constructor.
     * <p>
     * Initalise the cell with a specific tablename, the cell constraints and
     * the individual input constraints for the input fields.
     *
     * @param tableName        The table name where to save the data.
     * @param cellConstraints  The constraints of the cell.
     * @param maxRows          The maximum row size.
     * @param inputConstraints The constraints of the input fields.
     */
    public ITCell(String tableName, CellConstraints cellConstraints, int maxRows, ITConstraintsHelper... inputConstraints) {
        this(tableName, cellConstraints, maxRows, inputConstraints, null, null);
    }

    /**
     * Constructor.
     * <p>
     * Initalise the cell with a specific tablename, the cell constraints and
     * the individual input constraints for the input fields.
     *
     * @param tableName        The table name where to save the data.
     * @param cellConstraints  The constraints of the cell.
     * @param maxRows          The maximum row size.
     * @param inputConstraints The constraints of the input fields.
     * @param idColumn         The ID of the column (replaces Invisible Field).
     * @param idColumnValue    The value of the ID Column (mostly an ID)
     */
    public ITCell(String tableName, CellConstraints cellConstraints, int maxRows, ITConstraintsHelper[] inputConstraints, ColumnType idColumn, String idColumnValue) {
        this(tableName, cellConstraints, maxRows, inputConstraints, idColumn, idColumnValue, false);
    }

    /**
     * Constructor.
     * <p>
     * Initalise the cell with a specific tablename, the cell constraints and
     * the individual input constraints for the input fields.
     *
     * @param tableName        The table name where to save the data.
     * @param cellConstraints  The constraints of the cell.
     * @param maxRows          The maximum row size.
     * @param inputConstraints The constraints of the input fields.
     * @param idColumn         The ID of the column (replaces Invisible Field).
     * @param idColumnValue    The value of the ID Column (mostly an ID)
     * @param isExtendedTable  True if the table where the values are saved is used for multiple rows per entry
     */
    public ITCell(String tableName, CellConstraints cellConstraints, int maxRows, ITConstraintsHelper[] inputConstraints, ColumnType idColumn, String idColumnValue, boolean isExtendedTable) {
        this.tableName = tableName;
        this.cellConstraints = cellConstraints;
        this.inputConstraints = inputConstraints;
        this.maxRows = maxRows;
        this.idColumn = idColumn;
        this.idColumnValue = idColumnValue;
        this.isExtendedTable = isExtendedTable;
        setLayout(this);
        setBorder(BorderFactory.createMatteBorder(cellConstraints.getBorderNorth(),
                cellConstraints.getBorderWest(), cellConstraints.getBorderSouth(), cellConstraints.getBorderEast(), Color.BLACK));
        addRow();
    }

    /**
     * Adds a new row to the cell.
     */
    protected CellRow addRow() {
        //only addCell row if a new row is allowed
        if (allRows.size() >= maxRows) {
            return null;
        }
        // calculate the index of the new row
        int newIndex = allRows.size() + 1;
        // some initializations
        final IndexButton btnPlus;
        final IndexButton btnMinus;

        // init the row and addCell all available input fields
        CellRow row = new CellRow(this, newIndex);
        for (int i = 0; i < inputConstraints.length; i++) {
            AbstractInputConstraints c = inputConstraints[i].getConstraint();

            // addCell the east border if it's not the last item
            if (i == inputConstraints.length - 1 && maxRows == 1) {
                c.setBorderEast(0);
            }

            // addCell the input field to the row
            AbstractITRaw t = c.createInput();
            row.add(t);
            row.addRowInfo(inputConstraints[i].getColumnType(), t);
        }

        // if more than one row is allowed, addCell the necessary buttons
        if (maxRows > 1) {

            // a button to remove a specific row
            btnMinus = new IndexButton(newIndex, "–");
            btnMinus.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    boolean onlyOneRowAvailable = allMinusButtons.size() == 1;
                    removeRow(btnMinus.getIndex() - 1);
                    if (onlyOneRowAvailable) {
                        addRow();
                    }
                }
            });

            // a button to addCell a new row
            btnPlus = new IndexButton(newIndex, "+");
            btnPlus.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addRow();
                }
            });
            if (useButtons) {
                row.add(wrapComponent(50, btnPlus));
                row.add(wrapComponent(50, btnMinus));
            }
            allMinusButtons.add(btnMinus);
            allPlusButtons.add(btnPlus);
            btnMinus.setVisible(useButtons);
            btnPlus.setVisible(useButtons);
            JButton btnClear = new JButton("C");
            btnClear.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    clearAllRows();
                }
            });
            //row.addCell(wrapComponent(50, btnClear));
        }
        add(row);
        allRows.add(row);

        updateTable();

        revalidate();
        repaint();
        return row;
    }

    /**
     * Removes a row from the cell.
     */
    private void removeRow(int index) {
        CellRow rowToDelete = allRows.get(index);
        IndexButton buttonMinusToDelete = allMinusButtons.get(index);
        IndexButton buttonPlusToDelete = allPlusButtons.get(index);
        for (int i = index; i < allRows.size(); i++) {
            allRows.get(i).decreaseIndex();
            allMinusButtons.get(i).decreaseIndex();
            allPlusButtons.get(i).decreaseIndex();
        }
        rowToDelete.doActionOnRemovingARow();

        allRows.remove(rowToDelete);
        allMinusButtons.remove(buttonMinusToDelete);
        allPlusButtons.remove(buttonPlusToDelete);
        remove(rowToDelete);

        // do action on constraints when removing a row.

        updateTable();

        revalidate();
        repaint();
    }

    /**
     * Clear (remove) all Rows and addCell a new, empty row to the cell.
     */
    protected void clearAllRows() {

        removeAllRows();
        //if this is removed on loading a new row might be necessary to add.
        addRow();
    }

    /**
     * Removes all rows from this cell.
     */
    protected void removeAllRows() {
        // do action on constraints when removing a row.
        for (CellRow allRow : allRows) {
            allRow.doActionOnRemovingARow();
        }

        allRows.clear();
        allMinusButtons.clear();
        allPlusButtons.clear();
        removeAll();

        revalidate();
        repaint();

    }

    /**
     * Wraps the specified input field (component in general) with a JPanel to
     * gain a specific width.
     *
     * @param width     The favored width of the input field.
     * @param component The component to wrap.
     * @return The wrapped component.
     */
    protected JPanel wrapComponent(int width, JComponent component) {
        // content Panel
        JPanel contentPanel = new JPanel(new BorderLayout());
        int cellHeight = 30;
        contentPanel.setMinimumSize(new Dimension(width, cellHeight));
        contentPanel.setMaximumSize(new Dimension(width, cellHeight));
        contentPanel.setPreferredSize(new Dimension(width, cellHeight));
        contentPanel.add(BorderLayout.CENTER, component);

        return contentPanel;
    }

    /**
     * Update the plus buttons. Used to update the enability of the buttons.
     */
    private void updatePlusButtons() {
        boolean maxRowsReached = allPlusButtons.size() >= maxRows;
        for (int i = 0; i < allPlusButtons.size(); i++) {
            if (i == allPlusButtons.size() - 1) {
                allPlusButtons.get(i).setVisible(true);
                allPlusButtons.get(i).setEnabled(!maxRowsReached);
            } else {
                allPlusButtons.get(i).setVisible(false);
            }
        }
    }

    /**
     * Update the minus buttons.
     */
    private void updateMinusButtons() {
        //boolean onlyOneRowAvailable = allMinusButtons.size() == 1;
        //for (IndexButton btn : allMinusButtons) {
        //     btn.setEnabled(!onlyOneRowAvailable);
        //}
    }

    /**
     * Update all rows and adjust the graphical representation. Updates the
     * correct display of the borders and inserts the separators, if necessary.
     */
    private void updateRows() {
        for (int i = 0; i < allRows.size(); i++) {
            if (i == 0) {
                // if row ist the first row, do not draw the separator
                allRows.get(i).setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.BLACK));
            } else {
                // else draw the separator
                allRows.get(i).setBorder(BorderFactory.createMatteBorder(cellConstraints.getSeparator(), 0, 0, 0, Color.BLACK));
            }
        }
    }

    /**
     * Update the table, that means: The plus buttons, minus buttons, and the
     * rows themselves.
     */
    private void updateTable() {
        updateMinusButtons();
        updatePlusButtons();
        updateRows();
    }

    @Override
    public void load(DataSetOld data) {
        removeAllRows();

        // fetch data
        DataTableOld list = data.getOrCreateDataTable(tableName);

        // addCell an empty row if no data is available
        if (!list.isEmpty()) {
            // iterate all "data rows"
            for (DataRow row : list) {
                if (idColumnValue != null && idColumn != null) {
                    if (!row.get(idColumn).equals(idColumnValue)) {
                        continue;
                    }
                }
                CellRow cellRow = addRow();
                // iterate all "columns"
                for (ITCellRowHelper rh : cellRow.getRowInfos()) {
                    //small fix should be like saving
                    if (rh.getColumnType() != null) {
                        rh.getRaw().setValue(row.get(rh.getColumnType()));
                    }
                }
            }


        }
        //add a new row at the end. If max is reached nothing will happen
        addRow();
    }

    @Override
    public void loadMultiEdit(DataSetOld data) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void save(DataSetOld data) throws IsAMandatoryFieldException {
        DataTableOld list = data.getOrCreateDataTable(tableName);

        for (CellRow r : allRows) {
            //possibly this is not the best solution if a table should only have 1 datarow (inputunit) if now difference
            //kep this functionality


            // first: check if any data is entered
            boolean anythingToBeSaved = false;

            //for elements that are saved directly to the input tables, no matter what they are SAVE!
            if (maxRows > 1) {
                for (ITCellRowHelper rh : r.getRowInfos()) {

                    // ignore checking invisible fields, because they always have a value
                    AbstractITRaw raw = rh.getRaw();
                    if (raw instanceof ITRawInvisibleField || raw instanceof ITRawInvisibleIdField) {
                        continue;
                    }
                    // if one value was found, set variable to true and break for-loop
                    if (raw.isValidInput()) {
                        anythingToBeSaved = true;
                        break;
                    }
                    // else continue searching for values
                }
            } else {
                anythingToBeSaved = true;
            }
            // second: save values if they are available
            if (anythingToBeSaved) {
                DataRow dataa;
                if (isExtendedTable) {
                    dataa = new DataRow(tableName);
                    list.add(dataa);
                } else {
                    dataa = data.getDataRowForTable(tableName);
                }
                if (idColumnValue != null && idColumn != null) {
                    dataa.put(idColumn, idColumnValue);
                }

                for (ITCellRowHelper rh : r.getRowInfos()) {
                    rh.getRaw().save(dataa);
                }

            }

        }
    }

    @Override
    public void clear() {
        clearAllRows();
    }

    @Override
    public void setFocus() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isMandatory() {
        for (CellRow r : allRows) {
            for (ITCellRowHelper rh : r.getRowInfos()) {
                if (rh.getColumnType().isMandatory()) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean isValidInput() {
        for (CellRow r : allRows) {
            for (ITCellRowHelper rh : r.getRowInfos()) {
                if (!rh.getRaw().isValidInput()) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void actionOnFocusGain() {
        for (CellRow r : allRows) {
            for (ITCellRowHelper rh : r.getRowInfos()) {
                rh.getRaw().actionOnFocusGain();
            }
        }
    }

    @Override
    public void actionOnFocusLost() {
        for (CellRow r : allRows) {
            for (ITCellRowHelper rh : r.getRowInfos()) {
                rh.getRaw().actionOnFocusLost();
            }
        }
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        for (CellRow r : allRows) {
            for (ITCellRowHelper rh : r.getRowInfos()) {
                comp.addAll(rh.getRaw().getMyFocusableComponents());
            }
        }
        return comp;
    }

    @Override
    public void setEntry(UpdateableEntry entry) {
        for (CellRow r : allRows) {
            for (ITCellRowHelper rh : r.getRowInfos()) {
                rh.getRaw().setEntry(entry);
            }
        }
    }

    @Override
    public String getStringRepresentation() {
        String s = "";
        for (CellRow r : allRows) {
            for (ITCellRowHelper rh : r.getRowInfos()) {

                s += rh.getRaw().getStringRepresentation() + "-";
            }
            s += "|";
        }
        return s;
    }

    public int getGridY() {
        //TODO possibly check for sub rows?
        int height = 0;

        for (ITConstraintsHelper inputConstraint : inputConstraints) {
            height = Math.max(inputConstraint.getConstraint().getHeight(), height);
        }
        return height * allRows.size();
    }

    public int getNumberOfGridsX() {
        int width = 0;
        for (ITConstraintsHelper inputConstraint : inputConstraints) {
            width += inputConstraint.getConstraint().getWidth();
        }
        return width;
    }


    @Override
    public void addLayoutComponent(String name, Component comp) {

    }

    @Override
    public void removeLayoutComponent(Component comp) {

    }

    @Override
    public Dimension preferredLayoutSize(Container target) {
        synchronized (target.getTreeLock()) {
            Dimension dim = new Dimension(target.getWidth(), 0);
            ArrayList<CellRow> allCelles = allRows;
            for (CellRow cell : allCelles) {
                if (cell.isVisible()) {
                    dim.height = Math.max(cell.getGridY() * ITConstants.CELL_HEIGHT, dim.height);
                }
            }
            return dim;
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container target) {
        synchronized (target.getTreeLock()) {
            Dimension dim = new Dimension(target.getWidth(), 0);
            ArrayList<CellRow> allCelles = allRows;
            for (CellRow cell : allCelles) {
                if (cell.isVisible()) {
                    dim.height = Math.max(cell.getGridY() * ITConstants.CELL_HEIGHT, dim.height);
                }
            }
            return dim;
        }
    }

    @Override
    public void layoutContainer(Container target) {
        synchronized (target.getTreeLock()) {
            Insets insets = target.getInsets();
            int top = insets.top;
            int bottom = target.getHeight() - insets.bottom;
            int left = insets.left;
            int right = target.getWidth() - insets.right;

            int width = right - left;
//            int gridX = 0;
            int gridYMax = 0;


            for (ITConstraintsHelper inputConstraint : inputConstraints) {
                gridYMax = Math.max(inputConstraint.getConstraint().getHeight(), gridYMax);
            }
            gridYMax = Math.max(gridY, gridYMax * allRows.size());
            ArrayList<CellRow> allCelles = allRows;
//            for (CellRow cell : allCelles) {
//                if (cell.isVisible())
//                    gridX += cell.getGridX();
//            }
//
            int y = top;
            for (CellRow cell : allCelles) {
                int height = gridYMax / allCelles.size() * ITConstants.CELL_HEIGHT;
                cell.setDisplayGridY(gridYMax / allCelles.size());
                cell.setBounds(left, y, width, height);
                y += height;
            }
        }
    }

    public void setGridY(int gridY) {
        this.gridY = gridY;
    }

    /**
     * Should be called if a row was updated. This method then checks if a new row should be added or removed.
     *
     * @param row   The row that was updated
     * @param empty True if the row is now empty, false otherwise
     */
    public void rowUpdated(CellRow row, boolean empty) {
        int i = allRows.indexOf(row);
        if (i == -1) {
            System.out.println("row not found! this should not be possible!");
            ;
            return;
        }

        if (empty) {
            //only remove empty row if this not the last row
            if (i < allRows.size() - 1) {
                //check if last row is empty
                CellRow cellRow = allRows.get(allRows.size() - 1);
                boolean lastRowEmpty = cellRow.isEmpty();
                removeRow(i);
                //if the last row is not empty addCell a new row
                if (!lastRowEmpty) {
                    addRow();
                }
            }
        } else {
            //if this is the last row addCell a new row, otherwise
            if (i == allRows.size() - 1) {
                addRow();
            }
        }
    }

    public ArrayList<CellRow> getAllRows() {
        return allRows;
    }

    /**
     * Converts a list of ITLabelConstraintsHelper objects to a list of ITConstraintsHelper objects.
     *
     * @param inputConstraints The list of ITLabelConstraintsHelper objects to convert.
     * @return The converted list of ITConstraintsHelper objects.
     */
    private static ITConstraintsHelper[] convertToITConstraintsHelper(ITLabelConstraintsHelper[] inputConstraints) {
        ArrayList<ITConstraintsHelper> converted = new ArrayList<>();
        for (ITLabelConstraintsHelper l : inputConstraints) {
            converted.add(new ITConstraintsHelper(l.getConstraint()));
        }

        return converted.toArray(new ITConstraintsHelper[converted.size()]);
    }

}
