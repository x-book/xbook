package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import java.awt.event.KeyListener;
import javax.swing.JComponent;
import javax.swing.JLabel;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;

/**
 * A stack element that displays a placeholder.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackPlaceholder extends AbstractStackElement {

    /**
     * the JLabel holding the text of the place holder.
     */
    private JLabel label;

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param labelWidth The width of the label.
     */
    public StackPlaceholder(String labelText, Integer labelWidth) {
        super(labelText, labelWidth);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     */
    public StackPlaceholder(String labelText) {
        super(labelText, null, null);
    }

    @Override
    public JComponent getElement() {
        label = new JLabel("Place Holder");
        label.setFont(Fonts.FONT_STANDARD_BOLD);
        label.setBackground(Colors.CONTENT_BACKGROUND);
        return label;
    }

    @Override
    public String getValue() {
        return null;
    }



    @Override
    public void clearInput() {
        // nothing to do
    }

    @Override
    public void setMandatoryStyle() {
    }

    @Override
    public void setErrorStyle() {
    }

    @Override
    public void setDefaultStyle() {
    }

    @Override
    public void setEnabled(boolean b) {
    }

    @Override
    public void addKeyListener(KeyListener l) {
    }

    @Override
    public void requestFocus() {
    }

    @Override
    public void setValue(String value) {
        // nothing to do
    }

    @Override
    public void save(ProjectDataSet data) throws IsAMandatoryFieldException {

    }

}
