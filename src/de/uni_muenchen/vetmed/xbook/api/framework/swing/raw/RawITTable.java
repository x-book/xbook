package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.ITTable;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.ITTableRow;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.CellConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.ITConstraintsHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.LabelConstraints;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class RawITTable extends AbstractInputElementFunctions {

    /**
     * The text field object.
     */
    protected ITTable table;

    public RawITTable(ITTable table) {
        // super((ColumnType) null);
        super(new ColumnType("Test", ColumnType.Type.VALUE, ColumnType.ExportType.NONE));
        this.table = table;
        createField();
    }

    @Override
    public void load(DataSetOld data) {
        table.load(data);
    }

    @Override
    public void clear() {
        table.clear();
    }

    @Override
    public void save(DataSetOld data) {
        try {
            table.save(data);
        } catch (IsAMandatoryFieldException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return table.toString();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return table.getMyFocusableComponents();
    }

    @Override
    public boolean isValidInput() {
        return table.isValidInput();
    }

    public ITTable getTable() {
        return table;
    }

    public String getJUnitDefaultValue() {
        return "not in use";
    }



    @Override
    public void createField() {
        if (table != null) {
            setLayout(new BorderLayout());
            add(BorderLayout.CENTER, table);
        }
    }

    @Override
    public void setFocus() {
        table.requestFocusInWindow();
    }

    @Override
    public String getStringRepresentation() {
        return table.getStringRepresentation();
    }


}
