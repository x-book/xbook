package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JComponent;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XLabel;

/**
 * A stack element that holds a clickable label.
 *
 * By using the parameters of the constructor it is possible to assign an mouse listener to the element that is run when
 * interacting with the label.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackClickableLabel extends AbstractStackElement {

    /**
     * The valid options for the orientation.
     */
    public enum Orientation {
        LEFT, RIGHT;
    }
    /**
     * The link object.
     */
    private XLabel link;

    /**
     * Constructor.
     *
     * @param label The label of the link.
     * @param orientation The orientation where the link should be displayed. (Use <code>Orientation.LEFT</code> or
     * <code>Orientation.RIGHT</code>.
     * @param listener The mouse listener for the button.
     * @param labelWidth The width of the label.
     */
    public StackClickableLabel(String label, Orientation orientation, MouseListener listener, Integer labelWidth) {
        super("", labelWidth);

        setHeight(12);

        String text = "<html><u>" + label + "</u></html>";
        link = new XLabel(text);
        link.addMouseListener(listener);
        link.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                link.setForeground(Color.BLUE);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                link.setForeground(Color.BLACK);
            }
        });

        if (orientation == Orientation.RIGHT) {
            wrapper.add(BorderLayout.EAST, link);
        } else {
            wrapper.add(BorderLayout.CENTER, link);
        }
    }

    /**
     * Constructor.
     *
     * @param label The label of the link.
     * @param orientation The orientation where the link should be displayed. (Use <code>Orientation.LEFT</code> or
     * <code>Orientation.RIGHT</code>.
     * @param listener The mouse listener for the button.
     */
    public StackClickableLabel(String label, Orientation orientation, MouseListener listener) {
        this(label, orientation, listener, null);
    }

    /**
     * Constructor.
     *
     * @param label The label of the link.
     * @param listener The mouse listener for the button.
     * @param labelWidth The width of the label.
     */
    public StackClickableLabel(String label, MouseListener listener, Integer labelWidth) {
        this(label, Orientation.RIGHT, listener, labelWidth);
    }

    /**
     * Constructor.
     *
     * @param label The label of the link.
     * @param listener The mouse listener for the button.
     */
    public StackClickableLabel(String label, MouseListener listener) {
        this(label, Orientation.RIGHT, listener, null);
    }

    @Override
    public JComponent getElement() {
        return null;
    }

    @Override
    public String getValue() {
        // not in use
        return null;
    }

    @Override
    public void clearInput() {
    }

    @Override
    public void setMandatoryStyle() {
    }

    @Override
    public void setErrorStyle() {
    }

    @Override
    public void setDefaultStyle() {
    }

    @Override
    public void setEnabled(boolean b) {
        if (link != null) {
            link.setEnabled(b);
        }
    }

    @Override
    public void addKeyListener(KeyListener l) {
        link.addKeyListener(l);
    }

    @Override
    public void setValue(String value) {
        // not in use
    }

}
