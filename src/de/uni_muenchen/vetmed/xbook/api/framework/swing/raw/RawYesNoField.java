package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.YesNoField;
import de.uni_muenchen.vetmed.xbook.api.gui.content.entry.inputfields.miscellaneous.IYesNoField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * A text field as input element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class RawYesNoField extends AbstractInputElementFunctions {

    /**
     * The text field object.
     */
    protected YesNoField field;

    public RawYesNoField(ColumnType columnType) {
        super(columnType);
    }

    @Override
    public void load(DataSetOld data) {
        DataRow row = data.getDataRowForTable(getTableName());
        final String s = row.get(columnType);
        if (s != null) {
            setSelected(s);
        }
    }

    @Override
    public void clear() {
        field.setSelectedNone();
    }

    @Override
    public void save(DataSetOld data) {
        DataRow row = data.getDataRowForTable(getTableName());
        row.add(new DataColumn(getSelectedID(), columnType.getColumnName()));
    }

    @Override
    public String getStringRepresentation() {
        return getSelectedID();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(field.getYes());
        comp.add(field.getNo());
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return !toString().equals("-1");
    }


    @Override
    public void createField() {
        setLayout(new BorderLayout());
        field = new YesNoField();
        add(BorderLayout.CENTER, field);
    }

    @Override
    public void setFocus() {
        field.requestFocusInWindow();
    }

    // ========================================================================
    public JRadioButton getYesField() {
        return field.getYes();
    }

    public JRadioButton getNoField() {
        return field.getNo();
    }

    public void setSelectedYes() {
        field.setSelectedYes(true);
    }

    public void setSelectedNo() {
        field.setSelectedNo(true);
    }

    public void setSelectedNone() {
        field.setSelectedNone();
    }

    public void setSelected(String id) {
        if (id.equals("1")) {
            setSelectedYes();
        } else if (id.equals("0")) {
            setSelectedNo();
        } else {
            setSelectedNone();
        }
    }

    public void setSelected(IYesNoField.State state) {
        switch (state) {
            case YES:
                setSelectedYes();
                break;
            case NO:
                setSelectedNo();
                break;
            case NONE:
                setSelectedNone();
        }
    }

    public IYesNoField.State getSelectedValue() {
        if (isSelectedNone()) {
            return IYesNoField.State.NONE;
        } else if (isSelectedYes()) {
            return IYesNoField.State.YES;
        } else /* if (isSelectedNo()) */ {
            return IYesNoField.State.NO;
        }
    }

    public String getSelectedID() {
        return getSelectedValue().getId();
    }

    public boolean isSelectedYes() {
        return field.isSelectedYes();
    }

    public boolean isSelectedNo() {
        return field.isSelectedNo();
    }

    public boolean isSelectedNone() {
        return field.isSelectedNone();
    }
}
