package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import javax.swing.JComponent;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;

/**
 * A stack element that holds a button. By using the parameters of the constructor it is possible to assign an action
 * listener to the element.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackXButton extends AbstractStackElement {

    /**
     * The button object.
     */
    private XButton button;

    /**
     * Constructor.
     *
     * @param label The label of the stack element.
     * @param listener The action listener for the button.
     */
    public StackXButton(String label, ActionListener listener) {
        this(label, listener, null, null);
    }

    /**
     * Constructor.
     *
     * @param label The label of the stack element.
     * @param listener The action listener for the button.
     * @param sidebar The sidebar that is displayed when the field is focused.
     */
    public StackXButton(String label, ActionListener listener, SidebarPanel sidebar) {
        this(label, listener, null, sidebar);
    }

    /**
     * Constructor.
     *
     * @param label The label of the stack element.
     * @param listener The action listener for the button.
     * @param labelWidth The width of the label.
     */
    public StackXButton(String label, ActionListener listener, Integer labelWidth) {
        this(label, listener, labelWidth, null);
    }

    /**
     * Constructor.
     *
     * @param label The label of the stack element.
     * @param listener The action listener for the button.
     * @param labelWidth The width of the label.
     * @param sidebar The sidebar that is displayed when the field is focused.
     */
    public StackXButton(String label, ActionListener listener, Integer labelWidth, SidebarPanel sidebar) {
        super("", labelWidth, sidebar);
        button.setText(label);
        button.addActionListener(listener);
    }

    @Override
    public JComponent getElement() {
        button = new XButton("");
        return button;
    }

    @Override
    public String getValue() {
        // not in use
        return null;
    }

    @Override
    public void clearInput() {
    }

    @Override
    public void setMandatoryStyle() {
    }

    @Override
    public void setErrorStyle() {
    }

    @Override
    public void setDefaultStyle() {
    }

    @Override
    public void setEnabled(boolean b) {
        if (button != null) {
            button.setEnabled(b);
        }
    }

    @Override
    public void addKeyListener(KeyListener l) {
        button.addKeyListener(l);
    }

    public void setStyle(XButton.Style style) {
        button.setStyle(style);
    }

    @Override
    public void setValue(String value) {
        // not in use
    }

    @Override
    public void save(ProjectDataSet data) {
    }

//    @Override
//    public void load(ProjectDataSet data) {
//    }
}
