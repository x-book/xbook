package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XComboBox;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusListener;
import java.util.*;
import java.util.List;

/**
 * @param <T>
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractDynamicComboBoxList<T, E> extends AbstractMultiComponent<T> {

    /**
     * The dynamic combo box that is used above the list.
     */
    private XComboBox<T> combo;
    protected E databaseData;
    private boolean addEmptyEntry = false;

    /**
     * Constructor of the DynamicComboBoxList class.
     *
     * @param name
     */
    public AbstractDynamicComboBoxList(E data, String name) {
        super(name);
        this.databaseData = data;
        init();
    }

    @Override
    protected JComponent getInputComponent() {
        Object[] data = getValuesForCombo();
        if (data != null) {
            combo = new XComboBox(data);
        } else {
            combo = new XComboBox<>();

        }
        combo.setEditable(true);
        combo.setCustomInputEnabled(false);
        combo.setIntegerInputEnabled(true);
        combo.setAutoSort(true);
        return combo;
    }

    protected abstract Object[] getValuesForCombo();

    protected abstract E reloadDatabaseValues();

    @Override
    protected JComponent getInputTextComponent() {
        return combo.getTextField();
    }

    public abstract T getEmptyItem();

    public void updateData(boolean ignoreSelected) {
        ArrayList<T> selected = selectedItemsOnList;
        databaseData = reloadDatabaseValues();
        Object[] data = getValuesForCombo();
        combo.removeAllItems();
        for (Object d : data) {
            T t = (T) d;
            if (!selected.contains(t)) {
                combo.addItem(t);
            }
        }
        if (!ignoreSelected) {
            HashSet hashSet = new HashSet(Arrays.asList(data));
            SortableListModel dfm = (SortableListModel) listSelectedItems.getModel();
            for (T s : selected) {
                if (!hashSet.contains(s)) {
                    dfm.removeElement(s);
                }
            }
        }
    }

    @Override
    public abstract Comparator<T> getComparator();

    @Override
    public void addInputToList() {
        T selected = (T) combo.getSelectedItem();
        String input = combo.getTextField().getText();
        if (input.isEmpty() && (selected == null || selected == getEmptyItem())) {
            return;
        }
        if (containsValue(selected)) {
            addItemToList(selected);
            actionOnValueFound();
        } else {
            actionOnValueNotFound();
        }

        combo.setSelectedItem(getEmptyItem());
    }

    protected abstract boolean containsValue(T value);

    public void actionOnValueNotFound() {
        Footer.displayWarning(Loc.get("ID_NOT_FOUND", name));
    }

    /**
     * Can be used for classes that want to do stuff could be made into an event
     */
    public void actionOnValueFound() {

    }

    /**
     * Removes all items from the list and set the new ones.
     *
     * @param items
     */
    public void setItemsToCombo(ArrayList<T> items) {
        combo.removeAllItems();
        combo.setItems(items);
    }

    @Override
    public boolean addItemToList(T item) {
        if (item == null || item.equals(getEmptyItem())) {
            return false;
        }

        if (containsValue(item)) {
            if (!selectedItemsOnList.contains(item)) {
                // only add item if entry is NOT already in list
                selectedItemsOnList.add(item);
                listModel.addElement(item);
                combo.removeItem(item);
                return true;
            } else {
                // element is already in list
                combo.setSelectedItem(getEmptyItem());
            }
        } else {
            Object[] valuesForCombo = getValuesForCombo();
            if (valuesForCombo != null && valuesForCombo.length != 0) {
                Footer.displayWarning(Loc.get("NO_VALID_INPUT_ON_FIELD", name));
            }
        }

        return false;
    }

    @Override
    public boolean addItemsToList(ArrayList<T> items) {
        if (items == null || items.isEmpty()) {
            return false;
        }
        for (T item : items) {
            addItemToList(item);
        }
        return true;
    }

    @Override
    public void removeSelectedItemsFromList() {
        int[] selected = listSelectedItems.getSelectedIndices();
        T selectedItem = (T) combo.getSelectedItem();
        for (int i = selected.length - 1; i >= 0; i--) {
            if (containsValue(listModel.getElementAt(selected[i]))) {
                combo.addItem(listModel.getElementAt(selected[i]));
            }
            selectedItemsOnList.remove(selected[i]);
            listModel.removeElementAt(selected[i]);
        }
        combo.setSelectedItem(selectedItem);
    }

    @Override
    public void removeAllItemsFromList() {
        T selectedItem = (T) combo.getSelectedItem();
        for (int i = listModel.getSize() - 1; i >= 0; i--) {
            combo.addItem(listModel.getElementAt(i));
            selectedItemsOnList.remove(i);
            listModel.removeElementAt(i);
        }
        combo.setSelectedItem(selectedItem);
    }

    @Override
    public void addAllItems() {
        for (Object s : getValuesForCombo()) {
            addItemToList((T) s);
        }
    }

    /**
     * Returns the DynamicComboBox object (the textfield).
     *
     * @return The DynamicComboBox object.
     */
    public XComboBox getComboBox() {
        return combo;
    }

    @Override
    public ArrayList<T> getListItems(boolean addComboItemToo) {
        ArrayList<T> list = new ArrayList<>();
        for (int i = 0; i < listModel.size(); i++) {
            list.add(listModel.getElementAt(i));
        }

        if (addComboItemToo) {
            JTextField textField = (JTextField) combo.getEditor().getEditorComponent();
            if (!textField.getText().isEmpty()) {
                list.add((T) combo.getSelectedItem());
            }
        }
        return list;
    }

    @Override
    public void clearInput() {
        combo.setSelectedItem(getEmptyItem());
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        combo.setVisible(b);
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        combo.setEnabled(b);
    }

    @Override
    public void addFocusListener(FocusListener fl) {
        super.addFocusListener(fl);
        combo.addFocusListener(fl);
    }

    @Override
    public ArrayList<Component> getFocusableComponents() {
        ArrayList<Component> comp = super.getFocusableComponents();
        comp.add(combo.getTextField());
        return comp;
    }

    protected T getSelectedComboItem() {
        return (T) combo.getSelectedItem();
    }

    protected String getComboTextFieldInput() {
        return combo.getTextField().getText();
    }

    protected void setSelectedComboItem(T item) {
        combo.setSelectedItem(item);
    }

    @Override
    public boolean addItemsToList(List<T> items) {
        for (T item : items) {
            combo.addItem(item);
        }
        return true;
    }

    @Override
    public void removeItem(int index) {
        selectedItemsOnList.remove(index);
        listModel.removeElementAt(index);
    }

    @Override
    public T getElementFromInput() {
        return (T) combo.getSelectedItem();
    }

    public void setAddEmptyEntry(boolean addEmptyEntry) {
        this.addEmptyEntry = addEmptyEntry;
    }

}
