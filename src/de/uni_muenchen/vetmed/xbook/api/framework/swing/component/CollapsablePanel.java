package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class CollapsablePanel extends JPanel {
    /**
     * The logger object of this class.
     */
    private static final Log LOGGER = LogFactory.getLog(CollapsablePanel.class);

    private static final int TITLE_HEIGHT = 32;
    private static final int ARROW_LABEL_WIDTH = 16;
    private static final int HORIZONTAL_BORDER = 10;
    private static final int TOP_BOTTOM_BORDER_LABEL = 6;

    private boolean isCollapsed = false;
    private String title;

    private JComponent collapsablePanel = null;
    private JLabel titleLabel;
    private JLabel arrowLabel;
    private JPanel contentWrapper;

    public CollapsablePanel(String title) {
        this(title, null, false);
    }

    public CollapsablePanel(String title, JComponent collapsablePanel) {
        this(title, collapsablePanel, false);
    }

    public CollapsablePanel(String title, JComponent collapsablePanel, boolean isCollapsed) {
        super(new BorderLayout());
        this.title = title;
        this.isCollapsed = isCollapsed;
        this.collapsablePanel = collapsablePanel;
        init();
        setCollapsableContent(collapsablePanel);
    }

    private void init() {
        setBackground(Colors.CONTENT_BACKGROUND);
        arrowLabel = new JLabel();
        arrowLabel.setOpaque(true);
        arrowLabel.setBackground(Colors.BOOK_COLOR);
        arrowLabel.setForeground(Color.WHITE);
        arrowLabel.setPreferredSize(new Dimension(ARROW_LABEL_WIDTH, TITLE_HEIGHT));
        arrowLabel.addMouseListener(getMouseListener());
        arrowLabel.setFont(Fonts.FONT_BIG_BOLD);

        titleLabel = new JLabel();
        titleLabel.setOpaque(true);
        titleLabel.setBackground(Colors.BOOK_COLOR);
        titleLabel.setForeground(Color.WHITE);
        titleLabel.setPreferredSize(new Dimension(1, TITLE_HEIGHT));
        titleLabel.addMouseListener(getMouseListener());
        titleLabel.setFont(Fonts.FONT_BIG_BOLD);

        updateTitle();

        JPanel titleWrapper = new JPanel(new BorderLayout());
        titleWrapper.setBackground(Colors.BOOK_COLOR);
        titleWrapper.add(BorderLayout.WEST, ComponentHelper.wrapComponent(arrowLabel, Colors.BOOK_COLOR, 0, 8, 0, 0));
        titleWrapper.add(BorderLayout.CENTER, titleLabel);
        add(BorderLayout.NORTH, ComponentHelper.wrapComponent(
                ComponentHelper.wrapComponent(titleWrapper,
                        Colors.BOOK_COLOR, 0, HORIZONTAL_BORDER, 0, HORIZONTAL_BORDER)
                , Colors.CONTENT_BACKGROUND, TOP_BOTTOM_BORDER_LABEL, 0, TOP_BOTTOM_BORDER_LABEL, 0));

        contentWrapper = new JPanel(new BorderLayout());
        JLabel filler = new JLabel();
        filler.setBackground(Colors.BOOK_COLOR);
        filler.setPreferredSize(new Dimension(2 * HORIZONTAL_BORDER + ARROW_LABEL_WIDTH - 2, 1));
        contentWrapper.add(BorderLayout.WEST, filler);
        add(BorderLayout.CENTER, contentWrapper);
    }

    private void setCollapsableContent(JComponent contentToSet) {
        contentWrapper.add(BorderLayout.CENTER, contentToSet);
    }

    private MouseListener getMouseListener() {
        return new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                setCollapsed(!isCollapsed);
            }
        };
    }

    private void updateTitle() {
        String arrow = "▼";
        if (isCollapsed) {
            arrow = "►";
        }
        arrowLabel.setText(arrow);
        titleLabel.setText(title);
    }

    public void setCollapsed(boolean s) {
        this.isCollapsed = s;
        collapsablePanel.setVisible(!s);
        updateTitle();
    }

    public boolean isCollapsed(boolean s) {
        return isCollapsed;
    }
}