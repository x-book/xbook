package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable;

import com.jidesoft.plaf.LookAndFeelFactory;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.ITConstraintsHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.CellConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.CheckBoxConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.ComboBoxConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.LabelConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.TextFieldConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.*;

/**
 * @author Daniel Kaltenthaler
 */
public class MainInputTableTest extends JFrame {

    public MainInputTableTest() {
        setLayout(new StackLayout());

        setSize(1024, 640);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        add(getTestTable());

        revalidate();
        repaint();
    }

    private ITTable getTestTable() {
        ITTable table = new ITTable(null);

        ITTableRow row = new ITTableRow();
        CellConstraints c;
        c = new CellConstraints(1, 0, 1, 1, 1);
//        row.addCell("tableName", c, new ITConstraintsHelper(new TextFieldConstraints(null, 1, 1, 0)));
        c = new CellConstraints(1, 1, 1, 1, 1);
//        row.addCell("tableName", c, 3,
//                new ITConstraintsHelper(new TextFieldConstraints(null, 1, 1, 1)),
//                new ITConstraintsHelper(new LabelConstraints(1, 1, 1, "test")),
//                new ITConstraintsHelper(new CheckBoxConstraints(null, 2, 1, 1)),
//                new ITConstraintsHelper(new ComboBoxConstraints(null, 1, 1, 1, null)));

        table.addRow(row);
        return table;
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(WindowsLookAndFeel.class.getName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        LookAndFeelFactory.installJideExtension();
        new MainInputTableTest();
    }

}
