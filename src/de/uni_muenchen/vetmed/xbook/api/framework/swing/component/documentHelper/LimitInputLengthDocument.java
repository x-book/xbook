package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.documentHelper;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class LimitInputLengthDocument extends PlainDocument {

    private final int maxLength;

    public LimitInputLengthDocument(int maxLength) {
        this.maxLength = maxLength;
    }

    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        if ((getContent().length() - 1) + str.length() <= maxLength) {
            super.insertString(offs, str, a);
        } else {
            super.insertString(offs, str.substring(0, Math.min(maxLength - (getContent().length() - 1), str.length())), a);
            Footer.displayWarning(Loc.get("MAXIMUM_INPUT_LENGTH_IS", maxLength + ""));
        }
    }

}
