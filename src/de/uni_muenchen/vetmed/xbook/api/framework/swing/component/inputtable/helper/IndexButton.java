package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.helper;

import javax.swing.JButton;

/**
 * An IndexButton is a button that also holds the information of an ID. This is used for the ITTable to keep the index
 * of the row that holds the button also in the button object.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class IndexButton extends JButton {

	/**
	 * The index of the row in the cell where the button is included.
	 */
	private int index;

	/**
	 * Constructor.
	 *
	 * Intitialize the IndexButton with a label and the specified index.
	 *
	 * @param index The specified index.
	 * @param text The text to be displayed in the button.
	 */
	public IndexButton(int index, String text) {
		super(text);
		this.index = index;
	}

	/**
	 * Decreased the index by 1.
	 */
	public void decreaseIndex() {
		index--;
	}

	/**
	 * Returns the current index.
	 *
	 * @return The current index.
	 */
	public int getIndex() {
		return index;
	}

}
