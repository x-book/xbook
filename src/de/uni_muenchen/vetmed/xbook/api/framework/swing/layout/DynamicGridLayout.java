package de.uni_muenchen.vetmed.xbook.api.framework.swing.layout;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.BitSet;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class DynamicGridLayout implements LayoutManager {

    /**
     * The default value for the grid size (width and height).
     */
    private static final Dimension DEFAULT_GRID_SIZE = new Dimension(50, 50);
    /**
     * Necessary size of the layout. Used by getLayoutSize-Methods.
     */
    private Dimension layoutSize;
    /**
     * Size of the single grid units.
     */
    private Dimension gridUnitSize;

    /**
     * Constructor.
     *
     * Initialises the dynamic grid layout with the default grid size.
     */
    public DynamicGridLayout() {
        this(DEFAULT_GRID_SIZE);
    }

    /**
     * Constructor.
     *
     * Initialises the dynamic grid layout.
     *
     * @param gridSize The size of the grid units.
     */
    public DynamicGridLayout(Dimension gridSize) {
        this.gridUnitSize = gridSize;
    }

    /**
     * Constructor.
     *
     * Initialises the dynamic grid layout.
     *
     * @param gridWidth The width of a grid unit.
     * @param gridHeight The height of a grid unit.
     */
    public DynamicGridLayout(int gridWidth, int gridHeight) {
        this(new Dimension(gridWidth, gridHeight));
    }

    @Override
    public void layoutContainer(Container parent) {
        synchronized (parent.getTreeLock()) {
            Insets insets = parent.getInsets();
            int columns = Math.max(((parent.getWidth() - insets.left - insets.right) / gridUnitSize.width), 2);

            // a bitset object that helps identifying if specific grid positions are empty or not.
            // true (=1) means the grid position is occupied, false (=0) means the grid position is empty
            BitSet bitSet = new BitSet();

            int currentHeigth = 1;
            for (Component com : parent.getComponents()) {
                if (!com.isVisible()) {
                    continue;
                }
                Dimension size = new Dimension((com.getPreferredSize().width + gridUnitSize.width - 1) / gridUnitSize.width, (com.getPreferredSize().height + gridUnitSize.height - 1) / gridUnitSize.height);

                // true if element was inserted or cannot be imported
                boolean inserted = false;
                for (int y = 0; y < currentHeigth; y++) {
                    for (int x = 0; x < columns; x++) {

                        // check if the grid width of the element is bigger than the number of columns
                        // if yes ignore the element
                        if (size.width > columns) {
                            int minWidth = Math.min(com.getPreferredSize().width, com.getMinimumSize().width);
                            int minWidthNew = (minWidth + gridUnitSize.width - 1) / gridUnitSize.width;
                            if (minWidthNew > columns) {
                                System.out.println("=== Element "
                                        + "('" + com.getClass().getSimpleName() + "', width: " + minWidthNew + ")"
                                        + " does not fit into the grid."
                                        + " Too few columns (" + columns + "). Element will be ignored");
                                Footer.forceWarning(Loc.get("A_LEAST_ONE_INPUT_FIELD_WAS_HIDDEN_BECAUSE_OF_TO_FEW_SPACE"));
                                // hide the element
                                com.setBounds(0, 0, 0, 0);
                                inserted = true;
                                break;

                            } else {
                                size.width = minWidthNew;
                            }
                        }

                        // check if the element fits into the current position.
                        if (x + size.width > columns) {
                            break;
                        }

                        // check if the top left corner is empty
                        if (!bitSet.get(columns * y + x)) {
                            // check if there is enough space to insert the grid width and height
                            // set allFree to false if one of the grid positions is not empty
                            boolean allFree = true;
                            for (int wx = 0; wx < size.width; wx++) {
                                for (int wy = 0; wy < size.height; wy++) {
                                    if (bitSet.get(columns * (y + wy) + x + wx)) {
                                        allFree = false;
                                        break;
                                    }

                                }
                                if (!allFree) {
                                    break;
                                }
                            }
                            if (!allFree) {
                                break;
                            }

                            // insert the element
                            com.setBounds(x * gridUnitSize.width + insets.left,
                                    y * gridUnitSize.height + insets.top,
                                    size.width * gridUnitSize.width,
                                    size.height * gridUnitSize.height);

                            // set all used grid positions to true
                            for (int wy = 0; wy < size.height; wy++) {
                                bitSet.set(columns * (y + wy) + x, columns * (y + wy) + x + size.width, true);
                            }
                            if (y + size.height > currentHeigth) {
                                if (size.height > 1000) {
                                    System.out.println(size.height);
                                }
                                currentHeigth = y + size.height;
                            }

                            inserted = true;
                            break;
                        }
                    }
                    if (inserted) {
                        break;
                    }
                    if (y == currentHeigth - 1) {
                        currentHeigth++;
                    }
                }
            }

            layoutSize = new Dimension(gridUnitSize.width * columns + insets.left + insets.right, gridUnitSize.height * (currentHeigth) + insets.top + insets.bottom);

            if (parent.getHeight() != (layoutSize.getHeight())) {
                parent.setSize(layoutSize);
            }

        }
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {
    }

    @Override
    public void removeLayoutComponent(Component comp) {
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        if (layoutSize == null) {
            // necessary to avoid errors when display the element the first time.
            return new Dimension(999999999, 999999999);
        }
        layoutContainer(parent);

        return layoutSize;
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        return layoutSize;
    }
}
