package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiYearElement;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.*;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.swing.border.EmptyBorder;

public class MultiYearChooser extends AbstractMultiComponent<MultiYearElement> {

    private YearChooser yearChooser;

    private JTextField t1;
    private JTextField t2;

    /**
     * Constructor of the DynamicComboBoxList class.
     *
     * @param name
     */
    public MultiYearChooser(String name) {
        super(name);
        init();

        setSelectAllAndRemoveAllButtonVisible(false);
        topWrapper.add(BorderLayout.WEST, getInputComponent());
        buttonRemove.setText(Loc.get("REMOVE_SELECTION"));
    }

    @Override
    protected JComponent getInputComponent() {
        if (yearChooser == null) {
            yearChooser = new YearChooser();
        }
        return yearChooser;
    }

    @Override
    public void addInputToList() {
        addItemToList(getElementFromInput());
    }

    @Override
    public boolean addItemToList(MultiYearElement item) {

        if (item != null) {

            if (!selectedItemsOnList.contains(item)) {
                for (MultiYearElement multiYearElement : selectedItemsOnList) {
                    if (multiYearElement.intersects(item)) {
                        return false;
                    }
                }
                selectedItemsOnList.add(item);
                listModel.addElement(item);
                yearChooser.clear();
                return true;
            }
        }
        yearChooser.clear();
        return false;
    }

    @Override
    public boolean addItemsToList(ArrayList<MultiYearElement> items) {
        boolean allSuccessful = true;
        for (MultiYearElement s : items) {
            allSuccessful = allSuccessful && addItemToList(s);
        }
        return allSuccessful;
    }

    @Override
    public void removeSelectedItemsFromList() {
        int[] selected = listSelectedItems.getSelectedIndices();
        for (int i = selected.length - 1; i >= 0; i--) {
            selectedItemsOnList.remove(selected[i]);
            listModel.removeElementAt(selected[i]);
        }
    }

    @Override
    public void removeAllItemsFromList() {
        for (int i = listModel.getSize() - 1; i >= 0; i--) {
            selectedItemsOnList.remove(i);
            listModel.removeElementAt(i);
        }
    }

    public void removeItem(MultiYearElement text) {
        for (int i = 0; i < listModel.getSize(); i++) {
            if (text.equals(selectedItemsOnList.get(i))) {
                selectedItemsOnList.remove(i);
                listModel.removeElementAt(i);
                return;
            }
        }
    }

    public void removeItems(List<MultiYearElement> text) {
        for (MultiYearElement s : text) {
            removeItem(s);
        }
    }

    @Override
    public void addAllItems() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ArrayList<MultiYearElement> getListItems(boolean addComboItemToo) {
        ArrayList<MultiYearElement> list = new ArrayList<>();
        for (int i = 0; i < listModel.size(); i++) {
            list.add(listModel.getElementAt(i));
        }
        if (addComboItemToo) {
            addInputToList();
        }
        return list;
    }

    @Override
    public void clearInput() {
        yearChooser.clear();
        removeAllItemsFromList();
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        yearChooser.setVisible(b);
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        yearChooser.setEnabled(b);
    }

    @Override
    public void addFocusListener(FocusListener fl) {
        super.addFocusListener(fl);
        yearChooser.addFocusListener(fl);
    }

    @Override
    protected Comparator<MultiYearElement> getComparator() {
        return new Comparator<MultiYearElement>() {

            @Override
            public int compare(MultiYearElement o1, MultiYearElement o2) {
                if (o1 == null) {
                    return -1;
                }
                if (o2 == null) {
                    return 1;
                }
                if (o1.equals(o2)) {
                    return 0;
                }
                return o1.getBegin().compareTo(o2.getBegin());
            }
        };
    }

    @Override
    public MultiYearElement getElementFromInput() {
        String year1 = t1.getText();
        String year2 = t2.getText();

        // if two values were entered
        if (StringHelper.isInteger(year1) && StringHelper.isInteger(year2)) {
            return new MultiYearElement(StringHelper.parseInteger(year1), StringHelper.parseInteger(year2));
        }
        // if only the first value was entered
        if (StringHelper.isInteger(year1) && year2.isEmpty()) {
            return new MultiYearElement(StringHelper.parseInteger(year1), StringHelper.parseInteger(year1));
        }
        // if only the second value was entered
        if (year1.isEmpty() && StringHelper.isInteger(year2)) {
            return new MultiYearElement(StringHelper.parseInteger(year2), StringHelper.parseInteger(year2));
        }

        return null;
    }

    private class YearChooser extends JPanel {

        public YearChooser() {
            super(new FlowLayout());
            setBorder(new EmptyBorder(0, 0, 0, 0));

            add(new JLabel(" " + Loc.get("START") + ": "));

            t1 = new JTextField();
            t1.setPreferredSize(new Dimension(60, 24));
            t1.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    if (!StringHelper.isInteger(t1.getText())) {
                        t1.setText("");
                    }
                }
            });
            add(t1);

            add(new JLabel(" " + Loc.get("END") + ": "));

            t2 = new JTextField();
            t2.setPreferredSize(new Dimension(60, 24));
            t2.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    if (!StringHelper.isInteger(t2.getText())) {
                        t2.setText("");
                    }
                }
            });
            add(t2);

            add(new JLabel(" "));

            // rearrangement and invisibility for some elements
            XButton plusButton = new XButton(" + ");
            plusButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addInputToList();
                }
            });
            add(plusButton);
        }

        public void clear() {
            t1.setText("");
            t2.setText("");
        }

    }

}
