package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiSelection;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * An input field that is used to upload files to the server of xBook. This field allows to define a specfici "category"
 * from a list of available options that defines the categoryId of the folder structure.
 * <p>
 * Therefore there are three states of the field.
 * <p>
 * State 1 means that a file has been chosen, but the data set has not been save. In this case, the field displays the
 * selected categoryId and file name. This is always the state if a file has been selected by the file chooser.
 * <p>
 * State 2 means that a file is saved in the temporary file folder of xbook. In this case, the field displays the file
 * name (the categoryId is not necessary, because it is the general file folder). This is the state after the entry has
 * been saved and as long as the file has not been synchronized.
 * <p>
 * State 3 means that a file has been synced and is saved on the server. The saving in the temporary file is not
 * necessary any more. In this case, the field displays the unique file name that is saved on the server. This is always
 * the state after the entry has been synced and has not been changed since the synchronization.
 * <p>
 * As soon as another file has been selected from the file chooser or has inserted manually in the textfield, the field
 * is always reverted to state 1.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class RawFileUploadWithMultiCategorySelectionField extends RawFileUploadWithCategoryField {
    private Log LOGGER = LogFactory.getLog(RawFileUploadWithMultiCategorySelectionField.class);

    private JPanel optionsPanel;
    private JLabel countLabel;
    private MultiSelection categorySelection;
    private CodeTableHashMap categoryData;
    private String tableCategory;
    boolean statusFileMustBeSynced = false;
    private String fileExtension;

    /**
     * Constructor.
     *
     * @param columnTypeFileName         The corresponding column type for file name.
     * @param columnTypeExtension        The corresponding column type for file extension.
     * @param columnTypeFileMustBeSynced The corresponding column type for the information whether the file has to be
     *                                   synchronized or not.
     * @param columnTypeCategory         The corresponding column type for category information.
     * @param displayedCategoryIds       The IDs of the categories that should be selectable in the category category
     *                                   selection.
     * @param fileFilter                 The file filter options.
     */
    public RawFileUploadWithMultiCategorySelectionField(ApiControllerAccess controller, ColumnType columnTypeFileName,
                                                        ColumnType columnTypeExtension, ColumnType columnTypeFileMustBeSynced,
                                                        ColumnType columnTypeCategory,
                                                        String tableCategory, int[] displayedCategoryIds,
                                                        ArrayList<FileNameExtensionFilter> fileFilter) {
        super(controller, columnTypeFileName, columnTypeExtension, columnTypeFileMustBeSynced, columnTypeCategory,
                displayedCategoryIds, fileFilter);
        this.tableCategory = tableCategory;

        updateCategories();
    }

    @Override
    public void createField() {
        super.createField(true);

        // update the label status information
        labelStatus.setPreferredSize(new Dimension(100000, 32));

        // combine everything in the panel
        generalWrapper.add(BorderLayout.NORTH, textFieldWrapper);

        categorySelection = new MultiSelection();
        categorySelection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateCount();
            }
        });
        categorySelection.setPreferredSize(new Dimension(Sizes.INPUT_GRID_SIZE.width, 32));
        add(BorderLayout.WEST, ComponentHelper.wrapComponent(categorySelection, 0, 16, 0, 0));

        optionsPanel = new JPanel(new BorderLayout());
        countLabel = new JLabel("–");
        optionsPanel.add(BorderLayout.WEST, ComponentHelper.wrapComponent(countLabel, 49, 0, 4, 0));
        optionsPanel.add(BorderLayout.EAST, eastButtonWrapper);
        generalWrapper.add(BorderLayout.SOUTH, optionsPanel);

        currentStatus = Status.NOT_SET;
        updateToStatus(currentStatus);
        updateCount();
    }

    /**
     * Updates the count label with the new value.
     */
    public void updateCount() {
        int count = categorySelection.getSelectedData().size();
        if (count == 0) {
            countLabel.setText("–");
        } else {
            countLabel.setText("<html><b>(" + count + ")</b></html>");
        }
    }

    @Override
    protected void updateToStatus(Status newStatus) {
        // update the panels, if necessary
        boolean statusHasChanged = newStatus != currentStatus;
        if (statusHasChanged) {
            // elements must not be restructured if there is a change from Status.NOT_SET to
            // Status.NOT_SAVED or counterwise
            boolean mustDisplayTextField = (newStatus == Status.NOT_SET && currentStatus == Status.NOT_SAVED) ||
                    (newStatus == Status.NOT_SAVED && currentStatus == Status.NOT_SET);
            if (mustDisplayTextField) {
                // only update the text field wrapper if is really have to be reloaded.
                // if the textfield is removed from the panel and set again, there occure problems with component focus
                labelWrapper.removeAll();
                labelWrapper.add(BorderLayout.NORTH, textFieldWrapper);
            } else if (newStatus == Status.SAVED || newStatus == Status.SYNCED) {
                labelWrapper.removeAll();
                labelWrapper.add(BorderLayout.NORTH, labelStatus);
            }
        }

        // updates the visibility of the elements
        updateComponentVisibility(newStatus);
        // finally update the variable with the status
        currentStatus = newStatus;
    }

    /**
     * Sets a new set of data to the combo box.
     *
     * @param newData The data to set.
     */
    public void setCategoryData(CodeTableHashMap newData) {
        this.categoryData = newData;
        updateCategories();
    }

    /**
     * Updates the available Categories.
     */
    private void updateCategories() {
        if (categoryData == null) {
            return;
        }
        ArrayList<String> allValues = new ArrayList<>();
        for (Map.Entry<String, String> hash : categoryData.entrySet()) {
            if (containsKey(hash.getKey())) {
                allValues.add(hash.getValue());
            }
        }
        Collections.sort(allValues, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.toUpperCase().compareTo(o2.toUpperCase());
            }
        });
        categorySelection.setAvailableData(allValues);
    }

    /**
     * Select a row with a specific value selected or deselected.
     *
     * @param value    The value of the row that should be selected or deselected.
     * @param selected The selected status that the row should be set to.
     */
    public void setSelected(String value, boolean selected) {
        categorySelection.setSelected(value, selected);
    }

    /**
     * Select a row with a specific value enabled or disabled.
     *
     * @param value   The value of the row that should be selected or deselected.
     * @param enabled The enabled status that the row should be set to.
     */
    public void setEnabled(String value, boolean enabled) {
        categorySelection.setEnabled(value, enabled);
    }

    @Override
    public void load(DataSetOld data) {
        // category
        DataTableOld dataTable = data.getOrCreateDataTable(tableCategory);
        ArrayList<String> catData = new ArrayList<>();
        for (DataRow list : dataTable) {
            final String s = list.get(columnTypeCategory);
            if (s != null) {
                catData.add(categoryData.get(s));
            }
        }
        categorySelection.setAvailableData(catData);

        // files
        DataRow list = data.getDataRowForTable(getTableName());
        textField.setText(list.get(columnType));
        fileExtension = list.get(columnTypeExtension);
        statusFileMustBeSynced = false;
    }

    @Override
    public void clear() {
        // category
        categorySelection.clearSelection();
        textField.setText("");
        fileExtension = "";
        statusFileMustBeSynced = false;
    }

    @Override
    public void save(DataSetOld data) {
        // category
        DataTableOld list = data.getOrCreateDataTable(tableCategory);
        for (String s : getSelectedCategoryIds()) {
            if (s != null) {
                DataRow row = new DataRow(tableCategory);
                row.add(new DataColumn(s, columnTypeCategory));
                list.add(row);
            }
        }

        // files
        DataRow dataRow = data.getDataRow(getTableName());
        dataRow.put(columnType, getText());
        dataRow.put(columnTypeExtension, fileExtension);
        dataRow.put(columnTypeFileMustBeSynced, statusFileMustBeSynced ? "1" : "0");
    }

    private String getFileExtensionFromFileName(String file) {
        String[] splitted = file.split("\\.");
        return splitted[splitted.length - 1];
    }

    public List<String> getSelectedCategoryIds() {
        return categoryData.convertStringToId(categorySelection.getSelectedData());
    }

    @Override
    protected void onFileChoosen(File lastLoadedDirectory, File lastLoadedFile) throws FileTooLargeException {
        super.onFileChoosen(lastLoadedDirectory, lastLoadedFile);
        fileExtension = getFileExtensionFromFileName(getText());
        LOGGER.debug("TEMP ONLY! change text!");
        String newFileName = null;
        try {
            newFileName = ((AbstractController) controller).copyFileToLocalDirectory(lastLoadedFile.toPath(),fileExtension);
        }  catch (IOException e) {
            e.printStackTrace();
        }
        LOGGER.debug("new FIle Name: " + newFileName);
        statusFileMustBeSynced = true;
    }

    @Override
    public boolean isValidInput() {
        if (textField.getText().isEmpty()) {
            return false;
        }
        if (fileExtension.isEmpty()) {
            return false;
        }
        return true;
    }
}
