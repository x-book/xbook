package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.exception.TooManyDigitsException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.FloatConstraints;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.MathsHelper;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;

/**
 * The raw float field.
 *
 * @param <C> The corresponding input constraints for the raw input field.
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITRawFloatField<C extends FloatConstraints> extends AbstractITRaw<C> {

    private JTextField textField;

    public ITRawFloatField(C constraints) {
        super(constraints);
    }

    private Integer digitsAfterDecimalPoint = 7;

    @Override
    protected JComponent create() {
        JPanel wrapper = new JPanel(new BorderLayout());
        textField = new JTextField();
        textField.setBorder(new EmptyBorder(0, 5, 0, 5));
        wrapper.add(BorderLayout.CENTER, textField);
        return wrapper;
    }

    @Override
    public String getValue() {
        if (textField.getText().isEmpty()) {
            return "-1";
        }
        return textField.getText().replace(",", ".");
    }

    @Override
    public boolean isEmpty() {
        return textField.getText().isEmpty();
    }

    @Override
    public void setValue(String value) {
        if (value.startsWith("-1") || "0".equals(value)) {
            textField.setText("");
        } else {
            try {
                value = "" + Float.parseFloat(value.replace(",", "."));
            } catch (NumberFormatException ex) {
                value = "";
            }
            textField.setText(value);
        }
    }

    @Override
    public void clear() {
        textField.setText("");
    }

    @Override
    public boolean isValidInput() {
        String input = textField.getText();
        if (input.isEmpty()) {
            return false;
        }
        try {
            MathsHelper.roundFloat(input.replace(",", "."), digitsAfterDecimalPoint);
        } catch (NumberFormatException | TooManyDigitsException e) {
            return false;
        }
        return true;
    }

    @Override
    public void actionOnFocusGain() {
        textField.setBackground(Colors.SIDEBAR_BACKGROUND);
    }

    @Override
    public void actionOnFocusLost() {
        super.actionOnFocusLost();
        textField.setBackground(Color.WHITE);

        if (!(textField.getText().isEmpty())) {
            try {
                String input = textField.getText();
                Float value = MathsHelper.roundFloat(input.replace(",", "."), digitsAfterDecimalPoint);
                textField.setText(value + "");
            } catch (NumberFormatException e) {
                textField.setText("");
                Footer.displayError(Loc.get("NO_VALID_INPUT_ONLY_FLOAT_VALUES"));
                textField.setBackground(Color.RED);
            } catch (TooManyDigitsException r) {
                textField.setText("");
                Footer.displayError(Loc.get("MORE_THAN_7_DIGITS_ARE_NOT_SUPPORTED"));
                textField.setBackground(Color.RED);
            }
        }
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textField);
        return comp;
    }

    @Override
    public String getStringRepresentation() {
        return getValue();
    }

    /**
     * Returns the textfield object.
     *
     * @return The textfield object.
     */
    public JTextField getTextField() {
        return textField;
    }


    @Override
    public void addSidebarFunktionality() {
        textField.addFocusListener(getSidebarFunctionality());
    }
}
