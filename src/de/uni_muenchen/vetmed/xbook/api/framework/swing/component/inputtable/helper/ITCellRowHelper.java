package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.helper;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;

/**
 * The ITCellRowHelper combines a raw input field with the corresponding ColumnType.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITCellRowHelper {

	/**
	 * The corresponding ColumnType of the input field.
	 */
	private final ColumnType columnType;

	/**
	 * The raw input field object.
	 */
	private final AbstractITRaw raw;

	/**
	 * Constructor.
	 *
	 * @param columnType The corresponding ColumnType of the input field.
	 * @param raw The raw input field.
	 */
	public ITCellRowHelper(ColumnType columnType, AbstractITRaw raw) {
		this.columnType = columnType;
		this.raw = raw;
	}

	/**
	 * Returns the corresponding ColumnType of the input field.
	 *
	 * @return The corresponding ColumnType of the input field.
	 */
	public ColumnType getColumnType() {
		return columnType;
	}

	/**
	 * Returns the raw input field.
	 *
	 * @return The raw input field.
	 */
	public AbstractITRaw getRaw() {
		return raw;
	}
}
