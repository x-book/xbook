package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.ITRawLabel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntryRoot;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.dependable.AbstractBaseDependableEntry;

import javax.swing.*;

/**
 * @author Daniel Kaltenthaler
 */
public class EmptyRowConstraints extends LabelConstraints {

    /**
     * Constructor.
     * <p>
     * Initialize the constraints.
     *
     * @param entry The corresponding Entry object of the input field.
     */
    public EmptyRowConstraints(AbstractEntryRoot entry) {
        super(entry, 1, 1, 0, "", SwingConstants.CENTER);
    }

    @Override
    public AbstractITRaw createInput() {
        ITRawLabel label = new ITRawLabel(this);
        label.setBackground(new JLabel().getBackground());
        return label;
    }

}
