package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Extends the DynamicComboBox with a list that allows adding several elements
 * out of the combo box.
 *
 * @param <T>
 * @author Daniel Kaltenthaler
 * @version 1.0.0
 */
public abstract class AbstractMultiComponent<T> extends JPanel {

    /**
     * A list model holding the data of the selected elements.
     */
    protected SortableListModel<T> listModel;
    /**
     * The list object to display the selected animals.
     */
    protected JList<T> listSelectedItems;
    /**
     * The button object to remove an item from the list.
     */
    protected XButton buttonRemove;
    /**
     * The button object to add an item to the list.
     */
    protected XButton buttonAdd;
    /**
     * A JPanel object that is wrapped around the remove button.
     */
    protected JPanel buttonRemoveWrapper;
    /**
     * A JPanel object that is wrapped around the add button.
     */
    protected JPanel buttonAddWrapper;
    /**
     * The button object to add all items to the list.
     */
    protected XButton buttonSelectAll;
    /**
     * The button object to remove all items from the list.
     */
    protected XButton buttonRemoveAll;
    /**
     * Holds the selected items of the list.
     */
    protected ArrayList<T> selectedItemsOnList;
    /**
     * The scroll pane object that is used for scrolling in the list.
     */
    public JScrollPane listScroller;
    /**
     * A panel that wraps the content to allow adding more elements around the
     * content.
     */
    protected JPanel listSpecificWrapper;
    /**
     * A wrapper panel that holds the combo box an the buttons to add or remove
     * elements.
     */
    protected JPanel topWrapper;
    /**
     * A wrapper panel that holds the list.
     */
    protected JPanel centerWrapper;
    /**
     * A wrapper panel that holds the "select all" and "remove all" buttons.
     */
    protected JPanel selectButtonWrapper;
    /**
     * A description of the input field.
     */
    protected final String name;

    public AbstractMultiComponent(String name) {
        this.name = name;
    }

    protected abstract JComponent getInputComponent();

    protected JComponent getInputTextComponent() {
        return getInputComponent();
    }

    public T getSelectedItemInList(){
        
        return listSelectedItems.getSelectedValue();
    }
    /**
     * Creates and initialises all elements of the object.
     */
    public void init() {
        setLayout(new BorderLayout());
        selectedItemsOnList = new ArrayList<>();

        listSpecificWrapper = new JPanel(new BorderLayout());

        // the input field (the textfield)
        topWrapper = new JPanel(new BorderLayout());
        topWrapper.add(BorderLayout.NORTH, getInputComponent());

        // The "˄˄" button
        buttonRemove = new XButton("˄˄");
        buttonRemove.setStyle(XButton.Style.DEFAULT);
        buttonRemoveWrapper = new JPanel(new BorderLayout());
        buttonRemove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeSelectedItemsFromList();
            }
        });
        buttonRemoveWrapper.add(BorderLayout.CENTER, ComponentHelper.wrapComponent(buttonRemove, Colors.CONTENT_BACKGROUND, 2, 0, 2, 0));
        topWrapper.add(BorderLayout.EAST, buttonRemoveWrapper);

        // The "˅˅" button
        buttonAdd = new XButton("˅˅");
        buttonAdd.setStyle(XButton.Style.DEFAULT);
        buttonAddWrapper = new JPanel(new BorderLayout());
        buttonAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addInputToList();
            }
        });
        buttonAddWrapper.add(BorderLayout.CENTER, ComponentHelper.wrapComponent(buttonAdd, Colors.CONTENT_BACKGROUND, 2, 0, 2, 0));
        topWrapper.add(BorderLayout.WEST, buttonAddWrapper);

        listSpecificWrapper.add(BorderLayout.NORTH, topWrapper);

        // the list holding the selected elements
        centerWrapper = new JPanel();
        listModel = new SortableListModel(getComparator());
        listSelectedItems = new JList(listModel);
        listSelectedItems.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        listSelectedItems.setLayoutOrientation(JList.VERTICAL);
        listSelectedItems.setBackground(new Color(240, 240, 240));
        listScroller = new JScrollPane(listSelectedItems);
        listScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        listScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        listScroller.setBorder(new LineBorder(Color.LIGHT_GRAY));
        listScroller.removeMouseWheelListener(listScroller.getMouseWheelListeners()[0]);
        listSpecificWrapper.add(BorderLayout.CENTER, listScroller);

        // "Select all" and "Remove all" buttons
        selectButtonWrapper = new JPanel();
        // The "Select all" Button
        buttonSelectAll = new XButton("Select All");
        buttonSelectAll.setStyle(XButton.Style.DEFAULT);
        buttonSelectAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addAllItems();
            }
        });
        selectButtonWrapper.add(BorderLayout.WEST, ComponentHelper.wrapComponent(buttonSelectAll, Colors.CONTENT_BACKGROUND, 2, 0, 0, 0));

        // The "Remove all" Button
        buttonRemoveAll = new XButton("Remove All");
        buttonRemoveAll.setStyle(XButton.Style.DEFAULT);
        buttonRemoveAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeAllItems();
            }
        });
        selectButtonWrapper.add(BorderLayout.EAST, ComponentHelper.wrapComponent(buttonRemoveAll, Colors.CONTENT_BACKGROUND, 2, 0, 0, 0));

        listSpecificWrapper.add(BorderLayout.SOUTH, selectButtonWrapper);

        add(BorderLayout.CENTER, listSpecificWrapper);
        getInputTextComponent().addFocusListener(new FocusAdapter() {

            @Override
            public void focusLost(FocusEvent e) {
                addInputToList();
            }

        });
    }

    /**
     * Adds the current input of the textfield into the list. Don't allow adding
     * items that are already in the list
     */
    public abstract void addInputToList();

    /**
     * Adds a new item to the list. Don't allow adding items that are already in
     * the list.
     *
     * @param items The item to add.
     * @return <code>true</code> if the item was added successfully,
     * <code>false</code> if the item was not added because it is already in the
     * list.
     */
    public abstract boolean addItemToList(T item);

    /**
     * Adds several new items to the list. Don't allow adding items that are
     * already in the list.
     *
     * @param items The items to add.
     * @return <code>true</code> if all item were added successfully,
     * <code>false</code> if at least one item was not added because it is
     * already in the list.
     */
    public boolean addItemsToList(List<T> items) {
        boolean allAdded = true;
        for (T item : items) {
            allAdded &= addItemToList(item);
        }
        return allAdded;
    }

    /**
     * Adds more items to the list. Don't allow adding items that are already in
     * the list.
     *
     * @param items
     * @return <code>true</code> if the item was added successfully,
     * <code>false</code> if the item was not added because it is already in the
     * list.
     */
    public abstract boolean addItemsToList(ArrayList<T> items);

    /**
     * Removes all items from the list.
     */
    public abstract void removeAllItemsFromList();

    /**
     * Removes the selected items from the list.
     */
    public abstract void removeSelectedItemsFromList();

    /**
     * Removes the items with a specific index from the list.
     *
     * @param index The index to remove.
     */
    public void removeItem(int index) {
        selectedItemsOnList.remove(index);
        listModel.removeElementAt(index);
    }

    /**
     * Return the list that displays the selected items.
     *
     * @return The list that displays the selected items.
     */
    public JList getList() {
        return listSelectedItems;
    }

    public SortableListModel<T> getListModel() {
        return listModel;
    }

    /**
     * Removes all items from the list.
     */
    public void removeAllItems() {
        ArrayList<Integer> items = new ArrayList<>();
        for (int i = 0; i < selectedItemsOnList.size(); i++) {
            items.add(i);
        }
        listSelectedItems.setSelectedIndices(convertIntegers(items));
        removeSelectedItemsFromList();
    }

    private int[] convertIntegers(ArrayList<Integer> integers) {
        int[] ret = new int[integers.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = integers.get(i);
        }
        return ret;
    }

    /**
     * Removes all items from the list.
     */
    public abstract void addAllItems();

    /**
     * Sets a new size of the button to add an item to the list and of the
     * button to remove an item from the list.
     *
     * @param d The new size of the buttons.
     */
    public void setButtonSize(Dimension d) {
        buttonAdd.setSize(d);
        buttonAdd.setPreferredSize(d);
        buttonRemove.setSize(d);
        buttonRemove.setPreferredSize(d);
    }

    /**
     * Sets a new size of the button to add an item to the list and of the
     * button to remove an item from the list.
     *
     * @param width The new width of the buttons.
     * @param height The new height of the buttons.
     */
    public void setButtonSize(int width, int height) {
        setButtonSize(new Dimension(width, height));
    }

    /**
     * Set a padding around the "add" and "remove" buttons.
     *
     * @param top The size of the padding on top.
     * @param bottom The size of the padding on the bottom.
     */
    public void setButtonPadding(int top, int bottom) {
        setButtonAddPadding(top, 0, bottom, 0);
        setButtonRemovePadding(top, 0, bottom, 0);
    }

    /**
     * Set a padding around the "add" buttons.
     *
     * @param top The size of the padding on top.
     * @param right The size of the padding on the right.
     * @param bottom The size of the padding on the bottom.
     * @param left The size of the padding on the left.
     */
    public void setButtonAddPadding(int top, int right, int bottom, int left) {
        buttonAddWrapper.setBorder(new EmptyBorder(top, left, bottom, right));
    }

    /**
     * Set a padding around the "remove" buttons.
     *
     * @param top The size of the padding on top.
     * @param right The size of the padding on the right.
     * @param bottom The size of the padding on the bottom.
     * @param left The size of the padding on the left.
     */
    public void setButtonRemovePadding(int top, int right, int bottom, int left) {
        buttonRemoveWrapper.setBorder(new EmptyBorder(top, left, bottom, right));
    }

    /*
     * Returns the current list items (without the textfield value).
     *
     * @returns
     *		The current list items (without the textfield value).
     */
    public ArrayList<T> getListItems() {
        return getListItems(false);
    }

    /**
     * Returns the current list items.
     * <p/>
     * The parameter decides if the current value in the textfield will also be
     * added to the return value or not.
     *
     * @param addComboItemToo <code>true</code> if the value of the textfield
     * should be added to the return value, <code>false</code> else.
     * @return
     */
    public abstract ArrayList<T> getListItems(boolean addComboItemToo);

    /**
     * Add a specific content the the left side of the content.
     *
     * @param comp The component to add.
     */
    public void addContentToEast(JComponent comp) {
        add(BorderLayout.EAST, comp);
    }

    /**
     * Add a specific content the the right side of the content.
     *
     * @param comp The component to add.
     */
    public void addContentToWest(JComponent comp) {
        add(BorderLayout.WEST, comp);
    }

    /**
     * Adds an ActionListener to the remove and add buttons.
     *
     * @param al The new Action Listener.
     */
    public void addActionListenerToButtons(ActionListener al) {
        buttonAdd.addActionListener(al);
        buttonRemove.addActionListener(al);
    }

    /**
     * Adds an ActionListener to the remove button.
     *
     * @param al The new Action Listener.
     */
    public void addActionListenerToRemoveButton(ActionListener al) {
        buttonRemove.addActionListener(al);
    }

    /**
     * Adds an ActionListener to the add button.
     *
     * @param al The new Action Listener.
     */
    public void addActionListenerToAddButton(ActionListener al) {
        buttonAdd.addActionListener(al);
    }

    /**
     * Adds an ActionListener to the remove all button.
     *
     * @param al The new Action Listener.
     */
    public void addActionListenerToRemoveAllButton(ActionListener al) {
        buttonRemoveAll.addActionListener(al);
    }

    /**
     * Adds an ActionListener to the select all button.
     *
     * @param al The new Action Listener.
     */
    public void addActionListenerToSelectAllButton(ActionListener al) {
        buttonSelectAll.addActionListener(al);
    }

    /**
     * Removes the text out of the input text field.
     */
    public abstract void clearInput();

    /**
     * Set all elements concerning the list visible or not.
     * <p/>
     * Adding items to and removing items from the list will not work if the
     * list is invisible.
     *
     * @param b <code>true</code> if the list should be visible,
     * <code>false</code> else.
     */
    public void setListVisible(boolean b) {
        listScroller.setVisible(b);
        buttonRemove.setVisible(b);
        buttonRemoveAll.setVisible(b);
        buttonAdd.setVisible(b);
        buttonSelectAll.setVisible(b);
    }

    @Override
    public void setVisible(boolean b) {
        setListVisible(b);
    }

    @Override
    public void setEnabled(boolean b) {
        listScroller.setEnabled(b);
        buttonRemove.setEnabled(b);
        buttonRemoveAll.setEnabled(b);
        buttonAdd.setEnabled(b);
        buttonSelectAll.setEnabled(b);
    }

    @Override
    public void addFocusListener(FocusListener fl) {
        listScroller.addFocusListener(fl);
        buttonRemove.addFocusListener(fl);
        buttonRemoveAll.addFocusListener(fl);
        buttonAdd.addFocusListener(fl);
        buttonSelectAll.addFocusListener(fl);
    }

    /**
     * Set a new action listener to the "add all" button.
     *
     * @param al The new action listener.
     */
    public void setActionListenerToAddAllButton(ActionListener al) {
        for (ActionListener ls : buttonSelectAll.getActionListeners()) {
            buttonSelectAll.removeActionListener(ls);
        }
        buttonSelectAll.addActionListener(al);
    }

    /**
     * Returns whether the list is empty or not.
     *
     * @return <code>true</code> if the list is empty, <code>false</code> else.
     */
    public boolean isEmpty() {
        return selectedItemsOnList.isEmpty();
    }

    /**
     * Set a new text label to the "select all" button.
     *
     * @param label The text to display on the "select all" button.
     */
    public void setSelectAllButtonLabel(String label) {
        buttonSelectAll.setText(label);
    }

    /**
     * Set a new text label to the "remove all" button.
     *
     * @param label The text to display on the "remove all" button.
     */
    public void setRemoveAllButtonLabel(String label) {
        buttonRemoveAll.setText(label);
    }

    /**
     * Show or hide the all/remove all buttons.
     *
     * @param bool The status whether to show the buttons or not.
     */
    public void setSelectAllAndRemoveAllButtonVisible(boolean bool) {
        selectButtonWrapper.setVisible(bool);
    }

    /**
     * Return the add button object.
     *
     * @return The add button object.
     */
    public XButton getButtonAdd() {
        return buttonAdd;
    }

    /**
     * Return the remove button object.
     *
     * @return The remove button object.
     */
    public XButton getButtonRemove() {
        return buttonRemove;
    }

    /**
     * Return the select all button object.
     *
     * @return The select all button object.
     */
    public XButton getButtonSelectAll() {
        return buttonSelectAll;
    }

    /**
     * Return the remove all button object.
     *
     * @return The remove all button object.
     */
    public XButton getButtonRemoveAll() {
        return buttonRemoveAll;
    }

    /**
     * Return the scrollpane that is wrapped around the list holding the
     * selected items.
     *
     * @return The scrollpane around the list.
     */
    public JScrollPane getListScroller() {
        return listScroller;
    }

    @Override
    public void setBackground(Color bg) {
        ComponentHelper.colorAllChildren(this, bg);
    }

    public XButton getRemoveAllButton() {
        return buttonRemoveAll;
    }

    public XButton getSelectAllButton() {
        return buttonSelectAll;
    }

    protected abstract Comparator<T> getComparator();

    public ArrayList<Component> getFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(getButtonAdd().getButton());
        comp.add(getButtonRemove().getButton());
        comp.add(getButtonSelectAll().getButton());
        comp.add(getButtonRemoveAll().getButton());
        comp.add(getList());
        return comp;
    }
    
    public abstract T getElementFromInput();

    public static class SortableListModel<T> extends AbstractListModel {

        private final List<T> model = new ArrayList<>();
        private boolean isSorted = true;
        private final Comparator<T> comp;

        public SortableListModel(Comparator<T> comp) {
            this.comp = comp;
        }

        public void sort() {
            if (!isSorted) {
                Collections.sort(model, comp);
                fireContentsChanged(this, 0, model.size() - 1);
            }
        }

        public void addElement(T element) {
            addElement(element, true);
        }

        public void addElement(T element, int index) {
            model.add(index, element);
            fireIntervalAdded(this, index, index);
        }

        public void addElement(T element, boolean sort) {
            if (!sort) {
                addElement(element, model.size());
                isSorted = false;
            } else {
                if (!isSorted) {
                    sort();
                }
                int index = Collections.binarySearch(model, element, comp);
                if (index < 0) {
                    addElement(element, -index - 1);
                } else {
                    addElement(element, index);
                }
                isSorted = true;
            }
        }

        @Override
        public T getElementAt(int index) {
            return model.get(index);
        }

        @Override
        public int getSize() {
            return model.size();
        }

        public boolean contains(T element) {
            return model.contains(element);
        }

        public void removeElementAt(int index) {
            model.remove(index);
            fireIntervalRemoved(this, index, index);
        }

        public boolean removeElement(T obj) {
            int index = indexOf(obj);
            boolean rv = model.remove(obj);
            if (index >= 0) {
                fireIntervalRemoved(this, index, index);
            }
            return rv;
        }

        /**
         * Searches for the first occurrence of <code>elem</code>.
         *
         * @param elem an object
         * @return the index of the first occurrence of the argument in this
         * list; returns <code>-1</code> if the object is not found
         * @see Vector#indexOf(Object)
         */
        public int indexOf(T elem) {
            return model.indexOf(elem);
        }

        public int size() {
            return model.size();
        }
    }
}
