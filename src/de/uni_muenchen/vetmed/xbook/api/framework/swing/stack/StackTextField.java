package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.documentHelper.LimitInputLengthDocument;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.raw.RawTextField;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyListener;

/**
 * A stack element that holds a text field.
 *
 * @author Daniel Kaltenthaler
 * <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackTextField extends AbstractStackElement {

    /**
     * The textfield object.
     */
    private RawTextField raw;
    /**
     * The default style of the textfield.
     */
    private static Border defaultBorder = (new JTextField()).getBorder();

    /**
     * Constructor.
     *
     * @param columnType The correspondenting column type for this input field.
     * @param labelText The label of the stack element.
     * @param labelWidth The width of the label.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    public StackTextField(ColumnType columnType, String labelText, Integer labelWidth, SidebarPanel sidebar) {
        super(columnType, labelText, labelWidth, sidebar);
        raw.getTextField().addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                setDefaultStyle();
            }
        });
        if (columnType == null || columnType.getMaxInputLength() == null) {
            raw.getTextField().setDocument(new LimitInputLengthDocument(9999));
            System.out.println("Warning: No maximum input length defined in ColumnType of field '" + columnType.getDisplayName() + "'");
        } else {
            raw.getTextField().setDocument(new LimitInputLengthDocument(columnType.getMaxInputLength()));
        }
        addCustomSidebarFunctionality(raw.getTextField());
        setMandatory(columnType.isMandatory());
    }

    /**
     * Constructor.
     *
     * @param columnType The correspondenting column type for this input field.
     * @param labelText The label of the stack element.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    public StackTextField(ColumnType columnType, String labelText, SidebarPanel sidebar) {
        this(columnType, labelText, null, sidebar);
    }

    /**
     * Constructor.
     *
     * @param columnType The correspondenting column type for this input field.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    public StackTextField(ColumnType columnType, SidebarPanel sidebar) {
        this(columnType, columnType.getDisplayName(), null, sidebar);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param labelWidth The width of the label.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    public StackTextField(String labelText, int maxInputLength, Integer labelWidth, SidebarPanel sidebar) {
        super(labelText, labelWidth, sidebar);
        raw.getTextField().setDocument(new LimitInputLengthDocument(maxInputLength));
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param labelWidth The width of the label.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    public StackTextField(String labelText, Integer labelWidth, SidebarPanel sidebar) {
        this(labelText, 99999, labelWidth, sidebar);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     */
    public StackTextField(String labelText, int maxInputLength) {
        this(labelText, maxInputLength, null, (SidebarPanel) null);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     */
    public StackTextField(String labelText) {
        this(labelText, 99999, null, (SidebarPanel) null);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    public StackTextField(String labelText, int maxInputLength, SidebarPanel sidebar) {
        this(labelText, maxInputLength, null, sidebar);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param sidebar The custom sidebar for this input element.
     * <code>null</code> if no custom sidebar is available.
     */
    public StackTextField(String labelText, SidebarPanel sidebar) {
        this(labelText, 99999, null, sidebar);
    }

    /**
     * Constructor.
     *
     * @param labelText The label of the stack element.
     * @param sidebarText The text that will be displayed into the sidebar.
     */
    public StackTextField(String labelText, int maxInputLength, String... sidebarText) {
        this(labelText, maxInputLength, null, new SidebarPanel(sidebarText));
    }

    /**
     * Constructor.
     *
     * @param columnType The correspondenting column type for this input field.
     * @param labelText The label of the stack element.
     * @param sidebarText The text that will be displayed into the sidebar.
     */
    public StackTextField(ColumnType columnType, String labelText, String... sidebarText) {
        this(columnType, labelText, null, new SidebarPanel(sidebarText));
    }

    @Override
    public JComponent getElement() {
        if (raw == null) {
            raw = new RawTextField(columnType);
        }
        raw.getTextField().setFont(Fonts.FONT_STANDARD_PLAIN);
        return raw;
    }

    @Override
    public String getValue() throws IsAMandatoryFieldException {
        if (isMandatory() && raw.getText().isEmpty()) {
            throw new IsAMandatoryFieldException(columnType);
        }
        return raw.getText();
    }

    @Override
    public void clearInput() {
        raw.setText("");
    }

    @Override
    public void setMandatoryStyle() {
        if (raw != null) {
            raw.getTextField().setBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
            raw.getTextField().setFont(Fonts.FONT_STANDARD_BOLD);
        }
    }

    @Override
    public void setErrorStyle() {
        if (raw != null) {
            raw.getTextField().setBorder(BorderFactory.createLineBorder(Color.red, 2));
        }
    }

    @Override
    public void setDefaultStyle() {
        if (raw != null) {
            raw.getTextField().setBorder(defaultBorder);
        }
    }

    @Override
    public void setEnabled(boolean b) {
        if (raw != null) {
            raw.getTextField().setEnabled(b);
        }
    }

    @Override
    public void addKeyListener(KeyListener l) {
        raw.getTextField().addKeyListener(l);
    }

    @Override
    public void requestFocus() {
        raw.getTextField().requestFocusInWindow();
        raw.getTextField().setCaretPosition(raw.getTextField().getDocument().getLength());
    }

    @Override
    public void setValue(String value) {
        raw.getTextField().setText(value);
    }

}
