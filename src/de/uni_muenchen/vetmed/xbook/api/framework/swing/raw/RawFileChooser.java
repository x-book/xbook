package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.FileChooser;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class RawFileChooser extends AbstractInputElementFunctions {

    private ColumnType columnTypeFileName;
    private ColumnType columnTypeFile;
    private FileChooser fileChooser;
    private FileNameExtensionFilter fileFilter;

    public RawFileChooser(ColumnType columnTypeFile, ColumnType columnTypeFileName, FileNameExtensionFilter fileFilter) {
        super(columnTypeFile);
        this.columnTypeFile = columnTypeFile;
        this.columnTypeFileName = columnTypeFileName;
        this.fileFilter = fileFilter;
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());

        final String fileName = list.get(columnTypeFileName);
        fileChooser.setFileName(fileName);

        final String file = list.get(columnType);
        fileChooser.setFile(file);
    }

    @Override
    public void save(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        list.add(new DataColumn(getFileName(), columnTypeFileName.getColumnName()));

        if (getFileEncoded() != null) {
            list.add(new DataColumn(getFileEncoded(), columnType.getColumnName()));
        }
    }

    @Override
    public String getStringRepresentation() {
        return getFileName();
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        return fileChooser.getFocusableElements();
    }

    @Override
    public void createField() {
        setLayout(new BorderLayout());

        fileChooser = new FileChooser(fileFilter, columnTypeFile.getMaxInputLength());
        add(BorderLayout.CENTER, fileChooser);
    }

    @Override
    public boolean isValidInput() {
        return fileChooser.isValidInput();
    }

    @Override
    public void clear() {
        fileChooser.clear();
    }

    @Override
    public void setFocus() {
        fileChooser.getTextField();
    }



    public String getFileName() {
        return fileChooser.getFileName();
    }

    public void setFileName(String fileName) {
        fileChooser.setFileName(fileName);
    }

    public byte[] getFile() {
        return fileChooser.getFile();
    }

    public void setFile(String file) {
        fileChooser.setFile(file);
    }

    public String getFileEncoded() {
        return fileChooser.getFileEncoded();
    }

    public JTextField getTextField() {
        return fileChooser.getTextField();
    }

    public void setFileFilter(FileNameExtensionFilter fileFilter) {
        this.fileFilter = fileFilter;
    }

    public void setMaxFileSize(long maxFileSize) {
        fileChooser.setMaxFileSize(maxFileSize);
    }
}
