package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;
import java.awt.event.KeyListener;
import javax.swing.JComponent;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackTitle extends AbstractStackElement {

    private XTitle title;

    public StackTitle(String title) {
        super("", 1);
        wrapper.setLayout(new BorderLayout());
        wrapper.add(BorderLayout.CENTER, getElement());
        wrapper.setBorder(new EmptyBorder(0, 0, 0, 0));
        this.title.setText(title);
    }

    @Override
    public JComponent getElement() {
        JPanel elementWrapper = new JPanel(new BorderLayout());
        elementWrapper.setBorder(new EmptyBorder(0, 0, 0, 0));
        elementWrapper.setPreferredSize(new Dimension(450, 42));
        elementWrapper.setOpaque(true);

        elementWrapper.add(BorderLayout.CENTER, title = new XTitle("", 0));
        return elementWrapper;
    }

    @Override
    public void setMandatoryStyle() {
        // not in use
    }

    @Override
    public void setErrorStyle() {
        // not in use
    }

    @Override
    public void setDefaultStyle() {
        // not in use
    }

    @Override
    public void setEnabled(boolean b) {
        // not in use
    }

    @Override
    public void addKeyListener(KeyListener l) {
        // not in use
    }

    @Override
    public void setValue(String value) {
        // not in use
    }

    @Override
    public void clearInput() {
        // not in use
    }

    @Override
    public String getValue() {
        // not in use
        return "";
    }


}
