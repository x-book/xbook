package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import javax.swing.*;
import java.awt.*;

/**
 * A JLabel that is not automatically recolorable by Helper functions.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class NotColorableJPanel extends JPanel {

    /**
     * Creates a new JPanel with the specified layout manager and buffering strategy.
     *
     * @param layout           the LayoutManager to use
     * @param isDoubleBuffered a boolean, true for double-buffering, which uses additional memory space to achieve fast,
     *                         flicker-free updates
     */
    public NotColorableJPanel(LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
    }

    /**
     * Create a new buffered JPanel with the specified layout manager
     *
     * @param layout the LayoutManager to use
     */
    public NotColorableJPanel(LayoutManager layout) {
        super(layout);
    }

    /**
     * Creates a new <code>JPanel</code> with <code>FlowLayout</code> and the specified buffering strategy. If
     * <code>isDoubleBuffered</code> is true, the <code>JPanel</code> will use a double buffer.
     *
     * @param isDoubleBuffered a boolean, true for double-buffering, which uses additional memory space to achieve fast,
     *                         flicker-free updates
     */
    public NotColorableJPanel(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
    }

    /**
     * Creates a new <code>JPanel</code> with a double buffer and a flow layout.
     */
    public NotColorableJPanel() {
        super();
    }


}