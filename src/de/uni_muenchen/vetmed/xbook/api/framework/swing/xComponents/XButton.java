package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import java.awt.Dimension;
import javax.swing.JButton;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.Border;

/**
 * An object that inherits from the JButton class.
 *
 * It has the same functions than a standard JButton object, but is modified for a default style for xBook.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class XButton extends JPanel implements IButton {

    public enum Style {

        DEFAULT, DARKER
    }
    public static final int DEFAULT_HEIGHT = Sizes.BUTTON_HEIGHT;
    public static final int DEFAULT_WIDTH = Sizes.BUTTON_WIDTH;
    private Color borderColor;
    private Color backgroundColor;
    private Color backgroundColorMouseOver;
    private Color backgroundColorMousePressed;
    protected JButton button;
    private JPanel buttonBackgroundWrapper;
    private JPanel buttonWrapper;
    private JPanel wrapperCollapseable;
    private JLabel imageCollapseable;
    private JPopupMenu popup = new JPopupMenu();
    private boolean isPopupVisible = false;
    private boolean openPopupOnClick = false;

    /**
     * Constructor.
     *
     * Set the design for the object.
     *
     * @param text The text that is displayed on the button.
     */
    public XButton(String text) {
        super(new BorderLayout());

        setOpaque(false);

        buttonBackgroundWrapper = new JPanel(new BorderLayout());
        buttonBackgroundWrapper.setOpaque(true);

        buttonWrapper = new JPanel(new BorderLayout());
        buttonWrapper.setOpaque(true);

        button = new JButton();
        setText(text);
        button.setFont(Fonts.FONT_STANDARD_PLAIN);
        button.setBackground(Colors.CONTENT_BACKGROUND);

        button.setFocusPainted(false);
        button.setMargin(new Insets(0, 100, 0, 100));
        button.setContentAreaFilled(false);
        button.setOpaque(false);
        buttonBackgroundWrapper.add(BorderLayout.CENTER, button);
        buttonWrapper.add(BorderLayout.CENTER, buttonBackgroundWrapper);
        add(BorderLayout.CENTER, buttonWrapper);

        wrapperCollapseable = new JPanel(new BorderLayout());
        wrapperCollapseable.setBorder(BorderFactory.createEmptyBorder(0, 1, 0, 0));
        wrapperCollapseable.setOpaque(false);

        imageCollapseable = new JLabel();
        imageCollapseable.setHorizontalAlignment(JLabel.CENTER);
        imageCollapseable.setOpaque(false);
        imageCollapseable.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));
        imageCollapseable.setSize(new Dimension(Images.NAVIGATION_COLLAPSABLE_ICON_DOWN_BLACK.getIconWidth(), Images.NAVIGATION_COLLAPSABLE_ICON_DOWN_BLACK.getIconHeight()));
        imageCollapseable.setIcon(Images.NAVIGATION_COLLAPSABLE_ICON_DOWN_BLACK);
        wrapperCollapseable.add(BorderLayout.CENTER, imageCollapseable);
        wrapperCollapseable.setVisible(false);

        buttonBackgroundWrapper.add(BorderLayout.EAST, wrapperCollapseable);

        // set default design
        setStyle(Style.DEFAULT);

        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (button.isEnabled()) {
                    buttonBackgroundWrapper.setBackground(backgroundColorMouseOver);
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (button.isEnabled()) {
                    buttonBackgroundWrapper.setBackground(backgroundColor);
                }
            }
        });

        addCollapsableClickListener(imageCollapseable);
        addCollapsableClickListener(wrapperCollapseable);
        addCollapsableClickListener(buttonBackgroundWrapper);
        addCollapsableClickListener(this);
        addCollapsableClickListener(button);
    }

    /**
     * Constructor.
     *
     * Set the design for the object.
     */
    public XButton() {
        this("");
    }

    /**
     * Constructor.
     *
     * Set the design for the object.
     *
     * @param text The text that is displayed on the button.
     * @param width The width of the button in pixel.
     */
    public XButton(String text, int width) {
        this(text, width, DEFAULT_HEIGHT);
    }

    /**
     * Constructor.
     *
     * Set the design for the object.
     *
     * @param text The text that is displayed on the button.
     * @param width The width of the button in pixel.
     * @param height The height of the button in pixel.
     */
    public XButton(String text, int width, int height) {
        this(text);
        setButtonSize(width, height);
    }

    public void setText(String text) {
        text = text.replaceAll("<html>", "");
        text = text.replaceAll("</html>", "");
        text = text.replaceAll("<body>", "");
        text = text.replaceAll("</body>", "");
        button.setText("<html><body>&nbsp;&nbsp;" + text + "&nbsp;&nbsp;</body></html>");
    }

    public void addActionListener(ActionListener al) {
        button.addActionListener(al);
    }

    public void setIcon(Icon icon) {
        button.setIcon(icon);
    }

    public void setBorderAround(int top, int right, int bottom, int left) {
        super.setBorder(BorderFactory.createEmptyBorder(top, left, bottom, right));
    }

    public void setBorderButton(Border border, Border highlight) {
        if (buttonWrapper != null) {
            buttonWrapper.setBorder(border);
        }
        if (button != null) {
            button.setBorder(highlight);
        }
    }

    @Override
    @Deprecated
    public void setBackground(Color color) {
    }

    public void setBackgroundButton(Color color) {
        backgroundColor = color;
        if (buttonBackgroundWrapper != null) {
            buttonBackgroundWrapper.setBackground(color);
        }
    }

    public void setStyle(Style style) {
        switch (style) {
            case DARKER:
                setStyle(new Color(171, 173, 179), new Color(225, 225, 225), Colors.CONTENT_BACKGROUND, new Color(197, 197, 201));
                return;
            case DEFAULT:
            default:
                setStyle(new Color(171, 173, 179), Color.WHITE, Colors.CONTENT_BACKGROUND, new Color(197, 197, 201));
                return;
        }
    }

    public void setStyle(Color border, Color background, Color mouseover, Color pressed) {
        borderColor = border;
        backgroundColor = background;
        backgroundColorMouseOver = mouseover;
        backgroundColorMousePressed = pressed;

        setEnabled(isEnabled());
        wrapperCollapseable.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, borderColor));
        setBackgroundButton(backgroundColor);
    }

    public void setButtonSize(int width, int height) {
        button.setSize(new Dimension(width, height));
        button.setPreferredSize(new Dimension(width, height));
    }

    public void setButtonSizeToDefault() {
        setButtonSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    @Override
    public void setSize(Dimension d) {
        setSize(d.width, d.height);
    }

    @Override
    public void setSize(int width, int height) {
        super.setSize(width, height);
    }

    private void addCollapsableClickListener(final JComponent comp) {
        comp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (comp.equals(buttonBackgroundWrapper) || comp.equals(button) || comp.equals(this)) {
                    if (openPopupOnClick) {
                        togglePopup(e);
                    }
                } else {
                    togglePopup(e);
                }
            }
        });
    }

    private void togglePopup(MouseEvent e) {
        if (isEnabled()) {
            if (isPopupVisible) {
                popup.setVisible(false);
                isPopupVisible = false;
            } else {
                popup.show(e.getComponent(), e.getX() - DEFAULT_HEIGHT - 9, DEFAULT_HEIGHT - 9);
                isPopupVisible = true;
            }
        }
    }

    public JMenuItem addMenuItem(JMenuItem item) {
        popup.add(item);
        wrapperCollapseable.setVisible(popup.getSubElements().length > 0);
        return item;
    }

    public void addMenuSeparator() {
        popup.addSeparator();
        wrapperCollapseable.setVisible(popup.getSubElements().length > 0);
    }

    public void setOpenPopupOnClick() {
        openPopupOnClick = true;
        for (ActionListener al : button.getActionListeners()) {
            button.removeActionListener(al);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        button.setEnabled(enabled);
        if (enabled) {
            int r, g, b;
            if (borderColor.getRed() < backgroundColor.getRed()) {
                r = borderColor.getRed() + (255 - borderColor.getRed()) / 4 * 3;
                g = borderColor.getGreen() + (255 - borderColor.getGreen()) / 4 * 3;
                b = borderColor.getBlue() + (255 - borderColor.getBlue()) / 4 * 3;
            } else {
                r = backgroundColor.getRed() + (255 - backgroundColor.getRed()) / 4 * 3;
                g = backgroundColor.getGreen() + (255 - backgroundColor.getGreen()) / 4 * 3;
                b = backgroundColor.getBlue() + (255 - backgroundColor.getBlue()) / 4 * 3;
            }
            setBorderButton(BorderFactory.createLineBorder(borderColor, 2), BorderFactory.createLineBorder(new Color(r, g, b), 2));
        } else {
            int r = borderColor.getRed() + ((255 - borderColor.getRed()) / 2);
            int g = borderColor.getGreen() + ((255 - borderColor.getGreen()) / 2);
            int b = borderColor.getBlue() + ((255 - borderColor.getBlue()) / 2);
            buttonWrapper.setBorder(BorderFactory.createLineBorder(new Color(r, g, b, 1)));
        }
    }

    public void removeActionListener(ActionListener l) {
        button.removeActionListener(l);
    }

    public ActionListener[] getActionListeners() {
        return button.getActionListeners();
    }

    public JButton getButton() {
        return button;
    }
    
    
}
