package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.CustomDate;
import de.uni_muenchen.vetmed.xbook.api.exception.InvalidDateException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiDateElement;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.*;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.swing.border.EmptyBorder;

public class MultiDateChooser extends AbstractMultiComponent<MultiDateElement> {

    private DateChooser dateChooser;

    private JTextField date1Year;
    private JTextField date1Month;
    private JTextField date1Day;

    private JTextField date2Year;
    private JTextField date2Month;
    private JTextField date2Day;

    private XButton plusButton;
    private XButton minusButton;

    private JPanel customContentPanel;

    /**
     * Constructor of the DynamicComboBoxList class.
     *
     * @param name
     */
    public MultiDateChooser(String name) {
        super(name);
        customContentPanel = new JPanel(new StackLayout());

        init();

        setSelectAllAndRemoveAllButtonVisible(false);
        topWrapper.add(BorderLayout.WEST, getInputComponent());

        add(BorderLayout.NORTH, customContentPanel);
        topWrapper.remove(buttonRemoveWrapper);
    }

    @Override
    protected JComponent getInputComponent() {
        if (dateChooser == null) {
            dateChooser = new DateChooser();
        }
        return dateChooser;
    }

    @Override
    public void addInputToList() {
        addItemToList(getElementFromInput());
    }

    @Override
    public boolean addItemToList(MultiDateElement item) {

        if (item != null) {

            if (!item.getBegin().isValidDate() || !item.getEnd().isValidDate()) {
                return false;
            }
            if (!selectedItemsOnList.contains(item)) {
                for (MultiDateElement multiDateElement : selectedItemsOnList) {
                    if (multiDateElement.intersects(item)) {
                        return false;
                    }
                }
                selectedItemsOnList.add(item);
                listModel.addElement(item);
                dateChooser.clear();
                return true;
            }
        }
        dateChooser.clear();
        return false;
    }

    @Override
    public boolean addItemsToList(ArrayList<MultiDateElement> items) {
        boolean allSuccessful = true;
        for (MultiDateElement s : items) {
            allSuccessful = allSuccessful && addItemToList(s);
        }
        return allSuccessful;
    }

    @Override
    public void removeSelectedItemsFromList() {
        int[] selected = listSelectedItems.getSelectedIndices();
        for (int i = selected.length - 1; i >= 0; i--) {
            selectedItemsOnList.remove(selected[i]);
            listModel.removeElementAt(selected[i]);
        }
    }

    @Override
    public void removeAllItemsFromList() {
        for (int i = listModel.getSize() - 1; i >= 0; i--) {
            selectedItemsOnList.remove(i);
            listModel.removeElementAt(i);
        }
    }

    public void removeItem(MultiDateElement text) {
        for (int i = 0; i < listModel.getSize(); i++) {
            if (text.equals(selectedItemsOnList.get(i))) {
                selectedItemsOnList.remove(i);
                listModel.removeElementAt(i);
                return;
            }
        }
    }

    public void removeItems(List<MultiDateElement> text) {
        for (MultiDateElement s : text) {
            removeItem(s);
        }
    }

    @Override
    public void addAllItems() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ArrayList<MultiDateElement> getListItems(boolean addComboItemToo) {
        ArrayList<MultiDateElement> list = new ArrayList<>();
        for (int i = 0; i < listModel.size(); i++) {
            list.add(listModel.getElementAt(i));
        }
        if (addComboItemToo) {
            addInputToList();
        }
        return list;
    }

    @Override
    public void clearInput() {
        dateChooser.clear();
        removeAllItemsFromList();
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        dateChooser.setVisible(b);
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        dateChooser.setEnabled(b);
    }

    @Override
    public void addFocusListener(FocusListener fl) {
        super.addFocusListener(fl);
        dateChooser.addFocusListener(fl);
    }

    @Override
    protected Comparator<MultiDateElement> getComparator() {
        return new Comparator<MultiDateElement>() {

            @Override
            public int compare(MultiDateElement o1, MultiDateElement o2) {
                if (o1 == null) {
                    return -1;
                }
                if (o2 == null) {
                    return 1;
                }
                if (o1.equals(o2)) {
                    return 0;
                }
                return o1.getBegin().compareTo(o2.getBegin());
            }
        };
    }

    @Override
    public MultiDateElement getElementFromInput() {

        String valueDate1Year = date1Year.getText();
        if (valueDate1Year.isEmpty()) {
            valueDate1Year = null;
        }
        String valueDate1Month = date1Month.getText();
        if (valueDate1Month.isEmpty()) {
            valueDate1Month = null;
        }
        String valueDate1Day = date1Day.getText();
        if (valueDate1Day.isEmpty()) {
            valueDate1Day = null;
        }
        String valueDate2Year = date2Year.getText();
        if (valueDate2Year.isEmpty()) {
            valueDate2Year = null;
        }
        String valueDate2Month = date2Month.getText();
        if (valueDate2Month.isEmpty()) {
            valueDate2Month = null;
        }
        String valueDate2Day = date2Day.getText();
        if (valueDate2Day.isEmpty()) {
            valueDate2Day = null;
        }

        if (valueDate1Year == null && valueDate1Month == null && valueDate1Day == null && valueDate2Year == null && valueDate2Month == null && valueDate2Day == null) {
            return null;
        }

        CustomDate date1 = null;
        CustomDate date2 = null;
        try {
            date1 = new CustomDate(valueDate1Year, valueDate1Month, valueDate1Day);
        } catch (InvalidDateException ex) {
            //Logger.getLogger(MultiDateChooser.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            date2 = new CustomDate(valueDate2Year, valueDate2Month, valueDate2Day);
        } catch (InvalidDateException ex) {
            //Logger.getLogger(MultiDateChooser.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (date1 == null && date2 == null) {
            Footer.displayWarning(Loc.get("THE_DATE_IS_INVALID"));
            return null;
        }

        // if two dates were entered
        boolean date1Valid = (date1 != null && !date1.isEmpty());
        boolean date2Valid = (date2 != null && !date2.isEmpty());
        if (date1Valid && date2Valid) {
            return new MultiDateElement(date1, date2);
        }
        // if only the first date was entered
        if (date1Valid && !date2Valid) {
            return new MultiDateElement(date1);
        }
        // if only the second date was entered
        if (!date1Valid && date2Valid) {
            return new MultiDateElement(date2);
        }

        return null;
    }

    @Override
    public ArrayList<Component> getFocusableComponents() {
        ArrayList<Component> comp = super.getFocusableComponents();
        comp.add(date1Year);
        comp.add(date1Month);
        comp.add(date1Day);
        comp.add(date2Year);
        comp.add(date2Month);
        comp.add(date2Day);
        comp.add(plusButton.getButton());
        comp.add(minusButton.getButton());
        return comp;
    }

    private class DateChooser extends JPanel {

        public DateChooser() {
            super(new BorderLayout());

            JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));

            p1.add(new JLabel(" " + Loc.get("START") + ": "));

            date1Day = new JTextField();
            date1Day.setPreferredSize(new Dimension(30, 24));
            date1Day.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    if (!StringHelper.isInteger(date1Day.getText())) {
                        date1Day.setText("");
                    }
                }
            });
            p1.add(date1Day);

            p1.add(new JLabel("."));

            date1Month = new JTextField();
            date1Month.setPreferredSize(new Dimension(30, 24));
            date1Month.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    if (!StringHelper.isInteger(date1Month.getText())) {
                        date1Month.setText("");
                    }
                }
            });
            p1.add(date1Month);

            p1.add(new JLabel("."));

            date1Year = new JTextField();
            date1Year.setPreferredSize(new Dimension(60, 24));
            date1Year.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    if (!StringHelper.isInteger(date1Year.getText())) {
                        date1Year.setText("");
                    }
                }
            });
            p1.add(date1Year);

            p1.add(new JLabel("         " + Loc.get("END") + ": "));

            date2Day = new JTextField();
            date2Day.setPreferredSize(new Dimension(30, 24));
            date2Day.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    if (!StringHelper.isInteger(date2Day.getText())) {
                        date2Day.setText("");
                    }
                }
            });
            p1.add(date2Day);

            p1.add(new JLabel("."));

            date2Month = new JTextField();
            date2Month.setPreferredSize(new Dimension(30, 24));
            date2Month.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    if (!StringHelper.isInteger(date2Month.getText())) {
                        date2Month.setText("");
                    }
                }
            });
            p1.add(date2Month);

            p1.add(new JLabel("."));

            date2Year = new JTextField();
            date2Year.setPreferredSize(new Dimension(60, 24));
            date2Year.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    if (!StringHelper.isInteger(date2Year.getText())) {
                        date2Year.setText("");
                    }
                }
            });
            p1.add(date2Year);

            customContentPanel.add(p1);

            //////
            JPanel p2 = new JPanel(new BorderLayout());

            plusButton = new XButton(" ˅˅ ");
            plusButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addInputToList();
                }
            });
            p2.add(BorderLayout.WEST, plusButton);

            minusButton = new XButton(" ˄˄ ");
            minusButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    removeSelectedItemsFromList();
                }
            });
            p2.add(BorderLayout.EAST, minusButton);

            customContentPanel.add(p2);
        }

        public void clear() {
            date1Year.setText("");
            date1Month.setText("");
            date1Day.setText("");
            date2Year.setText("");
            date2Month.setText("");
            date2Day.setText("");
        }
    }

}
