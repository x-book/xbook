package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import java.util.ArrayList;
import javax.swing.JPanel;

import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import de.uni_muenchen.vetmed.xbook.api.exception.MissingInputException;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractSynchronisationManager;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.BorderFactory;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class SolveConflictLineGroup extends JPanel {

    /**
     * Holds all conflict lines of this group.
     */
    private ArrayList<SolveConflictLine> lines;
    /**
     * The table name of the group (e.g. input unit, project, etc...)
     */
    private String tableName;
    /**
     * The status when the group was synced the last time.
     */
    private String lastSync;
    /**
     * The state if the data set (the group) was already deleted globally.
     */
    private boolean deletedGlobal;
    private JPanel inner;

    private final boolean save;
    private Key projectKey;
    private Key entryKey;

    /**
     * Constructor.
     *
     * @param tableName The table name of the group (e.g. input unit, project,
     * etc...)
     */
    public SolveConflictLineGroup(String tableName) {
        this(tableName, true);
    }

    public SolveConflictLineGroup(String tableName, boolean save) {
        super(new BorderLayout());
        inner = new JPanel(new StackLayout());
        inner.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

        lines = new ArrayList<>();
        this.tableName = tableName;
        setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, Color.BLACK));
        add(inner);
        this.save = save;
    }

    /**
     * Adds a new conflict line to the group.
     *
     * @param line
     */
    public void add(SolveConflictLine line) {
        lines.add(line);
        inner.add(line);
    }

    /**
     * Saves the current selected data of all conflict lines of this group to
     * the ArrayList<DbDataHash> object.
     *
     * @param entryData The object where to save the data.
     * @throws MissingInputException When an necessary option was not selected.
     */
    public void save(DataSetOld entryData, boolean addStatus) throws MissingInputException {
        if (!save) {
            return;
        }
        DataTableOld table = entryData.getOrCreateDataTable(tableName);
        DataRow hash = new DataRow(tableName);
        for (SolveConflictLine line : lines) {
            line.save(hash);
        }
        if (projectKey != null) {
            hash.put(IStandardColumnTypes.PROJECT_ID, "" + projectKey.getID());
            hash.put(IStandardColumnTypes.PROJECT_DATABASE_ID, "" + projectKey.getDBID());
        }
        else if(entryData.getProjectKey()!= null){
            hash.put(IStandardColumnTypes.PROJECT_ID, "" + entryData.getProjectKey().getID());
            hash.put(IStandardColumnTypes.PROJECT_DATABASE_ID, "" + entryData.getProjectKey().getDBID());
        }
        if (entryKey != null) {
            hash.put(IStandardColumnTypes.ID, "" + entryKey.getID());
            hash.put(IStandardColumnTypes.DATABASE_ID, "" + entryKey.getDBID());
        }
        else if(entryData instanceof EntryDataSet && ((EntryDataSet) entryData).getEntryKey()!= null){

            final Key entryKey = ((EntryDataSet) entryData).getEntryKey();
            hash.put(IStandardColumnTypes.ID, "" + entryKey.getID());
            hash.put(IStandardColumnTypes.DATABASE_ID, "" + entryKey.getDBID());

        }
        if (addStatus) {
            hash.add(new DataColumn(lastSync, IStandardColumnTypes.STATUS.getColumnName()));
        }
        table.add(hash);
    }

    /**
     * Get all SolveConflictLine objects.
     *
     * @return All SolveConflictLine objects.
     */
    public ArrayList<SolveConflictLine> getLines() {
        return lines;
    }

    /**
     * Get the status when the group was synced the last time.
     *
     * @return The status when the group was synced the last time.
     */
    public String getLastSync() {
        return lastSync;
    }

    /**
     * Set the status, when the group was synced the last time, to a new value.
     *
     * @param lastSync The new synced status.
     */
    public void setLastSync(String lastSync) {
        this.lastSync = lastSync;
    }

    /**
     * Get the status if the data set was already deleted globally.
     *
     * @return The status if the data set was already deleted globally.
     */
    public boolean isDeletedGlobal() {
        return deletedGlobal;
    }

    /**
     * Set the data set deleted globally or not.
     *
     * @param deletedGlobal The state if the data set was deleted globally or
     * not.
     */
    public void setDeletedGlobal(boolean deletedGlobal) {
        this.deletedGlobal = deletedGlobal;
    }

    /**
     * Returns the table name of the conflict line group.
     *
     * @return The table name.
     */
    public String getTableName() {
        return tableName;
    }

    public void setProjectKey(Key projectKey) {
        this.projectKey = projectKey;
    }

    public void setEntryKey(Key entryKey) {
        this.entryKey = entryKey;
    }
}
