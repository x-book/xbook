package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.helper.MultiFileChooserElement;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.filechooser.FileNameExtensionFilter;

public class MultiFileChooser extends AbstractMultiComponent<MultiFileChooserElement> {

    private FileChooser fileChooser;
    private XButton openButton;
    private XButton removeSelectionButton;
    private final JLabel maxFileSizeLabel;

    /**
     * Constructor of the DynamicComboBoxList class.
     *
     * @param name
     */
    public MultiFileChooser(String name) {
        super(name);
        init();

        setSelectAllAndRemoveAllButtonVisible(false);

        openButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openSelectedFileInList();
            }
        });
        listSelectedItems.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    openSelectedFileInList();
                }
            }
        });
        removeSelectionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeSelectedItemsFromList();
            }
        });

        // rearrangement and invisibility for some elements
        getButtonAdd().setVisible(false);
        fileChooser.getSelectButton().setText(Loc.get("ADD_FILE"));
        topWrapper.add(BorderLayout.WEST, fileChooser.getSelectButton());
        topWrapper.add(BorderLayout.CENTER, maxFileSizeLabel = new JLabel(""));
        topWrapper.add(BorderLayout.EAST, removeSelectionButton);

        buttonRemove.setText(Loc.get("REMOVE_SELECTION"));
    }

    public XButton getRemoveSelectionButton() {
        return removeSelectionButton;
    }

    @Override
    protected JComponent getInputComponent() {
        if (fileChooser == null) {
            fileChooser = new FileChooser() {

                @Override
                protected void actionOnSelectButton() {
                    super.actionOnSelectButton();
                    addInputToList();
                }

            };
            fileChooser.getOpenButton().setVisible(false);
            fileChooser.getTextField().setVisible(false);

            openButton = new XButton(Loc.get("OPEN"));
            listSpecificWrapper.add(BorderLayout.EAST, openButton);
            removeSelectionButton = new XButton(Loc.get("REMOVE_SELECTION"));
        }
        return fileChooser;
    }

    private void openSelectedFileInList() {
        MultiFileChooserElement mfce = (MultiFileChooserElement) listSelectedItems.getSelectedValue();
        try {
            fileChooser.openFile(mfce);
        } catch (IOException ex) {
            Logger.getLogger(MultiFileChooser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void addInputToList() {
        String fileName = fileChooser.getFileName();
        if (fileName == null || fileName.isEmpty()) {
            return;
        }
        addItemToList(new MultiFileChooserElement(fileChooser.getFileName(), fileChooser.getFileEncoded()));
    }

    @Override
    public boolean addItemToList(MultiFileChooserElement item) {
        if (item != null) {
            if (!selectedItemsOnList.contains(item)) {
                selectedItemsOnList.add(item);
                listModel.addElement(item);
                fileChooser.clear();
                return true;
            }
        }
        return false;
    }

    @Override
    public void removeSelectedItemsFromList() {
        int[] selected = listSelectedItems.getSelectedIndices();
        for (int i = selected.length - 1; i >= 0; i--) {
            selectedItemsOnList.remove(selected[i]);
            listModel.removeElementAt(selected[i]);
        }
    }

    @Override
    public void removeAllItemsFromList() {
        for (int i = listModel.getSize() - 1; i >= 0; i--) {
            selectedItemsOnList.remove(i);
            listModel.removeElementAt(i);
        }
    }

    public void removeItem(MultiFileChooserElement text) {
        for (int i = 0; i < listModel.getSize(); i++) {
            if (text.equals(selectedItemsOnList.get(i))) {
                selectedItemsOnList.remove(i);
                listModel.removeElementAt(i);
                return;
            }
        }
    }

    public void removeItems(List<MultiFileChooserElement> text) {
        for (MultiFileChooserElement s : text) {
            removeItem(s);
        }
    }

    @Override
    public void addAllItems() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ArrayList<MultiFileChooserElement> getListItems(boolean addComboItemToo) {
        ArrayList<MultiFileChooserElement> list = new ArrayList<>();
        for (int i = 0; i < listModel.size(); i++) {
            list.add(listModel.getElementAt(i));
        }
        if (addComboItemToo) {
            list.add(new MultiFileChooserElement(fileChooser.getFileName(), fileChooser.getFileEncoded()));
        }
        return list;
    }

    @Override
    public void clearInput() {
        fileChooser.clear();
        removeAllItemsFromList();
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        fileChooser.setVisible(b);
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        fileChooser.setEnabled(b);
    }

    @Override
    public void addFocusListener(FocusListener fl) {
        super.addFocusListener(fl);
        fileChooser.addFocusListener(fl);
    }

    @Override
    protected Comparator<MultiFileChooserElement> getComparator() {
        return new Comparator<MultiFileChooserElement>() {

            @Override
            public int compare(MultiFileChooserElement o1, MultiFileChooserElement o2) {
                if (o1 == null) {
                    return -1;
                }
                if (o2 == null) {
                    return 1;
                }
                if (o1.equals(o2)) {
                    return 0;
                }
                return o1.getFileName().compareTo(o2.getFileName());
            }
        };
    }

    @Override
    public boolean addItemsToList(ArrayList<MultiFileChooserElement> items) {
        boolean allSuccessful = true;
        for (MultiFileChooserElement s : items) {
            allSuccessful = allSuccessful && addItemToList(s);
        }
        return allSuccessful;
    }

    public FileChooser getFileChooser() {
        return fileChooser;
    }

    @Override
    public MultiFileChooserElement getElementFromInput() {
        return new MultiFileChooserElement(fileChooser.getFileName(), fileChooser.getFileEncoded());
    }

    public void setFileFilter(FileNameExtensionFilter fileFilter) {
        fileChooser.setFileFilter(fileFilter);
    }

    public void setMaxFileSize(long maxFileSize) {
        fileChooser.setMaxFileSize(maxFileSize);
        updateMaxFileSizeLabel();
    }

    private void updateMaxFileSizeLabel() {
        maxFileSizeLabel.setText("  max: " + fileChooser.getMaxFileSizeInMB() + " MB");
    }

}
