package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.FileTooLargeException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@connact.io>
 */
public class RawFileNameChooser extends RawTextField {

    /**
     * The available modes for the functionality of the RawFileNameChooser.
     */
    public static enum Mode {
        DIRECTORY, FILE_AND_DIRECTORY, FILE
    }

    /**
     * The file filter options.
     */
    private final ArrayList<FileNameExtensionFilter> fileFilter;
    /**
     * The button to open the file chooser.
     */
    protected XButton buttonView;
    /**
     * The current selected file information.
     */
    protected File currentLoadedFile = null;
    /**
     * The current selected directory information.
     */
    protected File currentLoadedDirectory = null;

    /**
     * The current selected mode.
     */
    private Mode mode;

    /**
     * Constructor.
     *
     * @param columnType The corresponding ColumnType.
     * @param fileFilter The file filter options.
     * @param mode       The selected mode.
     */
    public RawFileNameChooser(ColumnType columnType, ArrayList<FileNameExtensionFilter> fileFilter, Mode mode) {
        super(columnType);
        this.fileFilter = fileFilter;
        this.mode = mode;
    }

    /**
     * Constructor.
     * <p>
     * Using the file option for mode as default.
     *
     * @param columnType The corresponding ColumnType.
     * @param fileFilter The file filter options.
     */
    public RawFileNameChooser(ColumnType columnType, ArrayList<FileNameExtensionFilter> fileFilter) {
        this(columnType, fileFilter, Mode.FILE);
    }

    @Override
    public void createField() {
        super.createField();

        buttonView = new XButton(Loc.get("LOAD"));
        buttonView.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser(currentLoadedFile);
                chooser.setAcceptAllFileFilterUsed(false);
                for (FileNameExtensionFilter filter : fileFilter) {
                    chooser.addChoosableFileFilter(filter);
                }
                int returnVal = chooser.showOpenDialog(RawFileNameChooser.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    currentLoadedFile = chooser.getSelectedFile();
                    currentLoadedDirectory = chooser.getCurrentDirectory();

                    if (mode == Mode.FILE_AND_DIRECTORY) {
                        textField.setText(currentLoadedDirectory.getAbsolutePath() + "/" + currentLoadedFile.getName());
                    } else if (mode == Mode.FILE) {
                        textField.setText(currentLoadedFile.getName());
                    } else if (mode == Mode.DIRECTORY) {
                        textField.setText(currentLoadedDirectory.getAbsolutePath());
                    }

                    try {
                        onFileChoosen(currentLoadedDirectory, currentLoadedFile);
                    } catch (FileTooLargeException e1) {
                        textField.setText("");
                        inputElement.setErrorStyle();
                    }
                }
            }
        });
        add(BorderLayout.EAST, buttonView);
    }

    /**
     * Action that is called after a file was chosen.
     *
     * @param lastLoadedDirectory The selected directory as a File object.
     * @param lastLoadedFile      The selected file as a File object.
     * @throws FileTooLargeException
     */
    protected void onFileChoosen(File lastLoadedDirectory, File lastLoadedFile) throws FileTooLargeException {
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = super.getMyFocusableComponents();
        comp.add(buttonView);
        comp.add(buttonView.getButton());
        return comp;
    }

    public XButton getButtonView() {
        return buttonView;
    }
}
