package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import javax.swing.JCheckBox;

import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

/**
 * 
 * @author Eduardo Granado <eduardo.granado@unibas.ch>
 * 
 */
public class XCheckBox extends JCheckBox {
	public XCheckBox(String label) {
		super(label);
		setBackground(Colors.CONTENT_BACKGROUND);
	}
}
