package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Images;
import de.uni_muenchen.vetmed.xbook.api.helper.ComponentHelper;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class BigButton extends JPanel {

    final Color BOX_COLOR;
    final Color BOX_COLOR_HOVERED;
    final Color BOX_COLOR_PRESSED;

    private String textValue = "";
    private String subLineValue = "";

    public final static int DEFAULT_WIDTH = 200;
    public final static int DEFAULT_HEIGHT = 100;

    private JLabel label;

    public BigButton(String text) {
        this(text, "", null, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public BigButton(String text, Icon icon) {
        this(text, "", icon, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public BigButton(String text, String subLine) {
        this(text, subLine, null, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public BigButton(String text, String subLine, Icon icon) {
        this(text, subLine, icon, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public BigButton(String text, int width, int height) {
        this(text, "", null, width, height);
    }

    public BigButton(String text, Icon icon, int width, int height) {
        this(text, "", icon, width, height);
    }

    public BigButton(String text, String subLine, int width, int height) {
        this(text, subLine, null, width, height);
    }

    public BigButton(String text, String subLine, Icon icon, int width, int height) {
        this.textValue = text;
        this.subLineValue = subLine;

        BOX_COLOR = Colors.BOOK_COLOR;
        BOX_COLOR_HOVERED = new Color(BOX_COLOR.getRed() / 2, BOX_COLOR.getGreen() / 2, BOX_COLOR.getBlue() / 2);
        BOX_COLOR_PRESSED = new Color(BOX_COLOR.getRed() + 30, BOX_COLOR.getGreen() + 30, BOX_COLOR.getBlue() + 30);

        setLayout(new BorderLayout());
        setSize(width, height);
        setPreferredSize(new Dimension(width, height));
        setMinimumSize(new Dimension(width, height));
        setBackground(Colors.CONTENT_BACKGROUND);

        label = new JLabel("", SwingConstants.CENTER);
        label.setForeground(Color.WHITE);
        label.setVerticalAlignment(SwingConstants.CENTER);
        label.setIcon(icon);
        label.setOpaque(true);
        label.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));

        setValue(textValue, subLineValue);

        label.addMouseListener(new MouseAdapter() {
            boolean isHovered = false;

            @Override
            public void mouseEntered(MouseEvent e) {
                isHovered = true;
                label.setBackground(BOX_COLOR_HOVERED);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                isHovered = false;
                label.setBackground(BOX_COLOR);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                label.setBackground(BOX_COLOR_PRESSED);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (isHovered) {
                    label.setBackground(BOX_COLOR_HOVERED);
                } else {
                    label.setBackground(BOX_COLOR);
                }
            }
        });

        add(BorderLayout.CENTER, ComponentHelper.wrapComponent(label, Colors.CONTENT_BACKGROUND, 10));
        label.setBackground(BOX_COLOR);

    }

    @Override
    public synchronized void addMouseListener(MouseListener l) {
        label.addMouseListener(l);
    }

    /**
     * Set a text to the button. Does not change the subline.
     *
     * @param text The text value.
     */
    public void setText(String text) {
        setValue(text, subLineValue);
    }

    /**
     * Set a text to the button. Does not change the subline.
     *
     * @param subline The subline value.
     */
    public void setSubLine(String subline) {
        setValue(textValue, subline);
    }

    /**
     * Set a text and a subline to the button.
     *
     * @param text The text value.
     * @param subLine The subline value.
     */
    public void setValue(String text, String subLine) {
        this.textValue = text;
        this.subLineValue = subLine;

        String t = "<html><font size='4'><center>" + textValue + "</center></font>";
        if (subLineValue != null && !subLineValue.equals("")) {
            t += "<br /><font size='2'><center>" + subLineValue + "</center></font>";
        }
        t += "</html>";

        label.setText(t);
    }

}
