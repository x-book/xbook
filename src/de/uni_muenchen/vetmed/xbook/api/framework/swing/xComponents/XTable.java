package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.table.CustomTableModel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.table.TableColumnAdjuster;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.report.AbstractReport;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.sort.SortController;
import org.jdesktop.swingx.sort.StringValueProvider;

/**
 * A JTable designed specifically for Export Result data, to be able to store and filter entries.
 */
public class XTable extends JXTable {

  TableColumnAdjuster adjuster;
  private XTableSorter<CustomTableModel> sorter;
  XRowFilter filter;
  ArrayList<ColorManager> cm = new ArrayList<>();

  public XTable() {
    super(new CustomTableModel());
    setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    setShowHorizontalLines(false);
    setShowVerticalLines(false);
    setGridColor(Color.GRAY);
    setIntercellSpacing(new Dimension(0, 1));
    setRowHeight(getRowHeight() + 4);
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    setRowSelectionAllowed(true);

    adjuster = new TableColumnAdjuster(this);
    filter = new XRowFilter();
    sorter = new XTableSorter<>((CustomTableModel) getModel());
    sorter.setRowFilter(filter);
    //sorter.setModel(getModel());
    setRowSorter(sorter);

    sorter.setRowFilter(filter);
    adjustColumns();
    setAutoCreateRowSorter(false);

  }

  @Override
  protected RowSorter<? extends TableModel> createDefaultRowSorter() {
    return new XTableSorter<>((CustomTableModel) getModel());
  }

  public boolean addColorManager(ColorManager colorManager) {
    return cm.add(colorManager);
  }

  public boolean removeColorManager(ColorManager colorManager) {
    return cm.remove(colorManager);
  }

  @Override
  public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
    try {
      Component c = super.prepareRenderer(renderer, row, column);

      // colorize the single rows
      if (!isRowSelected(row)) {
        c.setBackground(row % 2 == 0 ? null : Colors.CONTENT_BACKGROUND);
      }
      for (ColorManager colorManager : cm) {
        colorManager.colorize(c, row, column);
      }

      return c;
    } catch (NullPointerException ex) {
      return null;
    }
  }

  @Override
  public void setModel(TableModel dataModel) {
    super.setModel(dataModel);
    if (sorter != null) {
      sorter.setModel((CustomTableModel) dataModel);
      setRowSorter(sorter);
    }

  }

  /**
   * Sets the preferred width of the visible column specified by vColIndex. The column will be just
   * wide enough to show the column head and the widest cell in the column. margin pixels are added
   * to the left and right (resulting in an additional width of 2*margin pixels).
   */
  public void adjustColumns() {
    adjuster.adjustColumns();
  }

  public static class XRowFilter extends RowFilter<CustomTableModel, Integer> {

    ArrayList<DataColumn> columns;

    ArrayList<FilterManager> filterManagers = new ArrayList<>();

    public XRowFilter() {
      this(new ArrayList<DataColumn>());
    }

    public boolean addFilterManager(FilterManager filter) {

      return filterManagers.add(filter);

    }

    public boolean removeFilterManager(FilterManager filter) {
      return filterManagers.remove(filter);
    }

    public boolean removeAllFilters() {
      filterManagers.clear();
      return true;
    }

    public XRowFilter(ArrayList<DataColumn> columns) {
      this.columns = columns;
    }

    public void addFilter(DataColumn column) {
      columns.add(column);
    }

    public void removeFilter(DataColumn column) {
      columns.remove(column);
    }

    @Override
    public boolean include(Entry<? extends CustomTableModel, ? extends Integer> entry) {
      for (DataColumn column : columns) {
        int columnId = entry.getModel().findColumn(column.getColumnName());
        if (columnId != -1) {
          String value = entry.getStringValue(columnId);
          if (!value.contains(column.getValue())) {
            return false;
          }
        }
      }
      for (FilterManager fm : filterManagers) {
        if (!fm.include(entry)) {
          return false;
        }
      }
      return true;
    }
  }

  public static class XTableSorter<T extends TableModel> extends TableRowSorter<T> implements
      SortController<T> {


    Comparator<String> myComp = new Comparator<String>() {
      @Override
      public int compare(String o1, String o2) {
        return AbstractReport.compareNumber(o1, o2);
      }
    };
    private boolean b;
    private SortOrder[] sortOrders;
    private StringValueProvider stringValueProvider;
    private HashMap<Integer, SortOrder> sortOrderHashMap = new HashMap<>();

    public XTableSorter(T model) {
      super(model);
    }

    @Override
    public void setSortable(boolean b) {
      this.b = b;
    }

    @Override
    public boolean isSortable() {
      return b;
    }

    @Override
    public Comparator getComparator(int column) {
      return myComp;
    }

    @Override
    public void setSortOrderCycle(SortOrder... sortOrders) {
      this.sortOrders = sortOrders;
    }

    @Override
    public SortOrder[] getSortOrderCycle() {
      return sortOrders;
    }

    @Override
    public void setStringValueProvider(StringValueProvider stringValueProvider) {

      this.stringValueProvider = stringValueProvider;
    }

    @Override
    public StringValueProvider getStringValueProvider() {
      return stringValueProvider;
    }

    @Override
    public void setSortOrder(int i, SortOrder sortOrder) {
      sortOrderHashMap.put(i, sortOrder);
    }

    @Override
    public SortOrder getSortOrder(int i) {
      return sortOrderHashMap.get(i);
    }

    @Override
    public void resetSortOrders() {
      sortOrderHashMap.clear();
    }

  }

}
