package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XComboBox;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * A class which is similar to a JComboBox but makes it possible to search for
 * substrings in the list.
 *
 * Supports using ids instead of the value.
 *
 * @author Daniel Kaltenthaler
 * @version 1.0.0
 */
public abstract class DynamicComboBox<T> extends JPanel {

    /**
     * Always holds the current input in the textField. (Should be identical
     * with the text in the textfield.)
     */
    private String currentInput;
    /**
     * A list holding all items that can be displayed in the combo box.
     */
    private ArrayList<T> comboData;
    /**
     * A list holding all original items that can be displayed in the combo box.
     */
    private ArrayList<T> originalList;
    /**
     * A list holding all items that can be displayed in the combo box.
     * <code>null</code> if ids are not supported.
     */
    private HashMap<String, T> itemHash;
    /**
     * The JComboBox object.
     */
    private XComboBox<T> combo;
    /**
     * The object that holds the label.
     */
    private JLabel label;
    /**
     * A description of the input field.
     */
    private String name;

    /**
     * Creates and initialises the DynamicComboBox object.
     *
     * @param itemList An array holding all items that can be selected in the
     * combobox.
     */
    @Deprecated
    public DynamicComboBox(ArrayList<T> itemList, String name) {
        this.comboData = itemList;
        this.name = name;
        init();
    }

    /**
     * Creates and initialises the DynamicComboBox object.
     *
     * @param itemHash An HashMap holding all items and the correspondenting
     * ids. <code>null</code> if ids should not be used.
     * @param name
     */
    public DynamicComboBox(HashMap<String, T> itemHash, String name) {
        this.itemHash = itemHash;
        ArrayList<T> list = new ArrayList<>();
        list.add(getEmptyItem());
        for (T item : itemHash.values()) {
            list.add(item);
        }
        Collections.sort(list, getComparator());
        this.comboData = list;
        this.originalList = list;
        this.name = name;
        init();
    }

    /**
     * Initialise the object.
     */
    private void init() {
        label = new JLabel();

        setLayout(new BorderLayout());

        combo = new XComboBox(comboData.toArray()) {
            @Override
            protected void actionOnFocusLost(JTextField tf) {
                if (isIntegerInputEnabled()) {
                    String input = tf.getText();
                    try {
                        int id = Integer.parseInt(input);
                        T s = itemHash.get("" + id);
                        System.out.println("found: " + id + " -> " + s);
                        for (int i = 0, n = combo.getModel().getSize(); i < n; i++) {
                            String currentItem = combo.getModel().getElementAt(i).toString();
                            if (currentItem.equals(s)) {
                                combo.setSelectedItem(s);
                                return;
                            }
                        }
                        Object o = combo.getSelectedItem();
                        if (combo.getSelectedItem() == null) {
                            combo.setSelectedItem("");
                            Footer.displayWarning(Loc.get("ID_NOT_FOUND"));
                            combo.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                        }
                        return;
                    } catch (NumberFormatException nfe) {
                        // do nothing
                    }
                }
                super.actionOnFocusLost(tf);
            }
        };
        combo.setEditable(true);
        combo.setCustomInputEnabled(false);
        combo.setIntegerInputEnabled(true);
        combo.setEditable(true);
        combo.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (combo.getSelectedItem() == null || combo.getSelectedItem().toString().isEmpty()) {
                    combo.setToolTipText(null);
                } else {
                    combo.setToolTipText(combo.getSelectedItem().toString());
                }
            }
        });

        if (combo.getEditor() != null) {
            final JTextField textField = (JTextField) combo.getEditor().getEditorComponent();
            if (textField != null) {
                // try to convert an id to the correspondenting value when leaving the textfield.
                textField.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusGained(FocusEvent e) {
                        combo.setBorder(new JComboBox().getBorder());
                    }
                });
            }
        }

        add(BorderLayout.CENTER, combo);
    }

    public abstract T getEmptyItem();

    /**
     * Returns the current text of the textfield.
     *
     * @return The current text of the textfield.
     */
    public String getText() {
        JTextField textField = (JTextField) combo.getEditor().getEditorComponent();
        return textField.getText();
    }

    /**
     * Sets a new text to the textfield.
     *
     * @param s The text to be inserted to the textfield.
     */
    public void setText(String s) {
        JTextField textField = (JTextField) combo.getEditor().getEditorComponent();
        textField.setText(s);
        currentInput = s;
    }

    /**
     * Set all the original data to the combobox.
     */
    public void resetList() {
        updateComboItems(originalList);
    }

    /**
     * Replaced all elements of the combo box with new ones.
     *
     * @param newItems Holds the new items combo box which will replace the old
     * ones.
     */
    public void updateComboItems(ArrayList<T> newItems) {
        setItems(newItems);
    }

    public abstract Comparator<T> getComparator();

    public void updateComboItems(HashMap<String, T> itemHash) {
        ArrayList<T> list = new ArrayList<>();
        for (T item : itemHash.values()) {
            list.add(item);
        }
        setItems(list);
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        combo.setEditable(b);
        ((JTextField) combo.getEditor().getEditorComponent()).setEditable(b);
    }

    @Override
    public void addFocusListener(FocusListener fl) {
        if (combo.getEditor() != null) {
            JTextField textField = (JTextField) combo.getEditor().getEditorComponent();
            if (textField != null) {
                textField.addFocusListener(fl);
            }
        }
    }

    /**
     * Return the combo box.
     *
     * @return The combo box.
     */
    public XComboBox<T> getComboBox() {
        return combo;
    }

    /**
     * Return the combo box.
     *
     * @return The combo box.
     */
    public JTextField getTextFieldOfComboBox() {
        if (combo.getEditor() != null) {
            return (JTextField) combo.getEditor().getEditorComponent();
        }
        return null;
    }

    /**
     * Adds a text label on the left side of the combo box.
     *
     * @param text The text.
     */
    public void addLabel(String text) {
        label.setText(text);
        add(BorderLayout.WEST, label);
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (label != null) {
            label.setBackground(bg);
        }
    }

    public T getSelectedItem() {
        return combo.getItemAt(combo.getSelectedIndex());
    }

    public void setSelectedItem(T t) {
        combo.setSelectedItem(t);
    }

    public void removeAllItemsFromList() {
        comboData.clear();
        combo.removeAllItems();
    }

    public void removeItemFromList(T t) {
        comboData.remove(t);
        combo.removeItem(t);
    }

    public void addItemToList(T t) {
        comboData.add(t);
        combo.addItem(t);
    }

    public void setItems(ArrayList<T> items) {
        setItems(items, true);
    }

    public void setItems(ArrayList<T> items, boolean sort) {
        items.add(getEmptyItem());
        if (sort) {
            Collections.sort(items, getComparator());
        }
        comboData = items;
        combo.setModel(new DefaultComboBoxModel<>((T[]) comboData.toArray()));
    }

    public ArrayList<T> getComboData() {
        return comboData;
    }

    public ArrayList<Component> getFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(this);
        comp.add(getComboBox().getTextField());
        return comp;
    }

    public static class DynamicComboBoxString extends DynamicComboBox<String> {

        public DynamicComboBoxString(ArrayList<String> itemList, String name) {
            super(itemList, name);
        }

        public DynamicComboBoxString(HashMap<String, String> itemHash, String name) {
            super(itemHash, name);
        }

        @Override
        public String getEmptyItem() {
            return "";
        }

        @Override
        public Comparator<String> getComparator() {
            return new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
            };
        }
    }
}
