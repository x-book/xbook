package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.ITRawFloatField;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.ITRawNumericField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntryRoot;

/**
 *
 * @author Daniel Kaltenthaler
 */
public class FloatConstraints extends AbstractInputConstraints {

	/**
	 * Constructor.
	 *
	 * Initialize the constraints.
	 *
	 * @param entry      The corresponding Entry object of the input field.
	 * @param columnType The corresponding ColumnType of the input field.
	 * @param width The width of the input fields in pixels.
	 * @param height The height of the input fields in pixels.
	 * @param borderEast The size of the border in the east.
	 */
	public FloatConstraints(AbstractEntryRoot entry, ColumnType columnType, int width, int height, int borderEast) {
		super(entry, columnType, width, height, borderEast);
	}

	/**
	 * Constructor.
	 *
	 * Initialize the constraints. Use this constructor to display no border in the east.
	 *
	 * @param entry      The corresponding Entry object of the input field.
	 * @param columnType The corresponding ColumnType of the input field.
	 * @param width The width of the input fields in pixels.
	 * @param height The height of the input fields in pixels.
	 */
	public FloatConstraints(AbstractEntryRoot entry, ColumnType columnType, int width, int height) {
		super(entry, columnType, width, height, 0);
	}

	/**
	 * Constructor.
	 *
	 * Initialize the constraints. Use this constructor to display no border in the east.
	 *
	 * @param entry      The corresponding Entry object of the input field.
	 * @param columnType The corresponding ColumnType of the input field.
	 * @param width The width of the input fields in pixels.
	 */
	public FloatConstraints(AbstractEntryRoot entry, ColumnType columnType, int width) {
		super(entry, columnType, width, 1, 0);
	}

	@Override
	protected AbstractITRaw createInputCustom() {
		return new ITRawFloatField(this);
	}

}
