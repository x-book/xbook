package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.elements.SidebarEntryField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntryRoot;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.dependable.AbstractBaseDependableEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Defines the constraints for a specific input field in the ITTable.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public abstract class AbstractInputConstraints<T extends AbstractInputConstraints> {

    /**
     * The corresponding ColumnType of the input field.
     */
    private ColumnType columnType;

    /**
     * The border of the single input in the east, that can displayed when one cell consists of several inputs. Must be
     * defined manually when use is needed. Default: 0
     */
    private int borderEast = 0;

    /**
     * The width of the input field in pixels.
     */
    private int width = 1;
    /**
     * The height of the input field in pixels.
     */
    private int height = 1;

    /**
     * The custom sidebar for this input element. <code>null</code> if no custom
     * sidebar is available.
     */
    protected SidebarPanel sidebar;
    protected AbstractEntryRoot entry;

    /**
     * The log object.
     */
    protected static final Log log = LogFactory.getLog(AbstractInputElement.class);

    /**
     * Constructor.
     * <p>
     * Initialize the constraints.
     *
     * @param entry      The corresponding Entry object of the input field.
     * @param columnType The corresponding ColumnType of the input field.
     * @param width      The width of the input fields in pixels.
     * @param borderEast The size of the border in the east.
     */
    public AbstractInputConstraints(AbstractEntryRoot entry, ColumnType columnType, int width, int height, int borderEast) {
        this.width = width;
        this.height = height;
        this.borderEast = borderEast;
        this.columnType = columnType;
        this.entry = entry;
    }

    /**
     * Constructor.
     * <p>
     * Initialize the constraints. Use this constructor to display no border in the east.
     *
     * @param entry      The corresponding Entry object of the input field.
     * @param columnType The corresponding ColumnType of the input field.
     * @param width      The width of the input fields in pixels.
     */
    public AbstractInputConstraints(AbstractEntryRoot entry, ColumnType columnType, int width) {
        this(entry, columnType, width, 1, 0);
    }

    /**
     * Returns the border size in the east.
     *
     * @return The border size in the east.
     */
    public int getBorderEast() {
        return borderEast;
    }

    /**
     * Sets the border size in the east.
     *
     * @param borderEast The border size in the east.
     */
    public void setBorderEast(int borderEast) {
        this.borderEast = borderEast;
    }

    /**
     * Returns the width of the input field.
     *
     * @return The width of the input field.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the width of the input field.
     *
     * @param width The width of the input field.
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * Creates the raw input field.
     *
     * @return The raw input field.
     */
    protected abstract AbstractITRaw<T> createInputCustom();

    /**
     * Creates the raw input field. Creates it, if it does not exist yet.
     *
     * @return The raw input field.
     */
    public AbstractITRaw<T> createInput() {
        //saving not possible due to new rows otherwise being not created. If this behaviour is required, use other means...
        //input has to be created
        return createInputCustom();
    }

    /**
     * Returns the corresponding ColumnType of the input field.
     *
     * @return The corresponding ColumnType of the input field.
     */
    public ColumnType getColumnType() {
        return columnType;
    }

    /**
     * Returns the height of the input field.
     *
     * @return The heightof the input field.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the height of the input field.
     *
     * @param height The height of the input field.
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * Set the custom sidebar for this input element. <code>null</code> if no
     * custom sidebar is available..
     *
     * @param sidebar The custom sidebar of this input element.
     *                <code>null</code> if no custom sidebar is available.
     */
    public void setSidebar(AbstractBaseDependableEntry entry, SidebarPanel sidebar) {
        this.entry = entry;
        this.sidebar = sidebar;
    }

    /**
     * Set the custom sidebar for this input element. <code>null</code> if no
     * custom sidebar is available..
     *
     * @param sidebarText The sidebar text of this input element.
     */
    public void setSidebar(AbstractBaseDependableEntry entry, String sidebarText) {
        this.entry = entry;
        if (columnType != null) {
            this.sidebar = new SidebarEntryField(columnType, sidebarText);
        }
    }

    /**
     * Returns the current sidebar object.
     *
     * @return The current sidebar object.
     */
    public SidebarPanel getSidebar() {
        return sidebar;
    }

    public AbstractEntryRoot getEntry() {
        return entry;
    }
}
