package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.ITConstraintsHelper;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.CellConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.ITLabelConstraintsHelper;

import java.awt.*;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.JPanel;

/**
 * A Table Row is a JPanel that is used to assemble several cells to one row of the table.
 * <p>
 * Note: Use create() to create the graphical row.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITTableRow implements LayoutManager {

    /**
     * The basic panel that holds the cells.
     */
    private final JPanel rowPanel;
    /**
     * An array list that holds all cell information.
     */
    private final ArrayList<ITCell> allCells = new ArrayList<>();

    /**
     * An list of cells that have to be loaded and saved
     */
    private final ArrayList<ITCell> valueCells = new ArrayList<>();

    /**
     * Constructor.
     * <p>
     * Initializes the TableRow.
     */
    public ITTableRow() {
        rowPanel = new JPanel();
        rowPanel.setLayout(this);
    }

    /**
     * Add a new cell to the row.
     * <p>
     * Note:
     * - Is only added to the data, not to the GUI. Use create() to create the graphical row.
     * - Only use this method if no tablename definition is required (e.g. labels)
     *
     * @param c     The cell constraints
     * @param cells The cells to addCell.
     */
    public void addCell(CellConstraints c, ITLabelConstraintsHelper... cells) {
        allCells.add(new ITCell("not_defined", c, cells));
    }

    /**
     * Add a new cell to the row.
     * <p>
     * Note: Is only added to the data, not to the GUI. Use create() to create the graphical row.
     *
     * @param tableName The table name of the cells.
     * @param c         The cell constraints
     * @param cells     The cells to addCell.
     */
    public ITCell addCell(String tableName, CellConstraints c, ITConstraintsHelper... cells) {
        ITCell e = new ITCell(tableName, c, cells);
        allCells.add(e);
        valueCells.add(e);
        return e;
    }

    /**
     * Add a new cell to the row.
     * <p>
     * Note:
     * - Is only added to the data, not to the GUI. Use create() to create the graphical row.
     * - Only use this method if no tablename definition is required (e.g. labels)
     *
     * @param c       The cell constraints
     * @param maxRows The number of maximum rows.
     * @param cells   The cells to addCell.
     */
    public void addCell(CellConstraints c, int maxRows, ITConstraintsHelper... cells) {
        allCells.add(new ITCell("not_defined", c, maxRows, cells));
    }

    /**
     * Add a new cell to the row.
     * <p>
     * Note: Is only added to the data, not to the GUI. Use create() to create the graphical row.
     *
     * @param tableName The table name of the cells.
     * @param c         The cell constraints
     * @param maxRows   The number of maximum rows.
     * @param cells     The cells to addCell.
     */
    public void addCell(String tableName, CellConstraints c, int maxRows, ITConstraintsHelper... cells) {
        ITCell e = new ITCell(tableName, c, maxRows, cells);
        allCells.add(e);
        valueCells.add(e);
    }

    /**
     * Add a new cell to the row.
     * <p>
     * Note: Is only added to the data, not to the GUI. Use create() to create the graphical row.
     *
     * @param tableName       The table name of the cells.
     * @param c               The cell constraints
     * @param maxRows         The number of maximum rows.
     * @param isExtendedTable True if the table where the values are saved is used for multiple rows per entry
     * @param cells           The cells to addCell.
     */
    public void addCell(String tableName, CellConstraints c, int maxRows, boolean isExtendedTable,
                        ITConstraintsHelper... cells) {
        ITCell e = new ITCell(tableName, c, maxRows, cells, null, null, isExtendedTable);
        allCells.add(e);
        valueCells.add(e);
    }

    /**
     * Add a new cell to the row.
     * <p>
     * Note: Is only added to the data, not to the GUI. Use create() to create the graphical row.
     *
     * @param tableName     The table name of the cells.
     * @param c             The cell constraints
     * @param idColumn      The ID of the column (replaces Invisible Field)
     * @param idColumnValue The value of the ID Column (mostly an ID)
     * @param cells         The cells to addCell.
     */
    public void addCell(String tableName, CellConstraints c, ColumnType idColumn, String idColumnValue,
                        ITConstraintsHelper... cells) {
        ITCell cell = new ITCell(tableName, c, 1, cells, idColumn, idColumnValue, false);
        allCells.add(cell);
        valueCells.add(cell);

    }

    /**
     * Add a new cell to the row.
     * <p>
     * Note: Is only added to the data, not to the GUI. Use create() to create the graphical row.
     *
     * @param tableName       The table name of the cells.
     * @param c               The cell constraints
     * @param idColumn        The ID of the column (replaces Invisible Field)
     * @param idColumnValue   The value of the ID Column (mostly an ID)
     * @param isExtendedTable True if the table where the values are saved is used for multiple rows per entry
     * @param cells           The cells to addCell.
     */
    public void addCell(String tableName, CellConstraints c, ColumnType idColumn, String idColumnValue,
                        boolean isExtendedTable, ITConstraintsHelper... cells) {
        ITCell cell = new ITCell(tableName, c, 1, cells, idColumn, idColumnValue, isExtendedTable);
        allCells.add(cell);
        valueCells.add(cell);

    }

    /**
     * Add a new cell to the row.
     * <p>
     * Note: Is only added to the data, not to the GUI. Use create() to create the graphical row.
     *
     * @param tableName       The table name of the cells.
     * @param c               The cell constraints
     * @param maxRows         The number of maximum rows.
     * @param idColumn        The ID of the column (replaces Invisible Field)
     * @param idColumnValue   The value of the ID Column (mostly an ID)
     * @param isExtendedTable True if the table where the values are saved is used for multiple rows per entry
     * @param cells           The cells to addCell.
     */
    public void addCell(String tableName, CellConstraints c, int maxRows, ColumnType idColumn, String idColumnValue,
                        boolean isExtendedTable, ITConstraintsHelper... cells) {
        ITCell cell = new ITCell(tableName, c, maxRows, cells, idColumn, idColumnValue, isExtendedTable);
        allCells.add(cell);
        valueCells.add(cell);
    }

    /**
     * Add a new cell to the row.
     * <p>
     * Note: Is only added to the data, not to the GUI. Use create() to create the graphical row.
     *
     * @param tableName     The table name of the cells.
     * @param c             The cell constraints
     * @param maxRows       The number of maximum rows.
     * @param idColumn      The ID of the column (replaces Invisible Field)
     * @param idColumnValue The value of the ID Column (mostly an ID)
     * @param cells         The cells to addCell.
     */
    public void addCell(String tableName, CellConstraints c, int maxRows, ColumnType idColumn, String idColumnValue,
                        ITConstraintsHelper... cells) {
        ITCell cell = new ITCell(tableName, c, maxRows, cells, idColumn, idColumnValue);
        allCells.add(cell);
        valueCells.add(cell);
    }

    /**
     * Creates the graphical representation of the row and returns the JPanel object. Uses the data that has already
     * been inserted to the data in this object.
     *
     * @return The graphical represenation of the row as a JPanel object.
     */
    public JPanel create() {
        for (ITCell cell : allCells) {
//            if (cell.getCon instanceof XXX) {
//                continue;
//            }
            rowPanel.add(cell);
        }
        rowPanel.add(new Box.Filler(new Dimension(Integer.MAX_VALUE, 0), new Dimension(Integer.MAX_VALUE, 0),
                new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE)));
        return rowPanel;
    }

    // *******************************************************************
    // *******************************************************************
    // *******************************************************************

    //    @Override
    public void load(DataSetOld data) {
        for (ITCell cell : valueCells) {
            cell.load(data);
        }
    }

    //    @Override
    public void save(DataSetOld data) throws IsAMandatoryFieldException {
        for (ITCell cell : valueCells) {
            cell.save(data);
        }
    }

    //    @Override
    public void clear() {
        for (ITCell cell : valueCells) {
            cell.clear();
        }
    }

    //    @Override
    public void setFocus() {

    }

    //    @Override
    public boolean isValidInput() {
        for (ITCell cell : valueCells) {
            if (!cell.isValidInput()) {
                return false;
            }
        }
        return true;
    }

    //    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> elements = new ArrayList<>();
        for (ITCell cell : allCells) {
            elements.addAll(cell.getMyFocusableComponents());
        }
        return elements;
    }

    // *******************************************************************
    // *******************************************************************
    // *******************************************************************

    @Override
    public void addLayoutComponent(String name, Component comp) {

    }

    @Override
    public void removeLayoutComponent(Component comp) {

    }

    @Override
    public Dimension preferredLayoutSize(Container target) {
        synchronized (target.getTreeLock()) {
            Dimension dim = new Dimension(target.getWidth(), 0);
            ArrayList<ITCell> allCelles = this.allCells;
            for (ITCell cell : allCelles) {
                if (cell.isVisible()) {
                    dim.height = Math.max(cell.getGridY() * ITConstants.CELL_HEIGHT, dim.height);
                }
            }
//            dim.height += vgap;
            return dim;
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container target) {
        synchronized (target.getTreeLock()) {
            Dimension dim = new Dimension(target.getWidth(), 0);
            ArrayList<ITCell> allCelles = this.allCells;
            for (ITCell cell : allCelles) {
                if (cell.isVisible()) {
                    dim.height = Math.max(cell.getGridY() * ITConstants.CELL_HEIGHT, dim.height);
                }
            }
//            dim.height += vgap;
            return dim;
        }
    }

    @Override
    public void layoutContainer(Container target) {
        synchronized (target.getTreeLock()) {
            Insets insets = target.getInsets();
            int top = insets.top;
            int bottom = target.getHeight() - insets.bottom;
            int left = insets.left;
            int right = target.getWidth() - insets.right;

            int width = right - left;
            int gridX = 0;
            ArrayList<ITCell> allCelles = this.allCells;
            for (ITCell cell : allCelles) {
                if (cell.isVisible()) {
                    gridX += cell.getNumberOfGridsX();
                }
            }

            if (gridX == 0) {
                return;
            }

            double gridWith = width * 1.0 / gridX;

            double x = 0;
            int lastLeft = (int) (x + left + 0.5);

            for (ITCell cell : allCelles) {
                if (!cell.isVisible()) {
                    continue;
                }
                double cellWidth = (cell.getNumberOfGridsX() * gridWith);
                int newLeft = (int) ((left + x) + cellWidth + 0.5);
                cell.setBounds( lastLeft, top, newLeft - lastLeft,
                        cell.getGridY() * ITConstants.CELL_HEIGHT);
                x += cellWidth;
                lastLeft = newLeft;


//                long round = Math.round((left + x) + cellWidth);
//                long round1 = Math.round(left + x);
//                cell.setBounds((int) lastLeft, top, (int)(round - lastLeft),//(int)cellWidth,
//                        cell.getGridY() * ITConstants.CELL_HEIGHT);
//                x += cellWidth;
            }
        }
    }

}
