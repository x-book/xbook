package de.uni_muenchen.vetmed.xbook.api.framework.swing.raw;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XDateChooser;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.helper.StringHelper;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.abstracts.AbstractInputElementFunctions;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.miscellaneous.InputTwoDatesChooser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class RawTwoDatesChooser extends AbstractInputElementFunctions {

    public enum LabelType {

        FROM_TO, START_END, NONE
    }

    /**
     * Object to format the date.
     */
    protected final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * The correspondenting database column data of the second date field.
     */
    private final ColumnType columnTypeDate2;

    /**
     * The first date chooser object.
     */
    protected XDateChooser dateChooser1;
    /**
     * The second date chooser object.
     */
    protected XDateChooser dateChooser2;

    private JLabel label1;
    private JLabel label2;

    public RawTwoDatesChooser(ColumnType columnTypeDate1, ColumnType columnTypeDate2, LabelType labelType) {
        super(columnTypeDate1);
        this.columnTypeDate2 = columnTypeDate2;
        setLabel(labelType);
    }

    @Override
    public void createField() {
        setLayout(new GridLayout(1, 2, 3, 0));

        label1 = new JLabel("");
        label2 = new JLabel("");

        JPanel panel1 = new JPanel(new BorderLayout());
        dateChooser1 = new XDateChooser();
        dateChooser1.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        panel1.add(BorderLayout.WEST, label1);
        panel1.add(BorderLayout.CENTER, dateChooser1);
        add(panel1);

        JPanel panel2 = new JPanel(new BorderLayout());
        dateChooser2 = new XDateChooser();
        dateChooser2.getDateEditor().getUiComponent().setBorder(BorderFactory.createLineBorder(Colors.INPUT_FIELD_INPUT_BORDER, 1));
        panel2.add(BorderLayout.WEST, label2);
        panel2.add(BorderLayout.CENTER, dateChooser2);
        add(panel2);

//        if (columnType.isMandatory()) {
//            colorizeBackground(Colors.INPUT_FIELD_MANDATORY_BACKGROUND);
//        } else {
//            colorizeBackground(Colors.INPUT_FIELD_BACKGROUND);
//        }
        dateChooser1.getCalendarButton().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dateChooser1.getDateEditor().getUiComponent().requestFocus();
            }

        });
        dateChooser2.getCalendarButton().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dateChooser2.getDateEditor().getUiComponent().requestFocus();
            }

        });
    }

    @Override
    public void load(DataSetOld data) {
        DataRow list = data.getDataRowForTable(getTableName());
        final String s1 = list.get(columnType);
        final String s2 = list.get(columnTypeDate2);
        try {
            if (s1 != null && !s1.isEmpty()&&!columnTypeDate2.getDefaultValue().equals(s1)) {
                setDate1(s1);
            }
            if (s2 != null && !s2.isEmpty()&&!columnTypeDate2.getDefaultValue().equals(s2)) {
                setDate2(s2);
            }
        } catch (ParseException ex) {
            Logger.getLogger(InputTwoDatesChooser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void clear() {
        dateChooser1.setDate(null);
        dateChooser2.setDate(null);
    }

    @Override
    public void save(DataSetOld data) {
        DataRow row = data.getDataRowForTable(getTableName());
        row.add(new DataColumn(getDate1(), columnType.getColumnName()));
        row.add(new DataColumn(getDate2(), columnTypeDate2.getColumnName()));
    }

    @Override
    public String getStringRepresentation() {
        String s1 = "";
        if (dateChooser1.getDate() != null) {
            s1 += dateFormat.format(dateChooser1.getDate());
        }
        String s2 = "";
        if (dateChooser2.getDate() != null) {
            s2 += dateFormat.format(dateChooser2.getDate());
        }
        return s1 + "," + s2;
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.addAll(dateChooser1.getFocusableComponents());
        comp.addAll(dateChooser2.getFocusableComponents());
        return comp;
    }

    @Override
    public boolean isValidInput() {
        return dateChooser1.getDate() != null && dateChooser2.getDate() != null;
    }

    private Date getTime() {
        return new GregorianCalendar(2013, 12, 13).getTime();
    }




    /**
     * Sets the text of the two labels dependant of the overgiven LabelType.
     *
     * @param type The type of the labels to be set.
     */
    public void setLabel(LabelType type) {
        if (type == null) {
            type = LabelType.NONE;
        }

        switch (type) {
            case FROM_TO:
                setLabel1(Loc.get("FROM"));
                setLabel2(Loc.get("TO"));
                return;
            case START_END:
                setLabel1(Loc.get("START"));
                setLabel2(Loc.get("END"));
                return;
            case NONE:
            default:
                setLabel1("");
                setLabel2("");
                return;
        }
    }

    /**
     * Sets the text of the first label.
     *
     * @param text The text of the first label.
     */
    public void setLabel1(String text) {
        if (text == null || text.isEmpty()) {
            label1.setText("");
        } else {
            label1.setText(StringHelper.capitalize(text) + ":");
        }
    }

    /**
     * Sets the text of the second label.
     *
     * @param text The text of the second label.
     */
    public void setLabel2(String text) {
        if (text == null || text.isEmpty()) {
            label2.setText("");
        } else {
            label2.setText(text + ":");
        }
    }

    public XDateChooser getDateChooser1() {
        return dateChooser1;
    }

    public XDateChooser getDateChooser2() {
        return dateChooser2;
    }

    // ========================================================================
    public void setDate1(String date) throws ParseException {
        dateChooser1.setDate(dateFormat.parse(date));
    }

    public String getDate1() {
        if (dateChooser1.getDate() == null) {
            return "0000-00-00";
        }
        return dateFormat.format(dateChooser1.getDate());
    }

    public void setDate2(String date) throws ParseException {
        dateChooser2.setDate(dateFormat.parse(date));
    }

    public String getDate2() {
        if (dateChooser2.getDate() == null) {
            return "0000-00-00";
        }
        return dateFormat.format(dateChooser2.getDate());
    }

    @Override
    public void setFocus() {
        dateChooser1.requestFocusInWindow();
    }

}
