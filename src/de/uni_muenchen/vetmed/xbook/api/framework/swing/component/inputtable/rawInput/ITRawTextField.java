package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.AbstractInputConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.TextFieldConstraints;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.Sidebar;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * The raw text field.
 *
 * @param <C> The corresponding input constraints for the raw input field.
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITRawTextField<C extends TextFieldConstraints> extends AbstractITRaw<C> {

    private JTextField textField;

    public ITRawTextField(C constraints) {
        super(constraints);
    }

    @Override
    protected JComponent create() {
        JPanel wrapper = new JPanel(new BorderLayout());
        textField = new JTextField();
        textField.setBorder(new EmptyBorder(0, 5, 0, 5));
        wrapper.add(BorderLayout.CENTER, textField);
        return wrapper;
    }

    @Override
    public String getValue() {
        return textField.getText();
    }

    @Override
    public boolean isEmpty() {
        return textField.getText().isEmpty();
    }

    @Override
    public void setValue(String value) {
        if ("-1".equals(value)) {
            textField.setText("");
        } else {
            textField.setText(value);
        }
    }

    @Override
    public void clear() {
        textField.setText("");
    }

    @Override
    public boolean isValidInput() {
        return !getValue().isEmpty();
    }

    @Override
    public void actionOnFocusGain() {
        textField.setBackground(Colors.SIDEBAR_BACKGROUND);
    }

    @Override
    public void actionOnFocusLost() {
        super.actionOnFocusLost();
        textField.setBackground(Color.WHITE);
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textField);
        return comp;
    }

    @Override
    public String getStringRepresentation() {
        return getValue();
    }

    /**
     * Returns the textfield object.
     *
     * @return The textfield object.
     */
    public JTextField getTextField() {
        return textField;
    }

    @Override
    public void addSidebarFunktionality() {
        textField.addFocusListener(getSidebarFunctionality());
    }
}
