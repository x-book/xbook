package de.uni_muenchen.vetmed.xbook.api.framework.swing.stack;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataTableOld;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.event.EventRegistry;
import de.uni_muenchen.vetmed.xbook.api.event.ValueEventSender;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiTextField;
import de.uni_muenchen.vetmed.xbook.api.gui.sidebar.SidebarPanel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Sizes;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * A stack element that holds a text field.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StackMultiTextBox extends AbstractStackElement implements ValueEventSender {

    protected MultiTextField multiTextField;
    protected String tableName;

    public StackMultiTextBox(ColumnType columnType, String tableName, SidebarPanel sidebar) {
        super(columnType, columnType.getDisplayName(), sidebar);

        this.tableName = tableName;
        multiTextField.setSelectAllAndRemoveAllButtonVisible(false);
        multiTextField.getTextField().setSize(new Dimension(1, Sizes.INPUT_FIELD_HEIGHT));
        multiTextField.getTextField().setPreferredSize(new Dimension(1, Sizes.INPUT_FIELD_HEIGHT));
        multiTextField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                setDefaultStyle();
            }
        });
        addCustomSidebarFunctionality(multiTextField);
    }

    @Override
    public void clearInput() {
        multiTextField.clearInput();
    }

    @Override
    @Deprecated
    public String getValue() {
        // not in use
        return null;
    }



    @Override
    public JComponent getElement() {
        wrapper.setPreferredSize(new Dimension(Sizes.STACK_DEFAULT_WIDTH, Sizes.STACK_THREEQUARTER_HEIGHT));
        multiTextField = new MultiTextField(columnType.getDisplayName());
        return multiTextField;
    }

    @Override
    public void setMandatoryStyle() {
    }

    @Override
    public void setErrorStyle() {

    }

    @Override
    public void setDefaultStyle() {

    }

    @Override
    public void setEnabled(boolean b) {
        multiTextField.setEnabled(b);
    }

    @Override
    public void addKeyListener(KeyListener l) {
        multiTextField.addKeyListener(l);
    }

    @Override
    @Deprecated
    public void setValue(String value) {
        // not in use
    }

    @Override
    public void save(ProjectDataSet data) {
        if (columnType == null || tableName == null) {
            if (labelText == null) {
                throw new RuntimeException("AbstractStackElement: load-method is only supported if a valid ColumnType and a table name is available.");
            } else {
                throw new RuntimeException("AbstractStackElement (" + labelText + "): load-method is only supported if a valid ColumnType and a table name is available.");
            }
        }

        ArrayList<String> listItems = multiTextField.getListItems();
        DataTableOld dataTable = data.getOrCreateDataTable(tableName);
        for (String item : listItems) {
            DataRow row = new DataRow(tableName);
            row.put(columnType.getColumnName(), item);
            dataTable.add(row);
        }
    }

    @Override
    public void load(ProjectDataSet data) {
        if (columnType == null || tableName == null) {
            if (labelText == null) {
                throw new RuntimeException("AbstractStackElement: save-method is only supported if a valid ColumnType and a table name is available.");
            } else {
                throw new RuntimeException("AbstractStackElement (" + labelText + "): save-method is only supported if a valid ColumnType and a table name is available.");
            }
        }

        DataTableOld dataTable = data.getOrCreateDataTable(tableName);
        for (DataRow list : dataTable) {
            if (list.get(columnType) != null) {
                String s = list.get(columnType);
                multiTextField.addItemToList(s);
            }
        }
    }

    public MultiTextField getTextField() {
        return multiTextField;
    }

    @Override
    public void notifyUpdate() {
        EventRegistry.notifyListener(columnType);
    }
}
