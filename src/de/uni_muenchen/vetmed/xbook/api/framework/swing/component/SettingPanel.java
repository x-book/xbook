package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.settingPanel.AbstractSettingPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.settingPanel.NavigationElement;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class SettingPanel extends JPanel {

    private Color contentBackground = new Color(240, 240, 240);
    private final NavigationPosition NAVIGATION_POSITION;
    private JPanel navigationPanel;
    private JPanel contentPanel;
    private JPanel buttonPanel;
    private ArrayList<NavigationElement> arrayNavigationElements;
    private boolean isExtendedMode = false;
    private JButton cancelButton;
    private JButton saveCloseButton;
    private boolean isButtonPanelVisible = true;

    public enum NavigationPosition {

        WEST, NORTH, EAST, SOUTH
    }

    public SettingPanel() {
        this(NavigationPosition.WEST);
    }

    public SettingPanel(NavigationPosition navigationPosition) {
        this(NavigationPosition.WEST, null);
    }

    public SettingPanel(NavigationPosition navigationPosition, Color contentBackgroundColor) {
        this.NAVIGATION_POSITION = navigationPosition;
        if (contentBackgroundColor != null) {
            contentBackground = contentBackgroundColor;
        }

        arrayNavigationElements = new ArrayList<>();

        initializeLayout();
    }

    private void initializeLayout() {
        setLayout(new BorderLayout());
        setBorder(new EmptyBorder(0, 0, 0, 0));

        contentPanel = new JPanel(new BorderLayout());

        navigationPanel = new JPanel();
        navigationPanel.setBackground(Color.WHITE);

        switch (NAVIGATION_POSITION) {
            case WEST:
                navigationPanel.setLayout(new StackLayout());
                add(BorderLayout.WEST, navigationPanel);
                break;
            case EAST:
                navigationPanel.setLayout(new StackLayout());
                add(BorderLayout.EAST, navigationPanel);
                break;
            case NORTH:
                navigationPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
                add(BorderLayout.NORTH, navigationPanel);
                break;
            case SOUTH:
                navigationPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
                add(BorderLayout.SOUTH, navigationPanel);
                break;
        }

        setContent(null);

        add(BorderLayout.CENTER, contentPanel);
    }

    public void addNavigationElement(String label, AbstractSettingPanel panel) {
        addNavigationElement(label, panel, null);
    }

    public void addNavigationElement(String label, AbstractSettingPanel panel, ImageIcon icon) {
        NavigationElement element = new NavigationElement(this, label, panel, icon);
        navigationPanel.add(element);
        arrayNavigationElements.add(element);
        setContent(arrayNavigationElements.get(0).getContentPanel());
    }

    public void deselectAllNavigationElements() {
        for (NavigationElement element : arrayNavigationElements) {
            element.setActive(false);
        }
    }

    public void setContent(AbstractSettingPanel panel) {
        if (panel != null) {
            panel.setBackground(contentBackground);
        }
        contentPanel.removeAll();
        JScrollPane scroll = new JScrollPane(panel);
        scroll.getVerticalScrollBar().setUnitIncrement(16);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroll.setBorder(new EmptyBorder(0, 0, 0, 0));
        contentPanel.add(BorderLayout.CENTER, scroll);
        if (isButtonPanelVisible) {
            contentPanel.add(BorderLayout.SOUTH, getButtonPanel());
        }
        repaint();
        validate();
    }

    private JPanel getButtonPanel() {
        buttonPanel = new JPanel(new BorderLayout());

        JPanel buttonLine = new JPanel();

        JButton resetButton = new JButton("RESET");
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });
        buttonLine.add(resetButton);

        JButton defaultButton = new JButton("DEFAULT");
        defaultButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setDefaultValues();
            }
        });
        buttonLine.add(defaultButton);

        saveCloseButton = new JButton("SAVE & CLOSE");
        saveCloseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
            }
        });
        buttonLine.add(saveCloseButton);

        JButton applyButton = new JButton("SAVE");
        buttonLine.add(applyButton);

        cancelButton = new JButton("CANCEL");
        buttonLine.add(cancelButton);

        buttonPanel.add(BorderLayout.SOUTH, buttonLine);

        return buttonPanel;
    }

    public void save() {
        for (NavigationElement element : arrayNavigationElements) {
            element.getContentPanel().save();
        }
    }

    public void reset() {
        for (NavigationElement element : arrayNavigationElements) {
            element.getContentPanel().reset();
        }
    }

    public void setDefaultValues() {
        for (NavigationElement element : arrayNavigationElements) {
            element.getContentPanel().setDefaultValues();
        }
    }

    public void setExtendedMode(boolean isExtendedMode) {
        this.isExtendedMode = isExtendedMode;
    }

    public boolean isExtendedMode() {
        return isExtendedMode;
    }

    public void setButtonPanelVisible(boolean b) {
        this.isButtonPanelVisible = b;
        buttonPanel.setVisible(b);
    }

    public void setCloseButtonVisible(boolean b) {
        cancelButton.setVisible(b);
    }

    public void addActionListenerToAbortButton(ActionListener al) {
        cancelButton.addActionListener(al);
    }

    public void addActionListenerToSaveButton(ActionListener al) {
        saveCloseButton.addActionListener(al);
    }
}
