package de.uni_muenchen.vetmed.xbook.api.framework.swing.component;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XLabel;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StatusLabel extends XLabel {

    public enum Status {

        NO_VALUE_FOUND(Colors.INPUT_FIELD_BACKGROUND, Loc.get("NO_VALUES_FOUND")),
        DIFFERENT_VALUES_FOUND(new Color(239, 229, 188), "<b>" + Loc.get("DIFFERENT_VALUES_FOUND") + "</b>"),
        SAME_VALUES_FOUND(new Color(201, 214, 203), Color.BLACK, "<b>" + Loc.get("SAME_VALUES_FOUND") + "</b>"),
        NEW_VALUE_ENTERED(new Color(167, 182, 209), Color.BLACK, "<b>" + Loc.get("VALUES_OVERWRITTEN") + "</b>"),
        VALUE_DELETED(new Color(167, 182, 209), Color.BLACK, "<b>" + Loc.get("VALUES_DELETED") + "</b>"),
        NOT_CHECKED(new Color(141, 67, 148), Color.WHITE, "<b>" + Loc.get("NOT_CHECKABLE") + "</b>"),
        ERROR(Colors.INPUT_FIELD_ERROR_BACKGROUND, Color.BLACK, "<b>" + Loc.get("INVALID_VALUE") + "</b>"),
        MANDATORY_VALUE_MISSING(Colors.INPUT_FIELD_ERROR_BACKGROUND, Color.BLACK, "<b>" + Loc.get("MANDATORY_FIELD") + "</b>"),
        NOT_SUPPORTED(new Color(127, 127, 127), Color.BLACK, "<b>" + Loc.get("NOT_SUPPORTED") + "</b>");

        Color bgColor, fgColor;
        String txt;

        Status(Color bgColor, Color fgColor, String txt) {
            this.bgColor = bgColor;
            this.fgColor = fgColor;
            this.txt = txt;
        }

        Status(Color bgColor, String txt) {
            this(bgColor, Color.BLACK, txt);
        }

        public Color getBgColor() {
            return bgColor;
        }

    }

    Status currentStatus;

    public StatusLabel(Status status) {
        super("", SwingConstants.CENTER);

        setBorder(new EmptyBorder(0, 0, 0, 0));
        setBackground(Colors.INPUT_FIELD_BACKGROUND);
        setOpaque(true);
//        setPreferredSize(new Dimension(1, 16));

        setStatus(status);
    }

    public StatusLabel() {
        this(Status.DIFFERENT_VALUES_FOUND);
    }

    @Override
    public void setText(String text) {
        super.setText("<html><center>" + text + "</center></html>");
    }

    /**
     * Set the status to a new value
     *
     * @param status
     */
    public void setStatus(Status status) {
        setBackground(status.bgColor);
        setForeground(status.fgColor);
        setText(status.txt);
        this.currentStatus = status;
    }

    /**
     * Returns the current status.
     *
     * @return The current status.
     */
    public Status getCurrentStatus() {
        return currentStatus;
    }

}
