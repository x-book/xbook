package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.ITRawKey;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntryRoot;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.dependable.AbstractBaseDependableEntry;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 11.12.2017.
 */
public class KeyConstraints extends AbstractInputConstraints<KeyConstraints> {
    private final ColumnType columnDbID;

    public KeyConstraints(AbstractEntryRoot entry, ColumnType columnTypeKey, ColumnType columnDbID, int width, int height, int borderEast) {
        super(entry, columnTypeKey, width, height, borderEast);
        this.columnDbID = columnDbID;
    }

    public KeyConstraints(AbstractEntryRoot entry, ColumnType columnTypeKey, ColumnType columnDbID, int width) {
        super(entry, columnTypeKey, width);
        this.columnDbID = columnDbID;

    }

    @Override
    protected AbstractITRaw<KeyConstraints> createInputCustom() {
        return new ITRawKey<>(this);
    }
}
