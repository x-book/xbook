package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import javax.swing.border.EmptyBorder;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Fonts;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.MultiLineTextLabelTextArea;

/**
 * An object that inherits from the MultiLineTextLabel class.
 *
 * It has the same functions than a standard MultiLineTextLabel object, but is modified for a default style for xBook.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class XTextBlock extends MultiLineTextLabelTextArea {

    /**
     * Constructor.
     *
     * Set the design for the object.
     *
     * @param text The text that is displayed in the object.
     */
    public XTextBlock(String text) {
        super(text);

        setBackground(Colors.CONTENT_BACKGROUND);
        setBorder(new EmptyBorder(0, 0, Fonts.TEXT_PARAGRAPH_PADDING, 0));
    }
    
    
}
