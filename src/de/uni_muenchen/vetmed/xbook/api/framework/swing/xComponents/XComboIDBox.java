package de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.CodeTableHashMap;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.event.CodeTableListener;
import de.uni_muenchen.vetmed.xbook.api.framework.components.DataHolder;
import de.uni_muenchen.vetmed.xbook.api.gui.footer.Footer;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class XComboIDBox extends XComboBox<String> {

    private ColumnType columnType;
    private ApiControllerAccess apiControllerAccess;

    public XComboIDBox(ColumnType columnType, ApiControllerAccess apiControllerAccess) {
        this.columnType = columnType;
        this.apiControllerAccess = apiControllerAccess;
        //only autosort if there is no sort column
        setAutoSort(!columnType.isSort());

        setCustomInputEnabled(false);
        setIntegerInputEnabled(true);
        reloadAvailableValues();

        setSelectedItem("");
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (getSelectedItem() == null || getSelectedItem().toString().isEmpty()) {
                    setToolTipText(null);
                } else {
                    setToolTipText(getSelectedItem().toString());
                }
            }
        });
        addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
//                    actionOnFocusLost();
                }
            }
        });

        //
        apiControllerAccess.addCodeTableEventListener(new CodeTableListener() {
            @Override
            public void onCodeTablesUpdated() {
                reloadAvailableValues();
            }
        });
    }


    @Override
    protected void actionOnFocusLost(JTextField tf) {
        if (isIntegerInputEnabled()) {
            String input = tf.getText();
            try {
                //check if value is an integer
                Integer.parseInt(input);
                String value = DataHolder.getValueForInputID(columnType, input);
                if (value == null || value.isEmpty()) {
                    // setErrorStyle();
                    Footer.displayWarning(Loc.get("ID_NOT_FOUND"));
                }
                tf.setText(value);
                return;
            } catch (NumberFormatException nfe) {
                // do nothing
            }
        }
        super.actionOnFocusLost(tf);
    }

    public void reloadAvailableValues() {
        Object savedValue = getSelectedItem();
        CodeTableHashMap items = DataHolder.getData(columnType, apiControllerAccess);
        removeAllItems();

        ArrayList<String> listArray = new ArrayList<>();
//        if (addEmptyEntry) {
        listArray.add("");
//        }
        listArray.addAll(items.values());
        setItems(listArray);

        setSelectedItem(savedValue);
    }

    public void setSelectedId(String id) {
        if (id != null) {
            //combo.setSelectedItem(data.get(columnType).convertIdAsStringToString(id));
            setSelectedItem(DataHolder.getData(columnType, apiControllerAccess).convertIdAsStringToString(id));
        }
    }

    public String getSelectedId() {
        if (getSelectedItem() == null || getSelectedItem().equals("")) {
            return null;
        }
        return DataHolder.getData(columnType, apiControllerAccess).convertStringToIdAsString(getSelectedValue());
    }

    public String getSelectedValue() {
        return (String) getSelectedItem();
    }
}
