package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.AbstractITRaw;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput.ITRawInvisibleIdField;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntryRoot;

import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler
 */
public class InvisibleIdConstraints extends AbstractInputConstraints {

    private final int dbID;
    /**
     * Holds all input fields using this ID constraint.
     */
    ArrayList<ITRawInvisibleIdField> listOfRows = new ArrayList<>();

    /**
     * Constructor.
     * <p>
     * Initialize the constraints.
     *
     * @param entry      The corresponding Entry object of the input field.
     * @param columnType The corresponding ColumnType of the input field.
     */
    public InvisibleIdConstraints(AbstractEntryRoot entry, ColumnType columnType, int dbID) {
        // parameters "width", "height", and "borderEast" set to "0"
        // to avoid that the cell is displayed
        super(entry, columnType, 0, 0, 0);
        this.dbID = dbID;
    }

    @Override
    protected AbstractITRaw createInputCustom() {
        return new ITRawInvisibleIdField(this);
    }

    /**
     * Adds a new row to the list of rows.
     *
     * @param row The row to add.
     */
    public void addRow(ITRawInvisibleIdField row) {
        listOfRows.add(row);
    }

    /**
     * Removes a row from the list of rows.
     *
     * @param row The row to remove.
     */
    public void removeRow(ITRawInvisibleIdField row) {
        listOfRows.remove(row);
    }

    public Integer getNewNumber() {
        int id = 0;
        for (ITRawInvisibleIdField listOfRow : listOfRows) {
            if (listOfRow.getDBID() == dbID) {
                id = Math.max(id, listOfRow.getID());
            }
        }
        return id + 1;
    }

    public int getDBID() {
        return dbID;
    }
}
