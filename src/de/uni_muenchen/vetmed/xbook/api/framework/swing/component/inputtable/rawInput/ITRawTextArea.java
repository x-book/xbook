package de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.rawInput;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.field.TextAreaConstraints;
import de.uni_muenchen.vetmed.xbook.api.gui.stylesheet.Colors;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;

/**
 * The raw text field.
 *
 * @param <C> The corresponding input constraints for the raw input field.
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ITRawTextArea<C extends TextAreaConstraints> extends AbstractITRaw<C> {

    private JTextArea textArea;

    public ITRawTextArea(C constraints) {
        super(constraints);
    }

    @Override
    protected JComponent create() {
        JPanel wrapper = new JPanel(new BorderLayout());

        textArea = new JTextArea();
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setBorder(new EmptyBorder(0, 5, 0, 5));

        JScrollPane scroll = new JScrollPane();
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setViewportView(textArea);
        scroll.removeMouseWheelListener(scroll.getMouseWheelListeners()[0]);
        wrapper.add(BorderLayout.CENTER, scroll);

        return wrapper;
    }

    @Override
    public String getValue() {
        return textArea.getText();
    }

    @Override
    public boolean isEmpty() {
        return textArea.getText().isEmpty();
    }

    @Override
    public void setValue(String value) {
        textArea.setText(value);
    }

    @Override
    public void clear() {
        textArea.setText("");
    }

    @Override
    public boolean isValidInput() {
        // Always return true. Since this is should only be used as "
        return true;
    }

    @Override
    public void actionOnFocusGain() {
        textArea.setBackground(Colors.SIDEBAR_BACKGROUND);
    }

    @Override
    public void actionOnFocusLost() {
        super.actionOnFocusLost();
        textArea.setBackground(Color.WHITE);
    }

    @Override
    public ArrayList<Component> getMyFocusableComponents() {
        ArrayList<Component> comp = new ArrayList<>();
        comp.add(textArea);
        return comp;
    }

    @Override
    public String getStringRepresentation() {
        return getValue();
    }

    /**
     * Returns the textfield object.
     *
     * @return The textfield object.
     */
    public JTextArea getTextArea() {
        return textArea;
    }



    @Override
    public void addSidebarFunktionality() {
        textArea.addFocusListener(getSidebarFunctionality());
    }}
