package de.uni_muenchen.vetmed.xbook.api.framework.components;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.CodeTableHashMap;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.combos.InputComboIdBox;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataHolder {
    /**
     * The elements that are selectable in the combo box.
     */
    protected static HashMap<ColumnType, CodeTableHashMap> data = new HashMap<>();
    public static String getValueForInputID(ColumnType columnType, String id) {
        return data.get(columnType).get(id);
    }

    public static CodeTableHashMap getData(ColumnType columnType, ApiControllerAccess apiControllerAccess) {
        try {
            if (!data.containsKey(columnType)) {
                data.put(columnType, apiControllerAccess.getHashedCodeTableEntries(columnType));
            }
            return data.get(columnType);

        } catch (NotLoggedInException | StatementNotExecutedException ex) {
            Logger.getLogger(InputComboIdBox.class.getName()).log(Level.SEVERE, null, ex);
            return new CodeTableHashMap();
        }
    }


}
