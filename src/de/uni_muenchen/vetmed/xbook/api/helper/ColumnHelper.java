package de.uni_muenchen.vetmed.xbook.api.helper;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ColumnHelper {

    /**
     * Removed the database name from the columnName if it exists in the given
     * String.
     *
     * @param columnName The column name to remove the database name.
     * @return The column name without the database name.
     */
    public static String removeDatabaseName(String columnName) {
        if(columnName==null){
            return null;
        }
        return columnName.substring(columnName.lastIndexOf(".") + 1);
    }

    public static String getTableName(String columnName) {
        if (columnName == null||columnName.contains("(")) {
            return "";
        }
        int lastIndex = columnName.lastIndexOf(".");
        int firstIndex = columnName.indexOf(".");
        if (lastIndex == -1) {
            return "";
        }
        if (firstIndex != lastIndex) {
            return columnName.substring(firstIndex + 1, lastIndex);
        }
        return columnName.substring(0, lastIndex);
    }

}
