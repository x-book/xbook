package de.uni_muenchen.vetmed.xbook.api.helper;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.NotColorableJLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.NotColorableJPanel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.StatusLabel;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.documentHelper.LimitInputLengthDocument;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.modernCheckbox.ModernCheckbox;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XButton;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.xComponents.XTitle;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ComponentHelper {

    /**
     * Wraps a component with an empty border.
     *
     * @param comp              The component that should be wrapped.
     * @param backgroundColor   The backgroundColor for the JPanel.
     * @param emptyBorderTop    The size of the top empty border.
     * @param emptyBorderRight  The size of the right empty border.
     * @param emptyBorderBottom The size of the bottom empty border.
     * @param emptyBorderLeft   The size of the left empty border.
     * @return The wrapped component.
     */
    public static JPanel wrapComponent(JComponent comp, Color backgroundColor, int emptyBorderTop,
                                       int emptyBorderRight, int emptyBorderBottom, int emptyBorderLeft) {
        JPanel wrapper = new JPanel(new BorderLayout());
        if (backgroundColor != null) {
            wrapper.setBackground(backgroundColor);
        } else {
            wrapper.setOpaque(false);
        }
        wrapper.setBorder(BorderFactory.createEmptyBorder(emptyBorderTop, emptyBorderLeft, emptyBorderBottom,
                emptyBorderRight));
        wrapper.add(comp);
        return wrapper;
    }

    /**
     * Wraps a component with an empty border.
     *
     * @param comp              The component that should be wrapped.
     * @param emptyBorderTop    The size of the top empty border.
     * @param emptyBorderRight  The size of the right empty border.
     * @param emptyBorderBottom The size of the bottom empty border.
     * @param emptyBorderLeft   The size of the left empty border.
     * @return The wrapped component.
     */
    public static JPanel wrapComponent(JComponent comp, int emptyBorderTop, int emptyBorderRight,
                                       int emptyBorderBottom, int emptyBorderLeft) {
        return wrapComponent(comp, null, emptyBorderTop, emptyBorderRight, emptyBorderBottom, emptyBorderLeft);
    }

    /**
     * Wraps a component with an empty border.
     *
     * @param comp            The component that should be wrapped.
     * @param backgroundColor The backgroundColor for the JPanel.
     * @param emptyBorderSize The size of the empty border.
     * @return The wrapped component.
     */
    public static JPanel wrapComponent(JComponent comp, Color backgroundColor, int emptyBorderSize) {
        return wrapComponent(comp, backgroundColor, emptyBorderSize, emptyBorderSize, emptyBorderSize, emptyBorderSize);
    }

    /**
     * Wraps a component with an empty border.
     *
     * @param comp            The component that should be wrapped.
     * @param emptyBorderSize The size of the empty border.
     * @return The wrapped component.
     */
    public static JPanel wrapComponent(JComponent comp, int emptyBorderSize) {
        return wrapComponent(comp, null, emptyBorderSize, emptyBorderSize, emptyBorderSize, emptyBorderSize);
    }

    /**
     * Colorize all children of one container with a specific color.
     *
     * @param container          The parent container.
     * @param color              The color.
     * @param colorElementItself If <code>true</code> the container element will be coloured as well.
     */
    public static void colorAllChildren(Container container, Color color, boolean colorElementItself) {
        if (colorElementItself) {
            container.setBackground(color);
        }
        for (Component ck : container.getComponents()) {
            boolean allowRecolouring =
                    !(ck instanceof JTextField) && !(ck instanceof JList) && !(ck instanceof JTextArea)
                            && !(ck instanceof JComboBox) && !(ck instanceof XTitle) && !(ck instanceof XButton)
                            && !(ck instanceof StatusLabel) && !(ck instanceof ModernCheckbox)
                            && !(ck instanceof NotColorableJLabel) && !(ck instanceof NotColorableJPanel)
                            && !(ck instanceof JScrollPane);
            if (allowRecolouring) {
                ck.setBackground(color);
            }
            if (ck instanceof Container && allowRecolouring) {
                colorAllChildren((Container) ck, color);
            }
        }
        container.repaint();
    }

    /**
     * Colorize all children of one container with a specific color.
     *
     * @param container The parent container.
     * @param color     The color.
     */
    public static void colorAllChildren(Container container, Color color) {
        colorAllChildren(container, color, false);
    }

    public static void setMaximumInputLength(JTextField c, ColumnType columnType) {
        if (columnType == null || columnType.getMaxInputLength() == null) {
            c.setDocument(new LimitInputLengthDocument(9999));
            if (columnType != null && columnType.getMaxInputLength() == null) {
                System.out.println("Warning: No maximum input length defined in ColumnType of field '" + columnType.getDisplayName() + "'");
            }
        } else {
            c.setDocument(new LimitInputLengthDocument(columnType.getMaxInputLength()));
        }
    }

    public static List<Component> getAllComponents(final Container c) {
        Component[] comps = c.getComponents();
        List<Component> compList = new ArrayList<>();
        for (Component comp : comps) {
            compList.add(comp);
            if (comp instanceof Container) {
                compList.addAll(getAllComponents((Container) comp));
            }
        }
        return compList;
    }
//    public static void setMaximumInputLength(JTextArea c, ColumnType columnType) {
//        if (columnType == null || columnType.getMaxInputLength() == null) {
//            c.setDocument(new LimitInputLengthDocument(9999));
//            if (columnType != null && columnType.getMaxInputLength() == null) {
//                System.out.println("Warning: No maximum input length defined in ColumnType of field '" + columnType
//                .getDisplayName() + "'");
//            }
//        } else {
//            c.setDocument(new LimitInputLengthDocument(columnType.getMaxInputLength()));
//        }
//    }
}
