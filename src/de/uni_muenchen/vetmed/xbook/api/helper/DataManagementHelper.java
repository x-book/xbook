package de.uni_muenchen.vetmed.xbook.api.helper;

import de.uni_muenchen.vetmed.xbook.api.controller.ApiControllerAccess;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataColumn;
import de.uni_muenchen.vetmed.xbook.api.datatype.HierarchicData;
import de.uni_muenchen.vetmed.xbook.api.event.CodeTableListener;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class DataManagementHelper {

    /**
     * Converts the selected item to the correspondenting id from the DbDatahash object..
     * <p>
     * Returns null if the items was not found or the id is not a valid Integer.
     *
     * @param value The text value of the selected item.
     * @param data  The DbDataHash value of the elements that are displayed in the combo box.
     * @return The correspondenting id. <code>null</code> if the item was not found or the id is no valid Integer value.
     */
    public static Integer convertStringToIdAsInteger(String value, ArrayList<DataColumn> data) {
        for (DataColumn d : data) {
            if (d.getValue().equals(value) || d.getColumnName().equals(value)) {
                try {
                    return Integer.parseInt(d.getColumnName());
                } catch (NumberFormatException ex) {
                    return -1;
                }
            }
        }
        return -1;
    }

    public static String convertIdAsIntegerToString(Integer id, ArrayList<DataColumn> data) {
        for (DataColumn d : data) {
            if (d.getColumnName().equals(id + "")) {
                return d.getValue();
            }
        }
        return "";
    }

    public static DataColumn getDataColumnForId(String id, ArrayList<DataColumn> data) {
        for (DataColumn d : data) {
            if (d.getColumnName().equals(id)) {
                return d;
            }
        }
        return null;
    }

    public static String convertIdAsStringToString(String id, ArrayList<DataColumn> data) {
        for (DataColumn d : data) {
            if (d.getColumnName().equals(id)) {
                return d.getValue();
            }
        }
        return "";
    }

    public static String convertStringToIdAsString(String item, ArrayList<DataColumn> dataHash) {
        String id = null;
        for (DataColumn d : dataHash) {
            if (d.getValue().equals(item)) {
                id = d.getColumnName();
                break;
            }
        }
        return id;
    }

    public static String[] convertDbDataHashArrayListToStringArray(ArrayList<DataColumn> dataHash) {
        return convertDbDataHashArrayListToStringArray(dataHash, false);
    }

    public static String[] convertDbDataHashArrayListToStringArray(ArrayList<DataColumn> dataHash,
                                                                   boolean addEmptyEntry) {
        ArrayList<String> data = new ArrayList<>();
        if (addEmptyEntry) {
            data.add("");
        }
        for (DataColumn d : dataHash) {
            data.add(d.getValue());
        }
        return data.toArray(new String[0]);
    }

    private static HashMap<ColumnType, HashMap<String, HierarchicData>> hierarchicDataMap = new HashMap<>();
    private static HashMap<ColumnType, HashMap<String, String>> hierarchicCachedData = new HashMap<>();

    public static String getHierarchicString(ColumnType columnType, String id) {
        return getHierarchicString(columnType, id, false);
    }

    private static void addData(ColumnType multiTable) {
        HashMap<String, HierarchicData> data = null;
        try {
            data = controller.getHierarchicData(multiTable);
            hierarchicDataMap.put(multiTable, data);
        } catch (NotLoggedInException e) {
            e.printStackTrace();
        }
    }

    public static String getHierarchicString(ColumnType columnType, String id, boolean useHtml) {
        if (!hierarchicCachedData.containsKey(columnType)) {
            if (!hierarchicDataMap.containsKey(columnType)) {
                addData(columnType);
            }
            HashMap<String, HierarchicData> hd = hierarchicDataMap.get(columnType);

            // convert the parentId path to a readable string with values
            HashMap<String, String> v = new HashMap<>();
            final String ARROW = " > ";
            if (hd != null) {
                for (String key : hd.keySet()) {
                    String hierarchicValue = "";

                    HierarchicData h = hd.get(key);
                    boolean isFirst = true;
                    while (h != null) {
                        if (isFirst) {
                            hierarchicValue = "<b>" + h.getValue() + "</b>" + ARROW + hierarchicValue;
                        } else {
                            hierarchicValue = h.getValue() + ARROW + hierarchicValue;
                        }
                        HierarchicData temp = hd.get(h.getParentId());
                        // catch errors
                        if (temp == null && !h.getParentId().equals("0")) {
                            System.out.println("Error: Parent ID '" + h.getParentId() + "' does not exist in database" +
                                    ".");
                            hierarchicValue = "[patherror]" + ARROW + hierarchicValue;
                            break;
                        }
                        h = temp;
                        isFirst = false;
                    }
                    if (hierarchicValue.endsWith(ARROW)) {
                        hierarchicValue = hierarchicValue.substring(0, hierarchicValue.length() - ARROW.length());
                    }

                    hierarchicValue = "<html>" + hierarchicValue + "</html>";
                    v.put(key, hierarchicValue);
                }
            }
            hierarchicCachedData.put(columnType, v);

        }
        HashMap<String, String> data = hierarchicCachedData.get(columnType);
        if (!data.containsKey(id)) {
            return id;
        }

        String returnValue = hierarchicCachedData.get(columnType).get(id);
        if (!useHtml) {
            returnValue = returnValue.replace("<html>", "");
            returnValue = returnValue.replace("</html>", "");
            returnValue = returnValue.replace("<b>", "");
            returnValue = returnValue.replace("</b>", "");
        }
        return returnValue;
    }

    public static HashMap<ColumnType, HashMap<String, String>> getHierarchicCachedData() {
        return hierarchicCachedData;
    }

    public static HashMap<ColumnType, HashMap<String, HierarchicData>> getHierarchicDataMap() {
        return hierarchicDataMap;
    }

    private static ApiControllerAccess controller;

    public static void setController(ApiControllerAccess controller) {
        DataManagementHelper.controller = controller;
    }

    public static CodeTableListener getCodeTableListener() {
        return new CodeTableListener() {
            @Override
            public void onCodeTablesUpdated() {
                hierarchicCachedData.clear();
            }
        };
    }
}
