package de.uni_muenchen.vetmed.xbook.api.helper;

public enum DatabaseType {
    MYSQL, H2
}
