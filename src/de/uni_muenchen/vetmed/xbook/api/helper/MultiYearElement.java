package de.uni_muenchen.vetmed.xbook.api.helper;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class MultiYearElement {

    private final Integer begin;
    private final Integer end;

    public MultiYearElement(Integer begin, Integer end) {
        if (begin < end) {
            this.begin = begin;
            this.end = end;
        } else {
            this.begin = end;
            this.end = begin;
        }
    }

    public Integer getBegin() {
        return begin;
    }

    public Integer getEnd() {
        return end;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MultiYearElement)) {
            return false;
        }
        MultiYearElement mye = (MultiYearElement) obj;
        return begin.equals(mye.begin) && end.equals(mye.end);
    }

    public boolean intersects(MultiYearElement other) {
        return other.end >= begin && end >= other.begin;

    }

    @Override
    public String toString() {
        if (begin.equals(end)) {
            return begin + "";
        } else {
            return begin + " - " + end;
        }
    }

}
