package de.uni_muenchen.vetmed.xbook.api.helper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * A class that holds specific static methods for string-related operations.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class StringHelper {

    private static final CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder(); // or "ISO-8859-1" for ISO Latin 1
    private static final DecimalFormat digitGrouping = new DecimalFormat("###,###,###,###,###,###,###,###.##############");
    private static SecretKeySpec secretKeySpec = null;

    /**
     * Checks if a given String only contains chars that are ASCII, that means:
     * <p>
     * <code> !"#$%&'()*+,-./0123456789:;<=>?</code>
     * <code>@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_</code>
     * <code>`abcdefghijklmnopqrstuvwxyz{|}~</code>
     *
     * @param v The String to check
     * @return <code>true</code> if the String is pure ASCII, <code>false</code>
     * else.
     */
    public static boolean isPureAscii(String v) {
        return asciiEncoder.canEncode(v);
    }

    /**
     * Returns the String of all non-ASCII chars in the given string.
     *
     * @param input The String to check
     * @return A new String with all non-ASCII chars.
     */
    public static String getNonAsciiChars(String input) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (!isPureAscii(c + "")) {
                if (!sb.toString().contains(c + "")) {
                    sb.append(c);
                }
            }
        }
        return sb.toString();
    }

    /**
     * Converts a number into a formatted string adding thoudsands separators.
     * <p>
     * Examples: - Input 12345 will return "12,345"
     *
     * @param number The number to convert
     * @return The converted number.
     */
    public static String addThousandsSeparator(long number) {
        return digitGrouping.format(number);
    }

    /**
     * Sets first letter of a String to uppercase.
     *
     * @param string The string.
     * @return The capitalized string.
     */
    public static String capitalize(String string) {
        return Character.toString(string.charAt(0)).toUpperCase() + string.substring(1);
    }

    /**
     * Converts a char array to String.
     *
     * @param chars The char array that should be converted to a string.
     * @return The converted string.
     */
    public static String charArrayToString(char[] chars) {
        String str = "";
        for (char c : chars) {
            str += c;
        }
        return str;
    }

    /**
     * Creates the encoder key. Method won't create a new one, if one was
     * already created before.
     *
     * @return The enocder key.
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     */
    private static SecretKeySpec getDecoderKey() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if (secretKeySpec == null) {
            // The passworf of the key text
            String keyStr = "secret";
            // create byte array
            byte[] key = (keyStr).getBytes("UTF-8");
            // create a hash value of the array with MD5 or SHA
            MessageDigest sha = MessageDigest.getInstance("SHA-256");
            key = sha.digest(key);
            // only take the first 128 bits
            key = Arrays.copyOf(key, 16);
            // the final key
            secretKeySpec = new SecretKeySpec(key, "AES");
        }
        return secretKeySpec;
    }

    /**
     * Decodes a String, e.g. a password.
     *
     * @param text The string to encode.
     * @return The encoded string.
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public static String encodeString(String text) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        SecretKeySpec key = getDecoderKey();

        // Verschluesseln
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encrypted = cipher.doFinal(text.getBytes());

        // bytes zu Base64-String konvertieren (dient der Lesbarkeit)
        Base64 encoder = new Base64();
        String secret = encoder.encodeToString(encrypted);

        // Result
        // System.out.println("Encoded '" + text + "' to '" + geheim + "'.");
        return secret;
    }

    /**
     * Decodes a String, e.g. a password.
     *
     * @param secret The encoded string that should be decoded.
     * @return The decoded string.
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public static String decodeString(String secret) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        if (secret == null || secret.isEmpty()) {
            return "";
        }

        SecretKeySpec key = getDecoderKey();

        // BASE64 String zu Byte-Array konvertieren
        Base64 encoder = new Base64();
        byte[] crypted2 = encoder.decode(secret);

        // Decode
        Cipher cipher2 = Cipher.getInstance("AES");
        cipher2.init(Cipher.DECRYPT_MODE, key);
        byte[] cipherData2 = cipher2.doFinal(crypted2);
        String res = new String(cipherData2);

        // Plain text result
        // System.out.println("Decoded '" + secret + "' to '" + res + "'.");
        return res;
    }

    /**
     * Checks if a String is an Integer value.
     *
     * @param s The String to check.
     * @return <code>true</code> if the String is an Integer value,
     * <code>false</code> else
     */
    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Converts a String to an Integer value.
     *
     * @param s The String to convert.
     * @return The value as an Integer value. <code>null</code> if the value is
     * no Integer value.
     */
    public static Integer parseInteger(String s) {
        if (isInteger(s)) {
            return Integer.parseInt(s);
        }
        return null;
    }

    /**
     * Cut of a specific string from the end of another string. Returns the
     * original String, if it does not end with the chars.
     *
     * @param string The string that should be cut.
     * @param chars The chars to be cut.
     * @return The cutted String.
     */
    public static String cutString(String string, String chars) {
        if (string.endsWith(chars)) {
            string = string.substring(0, string.length() - chars.length());
        }
        return string;
    }

    public static String StringFromArray(ArrayList<String> periods, String s) {
        String str = "";
        for (String period : periods) {
            str += period + s;
        }

        return cutString(str, s);

    }

    public static String addLeadingZeros(Integer number, int leadingZeros) {
        return String.format("%0" + leadingZeros + "d", number);
    }

    public static String addLeadingZeros(String number, int leadingZeros) {
        return String.format("%0" + leadingZeros + "d", Integer.parseInt(number));
    }

    /**
     * Converts a date format of YYYY-MM-DD to DD.MM.YYYY.
     *
     * @param date The date in the format YYYY-MM-DD
     * @return The date in the format DD.MM.YYYY
     */
    public static String convertDate(String date) {
        String[] dateArray = date.split("-");

        String s = "Invalid Date";
        if (dateArray[0].equals("0000") || dateArray[0].equals("000") || dateArray[0].equals("00") || dateArray[0].equals("0")) {
            return s;
        }
        s = StringHelper.addLeadingZeros(dateArray[0], 4);
        if (dateArray[1].equals("00") || dateArray[1].equals("0")) {
            return s;
        }
        s = StringHelper.addLeadingZeros(dateArray[1], 2) + "." + s;
        if (dateArray[2].equals("00") || dateArray[2].equals("0")) {
            return s;
        }
        s = StringHelper.addLeadingZeros(dateArray[2], 2) + "." + s;

        return s;
    }
}
