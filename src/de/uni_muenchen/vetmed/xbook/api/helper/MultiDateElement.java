package de.uni_muenchen.vetmed.xbook.api.helper;

import de.uni_muenchen.vetmed.xbook.api.datatype.CustomDate;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class MultiDateElement {

    private final CustomDate begin;
    private final CustomDate end;

    public MultiDateElement(CustomDate date) {
        this(date, date);
    }

    public MultiDateElement(CustomDate begin, CustomDate end) {
        if (begin.toIntegerValue() < end.toIntegerValue()) {
            this.begin = begin;
            this.end = end;
        } else {
            this.begin = end;
            this.end = begin;
        }
    }

    public CustomDate getBegin() {
        return begin;
    }

    public CustomDate getEnd() {
        return end;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MultiDateElement)) {
            return false;
        }
        MultiDateElement mde = (MultiDateElement) obj;
        return begin.equals(mde.begin) && end.equals(mde.end);
    }

    public boolean intersects(MultiDateElement other) {
        return other.end.toIntegerValue() >= begin.toIntegerValue() && end.toIntegerValue() >= other.begin.toIntegerValue();
    }

    @Override
    public String toString() {
        if (begin.equals(end)) {
            return begin.parseToReadableDate() + "";
        } else {
            return begin.parseToReadableDate() + " - " + end.parseToReadableDate();
        }
    }

}
