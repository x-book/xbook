package de.uni_muenchen.vetmed.xbook.api.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * A class that holds specific static methods for date-related operations.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class DateHelper {

    /**
     * Checks if a String is a valid date defined by a specific date format.
     *
     * @param dateToValidate The String to check.
     * @param dateFormat The date format to check. E.g. "dd.MM.yyyy".
     * @return <code>true</code> if the string matches with the date formale,
     * <code>false</code> else.,
     */
    public static boolean isDateFormat(String dateToValidate, String dateFormat) {
        if (dateToValidate == null) {
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);
        try {
            sdf.parse(dateToValidate);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
}
