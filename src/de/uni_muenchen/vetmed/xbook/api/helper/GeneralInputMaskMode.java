package de.uni_muenchen.vetmed.xbook.api.helper;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public enum GeneralInputMaskMode {

    SINGLE, MULTI_EDIT
}
