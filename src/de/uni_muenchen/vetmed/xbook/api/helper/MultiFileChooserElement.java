package de.uni_muenchen.vetmed.xbook.api.helper;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class MultiFileChooserElement {

    private final String fileName;
    private final String fileEncoded;

    public MultiFileChooserElement(String fileName, String fileEncoded) {
        this.fileName = fileName;
        this.fileEncoded = fileEncoded;
    }

    public String getFileEncoded() {
        return fileEncoded;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public String toString() {
        return fileName;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MultiFileChooserElement)) {
            return false;
        }
        MultiFileChooserElement mfce = (MultiFileChooserElement) obj;
        return (fileName.equals(mfce.fileName) && fileEncoded.equals(mfce.fileEncoded));
    }

}
