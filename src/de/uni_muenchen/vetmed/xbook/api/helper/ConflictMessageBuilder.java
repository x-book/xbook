package de.uni_muenchen.vetmed.xbook.api.helper;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLine;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.SolveConflictLineGroup;
import de.uni_muenchen.vetmed.xbook.api.Loc;

import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ConflictMessageBuilder {

    private static final String css_line = "margin-top: 4px; padding: 2px 0 4px 0; ";
    private static final String css_conflicted_header = "margin-top: 20px; padding: 4px 6px; border: 1px solid black; background: black; color: white; font-weight: bold;";
    private static final String css_conflicted_line = "border: 1px solid black; border-top: 0;";

    public static String getMessageSolved(String projectName, String userName, ArrayList<SolveConflictLineGroup> conflictTable) {
        String message = Loc.get("MAIL_CONFLICT_NOTIFICATION_1", projectName, userName) + "\n\n";
        message += Loc.get("MAIL_CONFLICT_NOTIFICATION_5") + "\n\n";
        message += Loc.get("MAIL_CONFLICT_NOTIFICATION_3") + "\n\n";
        message += getConflictDataAsString(conflictTable, true) + "\n\n";
        message += Loc.get("MAIL_CONFLICT_NOTIFICATION_4") + "\n";
        return message;
    }

    public static String getMessageNotify(String projectName, String userName, ArrayList<SolveConflictLineGroup> conflictTable, boolean printSelected, String userInput) {
        String message = Loc.get("MAIL_CONFLICT_NOTIFICATION_1", projectName, userName) + "\n\n";
        message +=Loc.get("MAIL_CONFLICT_NOTIFICATION_2") +"\n";
        message += '"' + userInput + '"' + "\n\n";
        message += Loc.get("MAIL_CONFLICT_NOTIFICATION_3") + "\n\n";
        message += getConflictDataAsString(conflictTable, printSelected) + "\n\n";
        message += Loc.get("MAIL_CONFLICT_NOTIFICATION_4") + "\n";
        return message;
    }

    public static String getMessageSolvedHtml(String projectName, String userName, ArrayList<SolveConflictLineGroup> conflictTable) {
        StringBuilder sb = new StringBuilder();
        sb.append(getHtmlHead());
        sb.append(getHtmlTextLine(Loc.get("MAIL_CONFLICT_NOTIFICATION_1", "<strong>" + projectName + "</strong>", "<strong>" + userName + "</strong>")));
        sb.append(getHtmlTextLine("<strong><span style=\"font-size: large;\">" + Loc.get("MAIL_CONFLICT_NOTIFICATION_5") + "</span></strong>"));
        sb.append(getHtmlTextLine(Loc.get("MAIL_CONFLICT_NOTIFICATION_3")));
        sb.append(getConflictDataAsHtml(conflictTable, true));
        sb.append(getHtmlTextLine(Loc.get("MAIL_CONFLICT_NOTIFICATION_4")));
        sb.append(getHtmlTextLine("&nbsp;"));
        sb.append(getHtmlFoot());
        return sb.toString();
    }

    public static String getMessageNotifyHtml(String projectName, String userName, ArrayList<SolveConflictLineGroup> conflictTable, boolean printSelected, String userInput) {
        StringBuilder sb = new StringBuilder();
        sb.append(getHtmlHead());
        sb.append(getHtmlTextLine(Loc.get("MAIL_CONFLICT_NOTIFICATION_1", "<strong><span style=\"font-size: large;\">" + projectName + "</strong>", "<strong>" + userName + "</span></strong>")));
        sb.append(getHtmlTextLine("<strong>" + Loc.get("MAIL_CONFLICT_NOTIFICATION_2") + "</strong>"));
        sb.append(getHtmlTextLine('"' + userInput + '"'));
        sb.append(getHtmlTextLine(Loc.get("MAIL_CONFLICT_NOTIFICATION_3")));
        sb.append(getConflictDataAsHtml(conflictTable, printSelected));
        sb.append(getHtmlTextLine(Loc.get("MAIL_CONFLICT_NOTIFICATION_4")));
        sb.append(getHtmlFoot());
        return sb.toString();
    }

    private static String getConflictDataAsString(ArrayList<SolveConflictLineGroup> lines, boolean printSelected) {
        final String SEPARATOR = "=====================================================\n";
        final String SEPARATOR2 = "-----------\n";
        StringBuilder data = new StringBuilder(SEPARATOR);
        for (SolveConflictLineGroup group : lines) {
            data.append("|| " + group.getTableName().toUpperCase() + "\n");
            data.append(SEPARATOR);
            for (SolveConflictLine line : group.getLines()) {
                data.append(solveConflictLineToString(line, printSelected));
                data.append(SEPARATOR2);
            }
            if (data.toString().endsWith(SEPARATOR2)) {
                data.delete(data.length() - SEPARATOR2.length(), data.length());
            }
            data.append(SEPARATOR);
        }
        return data.toString();
    }

    private static String getConflictDataAsHtml(ArrayList<SolveConflictLineGroup> lines, boolean printSelected) {
        StringBuilder data = new StringBuilder();
        for (SolveConflictLineGroup group : lines) {
            data.append(getHtmlConflictedHeader(group.getTableName().toUpperCase()));
            data.append("<table style=\"width:100%; border-spacing: 0; border-collapse: collapse;\">");
            for (SolveConflictLine line : group.getLines()) {
                data.append(solveConflictLineToHtml(line, printSelected));
            }
            data.append("</table>");
        }
        return data.toString();
    }

    private static String getHtmlTextLine(String text) {
        return "<div style=\"" + css_line + "\">" + text + "</div>";
    }

    private static String getHtmlConflictedHeader(String text) {
        return "<div style=\"" + css_conflicted_header + "\">" + text + "</div>";
    }

    private static String getHtmlConflictedLine(String text) {
        return "<div style=\"" + css_conflicted_line + "\">" + text + "</div>";
    }

    private static String getHtmlHead() {
        return "<html><body>";
    }

    private static String getHtmlFoot() {
        return "</body></html>";
    }

    public static String solveConflictLineToString(SolveConflictLine solveConflictLine, boolean printSelected) {
        final String SEPARATOR = "##################";
        StringBuilder data = new StringBuilder();
        for (int i = 0; i < solveConflictLine.getTableName().size(); i++) {
            data.append(solveConflictLine.getDescription(i) + ": ");
            String localValue = solveConflictLine.getAllLocalDisplayValues().get(i);
            String serverValue = solveConflictLine.getAllServerDisplayValues().get(i);
            if (localValue.equals(serverValue)) {
                data.append(localValue + "\n");
            } else {
                data.append("\n");
                data.append("Server: " + serverValue);
                if (printSelected && solveConflictLine.isSelectedGlobal()) {
                    data.append(" (SELECTED)");
                }
                data.append("\n");

                data.append("Client: " + localValue);
                if (printSelected && solveConflictLine.isSelectedLocal()) {
                    data.append(" (SELECTED)");
                }
                data.append("\n");
            }
            if (i < solveConflictLine.getTableName().size() - 1) {
                data.append(SEPARATOR + "\n");
            }
        }
        return data.toString();
    }

    public static String solveConflictLineToHtml(SolveConflictLine solveConflictLine, boolean printSelected) {
        StringBuilder data = new StringBuilder();
        for (int i = 0; i < solveConflictLine.getTableName().size(); i++) {
            String displayName = solveConflictLine.getDescription(i);
            String localValue = solveConflictLine.getAllLocalDisplayValues().get(i);
            String globalValue = solveConflictLine.getAllServerDisplayValues().get(i);

            data.append(bla2(displayName, localValue, printSelected && solveConflictLine.isSelectedLocal(), globalValue, printSelected && solveConflictLine.isSelectedGlobal()));
        }
        return data.toString();
    }

    private static String bla2(String displayName, String localValue, boolean localSelected, String globalValue, boolean globalSelected) {
        boolean isEqual = localValue.equals(globalValue);
        String data = "<tr>";

        String bgLocal = (localSelected && !isEqual ? "background-color: lightgray; font-weight: bold;" : "");
        String bgDisplayName = (!isEqual ? "background-color: lightgray; font-weight: bold;" : "");
        String bgGlobal = (globalSelected && !isEqual ? "background-color: lightgray; font-weight: bold;" : "");

        data += "<td style=\"padding: 4px 6px; width: 30%; text-align: right; border: 1px solid black; border-right: 0; " + bgLocal + "\">" + localValue + "</td>";
        data += "<td style=\"padding: 4px 6px; width: 30%; text-align: center; border-top: 1px solid black; border-bottom: 1px solid black;" + bgDisplayName + "\">" + displayName + "</td>";
        data += "<td style=\"padding: 4px 6px; width: 30%; text-align: left; border: 1px solid black; border-left: 0;" + bgGlobal + "\">" + globalValue + "</td>";

        data += "</tr>";
        return data;
    }
}
