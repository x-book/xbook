package de.uni_muenchen.vetmed.xbook.api.helper;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class EntryModeHelper {

    public static GeneralInputMaskMode generalMode = GeneralInputMaskMode.SINGLE;

    public static void setToSingleEntryMode() {
        generalMode = GeneralInputMaskMode.SINGLE;
    }

    public static void setToMultiEntryMode() {
        generalMode = GeneralInputMaskMode.MULTI_EDIT;
    }

    public static boolean isSingleEntryMode() {
        return generalMode == GeneralInputMaskMode.SINGLE;
    }

    public static boolean isMultiEntryMode() {
        return generalMode == GeneralInputMaskMode.MULTI_EDIT;
    }

    public static GeneralInputMaskMode getEntryMode() {
        return generalMode;
    }

}
