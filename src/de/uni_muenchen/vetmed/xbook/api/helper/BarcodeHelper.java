package de.uni_muenchen.vetmed.xbook.api.helper;

import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.AbstractBarcodeSearch;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class BarcodeHelper {

    public static BarcodeWrapper getBarcode128B(String text) {
        try {
            return getBarcode(BarcodeFactory.createCode128B(text));

        } catch (BarcodeException ex) {
            Logger.getLogger(AbstractBarcodeSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public static BarcodeWrapper getBarcode128(String text) {
        try {
            return getBarcode(BarcodeFactory.createCode128(text));
        } catch (BarcodeException ex) {
            Logger.getLogger(AbstractBarcodeSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static BarcodeWrapper getBarcode(Barcode barcode) {

        barcode.setBarWidth(1);
        barcode.setBarHeight(40);
        barcode.setFont(new Font("Sans-serif", Font.PLAIN, 12));

        // add the barcode to a panel and return it
        return new BarcodeWrapper(barcode);
    }

    public static class BarcodeWrapper extends JPanel {
        Barcode barcode;

        public BarcodeWrapper(Barcode barcode) {
            this.barcode = barcode;

            setLayout(new BorderLayout());
            add(BorderLayout.CENTER, barcode);
        }

        public Barcode getBarcode() {
            return barcode;
        }

        public void updateBarcode(Barcode newBarcode) {
            remove(barcode);
            if (newBarcode != null) {
                barcode = newBarcode;
                barcode.setBarWidth(1);
                barcode.setBarHeight(40);
                barcode.setFont(new Font("Sans-serif", Font.PLAIN, 12));
                add(BorderLayout.CENTER, barcode);
            }
            revalidate();
            repaint();
        }
    }

}
