package de.uni_muenchen.vetmed.xbook.api.helper;

import de.uni_muenchen.vetmed.xbook.api.Loc;
import de.uni_muenchen.vetmed.xbook.api.datatype.UniqueArrayList;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.ArrayList;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@cesonia.io>
 */
public class FileTypeHelper {

    public static enum FileTypes {
        _3D_PLY, CSV, DAT, DWG_DWF, GEOFIFF, GIF, JPG, JPEG, JPG_JPEG,
        NEF, PDF, PNG, RAW, RAW_GENERAL, TIF, TIFF, TIF_TIFF, XLS, XLSX,
        XLS_XLSX
    }

    public static ArrayList<FileNameExtensionFilter> getFileFilter(FileTypes... fileTypes) {
        return FileTypeHelper.getFileFilter(false, fileTypes);
    }

    public static ArrayList<FileNameExtensionFilter> getFileFilter(boolean addAll, FileTypes... fileTypes) {
        UniqueArrayList<String> allExtensions = new UniqueArrayList<>();

        ArrayList<FileNameExtensionFilter> filters = new ArrayList<>();
        for (FileTypes fileType : fileTypes) {
            switch (fileType) {
                case _3D_PLY:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>" + Loc.get("3D_IMAGE") + " (<b>.ply</b>, <b>.obj</b>)</body></html>",
                            "ply", "obj"));
                    allExtensions.add("ply");
                    allExtensions.add("obj");
                    break;
                case CSV:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>Comma Separated Values (<b>.csv</b>)</body></html>",
                            "csv"));
                    allExtensions.add("csv");
                    break;
                case JPG:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>JPEG (<b>.jpg</b>)</body></html>",
                            "jpg"));
                    allExtensions.add("jpg");
                    break;
                case DWG_DWF:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>AutoCAD (<b>.dwg</b>, <b>.dwf</b>)</body></html>",
                            "dwg", "dwf"));
                    allExtensions.add("dwg");
                    allExtensions.add("dwf");
                    break;
                case DAT:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>" + Loc.get("DATA_FILE") + "(<b>.dat</b>)</body></html>",
                            "dat"));
                    allExtensions.add("dat");
                    break;
                case GEOFIFF:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>GeoTIFF (<b>.tiff</b>, <b>.tfw</b>)</body></html>",
                            "tiff", "tfw"));
                    allExtensions.add("tiff");
                    allExtensions.add("tfw");
                    break;
                case GIF:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>GIF (<b>.gif</b>)</body></html>",
                            "gif"));
                    allExtensions.add("gif");
                    break;
                case JPEG:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>JPEG (<b>.jpeg</b>)</body></html>",
                            "jpeg"));
                    allExtensions.add("jpeg");
                    break;
                case JPG_JPEG:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>JPEG (<b>.jpg</b>, <b>.jpeg</b>)</body></html>",
                            "jpg", "jpeg"));
                    allExtensions.add("jpg");
                    allExtensions.add("jpeg");
                    break;
                case NEF:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>Nikon Raw Image (<b>.nef</b>)</body></html>",
                            "nef"));
                    allExtensions.add("nef");
                    break;
                case PDF:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>PDF (<b>.pdf</b>)</body></html>",
                            "pdf"));
                    allExtensions.add("pdf");
                    break;
                case PNG:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>PDG (<b>.png</b>)</body></html>",
                            "png"));
                    allExtensions.add("png");
                    break;
                case RAW:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>" + Loc.get("RAW_FILE") + " (<b>.raw</b>)</body></html>",
                            "raw"));
                    allExtensions.add("raw");
                    break;
                case RAW_GENERAL:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>" + Loc.get("RAW_FILES") + " (<b>.nef</b>, <b>.raw</b>)</body></html>",
                            "nef", "raw"));
                    allExtensions.add("nef");
                    allExtensions.add("raw");
                    break;
                case TIF:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>TIFF (<b>.tif</b>)</body></html>",
                            "tif"));
                    allExtensions.add("tif");
                    break;
                case TIFF:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>TIFF (<b>.tiff</b>)</body></html>",
                            "tiff"));
                    allExtensions.add("tiff");
                    break;
                case TIF_TIFF:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>TIFF (<b>.tif</b>, <b>.tiff</b>)</body></html>",
                            "tif", "tiff"));
                    allExtensions.add("tif");
                    allExtensions.add("tiff");
                    break;
                case XLS:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>Excel 97-2003 (<b>.xls</b>)</body></html>",
                            "xls"));
                    allExtensions.add("xls");
                    break;
                case XLSX:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>Excel (<b>.xlsx</b>)</body></html>",
                            "xlsx"));
                    allExtensions.add("xlsx");
                    break;
                case XLS_XLSX:
                    filters.add(new FileNameExtensionFilter(
                            "<html><body>Excel (<b>.xls</b>, <b>.xlsx</b>)</body></html>",
                            "xls", "xlsx"));
                    allExtensions.add("xls");
                    allExtensions.add("xlsx");
                    break;
            }
        }

        if (addAll) {
            String s = "";
            for (String ext : allExtensions) {
                s += "<b>." + ext + "</b>, ";
            }
            s = s.substring(0, s.length() - 2);

            FileNameExtensionFilter allFilter = new FileNameExtensionFilter(
                    "<html><body>" + Loc.get("ALL") + " (" + s + ")</body></html>",
                    allExtensions.toArray(new String[allExtensions.size()]));

            filters.add(0, allFilter);
        }

        return filters;
    }
}
