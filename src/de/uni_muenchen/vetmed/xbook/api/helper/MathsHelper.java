package de.uni_muenchen.vetmed.xbook.api.helper;

import de.uni_muenchen.vetmed.xbook.api.exception.TooManyDigitsException;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * A class that holds specific static methods for mnathematic-related
 * operations.
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class MathsHelper {

    /**
     * Rounds the float field to a specific number of digits after the decimal
     * point. The value is taken from the <code>digitsAfterDecimalPoint</code>
     * parameter. <code>null</code> means infinite digits.
     *
     * @param number The float value to round.
     * @param digitsAfterDecimalPoint The number of digits after the decimal
     * point or <code>null</code>
     * @return The rounded float value.
     * @throws TooManyDigitsException if number has more digits that 7 (comma
     * and points ignored)
     */
    public static float roundFloat(String number, Integer digitsAfterDecimalPoint) throws TooManyDigitsException {
        // 7 digits allowsed for floats (ignore comma or point.
        if (number.replace(",", "").replace(".", "").length() >= 7) {
            throw new TooManyDigitsException();
        }

        if (digitsAfterDecimalPoint == null) {
            return Float.parseFloat(number);
        }
        BigDecimal rounded = new BigDecimal(number);
        float output = rounded.setScale(digitsAfterDecimalPoint, RoundingMode.HALF_UP).floatValue();
        return output;
    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
