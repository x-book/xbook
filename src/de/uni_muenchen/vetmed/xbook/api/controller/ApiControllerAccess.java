package de.uni_muenchen.vetmed.xbook.api.controller;

import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.database.xResultSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.*;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Group;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.Rank;
import de.uni_muenchen.vetmed.xbook.api.datatype.right.RightsInformation;
import de.uni_muenchen.vetmed.xbook.api.event.CodeTableListener;
import de.uni_muenchen.vetmed.xbook.api.event.LoginListener;
import de.uni_muenchen.vetmed.xbook.api.event.ProjectListener;
import de.uni_muenchen.vetmed.xbook.api.exception.*;
import de.uni_muenchen.vetmed.xbook.api.helper.DatabaseType;
import de.uni_muenchen.vetmed.xbook.api.plugin.PluginDatamanager;

import java.util.*;
import java.util.prefs.Preferences;

/**
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface ApiControllerAccess {

    /**
     * Returns whether a project is loaded or not.
     *
     * @return Whether a project is loaded or not.
     */
    boolean isProjectLoaded();

    HashMap<IBaseManager, TreeSet<ColumnType>> getAvailableExportEntries() throws StatementNotExecutedException, NotLoggedInException;

    /**
     * Returns the status if the user is admin or not.
     *
     * @return <code>true</code> if the current user is * * * * * * * * admin,
     * <code>false</code> else.
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NotLoggedInException          If the user is not logged in.
     */
    boolean isAdmin() throws StatementNotExecutedException, NotLoggedInException;

    boolean isDeveloper() throws StatementNotExecutedException, NotLoggedInException;

    boolean hasEditRights() throws NotLoggedInException, NotLoadedException, StatementNotExecutedException;

    /**
     * Saves the project with the specified data.
     *
     * @param projectData The data to save.
     * @param notifyable  <code>true</code> if Footer message should be
     *                    displayed.
     * @throws NotLoggedInException
     * @throws StatementNotExecutedException
     * @throws NoRightException
     * @throws EntriesException
     * @throws MissingInputException
     */
    void saveProject(ProjectDataSet projectData, boolean notifyable) throws NotLoggedInException, StatementNotExecutedException, NoRightException, EntriesException, MissingInputException;

    /**
     * Saves the project with the specified data.
     * <p>
     * Always display the footer message when the project was saved
     * successfully.
     *
     * @param projectData The data to save.
     * @throws NotLoggedInException
     * @throws StatementNotExecutedException
     * @throws NoRightException
     * @throws EntriesException
     * @throws MissingInputException
     */
    void saveProject(ProjectDataSet projectData) throws NotLoggedInException, StatementNotExecutedException, NoRightException, EntriesException, MissingInputException;

    /**
     * Returns the name of the database.
     *
     * @return The name of the database
     * @throws NotLoggedInException If the user is not logged in.
     */
    String getDbName() throws NotLoggedInException;

    boolean isCurrentProjectConflicted() throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException;

    boolean hasReadRights(ProjectDataSet thisProject) throws NotLoggedInException, NotLoadedException, StatementNotExecutedException;

    boolean hasEditRights(ProjectDataSet projectToEdit) throws StatementNotExecutedException, NotLoggedInException;

    int getNumberOfUncommittedEntries(ProjectDataSet thisProject) throws NotLoggedInException, EntriesException, StatementNotExecutedException, NoRightException;

    int getEntryCount(ProjectDataSet thisProject) throws NotLoggedInException, EntriesException, StatementNotExecutedException, NoRightException;

    /**
     * Returns the number of uncommitted Entries for the given Project
     *
     * @param thisProject
     * @return The number of uncommitted Entries for the given Project.
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NoRightException              If the user has no right to write to Project
     */
    int getNumberOfConflictedEntries(ProjectDataSet thisProject) throws NotLoggedInException, StatementNotExecutedException, NoRightException, EntriesException;

    HashMap<Key, ArrayList<String>> getUsersWithProjectRights(Collection<Key> thisProject) throws NotLoggedInException, EntriesException, StatementNotExecutedException, NoRightException;

    HashMap<Key, ArrayList<String>> getGroupsWithProjectRights(Collection<Key> thisProject) throws NotLoggedInException, EntriesException, StatementNotExecutedException, NoRightException;

    boolean saveEntry(EntryDataSet data) throws EntriesException, NotLoadedException, StatementNotExecutedException, NoRightException,
            NotLoggedInException;

    void saveEntries(EntryDataSet data, ArrayList<Key> keys) throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException;

    void unloadEntry(BaseEntryManager manager);

    ArrayList<User> getVisibleUsers() throws NotLoggedInException, StatementNotExecutedException;

    ArrayList<Rank> getRankOfGroup(int id) throws NotLoggedInException, StatementNotExecutedException;

    User getUserInformation(String text) throws NotLoggedInException, StatementNotExecutedException, EntriesException;

    ArrayList<ColumnType> getAvailableGroupRights() throws NotLoggedInException;

    ArrayList<Group> getGroups() throws NotLoggedInException, StatementNotExecutedException;

    ArrayList<ColumnType> getAvailableRightsGroup() throws NotLoggedInException;

    ArrayList<ColumnType> getAvailableRights() throws NotLoggedInException;

    /**
     * Returns a list of right information of groups for the current project
     *
     * @return
     * @throws NotLoggedInException
     * @throws NotLoadedException
     * @throws StatementNotExecutedException
     * @throws NoRightException
     */
    ArrayList<RightsInformation> getNewGroupRights() throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException;

    /**
     * Returns a list of right information of users for the current project
     *
     * @return
     * @throws NotLoggedInException
     * @throws NotLoadedException
     * @throws StatementNotExecutedException
     * @throws NoRightException
     */
    ArrayList<RightsInformation> getNewUserRights() throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException;

    /**
     * Sets the the given AbstractProjectOverviewScreen as the active project.
     *
     * @param get
     * @param displayInputMask
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws NoRightException
     */
    void loadProject(ProjectDataSet get, boolean displayInputMask) throws StatementNotExecutedException, NotLoggedInException, NoRightException;

    /**
     * Starts the CSVExport with the given list of columns to be exported
     *
     * @param entries
     * @param keys
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws NotLoadedException            If no Project is loaded
     * @throws NoRightException              If the user has no right to read from the
     *                                       Project
     * @throws StatementNotExecutedException If a sql error occurred
     */
    void startExport(HashMap<IBaseManager, ArrayList<ColumnType>> entries, ArrayList<Key> keys) throws NotLoggedInException, StatementNotExecutedException, NoRightException, NotLoadedException;

    /**
     * Returns whether the user is logged in.
     *
     * @return If the user is logged in.
     */
    boolean isLoggedIn();

    /**
     * Closes the connection
     */
    void logout();

    public void addLoginListener(LoginListener connectionListener);

    List<String> getSynchronisationTableNames() throws NotLoggedInException;

    List<ColumnType> getColumnsForTable(String managerName) throws NotLoggedInException;

    boolean canEditEntry() throws NotLoggedInException, NotLoadedException, StatementNotExecutedException;

    HashMap<String, HierarchicData> getHierarchicData(ColumnType multiTable) throws NotLoggedInException;

    void loadMultiEntries(BaseEntryManager itemAt, ArrayList<Key> keys) throws NotLoggedInException, NoRightException, StatementNotExecutedException;

    int getNumberOfHiddenProjects() throws NotLoggedInException, StatementNotExecutedException, EntriesException;

    int getNumberOfProjects() throws NotLoggedInException, StatementNotExecutedException;

    HashMap<Key, Integer> getNumberOfEntries() throws NotLoggedInException, StatementNotExecutedException;

    HashMap<Key, Integer> getNumberOfUnsyncedEntries() throws NotLoggedInException, StatementNotExecutedException;

    HashMap<Key, Integer> getNumberOfConflictedEntries() throws NotLoggedInException, StatementNotExecutedException;

    boolean isValidCodeID(ColumnType columnType, String valueToCheck) throws NotLoggedInException, StatementNotExecutedException;



    /**
     * enumeration of several events.
     */
    enum Event {

        /**
         * The event that the server disconnected
         */
        SERVER_DISCONNECTED,
        /**
         * The event that the server connected
         */
        SERVER_CONNECTED,
        /**
         * The event to reconnect the server *
         */
        RECONNECT
    }

    /**
     * Returns all possible entrys that have the given id as parent for the
     * given table name.
     *
     * @param tableName  The table name in which the entrys are searched.
     * @param id         The id of the parent entry.
     * @param sortedBy   The sortation of the entry within the return list.
     * @param isLanguage
     * @return The sorted list of possible Entrys
     * @throws NotLoggedInException          if the user is not logged in.
     * @throws StatementNotExecutedException if an error with SQL occurs.
     */
    ArrayList<DataColumn> getMultiComboBoxData(String tableName, String id, ColumnType.SortedBy sortedBy, boolean isLanguage)
            throws NotLoggedInException, StatementNotExecutedException;

    /**
     * Returns the List of parent ids for the given id and the given table name
     *
     * @param tableName The name of the table to get data for.
     * @param id        The id to get the parent ids for.
     * @return A list of the ids of the parents of the given entry.
     * @throws NotLoggedInException          if the user is not logged in.
     * @throws StatementNotExecutedException if an error with SQL occurs.
     */
    ArrayList<String> getMultiComboBoxDataParents(String tableName, String id)
            throws NotLoggedInException, StatementNotExecutedException;

    /**
     * Checks for the logged in user if he has write rights in the current
     * project.
     *
     * @return true if the current user has write rights for the current
     * project, false else.
     * @throws NotLoggedInException          if no user is logged in.
     * @throws NotLoadedException            if no project is load.
     * @throws StatementNotExecutedException if there occurs a SQL error.
     */
    boolean hasWriteRights() throws NotLoggedInException, NotLoadedException, StatementNotExecutedException;

    /**
     * Checks for the logged in user if he has write rights in the current
     * project. The parameter allows ignoring the NotLoadedException.
     *
     * @param ignoreNotLoadedException Whether the NotLoadedException should be
     *                                 ignored or not.
     * @return true if the current user has write rights for the current
     * project, false else.
     * @throws NotLoggedInException          if no user is logged in.
     * @throws NotLoadedException            if no project is load.
     * @throws StatementNotExecutedException if there occurs a SQL error.
     */
    boolean hasWriteRights(boolean ignoreNotLoadedException) throws NotLoggedInException, NotLoadedException, StatementNotExecutedException;

    /**
     * Checks for the logged in user if he has write rights in a given project.
     *
     * @param project The project to check.
     * @return true if the current user has write rights for the given project,
     * false else.
     * @throws NotLoggedInException          if no user is logged in.
     * @throws StatementNotExecutedException if there occurs a SQL error.
     */
    boolean hasWriteRights(ProjectDataSet project) throws NotLoggedInException, StatementNotExecutedException;

    /**
     * Returns the current name of the book.
     *
     * @return The book name.
     */
    String getBookName();

    CodeTableHashMap getHashedCodeTableEntries(ColumnType columnType)
            throws NotLoggedInException, StatementNotExecutedException;

    /**
     * Returns a Preferences object that hold the information where to save the
     * settings in the registry.
     * <p/>
     * In general should return the object that is predefined in the
     * Configuration class.
     *
     * @return A Preferences object that hold the information where to save the
     * settings in the registry
     */
    Preferences getPreferences();

    /**
     * Returns the project that is currently loaded.
     *
     * @return The current loaded project.
     * @throws NotLoadedException If no Project is loaded If no project is
     *                            currently loaded.
     */
    ProjectDataSet getCurrentProject() throws NotLoadedException;

    /**
     * Returns a list of input element entries used in the current project.
     *
     * @param columnName The column name of the entries.
     * @return A list of the column used in the current project.
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NotLoadedException            If no Project is loaded
     */
    ArrayList<String> getInputUnitInformation(String columnName) throws NotLoggedInException, StatementNotExecutedException, NotLoadedException;

    /**
     * Returns a list of input element entries used in the current project.
     *
     * @param columnType The column type of the entries.
     * @param projectKey The project key of the project whioh data should be
     *                   loaded.
     * @return A list of the column used in the current project.
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     * @throws NotLoadedException            If no Project is loaded
     */
    ArrayList<String> getInputUnitInformation(ColumnType columnType, Key projectKey) throws NotLoggedInException, StatementNotExecutedException, NotLoadedException;

    /**
     * Returns the display name of the current user
     *
     * @return the display name of the current user
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     */
    String getDisplayName() throws NotLoggedInException, StatementNotExecutedException;

    /**
     * Returns the Display Name of the given user
     *
     * @param id The ID of the User
     * @return The Display Name of the user.
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     */
    String getDisplayName(int id) throws NotLoggedInException, StatementNotExecutedException;

    /**
     * Adds the listener to the list of Project listeners
     *
     * @param listener The ProjectListener
     */
    void addProjectListener(ProjectListener listener);

    /**
     * Adds the listener to the list of codeTableEvent Listeners
     *
     * @param listener The CodeTableListener
     */
    void addCodeTableEventListener(CodeTableListener listener);

    /**
     * Returns if the current user is owner of the current project or not.
     *
     * @return <code>true</code> if the current user is owner of the current
     * project, <code>false</code> else.
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     */
    boolean isProjectOwner() throws NotLoggedInException, StatementNotExecutedException;

    ArrayList<ProjectDataSet> getProjects(boolean showHidden) throws NotLoggedInException, StatementNotExecutedException;

    void hideProject(Key projectKey, boolean hide) throws NotLoggedInException, StatementNotExecutedException;

    /**
     * Returns all Projects in the current Database
     *
     * @return All the Projects in the current Database
     * @throws NotLoggedInException          If the user is not logged in. If there is no
     *                                       connection
     * @throws StatementNotExecutedException If a sql error occurred
     */
    ArrayList<ProjectDataSet> getProjects() throws NotLoggedInException, StatementNotExecutedException;

    /**
     * Returns the ID of the Current User
     *
     * @return The current User ID
     * @throws NotLoggedInException          If the user is not logged in.
     * @throws StatementNotExecutedException If a sql error occurred
     */
    int getUserID() throws NotLoggedInException, StatementNotExecutedException;

    /**
     * Loads the data of the given entry of the loaded project.
     *
     * @param manager
     * @param entryKey
     * @param isEditMode <code>true</code> if it is edit MODE,
     *                   <code>false</code> if it is read MODE.
     * @throws NotLoggedInException          If the user is not logged in. If there is no
     *                                       Connection
     * @throws NoRightException              If The user has no right to read
     * @throws StatementNotExecutedException If a sql error occurred If there is
     *                                       a Error with the connection
     * @throws NotLoadedException            If no Project is loaded If no Projekt was
     *                                       loaded
     * @throws EntriesException
     */
    void loadAndDisplayEntry(BaseEntryManager manager, Key entryKey, boolean isEditMode)
            throws NotLoggedInException, NoRightException, StatementNotExecutedException, NotLoadedException, EntriesException;

    /**
     * Loads the data of the given entry of the given project.
     *
     * @param manager
     * @param entryKey
     * @param project
     * @param isEditMode <code>true</code> if it is edit MODE,
     *                   <code>false</code> if it is read MODE.
     * @throws NotLoggedInException          If the user is not logged in. If there is no
     *                                       Connection
     * @throws NoRightException              If The user has no right to read
     * @throws StatementNotExecutedException If a sql error occurred If there is
     *                                       a Error with the connection
     * @throws NotLoadedException            If no Project is loaded If no Projekt was
     *                                       loaded
     * @throws EntriesException
     */
    void loadAndDisplayEntry(BaseEntryManager manager, Key entryKey, ProjectDataSet project, boolean isEditMode)
            throws NotLoggedInException, NoRightException, StatementNotExecutedException, NotLoadedException, EntriesException;

    void createPdfOfCodeTableValues(ColumnType columnType, boolean printIds) throws NotLoggedInException, StatementNotExecutedException;

    xResultSet getStuff(ArrayList<ColumnType> columns, ArrayList<Key> projects, ArrayList<DataColumn> condition);

    /**
     * Removes the given Data Manager from the list of Data Managers
     *
     * @param aThis
     */
    public void removeDatamanager(PluginDatamanager aThis);

    /**
     * Delete an entry with a specific ID out of the current project.
     *
     * @param manager
     * @param key
     * @throws NotLoggedInException                                           If the user is not logged in.
     * @throws NoRightException
     * @throws StatementNotExecutedException                                  If a sql error occurred
     * @throws EntriesException
     * @throws NotLoadedException
     * @throws de.uni_muenchen.vetmed.xbook.api.exception.EntryInUseException
     */
    void deleteEntry(BaseEntryManager manager, Key key) throws NotLoggedInException, NoRightException, StatementNotExecutedException, EntriesException, NotLoadedException, EntryInUseException;

    //    ExportResult getDataForColumns(List<Key> projectKeys, List<ColumnType> columns, String table)
    ExportResult getDataForColumns(String managerName)
            throws NotLoggedInException, NoRightException, StatementNotExecutedException, NotLoadedException, EntriesException;

    ExportResult getSpecificDataForColumns(String managerName)
            throws NotLoggedInException, NoRightException, StatementNotExecutedException, NotLoadedException, EntriesException;

    ArrayList<ProjectDataSet> getProjectSearchResult(ArrayList<String> tableNames, String where) throws NotLoggedInException, NotLoadedException, StatementNotExecutedException, NoRightException;

    /**
     * Returns the current loaded entry, dependent on a specific manager.
     *
     * @param manager The manager which current entry should be loaded.
     * @return The current loaded entry of the named manager.
     */
    EntryDataSet getLoadedEntry(BaseEntryManager manager);

    /**
     * Returns the type of the database.
     *
     * @return The type of the database.
     */
    DatabaseType getDatabaseType();

}
