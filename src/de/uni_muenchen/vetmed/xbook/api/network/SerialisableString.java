package de.uni_muenchen.vetmed.xbook.api.network;

import java.io.IOException;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class SerialisableString extends Serialisable {

    private String string;

    public SerialisableString(String string) {
        this.string = string;
    }

    public SerialisableString(int string) {
        this.string = string + "";
    }

    public SerialisableString(SerialisableInputInterface is) throws IOException {
        string = is.readString();
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    @Override
    protected int getClassID() {
        return ISerializableTypes.STRING_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeString(string);
    }
    @Override
    public String toString() {
        return string;
    }

}
