package de.uni_muenchen.vetmed.xbook.api.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class ByteArrayList extends Serialisable {

    byte[] data;



    public ByteArrayList(byte[] data) {
        this.data = data;
    }

    public ByteArrayList(SerialisableInputInterface is) throws IOException {
        int datasize = is.readInt();
data = new byte[datasize];
        for (int i = 0; i < datasize; i++) {
            try {
                data[i] = (byte)is.readInt();
            } catch (Throwable t) {
                System.out.println("error in itteration " + i);
                throw t;
            }
        }
    }

    public byte[] getData() {
        return data;
    }


    @Override
    protected int getClassID() {
        return ISerializableTypes.BYTEARR_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeInt(data.length);
        for (byte entry : data) {
            outputStream.writeInt(entry);
        }
    }


}
