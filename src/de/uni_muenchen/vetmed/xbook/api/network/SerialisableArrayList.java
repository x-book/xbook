package de.uni_muenchen.vetmed.xbook.api.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class SerialisableArrayList<T extends Serialisable> extends Serialisable {

    ArrayList<T> data;

    public SerialisableArrayList() {
        data = new ArrayList<>();
    }

    public SerialisableArrayList(ArrayList<T> data) {
        this.data = data;
    }

    public SerialisableArrayList(SerialisableInputInterface is) throws IOException {
        data = new ArrayList<>();
        int datasize = is.readInt();
        for (int i = 0; i < datasize; i++) {
            try {
                data.add((T) is.deserialize());
            } catch (Throwable t) {
                System.out.println("error in itteration " + i);
                throw t;
            }
        }
    }

    public ArrayList<T> getData() {
        return data;
    }

    public boolean add(T data) {
        return this.data.add(data);
    }

    @Override
    protected int getClassID() {
        return ISerializableTypes.ARRAY_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeInt(data.size());
        for (Serialisable entry : data) {
            entry.serialize(outputStream);
        }
    }

    public static SerialisableArrayList<SerialisableString> makeListFromString(String messageString) {
        SerialisableArrayList<SerialisableString> list = new SerialisableArrayList<>();
        int i = 0;
        while (i < messageString.length()) {
            int end = Math.min(i + 65535 / 3, messageString.length());
            list.add(new SerialisableString(messageString.substring(i, end)));
            i = end;
        }
        return list;
    }

    public boolean addAll(Collection<? extends T> c) {
        return data.addAll(c);
    }

}
