package de.uni_muenchen.vetmed.xbook.api.network;

/**
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public interface ISerializableTypes {

    int ARRAY_ID = 9;
    int BYTEARR_ID = 12;
    int COLUMN_ID = 4;
    int COMMAND_ID = 10;
    int DATAHASH_ID = 5;
    int DATA_SET_ID = 48;
    int DATA_SET_NEW_ID = 47;
    int ENTRYDATA_ID = 50;
    int ENTRYKEY_ID = 6;
    int ENTRY_TABLE_NEW_ID = 46;
    int EXAMPLE_ID = 0;
    int INT_ID = 11;
    //int PROJECT_ID_PROJECT = 1;
    int KEY_ID = 2;
    int MESSAGE_ID = 8;
    int PROJECT_DATA_ID = 49;
    int RESULT_ID = 51;
    int RIGHTS_ID = 53;
    int RIGHTS_INFORMATION = 54;
    int RIGHT_ID = 52;
    int STRING_ID = 7;
    int TABLE_ID = 3;

    int USER_PROFILE_ID = 55;
    int INDEX_ID = 56;
    int PROJECT_INFORMATION_ID = 57;

}
