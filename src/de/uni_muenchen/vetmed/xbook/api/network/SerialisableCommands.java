
package de.uni_muenchen.vetmed.xbook.api.network;

import java.io.IOException;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class SerialisableCommands extends Serialisable{
    private Commands command;

    public SerialisableCommands(Commands command) {
        this.command = command;
    }

   public SerialisableCommands(SerialisableInputInterface is) throws IOException {
       command = is.readCommand();
    }

    
    @Override
    protected int getClassID() {
        return ISerializableTypes.COMMAND_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeCommand(command);
    }
}
