package de.uni_muenchen.vetmed.xbook.api.network;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 *
 * @author Johannes Lohrer <johanneslohrer@msn.com>
 */
public abstract class Serialisable {


    public void serialize(SerializableOutputInterface os) throws IOException {
        os.writeInt(getClassID());
        serialiseMe(os);
    }

    protected abstract int getClassID();

    protected abstract void serialiseMe(SerializableOutputInterface outputStream) throws IOException;


}
