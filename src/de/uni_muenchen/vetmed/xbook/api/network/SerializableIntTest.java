package de.uni_muenchen.vetmed.xbook.api.network;

import java.io.IOException;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class SerializableIntTest extends Serialisable {

    int length;

    public SerializableIntTest(int arr) {
        this.length = arr;
    }

    public SerializableIntTest(SerialisableInputInterface is) throws IOException {
        int size = is.readInt();
        System.out.println("=====================================================");
        System.out.println("=====================================================");
        System.out.println("s: " + size);

        for (int i = 0; i < size; i++) {
            int byt = is.readInt();
            if (byt != i+1) {
                System.out.println("fail");
                throw new AssertionError(byt != i);
            }

        }
    }

    @Override
    protected int getClassID() {
        return ISerializableTypes.BYTEARR_ID;
    }

    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {

        outputStream.writeInt(length);

        for (int i = 0; i < length; i++) {

            outputStream.writeInt(i);
        }
//        outputStream.write(arr);
    }
}
