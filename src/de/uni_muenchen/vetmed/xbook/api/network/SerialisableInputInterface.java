package de.uni_muenchen.vetmed.xbook.api.network;

import java.io.IOException;

/**
 * Created by lohrer on 19.11.2014.
 */
public interface SerialisableInputInterface {

     String readString() throws IOException;

     int readInt() throws IOException;

     boolean readBoolean() throws IOException;

     Commands readCommand() throws IOException;

     <T extends Serialisable>T deserialize() throws IOException;
}
