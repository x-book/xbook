/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.uni_muenchen.vetmed.xbook.api.network;

import java.io.IOException;

/**
 *
 * @author lohrer
 */
public interface SerializableOutputInterface {

    void writeCommand(Commands command) throws IOException;

    void writeInt(int number) throws IOException;

    void writeString(String x) throws IOException;
    
    void writeBoolean(boolean b) throws IOException;
    
}
