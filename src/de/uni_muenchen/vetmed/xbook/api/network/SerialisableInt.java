
package de.uni_muenchen.vetmed.xbook.api.network;

import java.io.IOException;

/**
 *
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class SerialisableInt extends Serialisable{
    private int value;

    public SerialisableInt(int value) {
        this.value = value;
    }

   public SerialisableInt(SerialisableInputInterface is) throws IOException {
       value = is.readInt();
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    
    @Override
    protected int getClassID() {
        return ISerializableTypes.INT_ID;
    }

    @Override
    public String toString() {
        return value+"";
    }
    
    @Override
    protected void serialiseMe(SerializableOutputInterface outputStream) throws IOException {
        outputStream.writeInt(value);
    }
      
}
