package de.uni_muenchen.vetmed.xbook.tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * IMPORTANT: When moving class ExternQuery to another project (to create jar
 * file) remember importing library mysql.jar!
 *
 * @author Daniel Kaltenthaler <kaltenthaler@dbs.ifi.lmu.de>
 */
public class ExternQuery {

    private static Connection con = null;

    private static final String dbHost = "localhost";
    private static final String dbPort = "53308";
    private static final String dbName = "excabook_test";
    private static final String dbUser = "root";
    private static final String dbPass = "";

    /**
     * Constructor.
     *
     * Creates a new connection to the database.
     */
    private ExternQuery() {
        try {
            // Load database driver for JDBC interface
            Class.forName("com.mysql.jdbc.Driver");
            // Create connection to JDBC database
            con = DriverManager.getConnection("jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName + "?" + "user=" + dbUser + "&" + "password=" + dbPass);
        } catch (ClassNotFoundException e) {
            System.out.println("JDBC driver not found.");
        } catch (SQLException e) {
            System.out.println("Connection not possible");
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
        }
    }

    /**
     * Get the connection to the database. Create a new connection if no
     * connection is available.
     *
     * @return The connection to the database.
     */
    private static Connection getInstance() {
        if (con == null) {
            new ExternQuery();
        }
        return con;
    }

    public static void sendQuery() {
        // get connection to the database
        con = getInstance();

        if (con != null) {
            Statement stmt;
            try {
                stmt = con.createStatement();

                // create queryManager
                String query = "REPAIR table version USE_FRM;";
                System.out.println("Query: " + query);
                stmt.executeQuery(query);

//                ResultSet result = queryManager.executeQuery(sql);
                // iterate result
//                while (result.next()) {
//                    String id = result.getString("ID");
//                    String dbid = result.getString("DatabaseNumber");
//                    String pid = result.getString("ProjectID");
//                    String pdbid = result.getString("ProjectDatabaseNumber");
//                    String info = id + ", " + dbid + ", " + pid + ", " + pdbid;
//
//                    System.out.println(info);
//                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        sendQuery();
    }

}
