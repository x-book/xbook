package de.uni_muenchen.vetmed.xbook;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.ISynchronisationManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.DatabaseInitializer;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractCodeTableManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractInputUnitManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractProjectManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractQueryManager;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DatabaseTests extends BaseTest {

//    @Test
//    public void ossoBookDatabaseTest() {
//        OBLoc.init();
//        String databaseName = "ossobook_dev";
//        doTest(databaseName);
//        try {
//            testManager(new OBQueryManager("root", "", databaseName));
//        } catch (NotLoggedInException e) {
//            fail();
//        }
//    }
//
//    @Test
//    public void archaeoBookDatabaseTest() {
//        ABLoc.init();
//        String databaseName = "archaeobook_dev";
//        doTest(databaseName);
//        try {
//            testManager(new ABQueryManager("root", "", databaseName));
//        } catch (NotLoggedInException e) {
//            fail();
//        }
//    }



//    @Test
//    public void anthroBookDatabaseTest() {
//        AHLoc.init();
//        String databaseName = "antrhobook_dev";
//        doTest(databaseName);
//        try {
//            testManager(new AHQueryManager("root", "", databaseName));
//        } catch (NotLoggedInException e) {
//            fail();
//        }
//    }

    public void doTest(String databaseName) {
        boolean started = DatabaseInitializer.init(databaseName,false);
        assertEquals(started, true);

    }

    public void testManager(AbstractQueryManager<? extends AbstractProjectManager,?extends AbstractInputUnitManager> manager) {
        AbstractCodeTableManager cm = manager.getCodeTableManager();
        Connection con = manager.getUnderlyingConnection();
        for (Field field : cm.getClass().getFields()) {

            if (field.getName().startsWith("TABLENAME")) {
                String tableName = null;
                try {
                    tableName = (String) field.get(cm);
                } catch (IllegalAccessException e) {
                    fail(e.getMessage());
                }
                if (!existsSchema(con, tableName, manager.getDbName())) {
                    fail("Table " + tableName + " not found");
                }
            }

        }
        for (IBaseManager base : manager.getSyncTables()) {
            assertTrue(existsSchema(con, base.getTableName(), manager.getDbName()));
            for (ISynchronisationManager synchronisationManager : base.getManagers()) {
                if (!existsSchema(con, synchronisationManager.getTableName(), manager.getDbName())) {
                    fail(synchronisationManager.getTableName() + " does not exist");
                }
            }
        }

    }

    public boolean existsSchema(Connection con, String tableName, String schemaName) {
        String query = "SELECT * "
                + " FROM INFORMATION_SCHEMA.TABLES "
                + " WHERE TABLE_SCHEMA = '" + schemaName + "' AND  TABLE_NAME = '" + tableName + "'";
        Statement s = null;
        ResultSet set = null;
        try {
            s = con.createStatement();

            set = s.executeQuery(query);
            return (set.next());

        } catch (SQLException e) {
            fail(e.getMessage());
            return false;
        } finally {

            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            try {
                if (set != null) {
                    set.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //TODO add check for columns and maybe also check unused
}
