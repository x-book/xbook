package de.uni_muenchen.vetmed.xbook;

import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.DatabaseConnection;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.fail;


/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 04.08.2017.
 */
public class TimeOutTest extends BaseTest {

    @Test
    public void TimeoutMYSQL() {
        XBookConfiguration.setDatabaseMode(XBookConfiguration.DatabaseMode.MYSQL);
        timeout();
    }
    @Test
    public void TimeoutH2() {
        XBookConfiguration.setDatabaseMode(XBookConfiguration.DatabaseMode.H2);
        timeout();
    }



    private synchronized void timeout(){
        try {

            DatabaseConnection c = new DatabaseConnection("root", "", "excabook_dev", true);
            Connection underlyingConnection = c.getUnderlyingConnection();
            String q = "SET session wait_timeout=8;";
            underlyingConnection.createStatement().executeQuery(q);
            String query = "SHOW VARIABLES LIKE 'wait_timeout';";
            ResultSet resultSet = underlyingConnection.createStatement().executeQuery(query);
            int timeout = 0;
            if(resultSet.next()){
                timeout = resultSet.getInt(2);
            }
            try {
                System.out.println("waiting");
                System.out.println(timeout);
                wait(timeout*1000+1000);
                System.out.println("wait ended");
                try {


                    resultSet = underlyingConnection.createStatement().executeQuery("SELECT 1;");
                    if (resultSet.next()) {
                        System.out.println(resultSet.getInt(1));
                    }
                }
                catch (SQLException e){
                    e.printStackTrace();
                    System.out.println("'"+e.getSQLState()+"'");
                    System.out.println("Error code"+e.getErrorCode());
                }
                try {


                    resultSet = underlyingConnection.createStatement().executeQuery("SELECT 2;");
                    if (resultSet.next()) {
                        System.out.println("I still was able to read "+resultSet.getInt(1));
                    }
                }
                catch (SQLException e){
                    fail(e.getMessage());
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
                fail(e.getMessage());
            }
        } catch (SQLException | NotLoggedInException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }
}
