package de.uni_muenchen.vetmed.xbook.gui;

import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import org.junit.jupiter.api.Test;


/**
 * @author Johannes Lohrer <lohrer@connact.io>
 */
public interface GuiElementTest {

    @Test
    void testSave() throws IsAMandatoryFieldException;

    @Test
    void testMaxLength();

    @Test
    void testClear();

    @Test
    void testLoad();


}
