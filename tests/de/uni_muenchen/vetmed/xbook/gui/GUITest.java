package de.uni_muenchen.vetmed.xbook.gui;

import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataRow;
import de.uni_muenchen.vetmed.xbook.api.datatype.DataSetOld;
import de.uni_muenchen.vetmed.xbook.api.exception.IsAMandatoryFieldException;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputTextArea;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.inputfields.texts.InputTextField;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


/**
 * @author Johannes Lohrer <lohrer@connact.io>
 */
public class GUITest {
    private static final Log LOGGER = LogFactory.getLog(GUITest.class);

    @BeforeAll
    static void initMainframe() {

    }

    @Nested
    @DisplayName("Test for the Text Field")
    class TextFliedTest implements GuiElementTest {

        final ColumnType columnType = new ColumnType("a.a", ColumnType.Type.VALUE_THESAURUS, ColumnType.ExportType.ALL)
                .setMaxInputLength(10);
        InputTextField inputTextField;

        @BeforeEach
        void initField() {
            inputTextField = new InputTextField(columnType);
            inputTextField.create();
        }

        @Test
        public void testSave() throws IsAMandatoryFieldException {

            DataSetOld dataSetOld = new DataSetOld(null, "a", "a");
            inputTextField.setText("text");
            inputTextField.save(dataSetOld);
            assertEquals("text", dataSetOld.getDataRowForTable("a").get(columnType));
        }

        @Test
        public void testMaxLength() {
            //TODO
        }

        @Test
        @Override
        public void testClear() {
            inputTextField.setText("tex");
            assertEquals("tex", inputTextField.getText());
            inputTextField.clear();
            assertEquals("", inputTextField.getText());
        }

        @Test
        public void testLoad() {
            DataSetOld dataSetOld = new DataSetOld(null, "a", "a");
            final DataRow a = dataSetOld.getDataRowForTable("a");
            a.put(columnType, "gnulf");

            inputTextField.load(dataSetOld);
            assertEquals("gnulf", inputTextField.getText());
        }

    }

    @Nested
    @DisplayName("Test for the Text Field")
    class TextAreaTest implements GuiElementTest {

        final ColumnType columnType = new ColumnType("a.a", ColumnType.Type.VALUE_THESAURUS, ColumnType.ExportType.ALL)
                .setMaxInputLength(10);
        InputTextArea inputTextField;

        @BeforeEach
        void initField() {
            inputTextField = new InputTextArea(columnType);
            inputTextField.create();

        }

        @Test
        public void testSave() throws IsAMandatoryFieldException {
            DataSetOld dataSetOld = new DataSetOld(null, "a", "a");
            inputTextField.setText("text");
            inputTextField.save(dataSetOld);
            assertEquals("text", dataSetOld.getDataRowForTable("a").get(columnType));
        }

        @Test
        public void testMaxLength() {
            inputTextField.setText("textThatisLargerThanAllowed");
            assertNotEquals("textThatisLargerThanAllowed", inputTextField.getText());
            assertEquals("textThatis", inputTextField.getText());
        }
        @Test
        public void testLoad() {
            DataSetOld dataSetOld = new DataSetOld(null, "a", "a");
            final DataRow a = dataSetOld.getDataRowForTable("a");
            a.put(columnType, "gnulf");

            inputTextField.load(dataSetOld);
            assertEquals("gnulf", inputTextField.getText());
        }

        @Test
        @Override
        public void testClear() {
            inputTextField.setText("tex");
            assertEquals("tex", inputTextField.getText());
            inputTextField.clear();
            assertEquals("", inputTextField.getText());
        }


    }
}
