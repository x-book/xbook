package de.uni_muenchen.vetmed.xbook.gui;

import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.ITTableRow;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.component.inputtable.constraints.CellConstraints;
import de.uni_muenchen.vetmed.xbook.api.framework.swing.layout.StackLayout;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.awt.*;

/**
 * Created by lordjoda on 13.09.2017.
 */
public class InputTableTest {

    @Test
    public void MainInputTableTest() {
        JFrame jFrame = new JFrame();

        jFrame.setLayout(new StackLayout());

        jFrame.setSize(1024, 640);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        jFrame.setLocation(dim.width / 2 - jFrame.getSize().width / 2, dim.height / 2 - jFrame.getSize().height / 2);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setVisible(true);

        ITTableRow row = new ITTableRow();
        CellConstraints c;
        c = new CellConstraints(1, 0, 1, 1, 1);
//        row.add(new TextF("tableName", c, new ITConstraintsHelper(new TextFieldConstraints(null, 1,1, 0), (ColumnType) null)));
//        c = new CellConstraints(1, 1, 1, 1, 1);
//        row.add(new ITCellTestField("tableName", c));
        jFrame.add(row.create());

        jFrame.revalidate();
        jFrame.repaint();
    }

    public static void main(String[] args) {
//        new ITable();
    }

}
