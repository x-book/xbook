package de.uni_muenchen.vetmed.xbook;

import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.MySqlServer;
import org.junit.jupiter.api.BeforeAll;

public class BaseTest {

    @BeforeAll
    public void setUp() {
        XBookConfiguration.init();
        MySqlServer.init();
    }
}
