/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.uni_muenchen.vetmed.xbook;

import de.uni_muenchen.vetmed.xbook.api.database.manager.IStandardColumnTypes;
import de.uni_muenchen.vetmed.xbook.api.database.manager.ossobook.IOBAnimalManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.ColumnType;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Johannes Lohrer <lohrer@dbs.ifi.lmu.de>
 */
public class TestArea {
    public static void main(String[] args) {
//    DataList list = new DataList();
//                    EntryMap map1 = new EntryMap();
//                    list.add(map1);
//                    EntryValues value1 = new EntryValues();
//                    value1.add("1");
//                    value1.add("2");
//                    value1.add("3");
//                    EntryValues value2 = new EntryValues();
//                    value2.add("2");
//                    value2.add("3");
//                    value2.add("4");
//                    EntryValues value3 = new EntryValues();
//                    value3.add("12");
//                    value3.add("1");
//                    value3.add("45");
//
//                    EntryValues id = new EntryValues();
//                    id.add(1);
//
//                    EntryValues dbID = new EntryValues();
//                    dbID.add(2);
//
//                    EntryValues projectID = new EntryValues();
//                    projectID.add(3);
//
//                    EntryValues projectDBID = new EntryValues();
//                    projectDBID.add(1);
//
//                    EntryValues projectDBID2 = new EntryValues();
//                    projectDBID2.add(2);
//                    map1.put(IOBAnimalManager.ANIMAL_ID, value1);
//                    map1.put(IStandardColumnTypes.ID, id);
//                    map1.put(IStandardColumnTypes.DATABASE_ID, dbID);
//                    map1.put(IStandardColumnTypes.PROJECT_ID, projectID);
//                    map1.put(IStandardColumnTypes.PROJECT_DATABASE_ID, projectDBID);
//
//                    EntryMap entryMap2 = new EntryMap();
//                    list.add(entryMap2);
//
//                    entryMap2.put(IStandardColumnTypes.ID, id);
//                    entryMap2.put(IStandardColumnTypes.DATABASE_ID, dbID);
//                    entryMap2.put(IStandardColumnTypes.PROJECT_ID, projectID);
//                    entryMap2.put(IStandardColumnTypes.PROJECT_DATABASE_ID, projectDBID);
//                    entryMap2.put(IOBAnimalManager.ANIMAL_NAME, value2);
//                    EntryMap entryMap3 = new EntryMap();
//                    list.add(entryMap3);
//                    entryMap3.put(IStandardColumnTypes.ID, id);
//                    entryMap3.put(IStandardColumnTypes.DATABASE_ID, dbID);
//                    entryMap3.put(IStandardColumnTypes.PROJECT_ID, projectID);
//                    entryMap3.put(IStandardColumnTypes.PROJECT_DATABASE_ID, projectDBID2);
//                    entryMap3.put(IOBAnimalManager.ANIMAL_VALUES_ANIMALS_ID, value3);
//                    
//                    HashMap<ColumnType,EntryValues> key = new HashMap<>();
//                    
//                    key.put(IStandardColumnTypes.ID, id);
//                    key.put(IStandardColumnTypes.DATABASE_ID, dbID);
//                    key.put(IStandardColumnTypes.PROJECT_ID, projectID);
//                    key.put(IStandardColumnTypes.PROJECT_DATABASE_ID, projectDBID);
//                    
//                    ArrayList<EntryMap> mapList = list.getEntrysForKeys(key);
//                    for(EntryMap m:mapList){
//                        System.out.println(m);
//                    }
    }

    @Nested
    class TimeFormatVsSubstringTest {


        List<String> getListOfStrings() {
            ArrayList<String> strings = new ArrayList<>();

            int hour1 = 0;
            int hour2 = 0;
            int minute1 = 0;
            int minute2 = 0;
            int second1 = 0;
            int second2 = 0;
            boolean generate = true;
            while (generate) {


                String newString = hour1 + "" + hour2 + ":" + minute1 + "" + minute2 + ":" + second1 + "" + second2;
                strings.add(newString);
                second2++;
                if (second2 == 10) {
                    second2 = 0;
                    second1++;
                }
                if (second1 == 10) {
                    second1 = 0;
                    minute2++;
                }
                if (minute2 == 10) {
                    minute2 = 0;
                    minute1++;
                }
                if (minute1 == 10) {
                    minute1 = 0;
                    hour2++;
                }
                if (hour2 == 10) {
                    hour2 = 0;
                    hour1++;
                }
                if (hour1 == 10) {
                    generate = false;
                }
            }
            return strings;
        }

        @Test
        void TimeFormatTest() {
            List<String> listOfStrings = getListOfStrings();

            for (String listOfString : listOfStrings) {

            }
        }

        @Test
        void TimeFormatTest2() {
            List<String> listOfStrings = getListOfStrings();

            List<String> returnString = new ArrayList<>();
            for (String listOfString : listOfStrings) {

            }
            System.out.println(returnString.size());

        }

        @Test
        void SubstringTest() {
            List<String> listOfStrings = getListOfStrings();

            List<String> returnString = new ArrayList<>();
            for (String listOfString : listOfStrings) {

                returnString.add(listOfString.substring(0, 5));
            }
            System.out.println(returnString.size());
        }
    }
}
