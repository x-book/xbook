package de.uni_muenchen.vetmed.xbook.network;

import de.uni_muenchen.vetmed.xbook.api.AbstractConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractQueryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.export.AbstractExport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


/**
 * @author Johannes Lohrer <lohrer@connact.io>
 */
public abstract class Functions<T extends AbstractQueryManager<?, ?>, E extends AbstractExport> {
    private static final Log LOGGER = LogFactory.getLog(Functions.class);


    protected static AbstractController<?, ?> controller;


    protected static void init(String databaseName) {
        System.out.println("before");
        XBookConfiguration.init();
        XBookConfiguration.setBookType(databaseName);

        Assertions.assertEquals(AbstractConfiguration.Mode.DEVELOPMENT, XBookConfiguration.MODE);

    }

    @Nested
    @DisplayName("Register Tests")
    class Register {

        @Test
        void checkController() {
            Assertions.assertNotNull(controller);
        }

        @Test
        void testRegistration() {
//            controller.register()
            LOGGER.debug("asd");
        }

    }

    @Nested
    @DisplayName("Login Tests")
    class Login {

    }

    @Nested
    @DisplayName("RequestLastUpdate Tests")
    class RequestLastUpdate {

    }

    @Nested
    @DisplayName("RequestTableScheme Tests")
    class RequestTableScheme {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_TABLE_SCHEME_ALL {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_TABLE_SCHEME_GENERAL {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_NUMBER_OF_ENTRIES {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_PROGRAMM_VERSION {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_RESPONSE {

    }

    @Nested
    @DisplayName("Login Tests")
    class IS_DB_ACTIVE {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_PROJECT_LIST {

    }

    @Nested
    @DisplayName("Login Tests")
    class LOGOUT {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_UPDATE {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_GENERAL_UPDATE {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_DATABASE_NUMBER {

    }

    @Nested
    @DisplayName("Login Tests")
    class RESET_PASSWORD {

    }

    @Nested
    @DisplayName("Login Tests")
    class CHANGE_PASSWORD {

    }

    @Nested
    @DisplayName("Login Tests")
    class CREATE_PROJECT {

    }

    @Nested
    @DisplayName("Login Tests")
    class COMMIT_ENTRY_DATA {

    }

    @Nested
    @DisplayName("Login Tests")
    class COMMIT_PROJECT_DATA {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_EXISTING_PROJECTS {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_PROJECT {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_ENTRY_DATA {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_PROJECT_DATA {

    }

    @Nested
    @DisplayName("Login Tests")
    class GET_NUMBER_OF_ENTRIES_IN_CODE_TABLES {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_DEFAULT_PROJECTS {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_ENTRY_WITH_HIGHEST_NUMBER {

    }

    @Nested
    @DisplayName("Login Tests")
    class GET_MOTD {

    }

    @Nested
    @DisplayName("Login Tests")
    class SET_MOTD {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_SPECIFIC_ENTRY {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_SPECIFIC_PROJECT_DATA {

    }

    @Nested
    @DisplayName("Login Tests")
    class DELETE_PROJECT {

    }

    @Nested
    @DisplayName("Login Tests")
    class GET_NUMBER_OF_ENTRIES_TO_SYNC {

    }

    @Nested
    @DisplayName("Login Tests")
    class GET_NUMBER_OF_PROJECT_DATA_TO_SYNC {

    }

    @Nested
    @DisplayName("Login Tests")
    class SEND_MAIL_TO_USER {

    }

    @Nested
    @DisplayName("Login Tests")
    class INSERT_NEW_CODETABLE_ENTRY {

    }

    @Nested
    @DisplayName("Login Tests")
    class UPDATE_CODETABLE_ENTRY {

    }

    @Nested
    @DisplayName("Login Tests")
    class CHANGE_USER_RIGHTS {

    }

    @Nested
    @DisplayName("Login Tests")
    class CHANGE_PROJECT_RIGHTS_GROUP {

    }

    @Nested
    @DisplayName("Login Tests")
    class LEAVE_GROUP {

    }

    @Nested
    @DisplayName("Login Tests")
    class UPDATE_USER_RANK {

    }

    @Nested
    @DisplayName("Login Tests")
    class CREATE_GROUP {

    }

    @Nested
    @DisplayName("Login Tests")
    class DELETE_GROUP {

    }

    @Nested
    @DisplayName("Login Tests")
    class CHANGE_RANK {

    }

    @Nested
    @DisplayName("Login Tests")
    class REMOVE_USER_FROM_GROUP {

    }

    @Nested
    @DisplayName("Login Tests")
    class REMOVE_GROUP_RANK {

    }

    @Nested
    @DisplayName("Login Tests")
    class CHANGE_PROFILE {

    }

    @Nested
    @DisplayName("Login Tests")
    class REQUEST_TABLE_DATA_NEW {

    }

    @Nested
    @DisplayName("Login Tests")
    class GLOBAL_SEARCH {

    }

    @Nested
    @DisplayName("Login Tests")
    class CHANGE_GENERAL_RIGHTS {

    }

    @Nested
    @DisplayName("Login Tests")
    class GET_GENERAL_RIGHTS {

    }

    @Nested
    @DisplayName("Login Tests")
    class IS_CURRENT_USER_ADMIN {

    }

    @Nested
    @DisplayName("Login Tests")
    class GET_NUMBER_OF_UNACTIVATED_USERS {

    }

    @Nested
    @DisplayName("Login Tests")
    class SEND_MAIL_TO_ALL_USERS {

    }

}
