package de.uni_muenchen.vetmed.xbook;

import de.uni_muenchen.vetmed.xbook.api.datatype.CustomDate;
import de.uni_muenchen.vetmed.xbook.api.exception.InvalidDateException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

/**
 * Created by Johannes Lohrer <lohrer@dbs.ifi.lmu.de> on 27.04.2017.
 */
public class DateTest {

    @Test
    public void testDate() {
        ArrayList<String> validDates = new ArrayList<>();
        validDates.add("2015");
        validDates.add("12.2015");
        validDates.add("13.2.2015");
        validDates.add("1.3.2015");
        validDates.add("2015");
        validDates.add("20. 03. 2015");
        for (String validDate : validDates) {

            try {
                CustomDate.tryParseDate(validDate);
            } catch (InvalidDateException e) {
                Assertions.fail("Date not valid" + validDate);
            }
        }

        ArrayList<String> invalidDates = new ArrayList<>();
        invalidDates.add("2015as");
        invalidDates.add("14/2/2015");
        invalidDates.add("30.2.2015");
        invalidDates.add("14.22.2015");
        invalidDates.add("44.12.2015");
        invalidDates.add("44.12.2015.23");
        for (String invalidDate : invalidDates) {
            try {
                CustomDate.tryParseDate(invalidDate);
                Assertions.fail("Date not valid but acceped: " + invalidDate);
            } catch (InvalidDateException  ignore) {

            }
        }

    }
}
