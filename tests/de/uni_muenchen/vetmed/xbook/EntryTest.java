package de.uni_muenchen.vetmed.xbook;

import de.uni_muenchen.vetmed.xbook.api.database.manager.BaseEntryManager;
import de.uni_muenchen.vetmed.xbook.api.database.manager.IBaseManager;
import de.uni_muenchen.vetmed.xbook.api.datatype.EntryDataSet;
import de.uni_muenchen.vetmed.xbook.api.datatype.ProjectDataSet;
import de.uni_muenchen.vetmed.xbook.api.event.EventRegistry;
import de.uni_muenchen.vetmed.xbook.api.exception.NoRightException;
import de.uni_muenchen.vetmed.xbook.api.exception.NotLoggedInException;
import de.uni_muenchen.vetmed.xbook.api.exception.StatementNotExecutedException;
import de.uni_muenchen.vetmed.xbook.api.helper.GeneralInputMaskMode;
import de.uni_muenchen.vetmed.xbook.implementation.XBookConfiguration;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.controller.AbstractController;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.database.manager.AbstractBaseEntryManager;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.entry.AbstractEntry;
import de.uni_muenchen.vetmed.xbook.implementation.xbook.gui.content.project.AbstractProjectEdit;
import org.junit.jupiter.api.Assertions;

public class EntryTest extends BaseTest {

    @Override
    public void setUp() {
        super.setUp();
        XBookConfiguration.DISPLAY_SOUTS = false;
    }

    public void tearDown(AbstractController abstractController, ProjectDataSet projectDataSet) {
        if (abstractController != null) {
            if (projectDataSet != null) {
                try {
                    abstractController.deleteProjectLocaly(projectDataSet);
                    projectDataSet = null;
                } catch (NotLoggedInException | StatementNotExecutedException | NoRightException e) {
                    e.printStackTrace();
                }
            }

        }
        EventRegistry.resetListeners();

    }

//    @Test
//    public void archaeoBookEntryTest() {
//        new ABSplashScreen(false);
//        Configuration.setBookType("archaeobook");
//        ABLoc.init();
//        AbstractController abstractController = new ABController("archaeobook_dev");
//        projectTest(abstractController);
//    }
//
//    // @Test
//
//    public void anthrobookBookEntryTest() {
//        new AHSplashScreen(false);
//        XBookConfiguration.setBookType("anthrobook");
//        AHLoc.init();
//        AbstractController abstractController = new AHController("anthrobook_dev");
//        projectTest(abstractController);
//    }



//
//    @Test
//    public void ossoBookEntryTest() {
//        new OBSplashScreen(false);
//        Configuration.setBookType("ossobook");
//        OBLoc.init();
//        AbstractController abstractController = new OBController("ossobook_dev");
//        projectTest(abstractController);
//    }

    private void projectTest(AbstractController abstractController) {
        login(abstractController);
        ProjectDataSet projectDataSet = null;
        try {
            projectDataSet = createProject(abstractController);
            createEntry(abstractController, projectDataSet);
            tearDown(abstractController, projectDataSet);
        } catch (AssertionError e) {
            tearDown(abstractController, projectDataSet);
            throw e;
        } finally {
            tearDown(abstractController, projectDataSet);
        }
    }

    public void login(AbstractController controller) {
        controller.login("ossobook", "ossobook");
    }

    public ProjectDataSet createProject(AbstractController controller) {
        AbstractProjectEdit newProjectScreen = controller.getMainFrame().getNewProjectScreen();
        ProjectDataSet projectDataSet = newProjectScreen.saveProject(false);

        AbstractProjectEdit projectEditScreen = controller.getMainFrame().getProjectEditScreen(projectDataSet);

        System.out.println(projectDataSet);
        return projectDataSet;
    }

    private void createEntry(AbstractController<?,?> controller, ProjectDataSet projectDataSet) {
        try {
            controller.loadProject(projectDataSet,true );
            for (IBaseManager iBaseManager : controller.getLocalManager().getSyncTables()) {
                if (iBaseManager instanceof BaseEntryManager) {
                    testManager(controller, projectDataSet, (BaseEntryManager) iBaseManager);
                }
            }

        } catch (StatementNotExecutedException | NotLoggedInException | NoRightException e) {
            e.printStackTrace();
        }
    }

    protected void testManager(AbstractController controller, ProjectDataSet projectDataSet, BaseEntryManager iBaseManager) throws StatementNotExecutedException {
        System.out.println(((AbstractBaseEntryManager) iBaseManager).tableName);
        AbstractEntry entryForManager = controller.getEntryForManager(GeneralInputMaskMode.SINGLE,iBaseManager);
        System.out.println(entryForManager.getClass());

        EntryDataSet entryDataSet = entryForManager.saveEntry(true);
        Assertions.assertNotNull( entryDataSet,"Entry Dataset Null");
        EntryDataSet data = new EntryDataSet(entryDataSet.getEntryKey(), projectDataSet.getProjectKey(), ((AbstractBaseEntryManager) iBaseManager).getDatabaseName(), ((AbstractBaseEntryManager) iBaseManager).tableName);
        iBaseManager.loadBase(data);
        entryForManager.setEntryData(data, false);
    }

}
