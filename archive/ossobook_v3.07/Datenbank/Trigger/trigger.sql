CREATE
TRIGGER project_creation BEFORE INSERT ON ossobook.projekt  FOR
EACH ROW 
BEGIN
DECLARE username VARCHAR(255);
DECLARE currenttime VARCHAR(50);
SET username=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
SET NEW.ProjEigentuemer= username;
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;



CREATE 
TRIGGER project_owner AFTER INSERT ON ossobook.projekt 
FOR EACH ROW 
BEGIN
DECLARE username VARCHAR(255);
SET username=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
INSERT INTO projektrechte (Benutzer,ProjNr,Recht,DBNummerProjekt) VALUES
 (username, NEW.ProjNr, 2, NEW.Datenbanknummer);
END;;



CREATE
TRIGGER update_project BEFORE UPDATE ON ossobook.projekt 
FOR EACH ROW
BEGIN
DECLARE maywrite VARCHAR(50);
DECLARE is_admin VARCHAR(50);
DECLARE currenttime VARCHAR(50);
SELECT Admin INTO is_admin FROM ossobook.benutzer WHERE
 BenutzerName= SUBSTRING(USER(),1, LOCATE('@', USER())-1);
	SELECT NOW() + 0 INTO currenttime;
	SET NEW.Zustand=currenttime;
IF is_admin!='Y' THEN
SET NEW.ProjEigentuemer=OLD.ProjEigentuemer;
SELECT Recht INTO maywrite FROM ossobook.projektrechte WHERE ProjNr=OLD.ProjNr AND DBNummerProjekt=OLD.Datenbanknummer AND 
Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1) AND 
DBNummerProjekt=OLD.Datenbanknummer;
IF maywrite!='schreiben' THEN
SELECT update_project INTO maywrite FROM projektrechte;
END IF;
END IF;
END;;



CREATE
TRIGGER update_rights BEFORE UPDATE ON ossobook.projektrechte
FOR EACH ROW
BEGIN
DECLARE projowner VARCHAR(100);
DECLARE is_admin ENUM('Y','N');
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
SELECT ProjEigentuemer INTO projowner FROM ossobook.projekt WHERE
projekt.ProjNr=OLD.ProjNr AND projekt.Datenbanknummer=OLD.DBNummerProjekt;
SELECT Admin INTO is_admin FROM ossobook.benutzer WHERE 
benutzer.BenutzerName=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF (SUBSTRING(USER(),1, LOCATE('@', USER())-1)!=projowner && 
is_admin!='Y') THEN
	SELECT update_rights INTO projowner FROM ossobook.projekt;
END IF;
END;;


CREATE
TRIGGER insert_rights BEFORE INSERT ON ossobook.projektrechte
FOR EACH ROW
BEGIN
DECLARE projowner VARCHAR(100);
DECLARE is_admin ENUM('Y','N');
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
SELECT ProjEigentuemer INTO projowner FROM ossobook.projekt WHERE
projekt.ProjNr=NEW.ProjNr AND projekt.Datenbanknummer=NEW.DBNummerProjekt;
SELECT Admin INTO is_admin FROM ossobook.benutzer WHERE 
benutzer.BenutzerName=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF (SUBSTRING(USER(),1, LOCATE('@', USER())-1)!=projowner && 
is_admin!='Y') THEN
	SELECT insert_rights INTO projowner FROM ossobook.projekt;
END IF;
END;;



CREATE
TRIGGER update_record BEFORE UPDATE ON ossobook.eingabeeinheit
FOR EACH ROW
BEGIN
DECLARE maywrite VARCHAR(50);
DECLARE is_admin ENUM('Y','N');
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
SELECT Admin INTO is_admin FROM ossobook.benutzer WHERE 
BenutzerName= SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF is_admin!='Y' THEN
	SET NEW.BenutzerName=OLD.BenutzerName;
	SELECT Recht INTO maywrite FROM ossobook.projektrechte,
ossobook.projekt WHERE projektrechte.ProjNr=projekt.ProjNr AND 
projekt.ProjNr =OLD.ProjNr  AND 
projekt.Datenbanknummer=projektrechte.DBNummerProjekt AND 
projektrechte.DBNummerProjekt =OLD.DBNummerProjekt AND
 Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
	IF (maywrite!='schreiben') THEN
	SELECT update_record INTO maywrite FROM ossobook.projekt;
	END IF;
END IF;
END;;


CREATE
TRIGGER insert_record BEFORE INSERT ON ossobook.eingabeeinheit
FOR EACH ROW
BEGIN
DECLARE maywrite VARCHAR(50);
DECLARE is_admin ENUM('Y','N');
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
SELECT Admin INTO is_admin FROM ossobook.benutzer WHERE 
BenutzerName= SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF is_admin!='Y' THEN
	SET NEW.BenutzerName=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
	SELECT Recht INTO maywrite FROM ossobook.projektrechte WHERE 
ProjNr=NEW.ProjNr AND DBNummerProjekt=NEW.DBNummerProjekt 
AND Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
	IF (maywrite!='schreiben') THEN
	SELECT insert_record INTO maywrite FROM ossobook.projekt;
	END IF;
END IF;
END;;


CREATE 
TRIGGER update_masse BEFORE UPDATE ON ossobook.masse
FOR EACH ROW
BEGIN
DECLARE is_admin ENUM('Y','N');
DECLARE maywrite VARCHAR(50);
DECLARE belongingTo int(11);
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
SELECT Admin INTO is_admin FROM ossobook.benutzer WHERE
BenutzerName= SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF is_admin!='Y' THEN
	SELECT count(*) INTO belongingTo FROM ossobook.eingabeeinheit WHERE
eingabeeinheit.massID=OLD.massID; 
	IF (belongingTo>0) THEN
	SELECT Recht INTO maywrite FROM ossobook.projektrechte, 
		ossobook.eingabeeinheit WHERE eingabeeinheit.massID=OLD.massID 
		AND eingabeeinheit.DBNummerMasse=OLD.Datenbanknummer AND 
	projektrechte.ProjNr=eingabeeinheit.projNr AND 
	projektrechte.DBNummerProjekt=eingabeeinheit.DBNummerProjekt 
	AND Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
	IF(maywrite!='schreiben') THEN
		SELECT update_masse INTO maywrite FROM ossobook.projekt;
	END IF;
	END IF;
END IF;
END;;


CREATE 
TRIGGER update_artefact BEFORE UPDATE ON ossobook.artefaktmaske
FOR EACH ROW
BEGIN
DECLARE is_admin ENUM('Y','N');
DECLARE maywrite VARCHAR(50);
DECLARE belongingTo int(11);
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
SELECT Admin INTO is_admin FROM ossobook.benutzer WHERE
	BenutzerName= SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF is_admin!='Y' THEN
	SELECT count(*) INTO belongingTo FROM ossobook.eingabeeinheit WHERE
eingabeeinheit.artefaktID=OLD.ID LIMIT 1; 
	IF (belongingTo>0) THEN
	SELECT Recht INTO maywrite FROM ossobook.projektrechte, 
		ossobook.eingabeeinheit WHERE eingabeeinheit.artefaktID=OLD.ID
		AND eingabeeinheit.DBNummerArtefakt=OLD.Datenbanknummer AND

	projektrechte.ProjNr=eingabeeinheit.projNr AND 
	eingabeeinheit.DBNummerProjekt=projektrechte.DBNummerProjekt AND Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
	IF(maywrite!='schreiben') THEN
		SELECT update_artefact INTO maywrite FROM ossobook.projekt;
	END IF;
	END IF;
END IF;
END;;



CREATE 
TRIGGER insert_artefact BEFORE INSERT ON ossobook.artefaktmaske
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_species  BEFORE UPDATE ON ossobook.tierart
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_species  BEFORE INSERT ON ossobook.tierart
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_skeletonpart  BEFORE UPDATE ON ossobook.skelteil 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;

CREATE 
TRIGGER insert_skeletonpart  BEFORE INSERT ON ossobook.skelteil
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_bonesfragment  BEFORE UPDATE ON ossobook.knochenteil FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_bonesfragment  BEFORE INSERT ON ossobook.knochenteil
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_massdefinition  BEFORE UPDATE ON ossobook.massdefinition FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_massdefinition  BEFORE INSERT ON ossobook.massdefinition FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;



CREATE 
TRIGGER update_massdependency  BEFORE UPDATE ON
ossobook.massabhaengigkeit FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_massdependency  BEFORE INSERT ON 
ossobook.massabhaengigkeit FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_alter1  BEFORE UPDATE ON ossobook.alter1 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_alter1  BEFORE INSERT ON ossobook.alter1
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_alter2  BEFORE UPDATE ON ossobook.alter2
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;



CREATE 
TRIGGER insert_alter2  BEFORE INSERT ON ossobook.alter2
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_alter3  BEFORE UPDATE ON ossobook.alter3
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_alter3  BEFORE INSERT ON ossobook.alter3
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_alter4  BEFORE UPDATE ON ossobook.alter4 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_alter4  BEFORE INSERT ON ossobook.alter4 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;



CREATE 
TRIGGER update_alter5  BEFORE UPDATE ON ossobook.alter5
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_alter5  BEFORE INSERT ON ossobook.alter5 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_user  BEFORE UPDATE ON ossobook.benutzer 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_user  BEFORE INSERT ON ossobook.benutzer 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_burning  BEFORE UPDATE ON ossobook.brandspur 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;



CREATE 
TRIGGER insert_burning  BEFORE INSERT ON ossobook.brandspur 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_breakingedge  BEFORE UPDATE ON ossobook.bruchkante
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_breakingedge  BEFORE INSERT ON ossobook.bruchkante 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_breakingedge2  BEFORE UPDATE ON ossobook.bruchkante2 FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_breakingedge2  BEFORE INSERT ON ossobook.bruchkante2 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;



CREATE 
TRIGGER update_conservation  BEFORE UPDATE ON ossobook.erhaltung 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_conservation  BEFORE INSERT ON ossobook.erhaltung 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_fat  BEFORE UPDATE ON ossobook.fett 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_fat  BEFORE INSERT ON ossobook.fett 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_gender  BEFORE UPDATE ON ossobook.geschlecht 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;



CREATE 
TRIGGER insert_gender  BEFORE INSERT ON ossobook.geschlecht 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_patina  BEFORE UPDATE ON ossobook.patina 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_patina  BEFORE INSERT ON ossobook.patina 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_slaughter1 BEFORE UPDATE ON ossobook.schlachtspur1 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_slaughter1  BEFORE INSERT ON ossobook.schlachtspur1
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;



CREATE 
TRIGGER update_slaughter2 BEFORE UPDATE ON ossobook.schlachtspur2 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_slaughter2  BEFORE INSERT ON ossobook.schlachtspur2 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_verbiss BEFORE UPDATE ON ossobook.verbiss 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_verbiss  BEFORE INSERT ON ossobook.verbiss 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER update_root BEFORE UPDATE ON ossobook.wurzelfrass
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;



CREATE 
TRIGGER insert_root  BEFORE INSERT ON ossobook.wurzelfrass 
FOR EACH ROW
BEGIN
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
END;;


CREATE 
TRIGGER insert_masse BEFORE INSERT ON ossobook.masse FOR EACH ROW
BEGIN
DECLARE maywrite VARCHAR(50);
DECLARE is_admin ENUM('Y','N');
DECLARE currenttime VARCHAR(50);
SELECT NOW() + 0 INTO currenttime;
SET NEW.Zustand=currenttime;
SELECT Admin INTO is_admin FROM ossobook.benutzer WHERE BenutzerName= SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF is_admin!='Y' THEN
	SELECT Recht INTO maywrite FROM ossobook.projektrechte, ossobook.eingabeeinheit 
	WHERE eingabeeinheit.massID=NEW.MassID AND projektrechte.ProjNr=eingabeeinheit.ProjNr AND Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1) LIMIT 1;
	IF(maywrite!='schreiben') THEN
		SELECT insert_masse INTO maywrite FROM ossobook.projekt;
	END IF;
END IF;
END;;

CREATE 
TRIGGER insert_artefact BEFORE INSERT ON ossobook.artefaktmaske FOR EACH ROW
BEGIN
DECLARE maywrite VARCHAR(50);
DECLARE is_admin ENUM('Y','N');
SELECT Admin INTO is_admin FROM ossobook.benutzer where BenutzerName= SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF is_admin!='Y' THEN
	SELECT Recht INTO maywrite FROM ossobook.projektrechte, ossobook.eingabeeinheit 
	WHERE eingabeeinheit.artefaktID=NEW.ID AND projektrechte.ProjNr=eingabeeinheit.ProjNr AND Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1) LIMIT 1;
	IF(maywrite!='schreiben') THEN
		SELECT artefact_masse INTO maywrite FROM ossobook.projekt;
	END IF;
END IF;
END;;

CREATE
TRIGGER delete_project BEFORE DELETE ON ossobook.projekt FOR EACH ROW
BEGIN
DECLARE maywrite VARCHAR(50);
DECLARE is_admin VARCHAR(50);
SELECT Admin INTO is_admin FROM ossobook.benutzer where BenutzerName= SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF is_admin!='Y' THEN
SELECT Recht INTO maywrite FROM ossobook.projektrechte where ProjNr=OLD.ProjNr and Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF maywrite!='schreiben' THEN
SELECT delete_project INTO maywrite FROM projektrechte;
	END IF;
END IF;
END;;

CREATE
TRIGGER delete_rights BEFORE DELETE ON ossobook.projektrechte FOR EACH ROW
BEGIN
DECLARE projowner VARCHAR(100);
DECLARE is_admin ENUM('Y','N');
SELECT Admin INTO is_admin FROM ossobook.benutzer where benutzer.BenutzerName=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF(is_admin!='Y') THEN
	SELECT ProjEigentuemer INTO projowner FROM ossobook.projekt where projekt.ProjNr=OLD.ProjNr;
	IF (SUBSTRING(USER(),1, LOCATE('@', USER())-1)!=projowner) THEN
		SELECT delete_rights INTO projowner FROM ossobook.projekt;
	END IF;
END IF;
END;;


CREATE
TRIGGER delete_record BEFORE DELETE ON ossobook.eingabeeinheit FOR EACH ROW
BEGIN
DECLARE maywrite VARCHAR(50);
DECLARE is_admin ENUM('Y','N');
SELECT Admin INTO is_admin FROM ossobook.benutzer where BenutzerName= SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF is_admin!='Y' THEN
	SELECT Recht INTO maywrite FROM ossobook.projektrechte where ProjNr=OLD.ProjNr and Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
	IF (maywrite!='schreiben') THEN
		SELECT delete_record INTO maywrite FROM ossobook.projekt;
	END IF;
END IF;
END;;

CREATE 
TRIGGER delete_masse BEFORE DELETE ON ossobook.masse FOR EACH ROW
BEGIN
DECLARE is_admin ENUM('Y','N');
DECLARE maywrite VARCHAR(50);
DECLARE belongingTo int(11);
SELECT Admin INTO is_admin FROM ossobook.benutzer where BenutzerName= SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF is_admin!='Y' THEN
	SELECT count(*) INTO belongingTo FROM ossobook.eingabeeinheit WHERE eingabeeinheit.massID=OLD.massID; 
	IF (belongingTo>0) THEN
		SELECT Recht INTO maywrite FROM ossobook.projektrechte, ossobook.eingabeeinheit WHERE eingabeeinheit.massID=OLD.MassID AND projektrechte.ProjNr=eingabeeinheit.ProjNr AND Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
		IF(maywrite!='schreiben') THEN
			SELECT delete_masse INTO maywrite FROM ossobook.projekt;
		END IF;
	END IF;
END IF;
END;;

CREATE 
TRIGGER delete_artefact BEFORE DELETE ON ossobook.artefaktmaske FOR EACH ROW
BEGIN
DECLARE is_admin ENUM('Y','N');
DECLARE maywrite VARCHAR(50);
DECLARE belongingTo int(11);
SELECT Admin INTO is_admin FROM ossobook.benutzer where BenutzerName= SUBSTRING(USER(),1, LOCATE('@', USER())-1);
IF is_admin!='Y' THEN
	SELECT count(*) INTO belongingTo FROM ossobook.eingabeeinheit WHERE eingabeeinheit.artefaktID=OLD.ID; 
	IF (belongingTo>0) THEN
		SELECT Recht INTO maywrite FROM ossobook.projektrechte, ossobook.eingabeeinheit WHERE eingabeeinheit.artefaktID=OLD.ID AND projektrechte.ProjNr=eingabeeinheit.ProjNr AND Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1);
		IF(maywrite!='schreiben') THEN
			SELECT delete_masse INTO maywrite FROM ossobook.projekt;
		END IF;
	END IF;
END IF;
END;;






