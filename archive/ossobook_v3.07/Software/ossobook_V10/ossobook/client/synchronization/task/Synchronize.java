package ossobook.client.synchronization.task;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Vector;

import ossobook.client.synchronization.step.*;
import ossobook.server.*;
import ossobook.client.base.metainfo.Projekt;
import ossobook.client.io.serverCommunication.*;
import ossobook.exceptions.*;
import ossobook.sqlQueries.*;

/**
 * is responsible for synchronization process
 * @author j.lamprecht
 *
 */
public class Synchronize extends Task {

	private Vector<String> logFiles;
	private static String event= "Synchronisation";
	
	public Synchronize(Client client, LocalQueryManager manager, Vector<Projekt> projects){
		super(client, manager, projects, event);
	}
	
	/**
	 * synchronize projects: contains logic of synchronization
	 */
	public Void doInBackground() throws StatementNotExecutedException, MetaStatementNotExecutedException{
	
		logFiles = new Vector<String>();
		schemePath= null;
		try{
			//adapt database scheme
			DatabaseScheme scheme=synchronizeDatabaseScheme();
	    	
	    	//synchronize project data
	    	exchangeProjectChanges(scheme);
	    	
  	    	//adapt code tables
	    	synchronizeCodes(scheme);
	    	
		}    	
	    catch(NoServerConnection e){}		
		return null;
	}
			
	/**
	 * synchronize projects: for each project start synchronization process:
	 * first insert local changes, then get global changes
	 * @throws NoServerConnection
	 * @throws StatementNotExecutedException
	 * @throws MetaStatementNotExecutedException
	 */
	private void exchangeProjectChanges(DatabaseScheme scheme) throws NoServerConnection,
		MetaStatementNotExecutedException, StatementNotExecutedException{
		try{
			manager.lockDatabase();
			setProgressString("\nSynchronisiere Projekte");
	    	firePropertyChange(event, oldProgress, progressString);
			Vector<String> conflictPath=new Vector<String>();
			ServerMethods stubLocalChanges=client.transmitChanges();
		   	ServerMethods stubGlobalChanges=client.getGlobalProjectChanges();
		   	LocalChangesTransmitter trans = new LocalChangesTransmitter(manager, stubLocalChanges);
		   	GlobalChangesGetter getter = new GlobalChangesGetter(manager, stubGlobalChanges);
		   	for(int i=0; i<projects.size(); i++){
		   		int[] projectKey = projects.elementAt(i).getKey();
		   		try{
		    		String path=trans.transmitChanges(projects.elementAt(i).getName(),projectKey, scheme.getProjectScheme());
		    		if(path!=null) conflictPath.add(path);
		    		getter.insertGlobalProjectChanges(projectKey, projects.elementAt(i).getName());
		    		setProgressString("\n"+tab+" Synchronisation des Projekts \"" + projects.elementAt(i).getName()+ "\" beendet");
			    	firePropertyChange(event, oldProgress, progressString);
		   		}
		   		catch(NoWriteRightExceptionForSynchronization e){
		   			setProgressString("\n"+tab+" Fehler bei Projektsynchronisation:\n"+tab+ tab+ " fehlende Schreibberechtigung f\u00FCr Projekt \""
		   						+ projects.elementAt(i).getName()+ "\n"+tab+tab+"  Projektsynchronisation abgebrochen");
			    	firePropertyChange("warning", oldProgress, progressString);
		   		}
		   		catch(NoReadRightExceptionForSynchronization e){
		   			setProgressString("\n"+tab+" Fehler bei Projektsynchronisation:\n"+tab+tab+" fehlende Leseberechtigung f\u00FCr Projekt \""
	   						+ projects.elementAt(i).getName()+ "\n"+tab+tab+"  Projektsynchronisation abgebrochen");
		   			firePropertyChange("warning", oldProgress, progressString);
		   		}
		   	}
		   	setProgressString("\nProjektsynchronisation beendet");
	    	firePropertyChange(event, oldProgress, progressString);
		   	logFiles= conflictPath;
		}
		catch (RemoteException e){
			errorHandling("\nFehler bei Serververbindung:\n"+tab+" Synchronisation der Projekte fehlgeschlagen" + 
					"\nSynchronisation abgebrochen!",NoServerConnection.PROJECT_SYNCHRONIZE_STEP);
		}
		catch(NotBoundException e){
			errorHandling("\nFehler bei Serververbindung:\n"+tab+" Synchronisation der Projekte fehlgeschlagen" + 
				"\nSynchronisation abgebrochen!", NoServerConnection.PROJECT_SYNCHRONIZE_STEP);
		}
	}
		
	public Vector<String> getLogFiles(){
		return logFiles;
	}
	
	public Vector<String> getSchemePath(){
		return schemePath;
	}
	
	public String getProgressDescription(){
		return progressString;
	}
}
