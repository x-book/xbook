package ossobook.client.synchronization.step;

import java.util.Vector;
import java.rmi.*;

import ossobook.client.LoginData;
import ossobook.server.ServerMethods;
import ossobook.sqlQueries.LocalQueryManager;
import ossobook.exceptions.*;

/**
 * makes characteristics synchronization - retransmit
 * @author j.lamprecht
 *
 */
public class GlobalDefinitionsGetter extends Step {

	public GlobalDefinitionsGetter(LocalQueryManager manager, ServerMethods stub){
		super(manager, stub);
	}
	
	/**
	 * retransmit characteristics: contains logic of retransmission
	 * @param tableName - name of characteristics table
	 * @param scheme - scheme of characteristics table
	 */
	public void synchronizeDefinitionTable(String tableName, String[] scheme){
		try{
			Vector<String[]> changes = getGlobalDefinitionChanges(stub, manager.getLastSynchronizationOfDefinitionTable(tableName), tableName);
			insertDefinitionChanges(tableName, changes, scheme);
		}
		catch(StatementNotExecutedException e){}
		catch(MetaStatementNotExecutedException e){}
	}

	/**
	 * get global changes from server
	 * @param stub
	 * @param lastSynchronization - last time of synchronization of characteristics table
	 * @param tableName
	 * @return
	 */
	private Vector<String[]> getGlobalDefinitionChanges(ServerMethods stub, String lastSynchronization, String tableName){
		try{
			return stub.getDefinitionChanges(LoginData.encryptUsername(), LoginData.encryptPassword(), tableName, lastSynchronization);
		} catch (RemoteException e) {
		    System.err.println("Client exception: " + e.toString());
		    e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param tableName
	 * @param changes
	 * 		  Vector<String[]>
	 * 			'- data records
	 * 						'- column content
	 * @param scheme - column names of table
	 * @throws StatementNotExecutedException
	 * @throws MetaStatementNotExecutedException
	 */
	private void insertDefinitionChanges(String tableName, Vector<String[]> changes, String[] scheme) throws StatementNotExecutedException, MetaStatementNotExecutedException{
		manager.disableAutoCommit();
		manager.changeDefinitions(tableName, changes, scheme);
		manager.deleteDefinitionPermanent(tableName);	
		manager.enableAutoCommit();
	}
	
}
