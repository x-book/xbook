package ossobook.client.controlers;

import java.util.*;

import javax.swing.*;
import java.awt.*;
import java.text.*;
import java.beans.*;
import java.rmi.*;
import java.math.*;

import ossobook.exceptions.*;
import ossobook.client.LoginData;
import ossobook.client.gui.common.*;
import ossobook.client.gui.synchronization.*;
import ossobook.client.io.serverCommunication.Client;
import ossobook.client.base.metainfo.*;
import ossobook.client.synchronization.task.*;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.*;
import ossobook.server.*;

/**
 * controls the synchronization act - opens SynchronizeWindow and 
 * SynchronizeCompletedWindow, starts the synchronization
 * @author j.lamprecht
 *
 */
public class Synchronizer extends Thread implements Observer, PropertyChangeListener{

	private SynchronizeWindow synchronize;
	private LockDatabaseMessage warning;
	private LocalQueryManager localManager;
	private OssobookHauptfenster parent;
	private ProgressWindow progress;
	private Client client;
	private Vector<Projekt> projectsSelected;
	private Vector<Projekt> selectableProjects;
	private Task task;
	
	public Synchronizer(OssobookHauptfenster parent, LocalQueryManager localManager){
		this.localManager=localManager;
		this.parent = parent;	
		client = new Client();
		selectableProjects=new Vector<Projekt>();
	}
	
	/**
	 * gets the projects which can be synchronized, initializes SynchronizeWindow
	 */
	public void run(){
		//not open a second synchronize window
		parent.getHauptMenu().synchronizeStarted(true);
		Vector<Projekt> projectListGlobal = new Vector<Projekt>();
		Vector<Projekt> projectListLocal = new Vector<Projekt>();
		try{
			Client client = new Client();
			ServerMethods globalProjects = client.getGlobalProjects();
			projectListGlobal = globalProjects.getGlobalProjects(LoginData.encryptUsername(), LoginData.encryptPassword());
			projectListLocal = localManager.getProjects();
		}
		catch (StatementNotExecutedException stat){}
		catch(RemoteException e){
			e.printStackTrace();
		}
		catch(NotBoundException e){
			e.printStackTrace();
		}
		progress = new ProgressWindow();
		Vector<Projekt> doubleProjects=distinct(projectListLocal, projectListGlobal);
		synchronize = new SynchronizeWindow(parent, selectableProjects, doubleProjects , progress.getWindow());
		
		synchronize.addObserver(this);
	}
	
	
	/**
	 * user pressed button in SynchronizeWindow to start synchronization
	 */
	public void update(Observable o, Object arg){
		if(o==synchronize){
			if(arg.equals("closed")) localManager.closeConnection();
			//program can't be finished
			else{
				parent.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
				synchronize.synchronizeStarted(true);
				if(arg.equals("synchronize")) startSynchronization();
				else initialize();
			}
		}
		else if(o instanceof SynchronizeCompletedWindow){
			finishSynchronize();
		}
		else if(o==warning){
			warning.getInternalFrame().dispose();
			if(arg.equals("abort")){
				finishSynchronize();
			}
			else {
				progress.appendText("Synchronisation gestartet", ProgressWindow.NORMAL);
				SwingUtilities.invokeLater(new Runnable() {
		            public void run() {	
		            	try{
		            		progress.start();
		            		synchronize(projectsSelected);
		            	}
		            	catch(StatementNotExecutedException e){}
		            	catch(MetaStatementNotExecutedException e){}
		            }
		        });			
			}
		}
		
	}
	
	/**
	 * gets at least one project with which shall be worked
	 * @return
	 * @throws NoProjectsSelectedException
	 */
	private void checkProject() throws NoProjectsSelectedException{
		projectsSelected = synchronize.getSelectedProjects();
		
		if(projectsSelected.size()==0){
			throw new NoProjectsSelectedException(synchronize.getWindow());
		}
	}
	
	/**
	 * starts and finishes synchronization:
	 * - tests whether projects have been selected
	 * - gives information about locking local database
	 * - stops synchronization
	 */
	private void startSynchronization(){
		
		try{
			progress.initializeTaskWindow("Synchronisation");
			checkProject();
			
			warning = new LockDatabaseMessage(parent);
			warning.addObserver(this);
		}
		catch(NoProjectsSelectedException e){
			finishSynchronize(); 
		}
	}
	
	/**
	 * synchronizes local and global database
	 * @param projectName: name of the project which shall be synchronized
	 * @return path to the conflict file if there have been conflicts during
	 * the synchronization of the project
	 */
	public void synchronize(Vector<Projekt> projectsSelected) throws StatementNotExecutedException, MetaStatementNotExecutedException{	
		task = new Synchronize(client, localManager, projectsSelected);
		task.addPropertyChangeListener(this); 
		task.execute();
	}
	
	public void propertyChange(PropertyChangeEvent evt){
		if(evt.getPropertyName()=="Synchronisation" || evt.getPropertyName()=="Initialisierung" || evt.getPropertyName()=="warning"){
			if(evt.getPropertyName()=="warning")
				progress.appendText((String) evt.getNewValue(), ProgressWindow.WARNING);
			else
				progress.appendText((String) evt.getNewValue(), ProgressWindow.NORMAL);
			if(evt.getNewValue().equals("\nCodesynchronisation beendet") || evt.getNewValue().equals("")){
				progress.finished();
				if(evt.getPropertyName()=="Synchronisation")
					finished(SynchronizeCompletedWindow.SYNCHRONIZE);
				else if(evt.getPropertyName()=="Initialisierung")
					finished(SynchronizeCompletedWindow.INITIALIZE);
			}
		}
	}
	
	/**
	 * initialization or synchronization finished - inform user about occurred failures
	 * @param event
	 */
	private void finished(int event){	
		Vector<String> logFiles=null;
		Point windowPoint = synchronize.getWindow().getLocation();
	    if(NoReadRightExceptionForSynchronization.noRights()) NoReadRightExceptionForSynchronization.showInformation(windowPoint, NoRightExceptionForSynchronization.synchronize);
	    if(NoWriteRightExceptionForSynchronization.noRights()) NoWriteRightExceptionForSynchronization.showInformation(windowPoint, NoRightExceptionForSynchronization.synchronize);
	    if(task instanceof Synchronize) logFiles=((Synchronize)task).getLogFiles();
	    SynchronizeCompletedWindow complete = new SynchronizeCompletedWindow(parent,(int) windowPoint.getX(), 
	    		(int) windowPoint.getY(), task.getSchemePath(), logFiles, event);
	    complete.addObserver(this);
	}
	
	/**
	 * start initialization - inform user about locking database
	 */
	private void initialize(){
		try{
			progress.initializeTaskWindow("Initialisierung");
			checkProject();
			
			// local database will be locked
			Object[] options = {"Vorgang abbrechen","Vorgang fortsetzen"};
			int n = JOptionPane.showOptionDialog(synchronize.getWindow(),
				    "Bei der Initialisierung werden alle bisherigen Projekteintr\u00E4ge GEL\u00F6SCHT. " +
				    "Dadurch GEHEN alle Eintr\u00E4ge dieses Projekts in der lokalen Datenbank VERLOREN.\n" +
				    "Dies betrifft auch Projekt\u00E4nderungen, die noch nicht in die globale Datenbank" +
				    " eingebracht wurden.\n" +
				    "Anschliessend werden alle in der globalen Datenbank enthaltenen Projekteintr\u00E4ge " +
				    "in die lokale Datenbank kopiert.\n\n" +
				    "W\u00E4hrend der Initialisierung wird die lokale Datenbank gesperrt.",
				    "ACHTUNG!",
				    JOptionPane.YES_NO_OPTION,
				    JOptionPane.QUESTION_MESSAGE,
				    null,options,
				    options[0]);
			// no projects selected
			if(n==0) {
				synchronize.synchronizeStarted(false);
				parent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				return;
			}
			
			// start process
			progress.start();
			task = new Initialize(client, localManager, projectsSelected);
			task.addPropertyChangeListener(this); 
			
			progress.appendText("Initialisierung gestartet", ProgressWindow.NORMAL);
			SwingUtilities.invokeLater(new Runnable() {
	            public void run() {	
	            	task.execute();
	            }
	        });
			
		}
		catch(NoProjectsSelectedException e){
			finishSynchronize(); 
		}
	}
	
	/**
	 * orders global and local projects ascending, discards duplicate projects
	 * @pre local and global ascending ordered by project names
	 * @param local
	 * @param global
	 * @return
	 */
	private Vector<Projekt> distinct(Vector<Projekt> local, Vector<Projekt> global){
		
		Vector<Projekt> doubleProjects = new Vector<Projekt>();
		
		Collator col = Collator.getInstance(Locale.GERMAN); 
		
		int globalIterator =0;
		int localIterator =0;
		
		//compare local and global projects
		for(int i=0; i<Math.min(local.size(), global.size()); i++){
			if(col.compare(global.elementAt(globalIterator).getName(), local.elementAt(localIterator).getName())==0){
				//different projects local and global with equal name
				if(!(local.elementAt(localIterator).getKey()[0]==global.elementAt(globalIterator).getKey()[0]
					&& local.elementAt(localIterator).getKey()[1]==global.elementAt(globalIterator).getKey()[1])){
					global.elementAt(globalIterator).setName(global.elementAt(globalIterator).getName()+" (global)");
					local.elementAt(localIterator).setName(local.elementAt(localIterator).getName()+" (lokal)");
					selectableProjects.add(global.elementAt(globalIterator));
					doubleProjects.add(global.elementAt(globalIterator));
					doubleProjects.add(local.elementAt(localIterator));
				}	
				selectableProjects.add(local.elementAt(localIterator));
				localIterator++;
				globalIterator++;
			}
			else if (col.compare(global.elementAt(globalIterator).getName(), local.elementAt(localIterator).getName())<0){
				selectableProjects.add(global.elementAt(globalIterator));
				globalIterator++;
			}
			else {
				selectableProjects.add(local.elementAt(localIterator));
				localIterator++;
			}		
		}
		
		// local projects remained
		if(local.size()>localIterator){
			for(int i=localIterator; i<local.size(); i++)
				selectableProjects.add(local.elementAt(i));
		}
		
		//global projects remained
		if(global.size()>globalIterator){
			for(int i=globalIterator; i<global.size(); i++)
				selectableProjects.add(global.elementAt(i));
		}
		
		return doubleProjects;
	}
	
	/**
	 * initialization / synchronization (abruptly) finished
	 * "clean up" 
	 */
	private void finishSynchronize(){
		try{
			localManager.unlockDatabase();
			synchronize.synchronizeStarted(false);
			parent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		catch(StatementNotExecutedException e){}
		catch(MetaStatementNotExecutedException e){}
	}
	
}	

