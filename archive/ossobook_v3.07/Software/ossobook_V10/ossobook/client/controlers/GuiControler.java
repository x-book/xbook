package ossobook.client.controlers;

import java.io.*;
import java.util.*;
import javax.swing.ImageIcon;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.rmi.*;
import java.sql.*;

import ossobook.client.LoginData;
import ossobook.client.gui.common.*;
import ossobook.client.config.KonfigurationOssobook;
import ossobook.client.gui.update.components.window.StartFensterThread;
import ossobook.exceptions.ConnectionException;
import ossobook.exceptions.LoginWrongException;
import ossobook.exceptions.OssobookVersionDismatch;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.GlobalQueryManager;
import ossobook.sqlQueries.LocalQueryManager;
import ossobook.sqlQueries.QueryManager;
import ossobook.client.io.serverCommunication.*;
import ossobook.server.*;

/**
 * controls the gui - arranges tasks, especially at user interaction
 * @author j.lamprecht
 *
 */
public class GuiControler implements Observer{
	private static Log _log = LogFactory.getLog(GuiControler.class);
	//configuration file for local and global database connection
	private final static File localPath= new File(KonfigurationOssobook.CONFIG_DIR+"ossobookLocalDB.txt");
	private final static File globalPath = new File(KonfigurationOssobook.CONFIG_DIR+"ossobookDBConfig.txt");
	
	private OssobookHauptfenster s;
	private SelectDatabase database;
	private LocalQueryManager localManager;
	private GlobalQueryManager globalManager;
	
	/**
	 * starts Ossobook, particularly the Ossobook main window
	 */
	public void init(){
		new StartFensterThread(new ImageIcon(KonfigurationOssobook.CONFIG_DIR+"ossobook.jpg"));
		KonfigurationOssobook.readKonfigruation(); // Einlesen der Display Konfiguration
		s = new OssobookHauptfenster(this);
		s.getHauptMenueBar().addObserver(this);
		s.setVisible(true);
		LocalQueryManager.setLocalFile(localPath);
		GlobalQueryManager.setGlobalFile(globalPath);
	}
	   
	/**
	 * This Method checks for DBServerconnection, configfile, jre
	 * 
	 * @see ossobook.config.KonfigurationOssobook
	 * @return true if Ossobook may run.
	 */	
	private boolean checkOssoBookEnvironement(QueryManager manager) {
		// alles was vor dem Programmstart gemacht werden muss.
		// Alles was in Static ConfigOssoBook.java eingetragen werden muss.
		// - connection DB -> localServer? mainServer?
		// -
		Properties version = new Properties();
		FileInputStream fis = null;
		String checkdbversion="";
		try {
			fis = new FileInputStream(new File(KonfigurationOssobook.CONFIG_DIR+"version.properties"));
			checkdbversion = manager.getOssobookVersion();
			version.load(fis);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}catch(StatementNotExecutedException e){}
		// dbversion-check
		if (version.getProperty("dbversion").equals(checkdbversion)) {
			return true;
		} 
		else {
			return false;
		}
	}
	
	/**
	 * GuiControler is informed about pressed buttons and user choices
	 */
	public void update(Observable o, Object arg){
		//login has been finished or aborted
		if(o instanceof Login){
			//login aborted
			if(arg.equals("login aborted")){
				GlobalQueryManager.setLoginData(null, null);
				((Login) o).close();
			}
			//login finished
			else {
				GlobalQueryManager.setLoginData(((Login)o).getUser(), ((Login)o).getPassword());
				if (checkLoginInformation()) {
					try{
						((Login) o).close();
						s.getHauptMenu().setEnabled(true);
						Client client = new Client();
						ServerMethods admin = client.isAdmin();
						boolean result = admin.isAdmin(LoginData.encryptUsername(), LoginData.encryptUsername());
						s.getHauptMenu().loginCompleted(result);
					}
					catch(NotBoundException e){}
					catch(RemoteException e){}
				}
			}
		}
		//project shall be opened - user must be asked which database he wants to use
		else if(o==s.getHauptMenueBar() && arg.equals("openProject")){
			database = new SelectDatabase(s, localPath.exists(), GlobalQueryManager.getStaticUser()!=null);
			database.addObserver(this);
		}
		//user wants to synchronize, builds up connection to global database
		else if(o==s.getHauptMenu() && arg.equals("synchronize")){
			try{
				LocalQueryManager synchronizeManagerLocal = new LocalQueryManager();
				Synchronizer syn=new Synchronizer(s, synchronizeManagerLocal);
				syn.start();
			}
			catch(SQLException s){
				connectionFailed(s);
			}
			
		}
		else if(o==database || o==s.getHauptMenueBar()){	
			if (Boolean.TRUE.equals(arg) || arg.equals("animalCode")){
				//global connection for using global database doesn't exist yet
				if(globalManager == null){
					createGlobalConnection();
				}
				s.getHauptMenu().setQueryManager(globalManager);
			}
			else {
				//local connection needed
				if (localManager==null){
					try{
						localManager = new LocalQueryManager();
					}
					catch (SQLException e){
						connectionFailed(e);
					}
				}
				if(readKonfiguration(localManager))
					s.getHauptMenu().setQueryManager(localManager);
			}
			if(o==s.getHauptMenueBar()){
				s.getHauptMenu().neuenTierCodeHinzufuegen();
			}
			else{
				s.getHauptMenu().openproject();
			}
		}
	}
	
	private boolean readKonfiguration(QueryManager manager){
		try{
			if(checkOssoBookEnvironement(manager)) { // check if ossobook will run
				return true;
			}
			else {
				throw new OssobookVersionDismatch();
			}
		}
		catch (OssobookVersionDismatch ovd){
		}
		return false;
	}
	
	/**
	 * checks whether the login data is correct
	 * @return
	 */
	private boolean checkLoginInformation(){
		try{
			Client client = new Client();
			ServerMethods check = client.checkLogin();
			Boolean result =check.checkLogin(LoginData.encryptUsername(), LoginData.encryptPassword());
			if(result==null) throw new ConnectionException(true);
			else if (!result.booleanValue()) throw new LoginWrongException(true);
			
		}
		catch(RemoteException e){}
		catch (NotBoundException e){}
		catch (LoginWrongException l){
			return false;
		}
		catch (ConnectionException e){}
		return true;		
	}
	
	/**
	 * try to establish the global connection
	 */
	private void createGlobalConnection(){
		try{
			globalManager = new GlobalQueryManager();
		}
		catch (SQLException e){
			connectionFailed(e);
		}
	}
	
	public static File getLocalFile(){
		return localPath;
	}
	
	/**
	 * informs user about reason of connection failure
	 * @param sql
	 */
	public void connectionFailed(SQLException sql){
		try{
			sql.printStackTrace();
	    	//ER_ACCESS_DENIED_ERROR
			if(sql.getErrorCode()==1045) throw new LoginWrongException(true);
			else throw new ConnectionException(true);
		}
		catch(LoginWrongException e){}
		catch (ConnectionException e){}
	}
}
