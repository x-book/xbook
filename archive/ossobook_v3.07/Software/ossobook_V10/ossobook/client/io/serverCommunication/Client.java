package ossobook.client.io.serverCommunication;

import java.rmi.registry.*;
import java.rmi.*;
import java.io.*;

import ossobook.server.*;
import ossobook.client.config.KonfigurationOssobook;
import ossobook.communication.file.DbKonfiguration;

/**
 * part of ossobook client that gets methods from server
 * @author j.lamprecht
 *
 */
public class Client{
	
	//file contains port and server address for connection
	private static final File file = new File(KonfigurationOssobook.CONFIG_DIR+"ServerConfig.txt");
	private static int port;
	private static String host;
	
	/**
	 * initializes client with server address and service port
	 */
    public Client(){
    	DbKonfiguration configuration = new DbKonfiguration();
    	host = configuration.getInfoFromFile(file)[0];
    	port = Integer.parseInt(configuration.getInfoFromFile(file)[1]);
    }
    
    /**
     * generate stub to get check login
     * @return
     * @throws RemoteException
     * @throws NotBoundException
     */
    public ServerMethods checkLogin() throws RemoteException, NotBoundException{
    	Registry registry = LocateRegistry.getRegistry(host, port);
    	ServerMethods stub = (ServerMethods) registry.lookup("Login");
    	return stub;
    }  
    
    /**
     * generate stub to get check whether user is admin
     * @return
     * @throws RemoteException
     * @throws NotBoundException
     */
    public ServerMethods isAdmin() throws RemoteException, NotBoundException{
    	Registry registry = LocateRegistry.getRegistry(host, port);
    	ServerMethods stub = (ServerMethods) registry.lookup("Admin");
    	return stub;
    } 
    
    /**
     * generate stub to get check whether user is admin
     * @return
     * @throws RemoteException
     * @throws NotBoundException
     */
    public ServerMethods getGlobalProjects() throws RemoteException, NotBoundException{
    	Registry registry = LocateRegistry.getRegistry(host, port);
    	ServerMethods stub = (ServerMethods) registry.lookup("Projekte");
    	return stub;
    } 

    /**
     * generate stub to get scheme
     * @return
     * @throws RemoteException
     * @throws NotBoundException
     */
    public ServerMethods getScheme() throws RemoteException, NotBoundException{
    	Registry registry = LocateRegistry.getRegistry(host, port);
    	ServerMethods stub = (ServerMethods) registry.lookup("Scheme");
    	return stub;
    }    
    
    /**
     * generate stub to get all project records
     * @return
     * @throws RemoteException
     * @throws NotBoundException
     */
    public ServerMethods initializeProjects() throws RemoteException, NotBoundException{
    	Registry registry = LocateRegistry.getRegistry(host, port);
    	ServerMethods stub = (ServerMethods) registry.lookup("Initialize");
    	return stub;
    }
    
    /**
     * generate stub to transmit local project changes
     * @return
     * @throws RemoteException
     * @throws NotBoundException
     */
    public ServerMethods transmitChanges() throws RemoteException, NotBoundException{
    	Registry registry = LocateRegistry.getRegistry(host, port);
    	ServerMethods stub = (ServerMethods) registry.lookup("LocalChanges");
    	return stub;
    }
    
    /**
     * generate stub to get global project changes
     * @return
     * @throws RemoteException
     * @throws NotBoundException
     */
    public ServerMethods getGlobalProjectChanges() throws RemoteException, NotBoundException{
    	Registry registry = LocateRegistry.getRegistry(host, port);
    	ServerMethods stub = (ServerMethods) registry.lookup("GlobalProjectChanges");
    	return stub;
    }
    
    /**
     * generate stub to get changes of definition tables
     * @return
     * @throws RemoteException
     * @throws NotBoundException
     */
    public ServerMethods getGlobalDefinitionChanges() throws RemoteException, NotBoundException{
    	Registry registry = LocateRegistry.getRegistry(host, port);
    	ServerMethods stub = (ServerMethods) registry.lookup("DefinitionChanges");
    	return stub;
    }
    
}