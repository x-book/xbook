/*
 * Created on 14.04.2005
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.JFrame;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.client.gui.update.components.other.TabellenAnsicht;
import ossobook.client.gui.update.components.window.ProjektFenster;

/**
 * @author ali Preferences - Java - Code Style - Code Templates
 */
public class CSVExport {
	private static Log _log = LogFactory.getLog(CSVExport.class);
	private Connection con;

	public CSVExport(Connection con) {
		this.con = con;
	}

	/**
	 * @param s
	 * @return
	 */
	// Eliminiere die Zeilenumbr\u00FCche im String
	private String elimCR(String s) {
		if (s.lastIndexOf("\n") > -1) {
			s = s.replaceAll("\n", " ");
		}
		return s;
	}

	// Eliminiere die Kommata im String
	private String elimComma(String input) {
		if (input != null) {
			if (input.lastIndexOf(",") > -1) {
				input = input.replaceAll(",", " - ");
			}
		}
		return input;
	}

	// DER CSV Export
	public void export(String query, File f, JFrame displayframe) {
		String result = "";
		long now = System.currentTimeMillis();
		if (_log.isDebugEnabled()) {
			_log.debug("Starting CSV-Export:");
		}
		try {
			Statement stat = con.createStatement();
			ResultSet rs = stat.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int count = rsmd.getColumnCount();
			String s = "";
			String cname = "";
			// Ueberschriften
			for (int i = 1; i <= count; i++) {
				cname = rsmd.getColumnName(i);
				// finde mass\u00FCberschriften
				if (!(cname == null)) {
					if (cname.startsWith("mass")) {
						try {
							cname = getMassName(cname);
						} catch (Exception e) {
						}
					}
				}
				s = s + "," + cname;
			}
			result = result + s.substring(1, s.length()) + "\n";
			// Inhalte
			if (_log.isTraceEnabled()) {
				_log.trace("Fetched Titles: "
						+ (System.currentTimeMillis() - now));
			}
			int rowcount = 0;

			StringBuffer theResult = new StringBuffer("");
			while (rs.next()) {
				s = "";// ",";
				String neuerString = "";
				for (int i = 1; i <= count; i++) {
					neuerString = rs.getString(i);
					neuerString = elimComma(neuerString);
					s = s + ", " + neuerString;
				}
				s = elimCR(s);
				theResult.append(s.substring(1, s.length()) + "\n");
				displayframe.setTitle("exporting " + rowcount);
				rowcount++;
			}
			if (_log.isTraceEnabled()) {
				_log.trace("Iterated " + rowcount + " rows.");
			}
			result = result + theResult;
		} catch (Exception e) {
			e.printStackTrace();
		}
		// schreibe
		try {
			if (_log.isTraceEnabled()) {
				_log.trace("Writing file time. time: "
						+ (System.currentTimeMillis() - now));
			}
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(result);
			bw.close();

			fw.close();
			f = null;
		} catch (Exception efile) {
		}
		displayframe.setTitle("OssoBook");
		if (_log.isTraceEnabled()) {
			_log.trace("End of CSV-Export. Time used: "
					+ (System.currentTimeMillis() - now));
		}
	}

	/**
	 * @param cname
	 * @return
	 */
	private String getMassName(String cname) throws Exception {
		String query = "SELECT massBeschreibung FROM massdefinition WHERE massName LIKE('"
				+ cname + "') AND geloescht='N';";
		Statement statMass = con.createStatement();
		ResultSet rsMassName = statMass.executeQuery(query);
		rsMassName.next();
		String result = rsMassName.getString("massBeschreibung");
		return result;
	}

	// Funktion zum schreiben einer Skelettteilliste
	public void exportSkelettListe(File f, int[] projectKey,
			int gewaehlterTiercode, String groupby) {

		String query1 = "SELECT @nAbsolut:=SUM(anzahl)  FROM eingabeeinheit,tierart WHERE tierart.TierCode="
				+ gewaehlterTiercode
				+ " AND tierart.tierCode=eingabeeinheit.tierart AND projNr="
				+ projectKey[0]
				+ " AND DBNummerProjekt="
				+ projectKey[1]
				+ " AND geloescht='N';";
		String query2 = " SELECT @gAbsolut:=SUM(gewicht) FROM eingabeeinheit,tierart WHERE tierart.TierCode="
				+ gewaehlterTiercode
				+ " AND tierart.tierCode=eingabeeinheit.tierart AND projNr="
				+ projectKey[0]
				+ " AND DBNummerProjekt="
				+ projectKey[1]
				+ " AND geloescht='N';";

		String selectClause = "";

		// Wenn gew\u00E4hlt ->
		if (groupby.contains("fK")) {
			String fk = "fK,";
			selectClause = selectClause + fk;
		}
		// Wenn gew\u00E4hlt ->
		if (groupby.contains("phase1")) {
			String phase1 = "phase1 AS 'FK-Konkordanz 1',";
			selectClause = selectClause + phase1;
		}
		// Wenn gew\u00E4hlt ->
		if (groupby.contains("phase2")) {
			String phase2 = "phase2 AS 'FK-Konkordanz 2',";
			selectClause = selectClause + phase2;
		}
		// Wenn gew\u00E4hlt ->
		if (groupby.contains("phase3")) {
			String phase3 = "phase3 AS 'FK-Konkordanz 3',";
			selectClause = selectClause + phase3;
		}
		// Wenn gew\u00E4hlt ->
		if (groupby.contains("phase4")) {
			String phase4 = "phase4 AS 'FK-Konkordanz 4',";
			selectClause = selectClause + phase4;
		}
		// immer ->
		String summeN = "SUM(anzahl) AS n,";
		selectClause = selectClause + summeN;
		// immer ->
		String proporzN = "SUM(anzahl)/(@nAbsolut/100 ) AS 'n%',";
		selectClause = selectClause + proporzN;
		// immer ->
		String summeG = "Sum(gewicht) As g,";
		selectClause = selectClause + summeG;
		// immer ->
		String proporzG = "SUM(gewicht)/(@gAbsolut/100 ) AS 'g%',";
		selectClause = selectClause + proporzG;
		// immer ->
		String tiername = "tiername,";
		selectClause = selectClause + tiername;
		// immer ->
		String skelname = "skelName";
		selectClause = selectClause + skelname;

		String query3 = "SELECT "
				+ selectClause
				+ " FROM `eingabeeinheit`,tierart,skelteil "
				+ " WHERE ProjNr="
				+ projectKey[0]
				+ " AND DBNummerProjekt="
				+ projectKey[1]
				+ " AND eingabeeinheit.tierart=tierart.tiercode  AND eingabeeinheit.skelettteil = skelteil.skelcode AND tierart.TierCode="
				+ gewaehlterTiercode
				+ " AND eingabeeinheit.geloescht='N' AND tierart.geloescht='N' AND skelteil.geloescht='N' Group by "
				+ groupby + ",skelCode Order by " + groupby
				+ ",SkelFolgeAuswertung;";
		// Die
		// Folge wird gem\u00E4ss Auswertungs Reihenfolge
		// in der tabelle tierart sortiert.
		String result = "";
		try {
			Statement stat = con.createStatement();
			stat.executeQuery(query1);
			stat.executeQuery(query2);
			ResultSet rs = stat.executeQuery(query3);
			ResultSetMetaData rsmd = rs.getMetaData();
			int count = rsmd.getColumnCount();
			String s = "";

			// Ueberschriften
			for (int i = 1; i <= count; i++) {
				s = s + "," + rsmd.getColumnName(i);
			}
			result = result + s.substring(1, s.length()) + "\n";
			// Inhalte
			while (rs.next()) {
				s = "";// ",";
				for (int i = 1; i <= count; i++) {
					s = s + ", " + rs.getString(i);
				}
				result = result + s.substring(1, s.length()) + "\n";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// schreibe
		try {
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(result);
			bw.close();

			fw.close();
			f = null;
		} catch (Exception efile) {
		}
	}

	// Funktion zum schreiben einer Tierartenliste f\u00FCr die Sortierreihenfolge
	// ist eine TierReihenfolge in der DB vorgesehen (Tierfolge Auswertung).
	public void exportTierartenListe(File f, int[] projectKey) {

		String query1 = "SELECT @nAbsolut:=SUM(anzahl)  FROM eingabeeinheit WHERE projNr="
				+ projectKey[0]
				+ " AND DBNummerProjekt="
				+ projectKey[1]
				+ " AND geloescht='N';";
		String query2 = " SELECT @gAbsolut:=SUM(gewicht) FROM eingabeeinheit WHERE projNr="
				+ projectKey[0]
				+ " AND DBNummerProjekt="
				+ projectKey[1]
				+ " AND geloescht='N';";
		String query3 = " SELECT tierart,TierName,SUM(anzahl) AS n, SUM(anzahl)  / (@nAbsolut /100 ) AS 'n%', SUM(gewicht) AS g, SUM(gewicht)  / (@gAbsolut /100 ) AS 'g%'"
				+ " FROM eingabeeinheit,tierart WHERE projNr="
				+ projectKey[0]
				+ " AND DBNummerProjekt="
				+ projectKey[1]
				+ " AND tierart.TierCode=eingabeeinheit.tierart "
				+ " AND eingabeeinheit.geloescht='N' AND tierart.geloescht='N' "
				+ "GROUP BY eingabeeinheit.tierart ORDER BY tierart.TierFolgeAuswertung;";
		// Die
		// Folge
		// wird
		// gem\u00E4ss
		// Auswertungs
		// Reihenfolge
		// in
		// der
		// tabelle
		// tierart
		// sortiert.
		String result = "";
		try {
			Statement stat = con.createStatement();
			stat.executeQuery(query1);
			stat.executeQuery(query2);
			ResultSet rs = stat.executeQuery(query3);
			ResultSetMetaData rsmd = rs.getMetaData();
			int count = rsmd.getColumnCount();
			String s = "";

			// Ueberschriften
			for (int i = 1; i <= count; i++) {
				s = s + "," + rsmd.getColumnName(i);
			}
			result = result + s.substring(1, s.length()) + "\n";
			// Inhalte
			while (rs.next()) {
				s = "";// ",";
				for (int i = 1; i <= count; i++) {
					s = s + ", " + rs.getString(i);
				}
				result = result + s.substring(1, s.length()) + "\n";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// schreibe
		try {
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(result);
			bw.close();

			fw.close();
			f = null;
		} catch (Exception efile) {
		}
	}

	// Gibt die CSV info als String zur\u00FCck
	public String showResult(String query) {
		String result = "";
		try {
			Statement stat = con.createStatement();
			ResultSet rs = stat.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int count = rsmd.getColumnCount();
			String s = "";

			// Ueberschriften
			for (int i = 1; i <= count; i++) {
				s = s + "\t" + rsmd.getColumnName(i);
			}
			result = result + s.substring(1, s.length()) + "\n";
			// Inhalte
			while (rs.next()) {
				s = "";
				for (int i = 1; i <= count; i++) {
					String neuerWert = rs.getString(i);
					if (neuerWert.equals("")) {
						neuerWert = "....";
					}
					s = s + "\t" + neuerWert;
				}
				result = result + s.substring(1, s.length()) + "\n";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// gibt die CSV Info als JTable zur\u00FCck
	@SuppressWarnings("unchecked")
	public TabellenAnsicht showResultTable(String query,
			ProjektFenster boneproject, int zweck) {
		TabellenAnsicht result = null;
		// TableModel model=new DefaultTableModel();
		// DefaultTableColumnModel column=new DefaultTableColumnModel();

		// result.setModel(model);
		String[] s;
		try {
			Statement stat = con.createStatement();
			ResultSet rs = stat.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int count = rsmd.getColumnCount();
			s = new String[count];

			// Ueberschriften
			for (int i = 1; i <= count; i++) {
				s[i - 1] = rsmd.getColumnName(i);
			}

			Vector rows = new Vector();
			while (rs.next()) {
				Vector v = new Vector();
				for (int i = 1; i <= count; i++) {
					v.add(rs.getString(i));
				}
				rows.add(v.toArray());
			}

			Object[][] Arr = new Object[rows.size()][count];
			for (int i = 0; i < rows.size(); i++) {
				Object[] o = (Object[]) rows.get(i);
				for (int j = 0; j < o.length; j++) {
					if (j == 0) {
						Arr[i][j] = o[j];
					} else {
						Arr[i][j] = o[j];
					}
				}
			}
			if (Arr.length == 0) {
				result = null;
			} else {
				result = new TabellenAnsicht(Arr, s, boneproject, zweck);
				// TableColumn ID=result.getColumnModel().getColumn(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}