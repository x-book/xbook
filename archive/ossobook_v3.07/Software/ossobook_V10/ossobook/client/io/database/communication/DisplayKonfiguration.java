package ossobook.client.io.database.communication;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
/**
 * @author ali
 */
public class DisplayKonfiguration {
	public static String[] getInfoFromFile(File infofile) {
		String[] info = new String[20];
		try {
			File f = infofile;
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			int i = 0; //index for info array
			while (br.ready()) {
				try {
					String s=br.readLine();
					s=s.substring(s.lastIndexOf("=")+1);
					info[i] = s;
				} catch (Exception e) {
				}
				i++;
			}
		} catch (Exception e) {
		}
		return info;
	}
}