/*
 * Created on 30.05.2001
 */
package ossobook.client.io.database.dbinfos;

import java.sql.Connection;

import ossobook.client.io.database.communication.AbstraktesDBInfo;



/**
 * @author ali
 *  
 */
public class TierartenUebersicht extends AbstraktesDBInfo {

    public TierartenUebersicht(Connection c, String query, String header) {
        super(c, query, header);
    }

    protected String bearbeiteResultat() {
        //Output
        String resultat = "";
        try {
            while (rs.next()) {
                resultat = resultat + rs.getString("TierName") + " / "
                        + rs.getString("Summe") + "\n";
            }
        } catch (Exception e) {
        }
        return resultat;
    }

}
