/*
 * Created on 30.05.2001
 */
package ossobook.client.io.database.dbinfos;

import java.sql.Connection;
import java.sql.ResultSetMetaData;

import ossobook.client.io.database.communication.AbstraktesDBInfo;



/**
 * @author ali
 * 
 */
public class ProjektInformationen extends AbstraktesDBInfo {

	public ProjektInformationen(Connection c, String query, String header) {
		super(c, query, header);
	}

	protected String bearbeiteResultat() {
		// Output
		String resultat = "";
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			rs.next();
			int i = rsmd.getColumnCount();
			for (int j = 1; j <= i; j++) {
				String name = rsmd.getColumnName(j);
				String value = rs.getString(j);
				if ((name.equals("projektNotiz")) || name.equals("befundNotiz")) {
					if (!value.equals("")) {
						value = "Notiz vorhanden";
					} else {
						value= "keine Notiz vorhanden";
					}
				}
				resultat = resultat + name + " : " + value + "\n";
			}
		} catch (Exception e) {
		}
		return resultat;
	}

}
