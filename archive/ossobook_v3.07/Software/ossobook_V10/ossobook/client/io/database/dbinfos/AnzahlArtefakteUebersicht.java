/*
 * Created on 30.05.2001
 */
package ossobook.client.io.database.dbinfos;

import java.sql.Connection;

import ossobook.client.io.database.communication.AbstraktesDBInfo;



/**
 * @author ali
 *  
 */
public class AnzahlArtefakteUebersicht extends AbstraktesDBInfo {

    public AnzahlArtefakteUebersicht(Connection c, String query, String header) {
        super(c, query, header);
    }

    protected String bearbeiteResultat() {
        //Output
        String resultat = "";
        try {
            while (rs.next()) {
                resultat = resultat + rs.getString("IDAnzahl");
            }
        } catch (Exception e) {
        }
        return resultat;
    }

}
