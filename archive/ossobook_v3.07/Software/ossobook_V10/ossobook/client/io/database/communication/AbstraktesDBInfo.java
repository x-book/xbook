/*
 * Created on 30.05.2001
 */
package ossobook.client.io.database.communication;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author ali
 *  
 */
public abstract class AbstraktesDBInfo {
    private Connection c;

    private String query;

    protected String header;

    protected ResultSet rs;

    private String answer;

    public AbstraktesDBInfo(Connection c, String query, String header) {
        this.c = c;
        this.query = query;
        this.header = header;
        this.rs = getProjektInformationen();
        this.answer = bearbeiteResultat();
    }

    private ResultSet getProjektInformationen() {
        ResultSet rs = null;
        try {
            Statement stat = c.createStatement();
            rs = stat.executeQuery(query);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rs;

    }

    abstract protected String bearbeiteResultat();

    public String getProjektInfo() {
        return header+answer;
    }
}
