/*
 * Created on 07.03.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package ossobook.client.io.file;
import java.io.File;

import javax.swing.JDesktopPane;

import ossobook.client.io.file.AbstraktesDatenEinlesen;
/**
 * @author ali
 * 
 * To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
public class DatenLeser extends AbstraktesDatenEinlesen {
	/**
	 * @param desktop
	 */
	public DatenLeser(JDesktopPane desktop) {
		super(desktop);
	}
	/**
	 * @param desktop
	 * @param f
	 */
	public DatenLeser(JDesktopPane desktop, File f) {
		super(desktop, f);
	}
	void readData() {
	}
}
