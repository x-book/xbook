/*
 * Created on 11.04.2005
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.base.metainfo;

import java.io.*;

/**
 * @author ali Preferences - Java - Code Style - Code Templates
 */
public class Projekt implements Serializable{
    private String name;

    private int nr;
    private int databasenumber;

    public Projekt(String name, int nr, int dbnr) {
        this.name = name;
        this.nr = nr;
        this.databasenumber=dbnr;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @return Returns the nr.
     */
    public int[] getKey() {
        int[] key= {nr, databasenumber};
        return key;
    }

    /**
     * @param name
     *            The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param nr
     *            The nr to set.
     */
    public void setKey(int nr, int dbnr) {
        this.nr = nr;
        this.databasenumber=dbnr;
    }
}