package ossobook.client.base;


import ossobook.sqlQueries.QueryManager;

public class BaseFasade {

	private QueryManager manager;
	
	public BaseFasade(QueryManager manager){
		this.manager = manager;
	}
	
	public QueryManager getManager(){
		return manager;
	}
	
}
