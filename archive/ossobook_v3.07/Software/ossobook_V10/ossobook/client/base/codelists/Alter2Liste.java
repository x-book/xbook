package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;



/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class Alter2Liste extends KodierungsListe {

    public Alter2Liste(Connection connection) {
        tabellenname = "alter2";
        namenColumne = "Alter2Name";
        codeColumne = "Alter2Code";
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

}