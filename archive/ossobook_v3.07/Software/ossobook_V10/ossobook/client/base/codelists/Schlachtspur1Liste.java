package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;



/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class Schlachtspur1Liste extends KodierungsListe {

    public Schlachtspur1Liste(Connection connection) {
        tabellenname = "schlachtspur1";
        namenColumne = "Schlachtspur1Name";
        codeColumne = "Schlachtspur1Code";
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

}