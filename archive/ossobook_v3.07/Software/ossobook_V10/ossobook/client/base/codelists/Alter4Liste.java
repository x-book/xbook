package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;



public class Alter4Liste extends KodierungsListe {

    public Alter4Liste(Connection connection) {
        tabellenname = "alter4";
        namenColumne = "Alter4Name";
        codeColumne = "Alter4Code";
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

}