package ossobook.client.base.codelists;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * The CodeList Abstract-Class is the Base for all OssoBook CodeLists. CodeLists
 * are ment to translate given codes into values to be displayes in the GUI. The
 * translation should prevent from Mistakes. e.g.: if an Animal code is given
 * the Codelist returns it's Name.
 * 
 * @see ossobook.gui.update.components.window.ProjektFenster
 * @author ali
 *  
 */

public class KnochenTeilListe {
	private static Log _log = LogFactory.getLog(KnochenTeilListe.class);
    protected HashMap codes;
    protected HashMap teile;
    protected ResultSet rs = null;
    protected String tabellenname = "knochenteil";
    protected String namenColumne = "KnochenTeilName";
    protected String skelCodeColumen = "SkelCode";
    protected String codeColumne = "KnochenTeilCode";

    public KnochenTeilListe(Connection connection) {
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

    protected ResultSet getList(Connection connection, String tableName) {
        ResultSet result = null;
        try {
            Statement s = connection.createStatement();
            String query="SELECT * FROM " + tableName + " WHERE geloescht='N';";
            if (_log.isDebugEnabled()) {
            	_log.debug("Query: " + query);
            }
            result = s.executeQuery(query);
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        return result;
    }

    public String getTeil(Object n) {
        return (String) teile.get(n);
    }

    @SuppressWarnings("unchecked")
	protected void fillHashmap(ResultSet rs) {
        try {
            while (rs.next()) {
                String s = rs.getString(namenColumne);
                Integer i = new Integer(rs.getInt(codeColumne));
                Integer j = new Integer(rs.getInt(skelCodeColumen));
                teile.put(i + "," + j, s);
                codes.put(s, i + "," + j);

            }
        } catch (SQLException e) {
        }
    }
}