package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;



/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class Alter3Liste extends KodierungsListe {

    public Alter3Liste(Connection connection) {
        tabellenname = "alter3";
        namenColumne = "Alter3Name";
        codeColumne = "Alter3Code";
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

}