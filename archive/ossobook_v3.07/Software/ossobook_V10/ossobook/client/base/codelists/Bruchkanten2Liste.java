package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;



/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class Bruchkanten2Liste extends KodierungsListe {

    public Bruchkanten2Liste(Connection connection) {
        tabellenname = "bruchkante2";
        namenColumne = "Bruchkante2Name";
        codeColumne = "Bruchkante2Code";
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

}