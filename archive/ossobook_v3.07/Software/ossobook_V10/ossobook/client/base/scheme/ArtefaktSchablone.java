package ossobook.client.base.scheme;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;

import ossobook.client.base.fields.FelderverbundArtefakte;
import ossobook.client.config.KonfigurationOssobook;
import ossobook.client.gui.common.OssobookHauptfenster;

/**
 * Template for artefact Screen
 * 
 * @author ali
 */
@SuppressWarnings("serial")
public class ArtefaktSchablone extends OssobookGrundSchablone {
    private JButton setDefaultBtn;

    /**
     * Constructs Object
     */
    public ArtefaktSchablone(OssobookHauptfenster mainframe) {
        super("Artefakt Template");
        setFields(new FelderverbundArtefakte(mainframe).getNewBasicFields());
        frameSettings();
        addFieldsToPanel();
        getMainPanel().add(createDefaultButton());
        try {
            loadDefault();
        } catch (Exception e) {
        }
    }

    private JButton createDefaultButton() {
        setDefaultBtn = new JButton("Default");
        setDefaultBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveTemplate(new File(KonfigurationOssobook.CONFIG_DIR+"defaultArtefaktTemplate.txt"));
            }
        });
        return setDefaultBtn;
    }

    private void loadDefault() {
        loadTemplate(new File(KonfigurationOssobook.CONFIG_DIR+"defaultArtefaktTemplate.txt"));
    }

    /**
     * 
     */
    public void loadMaxTemplate() {
//      verhindere \u00FCberschreiben des aktuellen templates f\u00FCr update
        File preserve=getAktuellesTemplate();
        File f=new File(KonfigurationOssobook.CONFIG_DIR+"maximumArtefaktTemp.txt");
        loadTemplate(f);
        updateAction();
        setAktuellesTemplate(preserve);
    }


    /**
     * 
     */
    public void loadActiveTemplate() {
        loadTemplate(getAktuellesTemplate());
        updateAction();
    }
}