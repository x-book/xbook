package ossobook.client.base.scheme;

import java.sql.Connection;

import ossobook.client.base.codelists.TierartListe;
import ossobook.client.base.fields.FelderverbundKnochenmaske;
import ossobook.client.gui.common.OssobookHauptfenster;
import ossobook.client.base.*;

/**
 * Template for Indet Case. If a Bone is indeterminable these Fields should be
 * filled in. Because indet fields are often a reduction of the hole fielset,
 * work should be saved by this template.
 * 
 * @author ali
 */
@SuppressWarnings("serial")
public class UnbestimmtSchablone extends OssobookGrundSchablone {
    public UnbestimmtSchablone(OssobookHauptfenster mainframe,
            TierartListe animallist, BaseFasade fasade, int[] projectKey) {
        super("Template f\u00FCr 'Indet'");
        setFields(new FelderverbundKnochenmaske(mainframe, fasade, projectKey).getNewBasicFields());
        frameSettings();
        addFieldsToPanel();
    }

    /**
     * @param parentReference
     */
    public UnbestimmtSchablone(OssobookHauptfenster mainframe, Connection c) {
        super("Template f\u00FCr 'Indet'");
    }
}