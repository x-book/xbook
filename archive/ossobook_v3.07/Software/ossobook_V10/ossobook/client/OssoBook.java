package ossobook.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.client.controlers.GuiControler;

/**
 * This is the Projects Main Class. Start Ossobook here.
 * 
 * @author ali
 */
public class OssoBook{
	private static Log _log = LogFactory.getLog(OssoBook.class);
	public static void main(String[] args) {
		if (_log.isInfoEnabled()) {
			_log.info("Executing application startup.");
		}
		GuiControler controler = new GuiControler();
		controler.init();
	}
}