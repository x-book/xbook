package ossobook.client.gui.administration;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

/**
 * panel where the project owner can change the rights of other users:
 * NORIGHTS: the selected user(s) may not write nor read
 * READ: the selected user(s) may read the data of the project
 * WRITE: the selected user(s) may read and can make updates, inserts and deletes
 * 		  on the project
 * @author j.lamprecht
 *
 */

public class AdminWindow extends Observable implements ActionListener{

	private JPanel panel;
	private JButton change;
	private JButton all;
	private JButton none;
	private JPanel userPanel;
	private JPanel buttonPanel;
	
	//constants for selected rights
	public static final int NOTHING = -1;
	public static final int NORIGHTS = 0;
	public static final int READ = 1;
	public static final int WRITE = 2;
		
	/**
	 * arranges the panel, so that the user can use the gui to change rights
	 * @param users: all "normal" users who use the database
	 * @param projName: name of the project which shall be changed
	 */
	public AdminWindow(Vector<String> users, String projName){
		
		panel=new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gridBagConstraints = new GridBagConstraints();

		//project description
		JLabel projectLabel = new JLabel();
		projectLabel.setFont(new Font("Tahoma", 1, 14));
        projectLabel.setText("Projekt: " + projName);
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.gridheight = GridBagConstraints.RELATIVE;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new Insets(0, 10, 10, 10);
        panel.add(projectLabel, gridBagConstraints);
		
        //user selection
		JLabel userLabel = new JLabel();
		userLabel.setText("Benutzer w\u00E4hlen:");
		gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.insets = new Insets(20, 0, 0, 20);
	    gridBagConstraints.anchor = GridBagConstraints.NORTH;
	    panel.add(userLabel, gridBagConstraints);
	        
		JPanel userSelection = new JPanel();
		userSelection.setLayout(new GridBagLayout());
		
        JScrollPane userScroll = new JScrollPane();
        userPanel = new JPanel();
        userPanel.setLayout(new GridBagLayout());
        
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
                
        for (int i=0; i<users.size(); i++){
        	JCheckBox check = new JCheckBox();
	        check.setText(users.elementAt(i));
	        userPanel.add(check, gridBagConstraints);
        }
        
        userScroll.setViewportView(userPanel);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        userSelection.add(userScroll, gridBagConstraints);
        
        all = new JButton();
        all.setText("alle User");
        all.addActionListener(this);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(10, 0, 0, 5);
        userSelection.add(all, gridBagConstraints);
        
        none = new JButton();
        none.setText("kein User");
        none.addActionListener(this);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(10, 0, 0, 0);
        userSelection.add(none, gridBagConstraints);
		
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(20, 0, 30, 0);
        panel.add(userSelection, gridBagConstraints);
        
        //rights selection
        JLabel rights = new JLabel();
        rights.setText("Rechte w\u00E4hlen:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        gridBagConstraints.anchor = GridBagConstraints.NORTH;
        gridBagConstraints.insets = new Insets(20, 30, 0, 0);
        panel.add(rights, gridBagConstraints);
        
        ButtonGroup group = new ButtonGroup();        
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());
        
        JRadioButton noRights = new JRadioButton();
        noRights.setText("weder lesen noch schreiben");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        group.add(noRights);
        buttonPanel.add(noRights, gridBagConstraints);
        
        JRadioButton read = new JRadioButton();
        read.setText("nur lesen");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        group.add(read);
        buttonPanel.add(read, gridBagConstraints);

        JRadioButton write = new JRadioButton();
        write.setText("lesen und schreiben");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        group.add(write);
        buttonPanel.add(write, gridBagConstraints);
        
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.insets = new Insets(20, 20, 0, 0);
        gridBagConstraints.anchor = GridBagConstraints.NORTH;
        panel.add(buttonPanel, gridBagConstraints);
        
        //button for right change
        change = new JButton();
        change.setText("Rechte \u00E4ndern");
        change.addActionListener(this);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.gridheight = GridBagConstraints.REMAINDER;
        gridBagConstraints.insets = new Insets(0, 0, 100, 0);
        panel.add(change, gridBagConstraints);
        panel.add(change, gridBagConstraints);        
	}

	/**
	 * button "Rechte \u00E4ndern" has been pushed
	 */
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==change){
			setChanged();
			notifyObservers();
		}
		else if(e.getSource()==all)
			changeSelectedUsers(true);
		else if (e.getSource()==none)
			changeSelectedUsers(false);
	}
	
	/**
	 * select or deselect all users
	 * @param selected: true=select all, false=deselect all
	 */
	private void changeSelectedUsers(boolean selected){
		Component[] comp = userPanel.getComponents();
		for(int i=0; i<comp.length; i++){
			((JCheckBox)comp[i]).setSelected(selected);	
		}		
	}
	
	/**
	 * get selected users
	 * @return users which have been selected by the project owner - 
	 * their rights will be changed
	 */
	public Vector<String> getUsers(){
		Vector<String> users = new Vector<String>();
		Component[] comp = userPanel.getComponents();
		for(int i=0; i<comp.length; i++){
			if (((JCheckBox)comp[i]).isSelected()) 
				users.add(((JCheckBox) comp[i]).getText());
		}
		return users;
	}
	
	/**
	 * 
	 * @return right that have been selected by the project owner - 
	 * selected users will get that right 
	 */
	public int getRight(){
		Component[] comp = buttonPanel.getComponents();
		for(int i=0; i<comp.length; i++){
			if (((JRadioButton)comp[i]).isSelected()){
				if(i==0) return NORIGHTS;
				else if(i==1) return READ;
				else if(i==2) return WRITE;
			}
		}
		return NOTHING;
	}
	
	public JPanel getPanel(){
		return panel;
	}
}
