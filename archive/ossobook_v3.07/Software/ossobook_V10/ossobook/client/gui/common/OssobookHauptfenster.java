package ossobook.client.gui.common;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;

import javax.swing.*;

import ossobook.client.config.KonfigurationOssobook;
import ossobook.client.controlers.GuiControler;

/**
 * @author ali
 */
public class OssobookHauptfenster extends JFrame {
		
	private JDesktopPane desktop;
	private MainMenuBar hauptmenu;
	private GuiControler controler;
	
	public OssobookHauptfenster(GuiControler controler) {
		this.controler = controler;
		settings();
		createMenu();
	}
	
	/**
	 *  setsup the MenuBar for this Frame
	 * @see ossobook.gui.common.MainMenuBar
	 */
	private void createMenu() {
		hauptmenu=new MainMenuBar(this, controler);
		this.setJMenuBar(hauptmenu.getJMenuBar());
	}
	
	public MainMenuBar getHauptMenueBar(){
		return hauptmenu;
	}
	
	
	/**
	 * @return Returns the desktop.
	 */
	public JDesktopPane getDesktop() {
		return desktop;
	}
	
	public MainMenuBar getHauptMenu(){
		return hauptmenu;
	}
	
	/**
	 *  setsup the frames look and feel, Size and properties
	 */
	private void settings() {
		// Windows Look and Feel einstellen
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			SwingUtilities.updateComponentTreeUI(this);
		} catch (Exception e) {
		}

		this.setTitle(KonfigurationOssobook.PROGRAMNAME);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		desktop = new JDesktopPane();
		desktop.setBackground(Color.black);
		this.setContentPane(desktop);
	}
	
	public GuiControler getGuiControler(){
		return controler;
	}
}