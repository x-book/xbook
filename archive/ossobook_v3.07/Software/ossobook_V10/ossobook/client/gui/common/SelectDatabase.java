package ossobook.client.gui.common;

import java.awt.*;
import javax.swing.*;

import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.util.*;

/**
 * asks the user with which database he wants to work on a project
 * -> user should know whether he has internet
 * @author j.lamprecht
 *
 */
public class SelectDatabase extends Observable implements ActionListener{

	private boolean globalDatabase;
	private JInternalFrame window;
	private JButton ok;
	private JCheckBox local;
	private JCheckBox global;
	
	/**
	 * bulids up the SelectDatabase window
	 * @param parentReference
	 */
	public SelectDatabase(OssobookHauptfenster parentReference, boolean fileExists,
			boolean loginDataExists){
		
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		window = new JInternalFrame();
		window.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		window.setLayout(layout);
		window.setSize(300,200);
		
		// description of what the user shall do
		JLabel question = new JLabel("Mit welcher Datenbank wollen Sie arbeiten?");
		constraints.fill=GridBagConstraints.BOTH;
		constraints.gridwidth=GridBagConstraints.REMAINDER;
		constraints.ipadx=10;
		constraints.ipady=10;
		layout.setConstraints(question, constraints);
		window.add(question);
		
		//local select button
		local = new JCheckBox("mit lokaler Datenbank arbeiten");
		layout.setConstraints(local, constraints);
		if(! fileExists) {
			local.setEnabled(false);
		}
		local.addActionListener(this);
		
		//global select button
		global = new JCheckBox("mit globaler Datenbank arbeiten");
		layout.setConstraints(global, constraints);
		if(!loginDataExists) {
			global.setEnabled(false);
			local.setSelected(true);
		}
		else{
			global.setSelected(true);
			global.addActionListener(this);
		}
		
		window.add(global);
		window.add(local);
		
		//button to start working with the selected database
		ok = new JButton("ok");
		constraints.fill=GridBagConstraints.NONE;
		constraints.insets=new Insets(20,0,0,0);
		constraints.gridheight=3;
		layout.setConstraints(ok, constraints);
		ok.addActionListener(this);
		window.add(ok);
		window.setVisible(true);
		
		parentReference.getDesktop().add(window);
		try{
			window.setSelected(true);
		}
		catch(PropertyVetoException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * user wants to start working with the global / local database
	 */
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==ok){
			//neither local nor global database was selected
			if (!global.isSelected() && !local.isSelected()){
				JOptionPane.showMessageDialog(window,
					"Bitte w\u00E4hlen Sie eine Datenbank!",
					"Verbindungsaufbau fehlgeschlagen",
					JOptionPane.WARNING_MESSAGE);
			}
			else{
				if (global.isSelected())
					globalDatabase=true;
				else if (local.isSelected())
					globalDatabase=false;
				window.dispose();
				setChanged();
				notifyObservers(globalDatabase);
			}
		}
		else if (e.getSource()==local){
			global.setSelected(false);
		}
		else if (e.getSource()== global){
			local.setSelected(false);
		}
	} 
}
