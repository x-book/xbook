package ossobook.client.gui.common;
import java.awt.Toolkit;

import javax.swing.JInternalFrame;

import ossobook.client.config.KonfigurationOssobook;


/**
 * The Basic Class for all Ossobook Projects
 * @author ali
 *  
 */
@SuppressWarnings("serial")
public class ProjektBasisFenster extends JInternalFrame {
	protected String projektName;
	
	public ProjektBasisFenster(String projektname) {
		this.projektName = projektname;
		settings();
	}
	/**
	 * sets up the frames Settings
	 *
	 */
	private void settings() {
		int xSize=Toolkit.getDefaultToolkit().getScreenSize().width-KonfigurationOssobook.projektbasisfensterBorderRight;
		this.setSize(xSize, KonfigurationOssobook.projektbasisfenster_y);
		this.setTitle(projektName);
		this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		this.setResizable(true);
		this.setMaximizable(true);
		this.setIconifiable(true);
		this.setClosable(true);
		this.setVisible(true);
	}
	
	public String getProjectName(){
		return projektName;
	}
}