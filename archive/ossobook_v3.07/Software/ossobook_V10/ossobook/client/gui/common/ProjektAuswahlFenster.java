/*
 * Created on 26.11.2004
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.gui.common;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Enumeration;
import javax.swing.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.client.base.metainfo.Projekt;
import ossobook.client.config.KonfigurationOssobook;
import ossobook.exceptions.NoWriteRightException;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.QueryManager;

/**
 * @author ali Preferences - Java - Code Style - Code Templates
 */

@SuppressWarnings("serial")
public class ProjektAuswahlFenster extends ProjektBasisFenster {
	private static Log _log = LogFactory.getLog(ProjektAuswahlFenster.class);

	private String filterString = "";

	private QueryManager manager;

	private OssobookHauptfenster parentReference;

	final JButton loesche = new JButton("Projekt Entfernen");

	final ButtonGroup bgroup = new ButtonGroup();

	JTextField filterField = new JTextField(filterString);

	private JPanel mainpanel;

	public ProjektAuswahlFenster(OssobookHauptfenster parentReference,
			QueryManager manager) {
		super("Projektauswahl");
		if (_log.isDebugEnabled()) {
			_log
					.debug("Called constructor ProjektAuswahlFenster(OssobookHauptfenster parentReference, QueryManager manager) ");
		}
		this.parentReference = parentReference;
		this.manager = manager;
		setSize(KonfigurationOssobook.projektauswahlfenster_x,
				KonfigurationOssobook.projektauswahlfenster_y);
		this.mainpanel = konstruiereInhalt("");
		this.setContentPane(mainpanel);
	}

	private JPanel konstruiereInhalt(String filterValue) {
		final JPanel result = new JPanel();
		final JPanel result2 = new JPanel();
		final JPanel commands = new JPanel();
		result2.setLayout(new GridLayout(2, 1));
		JButton newProject = new JButton("neues Projekt");
		newProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				newProjectProcedure();
			}
		});
		commands.add(newProject);

		try {
			long time = System.currentTimeMillis();
			if (_log.isDebugEnabled()) {
				_log.debug("Getting project-list");
			}
			Projekt[] plist = manager.holeProjekte(filterValue);
			if (_log.isDebugEnabled()) {
				_log.debug("Fetching project-list took "
						+ (System.currentTimeMillis() - time) + "ms");
			}
			for (int i = 0; i < plist.length; i++) {
				JRadioButton plistchoice = new JRadioButton(plist[i].getName());
				plistchoice.setToolTipText("" + plist[i].getKey());
				bgroup.add(plistchoice);
				result.add(plistchoice);
			}
			result.setLayout(new GridLayout(plist.length, 1));
		} catch (StatementNotExecutedException e) {
			if (_log.isErrorEnabled()) {
				_log
						.error("Problem in function konstruiereInhalt(), StatementNotExecutedException");
			}
		}
		loesche.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loescheProcedure();
			}
		});
		commands.add(loesche);

		JButton afirm = new JButton("OK");
		afirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okProcedure();
			}
		});
		commands.add(afirm);

		JButton abort = new JButton("abbrechen");
		abort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		commands.add(abort);

		Dimension fieldSize = new Dimension(150, 60);
		filterField = new JTextField(filterString);
		filterField.setSize(fieldSize);
		filterField.setPreferredSize(fieldSize);
		filterField.setMinimumSize(fieldSize);
		filterField.setMaximumSize(fieldSize);
		filterField.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent keyEvt) {
			}

			public void keyReleased(KeyEvent keyEvt) {
				filterString = filterField.getText();
				mainpanel.removeAll();
				setContentPane(konstruiereInhalt(filterString));
				findFocusedField();
			}

			public void keyTyped(KeyEvent keyEvt) {
			}
		});
		commands.add(filterField);

		JScrollPane sp = new JScrollPane(result);
		result2.add(sp);
		result2.add(commands);
		return result2;
	}

	/**
	 * 
	 */
	private void okProcedure() {
		Enumeration elem = bgroup.getElements();
		int[] projectKey = { -1, -1 };
		String projName = "";
		while (elem.hasMoreElements()) {// Suche gew\u00E4hltes Objekt
			JRadioButton b = (JRadioButton) elem.nextElement();
			if (b.isSelected()) {
				try {
					projectKey = manager.getProjectKey(b.getText());
					projName = b.getText();
					break;
				} catch (StatementNotExecutedException e) {
					if (_log.isErrorEnabled()) {
						_log
								.error("Problem in function okProcedure(), StatementNotExecutedException");
					}
					e.printStackTrace();
				}
			}
		}
		if (projectKey[0] != -1) { // nur falls projekt gew\u00E4hlt wurde
			// weiterfahren

			parentReference.getHauptMenu().setEnabled(true);
			new ProjectManipulation(projName, parentReference, manager,
					projectKey);
			dispose();
		}
	}

	/**
	 * 
	 */
	private void loescheProcedure() {
		int i = JOptionPane.showConfirmDialog(loesche, "Sind sie Sicher?");
		if (i == JOptionPane.OK_OPTION) {
			try {
				Enumeration elem = bgroup.getElements();
				int[] projectKey = new int[2];
				while (elem.hasMoreElements()) {// Suche gew\u00E4hltes Objekt
					JRadioButton b = (JRadioButton) elem.nextElement();
					if (b.isSelected()) {
						projectKey = manager.getProjectKey(b.getText());
						break;
					}
				}
				manager.deleteProject(projectKey);
				mainpanel.removeAll();
				setContentPane(konstruiereInhalt(""));
			} catch (StatementNotExecutedException e) {
				if (_log.isErrorEnabled()) {
					_log
							.error("Problem in function loescheProcedure(), StatementNotExecutedException");
				}
			} catch (NoWriteRightException n) {
				if (_log.isErrorEnabled()) {
					_log
							.error("Problem in function loescheProcedure(), NoWriteRightException");
				}
			}
		}
	}

	/**
	 * 
	 */
	private void newProjectProcedure() {
		String ProjektName = JOptionPane.showInputDialog("Projekt Name: ");
		try {
			manager.disableAutoCommit();
			manager.newProject(ProjektName);
			manager.enableAutoCommit();
			mainpanel.removeAll();
			setContentPane(konstruiereInhalt(""));
		} catch (StatementNotExecutedException s) {
			if (_log.isErrorEnabled()) {
				_log.error("Problem in function newProjectProcedure()");
				s.printStackTrace();
			}
		}
	}

	private void findFocusedField() {
		JPanel commands = ((JPanel) getContentPane().getComponent(1));
		JTextField filterField = null;
		for (int i = 0; i < commands.getComponentCount(); i++) {
			if (commands.getComponent(i).getClass().equals(JTextField.class)) {
				filterField = (JTextField) commands.getComponent(i);
			}
		}
		filterField.grabFocus();
	}
}