package ossobook.client.gui.update.elements.eingabefelder;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturTextFeld;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.QueryManager;




/*
 * ItemNrField.java
 *
 * Created on 21. M\u00E4rz 2003, 15:34
 */

/**
 *
 * @author  ali
 */
public class IndividuumFeld extends AbstraktesSelbstkorrekturTextFeld{
    
    private QueryManager manager;
    private int[] projectKey;

    /** Creates a new instance of ItemNrField */
    public IndividuumFeld(QueryManager manager,int[] projectKey) {
        super();
        this.projectKey=projectKey;
        this.manager = manager;
        this.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent keyEvt) {
			}
			public void keyReleased(KeyEvent keyEvt) {
				int pressedCharIntValue = Character.getNumericValue(keyEvt.getKeyChar());
				int actionValue = Character.getNumericValue('n');
				if(pressedCharIntValue == actionValue){
					setText(getNextFreeIndividualNr() + "");
				}
			}

			public void keyTyped(KeyEvent keyEvt) {
			}
		});
    }
	private int getNextFreeIndividualNr() {
		try {
			return manager.getNextFreeIndividualNr(projectKey)+1;
		} catch (StatementNotExecutedException e) {
			e.printStackTrace();
			return 0;
		}
	}
    
    public boolean condition() {
        boolean result = false;
        try {
            Integer.parseInt(getText());
            result = true;
        } catch (NumberFormatException e) {
            result = false;
        }

        if (result) {
        	try{
	        	int zahl = manager.getCountKnIndNr(Integer.parseInt(getText()), projectKey);
	        	if (zahl > 0) {
	        		setBackground(Color.YELLOW);
	        	} else {
	        		setBackground(null);
	        	}
        	}
        	catch (StatementNotExecutedException s){
        		setBackground(null);
        	}
        }
        if (this.getText().equals("")) {
            result = true;
        }
        return result;
    }

    /* (non-Javadoc)
     * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
     */
    public void useDefaultValue() {
        this.setText("");
    }
    
}
