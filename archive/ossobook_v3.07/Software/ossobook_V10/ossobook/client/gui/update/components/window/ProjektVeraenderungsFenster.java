/*
 * Created on 09.06.2001
 */
package ossobook.client.gui.update.components.window;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ossobook.client.config.KonfigurationOssobook;
import ossobook.client.gui.update.components.other.GewoehnlichesSelbstkorrekturTextFeld;
import ossobook.client.gui.update.components.other.ProjektPanel;
import ossobook.client.gui.update.elements.other.NotizButton;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.QueryManager;
import ossobook.client.gui.common.*;

/**
 * @author ali
 * 
 */
public class ProjektVeraenderungsFenster extends JInternalFrame {

	private QueryManager manager;
	private int[] projectKey;
	private JPanel maske;
	private OssobookHauptfenster parent;
	private ProjektPanel window;

	public ProjektVeraenderungsFenster(QueryManager manager, int[] projectKey,
			OssobookHauptfenster parentReference, ProjektPanel window ) {
		super("Projekt Mutation");
		this.parent = parentReference;
		this.manager=manager;
		this.projectKey = projectKey;
		this.window=window;
		settings();
		this.maske = generiereMaske();
		this.setContentPane(maske);
	}

	private void settings() {
		this.setSize(KonfigurationOssobook.projektveraenderungsfenster_x, KonfigurationOssobook.projektveraenderungsfenster_y);
		this.setVisible(true);
		this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		this.setResizable(true);
		this.setMaximizable(true);
		this.setIconifiable(true);
		this.setClosable(true);
		this.setVisible(true);
	}

	/**
	 * 
	 */
	private JPanel generiereMaske() {
		JPanel result = new JPanel();
		try{
			String[][] info = manager.getProjektInformations(projectKey);
		
			result.setLayout(new GridLayout(((info.length-1) * 2 / 3),
				3));
			for (int i = 0; i < info.length; i++) {
				// Unterscheidung f\u00FCr Feldarten
				// Memo-Felder
				if ((info[i][0].equals("projektNotiz")) || info[i][0].equals("befundNotiz")) {
					result.add(new JLabel(info[i][0]));
					NotizButton notebutton = new NotizButton(info[i][0],
							parent);
					notebutton.setNotizFeldText(info[i][1]);
					result.add(notebutton);
				} else {
					if(!info[i][0].equals("Zustand")&&!info[i][0].equals("Datum")){
						result.add(new JLabel(info[i][0]));
						GewoehnlichesSelbstkorrekturTextFeld cvtext = new GewoehnlichesSelbstkorrekturTextFeld
							(info[i][1]);
						if (info[i][0].equals("ProjNr") || info[i][0].equals("ProjEigentuemer")
								|| info[i][0].equals("zuletztSynchronisiert") || info[i][0].equals("Datenbanknummer")
								|| info[i][0].equals("geloescht") || info[i][0].equals("Nachrichtennummer")){
							cvtext.setEnabled(false);
						} else {
							cvtext.setEnabled(true);
						}
						result.add(cvtext);
					}
				}
			}
		}catch(StatementNotExecutedException e){}
		
		JButton UpdateButton = new JButton("Update");
		UpdateButton.setBackground(Color.YELLOW);
		UpdateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateAction();
			}
		});
		try{
			if(!(manager.getRight(projectKey).equals("schreiben"))&& !manager.getIsAdmin()) UpdateButton.setEnabled(false);
		}
		catch(StatementNotExecutedException e){}
		result.add(UpdateButton);
		return result;
	}

	/**
	 * 
	 */
	private void updateAction() {
		// STUB Updateaction f\u00FCr projektbutton
		int components = maske.getComponentCount() - 1; // der Knopf wird nicht
		String text = "";
		JLabel label = null;
		// gez\u00E4hlt
		for (int i = 0; i < components; i = i + 2) {
			GewoehnlichesSelbstkorrekturTextFeld ctext = null;
			NotizButton notebtn = null;
			// TODO Fallunterscheidung f\u00FCr Notizfelder
			label = (JLabel) maske.getComponent(i);
			if ((label.getText().equals("projektNotiz"))
					|| (label.getText().equals("befundNotiz"))) {
				notebtn = (NotizButton) maske.getComponent(i + 1);
				text = notebtn.toString();
			} else {
				ctext = (GewoehnlichesSelbstkorrekturTextFeld) maske
						.getComponent(i + 1);
				text = ctext.getText();
			}
			try{
				manager.updateProjekt(label.getText(), text, projectKey);
			}
			catch (StatementNotExecutedException s){}
		}
		
		window.updateInfo();
		this.dispose();

	}
}
