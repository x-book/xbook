/*
 * Created on 15.06.2001
 */
package ossobook.client.gui.update.components.other;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;

import javax.swing.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.client.gui.common.*;
import ossobook.client.gui.update.components.window.*;
import ossobook.client.io.database.dbinfos.ProjektInformationen;
import ossobook.sqlQueries.QueryManager;

/**
 * @author ali
 *  
 */
public class ProjektPanel extends JPanel {
	private static Log _log = LogFactory.getLog(ProjektPanel.class);
    private JTextArea ProjektInfo;
    private QueryManager manager;
    private int[] projectKey;
    private ProjektPanel thispanel;
    private ProjektFenster desktop;
	private OssobookHauptfenster mainframe;

    //    private JPanel thispanel;

  
    public ProjektPanel(QueryManager manager, int[] projectKey, ProjektFenster desktop, OssobookHauptfenster mainframe) {
        this.manager=manager;
        this.projectKey = projectKey;
        this.thispanel = this;
        this.desktop = desktop;
        this.mainframe=mainframe;
        layouteinstellung();
        updateInfo();

    }

    /**
     * 
     */
    private void layouteinstellung() {
        this.setLayout(new GridLayout(1,1));
    }

    public void updateInfo() {
        try {
            removeAll();
        } catch (Exception e) {
        }
        //Projektinformationen
        ProjektInfo = new JTextArea();
        String query = "SELECT * FROM projekt WHERE ProjNr=" + projectKey[0] + " " +
        		"AND Datenbanknummer="+projectKey[1]+" AND geloescht='N';";
        if (_log.isDebugEnabled()) {
        	_log.debug("Query: " + query);
        	}
        ProjektInformationen pinfo = new ProjektInformationen(manager.getConnection(),
                query,
                "Projekt Informationen: \n----------------\n");
        ProjektInfo.setText(pinfo.getProjektInfo());
        ProjektInfo.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                ProjektVeraenderungsFenster f = new ProjektVeraenderungsFenster(
                        manager, projectKey, mainframe, thispanel);
                mainframe.add(f);
                try {
                    f.setSelected(true);
                } catch (PropertyVetoException e1) {

                }
            }
        });
        JScrollPane sonstigeInfosSP = new JScrollPane(ProjektInfo);
        add(sonstigeInfosSP);
    }

	public OssobookHauptfenster getMainframe() {
		return mainframe;
	}

}
