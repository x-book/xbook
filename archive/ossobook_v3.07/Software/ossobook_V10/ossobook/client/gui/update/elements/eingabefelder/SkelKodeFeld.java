/*
 * SkelCodeField.java
 *
 * Created on 24. M\u00E4rz 2003, 16:42
 */
package ossobook.client.gui.update.elements.eingabefelder;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;
import ossobook.client.gui.update.components.window.ProjektFenster;
import ossobook.client.gui.update.elements.other.MassKnopf;

/**
 * @author ali
 */
public class SkelKodeFeld extends AbstraktesSelbstkorrekturKodeFeld {

	private KnochenTeilKodeFeld knochenteilfield;

	private MassKnopf massknopf;

	/** Creates a new instance of SkelCodeField */
	public SkelKodeFeld() {
		super();
        this.addFocusListener(new FocusAdapter() {   //on lostFocus : check
            public void focusLost(FocusEvent evt) {
				try {
					massknopf.reset();
				} catch (Exception e) {
				}
            }         
        });
	}

	public boolean condition() {
		boolean result = false;
		try {
			Integer i = new Integer(Integer.parseInt(this.getText()));
			String s = ProjektFenster.skellist.getTeil(i);
			if (!s.equals("")) {
				tb.setTitle(s);
				result = true;
				// bei \u00C4nderung knochenteilfield auf "" setzen
				// this.knochenteilfield.setText("");
				this.knochenteilfield.setForeground(Color.red);

			} else {
				tb.setTitle("");
				result = false;
			}

		} catch (Exception e) {
			tb.setTitle("");
			result = false;
		}

		return result;
	} /*
		 * (non-Javadoc)
		 * 
		 * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
		 */

	public void useDefaultValue() {
	}

	/**
	 * @param field
	 */
	public void setKnochenTeilField(KnochenTeilKodeFeld field) {
		this.knochenteilfield = field;
	}

	public void setMassKnopf(MassKnopf masse) {
		this.massknopf = masse;
	}
}
