/*
 * Created on 22.07.2004
 *

 */
package ossobook.client.gui.update.components.window;

import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import ossobook.client.config.KonfigurationOssobook;
import ossobook.client.gui.common.ProjektBasisFenster;
import ossobook.client.gui.update.components.other.GewoehnlichesSelbstkorrekturTextFeld;




/**
 * @author ali
 * 
 */
public class MassFenster extends ProjektBasisFenster {

	private JComponent[] felder;

	private JPanel p;

	/**
	 * @param projektname
	 */
	public MassFenster() {
		super("Mass Eingabe");
		setVisible(false);
		setClosable(false);
		setSize(KonfigurationOssobook.massfenster_x, KonfigurationOssobook.massfenster_y);
	}

	public JComponent[] getMassFields() {
		return this.felder;
	}

	public void konstruiereMassEingabe(
			GewoehnlichesSelbstkorrekturTextFeld[] felder, String[] bezeichner,
			String[] werte) {
		p = new JPanel();
		int x = felder.length / 2;
		p.setLayout(new GridLayout(x, 2));
		JPanel temppanel = new JPanel();
		this.felder = felder;
		for (int i = 0; i < felder.length; i++) {
			temppanel = new JPanel();
			GewoehnlichesSelbstkorrekturTextFeld feld = felder[i];
			feld = addListener(feld);
			if(werte.length>0) feld.setText(werte[i]);
			temppanel.add(feld);
			temppanel.setBorder(new TitledBorder(bezeichner[i]));
			p.add(temppanel);
		}

		JScrollPane sp = new JScrollPane(p);
		this.setContentPane(sp);
	}

	private GewoehnlichesSelbstkorrekturTextFeld addListener(
			final GewoehnlichesSelbstkorrekturTextFeld feld) {
		feld.addKeyListener(new KeyListener() {

			public void keyTyped(KeyEvent arg0) {

			}

			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					feld.nextFocus();
				}
			}

			public void keyReleased(KeyEvent arg0) {

			}

		});
		return feld;
	}

	/**
	 * 
	 */
	public void reset() {
		this.felder = null;
	}

}