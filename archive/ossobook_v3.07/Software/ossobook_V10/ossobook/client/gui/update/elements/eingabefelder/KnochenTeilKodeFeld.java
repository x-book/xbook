package ossobook.client.gui.update.elements.eingabefelder;


import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;
import ossobook.client.gui.update.components.window.ProjektFenster;

/**
 * @author ali
 */
public class KnochenTeilKodeFeld extends AbstraktesSelbstkorrekturKodeFeld {
	private SkelKodeFeld skelcodefield;

	/** Creates a new instance of SkelCodeField */
	public KnochenTeilKodeFeld(SkelKodeFeld skelcodefield) {
		super();
		this.skelcodefield = skelcodefield;
		skelcodefield.setKnochenTeilField(this);

	}

	public boolean condition() {
		boolean result = false;
		setForeground(null);
		try {
			int i = Integer.parseInt(this.getText());
			result = true;
		} catch (NumberFormatException e) {
			result = false;
		}
		if (result == true) {
			//falls keine Aufl\u00F6sung m\u00F6glich
			try {
				Integer i = new Integer(this.getText());
				Integer j = new Integer(this.skelcodefield.getText());
				tb.setTitle(ProjektFenster.knochenteillist.getTeil(i
						+ "," + j));
			} catch (Exception e) {
				// TODO NoSuchPartException
			}
		}
		if (this.getText().equals("")) {
			result = true;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
	 */
	public void useDefaultValue() {
	}

}
