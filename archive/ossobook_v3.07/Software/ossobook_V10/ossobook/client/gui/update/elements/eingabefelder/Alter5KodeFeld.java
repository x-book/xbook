/*
 * AnimalCodeField.java
 *
 * Created on 19. M\u00E4rz 2003, 18:22
 */
package ossobook.client.gui.update.elements.eingabefelder;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;


/**
 * @author ali
 */
public class Alter5KodeFeld extends AbstraktesSelbstkorrekturKodeFeld {

    /** Creates a new instance of AnimalCodeField */
    public Alter5KodeFeld() {
        super();

    }

    public boolean condition() {
        boolean result = true;
        if (getText().equals("")) {
            result = true;
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
     */
    public void useDefaultValue() {
    }
}