/*
 * SkelCodeField.java
 *
 * Created on 24. M\u00E4rz 2003, 16:42
 */
package ossobook.client.gui.update.components.other;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.border.TitledBorder;

import ossobook.client.config.KonfigurationOssobook;


/**
 * @author ali
 */
public abstract class AbstraktesSelbstkorrekturKodeFeld extends
		AbstraktesSelbstkorrekturTextFeld {
	protected TitledBorder tb;

	public AbstraktesSelbstkorrekturKodeFeld() {
		super();
		settings();
	}

	private void settings() {
		tb = new TitledBorder("");
		Font f = new Font("CodeTitelFont", Font.PLAIN, KonfigurationOssobook.codeTitelFont);
		Font cf = new Font("CodeFont", Font.PLAIN, KonfigurationOssobook.codeFont);
		// NOTE hier wird Titelposition f\u00FCr Codefelder festgelegt
		// tb.setTitlePosition(TitledBorder.LEFT);
		// NOTE hier wird TitelSchrift f\u00FCr Codefelder festgelegt //
		tb.setTitleFont(f);
		// NOTE hier wird die Feldgr\u00F6sse festgelegt
		setMargin(new Insets(0,0,0,0));
		setFont(cf);
		setPreferredSize(new Dimension(KonfigurationOssobook.selbstkorrekturfeld_x, KonfigurationOssobook.selbstkorrekturfeld_y));
		setBorder(tb);
	}

	public void reset() {
		setText("");
		tb.setTitle("");
	}
}
