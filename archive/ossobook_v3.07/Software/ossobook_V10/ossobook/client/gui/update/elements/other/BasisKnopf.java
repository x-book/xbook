package ossobook.client.gui.update.elements.other;

import javax.swing.JButton;

/**
 * The OssobookButtons SuperClass
 * 
 * @author ali
 */
public class BasisKnopf extends JButton {
	public BasisKnopf() {
	}

	/**
	 * @param Title
	 */
	public BasisKnopf(String Title) {
		super(Title);
	}

	public String toString() {
		return "KEINE EINGABE";
	}

}