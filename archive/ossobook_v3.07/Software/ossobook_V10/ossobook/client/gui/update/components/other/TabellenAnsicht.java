/*
 * Created on 25.04.2005
 */
package ossobook.client.gui.update.components.other;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;

import javax.swing.JInternalFrame;
import javax.swing.JTable;

import ossobook.client.gui.update.components.window.ProjektFenster;

/**
 * @author ali
 * 
 */
public class TabellenAnsicht extends JTable {
	private JInternalFrame parentContainer;

	// NOTE so werden zwecke f\u00FCr Tabellenausgebe definiert
	public static int DATENSATZSUCHE = 1;
	public static int TIERARTSUCHE = 2;
	public static int SKELTEILSUCHE = 3;
	public static int BRUCHKANTESUCHE = 4;
	public static int ERHALTUNGSUCHE = 5;
	private ProjektFenster projekt;
	public static final int ALTER1SUCHE = 6;
	public static final int ALTER2SUCHE = 7;
	public static final int ALTER3SUCHE = 8;
	public static final int ALTER4SUCHE = 9;
	public static final int ALTER5SUCHE = 10;
	public static final int WURZELFRASS = 11;
	public static final int VERSINTERUNG = 12;
	public static final int FETTIGSUCHE = 13;
	public static final int PATINASUCHE = 14;
	public static final int BRANDSPURSUCHE = 15;
	public static final int VERBISSSUCHE = 16;
	public static final int SCHLACHTSPUR1 = 17;
	public static final int SCHLACHTSPUR2 = 18;
	public static final int GESCHLECHT = 19;
	public static final int BRUCHKANTE2 = 20;

	public TabellenAnsicht() {

	}

	/**
	 * @param arr
	 * @param s
	 */
	public TabellenAnsicht(Object[][] arr, String[] s,
			ProjektFenster projekt, final int zweck) {

		super(arr, s);
		this.projekt = projekt;
		if (zweck == TabellenAnsicht.DATENSATZSUCHE) {
			this.setAutoResizeMode(TabellenAnsicht.AUTO_RESIZE_OFF);
		}

		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				mausaktion(zweck);

			}
		});

	}

	/**
	 * 
	 */
	protected void mausaktion(int zweck) {
		// NOTE so werden verschiedene Aktionen f\u00FCr die Tabellen ausgabe der
		// Tools implementiert
		int i = 0;
		if (zweck == DATENSATZSUCHE) {
			int[] key=new int[2];
			key[1] = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			key[0] = Integer.parseInt((String) (getValueAt(getSelectedRow(), 1)));
			projekt.searchValues(key);
		}
		if (zweck == TIERARTSUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setTierArtField(i);
		}
		if (zweck == SKELTEILSUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setSkelCodeField(i);
		}
		if (zweck == BRUCHKANTESUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setBruchkanteCodeField(i);
		}
		if (zweck == ERHALTUNGSUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setErhaltungCodeField(i);
		}
		if (zweck == ALTER1SUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setAlter1CodeField(i);
		}
		if (zweck == ALTER2SUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setAlter2CodeField(i);
		}
		if (zweck == ALTER3SUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setAlter3CodeField(i);
		}
		if (zweck == ALTER4SUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setAlter4CodeField(i);
		}
		if (zweck == ALTER5SUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setAlter5CodeField(i);
		}

		if (zweck == WURZELFRASS) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setWurzelfrassField(i);
		}
		if (zweck == VERSINTERUNG) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setVersinterungField(i);
		}
		if (zweck == FETTIGSUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setFettField(i);
		}
		if (zweck == PATINASUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setPatinaField(i);
		}
		if (zweck == BRANDSPURSUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setBrandspurField(i);
		}
		if (zweck == VERBISSSUCHE) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setVerbissField(i);
		}
		if (zweck == SCHLACHTSPUR1) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setSchlachtS1Field(i);
		}
		if (zweck == SCHLACHTSPUR2) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setSchlachtS2Field(i);
		}
		if (zweck == GESCHLECHT) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setGeschlechtsField(i);
		}
		if (zweck == BRUCHKANTE2) {
			i = Integer.parseInt((String) (getValueAt(getSelectedRow(), 0)));
			projekt.setBruchkanten2Field(i);
		}
		try {
			if (!(zweck == DATENSATZSUCHE)) {
				projekt.setSelected(true);
			}

		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		if (!(zweck == DATENSATZSUCHE)) {
			this.parentContainer.dispose();
		}
	}

	/**
	 * @param intFrame
	 */
	public void setParentContainer(JInternalFrame intFrame) {
		this.parentContainer = intFrame;
	}
}
