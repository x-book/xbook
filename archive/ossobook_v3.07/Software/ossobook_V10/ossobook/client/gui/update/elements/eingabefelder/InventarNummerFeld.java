package ossobook.client.gui.update.elements.eingabefelder;

import java.awt.Color;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturTextFeld;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.QueryManager;




/*
 * ItemNrField.java
 *
 * Created on 21. M\u00E4rz 2003, 15:34
 */

/**
 * 
 * @author ali
 */
public class InventarNummerFeld extends AbstraktesSelbstkorrekturTextFeld {

    private QueryManager manager;
    private int[] projectKey;

    /** Creates a new instance of ItemNrField */
    public InventarNummerFeld(QueryManager manager,int[] projectKey) {
        super();
        this.projectKey=projectKey;
        this.manager=manager;
    }

    public boolean condition() {
        boolean result = true;
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
     */
    public void useDefaultValue() {
        this.setText("");
    }

}
