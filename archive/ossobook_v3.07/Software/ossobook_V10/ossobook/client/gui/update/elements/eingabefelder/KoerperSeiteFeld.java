/*
 * SexField.java
 *
 * Created on 19. M\u00E4rz 2003, 17:44
 */
package ossobook.client.gui.update.elements.eingabefelder;

import javax.swing.JComboBox;

/**
 * 
 * @author ali
 */
public class KoerperSeiteFeld extends JComboBox {

	/** Creates a new instance of SexField */
	public KoerperSeiteFeld() {
		super();
		addItem("indet");
		addItem("r");
		addItem("l");
	}

	public String toString() {
		return getSelectedItem().toString();
	}
}