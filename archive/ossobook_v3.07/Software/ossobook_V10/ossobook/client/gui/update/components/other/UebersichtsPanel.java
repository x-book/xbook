/*
 * Created on 23.07.2004
 */
package ossobook.client.gui.update.components.other;

import java.awt.GridLayout;

import javax.swing.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.client.io.database.dbinfos.*;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.QueryManager;

/**
 * @author ali
 */
public class UebersichtsPanel extends JPanel {
	private static Log _log = LogFactory.getLog(UebersichtsPanel.class);
	private JTextArea Fundkomplexe, AnzahlKnochen, TierartenAnzahl;
	private int[] projectKey;
	private QueryManager manager;

	public UebersichtsPanel(QueryManager manager, int[] projectKey,
			JDesktopPane desktop) {
		this.setToolTipText("Uebersicht");
		this.manager = manager;
		this.projectKey = projectKey;
		this.setLayout(new GridLayout(2, 3));
		createFields();
	}

	private void createFields() {
		updateInfo();
	}

	/**
	 * TODO: only if this tab is selected.
	 */
	public void updateInfo() {
		if (_log.isInfoEnabled()) {
			_log.info("updating stats");
		}
		try {
			removeAll();
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Problem in function updateInfo()");
				e.printStackTrace();
			}
		}
		this.Fundkomplexe = new JTextArea();
		this.AnzahlKnochen = new JTextArea();
		this.TierartenAnzahl = new JTextArea();

		String s = "";
		String query = "";
		String header = "";

		// Welche Fundkomplexe sind schon angefangen
		header = "Fundkomplexe / Knochen \n -----------\n";
		query = "SELECT SUM(anzahl) AS Summe, fK FROM eingabeeinheit WHERE projNr='"
				+ projectKey[0]
				+ "' AND DBNummerProjekt="
				+ projectKey[1]
				+ " AND geloescht='N' GROUP BY fK;";
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query);
		}
		FKUebersicht fkuebersicht = new FKUebersicht(manager.getConnection(),
				query, header);
		Fundkomplexe.setText(fkuebersicht.getProjektInfo());
		JScrollPane fundkomplexeSP = new JScrollPane(Fundkomplexe);

		// Anzahl Aufgenommene Knochen
		header = "Anzahl Knochen: ";
		query = "SELECT SUM(Anzahl) as SUMAnzahl FROM eingabeeinheit WHERE projNr='"
				+ projectKey[0]
				+ "' AND DBNummerProjekt="
				+ projectKey[1]
				+ " AND geloescht='N';";
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query);
		}

		AnzahlKnochenUebersicht aKnochen = new AnzahlKnochenUebersicht(manager
				.getConnection(), query, header);
		AnzahlKnochen.setText(aKnochen.getProjektInfo());
		JScrollPane anzahlKnochenSP = new JScrollPane(AnzahlKnochen);

		// Tierarten Anzahl
		header = "Tierarten / n \n -----------\n";
		query = "SELECT TierName, SUM(anzahl) AS Summe FROM eingabeeinheit, tierart WHERE projNr='"
				+ projectKey[0]
				+ "' AND DBNummerProjekt='"
				+ projectKey[1]
				+ "' AND tierart.TierCode=eingabeeinheit.tierart AND eingabeeinheit.geloescht='N' "
				+ "GROUP BY TierName ORDER BY Summe DESC;";
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query);
		}

		TierartenUebersicht tuebersicht = new TierartenUebersicht(manager
				.getConnection(), query, header);
		TierartenAnzahl.setText(tuebersicht.getProjektInfo());
		JScrollPane tierartenSP = new JScrollPane(TierartenAnzahl);

		// Anzahl Aufgenommene Artefakte
		header = "Anzahl Artefakte: ";
		query = "SELECT COUNT(artefaktmaske.ID) as IDAnzahl FROM artefaktmaske,eingabeeinheit WHERE eingabeeinheit.projNr='"
				+ projectKey[0]
				+ "' AND eingabeeinheit.DBNummerProjekt="
				+ projectKey[1]
				+ " AND artefaktmaske.ID=eingabeeinheit.artefaktID"
				+ " AND artefaktmaske.Datenbanknummer=eingabeeinheit.Datenbanknummer"
				+ " AND artefaktmaske.geloescht='N' AND eingabeeinheit.geloescht='N';";
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query);
		}

		AnzahlArtefakteUebersicht anzahlArtefakte = new AnzahlArtefakteUebersicht(
				manager.getConnection(), query, header);
		AnzahlKnochen.setText(AnzahlKnochen.getText() + "\n"
				+ anzahlArtefakte.getProjektInfo());

		try {
			// Sonstige informationen: gewichtsdurchschnitt
			// Wieviele Knochen?
			int anzahl;
			anzahl = manager.getBonesNumber(projectKey);
			// gesamtgewicht?
			double gewicht;
			gewicht = manager.getOverallWeight(projectKey);

			s = s + "Gewicht/Knochen: ";
			s = s + (gewicht / anzahl);
			AnzahlKnochen.setText(AnzahlKnochen.getText() + "\n" + s);
		} catch (StatementNotExecutedException e) {
		}
		// Montage
		this.add(fundkomplexeSP);
		this.add(tierartenSP);
		this.add(anzahlKnochenSP);
	}
}