package ossobook.client.gui.update.components.window;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.*;

import ossobook.client.base.codelists.*;
import ossobook.client.base.scheme.*;
import ossobook.client.config.KonfigurationOssobook;
import ossobook.client.gui.common.*;
import ossobook.client.gui.update.components.other.*;
import ossobook.client.gui.update.elements.eingabefelder.*;
import ossobook.client.gui.update.elements.other.*;
import ossobook.client.io.database.modell.BasisFeldJava2Db;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.QueryManager;

/**
 * The Specific Ossobook Bone Project.
 * 
 * @author ali
 */
@SuppressWarnings("serial")
public class ProjektFenster extends ProjektBasisFenster {
	private static Log _log = LogFactory.getLog(ProjektFenster.class);

	public static Alter1Liste alter1list;

	public static Alter2Liste alter2list;

	public static Alter3Liste alter3list;

	public static Alter4Liste alter4list;

	public static Alter5Liste alter5list;

	public static TierartListe animallist;

	public static BruchkantenListe bruchkantenlist;

	public static Bruchkanten2Liste bruchkanten2list;

	public static ErhaltungListe erhaltunglist;

	public static WurzelfrassListe wurzelfrasslist;

	public static SkelettteilListe skellist;

	public static KnochenTeilListe knochenteillist;

	private int[] artefactID;

	private int[] recordID;

	private int[] massID;

	private ArtefaktSchablone artefaktTemplate;

	private OssobookHauptfenster mainframe;

	// ANFANG BENUTZER INFORMATIONEN
	// private int benutzerNummer;
	private JPanel commandPanel;

	private QueryManager manager;

	private JButton deleteButton;

	// ENDE BENUTZER INFORMATIONEN
	private UnbestimmtSchablone indetTemplate;

	// Elemente von CommandPanel
	private JButton insertButton;

	private KnochenSchablone knochenTemplate;

	private JTabbedPane maintab;

	private FehlernachrichtenFenster messageFrame;

	private JPanel p1, p2, p3, p4, p, pcontent, artefaktpanel;

	private int[] projectKey;

	private UebersichtsPanel ueberblickpanel;

	private KonkordanzPanel konkordanzpanel1;

	private KonkordanzPanel konkordanzpanel2;

	private KonkordanzPanel konkordanzpanel3;

	private KonkordanzPanel konkordanzpanel4;

	// Variable f\u00FCr Massinformationen
	private MassKnopf massComponente = null;

	private JDesktopPane desktop;

	private JButton emptyMaskButton;

	public static VersinterungListe versinterunglist;

	public static FettListe fettlist;

	public static PatinaListe patinalist;

	public static BrandspurListe brandspurlist;

	public static VerbissListe verbisslist;

	public static Schlachtspur1Liste schlachtspur1list;

	public static Schlachtspur2Liste schlachtspur2list;

	public static GeschlechtsListe geschlechtslist;

	// ENDE ELEMENTE VON COMMANDPANEL

	public ProjektFenster(String projektname, QueryManager manager,
			UnbestimmtSchablone indetTemplate,
			KnochenSchablone knochenTemplate,
			ArtefaktSchablone artefaktTemplate, int[] projectKey,
			ProjectManipulation parent, OssobookHauptfenster mainframe,
			JDesktopPane desktop) {

		super(projektname);

		parent.addInternalFrameListener(new InternalFrameAdapter() {
			public void internalFrameClosing(InternalFrameEvent arg0) {
				messageFrame.doDefaultCloseAction();
			}
		});
		// Feldzuweisungen
		indetTemplate.setParent(this);
		knochenTemplate.setParent(this);
		artefaktTemplate.setParent(this);
		this.indetTemplate = indetTemplate;
		this.knochenTemplate = knochenTemplate;
		this.artefaktTemplate = artefaktTemplate;
		this.manager = manager;
		this.projectKey = projectKey;
		this.desktop = desktop;
		this.mainframe = mainframe;
		this.messageFrame = new FehlernachrichtenFenster(projektname,
				(int) mainframe.getSize().getWidth()
						- KonfigurationOssobook.projektbasisfensterBorderRight,
				0);
		// produziere CodeListen
		ProjektFenster.animallist = new TierartListe(manager.getConnection());
		ProjektFenster.skellist = new SkelettteilListe(manager.getConnection());
		ProjektFenster.knochenteillist = new KnochenTeilListe(manager
				.getConnection());
		ProjektFenster.alter1list = new Alter1Liste(manager.getConnection());
		ProjektFenster.alter2list = new Alter2Liste(manager.getConnection());
		ProjektFenster.alter3list = new Alter3Liste(manager.getConnection());
		ProjektFenster.alter4list = new Alter4Liste(manager.getConnection());
		ProjektFenster.alter5list = new Alter5Liste(manager.getConnection());
		ProjektFenster.bruchkantenlist = new BruchkantenListe(manager
				.getConnection());
		ProjektFenster.bruchkanten2list = new Bruchkanten2Liste(manager
				.getConnection());
		ProjektFenster.erhaltunglist = new ErhaltungListe(manager
				.getConnection());
		ProjektFenster.wurzelfrasslist = new WurzelfrassListe(manager
				.getConnection());
		ProjektFenster.versinterunglist = new VersinterungListe(manager
				.getConnection());
		ProjektFenster.fettlist = new FettListe(manager.getConnection());
		ProjektFenster.patinalist = new PatinaListe(manager.getConnection());
		ProjektFenster.brandspurlist = new BrandspurListe(manager
				.getConnection());
		ProjektFenster.verbisslist = new VerbissListe(manager.getConnection());
		ProjektFenster.schlachtspur1list = new Schlachtspur1Liste(manager
				.getConnection());
		ProjektFenster.schlachtspur2list = new Schlachtspur2Liste(manager
				.getConnection());
		ProjektFenster.geschlechtslist = new GeschlechtsListe(manager
				.getConnection());

		// Montage der Tabs
		maintab = new JTabbedPane();
		maintab.addTab("Eingabe", produziereFelderHauptmaske());
		this.artefaktpanel = produziereFelderArtefaktmaske();
		maintab.addTab("Artefakt", this.artefaktpanel);
		this.ueberblickpanel = produziereUeberblick();
		maintab.addTab("\u00DCberblick", this.ueberblickpanel);
		maintab.addTab("Projekt", new ProjektPanel(manager, projectKey, this,
				mainframe));
		this.konkordanzpanel1 = new KonkordanzPanel(manager, projectKey, this,
				"phase1");
		this.konkordanzpanel2 = new KonkordanzPanel(manager, projectKey, this,
				"phase2");
		this.konkordanzpanel3 = new KonkordanzPanel(manager, projectKey, this,
				"phase3");
		this.konkordanzpanel4 = new KonkordanzPanel(manager, projectKey, this,
				"phase4");
		maintab.addTab("FK-Konkordanz 1", this.konkordanzpanel1);
		maintab.addTab("FK-Konkordanz 2", this.konkordanzpanel2);
		maintab.addTab("FK-Konkordanz 3", this.konkordanzpanel3);
		maintab.addTab("FK-Konkordanz 4", this.konkordanzpanel4);
		maintab.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JTabbedPane activePane = (JTabbedPane) e.getSource();
				int selectionIndex = activePane.getSelectedIndex();
				if (selectionIndex == 2) {
					if (_log.isDebugEnabled()) {
						_log.debug("ueberblick selected.");
					}
					updateUeberblick();
				} else if (selectionIndex == 3) {
					if (_log.isDebugEnabled()) {
						_log.debug("projekt selected.");
					}

				} else if (selectionIndex == 4) {
					if (_log.isDebugEnabled()) {
						_log.debug("Konkordanz 1 selected.");
					}
					updateKonkordanz1();
				} else if (selectionIndex == 5) {
					if (_log.isDebugEnabled()) {
						_log.debug("Konkordanz 2  selected.");
					}
					updateKonkordanz2();
				} else if (selectionIndex == 6) {
					if (_log.isDebugEnabled()) {
						_log.debug("Konkordanz 3  selected.");
					}
					updateKonkordanz3();
				} else if (selectionIndex == 7) {
					if (_log.isDebugEnabled()) {
						_log.debug("Konkordanz 4  selected.");
					}
					updateKonkordanz4();
				}
			}
		});
		this.setContentPane(maintab);
		this.desktop.add(messageFrame);
		// prob fokus
		fokussiereErstesFeld();
	}

	/**
	 * @return JPanel which Insert, update or Delete Commands
	 */
	private JPanel createCommandPanel() {
		JPanel result = new JPanel();
		insertButton = new JButton("Einf\u00FCgen");
		insertButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (_log.isTraceEnabled()) {
					_log.trace("starting insert.");
				}
				double time = System.currentTimeMillis();
				insertValues();
				fokussiereErstesFeld();
				time = System.currentTimeMillis() - time;
				if (_log.isTraceEnabled()) {
					_log.trace(this.getClass().getName()
							+ ": time used for insert: " + time);
				}
			}

		});

		deleteButton = new JButton("L\u00F6schen");
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteValues("delete");
				emptyMask();
				// prob fokus
				fokussiereErstesFeld();
			}

		});

		emptyMaskButton = new JButton("Maske Leeren");
		emptyMaskButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				emptyMask();
				fokussiereErstesFeld();
			}

		});

		result.add(insertButton);
		result.add(deleteButton);
		result.add(emptyMaskButton);
		// result.add(searchButton);

		try {
			// user may not write to the project
			if (!manager.getIsAdmin()
					&& !(manager.getRight(projectKey).equals("schreiben"))) {
				deleteButton.setEnabled(false);
				insertButton.setEnabled(false);
			}
		} catch (StatementNotExecutedException e) {
		}

		return result;
	}

	/**
	 * 
	 */
	// ist delete
	private void deleteValues(String source) {
		this.insertButton.setText("Einf\u00FCgen");
		try {
			manager.deleteFromEingabeeinheit(recordID);
			manager.deleteFromArtefaktmaske(artefactID);
			// Wenn delete oder Massknopf nicht Rot
			if (source.equals("delete")
					|| !(massComponente.getBackground() == Color.RED)) {
				manager.deleteFromMasse(massID);
			}
		} catch (StatementNotExecutedException e) {
		}

		this.knochenTemplate.loadActiveTemplate();
		this.artefaktTemplate.loadActiveTemplate();
		updateUeberblick();
	}

	private void emptyMask() {
		loescheHauptmaske();

		// Suche ArtefaktPanel
		JPanel jpa = getArtefaktPanel();

		// L\u00F6sche ArtefaktPanel
		loescheArtefaktPanel(jpa);
	}

	private void fokussiereErstesFeld() {
		JPanel tab = (JPanel) getEingabePanel();
		JPanel ueberPanel = (JPanel) tab.getComponent(0);
		JPanel firstPanel = (JPanel) ueberPanel.getComponent(0);

		int x = firstPanel.getComponentCount();
		for (int i = 0; i < x; i++) {
			JPanel ptemp = (JPanel) firstPanel.getComponent(i);
			try {
				AbstraktesSelbstkorrekturTextFeld ctemp0 = (AbstraktesSelbstkorrekturTextFeld) ptemp
						.getComponent(0);
				JCheckBox ctemp1 = (JCheckBox) ptemp.getComponent(1);
				if (!(ctemp1.isSelected())) {
					ctemp0.grabFocus();
					break;
				}
			} catch (Exception e) {
			}
		}
	}

	private void loescheHauptmaske() {
		for (int j = 0; j < p.getComponentCount(); j++) {
			p1 = (JPanel) p.getComponent(j);
			for (int i = 0; i < p1.getComponentCount(); i++) {

				JPanel p = (JPanel) p1.getComponent(i);

				JComponent c = (JComponent) p.getComponent(0);
				boolean loeschen = !((JCheckBox) p.getComponent(1))
						.isSelected();
				if (loeschen) {
					try {
						MassKnopf jmb = (MassKnopf) c;
						jmb.reset();
					} catch (Exception e) {
					}
					try {
						AbstraktesSelbstkorrekturTextFeld ctext = (AbstraktesSelbstkorrekturTextFeld) c;
						ctext.reset();
					} catch (Exception e) {
					}
					try {
						NotizButton n = (NotizButton) c;
						n.reset();
					} catch (Exception e) {
					}
					try {
						JCheckBox cbx = (JCheckBox) c;
						cbx.setSelected(false);
					} catch (Exception e) {

					}
					try {
						JComboBox jcb = (JComboBox) c;
						jcb.setSelectedIndex(0);
					} catch (Exception e) {
					}

				}

			}

		}
		messageFrame.updateMessageFrame("");

	}

	private void loescheArtefaktPanel(JPanel jpa) {
		JPanel jp = (JPanel) jpa.getComponent(0);
		for (int i = 0; i < jp.getComponentCount(); i++) {
			JPanel p = (JPanel) jp.getComponent(i);
			JComponent c = (JComponent) p.getComponent(0);
			boolean loeschen = !((JCheckBox) p.getComponent(1)).isSelected();
			if ((c.getClass().getSuperclass().getName()
					.equals("ossobook.framework.elements.AbstractValidatedTextField"))
					&& (loeschen)) {
				AbstraktesSelbstkorrekturTextFeld ctext = (AbstraktesSelbstkorrekturTextFeld) c;
				ctext.reset();
			}
		}
	}

	private JPanel getArtefaktPanel() {
		JPanel jpa = null;
		for (int i = 0; i < maintab.getComponentCount(); i++) {
			jpa = (JPanel) maintab.getComponent(i);
			try {
				if (jpa.getToolTipText().equals("Artefakt")) {
					break;
				}
			} catch (Exception e) {
			}
		}
		return jpa;
	}

	private JPanel getEingabePanel() {
		JPanel jpa = null;
		for (int i = 0; i < maintab.getComponentCount(); i++) {
			jpa = (JPanel) maintab.getComponent(i);
			try {
				if (jpa.getToolTipText().equals("Eingabe")) {
					break;
				}
			} catch (Exception e) {
			}
		}
		return jpa;
	}

	public int[] getMassID() {
		return massID;
	}

	/**
	 * @return Returns the connection.
	 */
	/*
	 * public Connection getConnection() { return connection; }
	 */

	public QueryManager getManager() {
		return manager;
	}

	/**
	 * 
	 */
	private JComponent getFieldByDbAequivalent(String a) {
		for (int j = 0; j < p.getComponentCount(); j++) {
			p1 = (JPanel) p.getComponent(j);
			for (int i = 0; i < p1.getComponentCount(); i++) {
				JPanel p = (JPanel) p1.getComponent(i);
				JComponent c = (JComponent) p.getComponent(0);
				if (c.getToolTipText().equals(a)) {
					return c;
				}
			}

		}
		return null;
	}

	/**
	 * @param name
	 * @return
	 */
	private JComponent getFieldByDbAequivalentArtefakt(String name) {
		JPanel jpa = null;
		for (int i = 0; i < maintab.getComponentCount(); i++) {
			jpa = (JPanel) maintab.getComponent(i);
			try {
				if (jpa.getToolTipText().equals("Artefakt")) {
					break;
				}
			} catch (Exception e) {
			}
		}
		JPanel parte = (JPanel) jpa.getComponent(0);
		for (int i = 0; i < parte.getComponentCount(); i++) {
			JPanel p = (JPanel) parte.getComponent(i);
			JComponent c = (JComponent) p.getComponent(0);
			if (c.getToolTipText().equals(name)) {
				return c;
			}
		}
		return null;
	}

	/**
	 * @return
	 */
	public int[] getProjectKey() {
		return projectKey;
	}

	/**
	 * @return
	 */
	public int getSkelCode() throws Exception {
		AbstraktesSelbstkorrekturTextFeld c = new GewoehnlichesSelbstkorrekturTextFeld();
		c = (AbstraktesSelbstkorrekturTextFeld) getFieldByDbAequivalent("skelettteil");
		return Integer.parseInt(c.toString());
	}

	/**
	 * @return
	 */
	public int getTierCode() throws Exception {
		AbstraktesSelbstkorrekturTextFeld c = (AbstraktesSelbstkorrekturTextFeld) getFieldByDbAequivalent("tierart");
		return Integer.parseInt(c.toString());
	}

	/**
	 * insert the Values if the validate_Form() method returns true insert and
	 * update substituted by update
	 */
	private void insertValues() {

		// Check ob Artefaktinformationen vorhanden sind

		// zugriff auf Artefaktpanel
		JPanel jpa = getArtefaktPanel();

		// Zugriff auf contentPane Panel von Artefakt
		JPanel jp = (JPanel) jpa.getComponent(0);

		// UPDATE statement variabeln
		Vector<String> fields = new Vector<String>();
		Vector<String> fieldsA = new Vector<String>();
		Vector<String> fieldsMasse = new Vector<String>();
		Vector<String> values = new Vector<String>();
		Vector<String> valuesA = new Vector<String>();
		Vector<String> valuesMasse = new Vector<String>();

		String phase1 = "''";
		String phase2 = "''";
		String phase3 = "''";
		String phase4 = "''";

		// Alle Artefakt eingabekomponenten durchsuchen und fieldsA bzw. valuesA
		// auff\u00FCllen
		for (int i = 0; i < jp.getComponentCount(); i++) {
			JPanel p = (JPanel) jp.getComponent(i);
			JComponent c = (JComponent) p.getComponent(0);
			if (!c.toString().equals("")) {
				fieldsA.add(c.getToolTipText());
				valuesA.add(c.toString());
			}
		}
		// Fehlerpr\u00FCfung Hauptmaske
		String fehler = "";
		boolean check = true;

		for (int j = 0; j < p.getComponentCount(); j++) {
			p1 = (JPanel) p.getComponent(j);
			for (int i = 0; i < p1.getComponentCount(); i++) {

				JPanel p = (JPanel) p1.getComponent(i);
				// Auslesen der Werte f\u00FCr Hauptmaske
				JComponent c = (JComponent) p.getComponent(0);
				// ausgeschlossen MassID -> Sonderbehandlung

				if (!c.getToolTipText().equals("massID")) {
					fields.add(c.getToolTipText());
					values.add(c.toString());
				} else {
					// Hole Massinfo
					massComponente = (MassKnopf) c;
					JComponent[] cmasse = new JComponent[0];
					try {

						cmasse = massComponente.getMasse();
					} catch (Exception e) {
					}

					// generiere MassEingabe
					// try {
					for (int y = 0; y < cmasse.length; y++) {
						GewoehnlichesSelbstkorrekturTextFeld mass = (GewoehnlichesSelbstkorrekturTextFeld) cmasse[y];
						fieldsMasse.add(mass.getToolTipText());
						String inhalt = mass.toString();
						if (inhalt.equals("")) {
							inhalt = "0";
						}
						valuesMasse.add(inhalt);
					}

					/*
					 * fieldsMasse = fieldsMasse.substring(0, fieldsMasse
					 * .lastIndexOf(",")); valuesMasse =
					 * valuesMasse.substring(0, valuesMasse .lastIndexOf(","));
					 */
					// } catch (Exception e) {
					// }
				}
				AbstraktesSelbstkorrekturTextFeld ctext = null;
				try {
					ctext = (AbstraktesSelbstkorrekturTextFeld) c;
					if (!ctext.check()) {
						check = false;// globale variable
						// informieren
						fehler = fehler + "- " + c.getToolTipText() + "\n";

					}
				} catch (Exception e1) {
				}

				// REMOVE }

			}
		}
		// Eintragen falls kein Fehler
		if (check) {
			// EINTRAGEN MASSE
			// String idvalueMasse = "";
			// if (this.insertButton.getText().equals("\u00E4ndern")) {
			// deleteValues();
			// idvorspannArte="ID,";
			// idvalueArte=this.aktuelleArtefaktDatensatzID+",";
			// }
			int[] massID = { -1, -1 };
			// Falls Masse masse vorhanden ABER nicht schon eingetragen (knopf:
			// rot)

			boolean zeroOnly = true;
			for (int i = 0; i < valuesMasse.size(); i++) {
				int value = Integer.parseInt(valuesMasse.get(i));
				if (value > 0) {
					zeroOnly = false;
				}
			}

			if (!valuesMasse.equals("")
					&& !(massComponente.getBackground() == Color.RED)
					&& !zeroOnly) {
				try {
					if (!insertButton.getText().equals("Einf\u00FCgen")) {
						massID = manager.getMassId(recordID);
						if (massID[0] != -1) {
							if (_log.isDebugEnabled()) {
								_log
										.debug("update Masse for massID: "
												+ massID);
							}
							manager.updateMasse(fieldsMasse, massID,
									valuesMasse);
						} else if (fieldsMasse.size() > 0) {
							massID = manager.insertIntoMasse(fieldsMasse,
									valuesMasse);
							if (_log.isDebugEnabled()) {
								_log.debug("insert Masse: " + massID);
							}
						}
					} else if (fieldsMasse.size() > 0) {
						massID = manager.insertIntoMasse(fieldsMasse,
								valuesMasse);
					}
				} catch (StatementNotExecutedException s) {
				}
				// Falls Knopf Rot DatensatzID \u00FCbernehmen
			} else if ((massComponente.getBackground() == Color.RED)) {
				massID = this.massID;
			}
			// EINTRAGEN ARTEFAKTE
			// if (this.insertButton.getText().equals("\u00E4ndern")) {
			// deleteValues();
			// idvorspannArte="ID,";
			// idvalueArte=this.aktuelleArtefaktDatensatzID+",";
			// }

			// falls Artefaktinfos vorhanden Artefakteintrag erstellen
			int[] ArtefaktID = { -1, -1 };
			/*
			 * if (!valuesA.equals("")) { fieldsA = fieldsA.substring(0,
			 * fieldsA.lastIndexOf(",")); valuesA = valuesA.substring(0,
			 * valuesA.lastIndexOf(","));
			 */

			try {
				if (!insertButton.getText().equals("Einf\u00FCgen")) {
					ArtefaktID = manager.getArtefaktId(recordID);
					if (ArtefaktID[0] != -1)
						manager.updateArtefaktmaske(fieldsA, ArtefaktID,
								valuesA);
					else if (fieldsA.size() > 0) {
						ArtefaktID = manager.insertIntoArtefaktmaske(fieldsA,
								valuesA);
					}
				} else if (fieldsA.size() > 0)
					ArtefaktID = manager.insertIntoArtefaktmaske(fieldsA,
							valuesA);
			} catch (StatementNotExecutedException s) {
			}
			// Falls es sich um eine \u00C4nderung handelt wird die ID
			// mitgegeben
			// String idvorspann = "";
			// String idvalue = "";
			if (this.insertButton.getText().equals("\u00E4ndern")) {
				try {
					phase1 = "'" + manager.getPhaseValue("phase1", recordID)
							+ "'";
					phase2 = "'" + manager.getPhaseValue("phase2", recordID)
							+ "'";
					phase3 = "'" + manager.getPhaseValue("phase3", recordID)
							+ "'";
					phase4 = "'" + manager.getPhaseValue("phase4", recordID)
							+ "'";
				} catch (StatementNotExecutedException s) {
				}
				// idvorspann = "ID";
				// idvalue = this.recordID+"";

			}

			// EINGABE DER HAUPTMASKE
			try {
				if (!insertButton.getText().equals("Einf\u00FCgen")) {
					manager.updateEingabeeinheit(recordID, projectKey, fields,
							ArtefaktID, massID, phase1, phase2, phase3, phase4,
							values);
				} else
					manager.insertIntoEingabeeinheit(projectKey, fields,
							ArtefaktID, massID, phase1, phase2, phase3, phase4,
							values);
			} catch (StatementNotExecutedException e) {
			}

			messageFrame.updateMessageFrame("");
			try {
				MainMenuBar mbar = this.mainframe.getHauptMenueBar();
				mbar.updateDurchsucheAlleFrame();
			} catch (Exception e) {
			}
			emptyMask();
			this.artefactID = new int[2];
			artefactID[0] = -1;
			artefactID[1] = -1;
			this.recordID = new int[2];
			recordID[0] = -1;
			recordID[1] = -1;
			this.massID = new int[2];
			massID[0] = -1;
			massID[1] = -1;
			if (this.insertButton.getText().equals("\u00E4ndern")) {
				this.knochenTemplate.loadActiveTemplate();
				this.artefaktTemplate.loadActiveTemplate();
			}

		} else {
			messageFrame.updateMessageFrame(" es fehlen noch: \n" + fehler);
		}
	}

	/**
	 * 
	 */
	private void updateKonkordanz1() {
		this.konkordanzpanel1.updateInfo();
	}

	private void updateKonkordanz2() {
		this.konkordanzpanel2.updateInfo();
	}

	private void updateKonkordanz3() {
		this.konkordanzpanel3.updateInfo();
	}

	private void updateKonkordanz4() {
		this.konkordanzpanel4.updateInfo();
	}

	/**
	 * @return
	 */
	private JPanel produziereFelderArtefaktmaske() {

		return new ArtefaktPanel(artefaktTemplate);
	}

	/**
	 * @return JPanel Main Mask for ProjectFrame. The Content of this Frame.
	 */
	private JPanel produziereFelderHauptmaske() {
		pcontent = new JPanel();
		pcontent.setLayout(new BorderLayout());
		p = new JPanel();
		p.setLayout(new GridLayout(2, 2));
		p1 = new JPanel();
		p1.setBorder(new TitledBorder("Sektion 1"));
		p2 = new JPanel();
		p2.setBorder(new TitledBorder("Sektion 2"));
		p3 = new JPanel();
		p3.setBorder(new TitledBorder("Sektion 3"));
		p4 = new JPanel();
		p4.setBorder(new TitledBorder("Sektion 4"));
		// Einf\u00FCgen der Felder auf die Panels nach Sektonsnummern sortiert
		BasisFeldJava2Db[] f = this.knochenTemplate.getFields();
		for (int i = 0; i < f.length; i++) {
			if (f[i].getCheckBoxSwitch().isSelected()) {
				String mem = f[i].getDbAequivalent();
				String s = f[i].getCheckBoxSwitch().getText();
				JComponent inputComponent = f[i].getEingabeKomponente();
				inputComponent.setToolTipText(mem);

				JPanel inputComponentUnit = new JPanel();

				inputComponentUnit.setLayout(new BoxLayout(inputComponentUnit,
						BoxLayout.X_AXIS));

				TitledBorder border = new TitledBorder(s);
				// NOTE Hier wird die Titelposition pro Feld ver\u00E4ndert!
				// //border.setTitlePosition(TitledBorder.LEFT);
				inputComponentUnit.setBorder(border);
				inputComponentUnit.add(inputComponent);
				JCheckBox b = new JCheckBox();// verantwortlich f\u00FCr das
				// Nichtl\u00F6schen der Werte
				b.setBorderPainted(false);
				b.setFocusable(false);
				inputComponentUnit.add(b);

				if (f[i].getSectionNr() == 1) {
					p1.add(inputComponentUnit);
				}
				if (f[i].getSectionNr() == 2) {
					p2.add(inputComponentUnit);
				}
				if (f[i].getSectionNr() == 3) {
					p3.add(inputComponentUnit);
				}
				if (f[i].getSectionNr() == 4) {
					p4.add(inputComponentUnit);
				}
			}
		}
		// Formatiere gem\u00E4ss Anzahl Felder
		int zeilen = 4;
		int count1 = p1.getComponentCount();
		p1.setLayout(new GridLayout(zeilen, (int) count1 / zeilen));
		int count2 = p2.getComponentCount();
		p2.setLayout(new GridLayout(zeilen, (int) count2 / zeilen));
		int count3 = p3.getComponentCount();
		p3.setLayout(new GridLayout(zeilen, (int) count3 / zeilen));
		int count4 = p4.getComponentCount();
		p4.setLayout(new GridLayout(zeilen, (int) count4 / zeilen));
		// Ende Formatieren
		p.add(p1);
		p.add(p2);
		p.add(p3);
		p.add(p4);
		pcontent.add(p, BorderLayout.CENTER);
		this.commandPanel = createCommandPanel();
		pcontent.add(this.commandPanel, BorderLayout.SOUTH);
		pcontent.setToolTipText("Eingabe");
		return pcontent;
	}

	/**
	 * @return
	 */
	private UebersichtsPanel produziereUeberblick() {
		return new UebersichtsPanel(manager, projectKey, desktop);
	}

	/**
	 * 
	 */
	public void searchValues(int[] recordKey) {
		emptyMask();
		this.knochenTemplate.loadMaxTemplate();
		this.artefaktTemplate.loadMaxTemplate();
		this.insertButton.setText("Einf\u00FCgen");
		this.recordID = recordKey;
		// Massbutton
		MassKnopf massbutton = null;

		try {
			// gew\u00FCnschtes Element Ermitteln
			// gew\u00FCnschtes Element in Maske einf\u00FCgen
			// Lese Informationen aus ROW
			String[][] rsEingabe = manager.getDataRecord(recordKey);
			this.artefactID = new int[2];
			artefactID[0] = -1;
			artefactID[1] = -1;
			this.massID = new int[2];
			massID[0] = -1;
			massID[1] = -1;

			for (int i = 0; i < rsEingabe.length; i++) {
				String cName = rsEingabe[i][0];
				String cWert = rsEingabe[i][1];

				// finde passendes Feld
				JComponent c = getFieldByDbAequivalent(cName);

				try {
					AbstraktesSelbstkorrekturTextFeld abs = (AbstraktesSelbstkorrekturTextFeld) c;
					abs.setText(cWert);
				} catch (Exception e) {
				}
				try {
					NotizButton nbt = (NotizButton) c;
					if (!cWert.equals("")) {
						nbt.setBackground(Color.RED);
					} else {
						nbt.setBackground(null);
					}
					nbt.setNotizFeldText(cWert);
				} catch (Exception e) {
				}
				try {
					JCheckBox chb = (JCheckBox) c;
					if (cWert.equals("JA")) {
						chb.setSelected(true);
					}
				} catch (Exception e) {
				}
				try {
					JComboBox cbx = (JComboBox) c;
					cbx.setSelectedItem(cWert);
				} catch (Exception e) {
				}
				try {
					if (c != null)
						massbutton = (MassKnopf) c;
				} catch (Exception e) {
				}
				// Test ob Artefakt vorhanden
				if (cName.equals("artefaktID")) {
					if (!cWert.equals("-1")) {
						this.artefactID[0] = Integer.parseInt(cWert);
					}
				}
				if (cName.equals("DBNummerArtefakt")) {
					if (!cWert.equals("-1")) {
						this.artefactID[1] = Integer.parseInt(cWert);
					}
				}
				// Test ob Masse vorhanden
				if (cName.equals("massID")) {
					if (!cWert.equals("-1")) {
						this.massID[0] = Integer.parseInt(cWert);
					}
				}
				if (cName.equals("DBNummerMasse")) {
					if (!cWert.equals("-1")) {
						this.massID[1] = Integer.parseInt(cWert);
					}
				}
			}
			// Mass Prozedur
			if (this.massID[0] >= 0) {
				massbutton.setBackground(Color.RED);
			} else {
				massbutton.setBackground(null);
			}
			// Artefakt Prozedur
			if (this.artefactID[0] >= 0) {

				// Lese Informationen aus ROW
				String result[][] = manager.getArtefaktmaske(artefactID);
				for (int i = 1; i <= result.length; i++) {
					String cName = result[i - 1][0];
					String cWert = result[i - 1][1];
					// finde passendes Feld
					JComponent c = getFieldByDbAequivalentArtefakt(cName);
					try {
						AbstraktesSelbstkorrekturTextFeld abs = (AbstraktesSelbstkorrekturTextFeld) c;
						abs.setText(cWert);
					} catch (Exception e) {
					}
					try {
						NotizButton nbt = (NotizButton) c;
						if (!cWert.equals("")) {
							nbt.setBackground(Color.RED);
						} else {
							nbt.setBackground(null);
						}
						nbt.setNotizFeldText(cWert);
					} catch (Exception e) {
					}
					try {
						JCheckBox chb = (JCheckBox) c;
						if (cWert.equals("JA")) {
							chb.setSelected(true);
						}
					} catch (Exception e) {
					}
					try {
						JComboBox cbx = (JComboBox) c;
						cbx.setSelectedItem(cWert);
					} catch (Exception e) {
					}
				}
			}
			this.insertButton.setText("\u00E4ndern");
			// Focus auf erstes TAB
			this.maintab.setSelectedIndex(0);
		} catch (StatementNotExecutedException e) {
			/*
			 * JOptionPane.showMessageDialog(this, "Konnte Aktion nicht
			 * ausf\u00FChren!"); e.printStackTrace();
			 */
		}
	}

	/**
	 * @return display the ArtefactTemplate for this Project
	 */
	public ArtefaktSchablone showArtefaktTemplate() {
		this.artefaktTemplate.setVisible(true);
		try {
			this.artefaktTemplate.setSelected(true);
		} catch (PropertyVetoException e) {

		}
		return this.artefaktTemplate;
	}

	/**
	 * @return display the indetTemplate for this Project
	 */
	public UnbestimmtSchablone showIndetTemplate() {
		this.indetTemplate.setVisible(true);
		return this.indetTemplate;
	}

	/**
	 * @return display the BoneTemplate for this Project
	 */
	public KnochenSchablone showKnochenTemplate() {
		this.knochenTemplate.setVisible(true);
		try {
			this.knochenTemplate.setSelected(true);
		} catch (PropertyVetoException e) {

		}
		return this.knochenTemplate;
	}

	public String toString() {
		return "mein frame";
	}

	/**
	 * apply a Template
	 */
	public void updateInputScreen() {
		maintab.setComponentAt(0, produziereFelderHauptmaske());
		maintab.setComponentAt(1, produziereFelderArtefaktmaske());
	}

	/**
	 * 
	 */
	private void updateUeberblick() {
		double time = System.currentTimeMillis();
		this.ueberblickpanel.updateInfo();
		time = System.currentTimeMillis() - time;
		if (_log.isTraceEnabled()) {
			_log.trace("Time used for updating update-tab: " + time);
		}
	}

	/**
	 * @param i
	 */
	// NOTE so werden methoden f\u00FCr nachschlagefelder eingebaut
	public void setTierArtField(int i) {
		TierartKodeFeld tf = (TierartKodeFeld) getFieldByDbAequivalent("tierart");
		tf.setText("" + i);
	}

	public void setSkelCodeField(int i) {
		SkelKodeFeld sf = (SkelKodeFeld) getFieldByDbAequivalent("skelettteil");
		sf.setText("" + i);
	}

	public void setBruchkanteCodeField(int i) {
		BruchkantenFeld bf = (BruchkantenFeld) getFieldByDbAequivalent("bruchkante");
		bf.setText("" + i);
	}

	public void setErhaltungCodeField(int i) {
		ErhaltungFeld ef = (ErhaltungFeld) getFieldByDbAequivalent("erhaltung");
		ef.setText("" + i);
	}

	/**
	 * @param i
	 */
	public void setAlter1CodeField(int i) {
		Alter1KodeFeld a1cf = (Alter1KodeFeld) getFieldByDbAequivalent("alter1");
		a1cf.setText("" + i);
	}

	/**
	 * @param i
	 */
	public void setAlter2CodeField(int i) {
		Alter2KodeFeld a2cf = (Alter2KodeFeld) getFieldByDbAequivalent("alter2");
		a2cf.setText("" + i);
	}

	/**
	 * @param i
	 */
	public void setAlter3CodeField(int i) {
		Alter3KodeFeld a3cf = (Alter3KodeFeld) getFieldByDbAequivalent("alter3");
		a3cf.setText("" + i);
	}

	/**
	 * @param i
	 */
	public void setAlter4CodeField(int i) {
		Alter4KodeFeld a4cf = (Alter4KodeFeld) getFieldByDbAequivalent("alter4");
		a4cf.setText("" + i);
	}

	/**
	 * @param i
	 */
	public void setAlter5CodeField(int i) {
		Alter5KodeFeld a5cf = (Alter5KodeFeld) getFieldByDbAequivalent("alter5");
		a5cf.setText("" + i);
	}

	public void setWurzelfrassField(int i) {
		WurzelfrassFeld wf = (WurzelfrassFeld) getFieldByDbAequivalent("wurzelfrass");
		wf.setText("" + i);
	}

	public void setVersinterungField(int i) {
		VersinterungFeld vf = (VersinterungFeld) getFieldByDbAequivalent("kruste");
		vf.setText("" + i);
	}

	public void setFettField(int i) {
		FettFeld ff = (FettFeld) getFieldByDbAequivalent("fettig");
		ff.setText("" + i);
	}

	public void setPatinaField(int i) {
		PatinaFeld pf = (PatinaFeld) getFieldByDbAequivalent("patina");
		pf.setText("" + i);
	}

	public void setBrandspurField(int i) {
		BrandspurFeld bf = (BrandspurFeld) getFieldByDbAequivalent("brandspur");
		bf.setText("" + i);
	}

	public void setVerbissField(int i) {
		VerbissFeld vf = (VerbissFeld) getFieldByDbAequivalent("verbiss");
		vf.setText("" + i);
	}

	public void setSchlachtS1Field(int i) {
		Schlachtspur1Feld s1f = (Schlachtspur1Feld) getFieldByDbAequivalent("schlachtS1");
		s1f.setText("" + i);
	}

	public void setSchlachtS2Field(int i) {
		Schlachtspur2Feld s2f = (Schlachtspur2Feld) getFieldByDbAequivalent("schlachtS2");
		s2f.setText("" + i);
	}

	public void setGeschlechtsField(int i) {
		GeschlechtsFeld gf = (GeschlechtsFeld) getFieldByDbAequivalent("geschlecht");
		gf.setText("" + i);
	}

	public void setBruchkanten2Field(int i) {
		Bruchkanten2Feld bf = (Bruchkanten2Feld) getFieldByDbAequivalent("bruchkante2");
		bf.setText("" + i);
	}

	public JTabbedPane getUpdateWindow() {
		return maintab;
	}

}