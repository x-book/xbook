/*
 * WeightField.java
 *
 * Created on 25. M\u00E4rz 2003, 16:02
 */
package ossobook.client.gui.update.elements.eingabefelder;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturTextFeld;


/**
 *
 * @author  ali
 */
public class GewichtFeld extends AbstraktesSelbstkorrekturTextFeld{
    
    /** Creates a new instance of WeightField */
    public GewichtFeld() {
        super();
    }
    
    public boolean condition() {
        try{
            Float.parseFloat(getText());
            return true;
        }catch(NumberFormatException e){
            return false;
        }
    }

    /* (non-Javadoc)
     * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
     */
    public void useDefaultValue() {
    }
    
}
