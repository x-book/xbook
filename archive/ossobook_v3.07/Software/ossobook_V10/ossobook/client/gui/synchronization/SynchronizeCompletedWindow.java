package ossobook.client.gui.synchronization;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import java.awt.event.*;

import ossobook.client.gui.common.*;

/**
 * informs user about completion of synchronization and occured conflicts
 * @author j.lamprecht
 *
 */
public class SynchronizeCompletedWindow extends Observable implements ActionListener{

	private JInternalFrame frame;
	public static int SYNCHRONIZE = 1;
	public static int INITIALIZE =2;
	int instance;
	
	
	/**
	 * builds up the SynchronizeCompletedWindow for Synchronization OR Initialization
	 * @param window
	 * @param path: path to the conflict files of the synchronized projects
	 */
	public SynchronizeCompletedWindow(OssobookHauptfenster parent, int x, int y, 
		Vector<String> databaseSchemePath, Vector<String> path, int endOf){
				
		this.instance = endOf;
		String conflictMessage="";
		String end="";
		String schemeMessage="";
		
		if(endOf==SYNCHRONIZE) {
			end="Synchronisation";
			if(path.size()==0){
				 conflictMessage = "Die " + end + " wurde beendet. " +
						"Es traten keine Konflikte auf.\n\n";
				 path=null;
			}
			else
				conflictMessage = "Die "+end+" wurde beendet. Es kam jedoch zu Konflikten. " +
						" Bitte \u00FCberpr\u00FCfen Sie die deswegen Datenbank auf Richtigkeit." +
						" Die Konfliktdatei(en) finden Sie unter: ";
		}
		else if(endOf==INITIALIZE){
			end ="Initialisierung";
			conflictMessage = "Die " + end + " wurde beendet.\n\n";
		}
		
		if(databaseSchemePath==null) 
			schemeMessage="Das Datenbankschema hat sich nicht ge\u00E4ndert.";
		else 
			schemeMessage="Das Datenbankschema hat sich ge\u00E4ndert " +
				"oder in Ihrer lokalen Datenbank befinden sich Tabellen bzw. Spalten, die es in " +
				"der globalen Datenbank nicht gibt. \u00C4nderungen hierin werden NICHT an die globale Datenbank " +
				"weitergegeben.\n" +
				"Informationen zu den \u00C4nderungen finden Sie unter: ";
			
		init(parent, x,y, conflictMessage, schemeMessage,path, databaseSchemePath, end);	
	}
	
	/**
	 * show information about success of synchronization and possible conflicts
	 * @param message
	 */
	private void init(OssobookHauptfenster parent, int x, int y, String conflictMessage,
			String schemeMessage, Vector<String> conflictPath, Vector<String> databasePath, String end){
		frame = new JInternalFrame();
		frame.setTitle(end + " beendet");
			  
		frame.setTitle("Synchronisation");
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		frame.setLayout(gbl);		  

		JPanel scheme = createPanel("Datenbankschema", schemeMessage, databasePath);
		gbc = makegbc(0, 0, 3, 1);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.WEST;	
		gbc.weightx=1;
	    gbc.weighty=1;
		gbl.setConstraints(scheme, gbc);
		frame.getContentPane().add(scheme);
		  
		JPanel conflict=createPanel("Konflikte", conflictMessage, conflictPath);
		gbc = makegbc(0, 1, 3, 1);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.WEST;	  
		gbc.weightx=1;
	    gbc.weighty=1;
		gbl.setConstraints(conflict, gbc);
		frame.getContentPane().add(conflict);
		 
		JButton ok = new JButton("ok");
		ok.addActionListener(this);
		gbc = makegbc(0, 2, 1, 1);
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.CENTER;	  
		gbc.weightx=1;
	    gbc.weighty=1;
		gbl.setConstraints(ok, gbc);
		frame.getContentPane().add(ok);
		
		frame.setPreferredSize(new Dimension(800, 500));
		frame.pack();
		frame.setVisible(true);
		parent.getContentPane().add(frame);       
        frame.toFront();
	}
	
	/**
	 * single parts of SynchronizeCompletedWindows - database scheme and conflicts
	 * @param caption
	 * @param message
	 * @param path
	 * @return
	 */
	private JPanel createPanel(String caption,String message, Vector<String> path){
		JPanel panel = new JPanel();   
	   	   
	    GridBagLayout gbl = new GridBagLayout();
	    GridBagConstraints gbc;
	    panel.setLayout(gbl);
	    	    
	    gbc = makegbc(0, 0, 1, 1);
	    gbc.fill = GridBagConstraints.NONE;
	    gbc.anchor = GridBagConstraints.WEST;
	    gbc.weightx=1;
	    gbc.weighty=1;
	    Label label = new Label(caption);
	    label.setFont(new Font("Tahoma", 1, 14));
	    gbl.setConstraints(label, gbc);
	    panel.add(label);
	    
	    gbc = makegbc(0, 1, 2, 1);
	    gbc.weightx=1;
	    gbc.weighty=1;
	    gbc.fill = GridBagConstraints.HORIZONTAL;
	    gbc.anchor = GridBagConstraints.WEST;
	    JTextArea information = new JTextArea(message);
	    information.setColumns(80);
	    information.setEditable(false);
	    information.setLineWrap(true);
	    information.setRows(3);
	    information.setWrapStyleWord(true);
        information.setBackground(new Color(236, 233, 216));
	    gbl.setConstraints(information, gbc);
	    panel.add(information);
	    
	    if(path!=null){
		    java.awt.List list = new java.awt.List(7);
		    for (int i = 0; i < path.size(); ++i) {
		      list.add(path.elementAt(i));
		    }
		    list.setBackground(new Color(236, 233, 216));
		    gbc = makegbc(0, 2, 2, 1);
		    gbc.weightx=1;
		    gbc.weighty=1;
		    gbc.fill = GridBagConstraints.HORIZONTAL;
		    gbc.anchor = GridBagConstraints.WEST;
		    gbl.setConstraints(list, gbc);
		    panel.add(list);
	    }
	    
	    panel.setPreferredSize(new Dimension(350, 300));
	    
	    return panel;
	}
	
	private GridBagConstraints makegbc(int x, int y, int width, int height){
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = width;
		gbc.gridheight = height;
		gbc.insets = new Insets(1, 15, 1, 15);
		return gbc;
	}
	
	
	/**
	 * user has noticed information
	 */
	public void actionPerformed(ActionEvent e){
		frame.setVisible(false);
		setChanged();
		notifyObservers();
	}
	
	public int getInstance(){
		return instance;
	}	
}
