package ossobook.client.gui.synchronization;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.*;
import javax.swing.text.*;

/**
 * informs user about progress of initialization or synchronization process
 * @author j.lamprecht
 *
 */
public class ProgressWindow{

	private JPanel panel;
	private JProgressBar progress;
	private JTextPane taskOutput;
	private JScrollPane scroll;
	public static SimpleAttributeSet WARNING = StyleConstant.RED;
	public static SimpleAttributeSet NORMAL = StyleConstant.BLACK;
	
	/**
	 * build up ProgressWindow
	 */
	public ProgressWindow() {
		
		panel = new JPanel();

		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();

		panel.setLayout(layout);
		
		progress = new JProgressBar(0, 100);
		
		scroll = new JScrollPane();		
		taskOutput = new JTextPane();
		taskOutput.setMargin(new Insets(5,5,5,5));
        taskOutput.setEditable(false);
        scroll.setViewportView(taskOutput);
		
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.gridheight = GridBagConstraints.RELATIVE;
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.insets = new Insets(5, 0, 0, 0);
        
        panel.add(progress, constraints);
        panel.add(scroll, constraints);
        
        scroll.setPreferredSize(new Dimension(500, 100));
	}

	/**
	 * 
	 * @param action - initialize or synchronize
	 */
	public void initializeTaskWindow(String action){
		taskOutput.setText("");
		appendText(action + "sfortschritt:\n\n", NORMAL);
		progress.setIndeterminate(false);
		progress.setValue(0);
	}
	
	/**
	 * display new progress
	 * @param text
	 * @param style
	 */
	public void appendText(String text, SimpleAttributeSet style){
		try{
			taskOutput.getDocument().insertString(taskOutput.getDocument().getLength(), text, style);
			scroll.getVerticalScrollBar().setValue((int) scroll.getMaximumSize().getHeight());
			scroll.repaint();
		}
		catch(BadLocationException e){
			e.printStackTrace();
		}
	}
	
	public JPanel getWindow(){
		return panel;
	}
		
	/**
	 * synchronization / initialization finished
	 */
	public void finished(){
		progress.setIndeterminate(false);
		progress.setValue(100);
	}
	
	/**
	 * synchronization / initialization started
	 */
	public void start(){
		progress.setIndeterminate(true);
	}
	
	/**
	 * different styles to display messages - "normal" actions and occurance of failures
	 * @author j.lamprecht
	 *
	 */
	private static class StyleConstant{
		public static SimpleAttributeSet RED = new SimpleAttributeSet();
		public static SimpleAttributeSet BLACK = new SimpleAttributeSet();
		static {
			// highlight failures
			StyleConstants.setForeground(RED, Color.red);
			StyleConstants.setForeground(BLACK, Color.black);
		}
	}
}
