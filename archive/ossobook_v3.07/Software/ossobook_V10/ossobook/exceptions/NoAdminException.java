package ossobook.exceptions;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * exception for the case that a user is no admin 
 * but wants to execute a statement for which you need admin rights
 * @author j.lamprecht
 *
 */
public class NoAdminException extends Exception{

	//not used yet
	/**
	 * inform user about missing admin right
	 */
	public NoAdminException(){
		JFrame frame = new JFrame();
		JOptionPane.showMessageDialog(frame,
			    "Sie haben keine Administratorrechte und d\u00FCrfen deswegen" +
			    " weder neue Codes anlegen, noch vorhandene Codes \u00E4ndern " +
			    " oder l\u00F6schen.",
			    "Keine Administratorrechte", 
			    JOptionPane.WARNING_MESSAGE);
	}
}
