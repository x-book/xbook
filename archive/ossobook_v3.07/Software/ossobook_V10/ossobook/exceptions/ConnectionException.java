package ossobook.exceptions;

import java.net.*;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * defines the exception for the case that the connection to the database can't be established
 * @author j.lamprecht
 *
 */
public class ConnectionException extends ConnectException{

	/**
	 * show information message
	 */
	public ConnectionException(boolean showInformation){
		if(showInformation){
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog(frame,
				    "Die Verbindung konnte nicht hergestellt werden. " 
					+ "\nBitte \u00FCberpr\u00FCfen Sie Ihre Internetverbindung und die ossobookDBConfig.txt bzw. ossobookLocalDB.txt bzw. ServerConfig.txt-Datei."
				    + "\nWenn Sie Hilfe ben\u00F6tigen, wenden Sie sich bitte an Ihren Administrator.",
				    "Verbindung fehlgeschlagen", 
				    JOptionPane.WARNING_MESSAGE);
		}
	}
	
}
