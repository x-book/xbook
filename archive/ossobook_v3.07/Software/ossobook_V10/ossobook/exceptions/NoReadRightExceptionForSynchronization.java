package ossobook.exceptions;

import java.awt.*;
import java.util.Vector;


/**
 * exception in case user has no right to read global project
 * used during initialization and synchronization
 * @author j.lamprecht
 *
 */
public class NoReadRightExceptionForSynchronization extends NoRightExceptionForSynchronization{
	
	public static Vector<String> projectNames = new Vector<String>();
	
	/**
	 * remember projects with missing read right
	 * @param projectName
	 */
	public NoReadRightExceptionForSynchronization(String projectName){
		projectNames.add(projectName);
	}
	
	/**
	 * show information about projects with missing read right
	 * @param point
	 * @param event - SynchronizeCompletedWindow.SYNCHRONIZE, SynchronizeCompletedWindow.INITIALIZE
	 */
	public static void showInformation(Point point, String event){
		showInformation(point, NoRightExceptionForSynchronization.read, event, projectNames);
		projectNames = new Vector<String>();
	}
	
	public static boolean noRights(){
		return noRights(projectNames);
	}
}

