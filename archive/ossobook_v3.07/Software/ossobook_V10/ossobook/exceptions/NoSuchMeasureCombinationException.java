package ossobook.exceptions;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class NoSuchMeasureCombinationException extends Exception {
	public NoSuchMeasureCombinationException(String info){
		JFrame frame = new JFrame();
		JOptionPane.showMessageDialog(frame, info,
			    "Statement fehlgeschlagen",
			    JOptionPane.WARNING_MESSAGE);
		this.printStackTrace();
	}
}
