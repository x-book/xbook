package ossobook.exceptions;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.sql.*;

/**
 * exception for the case that a DDL statement could not be executed
 * @author j.lamprecht
 *
 */
public class MetaStatementNotExecutedException extends SQLException{

	/**
	 * inform user about DDL-fault
	 * @param function - statement that could not be executed
	 */
	public MetaStatementNotExecutedException(String function){
		JFrame frame = new JFrame();
		JOptionPane.showMessageDialog(frame, 
			    "Das Datenbankschema konnte nicht von der Datenbank geholt werden.\n" +
			    "Fehler in Funktion " + function +".",
			    "Statement fehlgeschlagen",
			    JOptionPane.WARNING_MESSAGE);
		this.printStackTrace();
	}
}
