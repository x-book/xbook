package ossobook.communication.file;
import java.io.BufferedReader;
import java.io.File;
import java.util.*;
import java.io.FileReader;

/**
 * dbConfig reads lines from config files that are located in the Programms Root directory
 * 
 * 'ossobookDBConfig.txt' configuration file:
 * Serveraddress, DB-Name separated by newlines. 
 * @see ossobook.io.database.communication.DbVerbindung
 * @see ossobook.server.synchronizationSteps.Connection
 * 
 * 'ossobookLocalDB.txt' configuration file:
 * Server address, database name, user, password (should not be password used for global database)
 * @see ossobook.io.database.communication.DbVerbindung
 *
 * 'privateKey.txt' file:
 * used ONLY on Server - do not copy on client!
 * private exponent, modulus
 * @see ossobook.server.encryption.KeyGeneration
 *  
 * 'publicKey.txt' file:
 * used by clients, generated on server
 * public exponent, modulus
 * @see ossobook.server.encryption.KeyGeneration
 * @see ossobook.client.LoginData
 * 
 * 'ServerConfig.txt':
 * synchronization server config file
 * client access to server: server address, port
 * 
 * 'MailConfig.txt':
 * used during synchronization by server if conflicts arise
 * for conflict mails: smtp server, username and password for relay, sender
 * 
 * @author ali
 */
public class DbKonfiguration {

	/** Creates a new instance of dbConfig */
	public DbKonfiguration() {
	}
	
	/**getting the information from specified File
	 * 
	 * @param infofile 
	 * @return String[n] with the values described above. 
	 */
	public String[] getInfoFromFile(File infofile) {
		Vector<String> informations = new Vector<String>();
		try {
			File f = infofile;
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);			
			while (br.ready()) 
				informations.add(br.readLine());
		} catch (Exception e) {
			e.printStackTrace();
		}
		String[] info = new String[informations.size()];
		informations.toArray(info);
		return info;
	}
}