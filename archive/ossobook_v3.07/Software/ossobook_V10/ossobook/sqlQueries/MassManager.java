package ossobook.sqlQueries;

import java.sql.*;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.exceptions.NoSuchMeasureCombinationException;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages masse and massabhaengigkeit table 
 * (makes inserts, deletes, updates and select statements on these tables)
 * @author j.lamprecht
 *
 */
public class MassManager extends TableManager {
	private static Log _log = LogFactory.getLog(MassManager.class);
	int versionDatabaseNumber;
	
	public MassManager(Connection con, int databaseNumber, String databaseName){
		super(con, databaseName);
		this.versionDatabaseNumber=databaseNumber;
	}
	
	/**
	 * @precondition user has the right to change project (via record in eingabeeinheit)
	 * @param projnr
	 * @throws StatementNotExecutedException
	 */
	public void deleteMasse(int[] projectKey) throws StatementNotExecutedException{
		String query = "UPDATE " + databaseName+".masse, " + databaseName+".eingabeeinheit "
			+ "SET masse.geloescht='Y', masse.Zustand='ge\u00E4ndert' "
			+ "WHERE eingabeeinheit.massID=masse.MassID AND eingabeeinheit.DBNummerMasse=masse.Datenbanknummer" 
			+ " AND eingabeeinheit.projNr='" + projectKey[0] 
			+ "' AND eingabeeinheit.DBNummerProjekt="+projectKey[1]+";";
		executeStatement(query);
	}
	
	/**
	 * masse records definitely deleted from database for given project
	 * @precondition method is executed on local database during synchronization
	 * @param projectKey
	 * @param event - QueryManager.DELETE_DELETED, QueryManager.DELETE_ALL
	 * @throws StatementNotExecutedException
	 */
	public void deleteMassePermanent(int[] projectKey, int event)throws StatementNotExecutedException{
		String condition="";
		if(event==QueryManager.DELETE_DELETED) condition=" AND masse.geloescht='Y' AND masse.Zustand='synchronisiert'";
		String query = "DELETE masse FROM " + databaseName+".masse, " + databaseName+".eingabeeinheit "
			+ "WHERE eingabeeinheit.massID=masse.MassID " 
			+ "AND eingabeeinheit.DBNummerMasse=masse.Datenbanknummer "
			+ "AND eingabeeinheit.projNr=" + projectKey[0] 
			+ " AND eingabeeinheit.DBNummerProjekt="+projectKey[1]
			+" " +condition+";";
		executeStatement(query);
	}

	/**
	 * @precondition user has the right to change project (via record in eingabeeinheit)
	 * @param idvorspannMasse
	 * @param fieldsMasse
	 * @param idvalueMasse
	 * @param valuesMasse
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int[] insertIntoMasse(Vector<String> fieldsMasse,
			Vector<String> valuesMasse) throws StatementNotExecutedException{
		int[] massID=new int[2];
		
		String table = databaseName+".masse";
		String auto = "MassID";
		
		String query = "INSERT into " + table + " (" ;
		for(int i=0; i<fieldsMasse.size(); i++){
			query = query + fieldsMasse.elementAt(i) + ", ";
		}
		query = query + "Datenbanknummer, Zustand) VALUES (";
		for(int i=0; i<valuesMasse.size(); i++){
			query = query + valuesMasse.elementAt(i)+", ";
		}
		query = query + versionDatabaseNumber + ", 'ge\u00E4ndert');";
		try {
			massID[0]=executeInsert(query, auto, table);
			massID[1]=versionDatabaseNumber;
			/*query = "SELECT MAX(MassID) as mid FROM masse;";
			ResultSet rs = createStatement(query);
			rs.next();
			massID = rs.getInt("mid");*/
		} catch (SQLException e) {
			//printErrorMessage(e, query);
			e.printStackTrace();
		}
		return massID;
	}
	
	/**
	 * get names of masses from table massabhaengigkeit for given labels
	 * @param tierlabel
	 * @param skellabel
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getMasse(int tierlabel, int skellabel) throws StatementNotExecutedException, NoSuchMeasureCombinationException{
		String query = "SELECT Masse FROM " + databaseName+".massabhaengigkeit WHERE TierLB="
			+ tierlabel + " AND SkelLB=" + skellabel +";";
		String result = "";
		try{
			ResultSet rs = createStatement(query);
			rs.last();
			int count=rs.getRow();
			if(count==0){
				throw new NoSuchMeasureCombinationException("Fuer die eingegebene Kombination sind keine Masse vorhanden.");
			}
			rs.beforeFirst();
			rs.next();
			result = rs.getString("Masse");
		}
		catch(SQLException s){
			printErrorMessage(s, query);
		}
		return result;
	}
	
	/**
	 * get description for given name from massdefinition
	 * @param s
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String holeMassName(String s) throws StatementNotExecutedException{
		String result = "";
		String query = "SELECT massBeschreibung FROM " + databaseName+".massdefinition WHERE massName LIKE('"
				+ s + "');";
		try {
			ResultSet rs = createStatement(query);
			rs.next();
			result = rs.getString("massBeschreibung");

		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return result;
	}
	
	/**
	 * @precondition user has at least the right to read project (via record in eingabeeinheit)
	 * @param mass
	 * @param massID
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String holeMassWert(String mass, int[] massKey) throws StatementNotExecutedException{
		String result = "";
		String query = "SELECT " + mass + " FROM " + databaseName+".masse WHERE massID =" + massKey[0]
				+ " AND Datenbanknummer="+massKey[1]+" AND geloescht='N';";
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query);
			}
		try {
			ResultSet rs = createStatement(query);
			if(rs.next()) result = rs.getString(mass);

		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return result;
	}
	
	/**
	 * @precondition user has the right to change project (via record in eingabeeinheit)
	 * @param aktuelleMassDatensatzID
	 * @throws StatementNotExecutedException
	 */
	public void deleteFromMasse(int[] massID)throws StatementNotExecutedException{
		String query="UPDATE " + databaseName+".masse SET geloescht='Y', Zustand='ge\u00E4ndert' WHERE MassID='"
			+ massID[0] + "' AND Datenbanknummer="+massID[1]+";";
		try{
			executeStatement(query);
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
	/**
	 * updates a given data record in the table Masse
	 * @precondition user has the right to change project (via record in eingabeeinheit)
	 * @param idvorspannArte: identifier name for the Masse record
	 * @param fieldsA: fields in Masse that shall be updated
	 * @param idvalueArte: identifier for the Masse record
	 * @param valuesA: values to which the fields shall be changed
	 * @throws StatementNotExecutedException
	 */
	public void updateMasse(Vector<String> fieldsMasse,int[] massKey, Vector<String> valuesMasse)	throws StatementNotExecutedException{
		String query = "UPDATE " + databaseName+".Masse SET ";
		for(int i=0; i<fieldsMasse.size(); i++){
			query=query+fieldsMasse.elementAt(i) + "='"+valuesMasse.elementAt(i)+"', ";
		}
		query=query + "Zustand='ge\u00E4ndert'";
		query=query+ " WHERE massID="+massKey[0] +" AND masse.Datenbanknummer="+massKey[1]+";";
		try{
			executeUpdate(query);
		}
		catch(SQLException e){
			printErrorMessage(e, query);
		}	
	}

	/**
	 * get all masse entries which have been changed
	 * @param projectKey 
	 * @param columntypes columns of global database 
	 * -> needed to be independet from order and global database scheme changes
	 * @param event - QueryManager.CHANGES_LOCAL, QueryManager.CHANGES_GLOBAL, QueryManager.ALL_GLOBAL
	 * @param lastSynchronisation
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getEntries(int[] projectKey, String[][] columntypes, int event, String lastSynchronisation) throws StatementNotExecutedException{
		Vector<String[]> result;
		String condition =" WHERE DBNummerProjekt=" + projectKey[1] + " AND ProjNr=" + projectKey[0] 
		    +" AND eingabeeinheit.massID=masse.massID AND eingabeeinheit.DBNummerMasse=masse.Datenbanknummer";
		if(event==QueryManager.CHANGES_LOCAL) condition = condition + " AND masse.Zustand='ge\u00E4ndert' AND masse.Nachrichtennummer != -1";
		else if(event==QueryManager.CHANGES_GLOBAL)
			condition = condition + " AND masse.Zustand>'" + lastSynchronisation + "'";
		String[] tables = {databaseName+".masse", databaseName+".eingabeeinheit"};
		result = getEntries(columntypes, tables , condition);
		return result;
	}
	
	/**
	 * 
	 * @param projectKeyValues of global database
	 * -> needed to be independet from global database scheme changes
	 * @param columns of global database
	 * -> needed to be independet from order and global database scheme changes
	 * @param key
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getMassEntry(String[][] columns, int[] projectKeyValues, Vector<String> key) throws StatementNotExecutedException{
		String condition =" WHERE ";
		for(int i=0; i<key.size(); i++){
			condition = condition + key.elementAt(i) + "=" + projectKeyValues[i];
			if(i<key.size()-1) condition = condition + " AND ";
		}
		String[] tableNames = {databaseName+".masse"};
		return getEntries(columns, tableNames, condition);
	}
	
	public void insertData(String[] scheme, String[] data) throws StatementNotExecutedException{
		insertData(scheme, data, databaseName+".masse");
	}
	
	public int updateData(String[] scheme, String[] data, Vector<String> key) throws StatementNotExecutedException{
		return updateData(scheme, data, databaseName+".masse", key);
	}
	
	/**
	 * set Zustand to 'synchronisiert' after server affirmed changing global database
	 * @precondition executed on local database during synchronization
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 */
	public void setSynchronized(int[] projectKey) throws StatementNotExecutedException{
		String query = "UPDATE " + databaseName+".masse, " + databaseName+".eingabeeinheit SET masse.Zustand='synchronisiert', masse.Nachrichtennummer=-1 WHERE " +
				"ProjNr = '" + projectKey[0] + "' AND eingabeeinheit.DBNummerProjekt=" + projectKey[1] + " AND "+
				" eingabeeinheit.massID=masse.MassID AND eingabeeinheit.DBNummerMasse=masse.Datenbanknummer AND masse.Nachrichtennummer!=-1;";
		try{
			executeUpdate(query);
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
	public String getLastChange(Vector<String> key, int[] keyValues) throws StatementNotExecutedException{
		return getStatus(key, keyValues, databaseName+".masse");
	}
	
	/**
	 * assign message number to changed entries in masse of given project
	 * @param message
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 */
	public void setMessageNumber(int message, int[] projectKey) throws StatementNotExecutedException{
		String query = "UPDATE " + databaseName + ".masse, " + databaseName+".eingabeeinheit SET masse.Nachrichtennummer=" + message 
			+ " WHERE ProjNr="+projectKey[0]+" AND eingabeeinheit.DBNummerProjekt=" + projectKey[1] 
			+ " AND eingabeeinheit.massID=masse.MassID AND eingabeeinheit.DBNummerMasse=masse.Datenbanknummer AND masse.Zustand='ge\u00E4ndert';";
		try{
			executeStatement(query);
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
	
}
