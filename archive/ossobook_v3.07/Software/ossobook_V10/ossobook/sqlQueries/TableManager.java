package ossobook.sqlQueries;

import java.sql.*;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages database queries
 * 
 * @author j.lamprecht
 * 
 */
public abstract class TableManager {
	private static Log _log = LogFactory.getLog(TableManager.class);
	protected Connection connection;
	protected String databaseName;

	/**
	 * 
	 * @param connection
	 *            that has been established
	 */
	public TableManager(Connection connection, String databaseName) {
		this.connection = connection;
		this.databaseName = databaseName;
	}

	/**
	 * select, insert, update or delete statement on database shall be done
	 * 
	 * @param query:
	 *            select, insert, update or delete statement
	 * @throws StatementNotExecutedException
	 */
	public void executeStatement(String query)
			throws StatementNotExecutedException {
		try {
			if (_log.isDebugEnabled()) {
				_log.debug("Query: " + query);
			}
			Statement s = connection.createStatement();
			s.execute(query);
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}

	/**
	 * insert, update or delete statement on database shall be done
	 * 
	 * @param query:
	 *            insert, update or delete statement
	 * @throws SQLException
	 */
	public int executeUpdate(String query) throws SQLException {
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query);
		}
		Statement s = connection.createStatement();
		return s.executeUpdate(query);
	}

	/**
	 * select statement on database shall be done
	 * 
	 * @param query:
	 *            select Statement
	 * @return
	 * @throws SQLException
	 */
	public ResultSet createStatement(String query) throws SQLException {
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query);
		}
		Statement s = connection.createStatement();
		ResultSet rs = s.executeQuery(query);
		return rs;
	}

	/**
	 * executes an insert for the given table
	 * 
	 * @param query
	 * @param auto
	 * @param table
	 * @return primary key values of the inserted record
	 * @throws SQLException
	 */
	public int executeInsert(String query, String auto, String table)
			throws SQLException {
		int id = -1;
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query);
		}
		Statement s = connection.createStatement();
		s.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
		ResultSet rs = s.getGeneratedKeys();
		if (rs.next()) {
			id = rs.getInt(1);
		}
		return id;
	}

	/**
	 * updates the record for the given table
	 * 
	 * @param scheme -
	 *            scheme of the table
	 * @param data -
	 *            values of the record
	 * @param tableName
	 * @param key -
	 *            primary key of table
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int updateData(String[] scheme, String[] data, String tableName,
			Vector<String> key) throws StatementNotExecutedException {
		String query = "UPDATE " + tableName + " SET ";
		String[] keyValues = new String[key.size()];
		int result = -1;
		for (int i = 0; i < scheme.length; i++) {
			for (int j = 0; j < key.size(); j++) {
				if ((databaseName + "." + scheme[i]).equals(tableName + "."
						+ key.elementAt(j))) {
					keyValues[j] = data[i];
					break;
				}
			}
			if ((databaseName + "." + scheme[i]).equals(tableName + ".Zustand"))
				query = query + "Zustand='synchronisiert'";
			else
				query = query + scheme[i] + "='" + data[i] + "'";
			if (i < scheme.length - 1)
				query = query + ", ";
			else
				query = query + " WHERE ";
		}
		for (int i = 0; i < key.size(); i++) {
			query = query + key.elementAt(i) + "='" + keyValues[i] + "'";
			if (i < key.size() - 1)
				query = query + " AND ";
			else
				query = query + ";";
		}
		try {
			result = executeUpdate(query);
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return result;
	}

	/**
	 * inserts a new record into the given table
	 * 
	 * @param scheme -
	 *            scheme of the table
	 * @param data -
	 *            values of the record
	 * @param tableName
	 * @throws StatementNotExecutedException
	 */
	public void insertData(String[] scheme, String[] data, String tableName)
			throws StatementNotExecutedException {
		String query = "INSERT INTO " + tableName + " (";
		int statusPlace = -1;
		for (int i = 0; i < scheme.length; i++) {
			if (scheme[i].equals(tableName + ".Zustand"))
				statusPlace = i;
			query = query + scheme[i];
			if (i == scheme.length - 1)
				query = query + ") VALUES (";
			else
				query = query + ", ";
		}
		for (int i = 0; i < data.length; i++) {
			if (!tableName.equals(databaseName + ".projektrechte")
					&& i == statusPlace)
				query = query + "'synchronisiert'";
			else
				query = query + "'" + data[i] + "'";
			if (i == scheme.length - 1)
				query = query + ");";
			else
				query = query + ", ";
		}
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}

	/**
	 * gets changed records of given table
	 * 
	 * @param columns -
	 *            scheme of the table
	 * @param tableNames
	 * @param condition -
	 *            where condition
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getEntries(String[][] columns, String[] tableNames,
			String condition) throws StatementNotExecutedException {

		String query = "SELECT ";
		for (int i = 0; i < columns[0].length; i++) {
			query = query + columns[0][i];
			if (i < columns[0].length - 1)
				query = query + ", ";
			else
				query = query + " FROM ";
		}
		for (int i = 0; i < tableNames.length; i++) {
			query = query + tableNames[i];
			if (i < tableNames.length - 1)
				query = query + ", ";
			else
				query = query + condition;
		}
		Vector<String[]> result = new Vector<String[]>();
		try {
			ResultSet rs = createStatement(query);
			while (rs.next()) {
				String[] row = new String[columns[1].length];
				for (int i = 0; i < columns[0].length; i++) {
					if (columns[1][i].equals("int")) {
						row[i] = Integer.toString(rs.getInt(i + 1));
					} else if (columns[1][i].equals("float")) {
						row[i] = Float.toString(rs.getFloat(i + 1));
					} else {
						row[i] = rs.getString(i + 1);
					}
				}
				result.add(row);
			}
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return result;
	}

	/**
	 * gets status of the given record
	 * 
	 * @param key -
	 *            primary key fields
	 * @param keyValues -
	 *            primary key values
	 * @param tableName
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getStatus(Vector<String> key, int[] keyValues,
			String tableName) throws StatementNotExecutedException {
		String query = "Select Zustand FROM " + tableName + " WHERE ";
		for (int i = 0; i < key.size(); i++) {
			query = query + key.elementAt(i) + "=" + keyValues[i];
			if (i < key.size() - 1)
				query = query + " AND ";
			else
				query = query + ";";
		}
		try {
			ResultSet rs = createStatement(query);
			if (rs.next())
				return rs.getString(1);
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return null;
	}

	/**
	 * making error handling of queries which couldn't be dealt with
	 * 
	 * @param e:
	 *            exception which has been thrown
	 * @param query:
	 *            query which failed
	 * @throws StatementNotExecutedException
	 */
	public void printErrorMessage(SQLException e, String query)
			throws StatementNotExecutedException {
		if (_log.isErrorEnabled()) {
			_log.error("ErrorCode " + e.getErrorCode() + ", " + e.getMessage());
		}
		throw new StatementNotExecutedException(query);
	}
}
