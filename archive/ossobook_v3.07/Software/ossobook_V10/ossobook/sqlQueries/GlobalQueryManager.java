package ossobook.sqlQueries;

import java.sql.*;
import java.io.*;

import ossobook.client.LoginData;
import ossobook.communication.database.DbVerbindung;
import ossobook.communication.file.DbKonfiguration;
import ossobook.exceptions.ConnectionException;
import ossobook.exceptions.LoginWrongException;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * establishes and manages global connections
 * @author j.lamprecht
 *
 */
public class GlobalQueryManager extends QueryManager {
	
	private static File globalFile;
	
	private int benutzernr;
	
	/**
	 * establishes global connection
	 * @throws StatementNotExecutedException
	 * @throws ConnectionException
	 */
	public GlobalQueryManager() throws SQLException{
		DbKonfiguration DBC = new DbKonfiguration();
		String[] s = DBC.getInfoFromFile(globalFile);
		createGlobalConnection();
		init(s[1]);
		//benutzernr = getBenutzernummer(LoginData.getUser());
	}
	
	/**
	 * establishes connection
	 * @throws ConnectionException
	 */
	private void createGlobalConnection()throws SQLException{
		DbVerbindung db= new DbVerbindung(LoginData.getUser(), LoginData.getPassword(), globalFile);
		//try{
			connection = db.connect();
		//}		
		//catch (SQLException sql){
			//try{
				//ER_ACCESS_DENIED_ERROR
				//if(sql.getErrorCode()==1045){
				//	throw new LoginWrongException();
				//}
				//Connection couldn't be established (e.g. connection refused)
				//else {
					//sql.printStackTrace();
					//throw new ConnectionException();
				//}
			//}
		//}	
		//catch (Exception e){
			//e.printStackTrace();
		//}
	}
		
	//not used yet
	public int getBenutzernummer() throws StatementNotExecutedException{
		int result = getBenutzernummer(LoginData.getUser());
		return result;
	}
		
	public static String getStaticUser(){
		return LoginData.getUser();
	}
	
	public String getUser(){
		return getStaticUser();
	}
		
	public int getbenutzernr(){
		return benutzernr;
	}
		
	public static void setGlobalFile(File file){
		globalFile=file;
	}
	
	public static void setLoginData(String username, char[] password){
		LoginData.setUsername(username);
		LoginData.setPassword(password);
	}
	
	public static void setConvertedLoginData(String username, String password){
		LoginData.setUsername(username);
		LoginData.setStringPassword(password);
	}
}
