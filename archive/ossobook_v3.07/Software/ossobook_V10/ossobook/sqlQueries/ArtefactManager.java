package ossobook.sqlQueries;

import java.sql.*;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages artefaktmaske table (makes inserts, deletes, updates and select statements
 * on the table)
 * @author j.lamprecht
 *
 */
public class ArtefactManager extends TableManager{
	private static Log _log = LogFactory.getLog(ArtefactManager.class);
	// number of the local or global database of table version
	int versionDatabaseNumber;
	
	public ArtefactManager(Connection con, int databaseNumber, String databaseName){
		super(con, databaseName);
		this.versionDatabaseNumber=databaseNumber;
	}

	/**
	 * artefaktmaske records deleted from database by changing "geloescht" field for given project
	 * @precondition user has right to write on project
	 * @param projnr
	 * @throws StatementNotExecutedException
	 */
	public void deleteArtefaktMaske(int[] projectKey) throws StatementNotExecutedException{
		String query = "UPDATE " + databaseName+".artefaktMaske, " +databaseName+".eingabeeinheit "
			+ "SET artefaktMaske.geloescht='Y', artefaktMaske.Zustand='ge\u00E4ndert' "
			+ "WHERE eingabeeinheit.artefaktID=artefaktMaske.ID AND " 
			+ "eingabeeinheit.DBNummerArtefakt=artefaktmaske.Datenbanknummer AND "
			+ "eingabeeinheit.projNr='" + projectKey[0]
			+ "' AND eingabeeinheit.DBNummerProjekt=" + projectKey[1] + ";";
		executeStatement(query);
	}
	
	/**
	 * artefaktmaske records definitely deleted from database for given project
	 * @precondition method is executed on local database during synchronization
	 * @param projectKey
	 * @param event - QueryManager.DELETE_DELETED, QueryManager.DELETE_ALL
	 * @throws StatementNotExecutedException
	 */
	public void deleteArtefaktPermanent(int[] projectKey, int event) throws StatementNotExecutedException{
		String condition="";
		if(event==QueryManager.DELETE_DELETED) condition=" AND artefaktMaske.geloescht='Y' AND artefaktmaske.Zustand='synchronisiert'";
		String query = "DELETE artefaktMaske FROM " + databaseName+".artefaktMaske, " + databaseName+".eingabeeinheit "
			+ "WHERE eingabeeinheit.artefaktID=artefaktMaske.ID " 
			+ "AND eingabeeinheit.DBNummerArtefakt=artefaktMaske.Datenbanknummer " 
			+ "AND eingabeeinheit.projNr='"	+ projectKey[0] + "' "
			+ "AND eingabeeinheit.DBNummerProjekt=" +projectKey[1]
			+" " +condition+";";
		executeStatement(query);
	}
	
	/**
	 * inserts arbitrary record into artefaktmaske
	 * @param fieldsA
	 * @param valuesA
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int[] insertIntoArtefaktmaske(Vector<String> fieldsA, 
			Vector<String> valuesA) throws StatementNotExecutedException{
		String table = databaseName+".artefaktmaske";
		String auto = "ID";
		String query = "INSERT into " + table + " (";
		int[] artefact = new int[2];
		for (int i=0; i<fieldsA.size(); i++){
			query = query + fieldsA.elementAt(i) + ", ";
		}
		query = query + "Datenbanknummer, Zustand) VALUES (";
		for (int i=0; i<valuesA.size(); i++){
			query = query + "'" +valuesA.elementAt(i) + "', ";
		}	
		query = query + versionDatabaseNumber + ", 'ge\u00E4ndert');";
		try {
			artefact[0]=executeInsert(query, auto,  table);
			artefact[1]=versionDatabaseNumber;
			/*query = "SELECT MAX(ID) as mid FROM artefaktmaske;";
			ResultSet rs = createStatement(query);
			rs.next();
			ArtefaktID = rs.getInt("mid");*/
		} catch (SQLException e) {
			if(e.getErrorCode()==1364) {
				if (_log.isInfoEnabled()) {
					_log.info("Angabe fehlt");
				}
			}
			else{
				printErrorMessage(e, query);
			}
		}
		return artefact;
	}
	
	/**
	 * gets key for the given eingabeeinheit key
	 * @precondition user has right to read record
	 * @param datasetId: Id of a data record in the table eingabeeinheit
	 * @return the ID of the data record in the artefaktmaske table which belongs
	 * to the data set with the given datasetId in the table eingabeeinheit 
	 * @throws StatementNotExecutedException
	 */
	public int[] getArtefaktKey(int[] recordKey) throws StatementNotExecutedException{
		String query = "SELECT artefaktID, DBNummerArtefakt FROM " + databaseName+".eingabeeinheit WHERE ID=" + recordKey[0] +
			" AND Datenbanknummer="+ recordKey[1]+" AND geloescht='N';";
		int[] id=new int[2];
		try{
			ResultSet rs = createStatement(query);
			rs.next();
			id[0]=rs.getInt("artefaktID");
			id[1]=rs.getInt("DBNummerArtefakt");			
		}
		catch(SQLException e){
			printErrorMessage(e, query);
		}
		return id;
	}
	
	/**
	 * updates a given data record in the table artefaktmaske
	 * @precondition user has right to update record
	 * @param idvorspannArte: identifier name for the artefaktmaske record
	 * @param fieldsA: fields in artefaktmaske that shall be updated
	 * @param idvalueArte: identifier for the artefaktmaske record
	 * @param valuesA: values to which the fields shall be changed
	 * @throws StatementNotExecutedException
	 */
	public void updateArtefaktmaske(Vector<String> fieldsA, int[] artefactKey,
			Vector<String> valuesA) throws StatementNotExecutedException{
		String query = "UPDATE " + databaseName+".artefaktmaske SET ";
		for(int i=0; i<fieldsA.size(); i++){
			query=query+fieldsA.elementAt(i) + "='"+valuesA.elementAt(i)+"',";
		}
		query=query+" Zustand='ge\u00E4ndert'";
		query=query+ " WHERE ID=" + artefactKey[0] + " AND Datenbanknummer=" + artefactKey[1] + ";";
		try{
			executeUpdate(query);
		}
		catch(SQLException e){
			printErrorMessage(e, query);
		}
		
	}
	
	/**
	 * gets the artefaktmaske entry for given key
	 * @precondition user has right to read record
	 * @param artefactKey
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String[][] getArtefaktmaske(int[] artefactKey) throws StatementNotExecutedException{
		String query = "SELECT * FROM " + databaseName+".artefaktmaske WHERE ID='"
			+ artefactKey[0] + "'" +
					" AND geloescht='N' AND Datenbanknummer="+artefactKey[1]+";";
		String[][] result = new String[0][0];
		try{
			ResultSet rs = createStatement(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			rs.next();
			int columnCount=rsmd.getColumnCount();
			result = new String[columnCount][2];
			for(int i=0; i<columnCount; i++){
				result[i][0]=rsmd.getColumnName(i+1);
				result[i][1]=rs.getString(i+1);			
			} 
		}
		catch(SQLException s){
			printErrorMessage(s, query);
		}
		return result;
	}
	
	/**
	 * artefaktmaske record deleted from database by changing "geloescht" field
	 * @precondition user has right to "delete" record
	 * @param artefactID
	 * @throws StatementNotExecutedException
	 */
	public void deleteFromArtefaktmaske(int[] artefactID)throws StatementNotExecutedException{
		String query="UPDATE " + databaseName+".artefaktmaske SET geloescht='Y', Zustand='ge\u00E4ndert' WHERE ID='"
			+ artefactID[0] + "' AND Datenbanknummer=" + artefactID[1]+";";
		try{
			executeStatement(query);
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
	/**
	 * get all artefaktmaske entries which have been changed
	 * @param projectKey 
	 * @param columntypes columns of global database 
	 * -> needed to be independet from order and global database scheme changes
	 * @param event - QueryManager.CHANGES_LOCAL, QueryManager.CHANGES_GLOBAL, QueryManager.ALL_GLOBAL
	 * @param lastSynchronisation
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getEntries(int[] projectKey,String[][] columntypes, int event, String lastSynchronisation) throws StatementNotExecutedException{
		Vector<String[]> result;
		String condition=" WHERE projNr="+projectKey[0]+" AND eingabeeinheit.artefaktID=artefaktmaske.ID AND eingabeeinheit.DBNummerArtefakt=artefaktmaske.Datenbanknummer AND eingabeeinheit.DBNummerProjekt=" + projectKey[1]; 
		if(event==QueryManager.CHANGES_LOCAL) condition= condition + " AND artefaktmaske.Zustand='ge\u00E4ndert' AND artefaktmaske.Nachrichtennummer != -1";
		else if(event==QueryManager.CHANGES_GLOBAL)
			condition = condition + " AND artefaktmaske.Zustand>'" + lastSynchronisation + "'";
		String[] tables = {databaseName+".artefaktmaske", databaseName+".eingabeeinheit"};
		result = getEntries(columntypes, tables , condition);
		return result;
	}
	
	/**
	 * 
	 * @param projectKeyValues of global database
	 * -> needed to be independet from global database scheme changes
	 * @param columns of global database
	 * -> needed to be independet from order and global database scheme changes
	 * @param key
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getArtefactEntry(String[][] columns, int[] projectKeyValues, Vector<String> key) throws StatementNotExecutedException{
		String condition =" WHERE ";
		for(int i=0; i<key.size(); i++){
			condition = condition + key.elementAt(i) + "=" + projectKeyValues[i];
			if(i<key.size()-1) condition = condition + " AND ";
		}
		String[] tableNames = {databaseName+".artefaktmaske"};
		return getEntries(columns, tableNames, condition);
	}
	
	public void insertData(String[] scheme, String[] data) throws StatementNotExecutedException{
		insertData(scheme, data, databaseName+".artefaktmaske");
	}
	
	public int updateData(String[] scheme, String[] data, Vector<String> key) throws StatementNotExecutedException{
		return updateData(scheme, data, databaseName+".artefaktmaske", key);
	}
	
	/**
	 * set Zustand to 'synchronisiert' after server affirmed changing global database
	 * @precondition executed on local database during synchronization
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 */
	public void setSynchronized(int[] projectKey) throws StatementNotExecutedException{
		String query = "UPDATE " + databaseName+".artefaktmaske, " + databaseName+".eingabeeinheit SET artefaktmaske.Zustand='synchronisiert', artefaktmaske.Nachrichtennummer=-1 WHERE " +
				"ProjNr = '" + projectKey[0] + "' AND eingabeeinheit.DBNummerProjekt=" + projectKey[1] + " AND "+
				" eingabeeinheit.artefaktID=artefaktmaske.ID AND eingabeeinheit.DBNummerArtefakt=artefaktmaske.Datenbanknummer AND artefaktmaske.Nachrichtennummer!=-1;";
		try{
			executeUpdate(query);
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
	public String getLastChange(Vector<String> key, int[] keyValues) throws StatementNotExecutedException{
		return getStatus(key, keyValues, databaseName+".artefaktmaske");
	}
	
	/**
	 * assign message number to changed entries in artefaktmaske of given project
	 * @param message
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 */
	public void setMessageNumber(int message, int[] projectKey) throws StatementNotExecutedException{
		String query = "UPDATE " + databaseName + ".artefaktmaske, " + databaseName+".eingabeeinheit SET artefaktmaske.Nachrichtennummer=" + message 
			+ " WHERE ProjNr="+projectKey[0]+" AND eingabeeinheit.DBNummerProjekt=" + projectKey[1] 
			+ " AND eingabeeinheit.artefaktID=artefaktmaske.ID AND eingabeeinheit.DBNummerArtefakt=artefaktmaske.Datenbanknummer AND artefaktmaske.Zustand='ge\u00E4ndert';";
		try{
			executeStatement(query);
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
}
