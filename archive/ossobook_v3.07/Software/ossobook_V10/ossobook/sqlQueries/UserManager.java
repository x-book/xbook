package ossobook.sqlQueries;

import java.sql.*;
import java.util.*;

import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages benutzer table (makes inserts, deletes, updates and select statements
 * on the table)
 * manages rights of the users for the different projects
 * @author j.lamprecht
 *
 */
public class UserManager extends TableManager{
	
	//constants for changing rights
	public static final int NORIGHTS = 0;
	public static final int READ = 1;
	public static final int WRITE = 2;
	
	public UserManager(Connection con, String databaseName){
		super(con, databaseName);
	}

	// not used yet
	public int getBenutzernummer(String user) throws StatementNotExecutedException{
		// hole benutzernummer
		String query = "SELECT PIN FROM " + databaseName+".benutzer WHERE BenutzerName LIKE('"
			+ user + "');";
		int benutzernr = -1;
		try{
			ResultSet rs = createStatement(query);
			rs.next();
			benutzernr = rs.getInt("PIN");
		} catch(SQLException e){
			printErrorMessage(e, query);
		}
		return benutzernr;
	}
	
	//not used yet
	public String getBenutzername(int benutzernr) throws StatementNotExecutedException{
		String BenutzerName = "";
		String query="SELECT BenutzerName FROM " + databaseName+".benutzer WHERE PIN='"
			+ benutzernr + "';";
		try {
			ResultSet rs = createStatement(query);
			rs.next();
			BenutzerName = rs.getString("BenutzerName");
		} 
		catch (SQLException s){
			printErrorMessage(s, query);
		}
		return BenutzerName;
	}
	
	/**
	 * 
	 * @return the users of the database except the admins and the logged in user
	 * @throws StatementNotExecutedException
	 */
	public Vector<String> getUserNames() throws StatementNotExecutedException{
		Vector<String> users = new Vector<String>();
		String query = "SELECT BenutzerName FROM " + databaseName+".benutzer WHERE " +
				"BenutzerName!=SUBSTRING(USER(),1, LOCATE('@', USER())-1) AND" +
				" Admin!='Y' ORDER BY BenutzerName;";
		try{
			ResultSet rs = createStatement(query);
			while(rs.next()){
				users.add(rs.getString(1));
			}
		}
		catch(SQLException s){
			printErrorMessage(s, query);
		}
		return users;
	}
	
	/**
	 * proofs whether user may do everything
	 * @return true if User is administrator, else false
	 * @throws StatementNotExecutedException
	 */
	public boolean getIsAdmin() throws StatementNotExecutedException{
		boolean isAdmin = false;
		String query = "SELECT Admin FROM " + databaseName+".benutzer WHERE BenutzerName=SUBSTRING(USER(),1, LOCATE('@', USER())-1)" +
				" AND geloescht='N';";
		try{
			ResultSet rs = createStatement(query);
			rs.next();
			if(rs.getString(1).equals("Y")) return true; 
		}
		catch(SQLException s){
			printErrorMessage(s, query);
		}
		return isAdmin;
	}
	
	/**
	 * 
	 * @param ProjNr project number
	 * @return right which the User has for the project - read or read and write or nothing
	 * @throws StatementNotExecutedException
	 */
	public String getRight(int[] projectKey) throws StatementNotExecutedException{
		String right = null;
		String query = "SELECT Recht FROM " + databaseName+".projektrechte WHERE " +
				"Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1)" +
				" and ProjNr=" + projectKey[0] +" AND DBNummerProjekt=" + projectKey[1] + " AND geloescht='N';";
		try{
			ResultSet rs = createStatement(query);
			if(rs.next()){
				if(rs.getString(1).equals("schreiben")) return "schreiben"; 
				if(rs.getString(1).equals("lesen")) return "lesen";
			}
		}
		catch(SQLException s){
			printErrorMessage(s, query);
		}
		return right;
	}
	
	/**
	 * @precondition: "changer" is project owner or administrator
	 * gives / changes /revokes right to/ of / from given user for given project
	 * @param ProjNr 
	 * @param user whose right shall be changed
	 * @param right
	 * @throws StatementNotExecutedException
	 */
	public void changeRight(int[] projectKey, String user, int right)
	throws StatementNotExecutedException{
		String query="";
		if(user==null)
			user="SUBSTRING(USER(),1, LOCATE('@', USER())-1)";
		else user ="'"+user+"'";
		try{
			if(right==NORIGHTS) {
				query="UPDATE " + databaseName+".projektrechte SET geloescht='Y' WHERE " +
						"ProjNr=" + projectKey[0] +" AND Benutzer="+user + 
						" AND DBNummerProjekt="+projectKey[1]+";";
				executeUpdate(query);
			}
			else {
				query="UPDATE " + databaseName+".projektrechte SET Recht="+ right +" WHERE " +
					"ProjNr=" + projectKey[0] +" AND Benutzer="+user +
					" AND DBNummerProjekt="+projectKey[1]+";";
				int alreadyexists = executeUpdate(query);
				if(alreadyexists<1){
					query="INSERT INTO " + databaseName+".projektrechte (Benutzer,ProjNr, DBNummerProjekt, Recht) " +
							"VALUES ("+ user +", "+ projectKey[0] + ", " +projectKey[1]+ ", " +
							right + ");";
					executeUpdate(query);
				}
			}
		}
		catch(SQLException s){
				printErrorMessage(s, query);
		}
	}
	
	/**
	 * get e-mail address of project owner
	 * @param projectKey primary key of project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getMail(int[] projectKey) throws StatementNotExecutedException{
		String query ="SELECT Mail FROM " + databaseName+".projekt, " + databaseName+".benutzer WHERE " +
				"projNr=" + projectKey[0] + " AND Datenbanknummer=" + projectKey[1] +
				" AND ProjEigentuemer=BenutzerName AND benutzer.geloescht='N'";
		String mail="";
		try{
			ResultSet rs = createStatement(query);
			if(rs.next()){
				mail=rs.getString(1);
			}
		}
		catch(SQLException s){
			printErrorMessage(s, query);
		}
		return mail;
	}
}
