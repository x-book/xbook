package ossobook.sqlQueries;

import java.sql.*;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages eingabeeinheit table (makes inserts, deletes, updates and select
 * statements on the table)
 * 
 * @author j.lamprecht
 * 
 */
public class InputUnitManager extends TableManager {
	private static Log _log = LogFactory.getLog(InputUnitManager.class);
	int versionDatabaseNumber;

	public InputUnitManager(Connection con, int databaseNumber,
			String databaseName) {
		super(con, databaseName);
		this.versionDatabaseNumber = databaseNumber;
	}

	/**
	 * @precondition user has the right to read project
	 * @param text
	 * @param projNr
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getCountFk(String text, int[] projectKey)
			throws StatementNotExecutedException {
		String query = "SELECT COUNT(fK) AS fk FROM " + databaseName
				+ ".eingabeeinheit WHERE fK LIKE('" + text + "')AND projNr="
				+ projectKey[0] + " " + " AND DBNummerProjekt=" + projectKey[1]
				+ " AND geloescht='N';";
		int zahl = -1;
		try {
			ResultSet rs = createStatement(query);
			rs.next();
			zahl = rs.getInt("fk");

		} catch (SQLException s) {
			printErrorMessage(s, query);
		}
		return zahl;
	}

	/**
	 * @precondition user has the right to read project
	 * @param number
	 * @param projNr
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getCountInventar(int number, int[] projectKey)
			throws StatementNotExecutedException {
		String query = "SELECT COUNT(inventarNr) AS inventarNr FROM "
				+ databaseName + ".eingabeeinheit WHERE inventarNr LIKE('"
				+ number + "') AND projNr=" + projectKey[0]
				+ " AND DBNummerProjekt=" + projectKey[1]
				+ " AND geloescht='N';";
		int zahl = -1;
		try {
			ResultSet rs = createStatement(query);
			rs.next();
			zahl = rs.getInt("inventarNr");
		} catch (SQLException s) {
			printErrorMessage(s, query);
		}
		return zahl;
	}

	/**
	 * @precondition user has the right to read project
	 * @param number
	 * @param projNr
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getCountSkelNr(int number, int[] projectKey)
			throws StatementNotExecutedException {
		String query = "SELECT COUNT(skelNr) AS sknr FROM " + databaseName
				+ ".eingabeeinheit WHERE skelNr=" + number + " AND projNr="
				+ projectKey[0] + "AND DBNummerProjekt=" + projectKey[1]
				+ " AND geloescht='N';";
		int zahl = -1;
		try {
			ResultSet rs = createStatement(query);
			rs.next();
			zahl = rs.getInt("sknr");
		} catch (SQLException s) {
			printErrorMessage(s, query);
		}
		return zahl;
	}

	/**
	 * @precondition user has the right to read project
	 * @param number
	 * @param projNr
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getCountKnIndNr(int number, int[] projectKey)
			throws StatementNotExecutedException {
		String query = "SELECT COUNT(knIndNr) AS knnr FROM " + databaseName
				+ ".eingabeeinheit WHERE knIndNr=" + number + " AND projNr="
				+ projectKey[0] + " AND DBNummerProjekt=" + projectKey[1]
				+ " AND geloescht='N';";
		int zahl = -1;
		try {
			ResultSet rs = createStatement(query);
			rs.next();
			zahl = rs.getInt("knnr");
		} catch (SQLException s) {
			printErrorMessage(s, query);
		}
		return zahl;
	}

	/**
	 * @precondition user has the right to read project
	 * @param ProjNr
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getBonesNumber(int[] projectKey)
			throws StatementNotExecutedException {
		int anzahl = 0;
		String query = "SELECT SUM(Anzahl) as SUMAnzahl FROM " + databaseName
				+ ".eingabeeinheit WHERE projNr='" + projectKey[1]
				+ "' AND DBNummerProjekt=" + projectKey[1]
				+ " AND geloescht='N';";
		try {
			ResultSet rs = createStatement(query);
			while (rs.next()) {
				anzahl = rs.getInt("SUMAnzahl");
			}
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return anzahl;
	}

	/**
	 * @precondition user has the right to read project
	 * @param ProjNr
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public double getOverallWeight(int[] projectKey)
			throws StatementNotExecutedException {
		double gewicht = 0;
		String query = "SELECT SUM(gewicht) as gewicht FROM " + databaseName
				+ ".eingabeeinheit WHERE projNr='" + projectKey[0]
				+ "' AND DBNummerProjekt=" + projectKey[1]
				+ " AND geloescht='N';";
		try {
			ResultSet rs = createStatement(query);
			while (rs.next()) {
				gewicht = 0.0;
				try {
					gewicht = Double.parseDouble(rs.getString("gewicht"));
				} catch (Exception e) {
				}
			}
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return gewicht;
	}

	/**
	 * @precondition user has the right to delete from project
	 * @param projnr
	 * @throws StatementNotExecutedException
	 */
	public void deleteUnit(int[] projectKey)
			throws StatementNotExecutedException {
		String query = "UPDATE "
				+ databaseName
				+ ".eingabeeinheit SET geloescht='Y', Zustand='ge\u00E4ndert' WHERE eingabeeinheit.projNr='"
				+ projectKey[0] + "' AND DBNummerProjekt=" + projectKey[1]
				+ ";";
		executeStatement(query);
	}

	/**
	 * eingabeeinheit records definitely deleted from database for given project
	 * 
	 * @precondition method is executed on local database during synchronization
	 * @param projectKey
	 * @param event -
	 *            QueryManager.DELETE_DELETED, QueryManager.DELETE_ALL
	 * @throws StatementNotExecutedException
	 */
	public void deleteUnitPermanent(int[] projectKey, int event)
			throws StatementNotExecutedException {
		String condition = "";
		if (event == QueryManager.DELETE_DELETED)
			condition = " AND geloescht='Y' AND Zustand='synchronisiert'";
		String query = "DELETE FROM " + databaseName
				+ ".eingabeeinheit WHERE eingabeeinheit.projNr='"
				+ projectKey[0] + "' AND DBNummerProjekt=" + projectKey[1]
				+ " " + condition + ";";
		executeStatement(query);
	}

	/**
	 * @precondition user has the right to write in project
	 * @param idvorspann
	 * @param idvalue
	 * @param projektNummer
	 * @param fields
	 * @param benutzerNummer
	 * @param ArtefaktID
	 * @param massID
	 * @param phase1
	 * @param phase2
	 * @param phase3
	 * @param phase4
	 * @param values
	 * @throws StatementNotExecutedException
	 */
	public void insertIntoEingabeeinheit(int[] projectKey,
			Vector<String> fields, int[] ArtefaktID, int[] massID,
			String phase1, String phase2, String phase3, String phase4,
			Vector<String> values) throws StatementNotExecutedException {

		String query = "INSERT into " + databaseName + ".eingabeeinheit ("
				+ "ProjNr,DBNummerProjekt," + "BenutzerName,"
				+ "ArtefaktID,DBNummerArtefakt" + ",massID,DBNummerMasse"
				+ ",phase1,phase2,phase3,phase4, ";
		for (int i = 0; i < fields.size(); i++) {
			query = query + fields.elementAt(i) + ", ";
		}
		query = query + "Datenbanknummer, Zustand) Values (" + projectKey[0]
				+ "," + projectKey[1] + ", "
				+ "SUBSTRING(USER(),1, LOCATE('@', USER())-1)" + ","
				+ ArtefaktID[0] + ", " + ArtefaktID[1] + "," + massID[0] + ", "
				+ massID[1] + "," + phase1 + ", " + phase2 + ", " + phase3
				+ ", " + phase4 + ", ";
		for (int i = 0; i < values.size(); i++) {
			query = query + "'" + values.elementAt(i) + "', ";
		}
		query = query + versionDatabaseNumber + ", 'ge\u00E4ndert');";
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query);
		}
		executeStatement(query);
	}

	/**
	 * @precondition user has the right to write in project
	 * @param idvorspann
	 * @param idvalue
	 * @param projektNummer
	 * @param fields
	 * @param benutzerNummer
	 * @param ArtefaktID
	 * @param massID
	 * @param phase1
	 * @param phase2
	 * @param phase3
	 * @param phase4
	 * @param values
	 * @throws StatementNotExecutedException
	 */
	public void updateEingabeeinheit(int[] recordKey, int[] projectKey,
			Vector<String> fields, int[] ArtefaktKey, int[] massKey,
			String phase1, String phase2, String phase3, String phase4,
			Vector<String> values) throws StatementNotExecutedException {

		String query = "UPDATE " + databaseName + ".eingabeeinheit SET "
				+ "ProjNr=" + projectKey[0] + ",DBNummerProjekt="
				+ projectKey[1]
				+ ",BenutzerName=SUBSTRING(USER(),1, LOCATE('@', USER())-1)"
				+ ",ArtefaktID=" + ArtefaktKey[0] + ",DBNummerArtefakt="
				+ ArtefaktKey[1] + ",massID=" + massKey[0] + ",DBNummerMasse="
				+ massKey[1] + ",phase1=" + phase1 + ",phase2=" + phase2
				+ ",phase3=" + phase3 + ",phase4=" + phase4
				+ ",Zustand='ge\u00E4ndert'";

		for (int i = 0; i < fields.size(); i++) {
			query = query + "," + fields.elementAt(i) + "='"
					+ values.elementAt(i) + "'";
		}

		query = query + " WHERE ID =" + recordKey[0] + " AND Datenbanknummer="
				+ recordKey[1] + ";";
		executeStatement(query);
	}

	/**
	 * @precondition user has the right to read project
	 * @param phase
	 * @param aktuelleDatensatzID
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getPhaseValue(String phase, int[] recordKey)
			throws StatementNotExecutedException {
		String query = "SELECT " + phase + " FROM " + databaseName
				+ ".eingabeeinheit WHERE ID=" + "'" + recordKey[0]
				+ "' AND Datenbanknummer=" + recordKey[1]
				+ " AND geloescht='N';";
		String result = "";
		try {
			ResultSet rs = createStatement(query);
			rs.next();
			result = rs.getString(1);
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return result;
	}

	/**
	 * @precondition user has the right to read project
	 * @param aktuelleDatensatzID
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String[][] getDataRecord(int[] recordKey)
			throws StatementNotExecutedException {
		String query = "SELECT * FROM " + databaseName
				+ ".eingabeeinheit WHERE ID='" + recordKey[0]
				+ "' AND Datenbanknummer=" + recordKey[1]
				+ " AND geloescht='N';";
		String[][] result = new String[0][0];
		try {
			ResultSet rs = createStatement(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			rs.next();
			int columnCount = rsmd.getColumnCount();
			result = new String[columnCount][2];
			for (int i = 0; i < columnCount; i++) {
				result[i][0] = rsmd.getColumnName(i + 1);
				result[i][1] = rs.getString(i + 1);
			}
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return result;
	}

	/**
	 * @precondition user has the right to read project
	 * @param phasenlabel
	 * @param projektnummer
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String[][] getFK(String phasenlabel, int[] projectKey)
			throws StatementNotExecutedException {
		String query = "SELECT fK," + phasenlabel + " FROM " + databaseName
				+ ".eingabeeinheit WHERE projNr=" + projectKey[0]
				+ " AND DBNummerProjekt=" + projectKey[1]
				+ " AND geloescht='N' GROUP BY fK ORDER BY " + phasenlabel
				+ ",fK;";
		String[][] phase = new String[0][0];
		try {
			int i = 0;
			Vector<String> fk = new Vector<String>();
			Vector<String> label = new Vector<String>();
			ResultSet rs = createStatement(query);
			while (rs.next()) {
				fk.add(rs.getString("fK"));
				label.add(rs.getString(phasenlabel));
				i++;
			}
			phase = new String[i][2];
			for (int j = 0; j < i; j++) {
				phase[j][0] = fk.get(j);
				phase[j][1] = label.get(j);
			}
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return phase;
	}

	/**
	 * @precondition user has the right to write in project
	 * @param aktuelleDatensatzID
	 * @throws StatementNotExecutedException
	 */
	public void deleteFromEingabeeinheit(int[] recordID)
			throws StatementNotExecutedException {
		String query = "UPDATE "
				+ databaseName
				+ ".eingabeeinheit SET geloescht='Y', Zustand='ge\u00E4ndert' WHERE ID='"
				+ recordID[0] + "' AND Datenbanknummer=" + recordID[1] + ";";
		try {
			executeStatement(query);
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}

	/**
	 * @precondition user has the right to write in project
	 * @param phasenlabel
	 * @param content
	 * @param label
	 * @param projektnummer
	 * @throws StatementNotExecutedException
	 */
	public void update(String phasenlabel, String content, String label,
			int[] projectKey) throws StatementNotExecutedException {
		String query = "UPDATE " + databaseName + ".eingabeeinheit SET "
				+ phasenlabel + "='" + content + "', Zustand='ge\u00E4ndert'"
				+ " WHERE fK LIKE('" + label + "') AND ProjNr=" + projectKey[0]
				+ " AND DBNummerProjekt=" + projectKey[1] + ";";
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}

	/**
	 * @precondition user has the right to read project
	 * @param datasetId:
	 *            Id of a data record in the table eingabeeinheit
	 * @return the ID of the data record in the Masse table which belongs to the
	 *         data set with the given datasetId in the table eingabeeinheit
	 * @throws StatementNotExecutedException
	 */
	public int[] getMassId(int[] recordKey)
			throws StatementNotExecutedException {
		String query = "SELECT massID, DBNummerMasse FROM " + databaseName
				+ ".eingabeeinheit WHERE ID=" + recordKey[0]
				+ " AND Datenbanknummer=" + recordKey[1]
				+ " AND geloescht='N';";
		int[] id = new int[2];
		try {
			ResultSet rs = createStatement(query);
			rs.next();
			id[0] = rs.getInt("massID");
			id[1] = rs.getInt("DBNummerMasse");
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return id;
	}

	/**
	 * get all eingabeeinheit entries which have been changed
	 * 
	 * @param projectKey
	 * @param columntypes
	 *            columns of global database -> needed to be independet from
	 *            order and global database scheme changes
	 * @param event -
	 *            QueryManager.CHANGES_LOCAL, QueryManager.CHANGES_GLOBAL,
	 *            QueryManager.ALL_GLOBAL
	 * @param lastSynchronisation
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getEntries(int[] projectKey,
			String[][] columntypes, int event, String lastSynchronisation)
			throws StatementNotExecutedException {
		Vector<String[]> result;
		String condition = " WHERE eingabeeinheit.DBNummerProjekt="
				+ projectKey[1] + " AND projNr=" + projectKey[0];
		if (event == QueryManager.CHANGES_LOCAL)
			condition = condition
					+ " AND eingabeeinheit.Zustand='ge\u00E4ndert' AND eingabeeinheit.Nachrichtennummer != -1";
		else if (event == QueryManager.CHANGES_GLOBAL)
			condition = condition + " AND eingabeeinheit.Zustand>'"
					+ lastSynchronisation + "'";
		String[] tables = { databaseName + ".eingabeeinheit" };
		result = getEntries(columntypes, tables, condition);
		return result;
	}

	/**
	 * 
	 * @param projectKeyValues
	 *            of global database -> needed to be independet from global
	 *            database scheme changes
	 * @param columns
	 *            of global database -> needed to be independet from order and
	 *            global database scheme changes
	 * @param key
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getUnitEntry(String[][] columns,
			int[] projectKeyValues, Vector<String> key)
			throws StatementNotExecutedException {
		String condition = " WHERE ";
		for (int i = 0; i < key.size(); i++) {
			condition = condition + key.elementAt(i) + "="
					+ projectKeyValues[i];
			if (i < key.size() - 1)
				condition = condition + " AND ";
		}
		String[] tableNames = { databaseName + ".eingabeeinheit" };
		return getEntries(columns, tableNames, condition);
	}

	public void insertData(String[] scheme, String[] data)
			throws StatementNotExecutedException {
		insertData(scheme, data, databaseName + ".eingabeeinheit");
	}

	public int updateData(String[] scheme, String[] data, Vector<String> key)
			throws StatementNotExecutedException {
		return updateData(scheme, data, databaseName + ".eingabeeinheit", key);
	}

	/**
	 * set Zustand to 'synchronisiert' after server affirmed changing global
	 * database
	 * 
	 * @precondition executed on local database during synchronization
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 */
	public void setSynchronized(int[] projectKey)
			throws StatementNotExecutedException {
		String query = "UPDATE "
				+ databaseName
				+ ".eingabeeinheit SET Zustand='synchronisiert', eingabeeinheit.Nachrichtennummer=-1 WHERE "
				+ "ProjNr = '" + projectKey[0] + "' AND DBNummerProjekt="
				+ projectKey[1] + " AND eingabeeinheit.Nachrichtennummer!=-1;";
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}

	public String getLastChange(Vector<String> key, int[] keyValues)
			throws StatementNotExecutedException {
		return getStatus(key, keyValues, databaseName + ".eingabeeinheit");
	}

	public void setMessageNumber(int message, int[] projectKey)
			throws StatementNotExecutedException {
		String query = "UPDATE " + databaseName
				+ ".eingabeeinheit SET Nachrichtennummer=" + message
				+ " WHERE ProjNr=" + projectKey[0] + " AND DBNummerProjekt="
				+ projectKey[1] + " AND Zustand='ge\u00E4ndert';";
		try {
			executeStatement(query);
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}

	public int getNextFreeIndividualNr(int[] projectKey) 
		throws StatementNotExecutedException {
	String query = "SELECT MAX(knIndNr) AS knnr FROM " + databaseName
			+ ".eingabeeinheit WHERE projNr="
			+ projectKey[0] + " AND DBNummerProjekt=" + projectKey[1]
			+ " AND geloescht='N';";
	int zahl = -1;
	try {
		ResultSet rs = createStatement(query);
		rs.next();
		zahl = rs.getInt("knnr");
	} catch (SQLException s) {
		printErrorMessage(s, query);
	}
	return zahl;
	}

}
