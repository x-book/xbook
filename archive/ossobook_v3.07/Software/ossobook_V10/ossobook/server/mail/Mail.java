package ossobook.server.mail;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;
import java.io.*;
import javax.activation.*;

import ossobook.communication.file.DbKonfiguration;

/**
 * class to send conflict mails
 * @author j.lamprecht
 *
 */
public class Mail {

	private static File configFile;
	// project owner
	private String recipient;
	// ossobook server
	private String sender;
	// mail relay server
	private String server;
	// password for login on mail relay server
	private String password;
	// usernam for login on mail relay server
	private String username;
	private String projectName;
	
	/**
	 * initialize mail to send
	 * @param recipient - project owner of given project
	 * @param projectName
	 */
	public Mail(String recipient, String projectName){
		
		DbKonfiguration conf= new DbKonfiguration();
		String[] info=conf.getInfoFromFile(configFile);
		server=info[0];
		username=info[1];
		password=info[2];
		sender = info[3];
		
		this.recipient=recipient;	
		this.projectName=projectName;
	}
	
	/**
	 * send conflict mail, append given conflict file
	 * @param conflictFile - file that contains occurred conflicts
	 */
	public void sendConflictMail(File conflictFile){
			String warningMessage="W\u00E4hrend einer Synchronisation des Projekts \""+projectName+"\"" +
					" sind Konflikte aufgetreten. Im Anhang finden Sie hierzu weitere Informationen. " +
					"Bitte \u00FCberpr\u00FCfen Sie den Datenbankinhalt!";
		try{
			Properties props = new Properties();
			props.put("mail.smtp.host", server);
			Session s = Session.getInstance(props, null);
			props.put("mail.smtp.auth", "true");
			
			Message message = new MimeMessage(s);
			InternetAddress from = new InternetAddress(sender);
			message.setFrom(from);
			InternetAddress to = new InternetAddress(recipient);
			message.addRecipient(Message.RecipientType.TO, to);		
			message.setSubject("Konflikte in der Ossobook-Datenbank aufgetreten f\u00FCr Projekt "+projectName+"!");
			
			MimeMultipart content = new MimeMultipart();
			
			MimeBodyPart messageText = new MimeBodyPart();
			messageText.setContent(warningMessage, "text/plain");
			content.addBodyPart(messageText);
			
			MimeBodyPart file = new MimeBodyPart();
			FileDataSource fds = new FileDataSource(conflictFile);
			file.setDataHandler(new DataHandler(fds));
			file.setFileName(fds.getName());			
			content.addBodyPart(file);
			message.setContent(content,"multipart/mixed");

			Transport transport = s.getTransport("smtp");
			transport.connect(server, username, password);
			message.saveChanges(); 
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		}
		catch(AddressException ad){
			ad.printStackTrace();
		}
		catch(MessagingException m){
			m.printStackTrace();
		}
	}
	
	/**
	 * get information file about mail relay server
	 * @param config
	 */
	public static void setConfigFile(File config){
		configFile=config;
	}	
}
