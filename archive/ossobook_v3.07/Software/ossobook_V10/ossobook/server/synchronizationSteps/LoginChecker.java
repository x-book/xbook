package ossobook.server.synchronizationSteps;

import ossobook.exceptions.*;

public class LoginChecker extends Connection {

	public Boolean checkLogin(byte[] username, byte[] password){
		try{
		dbConnection(username, password); 
		}
		catch (LoginWrongException e){
			return false;
		}
		catch (ConnectionException c){
			return null;
		}
		return true;
	}
	
}
