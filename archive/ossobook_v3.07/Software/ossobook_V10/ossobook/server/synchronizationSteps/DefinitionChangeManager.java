package ossobook.server.synchronizationSteps;

import java.util.Vector;

import ossobook.exceptions.*;
import ossobook.sqlQueries.*;

/**
 * controls characteristics synchronization of single tables on server part
 * @author j.lamprecht
 *
 */
public class DefinitionChangeManager extends Connection {

	/**
	 * get changes of the given characteristics table from global database
	 * @param username
	 * @param password
	 * @param tableName - characteristics table
	 * @param lastSynchronization - last time client synchronized table
	 * @return
	 */
	public Vector<String[]> getDefinitionChanges(byte[] username, byte[] password,
			String tableName, String lastSynchronization){
		GlobalQueryManager manager = null;
		Vector<String[]> data = null;
		try{
			manager = dbConnection(username, password);
			
			int level=manager.setIsolationLevel(QueryManager.ISOLATION_LEVEL);
			manager.disableAutoCommit();			
			//table entries exists only on global system
			if(lastSynchronization!=null) 
				data=manager.getDefinitionData(tableName,lastSynchronization, QueryManager.CHANGES_GLOBAL);
			else {
				data= manager.getDefinitionData(tableName, lastSynchronization, QueryManager.ALL_GLOBAL);
			}
			Thread.sleep(1000);
			manager.enableAutoCommit();
			manager.setIsolationLevel(level);
		} 
		catch (MetaStatementNotExecutedException e){
    		manager.closeConnection();
    	}
		catch (StatementNotExecutedException e){
    		manager.closeConnection();
    	}
		catch(InterruptedException i){
			manager.closeConnection();
		}
    	catch(LoginWrongException l){
    		l.printStackTrace();
    	}
    	catch(ConnectionException c){
    		c.printStackTrace();
    	}
		return data; 
	}

}
