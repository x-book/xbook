package ossobook.server.synchronizationSteps;

import java.util.*;

import ossobook.exceptions.ConnectionException;
import ossobook.exceptions.LoginWrongException;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.GlobalQueryManager;
import ossobook.client.base.metainfo.*;

public class GlobalProjectsGetter extends Connection {

	public Vector<Projekt> getGlobalProjects(byte[] username, byte[] password){
		
		GlobalQueryManager manager = null;
		
		try{
			manager = dbConnection(username, password);
			return manager.getProjects();
		}
		catch (StatementNotExecutedException e){
    		manager.closeConnection();
    	}
	   	catch(LoginWrongException l){
    		l.printStackTrace();
    	}
    	catch(ConnectionException c){
    		c.printStackTrace();
    	}	
    	return null;
	}
	
}
