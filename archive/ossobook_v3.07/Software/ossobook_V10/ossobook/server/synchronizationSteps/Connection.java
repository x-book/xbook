package ossobook.server.synchronizationSteps;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.sql.*;

import ossobook.client.config.KonfigurationOssobook;
import ossobook.exceptions.*;
import ossobook.server.encryption.KeyGeneration;
import ossobook.sqlQueries.GlobalQueryManager;

/**
 * upper class of all synchronization steps of the server
 * @author j.lamprecht
 *
 */
public abstract class Connection {

	private static final File globalDb = new File (KonfigurationOssobook.CONFIG_DIR+"ossobookDBConfig.txt");
	private KeyGeneration generator;
	
	public Connection(){
		generator = new KeyGeneration();
	}
	
	/**
	 * connect to global database
	 * @param username - of client
	 * @param password - of client
	 * @return
	 */
    public GlobalQueryManager dbConnection(byte[] username, byte[] password) throws LoginWrongException, ConnectionException{
    	
    	GlobalQueryManager manager = null;
    	try{
	    	String usernameDec = new String(generator.decryptUsername(username), "UTF-16");
	    	String passwordDec = new String (generator.decryptPassword(password), "UTF-16");
    		GlobalQueryManager.setConvertedLoginData(usernameDec, passwordDec);
    		GlobalQueryManager.setGlobalFile(globalDb);
    		manager = new GlobalQueryManager();
    	}
    	catch(StatementNotExecutedException e){}
    	catch(SQLException sql){
	    	sql.printStackTrace();
	    	//ER_ACCESS_DENIED_ERROR
			if(sql.getErrorCode()==1045) throw new LoginWrongException(false);
			else throw new ConnectionException(false);
	    }
	    	
    	catch (UnsupportedEncodingException e){
    		e.printStackTrace();
    	}
    	return manager;
    }
	
}
