package ossobook.server.synchronizationSteps;

import ossobook.exceptions.ConnectionException;
import ossobook.exceptions.LoginWrongException;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.*;

public class AdminChecker extends Connection {

	public boolean isAdmin(byte[] username, byte[] password){
		
		GlobalQueryManager manager = null;
		
		try{
			manager = dbConnection(username, password);
			return manager.getIsAdmin();
		}
		catch (StatementNotExecutedException e){
    		manager.closeConnection();
    	}
	   	catch(LoginWrongException l){
    		l.printStackTrace();
    	}
    	catch(ConnectionException c){
    		c.printStackTrace();
    	}	
    	return false;
	}
	
}
