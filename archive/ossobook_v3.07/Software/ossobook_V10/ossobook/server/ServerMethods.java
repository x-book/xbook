package ossobook.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.*;

import ossobook.client.base.metainfo.*;
import ossobook.communication.file.*;
import ossobook.modell.*;


/**
 * defines remote methods for synchronization
 * @author j.lamprecht
 *
 */
public interface ServerMethods extends Remote {
	public boolean isAdmin(byte[] username, byte[] password) throws RemoteException;
	public Boolean checkLogin( byte[] username, byte[] password) throws RemoteException;
    public Vector<Projekt> getGlobalProjects( byte[] username, byte[] password) throws RemoteException;
	public Vector<Table> synchronizeDatabaseScheme(byte[] username, byte[] password) throws RemoteException;
    public Vector<String[][]> initializeProject(byte[] username, byte[] password, int[] projectKey) 
    	throws RemoteException;
    public Logging insertLocalChanges(byte[] username, byte[] password, int[] projectName,
    		Vector<String[][]> changes, String lastSynchronization, int[] messageIdentifier)throws RemoteException;
    public Vector<String[][]> getProjectChanges(byte[] username, byte[] password, int[] projectKey,
    		String lastSynchronisation)	throws RemoteException;
    public Vector<String[]> getDefinitionChanges(byte[] username, byte[] password,
    		String tableName, String lastSynchronization) throws RemoteException;
}
