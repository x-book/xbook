package ossobook.server.encryption;

import java.security.*;
import javax.crypto.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.client.config.KonfigurationOssobook;

import java.security.interfaces.*;
import java.security.spec.*;
import java.math.*;
import java.io.*;
import java.util.*;

/**
 * creation of private and public keys
 * used for the encryption and decryption of password and username transmission
 * during client / server synchronization
 * @author j.lamprecht
 *
 */
public class KeyGeneration {
	private static Log _log = LogFactory.getLog(KeyGeneration.class);
	private final static File publicFile = new File(KonfigurationOssobook.CONFIG_DIR+"publicKey.txt");
	private final static File privateFile = new File(KonfigurationOssobook.CONFIG_DIR+"privateKey.txt");
		
	/**
	 * generates private and public keys
	 */
	public void generateKeys(){
		try{			
			KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
			keyPairGen.initialize(1024);
			KeyPair keyPair = keyPairGen.generateKeyPair();
			RSAPrivateKey privKey = (RSAPrivateKey) keyPair.getPrivate();
			RSAPublicKey pubKey = (RSAPublicKey) keyPair.getPublic();
					
			BigInteger privateExp = privKey.getPrivateExponent();
			BigInteger publicExp = pubKey.getPublicExponent();
			BigInteger modulus = privKey.getModulus();
			
			savePublic(publicExp, modulus);
			savePrivate(privateExp, modulus);
			testKeys();
		}
		catch (NoSuchAlgorithmException e){
			e.printStackTrace();
		}
		

	}
	
	/**
	 * saves public Exponent and modulus n to public file
	 * shall be saved on every client with local database
	 * @param publicExp
	 * @param modulus
	 */
	private void savePublic(BigInteger publicExp, BigInteger modulus){
		try{
			publicFile.createNewFile();
			FileWriter fw = new FileWriter(publicFile);
			BufferedWriter bw = new BufferedWriter(fw);
			String pubExp = publicExp.toString();
			String mod = modulus.toString();
			//exponent could contain lf
			bw.write(Integer.toString(pubExp.length()));
			bw.newLine();
			bw.write(pubExp);
			bw.newLine();
			bw.write(mod);
			bw.flush();
			bw.close();
		}
		catch (IOException io){
			io.printStackTrace();
		}
	}
	
	/**
	 * saves private Exponent and modulus n to public file
	 * may only be saved on server
	 * @param publicExp
	 * @param modulus
	 */
	private void savePrivate(BigInteger privateExp, BigInteger modulus){
		try{
			privateFile.createNewFile();
			FileWriter fw = new FileWriter(privateFile);
			BufferedWriter bw = new BufferedWriter(fw);
			String privExp = privateExp.toString();
			String mod = modulus.toString();
			//exponent could contain lf
			bw.write(Integer.toString(privExp.length()));
			bw.newLine();
			bw.write(privExp);
			bw.newLine();
			bw.write(mod);
			bw.flush();
			bw.close();
		}
		catch (IOException io){
			io.printStackTrace();
		}
	}
		
	/**
	 * tests whether encryption and decryption with generated keys works
	 */
	private void testKeys(){
		String data = "a string to test keys";
		if (_log.isDebugEnabled()) {
			_log.debug("testdata: "+data);
		}
		try{
			FileReader fr = new FileReader(publicFile);
			BufferedReader br = new BufferedReader(fr);
			int publicExpLength = Integer.parseInt(br.readLine());
			String pubBuf = br.readLine();
			//exponent could contain lf
			while(pubBuf.length()<publicExpLength){
				pubBuf=pubBuf+ br.readLine();
			}
			BigInteger publicExp = new BigInteger(pubBuf);
			BigInteger modulus = new BigInteger(br.readLine());
					
			RSAPublicKeySpec pubSpec = new RSAPublicKeySpec(modulus, publicExp);
			PublicKey testPub = KeyFactory.getInstance("RSA").generatePublic(pubSpec);
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, testPub);
			byte [] chiffr= cipher.doFinal(data.getBytes("UTF-16"));
			if (_log.isDebugEnabled()) {
				_log.debug("encrypted testdata: " + chiffr);
			}
			
			byte[] result = decrypt(chiffr);
			String s=new String(result, 0, result.length, "UTF-16");
			if (_log.isDebugEnabled()) {
				_log.debug("decrypted testdata: " + s);
			}
			System.out.println();
			
		}
		catch (IOException io){
			io.printStackTrace();
		}
		catch (NoSuchPaddingException p){
			p.printStackTrace();
		}
		catch (InvalidKeyException i){
			i.printStackTrace();
		}
		catch (IllegalBlockSizeException e){
			e.printStackTrace();
		}
		catch (BadPaddingException e){
			e.printStackTrace();
		}		
		catch (InvalidKeySpecException e){
			e.printStackTrace();
		}
		catch (NoSuchAlgorithmException e){
			e.printStackTrace();
		}
	}
	
	public byte[] decryptUsername(byte[] username){
		return decrypt(username);
	}
	
	public byte[] decryptPassword(byte[] password){
		return decrypt(password);
	}
	
	/**
	 * decrypt encrypted username and password
	 * @param data
	 * @return
	 */
	private byte[] decrypt(byte[] data){
		try{
			FileReader fr = new FileReader(privateFile);
			BufferedReader br = new BufferedReader(fr);
			int privateExpLength = Integer.parseInt(br.readLine());
			String privBuf = br.readLine();
			//exponent could contain lf
			while(privBuf.length()<privateExpLength){
				privBuf=privBuf+ br.readLine();
			}
			BigInteger privateExp = new BigInteger(privBuf);
			BigInteger modulus = new BigInteger(br.readLine());
			
			RSAPrivateKeySpec privSpec = new RSAPrivateKeySpec(modulus, privateExp);
			PrivateKey testPriv = KeyFactory.getInstance("RSA").generatePrivate(privSpec);
			
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, testPriv);
			return cipher.doFinal(data);
		}
		catch (IOException io){
			io.printStackTrace();
		}
		catch (NoSuchPaddingException p){
			p.printStackTrace();
		}
		catch (InvalidKeyException i){
			i.printStackTrace();
		}
		catch (IllegalBlockSizeException e){
			e.printStackTrace();
		}
		catch (BadPaddingException e){
			e.printStackTrace();
		}		
		catch (InvalidKeySpecException e){
			e.printStackTrace();
		}
		catch (NoSuchAlgorithmException e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * method to start key creation (algorithm used: RSA):
	 * private key - consisting of private exponent and modulus n
	 * public key - consisting of public exponent and modulus n
	 * @param args
	 */
	public static void main(String[] args){
		KeyGeneration generator= new KeyGeneration();
		System.out.println("Sind Sie sich sicher, dass Sie neue Schl\u00FCssel" +
				" zur Authentifizierung generieren wollen? Die alten Schl\u00FCssel" +
				" werden dann \u00FCberschrieben. Y/N");
		Scanner scanner = new Scanner(System.in);
		String type = scanner.nextLine();
		while(!(type.equals("Y") || type.equals("N"))){
			System.out.println("Bitte tippen Sie Y oder N");
			type = scanner.nextLine();
		}
		if(type.equals("N")) System.exit(0);
		else generator.generateKeys();
	}
	
}
