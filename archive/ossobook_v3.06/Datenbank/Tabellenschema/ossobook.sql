# HeidiSQL Dump 
#
# --------------------------------------------------------
# Host:                 10.0.142.4
# Database:             ossobook
# Server version:       5.0.27-community-nt
# Server OS:            Win32
# Target-Compatibility: MySQL 5.0
# max_allowed_packet:   1048576
# HeidiSQL version:     3.1 Revision: 1124
# --------------------------------------------------------

/*!40100 SET CHARACTER SET latin1*/;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0*/;


#
# Database structure for database 'ossobook'
#

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `ossobook` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ossobook`;


#
# Table structure for table 'alter1'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `alter1` (
  `Alter1Code` int(11) NOT NULL default '0',
  `Alter1Name` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`Alter1Code`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'alter2'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `alter2` (
  `Alter2Code` int(11) NOT NULL default '0',
  `Alter2Name` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`Alter2Code`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'alter3'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `alter3` (
  `Alter3Code` int(11) NOT NULL default '0',
  `Alter3Name` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`Alter3Code`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'alter4'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `alter4` (
  `Alter4Code` int(11) NOT NULL default '0',
  `Alter4Name` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`Alter4Code`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'alter5'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `alter5` (
  `Alter5Code` int(11) NOT NULL default '0',
  `Alter5Name` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) character set latin1 collate latin1_german1_ci NOT NULL default '0',
  `geloescht` enum('Y','N') character set latin1 collate latin1_german1_ci NOT NULL default 'N',
  PRIMARY KEY  (`Alter5Code`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'artefaktmaske'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `artefaktmaske` (
  `ID` int(100) NOT NULL auto_increment,
  `Datenbanknummer` int(11) NOT NULL default '0',
  `typ` varchar(100) NOT NULL default '',
  `untertyp` varchar(100) NOT NULL default '',
  `verzierung` varchar(100) NOT NULL default '',
  `herstellungsspur` varchar(100) NOT NULL default '',
  `gebrauchsspur` varchar(100) NOT NULL default '',
  `basis` varchar(100) NOT NULL default '',
  `spitze` varchar(100) NOT NULL default '',
  `schaeftung` varchar(100) NOT NULL default '',
  `spitzenform` varchar(100) NOT NULL default '',
  `spitzenquerschnitt` varchar(100) NOT NULL default '',
  `meisselform` varchar(100) NOT NULL default '',
  `meissellaengsschnitt` varchar(100) NOT NULL default '',
  `fragmentierung` varchar(100) NOT NULL default '',
  `faerbung` varchar(100) NOT NULL default '',
  `inschrift` varchar(100) NOT NULL default '',
  `m1` varchar(100) NOT NULL default '',
  `m2` varchar(100) NOT NULL default '',
  `m3` varchar(100) NOT NULL default '',
  `m4` varchar(100) NOT NULL default '',
  `m5` varchar(100) NOT NULL default '',
  `diverseNotizen` text NOT NULL,
  `eisenteile` varchar(100) NOT NULL default '',
  `bronzeteile` varchar(100) NOT NULL default '',
  `einlagen` varchar(100) NOT NULL default '',
  `kompositObjekt` varchar(100) NOT NULL default '',
  `ueberschliffenerBruch` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `Nachrichtennummer` int(11) NOT NULL default '-1',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`ID`,`Datenbanknummer`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'benutzer'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `benutzer` (
  `BenutzerName` varchar(100) NOT NULL default '',
  `PIN` int(3) NOT NULL auto_increment,
  `Mail` varchar(50) character set latin1 collate latin1_german1_ci NOT NULL,
  `Admin` enum('Y','N') character set latin1 collate latin1_german1_ci NOT NULL default 'N',
  `Zustand` varchar(50) character set latin1 collate latin1_german1_ci NOT NULL default '0',
  `geloescht` enum('Y','N') character set latin1 collate latin1_german1_ci NOT NULL default 'N',
  PRIMARY KEY  (`BenutzerName`),
  UNIQUE KEY `PIN` (`PIN`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'brandspur'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `brandspur` (
  `BrandspurCode` int(11) NOT NULL default '0',
  `BrandspurName` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`BrandspurCode`,`BrandspurName`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'bruchkante'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `bruchkante` (
  `BruchkanteCode` int(11) NOT NULL default '0',
  `BruchkanteName` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`BruchkanteCode`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'bruchkante2'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `bruchkante2` (
  `Bruchkante2Code` int(11) NOT NULL default '0',
  `Bruchkante2Name` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`Bruchkante2Code`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'eingabeeinheit'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `eingabeeinheit` (
  `ID` int(100) NOT NULL auto_increment,
  `Datenbanknummer` int(11) NOT NULL default '0',
  `projNr` int(6) default '0',
  `DBNummerProjekt` int(11) NOT NULL default '0',
  `fK` varchar(200) default '',
  `phase1` varchar(100) default '',
  `phase2` varchar(100) default '',
  `phase3` varchar(100) default '',
  `phase4` varchar(100) default '',
  `BenutzerName` varchar(100) default NULL,
  `inventarNr` varchar(100) default '',
  `xAchse` varchar(100) default '0',
  `yAchse` varchar(100) default '0',
  `tierart` int(6) default '-1',
  `skelettteil` int(6) default '-1',
  `knochenteil` varchar(100) default '',
  `pathologie` varchar(100) default '',
  `alter1` varchar(100) default '',
  `alter2` varchar(100) default '',
  `alter3` varchar(100) default '',
  `alter4` varchar(100) default '',
  `alter5` varchar(100) default '',
  `bruchkante` varchar(100) default '',
  `bruchkante2` varchar(100) default '',
  `erhaltung` varchar(100) default '',
  `wurzelfrass` varchar(100) default '',
  `kruste` varchar(100) default '',
  `fettig` varchar(100) default '',
  `patina` varchar(100) default '',
  `brandspur` varchar(100) default '',
  `verbiss` varchar(100) default '',
  `verdaut` varchar(100) default '',
  `schlachtS1` varchar(100) default '',
  `schlachtS2` varchar(100) default '',
  `feuchtboden` varchar(100) default '',
  `geschlecht` varchar(100) default '',
  `koerperseite` enum('r','l','indet') default 'indet',
  `artefaktID` int(11) default '-1',
  `DBNummerArtefakt` int(11) NOT NULL default '-1',
  `gewicht` float default NULL,
  `massID` int(11) default '-1',
  `DBNummerMasse` int(11) NOT NULL default '-1',
  `massNotiz` text,
  `objektNotiz` text,
  `skelNr` varchar(100) default '',
  `knIndNr` varchar(100) default '',
  `groesse1` varchar(100) default '',
  `groesse2` varchar(100) default '',
  `saison` varchar(100) default '',
  `jahrring` varchar(100) default '',
  `zusatzFeld1` varchar(100) default '',
  `zusatzFeld2` varchar(100) default '',
  `zusatzFeld3` varchar(100) default '',
  `zusatzFeld4` varchar(100) default '',
  `anzahl` int(11) default '1',
  `indivnrsicher` varchar(100) default NULL,
  `Zustand` varchar(50) NOT NULL default '0',
  `Nachrichtennummer` int(11) NOT NULL default '-1',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`ID`,`Datenbanknummer`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'erhaltung'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `erhaltung` (
  `ErhaltungCode` int(11) NOT NULL default '0',
  `ErhaltungName` varchar(255) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`ErhaltungCode`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'fett'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `fett` (
  `FettCode` int(11) NOT NULL default '0',
  `FettName` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`FettCode`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'geschlecht'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `geschlecht` (
  `GeschlechtCode` int(11) NOT NULL default '0',
  `GeschlechtName` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`GeschlechtCode`,`GeschlechtName`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'knochenteil'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `knochenteil` (
  `SkelCode` int(11) NOT NULL default '0',
  `KnochenTeilCode` int(11) NOT NULL default '0',
  `KnochenTeilName` varchar(100) default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`SkelCode`,`KnochenTeilCode`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'massabhaengigkeit'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `massabhaengigkeit` (
  `TierLB` int(11) NOT NULL default '0',
  `SkelLB` int(11) NOT NULL default '0',
  `Masse` varchar(300) NOT NULL,
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`TierLB`,`SkelLB`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'massdefinition'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `massdefinition` (
  `massName` varchar(100) NOT NULL default '',
  `massBeschreibung` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`massName`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'masse'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `masse` (
  `MassID` int(10) NOT NULL auto_increment,
  `Datenbanknummer` int(11) NOT NULL default '0',
  `mass1` float default '0',
  `mass2` float default '0',
  `mass3` float default '0',
  `mass4` float default '0',
  `mass5` float default '0',
  `mass6` float default '0',
  `mass7` float default '0',
  `mass8` float default '0',
  `mass9` float default '0',
  `mass10` float default '0',
  `mass11` float default '0',
  `mass12` float default '0',
  `mass13` float default '0',
  `mass14` float default '0',
  `mass15` float default '0',
  `mass16` float default '0',
  `mass17` float default '0',
  `mass18` float default '0',
  `mass19` float default '0',
  `mass20` float default '0',
  `mass21` float default '0',
  `mass22` float default '0',
  `mass23` float default '0',
  `mass24` float default '0',
  `mass25` float default '0',
  `mass26` float default '0',
  `mass27` float default '0',
  `mass28` float default '0',
  `mass29` float default '0',
  `mass30` float default '0',
  `mass31` float default '0',
  `mass32` float default '0',
  `mass33` float default '0',
  `mass34` float default '0',
  `mass35` float default '0',
  `mass36` float default '0',
  `mass37` float default '0',
  `mass38` float default '0',
  `mass39` float default '0',
  `mass40` float default '0',
  `mass41` float default '0',
  `mass42` float default '0',
  `mass43` float default '0',
  `mass44` float default '0',
  `mass45` float default '0',
  `mass46` float default '0',
  `mass47` float default '0',
  `mass48` float default '0',
  `mass49` float default '0',
  `mass50` float default '0',
  `mass51` float default '0',
  `mass52` float default '0',
  `mass53` float default '0',
  `mass54` float default '0',
  `mass55` float default '0',
  `mass56` float default '0',
  `mass57` float default '0',
  `mass58` float default '0',
  `mass59` float default '0',
  `mass60` float default '0',
  `mass61` float default '0',
  `mass62` float default '0',
  `mass63` float default '0',
  `mass64` float default '0',
  `mass65` float default '0',
  `mass66` float default '0',
  `mass67` float default '0',
  `mass68` float default '0',
  `mass69` float default '0',
  `mass70` float default '0',
  `mass71` float default '0',
  `mass72` float default '0',
  `mass73` float default '0',
  `mass74` float default '0',
  `mass75` float default '0',
  `mass76` float default '0',
  `mass77` float default '0',
  `mass78` float default '0',
  `mass79` float default '0',
  `mass80` float default '0',
  `mass81` float default '0',
  `mass82` float default '0',
  `mass83` float default '0',
  `mass84` float default '0',
  `mass85` float default '0',
  `mass86` float default '0',
  `mass87` float default '0',
  `mass88` float default '0',
  `mass89` float default '0',
  `mass90` float default '0',
  `mass91` float default '0',
  `mass92` float default '0',
  `mass93` float default '0',
  `mass94` float default '0',
  `mass95` float default '0',
  `mass96` float default '0',
  `mass97` float default '0',
  `mass98` float default '0',
  `mass99` float default '0',
  `mass100` float default '0',
  `mass101` float default '0',
  `mass102` float default '0',
  `mass103` float default '0',
  `mass104` float default '0',
  `mass105` float default '0',
  `mass106` float default '0',
  `mass107` float default '0',
  `mass108` float default '0',
  `mass109` float default '0',
  `mass110` float default '0',
  `mass111` float default '0',
  `mass112` float default '0',
  `mass113` float default '0',
  `mass114` float default '0',
  `mass115` float default '0',
  `mass116` float default '0',
  `mass117` float default '0',
  `mass118` float default '0',
  `mass119` float default '0',
  `mass120` float default '0',
  `mass121` float default '0',
  `mass122` float default '0',
  `mass123` float default '0',
  `mass124` float default '0',
  `mass125` float default '0',
  `mass126` float default '0',
  `mass127` float default '0',
  `mass128` float default '0',
  `mass129` float default '0',
  `mass130` float default '0',
  `mass131` float default '0',
  `mass132` float default '0',
  `mass133` float default '0',
  `mass134` float default '0',
  `mass135` float default '0',
  `mass136` float default '0',
  `mass137` float default '0',
  `mass138` float default '0',
  `mass139` float default '0',
  `mass140` float default '0',
  `mass141` float default '0',
  `mass142` float default '0',
  `mass143` float default '0',
  `mass144` float default '0',
  `mass145` float default '0',
  `mass146` float default '0',
  `mass147` float default '0',
  `mass148` float default '0',
  `mass149` float default '0',
  `mass150` float default '0',
  `mass151` float default '0',
  `mass152` text character set latin1 collate latin1_german1_ci,
  `Zustand` varchar(50) character set latin1 collate latin1_german1_ci NOT NULL default '0',
  `Nachrichtennummer` int(11) NOT NULL default '-1',
  `geloescht` enum('Y','N') character set latin1 collate latin1_german1_ci NOT NULL default 'N',
  PRIMARY KEY  (`MassID`,`Datenbanknummer`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'nachrichten'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `nachrichten` (
  `Datenbanknummer` int(11) NOT NULL,
  `ProjNr` int(5) NOT NULL,
  `DBNummerProjekt` int(11) NOT NULL,
  `Nachrichtennummer` int(11) NOT NULL,
  PRIMARY KEY  (`Datenbanknummer`,`DBNummerProjekt`,`ProjNr`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'patina'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `patina` (
  `PatinaCode` int(11) NOT NULL default '0',
  `PatinaName` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`PatinaCode`,`PatinaName`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'projekt'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `projekt` (
  `ProjNr` int(5) NOT NULL auto_increment,
  `Datenbanknummer` int(11) NOT NULL default '0',
  `ProjName` varchar(150) default '',
  `ProjEigentuemer` varchar(100) NOT NULL,
  `grabungsNr` varchar(100) default '',
  `grabungsJahrVon` varchar(100) default '',
  `grabungsJahrBis` varchar(100) default '',
  `grabungsOrt` varchar(100) default '',
  `projektNotiz` text,
  `befundName` varchar(100) default '',
  `phasenBezeichnung` varchar(100) default '',
  `schicht` varchar(100) default '',
  `flaeche` varchar(100) default '',
  `feld` varchar(100) default '',
  `befundNotiz` text,
  `vonDatum` varchar(100) default '',
  `bisDatum` varchar(100) default '',
  `notizDatum` varchar(100) default '0',
  `zuletztSynchronisiert` varchar(50) default NULL,
  `Zustand` varchar(50) NOT NULL default '0',
  `Nachrichtennummer` int(11) NOT NULL default '-1',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`ProjNr`,`Datenbanknummer`),
  UNIQUE KEY `ProjName` (`ProjName`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'projektrechte'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `projektrechte` (
  `Benutzer` varchar(100) NOT NULL,
  `ProjNr` int(10) NOT NULL,
  `DBNummerProjekt` int(11) NOT NULL,
  `Recht` enum('lesen','schreiben') NOT NULL,
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`Benutzer`,`ProjNr`,`DBNummerProjekt`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'schlachtspur1'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `schlachtspur1` (
  `Schlachtspur1Code` int(11) NOT NULL default '0',
  `Schlachtspur1Name` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`Schlachtspur1Code`,`Schlachtspur1Name`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'schlachtspur2'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `schlachtspur2` (
  `Schlachtspur2Code` int(11) NOT NULL default '0',
  `Schlachtspur2Name` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`Schlachtspur2Code`,`Schlachtspur2Name`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'skelteil'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `skelteil` (
  `SkelCode` int(100) NOT NULL default '0',
  `SkelName` varchar(100) NOT NULL default '',
  `SkelLB` int(11) NOT NULL default '0',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`SkelCode`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'tierart'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `tierart` (
  `TierCode` int(11) NOT NULL default '0',
  `TierName` varchar(100) NOT NULL default '',
  `DTier` varchar(100) default '',
  `TierFolgeAuswertung` int(11) NOT NULL default '0',
  `LB` int(11) NOT NULL default '0',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`TierCode`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'verbiss'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `verbiss` (
  `VerbissCode` int(11) NOT NULL default '0',
  `VerbissName` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`VerbissCode`,`VerbissName`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'versinterung'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `versinterung` (
  `VersinterungCode` int(11) NOT NULL default '0',
  `VersinterungName` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`VersinterungCode`,`VersinterungName`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'version'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `version` (
  `ossobookversion` varchar(100) NOT NULL,
  `dbversion` varchar(100) NOT NULL,
  `Datenbanknummer` int(11) NOT NULL,
  `Nachrichtennummer` int(11) NOT NULL default '0',
  `Useranzahl` int(11) NOT NULL default '0',
  PRIMARY KEY  (`ossobookversion`,`dbversion`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;



#
# Table structure for table 'wurzelfrass'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `wurzelfrass` (
  `WurzelfrassCode` int(11) NOT NULL default '0',
  `WurzelfrassName` varchar(100) NOT NULL default '',
  `Zustand` varchar(50) NOT NULL default '0',
  `geloescht` enum('Y','N') NOT NULL default 'N',
  PRIMARY KEY  (`WurzelfrassCode`,`WurzelfrassName`)
) ENGINE=InnoDB /*!40100 DEFAULT CHARSET=latin1*/;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS*/;
