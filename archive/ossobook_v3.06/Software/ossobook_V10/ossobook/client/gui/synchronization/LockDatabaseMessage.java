package ossobook.client.gui.synchronization;

import java.util.*;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.*;
import javax.swing.*;

import ossobook.client.gui.common.*;

/**
 * informs user about locking database during synchronization
 * @author j.lamprecht
 *
 */
public class LockDatabaseMessage extends Observable implements ActionListener{

	private JInternalFrame frame;
	private JButton abort;
	private JButton ok;
	
	/**
	 * build up information window
	 * @param parent
	 */
	public LockDatabaseMessage(OssobookHauptfenster parent){
		
		frame = new JInternalFrame("Achtung!");
		frame.setClosable(false);
		
		GridBagConstraints gridBagConstraints;

        JTextArea text = new JTextArea();
        abort = new JButton();
        ok = new JButton();

        frame.setLayout(new GridBagLayout());

        text.setBackground(new Color(236, 233, 216));
        text.setColumns(30);
        text.setEditable(false);
        text.setLineWrap(true);
        text.setRows(4);
        text.setText("W\u00E4hrend der Synchronisation wird Ihre lokale Datenbank gesperrt. Sie k\u00F6nnen aber mit der zentralen Datenbank weiterarbeiten. " +
        		"\n\nBitte beenden Sie das Programm nicht und bitte fahren Sie Ihren PC nicht herunter, bevor der Synchronisationsvorgang beendet ist.");
        text.setWrapStyleWord(true);
        text.setAutoscrolls(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 20, 0);
        frame.add(text, gridBagConstraints);

        abort.setText("abbrechen");
        abort.addActionListener(this);
        frame.add(abort, new GridBagConstraints());

        ok.setText("ok");
        ok.addActionListener(this);
        frame.add(ok, new GridBagConstraints());
        
        parent.getContentPane().add(frame);
        frame.setSize(400, 400);
        frame.setVisible(true);         
	}

	/**
	 * user wants to continue or abort synchronization
	 */
	public void actionPerformed(ActionEvent e){
		setChanged();
		if(e.getSource()==abort){
			notifyObservers("abort");
		}
		else notifyObservers("ok");
	}
	
	public JInternalFrame getInternalFrame(){
		return frame;
	}
}
