package ossobook.client.gui.synchronization;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.util.*;

import ossobook.client.gui.common.*;
import ossobook.client.base.metainfo.*;

/**
 * part of the gui to manage synchronization: enter projects and press
 * button "jetzt synchronisieren" 
 * @author j.lamprecht
 *
 */
public class SynchronizeWindow extends Observable implements ActionListener, InternalFrameListener{

	private JButton synchronize;
	private JMenuItem changeFunction;
	private JButton all;
	private JButton none;
	private OssobookHauptfenster parent;
	private JInternalFrame frame;
	private JPanel checkbox;
	private String itemLabel;
	private String buttonLabel;
	private HashMap<String, Projekt> projects;
	private JPanel progress;
	private JTextArea description;
	
	/**
	 * build up SynchronizeWindow - default function: synchronization
	 * @param parent
	 * @param projectList
	 * @param doubleProjects
	 * @param progress
	 */
	public SynchronizeWindow(OssobookHauptfenster parent, Vector<Projekt> projectList, 
			Vector<Projekt> doubleProjects, JPanel progress) {
		this.parent = parent;	
		this.progress = progress;
		itemLabel="Initialisieren";
		buttonLabel="Synchronisieren";
		initHashMap(projectList);
		init(projectList, doubleProjects);		
	}
	
	private void initHashMap(Vector<Projekt> projectList){
		projects = new HashMap<String, Projekt>();
		for (int i=0; i<projectList.size(); i++){
			projects.put(projectList.elementAt(i).getName(), projectList.elementAt(i));
		}
	}
	
	/**
	 * builds up the synchronization window
	 * @param projectList: list of the projects which can be synchronized by the
	 * user (user has the rights to read them)
	 */
	private void init(Vector<Projekt> projectList, Vector<Projekt> doubleProjects){
					
		frame = new JInternalFrame(buttonLabel);
		frame.setClosable(true);
		frame.addInternalFrameListener(this);
		frame.setLayout(new GridBagLayout());
		GridBagConstraints gridBagConstraints = new GridBagConstraints();

		//description what the user shall do
		description = new JTextArea();
	    description.setBackground(new Color(236, 233, 216));
        description.setColumns(50);
        description.setEditable(false);
        description.setRows(1);
        description.setText("Bitte w\u00E4hlen Sie die Projekte aus, die Sie " + 
        		buttonLabel.toLowerCase(Locale.ENGLISH) + " wollen.");
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new Insets(0, 10, 30, 0);
        frame.add(description, gridBagConstraints);
        
        //frame for project selection
	    JLabel projectLabel = new JLabel();
	    projectLabel.setFont(new Font("Tahoma", 1, 12));
        projectLabel.setText("Projekt(e):");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(0, 10, 70, 20);
        frame.add(projectLabel, gridBagConstraints);
	   
	    JPanel boxPanel = new JPanel();
	    boxPanel.setLayout(new GridBagLayout());
	    
	    JScrollPane check = new JScrollPane();
	    checkbox = new JPanel();
	    checkbox.setLayout(new GridBagLayout());
	    check.setPreferredSize(new Dimension(300,150));
	    gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
	    
	    for (int i=0; i<projectList.size(); i++){
		    JCheckBox choice = new JCheckBox();
		    choice.setText(projectList.elementAt(i).getName());
	        checkbox.add(choice, gridBagConstraints);
	        if(doubleProjects.contains((Projekt) projectList.elementAt(i)))
	        		choice.setEnabled(false);
	    }
	    
	    check.setViewportView(checkbox);
	    gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = GridBagConstraints.NORTH;
        gridBagConstraints.insets = new Insets(0, 0, 10, 0);
        boxPanel.add(check, gridBagConstraints);
	        
	    all = new JButton();
	    all.setText("alle Projekte");
        boxPanel.add(all, new GridBagConstraints());
        all.addActionListener(this);
        
	    none = new JButton();
	    none.setText("kein Projekt");
        boxPanel.add(none, new GridBagConstraints());
        none.addActionListener(this);
	    
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.insets = new Insets(0, 0, 20, 0);
        frame.add(boxPanel, gridBagConstraints);
        
        //synchronization button
	    synchronize = new JButton();
	    synchronize.setText(buttonLabel);
	    synchronize.addActionListener(this);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.gridheight = GridBagConstraints.RELATIVE;
        gridBagConstraints.insets = new Insets(20, 20, 70, 0);
        frame.add(synchronize, gridBagConstraints);

        JMenuBar menu = new JMenuBar();
        JMenu change = new JMenu("Funktion \u00E4ndern");
        changeFunction = new JMenuItem(itemLabel);
        changeFunction.addActionListener(this);
        change.add(changeFunction);
        menu.add(change);
        frame.setJMenuBar(menu);
        
        frame.add(progress, gridBagConstraints);
        
        parent.getContentPane().add(frame);
        frame.setPreferredSize(new Dimension(750, 650)); 
        frame.pack();
        frame.setVisible(true);  
	}
	
	/**
	 * user pressed button button
	 */
	public void actionPerformed(ActionEvent e){
		if (e.getSource()==synchronize){
			setChanged();
			if(synchronize.getText().equals("Synchronisieren"))
				notifyObservers("synchronize");
			if(synchronize.getText().equals("Initialisieren"))
				notifyObservers("initialize");
		}
		else if (e.getSource()==all)
			changeSelectedProjects(true);
		else if (e.getSource()==none)
			changeSelectedProjects(false);
		else if(e.getSource()==changeFunction){
			if(changeFunction.getText().equals("Synchronisieren")){
				itemLabel="Initialisieren";
				buttonLabel="Synchronisieren";
			}
			else {
				itemLabel="Synchronisieren";
				buttonLabel="Initialisieren";
			}
			description.setText("Bitte w\u00E4hlen Sie die Projekte aus, die Sie " + 
		        buttonLabel.toLowerCase(Locale.ENGLISH) + " wollen.");
			synchronize.setText(buttonLabel);
			changeFunction.setText(itemLabel);
			frame.setTitle(buttonLabel);
		}
	}
	
	/**
     * enable build up synchronize window in main menu
     */
    public void internalFrameClosed(InternalFrameEvent e) {
    	parent.getHauptMenu().synchronizeStarted(false);
    	setChanged();
    	notifyObservers("closed");
    }
	
    public void internalFrameClosing(InternalFrameEvent e) {
    }

    public void internalFrameOpened(InternalFrameEvent e) {
    }

    public void internalFrameIconified(InternalFrameEvent e) {
    }

    public void internalFrameDeiconified(InternalFrameEvent e) {	
    }

    public void internalFrameActivated(InternalFrameEvent e) {
    }
 
    public void internalFrameDeactivated(InternalFrameEvent e) {
    }

    void displayMessage(String prefix, InternalFrameEvent e) {
    }
	
	/**
	 * get selected projects
	 * @return the list of projects, the User selected to be synchronized
	 */
	public Vector<Projekt> getSelectedProjects(){
		Component[] comp = checkbox.getComponents();
		Vector<Projekt> selectedProjects = new Vector<Projekt>();
		for(int i=0; i<comp.length; i++){
			if (((JCheckBox)comp[i]).isSelected()) 
				selectedProjects.add(projects.get(((JCheckBox)comp[i]).getText()));	
		}
		return selectedProjects;
	}
	
	/**
	 * select or deselect all projects
	 * @param selected: true=select all, false=deselect all
	 */
	private void changeSelectedProjects(boolean selected){
		Component[] comp = checkbox.getComponents();
		for(int i=0; i<comp.length; i++){
			((JCheckBox)comp[i]).setSelected(selected);	
		}		
	}
	
	/**
	 * enables / disables buttons of SyncronizeWindow
	 * @param syn: indicates whether synchronisation has yet been started
	 */
	public void synchronizeStarted(boolean syn){
		if(syn) frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		else frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		synchronize.setEnabled(!syn);
		all.setEnabled(!syn);
		none.setEnabled(!syn);
		Component[] comp = checkbox.getComponents();
		for(int i=0; i<comp.length; i++){
			((JCheckBox)comp[i]).setEnabled(!syn);
		}
	}
	
	public JInternalFrame getWindow(){
		return frame;
	}
}
