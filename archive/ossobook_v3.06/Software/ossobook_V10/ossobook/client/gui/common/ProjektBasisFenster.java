package ossobook.client.gui.common;
import javax.swing.JInternalFrame;

import ossobook.client.config.KonfigurationOssobook;


/**
 * The Basic Class for all Ossobook Projects
 * @author ali
 *  
 */
@SuppressWarnings("serial")
public class ProjektBasisFenster extends JInternalFrame {
	protected String projektName;
	
	public ProjektBasisFenster(String projektname) {
		this.projektName = projektname;
		settings();
	}
	/**
	 * sets up the frames Settings
	 *
	 */
	private void settings() {
		this.setSize(KonfigurationOssobook.projektbasisfenster_x, KonfigurationOssobook.projektbasisfenster_y);
		this.setTitle(projektName);
		this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		this.setResizable(true);
		this.setMaximizable(true);
		this.setIconifiable(true);
		this.setClosable(true);
		this.setVisible(true);
	}
	
	public String getProjectName(){
		return projektName;
	}
}