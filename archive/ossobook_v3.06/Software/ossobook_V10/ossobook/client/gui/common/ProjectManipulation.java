package ossobook.client.gui.common;

import java.beans.PropertyVetoException;
import javax.swing.*;

import ossobook.client.base.scheme.*;
import ossobook.client.base.*;
import ossobook.client.config.*;
import ossobook.client.gui.administration.*;
import ossobook.client.gui.update.components.window.*;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.LocalQueryManager;
import ossobook.sqlQueries.QueryManager;

/**
 * builds up project manipulation window - composed if ProjektFenster, 
 * UserAdministration window and ProjectConnectionInformation window
 * @author j.lamprecht
 *
 */
public class ProjectManipulation extends JInternalFrame{
	
	private ProjektFenster project;
	
	/**
	 * builds up ProjectManipulation window
	 * @param projName: name of the project which can be manipulated
	 * @param parentReference
	 * @param manager
	 * @param projnr: number of the project which can be manipulated
	 */
	public ProjectManipulation(String projName,OssobookHauptfenster parentReference, 
			QueryManager manager, int[] projectKey){
		
		JTabbedPane register = new JTabbedPane();
		this.setSize(KonfigurationOssobook.projektbasisfenster_x, KonfigurationOssobook.projektbasisfenster_y);
		
		//ProjektFenster
		project = new ProjektFenster(
			"Benutzer: " + manager.getUser() + " Projekt: " + projName,
			manager, new UnbestimmtSchablone(parentReference, manager.getConnection()),
			new KnochenSchablone(parentReference, new BaseFasade(manager) , projectKey),
			new ArtefaktSchablone(parentReference),
			projectKey, this, parentReference, parentReference.getDesktop());
		
		register.addTab("Datenbank\u00E4nderungen", project.getUpdateWindow());
		
		//UserAdministration
		try{
			UserAdministration admin = new UserAdministration(manager, projName);
			register.addTab("Userverwaltung", admin.getPanel());
			if(manager instanceof LocalQueryManager  || (!manager.isProjectOwner(manager.getProjectKey(projName))
					&& !manager.getIsAdmin())) {
				register.setEnabledAt(register.indexOfTab("Userverwaltung"),false);
			}
		}
		catch (StatementNotExecutedException e){}
		
		//connection information
		ProjectConnectionInformation information = new ProjectConnectionInformation(manager);
		register.addTab("Arbeitsinformationen", information.getPanel());
				
		setTitle(project.getProjectName());
		setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		setResizable(true);
		setMaximizable(true);
		setIconifiable(true);
		setClosable(true);
		setContentPane(register);
		setVisible(true);
		parentReference.getDesktop().add(this);
		try{
			setSelected(true);
		}
		catch (PropertyVetoException ef){
			ef.printStackTrace();
			JOptionPane.showMessageDialog(null, "Meldung: "
					+ ef.getStackTrace().toString() + ": " + toString());
		}
	}

	public ProjektFenster getProjectWindow(){
		return project;
	}
}
