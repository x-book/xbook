package ossobook.client.gui.common;

import java.awt.*;
import javax.swing.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;

/**
 * possibility to get user and password information from the ossobook user
 * 
 * @author j.lamprecht
 * 
 */
public class Login extends Observable implements ActionListener {
	private static Log _log = LogFactory.getLog(Login.class);

	private JInternalFrame window;
	private JButton ok;
	private JButton abbrechen;
	private JTextField user;
	private JPasswordField password;
	private String username;
	private char[] pw;
	private OssobookHauptfenster reference;

	/**
	 * builds up Login window
	 * 
	 * @param reference
	 */
	public Login(OssobookHauptfenster reference) {
		this.reference = reference;
		window = init();
		window.toFront();
	}

	/**
	 * builds up Login window
	 * 
	 * @return
	 */
	private JInternalFrame init() {

		window = new JInternalFrame("Login");

		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();

		JPanel buttonField = new JPanel();

		JPanel panel = new JPanel();
		panel.setLayout(layout);

		// getting user
		JLabel userlabel = new JLabel("Benutzername");
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridwidth = GridBagConstraints.RELATIVE;
		constraints.ipadx = 10;
		constraints.ipady = 10;
		layout.setConstraints(userlabel, constraints);
		panel.add(userlabel);

		user = new JTextField(10);
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		constraints.ipady = 0;
		layout.setConstraints(user, constraints);
		panel.add(user);

		// getting password
		JLabel passwordlabel = new JLabel("Passwort");
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridwidth = GridBagConstraints.RELATIVE;
		constraints.ipadx = 10;
		constraints.ipady = 10;
		layout.setConstraints(passwordlabel, constraints);
		panel.add(passwordlabel);

		password = new JPasswordField(10);
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		constraints.ipady = 0;
		layout.setConstraints(password, constraints);
		panel.add(password);

		// login finished
		ok = new JButton("ok");
		ok.addActionListener(this);
		buttonField.add(ok);

		// login aborted
		abbrechen = new JButton("abbrechen");
		abbrechen.addActionListener(this);
		buttonField.add(abbrechen);

		constraints.insets = new Insets(20, 0, 0, 0);
		constraints.gridheight = 3;
		layout.setConstraints(buttonField, constraints);
		panel.add(buttonField);

		window.add(panel);
		window.setSize(300, 200);
		window.setVisible(true);

		try {
			window.setSelected(true);
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}

		reference.getContentPane().add(window);
		// Select the user-inputfield immediatly.
		user.grabFocus();
		return window;
	}

	public String getUser() {
		return username;
	}

	public char[] getPassword() {
		return pw;
	}

	/**
	 * login event occured - user pressed ok button or abbrechen button
	 */
	public void actionPerformed(ActionEvent e) {
		// login finished
		if (e.getSource() == ok) {
			username = user.getText();
			pw = password.getPassword();
			setChanged();
			notifyObservers("login completed");
			if (_log.isInfoEnabled()) {
				_log.info("Login to ossobook as user: " + this.username);
			}
		}
		// login aborted
		else if (e.getSource() == abbrechen) {
			setChanged();
			notifyObservers("login aborted");
			if (_log.isInfoEnabled()) {
				_log.info("Login aborted");
			}
		}
	}

	public void close() {
		window.dispose();
	}
}
