package ossobook.client.gui.common;

import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.*;

import ossobook.client.config.KonfigurationOssobook;
import ossobook.client.controlers.GuiControler;

/**
 * @author ali
 */
public class OssobookHauptfenster extends JFrame {
		
	private JDesktopPane desktop;
	private HauptMenueBar hauptmenu;
	private GuiControler controler;
	
	public OssobookHauptfenster(GuiControler controler) {
		this.controler = controler;
		settings();
		createMenu();
	}
	
	/**
	 *  setsup the MenuBar for this Frame
	 * @see ossobook.gui.common.HauptMenueBar
	 */
	private void createMenu() {
		hauptmenu=new HauptMenueBar(this, controler);
		this.setJMenuBar(hauptmenu.getJMenuBar());
	}
	
	public HauptMenueBar getHauptMenueBar(){
		return hauptmenu;
	}
	
	
	/**
	 * @return Returns the desktop.
	 */
	public JDesktopPane getDesktop() {
		return desktop;
	}
	
	public HauptMenueBar getHauptMenu(){
		return hauptmenu;
	}
	
	/**
	 *  setsup the frames look and feel, Size and properties
	 */
	private void settings() {
		// Windows Look and Feel einstellen
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			SwingUtilities.updateComponentTreeUI(this);
		} catch (Exception e) {
		}
		this.setTitle(KonfigurationOssobook.PROGRAMNAME);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		desktop = new JDesktopPane();
		desktop.setBackground(Color.black);
		this.setContentPane(desktop);
	}
	
	public GuiControler getGuiControler(){
		return controler;
	}
}