package ossobook.client.gui.common;

import java.awt.Color;
import javax.swing.*;

import ossobook.sqlQueries.LocalQueryManager;
import ossobook.sqlQueries.QueryManager;

/**
 * informs the user which database he uses and which user name
 * @author j.lamprecht
 *
 */
public class ProjectConnectionInformation {

	private JPanel panel;
	
	/**
	 * builds up ProjectConnectionInformation window
	 * @param manager
	 */
	public ProjectConnectionInformation(QueryManager manager){
		String information = "globalen";
		
		panel = new JPanel();
		JTextArea description = new JTextArea();
		description.setEditable(false);
		description.setBackground(new Color(236, 233, 216));
	
		if(manager instanceof LocalQueryManager) information = "lokalen";
		description.setText("Sie arbeiten bei diesem Projekt " +
			"als User " + manager.getUser() +
			" auf der "+ information + " Datenbank.");
		panel.add(description);
		panel.setVisible(true);
	
	}
	
	public JPanel getPanel(){
		return panel;
	}
	
}
