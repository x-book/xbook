package ossobook.client.gui.common;

import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.io.*;
import java.util.*;

import javax.swing.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.client.base.scheme.*;
import ossobook.client.config.KonfigurationOssobook;
import ossobook.client.controlers.GuiControler;
import ossobook.client.gui.update.components.other.TabellenAnsicht;
import ossobook.client.gui.update.components.window.*;
import ossobook.client.io.CSVExport;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.QueryManager;

/**
 * The MenuBar of the Ossobook MainFrame.
 * 
 * @see ossobook.gui.common.OssobookHauptfenster
 * @author ali
 */
public class HauptMenueBar extends Observable {
	private static Log _log = LogFactory.getLog(HauptMenueBar.class);
	private JMenuBar menuBar;
	private JMenuItem about;
	private JMenuItem animalCodeTool, skelCodeTool, bruchkanteCodeTool,
			oberflaechenErhaltungCodeTool, alter1CodeTool, alter2CodeTool,
			alter3CodeTool, alter4CodeTool, alter5CodeTool,
			wurzelfrassCodeTool, versinterungCodeTool, fettCodeTool,
			patinaCodeTool, brandspurCodeTool, verbissCodeTool,
			schlachtspur1CodeTool, schlachtspur2CodeTool, geschlechtCodeTool,
			bruchkante2CodeTool, neuerTierCode;
	private JMenu datei, template, auswertung, tools, codes, hilfe;
	private JMenuItem login, openProjekt, abgebenProjekt, CSVExportAllgemein,
			CSVExport, CSVOhneMasse, SucheNach, DurchsucheAlle, synchronize;
	private OssobookHauptfenster parentReference;
	private JMenuItem templateIndet, templateKnochen, templateArtefakte;
	private JMenuItem tierartenAuswertung, skelettAuswertung;
	private JInternalFrame durchsucheAlleFrame = null;
	private String bedingung;
	private QueryManager manager;
	private GuiControler controler;

	public HauptMenueBar(OssobookHauptfenster parent, GuiControler controler) {
		this.parentReference = parent;
		this.controler = controler;
		createMenu();
		addListeners();
	}

	/**
	 * The listeners for the MenuItems are added here
	 */
	private void addListeners() {

		login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login login = new Login(parentReference);
				login.addObserver(controler);
			}
		});

		// openProjekt
		openProjekt.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
				ActionEvent.ALT_MASK));
		openProjekt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setChanged();
				notifyObservers("openProject");
			}
		});

		abgebenProjekt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});

		CSVExportAllgemein.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String felder = JOptionPane.showInputDialog(
						"Bitte geben sie die betroffenen Feldnamen ein: ", "*");
				String where = JOptionPane.showInputDialog(
						"Bitte geben sie eine Bedingung ein: ", "1");
				if (where.equals("")) {
					where = "1";
				}
				csvexportAllgemein(felder, where);
			}
		});

		CSVExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				csvexport();

			}
		});

		CSVOhneMasse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				csvexportKeineMasse();

			}
		});

		SucheNach.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				suchenNach();

			}
		});
		DurchsucheAlle.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F,
				ActionEvent.ALT_MASK));
		DurchsucheAlle.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				suchenNach("1");

			}
		});

		synchronize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setChanged();
				notifyObservers("synchronize");
			}
		});

		// BEARBEITE
		// Template Indet
		templateIndet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				zeigeTemplateIndet();

			}
		});
		// Template Knochen
		templateKnochen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K,
				ActionEvent.ALT_MASK));
		templateKnochen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				zeigeTemplateKnochen();

			}
		});
		// Template Artefakte
		templateArtefakte.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,
				ActionEvent.ALT_MASK));
		templateArtefakte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				zeigeTemplateArtefakt();

			}
		});
		// AUSWERTUNG
		tierartenAuswertung.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				macheTierartenAuswertung();
			}
		});
		skelettAuswertung.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				macheSkelettAuswertung();
			}
		});
		// TOOLS
		animalCodeTool.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2,
				ActionEvent.ALT_MASK));
		animalCodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachTierart();
			}
		});
		skelCodeTool.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3,
				ActionEvent.ALT_MASK));
		skelCodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachSkelTeil();
			}
		});
		bruchkanteCodeTool.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_F4, ActionEvent.ALT_MASK));
		bruchkanteCodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachBruchkante();
			}

		});
		oberflaechenErhaltungCodeTool.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_F5, ActionEvent.ALT_MASK));
		oberflaechenErhaltungCodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachOberflaechenErhaltung();
			}

		});

		alter1CodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachAlter1();
			}

		});

		alter2CodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachAlter2();
			}

		});
		alter3CodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachAlter3();
			}

		});
		alter4CodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachAlter4();
			}

		});
		alter5CodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachAlter5();
			}

		});
		wurzelfrassCodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachWurzelfrass();
			}

		});
		versinterungCodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachVersinterung();
			}

		});
		fettCodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachFettig();
			}

		});
		patinaCodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachPatina();
			}

		});
		brandspurCodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachBrandspur();
			}

		});
		verbissCodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachVerbiss();
			}

		});
		schlachtspur1CodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachSchlachtspur1();
			}

		});
		schlachtspur2CodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachSchlachtspur2();
			}

		});
		geschlechtCodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachGeschlecht();
			}

		});
		bruchkante2CodeTool.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNachBruchkante2();
			}

		});

		neuerTierCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setChanged();
				notifyObservers("animalCode");
			}
		});

		// HELP
		about.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1,
				ActionEvent.ALT_MASK));
		about.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				zeigeAboutPopup();

			}
		});
	}

	public void neuenTierCodeHinzufuegen() {
		/*
		 * ProjektFenster p = (ProjektFenster) (parentReference.getDesktop()
		 * .getSelectedFrame());
		 */
		parentReference.getDesktop().add(new NeuerTierCodeFenster(manager));
	}

	/**
	 * The Menu creation routine. Setting up the MenuItems and putting them
	 * together.
	 */
	private void createMenu() {

		menuBar = new JMenuBar();

		// -------------------------------
		datei = new JMenu("Datei");// MENU
		login = new JMenuItem("Login");
		openProjekt = new JMenuItem("Projekt \u00F6ffnen");

		abgebenProjekt = new JMenuItem("Projekt Abgeben");
		// abgebenProjekt.setEnabled(false);
		CSVExportAllgemein = new JMenuItem("CSV Export Allgemein");
		CSVExport = new JMenuItem("CSV Export");
		CSVOhneMasse = new JMenuItem("CSV Export ohne Masse");
		SucheNach = new JMenuItem("Suche Nach Knochen - Datensatz");
		DurchsucheAlle = new JMenuItem("Alle Datens\u00E4tze durchsuchen");
		synchronize = new JMenuItem("Datenbanken synchronisieren");
		// -------------------------------
		template = new JMenu("Template");// MENU
		templateIndet = new JMenuItem("Template Indet");
		// templateIndet.setEnabled(false);
		templateKnochen = new JMenuItem("Template Knochen");
		templateArtefakte = new JMenuItem("Template Artefakte");
		// -------------------------------
		auswertung = new JMenu("Auswertung");// MENU
		skelettAuswertung = new JMenuItem("Skelett - Auswertung");
		// skelettAuswertung.setEnabled(true);
		tierartenAuswertung = new JMenuItem("Tierarten - Auswertung");
		// -------------------------------
		tools = new JMenu("Tools");// MENU
		// tools.setEnabled(true);
		animalCodeTool = new JMenuItem("TierCode nachschlagen");
		skelCodeTool = new JMenuItem("SkelettCode nachschlagen");
		bruchkanteCodeTool = new JMenuItem("BruchkanteCode nachschlagen");
		oberflaechenErhaltungCodeTool = new JMenuItem(
				"Oberfl\u00E4chenerhaltungCode nachschlagen");
		alter1CodeTool = new JMenuItem("Alter1Code nachschlagen");
		alter2CodeTool = new JMenuItem("Alter2Code nachschlagen");
		alter3CodeTool = new JMenuItem("Alter3Code nachschlagen");
		alter4CodeTool = new JMenuItem("Alter4Code nachschlagen");
		alter5CodeTool = new JMenuItem("Alter5Code nachschlagen");
		wurzelfrassCodeTool = new JMenuItem("WurzelfrassCode nachschlagen");
		versinterungCodeTool = new JMenuItem("VersinterungCode nachschlagen");
		fettCodeTool = new JMenuItem("FettigCode nachschlagen");
		patinaCodeTool = new JMenuItem("Verf\u00E4rbung/PatinaCode nachschlagen");
		brandspurCodeTool = new JMenuItem("BrandspurCode nachschlagen");
		verbissCodeTool = new JMenuItem("VerbissCode nachschlagen");
		schlachtspur1CodeTool = new JMenuItem("Schlachtspur1Code nachschlagen");
		schlachtspur2CodeTool = new JMenuItem("Schlachtspur2Code nachschlagen");
		geschlechtCodeTool = new JMenuItem("GeschlechtCode nachschlagen");
		bruchkante2CodeTool = new JMenuItem("Bruchkante2Code nachschlagen");

		// -------------------------------

		codes = new JMenu("Codes");
		neuerTierCode = new JMenuItem("Neuer Tiercode");

		hilfe = new JMenu("Hilfe");// MENU
		about = new JMenuItem("About..");
		// MONTAGE
		datei.add(login);
		datei.add(openProjekt);
		datei.add(abgebenProjekt);
		datei.add(CSVExport);
		datei.add(CSVOhneMasse);
		datei.add(CSVExportAllgemein);
		datei.add(SucheNach);
		datei.add(DurchsucheAlle);
		datei.add(synchronize);
		template.add(templateIndet);
		template.add(templateKnochen);
		template.add(templateArtefakte);
		auswertung.add(skelettAuswertung);
		auswertung.add(tierartenAuswertung);
		tools.add(animalCodeTool);
		tools.add(skelCodeTool);
		tools.add(alter1CodeTool);
		tools.add(alter2CodeTool);
		tools.add(alter3CodeTool);
		tools.add(alter4CodeTool);
		tools.add(alter5CodeTool);
		tools.add(oberflaechenErhaltungCodeTool);
		tools.add(wurzelfrassCodeTool);
		tools.add(versinterungCodeTool);
		tools.add(fettCodeTool);
		tools.add(patinaCodeTool);
		tools.add(brandspurCodeTool);
		tools.add(verbissCodeTool);
		tools.add(schlachtspur1CodeTool);
		tools.add(schlachtspur2CodeTool);
		tools.add(geschlechtCodeTool);
		tools.add(bruchkanteCodeTool);
		tools.add(bruchkante2CodeTool);
		codes.add(neuerTierCode);
		hilfe.add(about);
		// -------------------------------
		menuBar.add(datei);
		menuBar.add(template);
		menuBar.add(auswertung);
		menuBar.add(tools);
		menuBar.add(codes);
		menuBar.add(hilfe);
		synchronize.setEnabled(false);
		setEnabled(false);
		codes.setEnabled(false);
	}

	/**
	 * enables / disables parts of the menu
	 * 
	 * @param enabled
	 */
	public void setEnabled(boolean enabled) {
		if (!GuiControler.getLocalFile().exists()) {
			openProjekt.setEnabled(enabled);
			abgebenProjekt.setEnabled(enabled);
			CSVExportAllgemein.setEnabled(enabled);
			CSVExport.setEnabled(enabled);
			CSVOhneMasse.setEnabled(enabled);
			SucheNach.setEnabled(enabled);
			DurchsucheAlle.setEnabled(enabled);
			template.setEnabled(enabled);
			auswertung.setEnabled(enabled);
			tools.setEnabled(enabled);
		}

	}

	public void loginCompleted(boolean isAdmin) {
		login.setEnabled(false);
		if (GuiControler.getLocalFile().exists())
			synchronize.setEnabled(true);
		if (isAdmin)
			codes.setEnabled(true);
	}

	public void synchronizeStarted(boolean syn) {
		synchronize.setEnabled(!syn);
	}

	/**
	 * 
	 */
	private void csvexport() {
		// W\u00E4hle File aus
		File f = null;
		try {
			JFileChooser fc = new JFileChooser();
			parentReference.getDesktop().add(fc);
			int returnVal = fc.showSaveDialog(CSVExport);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				f = new File(fc.getSelectedFile().getAbsolutePath());
			}
		} catch (Exception exception) {
		}
		try {
			ProjectManipulation window = (ProjectManipulation) (parentReference
					.getDesktop().getSelectedFrame());
			ProjektFenster p = window.getProjectWindow();
			CSVExport export = new CSVExport(manager.getConnection());
			int[] projectKey = p.getProjectKey();
			String query = "SELECT * "// eingabeeinheit.ID,fK,inventarNr,xAchse,yAchse,TierName,SkelName,gewicht
			// "
					+ "FROM eingabeeinheit,tierart, skelteil WHERE projNr="
					+ projectKey[0]
					+ " AND eingabeeinheit.DBNummerProjekt="
					+ projectKey[1]
					+ " AND tierart.TierCode=eingabeeinheit.tierart AND skelteil.skelCode=eingabeeinheit.skelettteil "
					+ "AND eingabeeinheit.geloescht='N' ORDER BY eingabeeinheit.ID;";
			if (_log.isDebugEnabled()) {
				_log.debug("Query: " + query);
			}
			export.export(query, f, this.parentReference);
		} catch (Exception fexc) {
			JOptionPane.showMessageDialog(parentReference.getDesktop(),
					"W\u00E4hlen sie das betroffene Projekt.");
		}
		try {
			f = new File(f.getAbsolutePath() + "_Masse.txt");
			ProjectManipulation window = (ProjectManipulation) (parentReference
					.getDesktop().getSelectedFrame());
			ProjektFenster p = window.getProjectWindow();
			CSVExport export = new CSVExport(manager.getConnection());
			int[] projectKey = p.getProjectKey();
			String query = "SELECT eingabeeinheit.tierart,eingabeeinheit.skelettteil, masse.* "// eingabeeinheit.ID,fK,inventarNr,xAchse,yAchse,TierName,SkelName,gewicht
			// "
					+ "FROM eingabeeinheit,masse WHERE projNr="
					+ projectKey[0]
					+ " AND eingabeeinheit.DBNummerProjekt="
					+ projectKey[1]
					+ " AND masse.MassID=eingabeeinheit.massID AND masse.Datenbanknummer=eingabeeinheit.DBNummerMasse "
					+ "AND eingabeeinheit.geloescht='N' AND masse.geloescht='N' "
					+ "ORDER BY eingabeeinheit.ID;";
			if (_log.isDebugEnabled()) {
				_log.debug("Query: " + query);
			}
			export.export(query, f, this.parentReference);
		} catch (Exception fexc) {
			JOptionPane.showMessageDialog(parentReference.getDesktop(),
					"W\u00E4hlen sie das betroffene Projekt.");
		}

	}

	/**
	 * 
	 */
	private void csvexportKeineMasse() {
		// W\u00E4hle File aus
		File f = null;
		try {
			JFileChooser fc = new JFileChooser();
			parentReference.getDesktop().add(fc);
			int returnVal = fc.showSaveDialog(CSVExport);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				f = new File(fc.getSelectedFile().getAbsolutePath());
			}
		} catch (Exception exception) {
		}
		try {
			ProjectManipulation window = (ProjectManipulation) (parentReference
					.getDesktop().getSelectedFrame());
			ProjektFenster p = window.getProjectWindow();
			CSVExport export = new CSVExport(manager.getConnection());
			int[] projectKey = p.getProjectKey();
			String query = "SELECT * "// eingabeeinheit.ID,fK,inventarNr,xAchse,yAchse,TierName,SkelName,gewicht
			// "
					+ "FROM eingabeeinheit,tierart, skelteil WHERE projNr="
					+ projectKey[0]
					+ " AND eingabeeinheit.DBNummerProjekt="
					+ projectKey[1]
					+ " AND tierart.TierCode=eingabeeinheit.tierart AND skelteil.skelCode=eingabeeinheit.skelettteil "
					+ "AND eingabeeinheit.geloescht='N' ORDER BY eingabeeinheit.ID;";
			if (_log.isDebugEnabled()) {
				_log.debug("Query: " + query);
			}
			export.export(query, f, this.parentReference);
		} catch (Exception fexc) {
			JOptionPane.showMessageDialog(parentReference.getDesktop(),
					"W\u00E4hlen sie das betroffene Projekt.");
		}
	}

	/**
	 * 
	 */
	private void csvexportAllgemein(String felder, String whereclause) {
		// W\u00E4hle File aus
		File f = null;
		try {
			JFileChooser fc = new JFileChooser();
			parentReference.getDesktop().add(fc);
			int returnVal = fc.showSaveDialog(CSVExport);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				f = new File(fc.getSelectedFile().getAbsolutePath());
			}
		} catch (Exception exception) {
		}
		try {
			ProjectManipulation window = (ProjectManipulation) (parentReference
					.getDesktop().getSelectedFrame());
			ProjektFenster p = window.getProjectWindow();
			CSVExport export = new CSVExport(manager.getConnection());
			int[] projectKey = p.getProjectKey();
			String query = "SELECT "
					+ felder// eingabeeinheit.ID,fK,inventarNr,xAchse,yAchse,TierName,SkelName,gewicht
					// "
					+ " FROM eingabeeinheit,tierart, skelteil WHERE projNr="
					+ projectKey[0]
					+ " AND eingabeeinheit.DBNummerProjekt="
					+ projectKey[1]
					+ " AND tierart.TierCode=eingabeeinheit.tierart AND skelteil.skelCode=eingabeeinheit.skelettteil AND "
					+ whereclause
					+ " AND eingabeeinheit.geloescht='N' ORDER BY eingabeeinheit.ID;";
			if (_log.isDebugEnabled()) {
				_log.debug("Query: " + query);
			}
			export.export(query, f, this.parentReference);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(parentReference.getDesktop(),
					"Syntax-Fehler");
		}
	}

	/**
	 * @param p
	 */
	private void macheSkelettAuswertung() {
		// W\u00E4hle File aus
		File f = null;
		try {
			JFileChooser fc = new JFileChooser();
			parentReference.getDesktop().add(fc);
			int returnVal = fc.showSaveDialog(CSVExport);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				f = new File(fc.getSelectedFile().getAbsolutePath());
			}
		} catch (Exception exception) {
		}
		try {
			ProjectManipulation window = (ProjectManipulation) (parentReference
					.getDesktop().getSelectedFrame());
			ProjektFenster p = window.getProjectWindow();
			CSVExport export = new CSVExport(manager.getConnection());
			int tiercode = Integer.parseInt(JOptionPane.showInputDialog(
					"Bitte geben sie den Tiercode ein.", "26"));
			String gruppen = JOptionPane.showInputDialog(
					"Bitte geben sie die Auswertungspriorit\u00E4t an:",
					"fK,phase1,phase2,phase3,phase4");
			export.exportSkelettListe(f, p.getProjectKey(), tiercode, gruppen);
		} catch (Exception fexc) {
			JOptionPane.showMessageDialog(parentReference.getDesktop(),
					"W\u00E4hlen sie das betroffene Projekt.");
		}
	}

	/**
	 * @param p
	 */
	private void macheTierartenAuswertung() {
		// W\u00E4hle File aus
		File f = null;
		try {
			JFileChooser fc = new JFileChooser();
			parentReference.getDesktop().add(fc);
			int returnVal = fc.showSaveDialog(CSVExport);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				f = new File(fc.getSelectedFile().getAbsolutePath());
			}
		} catch (Exception exception) {
		}
		try {
			ProjectManipulation window = (ProjectManipulation) (parentReference
					.getDesktop().getSelectedFrame());
			ProjektFenster p = window.getProjectWindow();
			CSVExport export = new CSVExport(manager.getConnection());

			export.exportTierartenListe(f, p.getProjectKey());
		} catch (Exception fexc) {
			JOptionPane.showMessageDialog(parentReference.getDesktop(),
					"W\u00E4hlen sie das betroffene Projekt.");
		}
	}

	/**
	 * 
	 */
	public void openproject() {
		/*
		 * lese File defaultUser.txt String user = ""; String passwd = ""; try {
		 * File f = new File("defaultUser.txt"); FileReader fr = new
		 * FileReader(f); BufferedReader br = new BufferedReader(fr); user =
		 * br.readLine(); passwd = br.readLine(); br.close(); fr.close(); }
		 * catch (Exception e) { } // falls keine Informationen vorhanden sind
		 * oder Anmeldung erw\u00FCnscht ist int i =
		 * JOptionPane.showConfirmDialog(null, "Defaultuser anmelden?"); if
		 * (user.equals("") || passwd.equals("") || !(i ==
		 * JOptionPane.OK_OPTION)) { user =
		 * JOptionPane.showInputDialog(parentReference.getDesktop(), "Geben Sie
		 * ihren Benutzernamen ein:"); passwd =
		 * JOptionPane.showInputDialog(parentReference.getDesktop(), "Geben Sie
		 * das Passwort f\u00FCr " + user + " ein:"); } DbVerbindung dcon = new
		 * DbVerbindung(user, passwd); Connection c = null;
		 */

		// Zeige vorhandenen Projekte an, gib m\u00F6glichkeit zur Projekter\u00F6ffnung
		ProjektAuswahlFenster projwahl = new ProjektAuswahlFenster(
				parentReference, manager);
		parentReference.getDesktop().add(projwahl);
		try {
			projwahl.setSelected(true);
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		projwahl.grabFocus();
	}

	/**
	 * 
	 */
	private void suchenNach() {
		suchenNach("");
	}

	private void suchenNach(String bedingung) {
		this.bedingung = bedingung;
		TabellenAnsicht result = produziereDurchsucheAlleTabelle(this.bedingung);

		if (result != null) {
			durchsucheAlleFrame = new JInternalFrame();
			result.setParentContainer(durchsucheAlleFrame);
			durchsucheAlleFrame.setSize(
					KonfigurationOssobook.durchsuchealleframe_x,
					KonfigurationOssobook.durchsuchealleframe_y);
			durchsucheAlleFrame.setClosable(true);
			durchsucheAlleFrame.setResizable(true);
			JScrollPane sp = new JScrollPane(result);
			durchsucheAlleFrame.getContentPane().add(sp);
			durchsucheAlleFrame.setVisible(true);
			parentReference.getDesktop().add(durchsucheAlleFrame);
		}
		try {
			durchsucheAlleFrame.setSelected(true);
		} catch (PropertyVetoException e1) {

		}
	}

	public void updateDurchsucheAlleFrame() {
		if (durchsucheAlleFrame.isVisible()) {
			TabellenAnsicht result = produziereDurchsucheAlleTabelle(this.bedingung);
			JScrollPane sp = new JScrollPane(result);
			durchsucheAlleFrame.getContentPane().removeAll();
			durchsucheAlleFrame.getContentPane().add(sp);
			try {
				durchsucheAlleFrame.setSelected(true);
			} catch (PropertyVetoException e) {
			}
		}
	}

	private TabellenAnsicht produziereDurchsucheAlleTabelle(String bedingung) {
		this.bedingung = bedingung;
		TabellenAnsicht result = null;
		if (this.bedingung.equals("")) {
			this.bedingung = JOptionPane.showInputDialog(
					"Bitte geben sie die Suchbedingung ein: ", "1");
		}
		try {
			ProjectManipulation window = (ProjectManipulation) (parentReference
					.getDesktop().getSelectedFrame());
			ProjektFenster p = window.getProjectWindow();
			CSVExport export = new CSVExport(manager.getConnection());
			int[] projectKey = p.getProjectKey();
			String query = "SELECT eingabeeinheit.Datenbanknummer, eingabeeinheit.ID,artefaktID,DBNummerArtefakt,massID,DBNummerMasse,fK,inventarNr,xAchse,yAchse,TierName,SkelName,knochenteil,alter1,alter2,alter3,alter4,alter5,erhaltung,wurzelfrass,kruste,fettig,patina,brandspur,verbiss,verdaut,schlachtS1,schlachtS2,feuchtboden,geschlecht,koerperseite,gewicht,pathologie,bruchkante,bruchkante2,groesse1,groesse2,saison,jahrring,skelNr,knIndNr,zusatzFeld1,zusatzFeld2,zusatzFeld3,zusatzFeld4,anzahl "
					+ "FROM eingabeeinheit,tierart,skelteil WHERE projNr="
					+ projectKey[0]
					+ " AND eingabeeinheit.DBNummerProjekt="
					+ projectKey[1]
					+ " AND tierart.TierCode=eingabeeinheit.tierart AND skelteil.skelCode=eingabeeinheit.skelettteil AND "
					+ this.bedingung
					+ " AND eingabeeinheit.geloescht='N' ORDER BY eingabeeinheit.ID DESC;";
			if (_log.isDebugEnabled()) {
				_log.debug("Query: " + query);
			}
			double time = System.currentTimeMillis();
			result = export.showResultTable(query, p,
					TabellenAnsicht.DATENSATZSUCHE);
			if (_log.isTraceEnabled()) {
				_log.trace("time for showResultTable: "
						+ (System.currentTimeMillis() - time));
			}
		} catch (Exception f) {
			JOptionPane.showMessageDialog(parentReference.getDesktop(),
					"W\u00E4hlen sie das betroffene Projekt.");
		}
		return result;
	}

	/**
	 * 
	 */
	private void suchenNachTierart() {
		TabellenAnsicht result = null;
		String query = "SELECT TierCode,TierName,DTier FROM tierart  WHERE geloescht='N' ORDER by TierName;";
		int suchmodus = TabellenAnsicht.TIERARTSUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachSkelTeil() {
		TabellenAnsicht result = null;
		String query = "SELECT SkelCode,SkelName FROM skelteil  WHERE geloescht='N' ORDER by SkelName;";
		int suchmodus = TabellenAnsicht.SKELTEILSUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachBruchkante() {
		TabellenAnsicht result = null;
		String query = "SELECT BruchkanteCode,BruchkanteName FROM bruchkante WHERE geloescht='N' ORDER by BruchkanteName;";
		int suchmodus = TabellenAnsicht.BRUCHKANTESUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachOberflaechenErhaltung() {
		TabellenAnsicht result = null;
		String query = "SELECT ErhaltungCode,ErhaltungName FROM erhaltung WHERE geloescht='N' ORDER by ErhaltungName;";
		int suchmodus = TabellenAnsicht.ERHALTUNGSUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachAlter1() {
		TabellenAnsicht result = null;
		String query = "SELECT Alter1Code,Alter1Name FROM alter1 WHERE geloescht='N' ORDER by Alter1Name;";
		int suchmodus = TabellenAnsicht.ALTER1SUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachAlter2() {
		TabellenAnsicht result = null;
		String query = "SELECT Alter2Code,Alter2Name FROM alter2 WHERE geloescht='N' ORDER by Alter2Name;";
		int suchmodus = TabellenAnsicht.ALTER2SUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachAlter3() {
		TabellenAnsicht result = null;
		String query = "SELECT Alter3Code,Alter3Name FROM alter3 WHERE geloescht='N' ORDER by Alter3Name;";
		int suchmodus = TabellenAnsicht.ALTER3SUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachAlter4() {
		TabellenAnsicht result = null;
		String query = "SELECT Alter4Code,Alter4Name FROM alter4 WHERE geloescht='N' ORDER by Alter4Name;";
		int suchmodus = TabellenAnsicht.ALTER4SUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachAlter5() {
		TabellenAnsicht result = null;
		String query = "SELECT Alter5Code,Alter5Name FROM alter5 WHERE geloescht='N' ORDER by Alter5Name;";
		int suchmodus = TabellenAnsicht.ALTER5SUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachWurzelfrass() {
		TabellenAnsicht result = null;
		String query = "SELECT WurzelfrassCode,WurzelfrassName FROM wurzelfrass WHERE geloescht='N' ORDER by WurzelfrassName;";
		int suchmodus = TabellenAnsicht.WURZELFRASS;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachVersinterung() {
		TabellenAnsicht result = null;
		String query = "SELECT VersinterungCode,VersinterungName FROM versinterung WHERE geloescht='N' ORDER by VersinterungName;";
		int suchmodus = TabellenAnsicht.VERSINTERUNG;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachFettig() {
		TabellenAnsicht result = null;
		String query = "SELECT FettCode,FettName FROM fett WHERE geloescht='N' ORDER by FettName;";
		int suchmodus = TabellenAnsicht.FETTIGSUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachPatina() {
		TabellenAnsicht result = null;
		String query = "SELECT PatinaCode,PatinaName FROM patina WHERE geloescht='N' ORDER by PatinaName;";
		int suchmodus = TabellenAnsicht.PATINASUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachBrandspur() {
		TabellenAnsicht result = null;
		String query = "SELECT BrandspurCode,BrandspurName FROM brandspur WHERE geloescht='N' ORDER by BrandspurName;";
		int suchmodus = TabellenAnsicht.BRANDSPURSUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachVerbiss() {
		TabellenAnsicht result = null;
		String query = "SELECT VerbissCode,VerbissName FROM verbiss WHERE geloescht='N' ORDER by VerbissName;";
		int suchmodus = TabellenAnsicht.VERBISSSUCHE;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachSchlachtspur1() {
		TabellenAnsicht result = null;
		String query = "SELECT Schlachtspur1Code,Schlachtspur1Name FROM schlachtspur1 WHERE geloescht='N' ORDER by Schlachtspur1Name;";
		int suchmodus = TabellenAnsicht.SCHLACHTSPUR1;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachSchlachtspur2() {
		TabellenAnsicht result = null;
		String query = "SELECT Schlachtspur2Code,Schlachtspur2Name FROM schlachtspur2 WHERE geloescht='N' ORDER by Schlachtspur2Name;";
		int suchmodus = TabellenAnsicht.SCHLACHTSPUR2;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachGeschlecht() {
		TabellenAnsicht result = null;
		String query = "SELECT GeschlechtCode,GeschlechtName FROM geschlecht WHERE geloescht='N' ORDER by GeschlechtName;";
		int suchmodus = TabellenAnsicht.GESCHLECHT;
		lookupCode(result, query, suchmodus);
	}

	private void suchenNachBruchkante2() {
		TabellenAnsicht result = null;
		String query = "SELECT Bruchkante2Code,Bruchkante2Name FROM bruchkante2 WHERE geloescht='N' ORDER by Bruchkante2Name;";
		int suchmodus = TabellenAnsicht.BRUCHKANTE2;
		lookupCode(result, query, suchmodus);
	}

	/**
	 * @param result
	 * @param query
	 * @param suchmodus
	 */
	private void lookupCode(TabellenAnsicht result, String query, int suchmodus) {
		try {
			if (_log.isDebugEnabled()) {
				_log.debug("Query: " + query);
			}
			ProjectManipulation window = (ProjectManipulation) (parentReference
					.getDesktop().getSelectedFrame());
			ProjektFenster p = window.getProjectWindow();
			CSVExport export = new CSVExport(manager.getConnection());
			result = export.showResultTable(query, p, suchmodus);
		} catch (Exception f) {
			JOptionPane.showMessageDialog(parentReference.getDesktop(),
					"W\u00E4hlen sie das betroffene Projekt.");
		}
		if (!(result == null)) {
			JInternalFrame intFrame = new JInternalFrame();
			result.setParentContainer(intFrame);
			intFrame.setSize(400, 500);
			intFrame.setClosable(true);
			intFrame.setResizable(true);
			JScrollPane sp = new JScrollPane(result);
			intFrame.getContentPane().add(sp);
			intFrame.setVisible(true);
			parentReference.getDesktop().add(intFrame);
			try {
				intFrame.setSelected(true);
			} catch (PropertyVetoException e1) {

			}
		}
	}

	/**
	 * 
	 */
	private void zeigeAboutPopup() {
		JOptionPane
				.showInternalMessageDialog(
						parentReference.getDesktop(),
						KonfigurationOssobook.PROGRAMNAME
								+ " (c) by M.D\u00FCrrenberger 2005.\n\nDebugging by C.v.d.Meijden 2007.\n\n"
								+ "\n\n Further Development by J.Lamprecht 2008.");
	}

	/**
	 * 
	 */
	private void zeigeTemplateArtefakt() {
		try {
			ProjectManipulation window = (ProjectManipulation) (parentReference
					.getDesktop().getSelectedFrame());
			ProjektFenster p = window.getProjectWindow();
			ArtefaktSchablone atemp = p.showArtefaktTemplate();
			parentReference.getDesktop().add(atemp);
			atemp.moveToFront();
			atemp.setSelected(true);
		} catch (Exception f) {
			JOptionPane.showMessageDialog(parentReference.getDesktop(),
					"W\u00E4hlen sie das betroffene Projekt.");
		}
	}

	/**
	 * 
	 */
	private void zeigeTemplateIndet() {
		try {
			ProjectManipulation window = (ProjectManipulation) (parentReference
					.getDesktop().getSelectedFrame());
			ProjektFenster p = window.getProjectWindow();
			parentReference.getDesktop().add(p.showIndetTemplate());
		} catch (Exception f) {
			JOptionPane.showMessageDialog(parentReference.getDesktop(),
					"W\u00E4hlen sie das betroffene Projekt.");
		}
	}

	/**
	 * 
	 */
	private void zeigeTemplateKnochen() {
		try {
			ProjectManipulation window = (ProjectManipulation) (parentReference
					.getDesktop().getSelectedFrame());
			ProjektFenster p = window.getProjectWindow();
			KnochenSchablone btemp = p.showKnochenTemplate();
			parentReference.getDesktop().add(btemp);
			btemp.moveToFront();
			btemp.setSelected(true);
		} catch (Exception f) {
			JOptionPane.showMessageDialog(parentReference.getDesktop(),
					"W\u00E4hlen sie das betroffene Projekt.");
		}
	}

	public JInternalFrame getDurchsucheAlleFrame() {
		return durchsucheAlleFrame;
	}

	public JMenuBar getJMenuBar() {
		return menuBar;
	}

	public void setDurchsucheAlleFrame(JInternalFrame durchsucheAlleFrame) {
		this.durchsucheAlleFrame = durchsucheAlleFrame;
	}

	public void setQueryManager(QueryManager manager) {
		this.manager = manager;
	}
}
