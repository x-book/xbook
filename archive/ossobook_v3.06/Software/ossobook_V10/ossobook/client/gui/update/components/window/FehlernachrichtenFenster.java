package ossobook.client.gui.update.components.window;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class FehlernachrichtenFenster extends JInternalFrame {
	private JTextPane p;

	public FehlernachrichtenFenster(String title, int posx, int posy) {
		super(title);
		settings(posx, posy);
		this.setContentPane(constructContent());
	}

	private void settings(int x, int y) {
		this.setFocusable(false);
		this.setEnabled(false);
		this.setSize(100, 200);
		this.setLocation(x, y);
		this.setResizable(true);
		this.setVisible(true);
	}

	private JScrollPane constructContent() {
		p = new JTextPane();
		p.setEditable(false);
		p.setFocusable(false);
		JScrollPane result = new JScrollPane(p);
		return result;

	}

	public void updateMessageFrame(String message) {
			p.setText(message);

	}

}
