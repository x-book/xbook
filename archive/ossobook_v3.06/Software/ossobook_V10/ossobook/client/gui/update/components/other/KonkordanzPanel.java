/*
 * Created on 16.06.2001
 */
package ossobook.client.gui.update.components.other;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import ossobook.client.gui.update.components.window.*;
import ossobook.client.gui.update.elements.eingabefelder.PhasenFeld;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.QueryManager;

/**
 * @author ali
 * 
 */
public class KonkordanzPanel extends JPanel {

	private QueryManager manager;
	private int[] projectKey;
	private JPanel info;
	private String phasenlabel;

	public KonkordanzPanel(QueryManager manager, int[] projectKey,
			ProjektFenster desktop, String phasenlabel) {
		this.manager = manager;
		this.projectKey = projectKey;
		this.phasenlabel = phasenlabel;
		layouteinstellung();
		updateInfo();

	}

	/**
	 * 
	 */
	private void layouteinstellung() {
		setLayout(new GridLayout());
	}

	public void updateInfo() {
		try {
			removeAll();
		} catch (Exception e) {
		}
		try {
			info = getPanel();
			JScrollPane infoPanel = new JScrollPane(info);
			add(infoPanel);
		} catch (Exception e) {
		}
	}

	/**
	 * @return
	 */
	private JPanel getPanel() {
		JPanel result = new JPanel();
		int count = 0;
		int phasencount = 1;
		String phasencountTemp = "";
		
		try{
			String fk[][] = manager.getFK(phasenlabel, projectKey);
			for (int i = 0; i<fk.length; i++) {
				JLabel l = new JLabel(fk[i][0]);
				String phase = fk[i][1];
				// falls vorg\u00E4nger nicht gleich; eine Phase mehr
				if (phasencountTemp.equals(phase)) {
				} else {
					phasencount = phasencount + 1;
				}
				phasencountTemp = phase;
				PhasenFeld t = new PhasenFeld(phase);
				t.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						updateDB();
					}
	
				});
				result.add(l);
				result.add(t);
				count++;
			}
		}catch (StatementNotExecutedException s){}
		
		// Einheit f\u00FCr Farbbestimmung
		int colorvalue = 254 / phasencount;
		int aktuelleFarbe = colorvalue;
		PhasenFeld ftemp = (PhasenFeld) result.getComponent(1);
		for (int i = 0; i < result.getComponentCount(); i = i + 2) {
			PhasenFeld f = (PhasenFeld) result.getComponent(i + 1);

			if (f.getText().equals(ftemp.getText())) {
				f.setBackground(new Color(aktuelleFarbe, aktuelleFarbe, 100));// konstant
			} else {
				aktuelleFarbe = aktuelleFarbe + colorvalue;
				f.setBackground(new Color(aktuelleFarbe, aktuelleFarbe, 100));// konstant
			}
			if (aktuelleFarbe < 100) {
				f.setForeground(Color.WHITE);
			} else {
				f.setForeground(Color.black);
			}
			ftemp = f;
		}
		result.setLayout(new GridLayout(count, 2));
		return result;
	}

	private void updateDB() {
		int count = info.getComponentCount();
		try{
			for (int i = 0; i < count; i = i + 2) {
				String label = ((JLabel) info.getComponent(i)).getText();// Jlabel
				String content = ((JTextField) info.getComponent(i + 1))
					.getText();// JTextField
				manager.update(phasenlabel, content, label, projectKey);
			}
		}catch (StatementNotExecutedException s){}
		updateInfo();
	}
}
