/*
 * Created on 19.07.2004
* Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.gui.update.elements.eingabefelder;

import javax.swing.JCheckBox;

/**
 * @author ali
* Preferences - Java - Code Style - Code Templates
 */
@SuppressWarnings("serial")
public class AfBronzeteilFeld extends JCheckBox {
	public String toString() {
		if (isSelected()) {
			return "JA";
		} else {
			return "";
		}
	}
}