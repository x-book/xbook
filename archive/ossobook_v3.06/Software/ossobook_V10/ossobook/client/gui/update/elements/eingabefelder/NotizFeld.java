/*
 * NotizField.java
 *
 * Created on 19. M\u00E4rz 2003, 17:01
 */
package ossobook.client.gui.update.elements.eingabefelder;

import javax.swing.JTextArea;

/**
 * 
 * @author ali
 */
public class NotizFeld extends JTextArea {
	boolean keepValue = false;

	/** Creates a new instance of NotizField */
	public NotizFeld() {
		super();
		setVisible(true);

	}

	public void reset() {
		this.setText("");
	}

	public String toString() {
		return this.getText();
	}
}