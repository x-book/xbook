/*
 * Created on 19.07.2004
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.gui.update.elements.eingabefelder;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturTextFeld;



/**
 * @author ali Window - Preferences - Java - Code Style - Code Templates
 */
public class PhasenFeld extends AbstraktesSelbstkorrekturTextFeld {

    public PhasenFeld(String s) {
        super(s);
    }

    public boolean condition() {
        boolean result = true;
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
     */
    public void useDefaultValue() {
        setText("LEER");
    }

}
