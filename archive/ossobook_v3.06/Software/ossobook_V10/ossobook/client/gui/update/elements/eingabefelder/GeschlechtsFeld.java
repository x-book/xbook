/*
 * SkelCodeField.java
 *
 * Created on 24. M\u00E4rz 2003, 16:42
 */
package ossobook.client.gui.update.elements.eingabefelder;
import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;
import ossobook.client.gui.update.components.window.ProjektFenster;
/**
 * @author ali
 */
public class GeschlechtsFeld extends AbstraktesSelbstkorrekturKodeFeld {
	/** Creates a new instance of SkelCodeField */
	public GeschlechtsFeld() {
		super();

	}
	public boolean condition() {
	       boolean result = false;
	        try {
	            Integer i = new Integer(Integer.parseInt(this.getText()));
	            String s = ProjektFenster.geschlechtslist.getTeil(i);
	            if (!s.equals("")) {
	                tb.setTitle(s);
	                result = true;
	            } else {
	                tb.setTitle("");
	                result = false;
	            }

	        } catch (Exception e) {
	            tb.setTitle("");
	            result = false;
	        }
	        if (getText().equals("")) {
	            result=true;
	        }
	        return result;
	}
    /* (non-Javadoc)
     * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
     */
    public void useDefaultValue() {
    }
}
