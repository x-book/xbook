/*
 * AnimalCodeField.java
 *
 * Created on 19. M\u00E4rz 2003, 18:22
 */
package ossobook.client.gui.update.elements.eingabefelder;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;
import ossobook.client.gui.update.components.window.ProjektFenster;

/**
 * @author ali
 */
public class Alter1KodeFeld extends AbstraktesSelbstkorrekturKodeFeld {

	/** Creates a new instance of AnimalCodeField */
	public Alter1KodeFeld() {
		super();

	}

	public boolean condition() {
	       boolean result = false;
	        try {
	            Integer i = new Integer(Integer.parseInt(this.getText()));
	            String s = ProjektFenster.alter1list.getTeil(i);
	            if (!s.equals("")) {
	                tb.setTitle(s);
	                result = true;
	            } else {
	                tb.setTitle("");
	                result = false;
	            }

	        } catch (Exception e) {
	            tb.setTitle("");
	            result = false;
	        }
	        if (getText().equals("")) {
	            result=true;
	        }
	        return result;
	}

    /* (non-Javadoc)
     * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
     */
    public void useDefaultValue() {
        this.setText("0");
    }
}