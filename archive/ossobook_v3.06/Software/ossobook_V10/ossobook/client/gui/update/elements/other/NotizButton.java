/*
 * Created on 12.04.2005
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.gui.update.elements.other;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;


import ossobook.client.config.KonfigurationOssobook;
import ossobook.client.gui.common.OssobookHauptfenster;
import ossobook.client.gui.update.elements.eingabefelder.NotizFeld;

/**
 * @author ali Preferences - Java - Code Style - Code Templates
 */
public class NotizButton extends JButton {

	private JInternalFrame intFrame;

	private NotizFeld notizFeld;

	private OssobookHauptfenster mainframe;

	public NotizButton(String Beschriftung, final OssobookHauptfenster mainframe) {
		super(Beschriftung);
		this.mainframe = mainframe;
		this.intFrame = new JInternalFrame();
		this.notizFeld = new NotizFeld();
		JScrollPane sp = new JScrollPane(notizFeld);
		this.intFrame.getContentPane().add(sp);
		this.intFrame.setSize(KonfigurationOssobook.notizfeld_x, KonfigurationOssobook.notizfeld_y);
		this.intFrame.setResizable(true);
		this.intFrame.setClosable(true);
		this.intFrame.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
		mainframe.getDesktop().add(intFrame);
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				intFrame.setVisible(!intFrame.isVisible());
			}
		});
	}

	/**
	 * 
	 */
	public void reset() {
		this.notizFeld.reset();

	}

	public void setNotizFeldText(String text) {
		notizFeld.setText(text);
	}

	public String toString() {
		return this.notizFeld.toString();
	}
}