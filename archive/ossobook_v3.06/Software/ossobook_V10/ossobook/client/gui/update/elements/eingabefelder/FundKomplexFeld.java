/*
 * Created on 19.07.2004
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.gui.update.elements.eingabefelder;

import java.awt.Color;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturTextFeld;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.QueryManager;




/**
 * @author ali Window - Preferences - Java - Code Style - Code Templates
 */
public class FundKomplexFeld extends AbstraktesSelbstkorrekturTextFeld {
    private QueryManager manager;
    private int[] projectKey;

    public FundKomplexFeld(QueryManager manager, int[] projectKey) {
        this.projectKey=projectKey;
        this.manager=manager;
    }

    public boolean condition() {
        boolean result = false;
        if (getText().equals("")) {
            result = false;
        } else {
            result = true;
        }
        if (result) {
        	try{
        		int zahl = manager.getCountFk(getText(),projectKey);
        		if (zahl > 0) {
        			setBackground(Color.YELLOW);
        		} else {
        			setBackground(null);
        		}
        	}catch(StatementNotExecutedException s){
        		setBackground(null);
        	}
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
     */
    public void useDefaultValue() {
        setText("LEER");
    }

}
