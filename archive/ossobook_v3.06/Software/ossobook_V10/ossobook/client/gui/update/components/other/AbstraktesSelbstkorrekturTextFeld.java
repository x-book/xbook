/*
 * validatedTextField.java
 *
 * Created on 21. Februar 2003, 15:30
 */
package ossobook.client.gui.update.components.other;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;


/**
 *
 * @author  ali
 */
public abstract class AbstraktesSelbstkorrekturTextFeld extends GewoehnlichesTextFeld{
    private boolean correct=false;  // state
    private String lastValue;       // Last accepted Value for Rollback
    private boolean rollback=false; // Rollback Yes or No
    
    /** Creates a new instance of validatedTextField */
    public AbstraktesSelbstkorrekturTextFeld(){
        super();
        this.addFocusListener(new FocusAdapter() {   //on lostFocus : check
            public void focusLost(FocusEvent evt) {
                setBackground(null);
                checkValue();
                
                
            }
           
        });
        this.addFocusListener(new FocusAdapter() {   //on lostFocus : check
            public void focusGained(FocusEvent evt) {
                setBackground(Color.cyan);
                // Wenn Feld Aktiviert wird .. automatisch Text Markieren
                setSelectionStart(0);
                setSelectionEnd(getText().length());
            }
        });
         
    }
    
    /**
     * @param s
     */
    public AbstraktesSelbstkorrekturTextFeld(String s) {
        super(s);
        this.addFocusListener(new FocusAdapter() {   //on lostFocus : check
            public void focusLost(FocusEvent evt) {
                setBackground(null);
                checkValue();
                
                
            }
           
        });
        this.addFocusListener(new FocusAdapter() {   //on lostFocus : check
            public void focusGained(FocusEvent evt) {
                setBackground(Color.cyan);
                
            }
        });
    }

    // to be used by other classes
    public boolean check(){
        checkValue();
        return this.correct;
    }
    public void setText(String s) {
        super.setText(s);
        check();
    }
    // use condition to determine if true or false
    private void checkValue() {
        if (condition()){
            correct();
        }else{
            notCorrect();
        }   
    }
    // Conditions to be implemented
    public abstract boolean condition();
    
    // proceede if correct
    private void correct(){
        lastValue=getText();     // f\u00FCr Rollback
        this.correct=true;
        this.setForeground(Color.black);
    }
    
    // procede if false
    private void notCorrect(){      // If check fails
        this.correct=false;
        if (rollback){              //Mit Rollback
            this.setText(lastValue);
        }else{                      // Ohne Rollback
            this.setForeground(Color.red);
        }
    }

	/**
	 * 
	 */
	public void reset() {
		setText("");
		setBackground(null);
	}
    
    // Use rollback or not
    public void setRollback(boolean b){
        this.rollback=b;
    }
    
    public abstract void useDefaultValue();
}