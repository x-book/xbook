/*
 * Created on 19.07.2004
 *
* Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.gui.update.components.other;



/**
 * @author ali
* Window - Preferences - Java - Code Style - Code Templates
 */
public class GewoehnlichesSelbstkorrekturTextFeld extends AbstraktesSelbstkorrekturTextFeld {

	/**
     * @param string
     */
    public GewoehnlichesSelbstkorrekturTextFeld(String string) {
        this.setText(string);
    }

    /**
     * 
     */
    public GewoehnlichesSelbstkorrekturTextFeld() {
    }

    /* (non-Javadoc)
	 * @see ossobook.framework.elements.AbstractValidatedTextField#condition()
	 */
	public boolean condition() {
		return true;
	}

    /* (non-Javadoc)
     * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
     */
    public void useDefaultValue() {
        setText("LEER");
    }

}
