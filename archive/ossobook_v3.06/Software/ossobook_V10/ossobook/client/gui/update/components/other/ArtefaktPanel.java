/*
 * Created on 23.07.2004
 */
package ossobook.client.gui.update.components.other;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import ossobook.client.base.scheme.ArtefaktSchablone;
import ossobook.client.io.database.modell.BasisFeldJava2Db;
/**
 * @author ali
 */
@SuppressWarnings("serial")
public class ArtefaktPanel extends JPanel {
	private JPanel p1;
	private ArtefaktSchablone template;

	public ArtefaktPanel(ArtefaktSchablone template) {
		this.template = template;
		this.setToolTipText("Artefakt");
		createFields();
	}

	private void createFields() {
		setLayout(new BorderLayout());

		// Einf\u00FCgen der Felder auf die Panels nach Sektonsnummern
		// sortiert
		p1 = new JPanel();
		BasisFeldJava2Db[] f = template.getFields();
		for (int i = 0; i < f.length; i++) {
			if (f[i].getCheckBoxSwitch().isSelected()) {
				
				String mem = f[i].getDbAequivalent();
				String s = f[i].getCheckBoxSwitch().getText();
				JComponent t = f[i].getEingabeKomponente();
				t.setToolTipText(mem);
				JPanel pint = new JPanel();

				pint.setBorder(new TitledBorder(s));
				pint.add(t);
				
				JCheckBox b = new JCheckBox();//verantwortlich f\u00FCr das
				// Nichtl\u00F6schen der Werte
				b.setFocusable(false);
				pint.add(b);
				p1.add(pint);

			}
		}
		//formatiere
		int zeilen = 9; // konstante zeilenzahl: vorsicht Einschr\u00E4nkung!
		int count1 = p1.getComponentCount();
		p1.setLayout(new GridLayout(zeilen, (int) count1 / zeilen));
		add(p1);
	}

	/**
	 * @return
	 */
	public Component[] getFields() {
		return p1.getComponents();
	}
}