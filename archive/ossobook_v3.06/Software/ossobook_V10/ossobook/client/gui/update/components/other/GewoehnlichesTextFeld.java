/*
 * CommonTextField.java
 * 
 * Created on 24. M\u00E4rz 2003, 16:40
 */
package ossobook.client.gui.update.components.other;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;

import ossobook.client.config.KonfigurationOssobook;


/**
 * @author ali
 */
public class GewoehnlichesTextFeld extends javax.swing.JTextField {
	/** Creates a new instance of CommonTextField */
	public GewoehnlichesTextFeld() {
		super();
		settings();
	}

	private void settings() {
		setMargin(new Insets(0, 0, 0, 0));
		setPreferredSize(new Dimension(
				KonfigurationOssobook.gewoehnlichestextfeld_x,
				KonfigurationOssobook.gewoehnlichestextfeld_y));
		Font cf = new Font("CodeFont", Font.PLAIN, KonfigurationOssobook.kodeFont);
		setFont(cf);
	}

	/**
	 * @param s
	 */
	public GewoehnlichesTextFeld(String s) {
		super(s);
		settings();
	}

	public String toString() {
		return getText();
	}
}