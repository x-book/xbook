/*
 * AnimalCodeField.java
 *
 * Created on 19. M\u00E4rz 2003, 18:22
 */
package ossobook.client.gui.update.elements.eingabefelder;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;


/**
 * @author ali
 */
public class Alter2KodeFeld extends AbstraktesSelbstkorrekturKodeFeld {

    /** Creates a new instance of AnimalCodeField */
    public Alter2KodeFeld() {
        super();

    }

    public boolean condition() {
        boolean result = false;
        try {
            Integer i = new Integer(Integer.parseInt(this.getText()));
            // Falls vergleich mit Liste gew\u00FCnscht: Auskommentieren
            // String s = JIntFrameBoneProject.alter2list.getTeil(i);
            // if (!(s==null)) {
            // tb.setTitle(s);
            // result = true;
            // } else {
            // tb.setTitle("");
            // result = true;
            // }
            result = true; // entfernen, falls vergleich mit Liste gew\u00FCnscht
        } catch (Exception e) {
            tb.setTitle("");
            result = false;
        }
        if (getText().equals("")) {
            result = true;
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
     */
    public void useDefaultValue() {
    }
}