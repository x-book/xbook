/*
 * Created on 09.06.2001
 */
package ossobook.client.gui.update.components.window;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ossobook.client.gui.*;
import ossobook.client.config.KonfigurationOssobook;
import ossobook.client.gui.update.components.other.GewoehnlichesSelbstkorrekturTextFeld;
import ossobook.sqlQueries.QueryManager;


/**
 * @author ali
 * 
 */
@SuppressWarnings("serial")
public class NeuerTierCodeFenster extends JInternalFrame {

	private QueryManager manager;
	private JPanel maske;
	private GewoehnlichesSelbstkorrekturTextFeld TierCode, TierName, DTier,
			TierFolgeAuswertung, LB;
	private JLabel message;

	public NeuerTierCodeFenster(QueryManager manager) {
		super("Tiercode hinzuf\u00FCgen");
		this.manager = manager;
		settings();
		this.maske = generiereMaske();
		this.setContentPane(maske);
	}

	private void settings() {
		this.setSize(KonfigurationOssobook.projektveraenderungsfenster_x,
				KonfigurationOssobook.projektveraenderungsfenster_y);
		this.setVisible(true);
		this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		this.setResizable(true);
		this.setMaximizable(true);
		this.setIconifiable(true);
		this.setClosable(true);
		this.setVisible(true);
	}

	/**
	 * 
	 */
	private JPanel generiereMaske() {
		JPanel result = new JPanel();

		// formatierung
		result.setLayout(new GridLayout(6, 2));
		result.add(new JLabel("TierCode"));
		TierCode = new GewoehnlichesSelbstkorrekturTextFeld();
		result.add(TierCode);

		result.add(new JLabel("TierName"));
		TierName = new GewoehnlichesSelbstkorrekturTextFeld();
		result.add(TierName);

		result.add(new JLabel("Tier-Name Deutsch"));
		DTier = new GewoehnlichesSelbstkorrekturTextFeld();
		result.add(DTier);

		result.add(new JLabel("TierFolgeAuswertung"));
		TierFolgeAuswertung = new GewoehnlichesSelbstkorrekturTextFeld("1");
		result.add(TierFolgeAuswertung);

		result.add(new JLabel("LB"));
		LB = new GewoehnlichesSelbstkorrekturTextFeld("0");
		result.add(LB);

		JButton UpdateButton = new JButton("Insert");
		UpdateButton.setBackground(Color.YELLOW);
		UpdateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InsertAction();
			}
		});
		result.add(UpdateButton);
		message=new JLabel("Messages");
		result.add(message);
		return result;
	}

	/**
	 * 
	 */
	private void InsertAction() {
		// FELD Eintragen
		try{
			manager.updateTierart(TierCode.getText(),TierName.getText(),DTier.getText(),
				TierFolgeAuswertung.getText(),LB.getText());
			this.dispose();
		}catch(Exception e){
			this.message.setText(e.getMessage());
		}
			
	}
}
