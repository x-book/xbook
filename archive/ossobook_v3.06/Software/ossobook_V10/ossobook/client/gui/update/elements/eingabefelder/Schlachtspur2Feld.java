/*
 * SkelCodeField.java
 *
 * Created on 24. M\u00E4rz 2003, 16:42
 */
package ossobook.client.gui.update.elements.eingabefelder;
import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;
import ossobook.client.gui.update.components.window.ProjektFenster;
/**
 * @author ali
 */
public class Schlachtspur2Feld extends AbstraktesSelbstkorrekturKodeFeld {
	/** Creates a new instance of SkelCodeField */
	public Schlachtspur2Feld() {
		super();

	}
	public boolean condition() {
		boolean result = false;
		try {
			int i = Integer.parseInt(this.getText());
			result = true;
		} catch (NumberFormatException e) {
			result = false;
		}
		if (result == true) {
			Integer i=new Integer(this.getText());
			tb.setTitle(ProjektFenster.schlachtspur2list.getTeil(i));
		}
        if (this.getText().equals("")) {
            result = true;
        }
		return result;
	}
    /* (non-Javadoc)
     * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
     */
    public void useDefaultValue() {
    }
}
