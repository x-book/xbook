/*
 * AnimalCodeField.java
 *
 * Created on 19. M\u00E4rz 2003, 18:22
 */
package ossobook.client.gui.update.elements.eingabefelder;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;
import ossobook.client.gui.update.components.window.ProjektFenster;
import ossobook.client.gui.update.elements.other.MassKnopf;

/**
 * @author ali
 */
@SuppressWarnings("serial")
public class TierartKodeFeld extends AbstraktesSelbstkorrekturKodeFeld {

	private MassKnopf massknopf;

	/** Creates a new instance of AnimalCodeField */
	public TierartKodeFeld() {
		super();
		this.addFocusListener(new FocusAdapter() { // on lostFocus : check
					public void focusLost(FocusEvent evt) {
						try {
							massknopf.reset();
						} catch (Exception e) {
						}
					}
				});
	}

	public boolean condition() {
		boolean result = false;
		try {
			Integer i = new Integer(Integer.parseInt(this.getText()));
			String s = ProjektFenster.animallist.getTeil(i);
			if (!s.equals("")) {
				tb.setTitle(s);
				result = true;

			} else {
				tb.setTitle("");
				result = false;
			}

		} catch (Exception e) {
			tb.setTitle("");
			result = false;
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
	 */
	public void setMassKnopf(MassKnopf masse) {
		this.massknopf = masse;
	}

	public void useDefaultValue() {
	}
}