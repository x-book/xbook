/**
 * @author ali
 */
package ossobook.client.gui.update.components.window;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class StartFensterThread implements Runnable {

//    public static void main(String[] args) {
//        new StartFensterThread(new ImageIcon("ossobook.jpg"));
//    }
    //VARIABLEN
    java.util.Timer timer;
	private ImageIcon image;

    //ENDE VARIABLEN
    //KONSTRUKTOREN
    /** Creates a new instance of teslaView */
    public StartFensterThread(ImageIcon i) {
    	this.image=i;
        Thread thread = new Thread(this, "startup");
        thread.start();
    }

    //ENDE KONSTRUKTOREN
    //INTERFACES
    public void run() {
        JFrame fr = new JFrame();
        fr.setSize(image.getIconWidth(), image.getIconHeight());
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        fr.setLocation((int) (d.getWidth() / 2 - image.getIconWidth() / 2),
                (int) (d.getHeight() / 2 - image.getIconHeight() / 2));
        fr.setUndecorated(true);
        JLabel l = new JLabel(image);
        fr.setContentPane(l);
        fr.setVisible(true);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }
        fr.dispose();
    }
    //ENDE INTERFACES
}