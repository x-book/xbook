package ossobook.client.gui.administration;

import java.util.*;
import javax.swing.*;

import ossobook.exceptions.StatementNotExecutedException;
import ossobook.sqlQueries.QueryManager;

/**
 * controls the right changing process
 * @author j.lamprecht
 *
 */
public class UserAdministration implements Observer{

	private QueryManager manager;
	private AdminWindow window;
	private String projName;
	
	/**
	 * initializes AdminWindow to get user for which rights shall be changed
	 * and to get the right to which shall be changed
	 * @param manager
	 * @param projName: name of the project for that rights shall be changed
	 */
	public UserAdministration(QueryManager manager, String projName){
		this.manager = manager;
		this.projName=projName;
		Vector<String> users= new Vector<String>();
		try{
			users = manager.getUserNames();
		}
		catch(StatementNotExecutedException stat){}
		window = new AdminWindow(users, projName);
		window.addObserver(this);
	}
	
	/**
	 * rights shall be changed
	 */
	public void update(Observable o, Object arg){
		if(o==window){
			startChangingRight();
		}
	}
	
	/**
	 * gets the users and the right: starts the changing of the given right
	 * owned by the single users to the selected right
	 */
	private void startChangingRight(){
		Vector<String> users = window.getUsers();
		int right = window.getRight();
		boolean missingInfo = false;
		
		//project owner has selected no user
		if(users.size()==0){
			JOptionPane.showMessageDialog(window.getPanel(),
				    "Sie haben keinen User ausgew\u00E4hlt.",
				    "Rechte\u00E4nderung fehlgeschlagen",
				    JOptionPane.WARNING_MESSAGE);
			missingInfo = true;
		}
		
		//project owner has selected no right
		if(right == AdminWindow.NOTHING){
			JOptionPane.showMessageDialog(window.getPanel(),
				    "Sie haben kein Recht ausgew\u00E4hlt.",
				    "Rechte\u00E4nderung fehlgeschlagen",
				    JOptionPane.WARNING_MESSAGE);
			missingInfo= true;
		}
		
		if(missingInfo) return;
		
		try{
			for(int i=0; i<users.size(); i++){
				int[] projectKey = manager.getProjectKey(projName);
				manager.changeRight(projectKey, users.elementAt(i), right);
			}
			
			JOptionPane.showMessageDialog(window.getPanel(),
				    "Die Recht\u00E4nderung wurde erfolgreich beendet.",
				    "Rechte\u00E4nderung abgeschlossen",
				    JOptionPane.WARNING_MESSAGE);
		}
		catch(StatementNotExecutedException e){
			JOptionPane.showMessageDialog(window.getPanel(),
				    "Die Rechte konnten nicht ge\u00E4ndert werden. Bitte versuchen " +
				    "Sie es erneut oder fragen Sie Ihren Administrator.",
				    "Rechte\u00E4nderung fehlgeschlagen",
				    JOptionPane.WARNING_MESSAGE);			
		}
		
	}
	
	public JPanel getPanel(){
		return window.getPanel();
	}
}
