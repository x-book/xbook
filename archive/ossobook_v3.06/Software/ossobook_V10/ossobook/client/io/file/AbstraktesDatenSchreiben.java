/*
 * Created on 07.03.2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package ossobook.client.io.file;
import java.io.File;

import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
/**
 * @author ali
 * 
 * To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
public abstract class AbstraktesDatenSchreiben {
	protected File choice;
	protected JDesktopPane desktop;
	public AbstraktesDatenSchreiben(JDesktopPane desktop) {
		this.desktop=desktop;
		JFileChooser fc = new JFileChooser();
		desktop.add(fc);
		int returnVal = fc.showSaveDialog(desktop);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			choice = fc.getSelectedFile();
		}
		writeData();
	}
	public AbstraktesDatenSchreiben(JDesktopPane desktop, File f) {
		this.desktop=desktop;
		this.choice=f;
		writeData();
	}
	/**
	 *  
	 */
	abstract void writeData();
}
