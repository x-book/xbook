package ossobook.client.io.database.modell;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JCheckBox;
import javax.swing.JComponent;

import ossobook.client.io.database.modell.Kategorie;

/**
 * BasicField defines the minimum Attributes a Ossobook field must provide.
 * 
 * @author ali
 */
public class BasisFeldJava2Db {
	private String bezeichnung;

	private Color bgColor;

	private JCheckBox CheckBoxSwitch;

	private Color coloring;

	private String dbAequivalent;

	private JComponent eingabeKomponente;

	private boolean eingabeVeraenderbar;

	private boolean feldImmerAktiv;

	private Kategorie kategorie;

	private int sectionNr;

	private boolean useDefaultValue;

	public BasisFeldJava2Db(final JComponent eingabeKomponente,
			String bezeichnung, int sectionNr, String InfoText,
			String dbAequivalent, Kategorie kategorie, Color checkTextColor,
			Color bgColor, boolean eingabeVeraenderbar, boolean feldImmerAktiv,
			boolean defaultChecked) {
		this.eingabeKomponente = eingabeKomponente;

		this.eingabeKomponente.setToolTipText(InfoText);
		this.eingabeKomponente.addKeyListener(new KeyListener() {

			public void keyTyped(KeyEvent arg0) {

			}

			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					eingabeKomponente.nextFocus();
				}
			}

			public void keyReleased(KeyEvent arg0) {

			}

		});
		this.bezeichnung = bezeichnung;
		this.dbAequivalent = dbAequivalent;
		this.kategorie = kategorie;
		this.CheckBoxSwitch = new JCheckBox(bezeichnung, defaultChecked);
		this.CheckBoxSwitch.setToolTipText(InfoText);
		// this.CheckBoxSwitch.addFocusListener(new FocusListener() {
		// public void focusGained(FocusEvent arg0) {
		//            	
		// }
		// public void focusLost(FocusEvent arg0) {
		// }
		// });
		this.coloring = checkTextColor;
		this.sectionNr = sectionNr;
		if (bgColor != null) {
			this.bgColor = bgColor;
		} else {
			bgColor = Color.white;
		}
		this.eingabeKomponente.setBackground(this.bgColor);
		this.CheckBoxSwitch.setBackground(this.bgColor);
		this.feldImmerAktiv = feldImmerAktiv;
		// REMOVE (LEICHE) 'eingabeVeraenderbar' scheint keinen Einfluss zu
		// haben
		this.eingabeVeraenderbar = eingabeVeraenderbar;
		if (useDefaultValue) {
		}

	}

	/**
	 * @return Returns the Displayed Name of the Component.
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * @return Returns the bgColor.
	 */
	public Color getBgColor() {
		return bgColor;
	}

	/**
	 * @return Returns the checkBoxSwitch of the Component.
	 */
	public JCheckBox getCheckBoxSwitch() {
		this.CheckBoxSwitch.setEnabled(!feldImmerAktiv);
		this.CheckBoxSwitch.setForeground(this.coloring);
		return CheckBoxSwitch;
	}

	/**
	 * @return Returns the coloring.
	 */
	public Color getColoring() {
		return coloring;
	}

	/**
	 * @return Returns the dbAequivalent.
	 */
	public String getDbAequivalent() {
		return dbAequivalent;
	}

	/**
	 * @return Returns the Component.
	 */
	public JComponent getEingabeKomponente() {
		return eingabeKomponente;
	}

	/**
	 * @return Returns the kategorie.
	 */
	public Kategorie getKategorie() {
		return kategorie;
	}

	/**
	 * @return Returns the sectionNr.
	 */
	public int getSectionNr() {
		return sectionNr;
	}

	public boolean isEingabeVeraenderbar() {
		return eingabeVeraenderbar;
	}

	public boolean isFeldImmerAktiv() {
		return feldImmerAktiv;
	}

	public boolean isUseDefaultValue() {
		return useDefaultValue;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/**
	 * @param bgColor
	 *            The bgColor to set.
	 */
	public void setBgColor(Color bgColor) {
		this.bgColor = bgColor;
	}

	public void setColoring(Color coloring) {
		this.coloring = coloring;
	}

	public void setDbAequivalent(String dbAequivalent) {
		this.dbAequivalent = dbAequivalent;
	}

	public void setEingabeKomponente(JComponent eingabeKomponente) {
		this.eingabeKomponente = eingabeKomponente;
	}

	public void setEingabeVeraenderbar(boolean eingabeVeraenderbar) {
		this.eingabeVeraenderbar = eingabeVeraenderbar;
	}

	public void setFeldImmerAktiv(boolean feldImmerAktiv) {
		this.feldImmerAktiv = feldImmerAktiv;
	}

	public void setKategorie(Kategorie kategorie) {
		this.kategorie = kategorie;
	}

	/**
	 * @param sectionNr
	 *            The sectionNr to set.
	 */
	public void setSectionNr(int sectionNr) {
		this.sectionNr = sectionNr;
	}

	public void setUseDefaultValue(boolean useDefaultValue) {
		this.useDefaultValue = useDefaultValue;
	}
}