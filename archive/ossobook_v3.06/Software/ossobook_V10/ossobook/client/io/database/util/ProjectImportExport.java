package ossobook.client.io.database.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import javax.swing.JFrame;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.communication.database.DbVerbindung;

public class ProjectImportExport {
	private static Log _log = LogFactory.getLog(ProjectImportExport.class);
	private Stack entryStack = new Stack();

	private Stack measuresStack = new Stack();

	private Stack infoStack = new Stack();

	private Stack artefactStack = new Stack();

	private Connection connection = null;

	public ProjectImportExport() {
		try {
			this.connection = new DbVerbindung().connect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void projectImport(File file, int newProjectNumber, int actualPIN,
			JFrame displayframe) {
		int measureCount = getFreeMeasureNumber();
		int artefactCount = getFreeArtefactNumber();
		int counter = 1;
		displayframe.setTitle("exporting " + counter);
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			Map measureIds = new HashMap();
			Map artefactIds = new HashMap();
			while (br.ready()) {
				String line = br.readLine();
				line = line.replaceAll("__OSSOBOOKPROJECT_NAME", ""
						+ System.currentTimeMillis());
				line = line.replaceAll("__OSSOBOOKPROJECT", "'"
						+ newProjectNumber + "'");
				line = line.replaceAll("__OSSOBOOKUSER", "'" + actualPIN + "'");
				if (line.contains("__OSSOBOOKMASS_")) {
					String key = line.substring(
							line.indexOf("__OSSOBOOKMASS_"), line.indexOf(",",
									line.indexOf("__OSSOBOOKMASS_")));
					if (measureIds.get(key) != null) {
						String newID = (String) measureIds.get(key);
						line = line.replaceAll(key, "'" + newID + "'");
					} else {
						String newValue = "" + measureCount;
						measureCount++;
						measureIds.put(key, newValue);
						line = line.replaceAll(key, "'" + newValue + "'");
					}
				}
				if (line.contains("__OSSOBOOKARTEFAKT_")) {
					String key = line.substring(line
							.indexOf("__OSSOBOOKARTEFAKT_"), line.indexOf(",",
							line.indexOf("__OSSOBOOKARTEFAKT_")));
					if (artefactIds.get(key) != null) {
						String newID = (String) artefactIds.get(key);
						line = line.replaceAll(key, "'" + newID + "'");
					} else {
						String newValue = "" + artefactCount;
						artefactCount++;
						artefactIds.put(key, newValue);
						line = line.replaceAll(key, "'" + newValue + "'");
					}
				}
				displayframe.setTitle("importing " + counter);
				counter++;
				write2Database(line);
			}
			displayframe.setTitle("OssoBook");
		} catch (Exception e) {
			displayframe.setTitle("OssoBook");
			e.printStackTrace();
		}

	}

	public void projectExport(String[] projectNrArray, File file,
			JFrame displayframe) {
		displayframe.setTitle("Prepare for export ...");
		String queryLike = "";
		for (int i = 0; i < projectNrArray.length; i++) {
			queryLike = queryLike + "projNr LIKE('" + projectNrArray[i] + "')";
			if (i < projectNrArray.length - 1) {
				queryLike = queryLike + " || ";
			}
		}
		prepareExportStacks("SELECT * FROM eingabeeinheit WHERE " + queryLike
				+ " AND geloescht='N';", entryStack);
		prepareExportStacks("SELECT * FROM projekt WHERE projNr LIKE('"
				+ projectNrArray[0] + "') AND geloescht='N';", infoStack);
		writeExport(file, displayframe);
	}

	private void writeExport(File file, JFrame displayframe) {
		int counter = 1;
		try {
			FileWriter fw = new FileWriter(file);
			fw.write("USE ossobook;\n");
			while (!entryStack.empty()) {
				fw.write("INSERT INTO eingabeeinheit VALUES ("
						+ entryStack.pop() + ");\n");
				displayframe.setTitle("exporting entries " + counter);
				counter++;
			}
			while (!measuresStack.empty()) {
				fw.write("INSERT INTO masse VALUES (" + measuresStack.pop()
						+ ");\n");
				displayframe.setTitle("exporting measures " + counter);
				counter++;
			}
			while (!artefactStack.empty()) {
				fw.write("INSERT INTO artefaktmaske VALUES ("
						+ artefactStack.pop() + ");\n");
				displayframe.setTitle("exporting artifacts " + counter);
				counter++;
			}
			while (!infoStack.empty()) {
				fw.write("INSERT INTO projekt VALUES (" + infoStack.pop()
						+ ");\n");
				displayframe.setTitle("exporting infos " + counter);
				counter++;
			}
			displayframe.setTitle("OssoBook");
			fw.close();
		} catch (IOException e) {
			displayframe.setTitle("OssoBook");
			e.printStackTrace();
		}
	}

	private void prepareExportStacks(String query, Stack stack) {
		try {
			Statement s = this.connection.createStatement();
			ResultSet rs = s.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int count = rsmd.getColumnCount();
			while (rs.next()) {
				String result = "";
				for (int i = 1; i <= count; i++) {
					String columnName = rsmd.getColumnName(i);
					String value = rs.getString(i);
					if (columnName.equals("ProjName")) {
						result = result + "'" + value
								+ "__OSSOBOOKPROJECT_NAME" + "'";
					} else if (columnName.equals("ArtefaktID")
							|| columnName.equals("artefaktID")
							&& !value.equals("-1")) {
						result = result + "__OSSOBOOKARTEFAKT_" + value;
					} else if (columnName.equals("MassID")
							|| columnName.equals("massID")
							&& !value.equals("-1")) {
						result = result + "__OSSOBOOKMASS_" + value;
					} else if (columnName.equals("projNr")
							|| columnName.equals("ProjNr")) {
						result = result + "__OSSOBOOKPROJECT";
					} else if (columnName.equals("ProjBearbeiter")
							|| columnName.equals("PIN")) {
						result = result + "__OSSOBOOKUSER";
					} else if (columnName.equals("ID")) {
						result = result + "null";
					} else {
						result = result + "'" + value + "'";
					}
					if (i < count) {
						result = result + ", ";
					}
					if (columnName.equals("massID") && !value.equals("-1")) {
						prepareExportStacks(
								"SELECT * FROM masse WHERE massID LIKE('"
										+ value + "') AND geloescht='N';",
								measuresStack);
					}
					if (columnName.equals("artefaktID") && !value.equals("-1")) {
						prepareExportStacks(
								"SELECT * FROM artefaktmaske WHERE artefaktID LIKE('"
										+ value + "') AND geloescht='N';",
								artefactStack);
					}
				}
				stack.push(result);
			}
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Query: " + query);
			}
			e.printStackTrace();
		}
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query);
		}
	}

	private int getFreeMeasureNumber() {
		int result = -1;
		try {
			String query = "SELECT MAX(MassID) FROM masse;";
			Statement stat = this.connection.createStatement();
			if (_log.isDebugEnabled()) {
				_log.debug("Query: " + query);
			}
			ResultSet rs = stat.executeQuery(query);
			rs.next();
			result = rs.getInt(1);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return result + 1;
	}

	private int getFreeArtefactNumber() {
		int result = -1;
		try {
			String query = "SELECT MAX(artefaktID) FROM artefaktmaske;";
			Statement stat = this.connection.createStatement();
			if (_log.isDebugEnabled()) {
				_log.debug("Query: " + query);
			}
			ResultSet rs = stat.executeQuery(query);
			rs.next();
			result = rs.getInt(1);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return result + 1;
	}

	private void write2Database(String line) {
		try {
			Statement stat = this.connection.createStatement();
			stat.execute(line);
			if (_log.isDebugEnabled()) {
				_log.debug("Write2Database: " + line);
				}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
