package ossobook.client.io.database.modell;

import java.awt.Color;

import ossobook.client.io.database.modell.Kategorie;

/**
 * Category defines the subset to which the BasicField belongs.
 * 
 * @author ali
 *  
 */
public class Kategorie {
    public static final int BENUTZERDEFINIERT = 5;

    public static final int BESTIMMUNG = 2;

    public static final int ERHALTUNG = 3;

    public static final int IDENTIFIKATION = 1;

    public static final int PASSNUMMERN = 4;

    private int dieseKategorie;

    /**
     * constructs Category with one of the static Values:
     * Category.IDENTIFIKATION, Category.BESTIMMUNG ...
     */
    public Kategorie(int kategorie) {
        this.dieseKategorie = kategorie;
    }

    /**
     * @return the Color belonging to the Category
     */
    public Color getColor() {
        if (this.dieseKategorie == Kategorie.IDENTIFIKATION) {
            return Color.blue;
        } else if (this.dieseKategorie == Kategorie.BESTIMMUNG) {
            return Color.green;
        } else if (this.dieseKategorie == Kategorie.ERHALTUNG) {
            return Color.orange;
        } else if (this.dieseKategorie == Kategorie.PASSNUMMERN) {
            return Color.cyan;
        } else if (this.dieseKategorie == Kategorie.BENUTZERDEFINIERT) {
            return Color.red;
        } else {
            return Color.black;
        }
    }

    /**
     * @return the Name belonging to the Category
     */
    public String getKatName() {
        if (this.dieseKategorie == Kategorie.IDENTIFIKATION) {
            return "Identifikation";
        } else if (this.dieseKategorie == Kategorie.BESTIMMUNG) {
            return "Bestimmung";
        } else if (this.dieseKategorie == Kategorie.ERHALTUNG) {
            return "Erhaltung";
        } else if (this.dieseKategorie == Kategorie.PASSNUMMERN) {
            return "Passnummern";
        } else if (this.dieseKategorie == Kategorie.BENUTZERDEFINIERT) {
            return "Benutzerdefiniert";
        } else {
            return "Keine";
        }
    }
}