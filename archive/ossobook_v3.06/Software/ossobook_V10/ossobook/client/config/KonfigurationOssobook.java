package ossobook.client.config;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.client.config.KonfigurationOssobook;
import ossobook.client.io.database.communication.DisplayKonfiguration;

/**
 * Global options for OssoBook.
 * 
 * @author ali
 * 
 * 
 */
public class KonfigurationOssobook {
	private static Log _log = LogFactory.getLog(KonfigurationOssobook.class);
	/**
	 * Do we use LOCALSERVERCONNECTION mode
	 */
	public static boolean LOCALSERVERCONNECTION = false;

	/**
	 * Do we use MAINSERVERCONNECTION mode
	 */
	public static boolean MAINSERVERCONNECTION = false;

	/**
	 * The Programs Name
	 */
	public static final String PROGRAMNAME = "OssoBook";

	/** Konfiguration der Fenstergr\u00F6ssen */
	public static int notizfeld_x = 300;

	public static int notizfeld_y = 300;

	public static int durchsuchealleframe_x = 800;

	public static int durchsuchealleframe_y = 300;

	public static int massfenster_x = 500;

	public static int massfenster_y = 400;

	public static int projektauswahlfenster_x = 400;

	public static int projektauswahlfenster_y = 400;

	public static int projektbasisfenster_x = 900;

	public static int projektbasisfenster_y = 650;

	public static int projektveraenderungsfenster_x = 400;

	public static int projektveraenderungsfenster_y = 400;

	public static int ossobookgrundschablone_x = 400;

	public static int ossobookgrundschablone_y = 400;

	/** Konfiguration der Feldgr\u00F6ssen */
	public static int gewoehnlichestextfeld_x = 50;

	public static int gewoehnlichestextfeld_y = 30;

	public static int selbstkorrekturfeld_x = 50;

	public static int selbstkorrekturfeld_y = 35;

	/** Konfiguration der Schrift-gr\u00F6ssen */
	public static int kodeTitelFont = 9;

	public static int kodeFont = 9;

	public static void readKonfigruation() {

		try {
			String[] konfiguration = DisplayKonfiguration
					.getInfoFromFile(new File(
							"config/ossobookDisplayConfig.txt"));
			KonfigurationOssobook.notizfeld_x = Integer
					.parseInt(konfiguration[0]);
			KonfigurationOssobook.notizfeld_y = Integer
					.parseInt(konfiguration[1]);
			KonfigurationOssobook.durchsuchealleframe_x = Integer
					.parseInt(konfiguration[2]);
			KonfigurationOssobook.durchsuchealleframe_y = Integer
					.parseInt(konfiguration[3]);
			KonfigurationOssobook.massfenster_x = Integer
					.parseInt(konfiguration[4]);
			KonfigurationOssobook.massfenster_y = Integer
					.parseInt(konfiguration[5]);
			KonfigurationOssobook.projektauswahlfenster_x = Integer
					.parseInt(konfiguration[6]);
			KonfigurationOssobook.projektauswahlfenster_y = Integer
					.parseInt(konfiguration[7]);
			KonfigurationOssobook.projektbasisfenster_x = Integer
					.parseInt(konfiguration[8]);
			KonfigurationOssobook.projektbasisfenster_y = Integer
					.parseInt(konfiguration[9]);
			KonfigurationOssobook.projektveraenderungsfenster_x = Integer
					.parseInt(konfiguration[10]);
			KonfigurationOssobook.projektveraenderungsfenster_y = Integer
					.parseInt(konfiguration[11]);
			KonfigurationOssobook.ossobookgrundschablone_x = Integer
					.parseInt(konfiguration[12]);
			KonfigurationOssobook.ossobookgrundschablone_y = Integer
					.parseInt(konfiguration[13]);
			KonfigurationOssobook.gewoehnlichestextfeld_x = Integer
					.parseInt(konfiguration[14]);
			KonfigurationOssobook.gewoehnlichestextfeld_y = Integer
					.parseInt(konfiguration[15]);
			KonfigurationOssobook.selbstkorrekturfeld_x = Integer
					.parseInt(konfiguration[16]);
			KonfigurationOssobook.selbstkorrekturfeld_y = Integer
					.parseInt(konfiguration[17]);
			KonfigurationOssobook.kodeTitelFont = Integer
					.parseInt(konfiguration[18]);
			KonfigurationOssobook.kodeFont = Integer
					.parseInt(konfiguration[19]);
			if (_log.isInfoEnabled()) {
				_log.info("ossobookDisplayConfig.txt read successfuly.");
			}
		} catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug("can't read ossobookDisplayConfig.txt");
			}
		}
	}
}