package ossobook.client;

import java.io.*;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * saves username and password for global database connection and authentication during synchronization
 * @author j.lamprecht
 *
 */
public class LoginData{
	
	private static String username;
	private static String password;
	// file contains public key for encryption
	private final static File publicFile = new File("config/publicKey.txt");
	
	public static byte[] encryptUsername(){
		return encrypt(username);
	}
	
	public static byte[] encryptPassword(){
		return encrypt(password.toString());
	}
	
	/**
	 * used for login at the server
	 * password and username should not be transmitted unencrypted
	 * @param data
	 * @return
	 */
	private static byte[] encrypt(String data){
		try{
			FileReader fr = new FileReader(publicFile);
			BufferedReader br = new BufferedReader(fr);
			int publicExpLength = Integer.parseInt(br.readLine());
			String pubBuf = br.readLine();
			//exponent could contain lf
			while(pubBuf.length()<publicExpLength){
				pubBuf=pubBuf+ br.readLine();
			}
			BigInteger publicExp = new BigInteger(pubBuf);
			BigInteger modulus = new BigInteger(br.readLine());
					
			RSAPublicKeySpec pubSpec = new RSAPublicKeySpec(modulus, publicExp);
			PublicKey testPub = KeyFactory.getInstance("RSA").generatePublic(pubSpec);
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, testPub);
			return cipher.doFinal(data.getBytes("UTF-16"));
		}
		catch (IOException io){
			io.printStackTrace();
		}
		catch (NoSuchPaddingException p){
			p.printStackTrace();
		}
		catch (InvalidKeyException i){
			i.printStackTrace();
		}
		catch (IllegalBlockSizeException e){
			e.printStackTrace();
		}
		catch (BadPaddingException e){
			e.printStackTrace();
		}		
		catch (InvalidKeySpecException e){
			e.printStackTrace();
		}
		catch (NoSuchAlgorithmException e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * converts the char[] of the login into the String needed for establishment of connection
	 * @param password: password read in from login
	 */
	public static void setPassword(char[] password){
		if(password==null) password=null;
		else{
			LoginData.password = new String();
			for (int i=0; i<password.length; i++) LoginData.password=LoginData.password+password[i];
		}
	}
	
	public static void setStringPassword(String password){
		LoginData.password = password;
	}
	
	public static String getPassword(){
		return password;
	}
	
	public static void setUsername(String username){
		LoginData.username=username;
	}
	
	public static String getUser(){
		return username;
	}
	
}
