package ossobook.client.base.fields;

import java.sql.Connection;
import java.util.*;

import ossobook.client.gui.common.OssobookHauptfenster;
import ossobook.client.gui.update.components.other.GewoehnlichesSelbstkorrekturTextFeld;
import ossobook.client.gui.update.elements.eingabefelder.*;
import ossobook.client.gui.update.elements.other.*;
import ossobook.client.io.database.modell.*;
import ossobook.client.base.*;


/**
 * The Set of Fields designed for the Main-Screen.
 * 
 * @author ali
 */
public class FelderverbundKnochenmaske{

	private BaseFasade fasade;

	private OssobookHauptfenster mainframe;

	private Vector v; // beinhaltet alle BasicFields

	private int[] projectKey;

	public FelderverbundKnochenmaske(OssobookHauptfenster mainframe,
			BaseFasade fasade, int[] projectKey) {
		this.projectKey = projectKey;
		this.fasade=fasade;
		this.mainframe = mainframe;
		produceFields();
	}

	/**
	 * @return returns the dbAequivalent name for the given descriptor
	 *         (bezeichnung)
	 */
	public String getDbAequivalentFor(String bezeichnung) {
		for (int i = 0; i < v.size(); i++) {
			BasisFeldJava2Db bfield = (BasisFeldJava2Db) v.get(i);
			String bez = bfield.getBezeichnung();
			if (bez.equals(bezeichnung)) {
				return bfield.getDbAequivalent();
			}
		}
		return null;
	}

	/**
	 * @return the defined Array of BasicField[]
	 */
	public BasisFeldJava2Db[] getNewBasicFields() {
		BasisFeldJava2Db[] bf = new BasisFeldJava2Db[this.v.size()];
		for (int i = 0; i < this.v.size(); i++) {
			bf[i] = (BasisFeldJava2Db) v.get(i);
		}
		return bf;
	}

	/**
	 * In this Method the Fields to be hold by the Set are defined.
	 * 
	 * @see io.datenbank.modell.BasisFeldJava2Db
	 */
	@SuppressWarnings("unchecked")
	private void produceFields() {
		this.v = new Vector();
		// KNOCHENLOKALISIERUNG u. IDENTIFIKATION
		Kategorie kat = new Kategorie(Kategorie.IDENTIFIKATION);
		v.add(new BasisFeldJava2Db(new InventarNummerFeld(fasade.getManager(), projectKey),
				"Inventarnummer", 1, null, "inventarNr", kat, kat.getColor(),
				null, false, false, false));
		v.add(new BasisFeldJava2Db(new FundKomplexFeld(fasade.getManager(), projectKey),
				"Fundkomplex", 1, null, "fK", kat, kat.getColor(), null, false,
				true, true));
		v
				.add(new BasisFeldJava2Db(new KoordinatenFeld(),
						"x-Koordinaten", 1, null, "xAchse", kat,
						kat.getColor(), null, false, false, false));
		v
				.add(new BasisFeldJava2Db(new KoordinatenFeld(),
						"y-Koordinaten", 1, null, "yAchse", kat,
						kat.getColor(), null, false, false, false));
		// KNOCHENBESTIMMUNG
		kat = new Kategorie(Kategorie.BESTIMMUNG);
		TierartKodeFeld tiercodefield = new TierartKodeFeld();
		v.add(new BasisFeldJava2Db(tiercodefield, "Tierart", 1, null,
				"tierart", kat, kat.getColor(), null, false, true, true));
		SkelKodeFeld skelcodefield = new SkelKodeFeld();
		v.add(new BasisFeldJava2Db(skelcodefield, "Skelettteil", 1, null,
				"skelettteil", kat, kat.getColor(), null, false, true, true));
		v.add(new BasisFeldJava2Db(new KnochenTeilKodeFeld(skelcodefield),
				"Knochenteil", 1, null, "knochenteil", kat, kat.getColor(),
				null, false, true, true));
		v.add(new BasisFeldJava2Db(new Alter1KodeFeld(), "Alter1 Ossobook", 1,
				null, "alter1", kat, kat.getColor(), null, false, true, true));
		v.add(new BasisFeldJava2Db(new Alter2KodeFeld(), "Alter2 Ossobook", 1,
				null, "alter2", kat, kat.getColor(), null, false, true, true));
		v
				.add(new BasisFeldJava2Db(new Alter3KodeFeld(), "Alter Grant",
						1, null, "alter3", kat, kat.getColor(), null, false,
						false, false));
		v
				.add(new BasisFeldJava2Db(new Alter4KodeFeld(), "Alter Payne",
						1, null, "alter4", kat, kat.getColor(), null, false,
						false, false));
		v.add(new BasisFeldJava2Db(new Alter5KodeFeld(), "Alter HH", 1, null,
				"alter5", kat, kat.getColor(), null, false, false, false));
		v.add(new BasisFeldJava2Db(new GeschlechtsFeld(), "Geschlecht", 2,
				null, "geschlecht", kat, kat.getColor(), null, false, false,
				true));
		v.add(new BasisFeldJava2Db(new KoerperSeiteFeld(), "K\u00F6rperseite", 2,
				null, "koerperseite", kat, kat.getColor(), null, false, false,
				true));
		v.add(new BasisFeldJava2Db(new GewichtFeld(), "Gewicht", 2, null,
				"gewicht", kat, kat.getColor(), null, false, true, true));
		v.add(new BasisFeldJava2Db(new MassKnopf(mainframe, skelcodefield,
				tiercodefield), "Masse", 2, null, "massID", kat,
				kat.getColor(), null, false, true, true));
		v.add(new BasisFeldJava2Db(new NotizButton("I/O", mainframe),
				"Notizfeld Masse", 2, null, "massNotiz", kat, kat.getColor(),
				null, false, false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"Pathologie", 2, null, "pathologie", kat, kat.getColor(), null,
				false, false, true));

		v.add(new BasisFeldJava2Db(new BruchkantenFeld(), "Bruchkante", 2,
				null, "bruchkante", kat, kat.getColor(), null, false, false,
				true));

		v.add(new BasisFeldJava2Db(new Bruchkanten2Feld(), "Bruchkante2", 2,
				null, "bruchkante2", kat, kat.getColor(), null, false, false,
				true));
		// KNOCHENERHALTUNG
		kat = new Kategorie(Kategorie.ERHALTUNG);

		v.add(new BasisFeldJava2Db(new ErhaltungFeld(), "Oberfl\u00E4chenerhaltung",
				3, null, "erhaltung", kat, kat.getColor(), null, false, false,
				true));
		v.add(new BasisFeldJava2Db(new WurzelfrassFeld(), "Wurzelfrass", 3,
				null, "wurzelfrass", kat, kat.getColor(), null, false, false,
				true));
		v.add(new BasisFeldJava2Db(new VersinterungFeld(),
				"Versinterung/Krusten", 3, null, "kruste", kat, kat.getColor(),
				null, false, false, true));
		v.add(new BasisFeldJava2Db(new FettFeld(), "fettig gl\u00E4nzend", 3, null,
				"fettig", kat, kat.getColor(), null, false, false, true));
		v.add(new BasisFeldJava2Db(new PatinaFeld(), "Verf\u00E4rbungen/Patina", 3,
				null, "patina", kat, kat.getColor(), null, false, false, true));
		v.add(new BasisFeldJava2Db(new BrandspurFeld(), "Brandspuren", 3, null,
				"brandspur", kat, kat.getColor(), null, false, false, true));
		v.add(new BasisFeldJava2Db(new VerbissFeld(), "Verbiss", 3, null,
				"verbiss", kat, kat.getColor(), null, false, false, true));
		v.add(new BasisFeldJava2Db(new VerdautFeld(), "verdaut", 3, null,
				"verdaut", kat, kat.getColor(), null, false, false, true));
		v.add(new BasisFeldJava2Db(new Schlachtspur1Feld(), "Schlachtspur 1",
				3, null, "schlachtS1", kat, kat.getColor(), null, false, false,
				true));
		v.add(new BasisFeldJava2Db(new Schlachtspur2Feld(), "Schlachtspur 2",
				3, null, "schlachtS2", kat, kat.getColor(), null, false, false,
				true));
		v.add(new BasisFeldJava2Db(new VerdautFeld(), "Feuchtboden", 3, null,
				"feuchtboden", kat, kat.getColor(), null, false, false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"Gr\u00F6sse 1 (Fisch)", 4, null, "groesse1", kat, kat.getColor(),
				null, false, false, false));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"Gr\u00F6sse 2 (Fisch)", 4, null, "groesse2", kat, kat.getColor(),
				null, false, false, false));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"Saison (Fisch)", 4, null, "saison", kat, kat.getColor(), null,
				false, false, false));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"Jahrring (Fisch)", 4, null, "jahrring", kat, kat.getColor(),
				null, false, false, false));
		// PASSNUMMERN
		kat = new Kategorie(Kategorie.PASSNUMMERN);
		v.add(new BasisFeldJava2Db(new SketeillNummerFeld(fasade.getManager(), projectKey),
			"Skelettnummer",4,
			"Falls mehrere Knochen zu einem Skellett geh\u00F6ren, kann daf\u00FCr eine Skelletnummer vergeben werden.",
			"skelNr", kat, kat.getColor(), null, false, false,false));
		v.add(new BasisFeldJava2Db(new IndividuumFeld(fasade.getManager(), projectKey),
			"Individuums Nummer",4,
			"Falls mehrere Splitter eines Knochens zu einem Individuum zusammengefasst werden k\u00F6nnen, kann daf\u00FCr eine Individuumsnummer vergeben werden.",
			"knIndNr", kat, kat.getColor(), null, false, false,false));
		v.add(new BasisFeldJava2Db(new VerdautFeld(), "Ind.Nr Sicher", 4, null,
				"indivnrsicher", kat, kat.getColor(), null, false, false, false));

		// BENUTZERDEFINIERTE FELDER
		kat = new Kategorie(Kategorie.BENUTZERDEFINIERT);
		v.add(new BasisFeldJava2Db(new NotizButton("I/O", mainframe),
				"Notizfeld Objekt", 4, null, "objektNotiz", kat,
				kat.getColor(), null, false, false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"Zusatz 1", 4, null, "zusatzFeld1", kat, kat.getColor(), null,
				false, false, false));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"Zusatz 2", 4, null, "zusatzFeld2", kat, kat.getColor(), null,
				false, false, false));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"Zusatz 3", 4, null, "zusatzFeld3", kat, kat.getColor(), null,
				false, false, false));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"Zusatz 4", 4, null, "zusatzFeld4", kat, kat.getColor(), null,
				false, false, false));
		v.add(new BasisFeldJava2Db(new AnzahlFeld(), "Anzahl", 4, null,
				"anzahl", kat, kat.getColor(), null, false, true, true));
	}
}