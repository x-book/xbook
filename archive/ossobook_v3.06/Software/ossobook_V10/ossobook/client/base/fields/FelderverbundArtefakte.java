package ossobook.client.base.fields;

import java.awt.Color;
import java.util.Vector;

import ossobook.client.gui.common.OssobookHauptfenster;
import ossobook.client.gui.update.components.other.GewoehnlichesSelbstkorrekturTextFeld;
import ossobook.client.gui.update.elements.eingabefelder.*;
import ossobook.client.gui.update.elements.other.NotizButton;
import ossobook.client.io.database.modell.BasisFeldJava2Db;

/**
 * The Set of Fields designed for the Artefakt-Screen.
 * 
 * @author ali
 */
public class FelderverbundArtefakte {

    private OssobookHauptfenster mainframe;

    private Vector v;

    public FelderverbundArtefakte(OssobookHauptfenster mainframe) {
        this.mainframe = mainframe;
        produceFields();
    }

    /**
     * @return the defined Array of BasicField[]
     */
    public BasisFeldJava2Db[] getNewBasicFields() {
        BasisFeldJava2Db[] bf = new BasisFeldJava2Db[this.v.size()];
        for (int i = 0; i < this.v.size(); i++) {
            bf[i] = (BasisFeldJava2Db) v.get(i);
        }
        return bf;
    }

    /**
     * In this Method the Fields to be hold by the Set are defined.
     * 
     * @see io.datenbank.modell.BasisFeldJava2Db
     */
    @SuppressWarnings("unchecked")
	private void produceFields() {
        this.v = new Vector();
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "Typ", 1, null,
                "typ", null, Color.red, null, false, false, true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "Untertyp", 1,
                null, "untertyp", null, Color.red, null, false, false, true));
        v
                .add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
                        "Verzierung", 1, null, "verzierung", null, Color.blue,
                        null, false, false, true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
                "Herstellungsspur", 1, null, "herstellungsspur", null,
                Color.blue, null, false, false, true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "Gebrauchsspur",
                1, null, "gebrauchsspur", null, Color.blue, null, false, false,
                true));

        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "Basis", 1, null,
                "basis", null, Color.blue, null, false, false, true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "Spitze", 1, null,
                "spitze", null, Color.blue, null, false, false, true));
        v
                .add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
                        "Sch\u00E4ftung", 1, null, "schaeftung", null, Color.blue,
                        null, false, false, true));
        v
                .add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
                        "Spitzenform", 1, null, "spitzenform", null,
                        Color.blue, null, false, false, true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
                "Spitzenquerschnitt", 1, null, "spitzenquerschnitt", null,
                Color.blue, null, false, false, true));
        v
                .add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
                        "Meisselform", 1, null, "meisselform", null,
                        Color.blue, null, false, false, true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
                "Meissell\u00E4ngsschnitt", 1, null, "meissellaengsschnitt", null,
                Color.blue, null, false, false, true));

        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "Fragmentierung",
                1, null, "fragmentierung", null, Color.blue, null, false,
                false, true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "F\u00E4rbung", 1,
                null, "faerbung", null, Color.blue, null, false, false, true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "Inschrift", 1,
                null, "inschrift", null, Color.blue, null, false, false, true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "M1", 1,
                "Gr\u00F6sste L\u00E4nge", "m1", null, Color.blue, null, false, false,
                true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "M2", 1,
                "Gr\u00F6sste Breite", "m2", null, Color.blue, null, false, false,
                true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "M3", 1,
                "Gr\u00F6sste H\u00F6he", "m3", null, Color.blue, null, false, false,
                true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "M4", 1,
                "Durchmesser 1. Bohrloch", "m4", null, Color.blue, null, false,
                false, true));
        v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(), "M5", 1,
                "Durchmesser 2. Bohrloch", "m5", null, Color.blue, null, false,
                false, true));
        v.add(new BasisFeldJava2Db(new NotizButton("Artefakt Notizen", mainframe),
                "Diverse Notizen", 1, null, "diverseNotizen", null, Color.blue,
                null, false, false, true));
        v.add(new BasisFeldJava2Db(new AfEisenteilFeld(), "Eisenteile", 1, null,
                "eisenteile", null, Color.blue, null, false, false, true));
        v.add(new BasisFeldJava2Db(new AfBronzeteilFeld(), "Bronzeteile", 1, null,
                "bronzeteile", null, Color.blue, null, false, false, true));
        v.add(new BasisFeldJava2Db(new AfEinlagenFeld(), "Einlagen", 1, null,
                "einlagen", null, Color.blue, null, false, false, true));
        v.add(new BasisFeldJava2Db(new AfKompositobjektFeld(), "Kompositobjekt", 1,
                null, "kompositObjekt", null, Color.blue, null, false, false,
                true));
        v.add(new BasisFeldJava2Db(new AfUeberschliffenerBruchFeld(),
                "\u00FCberschliffener Bruch", 1, null, "ueberschliffenerBruch",
                null, Color.blue, null, false, false, true));
    }
}