package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;



/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class FettListe extends KodierungsListe {

    public FettListe(Connection connection) {
        tabellenname = "fett";
        namenColumne = "FettName";
        codeColumne = "FettCode";
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

}