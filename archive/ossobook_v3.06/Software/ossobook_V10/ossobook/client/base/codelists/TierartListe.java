package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;



/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class TierartListe extends KodierungsListe {
    /** Creates a new instance of AnimalList */
    public TierartListe(Connection connection) {
        tabellenname = "tierart";
        namenColumne = "TierName";
        codeColumne = "TierCode";
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

}