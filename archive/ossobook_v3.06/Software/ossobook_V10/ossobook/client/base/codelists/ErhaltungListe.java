package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;



/**
 * SkelCodeList from MainServer.
 * 
 * @author ali
 */
public class ErhaltungListe extends KodierungsListe {

    public ErhaltungListe(Connection connection) {
        tabellenname = "erhaltung";
        namenColumne = "ErhaltungName";
        codeColumne = "ErhaltungCode";
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

}
