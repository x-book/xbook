package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;



public class Alter5Liste extends KodierungsListe {

    public Alter5Liste(Connection connection) {
        tabellenname = "alter5";
        namenColumne = "Alter5Name";
        codeColumne = "Alter5Code";
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

}