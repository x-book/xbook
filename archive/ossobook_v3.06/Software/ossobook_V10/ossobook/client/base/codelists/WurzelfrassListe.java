package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;



/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class WurzelfrassListe extends KodierungsListe {

    public WurzelfrassListe(Connection connection) {
        tabellenname = "wurzelfrass";
        namenColumne = "WurzelfrassName";
        codeColumne = "WurzelfrassCode";
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

}