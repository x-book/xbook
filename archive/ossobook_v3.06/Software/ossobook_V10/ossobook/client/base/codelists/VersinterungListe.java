package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;



/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class VersinterungListe extends KodierungsListe {

    public VersinterungListe(Connection connection) {
        tabellenname = "versinterung";
        namenColumne = "VersinterungName";
        codeColumne = "VersinterungCode";
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

}