package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;



/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class Schlachtspur2Liste extends KodierungsListe {

    public Schlachtspur2Liste(Connection connection) {
        tabellenname = "schlachtspur2";
        namenColumne = "Schlachtspur2Name";
        codeColumne = "Schlachtspur2Code";
        teile = new HashMap();
        codes = new HashMap();
        fillHashmap(getList(connection, tabellenname));
    }

}