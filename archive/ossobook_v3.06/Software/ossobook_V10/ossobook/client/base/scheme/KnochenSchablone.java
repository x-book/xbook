package ossobook.client.base.scheme;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;

import javax.swing.JButton;

import ossobook.client.base.fields.FelderverbundKnochenmaske;
import ossobook.client.gui.common.OssobookHauptfenster;
import ossobook.client.base.*;

/**
 * Template for MainScreen.
 * 
 * @author ali
 */
@SuppressWarnings("serial")
public class KnochenSchablone extends OssobookGrundSchablone {

    private JButton setDefaultBtn;

    /**
     * Constructs BoneTemplate
     */
    public KnochenSchablone(OssobookHauptfenster mainframe, BaseFasade fasade, int[] projectKey) {
        super("Knochen Template");
        setFields(new FelderverbundKnochenmaske(mainframe, fasade,projectKey).getNewBasicFields());
        frameSettings();
        addFieldsToPanel();
        getMainPanel().add(createDefaultButton());
        try {
            loadDefault();
        } catch (Exception e) {
        }
    }

    /**
     *  
     */
    private JButton createDefaultButton() {
        setDefaultBtn = new JButton("Default");
        setDefaultBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveTemplate(new File("config/defaultKnochenTemplate.txt"));
            }
        });
        return setDefaultBtn;
    }

    /**
     *  
     */
    private void loadDefault() {
        loadTemplate(new File("config/defaultKnochenTemplate.txt"));
    }

    /**
     * 
     */
    public void loadMaxTemplate() {
        // verhindere \u00FCberschreiben des aktuellen templates f\u00FCr update
        File preserve=getAktuellesTemplate();
        File f=new File("config/maximumKnochenTemp.txt");
        loadTemplate(f);
        updateAction();
        setAktuellesTemplate(preserve);
    }



    /**
     * 
     */
    public void loadActiveTemplate() {
        loadTemplate(getAktuellesTemplate());
        updateAction();
    }

}
