package ossobook.client.base.scheme;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.client.config.KonfigurationOssobook;
import ossobook.client.gui.update.components.window.ProjektFenster;
import ossobook.client.io.database.modell.BasisFeldJava2Db;

/**
 * Basic Class for all the Templates.
 * 
 * @author ali
 * 
 */
@SuppressWarnings("serial")
public class OssobookGrundSchablone extends JInternalFrame {
	private static Log _log = LogFactory.getLog(OssobookGrundSchablone.class);
	private File aktuellesTemplate;
	private JButton confirm;

	private BasisFeldJava2Db[] fields;

	private JButton loadButton;

	private JPanel mainPanel;

	private ProjektFenster parentRef;

	private JButton saveButton;

	private String templateName = "Default";

	public OssobookGrundSchablone(String templateName) {
		if (_log.isDebugEnabled()) {
			_log.debug("Called constructor: OssobookGrundSchablone("
					+ templateName + ")");
		}
		this.templateName = templateName;
	}

	/**
	 * 
	 */
	protected void addFieldsToPanel() {
		if (_log.isDebugEnabled()) {
			_log.debug("Adding fields to panel.");
		}
		for (int i = 0; i < fields.length; i++) {
			mainPanel.add(fields[i].getCheckBoxSwitch());
		}
		confirm = new JButton("Confirm");
		confirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeAction();
			}
		});
		saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveTemplate();
			}
		});
		loadButton = new JButton("Load");
		loadButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadTemplate();
			}
		});

		this.mainPanel.add(confirm);
		this.mainPanel.add(saveButton);
		this.mainPanel.add(loadButton);
	}

	private void closeAction() {
		if (_log.isDebugEnabled()) {
			_log.debug("Closing OssobookGrundSchablone window.");
		}
		updateAction();
		try {
			parentRef.setSelected(true);
			parentRef.moveToFront();
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		this.dispose();
	}

	/**
	 * 
	 * Display properties of the Frame.
	 */
	protected void frameSettings() {
		if (_log.isDebugEnabled()) {
			_log.debug("Setting frame main-properties.");
		}
		this.setSize(KonfigurationOssobook.ossobookgrundschablone_x,
				KonfigurationOssobook.ossobookgrundschablone_y);
		this.setTitle(templateName);
		this.mainPanel = new JPanel();
		this.mainPanel.setBackground(Color.white);
		this.mainPanel.setLayout(new GridLayout((int) (this.fields.length / 2),
				2));
		this.setContentPane(mainPanel);
		this.setVisible(true);
	}

	public File getAktuellesTemplate() {
		return aktuellesTemplate;
	}

	public JButton getConfirm() {
		return confirm;
	}

	/**
	 * @return Returns the fields.
	 */
	public BasisFeldJava2Db[] getFields() {
		return fields;
	}

	public JButton getLoadButton() {
		return loadButton;
	}

	public JPanel getMainPanel() {
		return mainPanel;
	}

	/**
	 * The Mask is returning the isSelected(), !isSelected() values of the field
	 * as boolean[].
	 */
	public boolean[] getMask() {
		boolean[] b = new boolean[this.fields.length];
		for (int i = 0; i < b.length; i++) {
			b[i] = this.fields[i].getCheckBoxSwitch().isSelected();
		}
		return b;
	}

	public ProjektFenster getParentRef() {
		return parentRef;
	}

	public JButton getSaveButton() {
		return saveButton;
	}

	/**
	 * @return Returns the templateName.
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * 
	 */
	public void loadTemplate() {
		File templateFile = null;
		try {
			JFileChooser fileChooser = new JFileChooser();
			parentRef.add(fileChooser);
			int returnVal = fileChooser.showOpenDialog(parentRef
					.getDesktopPane());
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				templateFile = new File(fileChooser.getSelectedFile()
						.getAbsolutePath());
			}
			loadTemplate(templateFile);
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Problem in Function loadTemplate().");
				e.printStackTrace();
			}
		}
	}

	/**
	 * if closing button is pressed, the fields will be updated in
	 * ossobook.framework.JIntFrameBoneProject.
	 * 
	 * @see ossobook.gui.update.components.window.ProjektFenster
	 * 
	 */
	public void loadTemplate(File templateFile) {
		if (_log.isInfoEnabled()) {
			_log.info("loading template-file: " + templateFile.getPath());
		}
		// Lese Information aus File
		String s = "";
		try {
			FileReader fread = new FileReader(templateFile);
			BufferedReader bread = new BufferedReader(fread);
			while (bread.ready()) {
				s = s + bread.readLine();
			}
			aktuellesTemplate = templateFile;
		} catch (Exception e) {
		}
		// Setzte Information in boolean[] Maske um
		boolean[] barr = new boolean[s.length()];
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '0') {
				barr[i] = false;
			} else {
				barr[i] = true;
			}
		}
		setMask(barr);
	}

	public void saveTemplate() {
		File templateFile = null;
		try {
			JFileChooser fc = new JFileChooser();
			parentRef.add(fc);
			int returnVal = fc.showSaveDialog(parentRef);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				templateFile = new File(fc.getSelectedFile().getAbsolutePath());
			}
			if (_log.isDebugEnabled()) {
				_log.debug("File selected for saving: "
						+ templateFile.getPath());
			}
			saveTemplate(templateFile);
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error locating file for saveTemplate() function.");
			}
			e.printStackTrace();
		}
	}

	public void saveTemplate(File templateFile) {
		try {
			FileWriter fw = new FileWriter(templateFile);
			BufferedWriter bw = new BufferedWriter(fw);

			boolean[] barr = this.getMask();
			for (int i = 0; i < barr.length; i++) {
				if (barr[i]) {
					bw.write("1");
				} else {
					bw.write("0");
				}
			}
			bw.close();
			fw.close();
			if (_log.isInfoEnabled()) {
				_log.info("Template-file: " + templateFile.getPath()
						+ " saved.");
			}
			templateFile = null;
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Problem saving file.");
			}
			e.printStackTrace();
		}
	}

	public void setAktuellesTemplate(File aktuellesTemplate) {
		this.aktuellesTemplate = aktuellesTemplate;
	}

	public void setConfirm(JButton confirm) {
		this.confirm = confirm;
	}

	/**
	 * @param fields
	 *            The fields to set.
	 */
	public void setFields(BasisFeldJava2Db[] fields) {
		this.fields = fields;
	}

	public void setLoadButton(JButton loadButton) {
		this.loadButton = loadButton;
	}

	public void setMainPanel(JPanel mainPanel) {
		this.mainPanel = mainPanel;
	}

	/**
	 * Sets a bitmask for checkbox properties.
	 */
	public void setMask(boolean[] mask) {
		for (int i = 0; i < this.fields.length; i++) {
			this.fields[i].getCheckBoxSwitch().setSelected(mask[i]);
		}
	}

	public void setParent(ProjektFenster parent) {
		this.parentRef = parent;
	}

	public void setSaveButton(JButton saveButton) {
		this.saveButton = saveButton;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public void updateAction() {
		parentRef.updateInputScreen();
	}
}