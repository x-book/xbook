package ossobook.client.synchronization.task;

import java.util.Vector;
import javax.swing.SwingWorker;
import java.rmi.*;

import ossobook.client.base.metainfo.Projekt;
import ossobook.client.io.serverCommunication.Client;
import ossobook.client.synchronization.step.DatabaseScheme;
import ossobook.client.synchronization.step.GlobalDefinitionsGetter;
import ossobook.server.ServerMethods;
import ossobook.sqlQueries.LocalQueryManager;
import ossobook.exceptions.*;

/**
 * upper class of all tasks
 * @author j.lamprecht
 *
 */
public abstract class Task extends SwingWorker<Void, Void>{

	protected Client client;
	protected LocalQueryManager manager;
	protected Vector<Projekt> projects;
	protected Vector<String> schemePath;
	
	// document process progress
	protected String progressString;
	protected String oldProgress;

	protected String event; //"Synchronisation" or "Initialisierung"
	protected ServerMethods stub;
	protected static String tab = "     ";
	
	public Task(Client client, LocalQueryManager manager, Vector<Projekt> projects, String event){
		this.client=client;
		this.manager=manager;
		this.projects=projects;
		this.event=event;
	}
	
	/**
	 * synchronize database scheme: start process
	 * @return
	 * @throws NoServerConnection
	 */
	public DatabaseScheme synchronizeDatabaseScheme() throws NoServerConnection{
		DatabaseScheme scheme=null;
		try{
			setProgressString("\nSynchronisiere Datenbankschema");
	    	firePropertyChange(event, oldProgress, progressString);
			stub = client.getScheme();
			scheme = new DatabaseScheme(manager, stub);
			String path = scheme.synchronizeScheme();
			if(path!=null) {
	    		schemePath=new Vector<String>();
	    		schemePath.add(path); 
	    	}
			setProgressString("\nDatenbankschemasynchronisation beendet");
	    	firePropertyChange(event, oldProgress, progressString);
		}
		catch (RemoteException e){
			errorHandling("\nFehler bei Serververbindung:\n"+tab+" Datenbankschemasynchronisation fehlgeschlagen\n"+event+" abgebrochen!",
					NoServerConnection.SCHEME_STEP);
		}
		catch(NotBoundException e){
			errorHandling("\nFehler bei Serververbindung:\n"+tab+" Datenbankschemasynchronisation fehlgeschlagen\n"+event+" abgebrochen!",
					NoServerConnection.SCHEME_STEP);
		}
    	return scheme;
	}

	/**
	 * synchronize characteristics tables: start process
	 * @param scheme - scheme of global database
	 * @throws NoServerConnection
	 */
	public void synchronizeCodes(DatabaseScheme scheme) throws NoServerConnection{
		setProgressString("\nSynchronisiere Codetabellen");
    	firePropertyChange(event, oldProgress, progressString);
    	this.synchronizeDefinitions(scheme);
    	setProgressString("\nCodesynchronisation beendet");
    	firePropertyChange(event, oldProgress, progressString);
	}
	
	/**
	 * synchronize characteristics tables: for each table start process
	 * @param scheme - scheme of global database
	 * @throws NoServerConnection
	 */
	private void synchronizeDefinitions(DatabaseScheme scheme) throws NoServerConnection{
		try{
			String[] definitionTables = scheme.getDefinitionTableNames();
	    	ServerMethods stubDefinition=client.getGlobalDefinitionChanges();
	    	GlobalDefinitionsGetter def = new GlobalDefinitionsGetter(manager, stubDefinition);
	    	for(int i=0; i<definitionTables.length; i++){
	    		def.synchronizeDefinitionTable(definitionTables[i], scheme.getDefinitionTableScheme(definitionTables[i]));
	    		setProgressString("\n"+tab+" Synchronisation der Tabelle \"" + definitionTables[i]+ "\" beendet");
	    		firePropertyChange(event, oldProgress, progressString);
	    	}
		}
    	catch (RemoteException e){
    		errorHandling("\nFehler bei Serververbindung:\n"+tab+" Synchronisation der Codetabellen fehlgeschlagen\n"+event+" abgebrochen!",
    				NoServerConnection.CODE_STEP);
		}
		catch(NotBoundException e){
			errorHandling("\nFehler bei Serververbindung:\n"+tab+" Synchronisation der Codetabellen fehlgeschlagen\n"+event+" abgebrochen!",
					NoServerConnection.CODE_STEP);
		}
	}
	
	public Vector<String> getSchemePath(){
		return schemePath;
	}
	
	public String getProgressDescription(){
		return progressString;
	}
	
	/**
	 * change process status
	 * @param newValue
	 */
	protected void setProgressString(String newValue){
		oldProgress=progressString;
		progressString=newValue;
	}
	
	/**
	 * inform user about connection failure during process
	 * @param message - to print
	 * @param step - at which error occurred
	 * @throws NoServerConnection
	 */
	protected void errorHandling(String message, String step) throws NoServerConnection{
		setProgressString(message);
    	firePropertyChange("warning", oldProgress, progressString);
    	setProgressString("");
    	firePropertyChange(event, oldProgress, progressString);
		if(event.equals("Initialisierung")) event=NoServerConnection.INITIALIZE;
		else event=NoServerConnection.SYNCHRONIZE;
		throw new NoServerConnection(event, step);
	}
}
