package ossobook.client.synchronization.task;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Vector;

import ossobook.client.base.metainfo.Projekt;
import ossobook.client.io.serverCommunication.Client;
import ossobook.client.synchronization.step.DatabaseScheme;
import ossobook.client.synchronization.step.Initializer;
import ossobook.exceptions.*;
import ossobook.sqlQueries.LocalQueryManager;

/**
 * is responsible for initialization process
 * @author j.lamprecht
 *
 */
public class Initialize extends Task{
	
	private static String event= "Initialisierung";
	
	public Initialize(Client client, LocalQueryManager manager, Vector<Projekt> projects){
		super(client, manager, projects,event);
	}
	
	/**
	 * initialize projects: contains logic of initialization
	 */
	public Void doInBackground() throws StatementNotExecutedException, MetaStatementNotExecutedException{
		try{
			//adapt database scheme
			DatabaseScheme scheme=synchronizeDatabaseScheme();
			
	    	//initialize projects
			initializeProjects();
	    	
	    	//adapt characteristics tables
	    	synchronizeCodes(scheme);
		}
		catch(NoServerConnection e){
		}		
		return null;
	}	
	
	/**
	 * initialize projects: for each project start initialization process
	 * @throws NoServerConnection
	 * @throws StatementNotExecutedException
	 * @throws MetaStatementNotExecutedException
	 */
	private void initializeProjects() throws NoServerConnection, StatementNotExecutedException, MetaStatementNotExecutedException{
		try{
			manager.lockDatabase();
			setProgressString("\nInitialisiere Projekte");
	    	firePropertyChange(event, oldProgress, progressString);
			stub = client.initializeProjects();
			Initializer init = new Initializer(manager, stub);
			for(int i=0; i<projects.size(); i++){
				int[] projectKey=projects.elementAt(i).getKey();
				try{
					init.initialize(projectKey);
					setProgressString("\n"+tab+" Initialisierung des Projekts \"" +projects.elementAt(i).getName() + "\" beendet");
			    	firePropertyChange(event, oldProgress, progressString);
				}
				catch(NoReadRightExceptionForSynchronization e){
					setProgressString("\n"+tab+" Fehler bei Projektinitialisierung:\n"+tab+tab+" fehlende Leseberechtigung f\u00FCr Projekt \""
	   						+ projects.elementAt(i).getName()+ "\"\n"+tab+tab+" Projektinitialisierung abgebrochen");
			    	firePropertyChange("warning", oldProgress, progressString);
				}
			}
			setProgressString("\nProjektinitialisierung beendet");
	    	firePropertyChange(event, oldProgress, progressString);
		}
    	catch (RemoteException e){
    		errorHandling("\nFehler bei Serververbindung:\n"+tab+" Initialisierung der Projekte fehlgeschlagen\nInitialisierung abgebrochen!",
    				NoServerConnection.PROJECT_INITIALIZE_STEP);
	    	}
		catch(NotBoundException e){
			errorHandling("\nFehler bei Serververbindung:\n"+tab+" Initialisierung der Projekte fehlgeschlagen\nInitialisierung abgebrochen!",
					NoServerConnection.PROJECT_INITIALIZE_STEP);
	    }
	}
	 
}
