package ossobook.client.synchronization.step;

import java.util.*;
import java.rmi.*;

import ossobook.client.LoginData;
import ossobook.server.ServerMethods;
import ossobook.sqlQueries.*;
import ossobook.exceptions.*;

/**
 * @author j.lamprecht
 * initializes projects: first drops every project entry 
 * 						 then gets all entries for the projekt from global database 
 * 						 and inserts them in the local database
 */
public class Initializer extends Step{

	public Initializer(LocalQueryManager manager, ServerMethods stub){
		super(manager, stub);
	}
	
	/**
	 * drops project, gets project records and inserts them
	 * @param projects
	 * @return
	 */
	public void initialize(int[] projectKey) throws NoReadRightExceptionForSynchronization{
		Vector<String[][]> response = new Vector<String[][]>();
		try{
			manager.disableAutoCommit();
			response = getRecords(projectKey);		
			dropRecords(response, projectKey);
			insertRecords(response, projectKey);
			manager.enableAutoCommit();
		}
		catch (StatementNotExecutedException e){} 
	}
	
	/**
	 * drops every project entry (projekt, eingabeeinheit, masse, artefaktmaske)
	 * @param projects
	 * @throws StatementNotExecutedException
	 */
	private void dropRecords(Vector<String[][]> projectData, int[] projectKey) throws StatementNotExecutedException{			
			//project exists in database
			if(projectKey[0]!=-1) manager.deleteProjectPermanent(projectKey, QueryManager.DELETE_ALL);
	}
	
	/**
	 * gets the global project entries from the server
	 * @param projects
	 * @return
	 */
	private Vector<String[][]> getRecords(int[] projectKey) throws NoReadRightExceptionForSynchronization, StatementNotExecutedException{
		Vector<String[][]> response = null;
		try {
    	    response = stub.initializeProject(LoginData.encryptUsername(), LoginData.encryptPassword(), projectKey);
    	    if(response.size()==0){
				throw new NoReadRightExceptionForSynchronization(manager.getProjectName(projectKey));
			}
		}
		catch (RemoteException e) {
		    System.err.println("Client exception: " + e.toString());
		    e.printStackTrace();
		}
		return response;
	}
	 
	/**
	 * insert record into local database
	 * @param changes
	 * 		  Vector<String[][]> data
	 * 			'- tables: projekt, eingabeeinheit, artefaktmaske, masse, system time of server
	 * 						'- single records
	 * 						 '- column content
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 * @throws MetaStatementNotExecutedException
	 */
	private void insertRecords(Vector<String[][]> data, int[] projectKey) throws StatementNotExecutedException{
		manager.insertData(data, projectKey);
		manager.setLastSynchronizeTime(data.elementAt(5)[0][0], projectKey);
		manager.setSynchronized(projectKey);
		manager.deleteProjectPermanent(projectKey, QueryManager.DELETE_DELETED);
	}
}
