package ossobook.client.synchronization.step;

import java.util.*;
import java.rmi.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.client.LoginData;
import ossobook.communication.file.Logging;
import ossobook.exceptions.*;
import ossobook.modell.Column;
import ossobook.modell.Table;
import ossobook.server.ServerMethods;
import ossobook.sqlQueries.*;

/**
 * synchronizes the database scheme and logs differences
 * 
 * @author j.lamprecht
 * 
 */
public class DatabaseScheme extends Step {
	private static Log _log = LogFactory.getLog(DatabaseScheme.class);
	private Logging changes;
	private Vector<String[][]> projectScheme;
	private HashMap<String, String[]> definitionTables;

	/**
	 * instantiates amongst others the log file
	 * 
	 * @param manager
	 * @param stub
	 */
	public DatabaseScheme(LocalQueryManager manager, ServerMethods stub) {
		super(manager, stub);
		changes = new Logging("Schema", null);
	}

	/**
	 * 
	 * @return absolute path to log file
	 */
	public String synchronizeScheme() {
		String path = null;
		try {
			manager.disableAutoCommit();
			Vector<Table> globalTables = getGlobalDatabaseScheme(stub);
			setScheme(globalTables);
			Vector<Table> localTables = getLocalDatabaseScheme();
			checkDatabaseScheme(globalTables, localTables);
			if (changes.getRowsOfContent() >= 1)
				path = changes.getPath();
			manager.enableAutoCommit();
		} catch (StatementNotExecutedException e) {
		} catch (MetaStatementNotExecutedException e) {
		}
		return path;
	}

	/**
	 * gets the global database scheme from the server
	 * 
	 * @param stub
	 * @return
	 */
	private Vector<Table> getGlobalDatabaseScheme(ServerMethods stub) {
		Vector<Table> response = new Vector<Table>();
		try {
			response = stub.synchronizeDatabaseScheme(LoginData
					.encryptUsername(), LoginData.encryptPassword());
		} catch (RemoteException e) {
			System.err.println("Client exception: " + e.toString());
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * gets the local database scheme
	 * 
	 * @return tables with their columns and primary keys
	 */
	private Vector<Table> getLocalDatabaseScheme()
			throws MetaStatementNotExecutedException {
		Vector<Table> tables = new Vector<Table>();
		Vector<String> tableNames = manager.getTables();
		Vector<String[]> columnDescription = new Vector<String[]>();

		for (int i = 0; i < tableNames.size(); i++) {
			String tableName = tableNames.elementAt(i);
			Vector<Column> columns = new Vector<Column>();
			columnDescription = manager.getTableDescription(tableName);
			for (int j = 0; j < columnDescription.size(); j++) {
				String[] column = columnDescription.elementAt(j);
				columns.add(new Column(column[0], column[1], column[2],
						column[3], column[4], column[5]));
			}
			tables.add(new Table(tableName, columns, manager
					.getPrimaryKeys(tableName)));
		}
		return tables;
	}

	/**
	 * compares the global and local database scheme adapts the local scheme
	 * logs the differences
	 * 
	 * @param global
	 * @param local
	 */
	private void checkDatabaseScheme(Vector<Table> global, Vector<Table> local)
			throws StatementNotExecutedException {
		Vector<Table> tableGlobal = (Vector<Table>) global.clone();
		Vector<Table> tableLocal = (Vector<Table>) local.clone();

		int i = 0;

		while (tableLocal.size() > i) {
			String logEntryColumn = "";
			String logEntryPrim = "";

			String tableName = tableLocal.elementAt(i).getName();
			if (_log.isInfoEnabled()) {
				_log.info("synchronisiere Tabelle " + tableName);
			}
			for (int j = 0; j < tableGlobal.size(); j++) {
				// table already exists
				if (tableName.equals(tableGlobal.elementAt(j).getName())) {

					// adapt column scheme in local database
					logEntryColumn = adaptColumns(tableLocal.elementAt(i),
							tableGlobal.elementAt(j));

					// adapt primary keys
					logEntryPrim = adaptPrimaryKey(tableLocal.elementAt(i),
							tableGlobal.elementAt(j));

					// next table
					tableLocal.remove(tableLocal.elementAt(i));
					tableGlobal.remove(tableGlobal.elementAt(j));

					// log changes
					if (!(logEntryColumn.equals("") && (logEntryPrim.equals("")))) {
						changes.log("Tabelle " + tableName + ":\n\n");
						if (!(logEntryColumn.equals("")))
							changes.log(logEntryColumn);
						if (!(logEntryPrim.equals("")))
							changes.log(logEntryPrim + "\n\n");
						changes
								.log("-------------------------------------------------------------\n\n");
					}

					break;
				}
				if (j == tableGlobal.size() - 1) {
					i++;
				}
			}
		}
		// log tables that don't exist in global database
		for (int k = 0; k < tableLocal.size(); k++) {
			changes
					.log(tableLocal.elementAt(k).getName()
							+ ": "
							+ "Tabelle existiert nicht in globaler Datebank."
							+ " \u00C4nderungen dieser Tabelle werden NICHT in die globale Datenbank"
							+ " \u00FCbernommen.\n\n");
			changes
					.log("-------------------------------------------------------------\n\n");
		}
		createTables(tableGlobal);
	}

	/**
	 * compares primary keys of local and global table, adapts primary key if
	 * necessary
	 * 
	 * @param tableLocal
	 * @param tableGlobal
	 * @return
	 */
	private String adaptPrimaryKey(Table tableLocal, Table tableGlobal)
			throws StatementNotExecutedException {
		String logEntry = "";

		String tableName = tableGlobal.getName();
		if (!tableLocal.getPrimaryKeyIdentic(tableGlobal)) {
			Vector<String> primaryKeys = tableGlobal.getPrimaryKey();
			String[] primaryKey = new String[primaryKeys.size()];
			primaryKeys.toArray(primaryKey);
			if (tableLocal.getPrimaryKey().size() > 0)
				manager.changePrimaryKey(tableName, primaryKey);
			else
				manager.addPrimaryKey(tableName, primaryKey);

			logEntry = "Prim\u00E4rschl\u00FCssel wurde ge\u00E4ndert zu   ";
			for (int i = 0; i < primaryKey.length; i++) {
				logEntry = logEntry + primaryKey[i];
			}
		}
		return logEntry;
	}

	/**
	 * compares every single column of the local table to its global pendant and
	 * the other way round adapts local table columns to its namesake if
	 * necessary logs differences
	 * 
	 * @param localTable
	 * @param globalTable
	 * @return
	 */
	private String adaptColumns(Table localTable, Table globalTable) {
		Vector<Column> local = (Vector<Column>) localTable.getColumns().clone();
		Vector<Column> global = (Vector<Column>) globalTable.getColumns()
				.clone();

		String logEntry = "";

		String logEntryAdd = "Spalte(n) hinzugef\u00FCgt\n"
				+ "Spaltenname;   Typ;  Gr\u00F6sse;   kann null sein;   Standardwert;   Wert automatisch setzen\n";
		boolean mustLogAdd = false;
		String logEntryChange = "Spalte(n) ge\u00E4ndert\n"
				+ "Spaltenname;   Typ;   Gr\u00F6sse;   kann null sein;   Standardwert;   Wert automatisch setzen\n";
		boolean mustLogChange = false;
		boolean mustLogExist = false;
		String logEntryExist = "Spalten existieren nicht in der globalen Datenbank. "
				+ "\u00C4nderungen dieser Spalten werden NICHT in die globale Datenbank \u00FCbernommen:\n";

		try {
			while (global.size() > 0) {
				Column globalColumn = global.elementAt(0);
				if (_log.isInfoEnabled()) {
					_log
							.info("synchronisiere Spalte "
									+ globalColumn.getName());
				}
				for (int j = 0; j < local.size(); j++) {
					Column localColumn = local.elementAt(j);
					// namesake found
					if (localColumn.getName().equals(globalColumn.getName())) {
						// column must be changed in local database
						if (!localColumn.equals(globalColumn)) {
							manager.changeColumn(localTable.getName(),
									globalColumn.getName(), globalColumn
											.getType(), globalColumn.getSize(),
									globalColumn.getNullable(), globalColumn
											.getDefaultValue(), globalColumn
											.getAutoincrement());
							logEntryChange = logEntryChange
									+ logColumn(globalColumn);
							mustLogChange = true;
						}
						// next column
						local.remove(localColumn);
						global.remove(globalColumn);
						break;
					}

					// column doesn't exist in local database
					if (j == local.size() - 1) {
						logEntryAdd = logEntryAdd
								+ addColumn(globalColumn, globalTable.getName());
						global.remove(globalColumn);
						mustLogAdd = true;
					}
				}
				// column doesn't exist in local database
				if (local.size() == 0 && global.size() > 0) {
					for (int j = 0; j < global.size(); j++) {
						logEntryAdd = logEntryAdd
								+ addColumn(global.elementAt(j), globalTable
										.getName());
					}
					mustLogAdd = true;
				}
			}
			// columns don't exist in global database
			if (local.size() > 0) {
				for (int i = 0; i < local.size(); i++) {
					logEntryExist = logEntryExist
							+ local.elementAt(i).getName();
					if (i < local.size() - 1)
						logEntryExist = logEntryExist + "; ";
				}
				mustLogExist = true;
			}
		} catch (StatementNotExecutedException e) {
			e.printStackTrace();
		}
		// log differences
		if (mustLogAdd || mustLogChange || mustLogExist) {
			if (mustLogExist)
				logEntry = logEntry + logEntryExist + "\n";
			if (mustLogAdd)
				logEntry = logEntryAdd + "\n";
			if (mustLogChange)
				logEntry = logEntryChange + "\n";
		}
		return logEntry;
	}

	/**
	 * adds a column to a local table
	 * 
	 * @param column
	 * @param tableName
	 * @return
	 */
	private String addColumn(Column column, String tableName) {
		String logEntry = "";
		try {
			if (_log.isInfoEnabled()) {
				_log.info("f\u00FCge Spalte hinzu: " + column.getName());
			}
			manager.addColumn(tableName, column.getName(), column.getType(),
					column.getSize(), column.getNullable(), column
							.getDefaultValue(), column.getAutoincrement());
			logEntry = logEntry + logColumn(column);
		} catch (StatementNotExecutedException e) {
			e.printStackTrace();
		}
		return (logEntry);
	}

	/**
	 * creates non existing table in local database
	 * 
	 * @param tables
	 */
	private void createTables(Vector<Table> tables) {
		for (int i = 0; i < tables.size(); i++) {
			String tableName = tables.elementAt(i).getName();

			Vector<Column> columns = tables.elementAt(i).getColumns();
			String[] columnNames = new String[columns.size()];
			String[] types = new String[columns.size()];
			String[] sizes = new String[columns.size()];
			String[] nullable = new String[columns.size()];
			String[] defaultValues = new String[columns.size()];
			String[] autoincrement = new String[columns.size()];

			Vector<String> primKey = tables.elementAt(i).getPrimaryKey();
			String[] primaryKey = new String[tables.elementAt(i)
					.getPrimaryKey().size()];
			primKey.toArray((String[]) primaryKey);

			for (int j = 0; j < columns.size(); j++) {
				columnNames[j] = columns.elementAt(j).getName();
				types[j] = columns.elementAt(j).getType();
				sizes[j] = columns.elementAt(j).getSize();
				nullable[j] = columns.elementAt(j).getNullable();
				defaultValues[j] = columns.elementAt(j).getDefaultValue();
				autoincrement[j] = columns.elementAt(j).getAutoincrement();
			}
			try {
				manager.createTable(tableName, columnNames, types, sizes,
						nullable, defaultValues, autoincrement, primaryKey);
				logCreateTable(tables.elementAt(i));
			} catch (StatementNotExecutedException e) {
			}
		}
	}

	/**
	 * logs the description for the created table
	 * 
	 * @param table
	 */
	private void logCreateTable(Table table) {
		String logEntry = table.getName() + ":\n\n" + "neu erstellt:\n";

		Vector<Column> columns = table.getColumns();
		Vector<String> primKey = table.getPrimaryKey();

		logEntry = logEntry
				+ "Spaltenname; Typ; Gr\u00F6sse; kann null sein; Standardwert; Wert automatisch setzen\n";

		for (int j = 0; j < columns.size(); j++) {
			logEntry = logEntry + logColumn(columns.elementAt(j));
		}

		logEntry = logEntry + "Prim\u00E4rschl\u00FCssel:   ";
		for (int j = 0; j < primKey.size(); j++) {
			logEntry = logEntry + primKey.elementAt(j);
			if (j < primKey.size() - 1)
				logEntry = logEntry + "; ";
		}
		logEntry = logEntry + "\n\n";
		changes.log(logEntry);
		changes
				.log("-------------------------------------------------------------\n\n");
	}

	/**
	 * describes the structure of a column
	 * 
	 * @param column
	 * @return
	 */
	private String logColumn(Column column) {
		String logEntry = column.getName() + "; " + column.getType() + "; "
				+ column.getSize() + "; " + column.getNullable() + "; "
				+ column.getDefaultValue() + "; " + column.getAutoincrement()
				+ "\n";
		return logEntry;
	}

	/**
	 * remember global scheme
	 * 
	 * @param tables
	 */
	private void setScheme(Vector<Table> tables) {
		String[][] artefact = null;
		String[][] masse = null;
		String[][] input = null;
		String[][] project = null;
		definitionTables = new HashMap<String, String[]>();
		projectScheme = new Vector<String[][]>();
		for (int i = 0; i < tables.size(); i++) {
			Table table = tables.elementAt(i);
			if (table.getName().equals("projekt")) {
				project = new String[2][table.getColumns().size()];
				for (int k = 0; k < table.getColumns().size(); k++) {
					Column column = table.getColumns().elementAt(k);
					project[0][k] = table.getName() + "." + column.getName();
					project[1][k] = column.getType();
				}
			} else if (table.getName().equals("eingabeeinheit")) {
				input = new String[2][table.getColumns().size()];
				for (int k = 0; k < table.getColumns().size(); k++) {
					Column column = table.getColumns().elementAt(k);
					input[0][k] = table.getName() + "." + column.getName();
					input[1][k] = column.getType();
				}
			} else if (table.getName().equals("artefaktmaske")) {
				artefact = new String[2][table.getColumns().size()];
				for (int k = 0; k < table.getColumns().size(); k++) {
					Column column = table.getColumns().elementAt(k);
					artefact[0][k] = table.getName() + "." + column.getName();
					artefact[1][k] = column.getType();
				}
			} else if (table.getName().equals("masse")) {
				masse = new String[2][table.getColumns().size()];
				for (int k = 0; k < table.getColumns().size(); k++) {
					Column column = table.getColumns().elementAt(k);
					masse[0][k] = table.getName() + "." + column.getName();
					masse[1][k] = column.getType();
				}
			} else if (!table.getName().equals("version")
					&& !table.getName().equals("nachrichten")) {
				String[] definition = new String[table.getColumns().size()];
				for (int k = 0; k < table.getColumns().size(); k++) {
					Column column = table.getColumns().elementAt(k);
					definition[k] = column.getName();
					definitionTables.put(table.getName(), definition);
				}
			}
		}
		projectScheme.add(project);
		projectScheme.add(input);
		projectScheme.add(masse);
		projectScheme.add(artefact);
	}

	public Vector<String[][]> getProjectScheme() {
		return projectScheme;
	}

	public String[] getDefinitionTableScheme(String tableName) {
		return definitionTables.get(tableName);
	}

	/**
	 * get tables of database
	 * 
	 * @return
	 */
	public String[] getDefinitionTableNames() {
		Set<String> tableSet = definitionTables.keySet();
		String[] tables = new String[tableSet.size()];
		tableSet.toArray(tables);
		return tables;
	}
}
