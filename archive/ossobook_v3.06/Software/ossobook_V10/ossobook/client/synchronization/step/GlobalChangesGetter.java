package ossobook.client.synchronization.step;

import java.util.*;
import java.rmi.*;

import ossobook.client.LoginData;
import ossobook.server.ServerMethods;
import ossobook.sqlQueries.*;
import ossobook.exceptions.*;

/**
 * makes project synchronization - retransmit
 * @author j.lamprecht
 *
 */
public class GlobalChangesGetter extends Step{

	public GlobalChangesGetter(LocalQueryManager manager, ServerMethods stub){
		super(manager, stub);
	}
	
	/**
	 * retransmit project: contains logic of retransmission
	 * @param projectKey
	 * @param projectName
	 * @throws NoReadRightExceptionForSynchronization
	 */
	public void insertGlobalProjectChanges(int[] projectKey, String projectName)
			throws NoReadRightExceptionForSynchronization{
		try{
			String lastSynchronization = manager.getLastSynchronizationOfProject(projectKey);
			Vector<String[][]> changes = getGlobalChanges(projectKey, lastSynchronization);
			if (changes == null) throw new NoReadRightExceptionForSynchronization(projectName);
			insertGlobalChanges(changes, projectKey);
		}
		catch(StatementNotExecutedException s){}
		catch(MetaStatementNotExecutedException s){}
	}
	
	/**
	 * get global changes from server
	 * @param projectKey
	 * @param lastSynchronization - last time of synchronization of project
	 * @return
	 */
	private Vector<String[][]> getGlobalChanges(int[] projectKey, String lastSynchronization){
		Vector<String[][]> changes = new Vector<String[][]>();
    	try {
    	    changes = stub.getProjectChanges(LoginData.encryptUsername(), LoginData.encryptPassword(), projectKey, lastSynchronization);		    
		} catch (RemoteException e) {
		    System.err.println("Client exception: " + e.toString());
		    e.printStackTrace();
		}
		return changes;
	}
	
	/**
	 * insert changes into local database
	 * @param changes
	 * 		  Vector<String[][]> data
	 * 			'- tables: projekt, eingabeeinheit, artefaktmaske, masse, system time of server
	 * 						'- single records
	 * 						 '- column content
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 * @throws MetaStatementNotExecutedException
	 */
	private void insertGlobalChanges(Vector<String[][]> changes, int[] projectKey) throws StatementNotExecutedException, MetaStatementNotExecutedException{
		manager.disableAutoCommit();
		manager.changeData(changes, null);
		manager.setLastSynchronizeTime(changes.elementAt(5)[0][0], projectKey);
		manager.setSynchronized(projectKey);
		manager.deleteProjectPermanent(projectKey, QueryManager.DELETE_DELETED);
		manager.enableAutoCommit();
	}
	
}
