package ossobook.client.synchronization.step;

import ossobook.server.ServerMethods;
import ossobook.sqlQueries.LocalQueryManager;

/**
 * upper class of all synchronization steps of the client
 * @author j.lamprecht
 *
 */
public abstract class Step {

	ServerMethods stub;
	LocalQueryManager manager;
	
	public Step(LocalQueryManager manager, ServerMethods stub){
		this.manager = manager;
		this.stub = stub;
	}	
}
