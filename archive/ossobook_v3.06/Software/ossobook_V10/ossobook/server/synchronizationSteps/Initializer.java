package ossobook.server.synchronizationSteps;

import java.util.Vector;

import ossobook.sqlQueries.*;
import ossobook.exceptions.*;

/**
 * controls retransmission of single project on server part during initialization
 * @author j.lamprecht
 *
 */
public class Initializer extends Connection{

	/**
	 * get all entries of the given project from global database
	 * @param username
	 * @param password
	 * @param projectKey
	 * @return
	 */
	public Vector<String[][]> getProjectData(byte[] username, byte[] password, int[] projectKey){
		GlobalQueryManager manager = null;
		Vector<String[][]> data = null;
		
		try{
			manager = dbConnection(username, password);
			// prove whether user has read right
			String right=manager.getRight(projectKey);
			if(!manager.getIsAdmin()&& right==null){
				return new Vector<String[][]>();
			}
			int level=manager.setIsolationLevel(QueryManager.ISOLATION_LEVEL);
			manager.disableAutoCommit();
			data=manager.getGlobalProjectData(projectKey,QueryManager.ALL_GLOBAL, null);
			Thread.sleep(1000);
			manager.enableAutoCommit();
			manager.setIsolationLevel(level);
		}
		catch (MetaStatementNotExecutedException e){
    		manager.closeConnection();
    	}
		catch (StatementNotExecutedException e){
    		manager.closeConnection();
    	}
		catch(InterruptedException i){
			manager.closeConnection();
		}
    	catch(LoginWrongException l){
    		l.printStackTrace();
    	}
    	catch(ConnectionException c){
    		c.printStackTrace();
    	}		
		return data;
	}
	
}
