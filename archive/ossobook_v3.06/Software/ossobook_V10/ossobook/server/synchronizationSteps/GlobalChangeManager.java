package ossobook.server.synchronizationSteps;

import java.util.Vector;

import ossobook.exceptions.*;
import ossobook.sqlQueries.*;

/**
 * controls retransmission of single project on server part during synchronization
 * @author j.lamprecht
 *
 */
public class GlobalChangeManager extends Connection{

	/**
	 * get changes of the given project from global database
	 * @param username
	 * @param password
	 * @param projectKey
	 * @param lastSynchronization - last time client synchronized project
	 * @return
	 */
	public Vector<String[][]> getProjectChanges(byte[] username, byte[] password, int[] projectKey,
			String lastSynchronization){
		
		Vector<String[][]> data = new Vector<String[][]>();
		GlobalQueryManager manager = null;
		
		try{
			manager = dbConnection(username, password);
			
			// prove whether user has read right
			String right=manager.getRight(projectKey);
			if(!manager.getIsAdmin() && right==null && manager.projectExists(projectKey)){
				return null;
			}
			int level = manager.setIsolationLevel(QueryManager.ISOLATION_LEVEL);
			manager.disableAutoCommit();
			//project exists on local system
			if(lastSynchronization!=null) {
				data=manager.getGlobalProjectData(projectKey,QueryManager.CHANGES_GLOBAL,
					lastSynchronization);
			}
			else {
				data= manager.getGlobalProjectData(projectKey, QueryManager.ALL_GLOBAL, lastSynchronization);
			}
			Thread.sleep(1000);
			manager.enableAutoCommit();
			manager.setIsolationLevel(level);
		} 
		catch (MetaStatementNotExecutedException e){
    		manager.closeConnection();
    	}
		catch (StatementNotExecutedException e){
    		manager.closeConnection();
    	}
		catch(InterruptedException i){
			manager.closeConnection();
		}
    	catch(LoginWrongException l){
    		l.printStackTrace();
    	}
    	catch(ConnectionException c){
    		c.printStackTrace();
    	}		
		return data; 
	}
		
}
