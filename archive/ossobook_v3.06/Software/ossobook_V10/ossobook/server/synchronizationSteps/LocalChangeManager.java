package ossobook.server.synchronizationSteps;

import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.communication.file.*;
import ossobook.server.mail.*;
import ossobook.exceptions.*;
import ossobook.sqlQueries.*;

/**
 * controls reintegration of single project on server part during synchronization
 * @author j.lamprecht
 *
 */
public class LocalChangeManager extends Connection{
	private static Log _log = LogFactory.getLog(LocalChangeManager.class);
	private Logging conflictFile;
			
	/**
	 * insert all project changes from local database, check for conflicts
	 * @param username
	 * @param password
	 * @param projectKey
	 * @param changes
	 * @param lastSynchronization
	 * @param messageIdentify
	 * @return logged conflicts
	 */
	public Logging insertChanges(byte[] username, byte[] password, int[] projectKey,
			Vector<String[][]> changes, String lastSynchronization, int[] messageIdentify){
		GlobalQueryManager 	manager = null;
		Vector<String[]> scheme = new Vector<String[]>();
		scheme.add(changes.elementAt(0)[0]);
		scheme.add(changes.elementAt(1)[0]);
		scheme.add(changes.elementAt(2)[0]);
		scheme.add(changes.elementAt(3)[0]);
		try{
			manager = dbConnection(username, password);
			// prove whether user has write right
			String right=manager.getRight(projectKey);
			if(!manager.getIsAdmin()&& manager.projectExists(projectKey)){
				if(right==null) return null;
				if(!right.equals("schreiben")) return null;
			}
			if (_log.isInfoEnabled()) {
				_log.info("Lokale \u00E4nderungen einbringen");
			}
			int level=manager.setIsolationLevel(QueryManager.ISOLATION_LEVEL);
			manager.disableAutoCommit();
			if(manager.checkMessageNumber(messageIdentify, projectKey)){		
				Vector<Vector<String[]>> conflicts= manager.changeData(changes, lastSynchronization);
				// send conflict mail to project owner?
				if(!logConflicts(conflicts, manager.getProjectName(projectKey), scheme)){
					Mail conflictMail = new Mail(manager.getMail(projectKey), manager.getProjectName(projectKey));
					conflictMail.sendConflictMail(conflictFile.getFile());
				}
				manager.updateMessageNumber(messageIdentify, projectKey);
			}
			else{
				Vector<Vector<String[]>> conflicts = new Vector<Vector<String[]>>();
				logConflicts(conflicts, manager.getProjectName(projectKey), scheme);
			}
			manager.enableAutoCommit();
			manager.setIsolationLevel(level);
		}
		catch (MetaStatementNotExecutedException e){
    		manager.closeConnection();
    	}
		catch (StatementNotExecutedException e){
    		manager.closeConnection();
    	}
    	catch(LoginWrongException l){
    		l.printStackTrace();
    	}
    	catch(ConnectionException c){
    		c.printStackTrace();
    	}		
		return conflictFile;
	}
	
	
	private boolean logConflicts(Vector<Vector<String[]>> conflicts, String projectName, Vector<String[]> schemeVector){
		boolean noConflicts =true;
		conflictFile = new Logging("Konflikte-"+projectName, projectName);
		conflictFile.log("Projekt: " + projectName+"\n");
		conflictFile.log("-------------------------------------------------------------\n\n");
		if(conflicts.size()<1 || 
				(conflicts.elementAt(0).size()<1 && conflicts.elementAt(1).size()<1 &&
				 conflicts.elementAt(2).size()<1 && conflicts.elementAt(3).size()<1)) {
			conflictFile.log("Es sind keine Widerspr\u00FCche zum Inhalt der globalen Datenbank aufgetreten");
			if (_log.isInfoEnabled()) {
				_log.info("Keine Konflikte aufgetreten");
			}
			noConflicts=true;
		}
		else{
			if(conflicts.elementAt(0).size()>0){
				String[] columnsNotToLog ={"Zustand", "zuletztSynchronisiert"};
				this.logTableConflicts("projekt", conflicts.elementAt(0), schemeVector.elementAt(0), columnsNotToLog);				
			}
			if(conflicts.elementAt(1).size()>0){
				String[] columnsNotToLog ={"Zustand"};
				this.logTableConflicts("eingabeeinheit", conflicts.elementAt(1), schemeVector.elementAt(1), columnsNotToLog);				
			}
			if(conflicts.elementAt(2).size()>0){
				String[] columnsNotToLog ={"Zustand"};
				this.logTableConflicts("masse", conflicts.elementAt(2), schemeVector.elementAt(2), columnsNotToLog);				
			}
			if(conflicts.elementAt(3).size()>0){
				String[] columnsNotToLog ={"Zustand"};
				this.logTableConflicts("artefaktmaske", conflicts.elementAt(3), schemeVector.elementAt(3), columnsNotToLog);				
			}
			noConflicts=false;
		}
		return noConflicts;
	}
	
	private void logTableConflicts(String tableName, Vector<String[]> conflicts, String[] scheme, String[] columnsNotToLog){
		conflictFile.log("Tabelle " + tableName + ": \n\n");	
		Vector<Integer> notToLog = new Vector<Integer>();
		conflictFile.log(", ");
		for(int i=0; i<scheme.length; i++){
			boolean noLogging = false;
			String column=scheme[i].substring(scheme[i].indexOf(".")+1);
			for(int j=0; j<columnsNotToLog.length; j++){
				noLogging=false;
				if(column.equals(columnsNotToLog[j])){
					notToLog.add(Integer.valueOf(i));
					noLogging=true;
					break;
				}
			}
			if(!noLogging){
				conflictFile.log(column+" ");
				if(i<scheme.length-1) conflictFile.log(",");
			}
		}
		conflictFile.log("\n");
		for(int j=0; j<conflicts.size(); j++){
			if(j%2==0) conflictFile.log("\nalt:\n, ");
			else conflictFile.log("neu:\n, ");
			for(int i=0; i<conflicts.elementAt(j).length; i++){
				if(!notToLog.contains(Integer.valueOf(i))){
					String value = conflicts.elementAt(j)[i];
					if(value==null) conflictFile.log("-  ");
					else if(value.equals("")) conflictFile.log("-  ");
					else conflictFile.log(value+" ");
					if(i<conflicts.elementAt(j).length) conflictFile.log(",");
				}
			}
			conflictFile.log("\n");
		}
		conflictFile.log("\n");
		conflictFile.log("------------------------------");
	}
	
}
