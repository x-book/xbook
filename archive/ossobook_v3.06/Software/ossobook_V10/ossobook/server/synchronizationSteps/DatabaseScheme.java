package ossobook.server.synchronizationSteps;

import java.util.Vector;

import ossobook.exceptions.*;
import ossobook.modell.Column;
import ossobook.modell.Table;
import ossobook.sqlQueries.GlobalQueryManager;

/**
 * controls database scheme synchronization on server part
 * @author j.lamprecht
 *
 */
public class DatabaseScheme extends Connection {

	/**
	 * get database scheme from global database consisting of tables, columns and primary keys
	 * @param username
	 * @param password
	 * @return
	 */
    public Vector<Table> getDatabaseScheme(byte[] username, byte[] password){
    	Vector<Table> tables = new Vector<Table>();
    	GlobalQueryManager manager = null;
    	try{
    		manager = dbConnection(username, password); 
	    	Vector<String> tableNames = manager.getTables();
    		    		
    		for(int i=0; i<tableNames.size(); i++){
    			String tableName = tableNames.elementAt(i);
    			Vector<String[]> columnDescription = new Vector<String[]>();
    			Vector<Column> columns = new Vector<Column>();
    			columnDescription = manager.getTableDescription(tableName);
    			for(int j=0; j<columnDescription.size();j++){
    				String[] column = columnDescription.elementAt(j);
    				columns.add(new Column(column[0], column[1], column[2],
    						column[3], column[4],column[5]));
    			}
    			tables.add(new Table(tableName,columns,manager.getPrimaryKeys(tableName)));
    		} 		
    	}
    	catch (MetaStatementNotExecutedException e){
    		manager.closeConnection();
    	}
    	catch(LoginWrongException l){
    		l.printStackTrace();
    	}
    	catch(ConnectionException c){
    		c.printStackTrace();
    	}
    	return tables;
    }
	
}
