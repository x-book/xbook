package ossobook.server;

import java.rmi.server.*;
import java.rmi.registry.*;
import java.rmi.*;
import java.util.Vector;
import java.io.*;

import ossobook.communication.file.*;
import ossobook.modell.*;
import ossobook.server.synchronizationSteps.*;
import ossobook.server.mail.*;
import ossobook.client.base.metainfo.*;

/**
 * accept client requests for single synchronization steps
 * @author j.lamprecht
 *
 */
public class Server implements ServerMethods {
	
	private static final File serverFile = new File("config/ServerConfig.txt");
	private static final File mailFile = new File("config/MailConfig.txt");
		 
	/**
	 * initialize mail relay server to send conflict mails if necessary
	 */
    public Server() {
    	Mail.setConfigFile(mailFile);
    }
    
    public Boolean checkLogin(byte[] username, byte[] password){
    	return (new LoginChecker()).checkLogin(username, password);
    }
    
    public boolean isAdmin(byte[] username, byte[] password){
    	return (new AdminChecker()).isAdmin(username, password);
    }
    
    public Vector<Projekt> getGlobalProjects(byte[] username, byte[] password){
    	return (new GlobalProjectsGetter()).getGlobalProjects(username, password);
    }

    public Vector<Table> synchronizeDatabaseScheme(byte[] username, byte[] password){
    	Vector<Table> result = (new DatabaseScheme()).getDatabaseScheme(username, password);
    	return result;
    }
        
    public Vector<String[][]> initializeProject(byte[] username, byte[] password, int[] projectKey){
    	return (new Initializer()).getProjectData(username, password, projectKey);
    }
     
    public Logging insertLocalChanges(byte[] username, byte[] password, int[] projectKey,
    		Vector<String[][]> changes, String lastSynchronization, int[] messageIdentifier){
    	return (new LocalChangeManager()).insertChanges(username, password, projectKey, changes, lastSynchronization,messageIdentifier);
    }
    
    public Vector<String[][]> getProjectChanges(byte[] username, byte[] password, int[] projectKey,
    		String lastSynchronisation){
    	return (new GlobalChangeManager()).getProjectChanges(username, password, projectKey, lastSynchronisation);
    }
    
    public Vector<String[]> getDefinitionChanges(byte[] username, byte[] password,
    		String tableName, String lastSynchronization) throws RemoteException{
    	return (new DefinitionChangeManager()).getDefinitionChanges(username, password, tableName, lastSynchronization);
    } 
    
    /**
     * start server, register single server methods
     * @param args
     */
    public static void main(String args[]) {
		try {	
			DbKonfiguration con = new DbKonfiguration();
			int port = Integer.parseInt(con.getInfoFromFile(serverFile)[1]);
			
		    Server obj = new Server();
		    ServerMethods stub = (ServerMethods) UnicastRemoteObject.exportObject(obj, 0);
	
		    // Bind the remote object's stub in the registry
		    LocateRegistry.createRegistry(port);
		    Registry registry = LocateRegistry.getRegistry(port); 
		    //login check
		    registry.rebind("Login", stub);
		    registry.rebind("Admin",stub);
		    //global projects
		    registry.rebind("Projekte", stub);
		    //scheme synchronization
		    registry.rebind("Scheme", stub);
		    //project initialization
		    registry.rebind("Initialize", stub);
		    //project synchronization
		    registry.rebind("LocalChanges", stub);
		    registry.rebind("GlobalProjectChanges", stub);
		    registry.rebind("DefinitionChanges", stub);
	
		    System.err.println("Server ready");
		} 
		catch (RemoteException e) {
		    System.err.println("Server exception: " + e.toString());
		    e.printStackTrace();
		}
    }
}