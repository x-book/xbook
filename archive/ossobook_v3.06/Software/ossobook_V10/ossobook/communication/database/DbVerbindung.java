package ossobook.communication.database;
import java.io.File;
import java.sql.*;

import ossobook.communication.database.DbVerbindung;
import ossobook.communication.file.DbKonfiguration;
/**
 * This Class is the supporting Class for DBConnection. It Provides the default
 * settings for an ossobook Connection to the used MYSQL DB on Local or
 * MainHost.
 * 
 * @author ali
 */
public class DbVerbindung {
	static String db;
	static String host;
	static String passwd;
	static String user;
	private java.sql.Connection conn;
	
	/** Creates a new instance of mySqlConnection using 'ossobookDBConfig.txt' only*/
	public DbVerbindung() {
		super();
		DbKonfiguration DBC = new DbKonfiguration();
		String[] s = DBC.getInfoFromFile(new File("config/ossobookDBConfig.txt"));
		DbVerbindung.host = s[0];
		DbVerbindung.db = s[1];
		user="ossobook";
		passwd="ossobook";
		//DbVerbindung.user = s[2];
		//DbVerbindung.passwd = s[3];
		loadDriver();
	}
	/** Creates a new instance of mySqlConnection using 'ossobookDBConfig.txt' and login info from ossobook*/
	public DbVerbindung(String user, String passwd, File file) {
		super();
		DbKonfiguration DBC = new DbKonfiguration();
		String[] s = DBC.getInfoFromFile(file);
		DbVerbindung.host = s[0];
		DbVerbindung.db = s[1];
		DbVerbindung.user = user;
		DbVerbindung.passwd = passwd;
		loadDriver();
	}
	
	/** Creates a new instance of mySqlConnection using PCDATA only*/
	public DbVerbindung(String host, String db, String user, String passwd) {
		DbVerbindung.host = host;
		DbVerbindung.db = db;
		DbVerbindung.user = user;
		DbVerbindung.passwd = passwd;
		loadDriver();
	}
	

	/**
	 * 
	 * @return Connection for Project
	 * @throws Exception
	 */
	public Connection connect() throws SQLException{
		/**String connectionString = "jdbc:mysql://" + host + "/" + db + "?user="
				+ user + "&password=" + passwd;*/
		String connectionString = "jdbc:mysql://" + host + "/" + db + "?user="
			+ user + "&password=" + passwd;
		// connect to Database
		conn = DriverManager.getConnection(connectionString);
		return conn;
	}
	
	/**
	 * loading the Driver
	 *
	 */
	private void loadDriver() {
		// LOAD connectionDriver
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}