package ossobook.exceptions;

import java.awt.Point;
import java.util.Vector;

/**
 * exception in case user has no right to write global project
 * used during initialization and synchronization
 * @author j.lamprecht
 *
 */
public class NoWriteRightExceptionForSynchronization extends NoRightExceptionForSynchronization{

	public static Vector<String> projectNames = new Vector<String>();
	
	/**
	 * remember projects with missing write right
	 * @param projectName
	 */
	public NoWriteRightExceptionForSynchronization(String projectName){
		projectNames.add(projectName);
	}
	
	/**
	 * show information about projects with missing write right
	 * @param point
	 * @param event - SynchronizeCompletedWindow.SYNCHRONIZE, SynchronizeCompletedWindow.INITIALIZE
	 */
	public static void showInformation(Point point, String event){
		showInformation(point, NoRightExceptionForSynchronization.write, event, projectNames);
		projectNames = new Vector<String>();
	}
	
	public static boolean noRights(){
		return noRights(projectNames);
	}
	
}
