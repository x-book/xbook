package ossobook.exceptions;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * exception in case user has no right to change project
 * especially needed for deletion of project
 * @author j.lamprecht
 *
 */
public class NoWriteRightException extends Exception{
	 
	/**
	 * inform user about missing write right
	 * @param projname
	 * @param message
	 */
	public NoWriteRightException(String projname, String message){
		JFrame frame = new JFrame();
		JOptionPane.showMessageDialog(frame,
			    "Sie haben f\u00F6r das Projekt " + projname 
			    +" keine Schreibberechtigung. Sie k\u00F6nnen das Projekt deswegen" +
			    " nicht " + message + ". Bitte wenden Sie sich an den Projektleiter.",
			    "Fehlende Schreibberechtigung",		
			    JOptionPane.WARNING_MESSAGE
			    );
	}

}
