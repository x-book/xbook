package ossobook.exceptions;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.sql.*;

/**
 * exception for the case that a DML statement could not be executed
 * @author j.lamprecht
 *
 */
public class StatementNotExecutedException extends SQLException{

	/**
	 * inform user about DML fault
	 * @param function - statement that could not be executed
	 */
	public StatementNotExecutedException(String query){
		JFrame frame = new JFrame();
		JOptionPane.showMessageDialog(frame, query+
			    "konnte nicht ausgef\u00FChrt werden.",
			    "Statement fehlgeschlagen",
			    JOptionPane.WARNING_MESSAGE);
		this.printStackTrace();
	}
}
