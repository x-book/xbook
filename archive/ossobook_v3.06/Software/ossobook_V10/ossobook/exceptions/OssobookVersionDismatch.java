package ossobook.exceptions;

import javax.swing.*;

import java.beans.*;

public class OssobookVersionDismatch extends Exception{
	
	public OssobookVersionDismatch(){
		JFrame frame = new JFrame();
		final JOptionPane optionPane = new JOptionPane("problem initializing .. probably versionproblem. " +
			    "Bitte \u00FCberpr\u00FCfen Sie version.properties",
			    JOptionPane.WARNING_MESSAGE);	
		final JDialog dialog = new JDialog(frame, "Versionsprobleme");
		dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		optionPane.addPropertyChangeListener(
			    new PropertyChangeListener() {
			        public void propertyChange(PropertyChangeEvent e) {
			        	String prop = e.getPropertyName();
			            if ((e.getSource() == optionPane)
			            		&& (prop.equals(JOptionPane.VALUE_PROPERTY))) {
			                System.exit(0);
			            }
			        }
			    }
		);
		
		dialog.setContentPane(optionPane);
		dialog.pack();
		dialog.setVisible(true);
	}	
}
