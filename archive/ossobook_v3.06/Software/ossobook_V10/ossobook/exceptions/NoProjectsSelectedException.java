package ossobook.exceptions;

import javax.swing.*;

/**
 * exception in case user doesn't choose projects for synchronization or initialization
 * @author j.lamprecht
 *
 */
public class NoProjectsSelectedException extends Exception{

	/**
	 * inform user about missing selected projects
	 * @param frame
	 */
	public NoProjectsSelectedException(JInternalFrame frame){
		JOptionPane.showMessageDialog(frame,
			    "Sie haben keine Projekte ausgesucht, die synchronisiert bzw. initialisiert " +
			    "werden sollen.",
			    "Vorgang fehlgeschlagen",
			    JOptionPane.WARNING_MESSAGE);
	}	
}
