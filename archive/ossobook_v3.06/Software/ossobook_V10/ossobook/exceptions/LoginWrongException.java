package ossobook.exceptions;

import javax.swing.*;

/**
 * Login went wrong -> user and / or password was / were wrong
 * @author j.lamprecht
 *
 */
public class LoginWrongException extends Exception{
	
	/**
	 * informs the user about the failure of login
	 */
	public LoginWrongException(boolean showInformation){
		if(showInformation){
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog(frame,
				    "Ihr Passwort und / oder Ihr Benutzername waren falsch." +
				    "Bitte versuchen Sie es erneut.",
				    "Login fehlgeschlagen",
				    JOptionPane.WARNING_MESSAGE);
		}
	}
}
