package ossobook.exceptions;

import javax.swing.*;

/**
 * exception in case tha connection to server failed
 * used during synchronization and initialization
 * @author j.lamprecht
 *
 */
public class NoServerConnection extends Exception{

	public static String SCHEME_STEP = "Synchronisation des Datenbankschemas";
	public static String CODE_STEP = "Synchronisation einer Merkmalstabelle";
	public static String PROJECT_INITIALIZE_STEP = "Initialisierung eines Projekts";
	public static String PROJECT_SYNCHRONIZE_STEP = "Synchronisation eines Projekts";
	
	public static String INITIALIZE= "Initialisierung";
	public static String SYNCHRONIZE= "Synchronisation";
	
	/**
	 * inform user about synchronization abort of synchronization
	 * @param event - NoServerConnection.INITIALIZE,
	 * 				  NoServerConnection.SYNCHRONIZE
	 * @param step - NoServerConnection.PROJECT_INITIALIZE_STEP, 
	 * 				 NoServerConnection.PROJECT_SYNCHRONIZE_STEP,
	 * 				 NoServerConnection.SCHEME_STEP,
	 * 				 NoServerConnection.CODE_STEP
	 */
	public NoServerConnection(String event, String step){
				
		String warning="";
		
		if(!step.equals(SCHEME_STEP))
			warning = "\nEs konnten nicht alle \u00C4nderungen der Codetabellen \u00FCbertragen werden." +
					" \nDeswegen kann es beim Arbeiten mit der Ossobook-Programm zu \nProblemen. " +
					" N\u00E4here Informationen hierzu finden Sie in der Dokumentation. \nAuftretende Probleme" +
					" lassen sich durch eine erneue " + event + " \nbeheben.";
		
		String message = "W\u00E4hrend der " + step + " wurde die Verbindung zum \nServer unterbrochen." +
					" Die " + event + " ist fehlgeschlagen. Bitte versuchen \nSie es sp\u00E4ter nochmals oder wenden" +
					" Sie sich an den Administrator.\n" + warning;
		
		
		JOptionPane pane = new JOptionPane(message, JOptionPane.WARNING_MESSAGE);
		JDialog dialog = pane.createDialog(new JFrame(), "Verbindung zum Server unterbrochen!");
		dialog.setVisible(true);
	}	
}
