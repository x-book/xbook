package ossobook.exceptions;

import java.awt.Point;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * exception in case user has no right to read / write global project
 * used during initialization and synchronization
 * @author j.lamprecht
 *
 */
public abstract class NoRightExceptionForSynchronization extends Exception{

	public static final String initialize = "Initialisierung";
	public static final String synchronize = "Synchronisation";
	public static final String read = "Leseberechtigung";
	public static final String write = "Schreibberechtigung";
	
	/**
	 * inform user about missing right for projects
	 * @param point - position to show JOptionPane
	 * @param missingAuthority - NoRightExceptionForSynchronization.read, NoRightExceptionForSynchronization.write
	 * @param event - SynchronizeCompletedWindow.SYNCHRONIZE, SynchronizeCompletedWindow.INITIALIZE
	 * @param projectNames
	 */
	public static void showInformation(Point point, String missingAuthority, 
			String event, Vector<String> projectNames){
		
		String multiple;
		if(projectNames.size()>1) multiple="die Projekte";
		else multiple = "das Projekt";
		
		String projects="";
		for(int i=0; i<projectNames.size(); i++){
			projects = projects + projectNames.elementAt(i);
			if(i<projectNames.size()-1) projects = projects + "\n";
		}
		
		JFrame frame = new JFrame();
		frame.setLocation(point);
		JOptionPane.showMessageDialog(frame,
			    "Sie haben f\u00FCr " + multiple + "\n\n" + projects
			    +"\n\nkeine "+missingAuthority+". Die "+ event+" ist deswegen fehlgeschlagen.\n" +
			    "Bitte wenden Sie sich an den Projektleiter.",
			    "Fehlende "+missingAuthority+"(en)",		
			    JOptionPane.WARNING_MESSAGE
			    );
		projectNames=new Vector<String>();
	}

	/**
	 * proofs whether read / write right was missed for at least one project
	 * @param projectNames
	 * @return
	 */
	public static boolean noRights(Vector<String> projectNames){
		int size=projectNames.size();
		if(size==0) return false;
		else return true;
	}
	
}
