package ossobook.sqlQueries;

import java.io.File;
import java.sql.*;

import ossobook.communication.database.DbVerbindung;
import ossobook.communication.file.DbKonfiguration;
import ossobook.exceptions.ConnectionException;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * establishes and manages the local connection
 * @author j.lamprecht
 *
 */
public class LocalQueryManager extends QueryManager{

	private static String user = null;
	private static String pw = null;
	private static File localFile;
		
	protected int benutzernr;
	
	/**
	 * establishes the local connection
	 * @param localFile: configuration file for local connections
	 * @throws StatementNotExecutedException
	 * @throws ConnectionException
	 */
	public LocalQueryManager() throws SQLException{
		DbKonfiguration DBC = new DbKonfiguration();
		String[] s = DBC.getInfoFromFile(localFile);
		user = s[2];
		pw = s[3];
		createLocalConnection();
		init(s[1]);
		//benutzernr = getBenutzernummer(user);
	}
	
	/**
	 * establish connection
	 * @throws ConnectionException
	 */
	private void createLocalConnection() throws SQLException{		
		//try{
			DbVerbindung db = new DbVerbindung(user, pw, localFile);
			connection = db.connect();
		//}
		//catch (SQLException sql){
			//sql.printStackTrace();
			//throw new ConnectionException(true);
		//}
	}	
	
	public String getUser(){
		return user;
	}
	
	public static String getStaticUser(){
		return user;
	}
	
	//not used yet
	public int getbenutzernr(){
		return benutzernr;
	}
	
	public static void setLocalFile(File file){
		localFile=file;
	}
	
	public static File getLocalFile(){
		return localFile;
	}
}
