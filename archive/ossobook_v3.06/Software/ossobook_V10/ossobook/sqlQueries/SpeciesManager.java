package ossobook.sqlQueries;

import java.sql.*;

import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages tierart table (makes inserts, updates and select statements
 * on the table)
 * @author j.lamprecht
 *
 */
public class SpeciesManager extends TableManager{

	public SpeciesManager(Connection con, String databaseName){
		super(con, databaseName);
	}
	
	/**
	 * @precondition logged in user is admin
	 * @param TierCode
	 * @param TierName
	 * @param DTier
	 * @param TierFolgeAuswertung
	 * @param LB
	 */
	public void updateTierart(String TierCode, String TierName, String DTier, 
			String TierFolgeAuswertung, String LB){
		String insertquery = "INSERT INTO " + databaseName+".tierart VALUES("
			+"'"+ TierCode+"'" + "," +"'"+ TierName+"'"	+ ","
			+"'"+ DTier+"'"	+ ","+"'"+ TierFolgeAuswertung+"'"
			+ ","+"'"+ LB +"'"+ ", Now()+0, 'N');";
		try {
			executeUpdate(insertquery);
			
		} catch (SQLException e) {}
	}
	
	public int getTierlabel(int tiercode)throws StatementNotExecutedException{
		String query = "SELECT LB FROM " + databaseName+".tierart WHERE TierCode="
				+ tiercode + " AND geloescht='N';";
		int tierlabel = 0;
		try{
			ResultSet rs = createStatement(query);
			rs.next();
			tierlabel = rs.getInt("LB");
		}
		catch(SQLException s){
			printErrorMessage(s, query);
		}
		return tierlabel;
	}
}
