package ossobook.sqlQueries;

import java.sql.*;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.exceptions.*;

/**
 * manages database scheme 
 * @author j.lamprecht
 *
 */
public class SchemeManager extends TableManager {
	private static Log _log = LogFactory.getLog(SchemeManager.class);
	public SchemeManager(Connection con, String databaseName){
		super(con, databaseName);
	}
	
	/**
	 * gets  all table names of database
	 * @return
	 * @throws MetaStatementNotExecutedException
	 */
	public Vector<String> getTables() throws MetaStatementNotExecutedException{
		Vector<String> tableNames = new Vector<String>();
		try{
			DatabaseMetaData database = connection.getMetaData();
			String [] tblTypes = {"TABLE"};
			ResultSet rs = database.getTables(null, connection.getCatalog(), "%", tblTypes);
			while (rs.next()){
				tableNames.add(rs.getString("TABLE_NAME"));
			}
		}
		catch (SQLException sql){
			printMetaErrorMessage(sql,"getTables");
		}
		
		return tableNames;
	}
	
	/**
	 * gets descriptions (name and type) of the columns of the given table
	 * @param tableName
	 * @return
	 * @throws MetaStatementNotExecutedException
	 */
	public String[][] getColumnLabelsTypes(String tableName)throws MetaStatementNotExecutedException{
		String[][] result = null;
		try{
			Vector<String> columns = new Vector<String>();
			Vector<String> types = new Vector<String>();
			DatabaseMetaData database = connection.getMetaData();
			ResultSet rs = database.getColumns(null,null,tableName, "%");
				
			while(rs.next()){
				columns.add(tableName+"."+rs.getString("COLUMN_NAME"));
				types.add(rs.getString("TYPE_NAME"));
			}
			
			result = new String[2][columns.size()];
			columns.toArray(result[0]);
			types.toArray(result[1]);		
			
		}
		catch (SQLException sql){
			printMetaErrorMessage(sql,"getColumnLabelsTypes");
		}		
		return result;
	}
	
	/**
	 * gets the description of the given table - consisting of:
	 * columns: name, type, default value, is nullable and autoincrement
	 * @param tableName
	 * @return
	 * @throws MetaStatementNotExecutedException
	 */
	public Vector<String[]> getTableDescription(String tableName) throws MetaStatementNotExecutedException{
			
		Vector<String[]> result = new Vector<String[]>();
		try{
			DatabaseMetaData database = connection.getMetaData();
			ResultSet rs = database.getColumns(null,null,tableName, "%");
			String[] elements = new String[6];
			//rs.getString("IS_AUTOINCREMENT"); just works since younger database version 
			//or with extra patch
			String query = "Explain " + tableName + ";";
			ResultSet rsHelp = createStatement(query);
			String extra;
			while (rs.next()){
				rsHelp.next();
				elements = new String[6];
				elements[0]=rs.getString("COLUMN_NAME");
				elements[1]=rs.getString("TYPE_NAME");
				
				if(elements[1].equals("int")) {
					extra = rsHelp.getString("Type");
					elements[2]=extra.substring(4, extra.length()-1);
				}
				else if(elements[1].equals("enum")){
					extra = rsHelp.getString("Type");
					elements[2]=extra.substring(5, extra.length()-1);
				}
				else elements[2]=String.valueOf(rs.getInt("COLUMN_SIZE"));
								
				elements[3]=rs.getString("COLUMN_DEF");
				elements[4]=rs.getString("IS_NULLABLE");
				
				extra = rsHelp.getString("Extra");
				if(extra.contains("auto_increment")){
					elements[5]="YES";	
				}
				else elements[5]="NO";
				//elements[5]=rs.getString("IS_AUTOINCREMENT");
				result.add(elements);
			}	
		}
		catch (SQLException sql){
			printMetaErrorMessage(sql,"getTableDescription");
		}
		
		return result;
	}
	
	/**
	 * gets the primary key fields of the given table
	 * @param tableName
	 * @return
	 * @throws MetaStatementNotExecutedException
	 */
	public Vector<String> getPrimaryKeys(String tableName)throws MetaStatementNotExecutedException{
		Vector<String> result = new Vector<String>();
		try{
			DatabaseMetaData database = connection.getMetaData();
			ResultSet rs = database.getPrimaryKeys(null, connection.getCatalog(), tableName);
			while (rs.next()){
				result.add(rs.getString("COLUMN_NAME"));
			}
		}
		catch (SQLException sql){
			printMetaErrorMessage(sql,"getPrimaryKeys");
		}
		return result;
	}
	
	/**
	 * creates a new table with the given column attributes and primary key
	 * @param tableName
	 * @param columnNames
	 * @param types
	 * @param sizes
	 * @param nullable - column can be null
	 * @param defaultValues - column has a default value
	 * @param autoincrement - column is autoincrement
	 * @param primaryKey
	 * @throws StatementNotExecutedException
	 */
	public void createTable(String tableName, String[] columnNames, String[] types,
			String[] sizes, String[] nullable, String[] defaultValues, String[] autoincrement, 
			String[] primaryKey) throws StatementNotExecutedException{
		String query = "CREATE TABLE " + tableName + " (";
		for(int i=0; i<columnNames.length; i++){
			query= query + columnOptions(columnNames[i], types[i], sizes[i],
					nullable[i], defaultValues[i], autoincrement[i]);
			query = query + ", ";
		}
		query = query + "PRIMARY KEY (";
		for(int i=0; i<primaryKey.length; i++){
			query = query + primaryKey[i];
			if(!(i==primaryKey.length-1))  query = query + ", ";
			else query=query + ") ";
		}
		
		query = query + ");";
		
		executeStatement(query);
	}
	
	/**
	 * gets the description of the default value
	 * @param defaultValue
	 * @param type
	 * @return
	 */
	private String getDefaultValueString(String defaultValue, String type){
		if(defaultValue==null) return "";
		else if(defaultValue.equals("") && (type.equals("int") || type.equals("float"))) return "";
		else if (!defaultValue.equals("")) return "DEFAULT '" + defaultValue + "' ";
		else return "DEFAULT ''";
	}
	
	/**
	 * builds the query part for the column definition
	 * @param columnName
	 * @param type
	 * @param size
	 * @param nullable
	 * @param defaultValues
	 * @param autoincrement
	 * @return
	 */
	private String columnOptions(String columnName, String type, String size,
			String nullable, String defaultValues, String autoincrement){
		String query = columnName + " " + type + " (" + size + ") ";
		if(nullable.equals("NO")) query=query + "NOT NULL ";
		query = query + getDefaultValueString(defaultValues, type);
		if(autoincrement.equals("YES")) query = query + "AUTO_INCREMENT ";
		return query;
	}
	
	/**
	 * changes the definition of the given column
	 * @param tableName
	 * @param columnName
	 * @param type
	 * @param size
	 * @param nullable - can be null
	 * @param defaultValues - has a default value
	 * @param autoincrement - is autoincrement
	 * @throws StatementNotExecutedException
	 */
	public void changeColumn(String tableName, String columnName, String type, String size,
			String nullable, String defaultValues, String autoincrement) throws StatementNotExecutedException{
		String query = "ALTER TABLE " + tableName + " CHANGE " + columnName + " " + 
			columnOptions(columnName, type, size, nullable, defaultValues,  autoincrement) + ";";
		executeStatement(query);
	
	}
	
	/**
	 * adds a column to the given table
	 * @param tableName
	 * @param columnName
	 * @param type
	 * @param size
	 * @param nullable - can be null
	 * @param defaultValues - has a default value
	 * @param autoincrement - is autoincrement
	 * @throws StatementNotExecutedException
	 */
	public void addColumn(String tableName, String columnName, String type, String size,
			String nullable, String defaultValues, String autoincrement) throws StatementNotExecutedException{
		String query = "ALTER TABLE " + tableName + " ADD " + 
			columnOptions(columnName, type, size, nullable, defaultValues, autoincrement);
		executeStatement(query);
	}
	
	/**
	 * adds a primary key to the given table
	 * @param tableName
	 * @param columns - new primary key
	 * @throws StatementNotExecutedException
	 */
	public void addPrimaryKey(String tableName, String[] columns) throws StatementNotExecutedException{
		String query = "ALTER TABLE " + tableName + " ADD PRIMARY KEY (";
		for (int i=0; i<columns.length-1; i++){
			query=query + columns[i] + ", ";
		}
		query = query + columns[columns.length-1] + ");";
		executeStatement(query);
	}
	
	/**
	 * adapts primary key of given table
	 * @param tableName
	 * @param columns - new primary key
	 * @throws StatementNotExecutedException
	 */
	public void changePrimaryKey(String tableName, String[] columns)throws StatementNotExecutedException{
		String query = "ALTER TABLE " + tableName + " DROP PRIMARY KEY, " +
				"ADD PRIMARY KEY (";
		for (int i=0; i<columns.length-1; i++){
			query=query + columns[i] + ", ";
		}
		query = query + columns[columns.length-1] + ");";
		executeStatement(query);
	}

	private void printMetaErrorMessage(SQLException sql, String function) throws MetaStatementNotExecutedException{
		if (_log.isErrorEnabled()) {
			_log.error("ErrorCode "+sql.getErrorCode());
		}
		throw new MetaStatementNotExecutedException(function);
	}
}
