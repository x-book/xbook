package ossobook.sqlQueries;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import ossobook.exceptions.*;

/**
 * manages version code tables except of "tierart" and "skelteil" (makes inserts, deletes, updates and select statements
 * on the tables)
 * @author j.lamprecht
 *
 */
public class DefinitionManager extends TableManager {

	public DefinitionManager(Connection con, String databaseName){
		super(con, databaseName);
	}
	
	/**
	 * gets changed definition entries which have been changed
	 * @param tableName
	 * @param scheme columns of global database 
	 * -> needed to be independent from order and global database scheme changes
	 * @param lastSynchronization
	 * @param event - QueryManager.CHANGES_LOCAL, QueryManager.CHANGES_GLOBAL, QueryManager.ALL_GLOBAL
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getDefinitionData(String tableName, String[][] scheme, String lastSynchronization, int event)
		throws StatementNotExecutedException{
		String[] tableNames={tableName};
		String condition="";
		if(event==QueryManager.CHANGES_GLOBAL) condition=" WHERE Zustand>"+lastSynchronization;
		return this.getEntries(scheme, tableNames, condition);
	}
	
	/**
	 * get last time when table has been changed
	 * @param tableName
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getLastSynchronization(String tableName) throws StatementNotExecutedException{
		String query = "SELECT Zustand FROM "+tableName+" LIMIT 1";
		try{
			ResultSet rs = createStatement(query);
			if(rs.next()){
				return rs.getString(1);
			}
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return null;
	}
	
	public int updateDefinitionData(String[] scheme, String[] data, Vector<String> key, String tableName) throws StatementNotExecutedException{
		return updateData(scheme, data, databaseName+"."+tableName, key);
	}
	
	public void insertDefinitionData(String[] scheme, String[] data, String tableName) throws StatementNotExecutedException{
		insertData(scheme, data, tableName);
	}
	
	/**
	 * set Zustand to 'synchronisiert' after adapting local definition table
	 * @precondition executed on local database during synchronization
	 * @param tableName
	 * @param lastSynchronization
	 * @throws StatementNotExecutedException
	 */
	public void setLastSynchronizationForDefinitionTable(String tableName, String lastSynchronization)throws StatementNotExecutedException{
		String query = "UPDATE "+tableName+" SET Zustand="+lastSynchronization+";";
		try{
			executeStatement(query);
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
	/**
	 * definition table records definitely deleted from database
	 * @precondition method is executed on local database during synchronization
	 * @param tableName
	 * @throws StatementNotExecutedException
	 */
	public void deleteDefinitionPermanent(String tableName) throws StatementNotExecutedException{
		String query = "DELETE FROM " + tableName + " WHERE geloescht='Y'";
		try{
			executeStatement(query);
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
}
