package ossobook.sqlQueries;

import java.sql.*;

import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages skelteil table (makes inserts, updates and select statements
 * on the table)
 * @author j.lamprecht
 *
 */
public class SkeletonManager extends TableManager{

	public SkeletonManager(Connection con, String databaseName){
		super(con, databaseName);
	}
	
	public int getSkelLB(int skelcode)throws StatementNotExecutedException{
		String query = "SELECT skelLB FROM " + databaseName+".skelteil WHERE SkelCode="
			+ skelcode + " AND geloescht='N';";
		int skellabel = 0;
		try{
			ResultSet rs = createStatement(query);
			rs.next();
			skellabel = rs.getInt("skelLB");
		}
		catch(SQLException s){
			printErrorMessage(s, query);
		}
		return skellabel;
	}
}
