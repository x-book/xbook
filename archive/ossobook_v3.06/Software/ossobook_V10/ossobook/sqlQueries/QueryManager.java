package ossobook.sqlQueries;

import ossobook.client.base.metainfo.*;
import ossobook.exceptions.MetaStatementNotExecutedException;
import ossobook.exceptions.NoWriteRightException;
import ossobook.exceptions.StatementNotExecutedException;

import java.sql.*;
import java.util.*;

/**
 * QueryManager is the interface between the gui and the database. 
 * It takes requests of the gui and delegates them to the adequate 
 * TableManager, which makes the database query. 
 * @author j.lamprecht
 *
 */

public abstract class QueryManager {

	protected Connection connection;
	protected ProjectManager projectManager;
	protected DatabaseManager databaseManager;
	protected UserManager userManager;
	protected InputUnitManager inputUnit;
	protected MassManager massManager;
	protected ArtefactManager artefactManager;
	protected SpeciesManager speciesManager;
	protected SkeletonManager skeletonManager;
	protected SchemeManager schemeManager;
	protected DefinitionManager definitionManager;

	public static int ALL_GLOBAL=1;
	public static int CHANGES_LOCAL=2;
	public static int CHANGES_GLOBAL=3;
	public static int DELETE_ALL=1;
	public static int DELETE_DELETED=2;
	
	public static int ISOLATION_LEVEL = Connection.TRANSACTION_SERIALIZABLE;
	
	private String databaseName;
	
	/**
	 * initializes the managers to which the single queries will be delegated
	 */
	public void init(String databaseName){
		this.databaseName=databaseName;
		try{
			databaseManager = new DatabaseManager(connection, databaseName);
			int databaseNumber=databaseManager.getDatabaseNumber();
			userManager = new UserManager(connection, databaseName);
			inputUnit = new InputUnitManager(connection, databaseNumber, databaseName);
			massManager = new MassManager(connection, databaseNumber, databaseName);
			artefactManager = new ArtefactManager(connection, databaseNumber, databaseName);
			speciesManager = new SpeciesManager(connection, databaseName);
			skeletonManager = new SkeletonManager(connection, databaseName);
			projectManager = new ProjectManager(connection, userManager, databaseNumber, databaseName);
			schemeManager = new SchemeManager(connection, databaseName);
			definitionManager=new DefinitionManager(connection, databaseName);
		}
		catch(StatementNotExecutedException e){}
		
	}
	
	/**
	 * needed because of the dynamic determination of the kind of the QueryManager
	 * @return static host of GlobalQueryManager or LocalQueryManager
	 */
	public abstract String getUser();
	public abstract int getbenutzernr();
	
	
	//using table version and nachrichten
	public String getOssobookVersion() throws StatementNotExecutedException{
		return databaseManager.getOssobookVersion();
	}
	
	public String getSystemTime() throws StatementNotExecutedException{
		return databaseManager.getSystemTime();
	}
	
	public int getAssignedMessageNumber(int[] projectKey) throws StatementNotExecutedException {
		return databaseManager.getAssignedMessageNumber(projectKey);
	}

	/**
	 * assign next nachrichtennummer for project changes
	 * @param projectKey primary key of project
	 * @throws StatementNotExecutedException
	 */
	public void setMessageNumber(int[] projectKey) throws StatementNotExecutedException {
		int number = databaseManager.getMessageNumber();
		projectManager.setMessageNumber(number, projectKey);
		inputUnit.setMessageNumber(number, projectKey);
		massManager.setMessageNumber(number, projectKey);
		artefactManager.setMessageNumber(number, projectKey);
		databaseManager.increaseMessageNumber();
	}

	public boolean checkMessageNumber(int[] messageIdentifier, int[] projectKey)
			throws StatementNotExecutedException {
		return databaseManager.checkMessageNumber(messageIdentifier, projectKey);
	}

	/**
	 * memorize handled project changes
	 * @param messageIdentifier Nachrichtennummer and Datenbanknummer of local database
	 * @param projectKey primary Key of project
	 * @throws StatementNotExecutedException
	 */
	public void updateMessageNumber(int[] messageIdentifier, int[] projectKey)
			throws StatementNotExecutedException {
		if(databaseManager.updateMessageNumber(messageIdentifier, projectKey)<1)
			databaseManager.insertMessageNumber(messageIdentifier, projectKey);
	}

	public int getDatabaseNumber() throws StatementNotExecutedException{
		return databaseManager.getDatabaseNumber();
	}
	
	
	//using table projekt
	public Projekt[] holeProjekte() throws StatementNotExecutedException{
		return projectManager.holeProjekte();
	}
	
	public String[][] getProjektInformations(int[] projectKey) throws StatementNotExecutedException{
		return projectManager.getProjektInformations(projectKey);
	}
	
	/**
	 * create new project; on local database set right for project owner
	 * @param ProjektName
	 * @throws StatementNotExecutedException
	 */
	public void newProject(String ProjektName) throws StatementNotExecutedException{
		try{
			projectManager.newProject(ProjektName);
			if(this instanceof LocalQueryManager){
				userManager.changeRight(getProjectKey(ProjektName), null, UserManager.WRITE);
			}
		}
		catch(SQLException sql){
			sql.printStackTrace();
		}
	}
	
	/**
	 * delete all entries of project by changing "geloescht" field
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 * @throws NoWriteRightException
	 */
	public void deleteProject(int[] projectKey) throws StatementNotExecutedException, NoWriteRightException{
		// L\u00F6sche Projekt aus Projekttabelle und Rechte zu Projekt
		projectManager.deleteProject(projectKey);
		// L\u00F6sche Masse zu Projekt
		massManager.deleteMasse(projectKey);
		// L\u00F6sche Artefakte zu Projekt
		artefactManager.deleteArtefaktMaske(projectKey);
		// L\u00F6sche Knochen zu Projekt
		inputUnit.deleteUnit(projectKey);
	}
	
	/**
	 * delete all project entries definitely
	 * @precondition method is executed on local database during synchronization
	 * @param projectKey
	 * @param event
	 * @throws StatementNotExecutedException
	 */
	public void deleteProjectPermanent (int[] projectKey, int event) throws StatementNotExecutedException{
		// L\u00F6sche Projekt aus Projekttabelle und Rechte zu Projekt
		projectManager.deleteProjectPermanent(projectKey, event);
		// L\u00F6sche Masse zu Projekt
		massManager.deleteMassePermanent(projectKey, event);
		// L\u00F6sche Artefakte zu Projekt
		artefactManager.deleteArtefaktPermanent(projectKey, event);
		// L\u00F6sche Knochen zu Projekt
		inputUnit.deleteUnitPermanent(projectKey, event);
	}
	
	public void updateProjekt(String label, String text, int[] projectKey) throws StatementNotExecutedException{
		projectManager.updateProjekt(label, text, projectKey);
	}
	
	public int[] getProjectKey(String projName) throws StatementNotExecutedException{
		return projectManager.getProjectKey(projName);
	}
	
	public boolean isProjectOwner(int[] projectKey) throws StatementNotExecutedException{
		return projectManager.isProjectOwner(projectKey);
	}
	
	public String getProjectOwner(int[] projectKey) throws StatementNotExecutedException{
		return projectManager.getProjectOwner(projectKey);
	}
	
	/**
	 * get global project entries for all project tables - including column names
	 * @param projectKey
	 * @param event
	 * @param lastSynchronisation
	 * @return Vector<String[][]>
	 * 			'- tables in order projekt, eingabeeinheit, masse, artefaktmaske, projektrechte (and system time)
	 * 						'- single data records
	 * 						  '- single column content
	 * @throws StatementNotExecutedException
	 * @throws MetaStatementNotExecutedException
	 */
	public Vector<String[][]> getGlobalProjectData(int[] projectKey, int event, String lastSynchronisation) 
	throws StatementNotExecutedException,MetaStatementNotExecutedException{
		Vector<String[][]> result = new Vector<String[][]>();
		
		try{		
			//get project information
			String[][] projectScheme = schemeManager.getColumnLabelsTypes("projekt");
			//get entries in eingabeeinheit to project
			String[][] inputScheme = schemeManager.getColumnLabelsTypes("eingabeeinheit");
			//get entries in masse to project
			String[][] massScheme = schemeManager.getColumnLabelsTypes("masse");
			//get entries in artefaktmaske to project
			String[][] artefactScheme = schemeManager.getColumnLabelsTypes("artefaktmaske");
			
			result = getProjectEntries(projectKey, projectScheme, inputScheme, massScheme, artefactScheme, event, lastSynchronisation);
			
			//get entries in benutzer to project
			String[][] rightScheme = schemeManager.getColumnLabelsTypes("projektrechte");
			Vector<String[]> rightData = projectManager.getRightEntries(projectKey, rightScheme);
			String[][] right = format(rightScheme, rightData);	
			
			String databaseTime = getSystemTime();
			String[][] time = new String[1][1];
			time[0][0] = databaseTime;
			
			result.add(right);
			result.add(time);
		}
		catch(SQLException s){
			s.printStackTrace();
		}
		return result;
	}
	
	/**
	 * get project entries for all project tables - including column names
	 * @param projectKey
	 * @param projectScheme scheme of table projekt (global)
	 * @param inputScheme scheme of table eingabeeinheit (global)
	 * @param massScheme scheme of table masse (global)
	 * @param artefactScheme scheme of table artefaktmaske (global)
	 * @param event: CHANGES_GLOBAL only changes of global project
	 * 				 CHANGES_LOCAL only changes of local project
	 * 				 ALL_GLOBAL all entries of global project
	 * 
	 * @param lastSynchronisation
	 * @return Vector<String[][]>
	 * 			'- different tables projekt, eingabeeinheit, masse, artefaktmaske
	 * 						 '- different data records
	 * 						   '- diefferent column content
	 * @throws StatementNotExecutedException
	 */
	private Vector<String[][]> getProjectEntries(int[] projectKey, String[][] projectScheme,
			String[][] inputScheme, String[][] massScheme, String[][] artefactScheme,
			int event, String lastSynchronisation) throws StatementNotExecutedException{
		Vector<String[][]> result = new Vector<String[][]>();
		
		Vector<String[]> projectData = projectManager.getProjectEntries(projectKey,projectScheme, event, lastSynchronisation);			
		String[][] project = format(projectScheme, projectData);
		Vector<String[]> inputData = inputUnit.getEntries(projectKey,inputScheme, event, lastSynchronisation);
		String[][] input = format(inputScheme, inputData);
		Vector<String[]> massData = massManager.getEntries(projectKey, massScheme, event, lastSynchronisation);
		String[][] masse = format(massScheme, massData);
		Vector<String[]> artefactData = artefactManager.getEntries(projectKey, artefactScheme, event, lastSynchronisation);
		String[][] artefact = format(artefactScheme, artefactData);
		
		result.add(project);
		result.add(input);
		result.add(masse);
		result.add(artefact);
		
		return result;
	}
	
	/**
	 * merge scheme and data
	 * @param scheme
	 * @param data
	 * @return
	 */
	private String[][] format(String[][] scheme, Vector<String[]> data){
		String[][] result = new String[data.size()+1][scheme[0].length];
		for(int j=0; j<scheme[0].length; j++){
			result[0][j] = scheme[0][j];
		}
		for(int i=0; i<data.size(); i++){
			for(int j=0; j<scheme[0].length; j++){
				result[i+1][j] = data.elementAt(i)[j];
			}
		}
		return result;
	}
	
	/**
	 * get all changes of local database
	 * used on client
	 * @param projectKey
	 * @param scheme
	 * @return
	 */
	public Vector<String[][]> getChangesOfProject(int[] projectKey, Vector<String[][]> scheme){
		Vector<String[][]> result = new Vector<String[][]>();
		try{
			result = getProjectEntries(projectKey, scheme.elementAt(0), scheme.elementAt(1),
					scheme.elementAt(2), scheme.elementAt(3), CHANGES_LOCAL, null);
		}
		catch(SQLException s){
			s.printStackTrace();
		}
		return result;
	}
	
	/**
	 * insert changes
	 * @precondition no conflicts can occur
	 * @param data
	 * @param projectKey
	 */
	public void insertData(Vector<String[][]> data, int[] projectKey){
		try{			
			for(int j=1; j<data.elementAt(0).length; j++){			
				projectManager.insertProjectData(data.elementAt(0)[0], data.elementAt(0)[j]);
			}
			for(int j=1; j<data.elementAt(1).length; j++){
				inputUnit.insertData(data.elementAt(1)[0], data.elementAt(1)[j]);
			}
			for(int j=1; j<data.elementAt(2).length; j++){
				massManager.insertData(data.elementAt(2)[0], data.elementAt(2)[j]);
			}
			for(int j=1; j<data.elementAt(3).length; j++){
				artefactManager.insertData(data.elementAt(3)[0], data.elementAt(3)[j]);
			}
			
			for(int j=1; j<data.elementAt(4).length; j++){
				projectManager.insertRightData(data.elementAt(4)[0], data.elementAt(4)[j]);
			}
			
			projectManager.setLastSynchronizeTime(data.elementAt(5)[0][0], projectKey);
		}
		catch(SQLException s){
			s.printStackTrace();
		}
	} 
	
	/**
	 * first tries to update existing record, then (at fault) inserts as new record
	 * assumes checking for conflicts 
	 * @param data
	 * @param lastSynchronization 
	 * 			- null for changing local database
	 * @return Vector<Vector<String[]>>:
	 * 			'- different tables: projekt, eingabeeinheit, masse, artefaktmaske
	 * 					'- different conflict records in single table
	 * 							'- single data records (always even number: first old record, second new record)
	 * @throws StatementNotExecutedException
	 * @throws MetaStatementNotExecutedException
	 */
	public Vector<Vector<String[]>> changeData(Vector<String[][]> data, String lastSynchronization) throws StatementNotExecutedException, MetaStatementNotExecutedException{
				
		Vector<String> projectKey= this.getPrimaryKeys("projekt");
		Vector<String> unitKey= this.getPrimaryKeys("eingabeeinheit");
		Vector<String> artefactKey= this.getPrimaryKeys("artefaktmaske");
		Vector<String> massKey= this.getPrimaryKeys("masse");			
			
		Vector<Vector<String[]>> conflict = new Vector<Vector<String[]>>();
		Vector<String[]> projectConflicts = new Vector<String[]>();
		Vector<String[]> unitConflicts = new Vector<String[]>();
		Vector<String[]> massConflicts = new Vector<String[]>();
		Vector<String[]> artefactConflicts = new Vector<String[]>();
		
		try{
			for(int j=1; j<data.elementAt(0).length; j++){	
				//conflict version only for changes in global database
				if(lastSynchronization!=null){
					int[] projectKeyValues=this.getKeyValues(projectKey, data.elementAt(0)[j], data.elementAt(0)[0]);
					//conflict?
					String lastChange=projectManager.getLastChange(projectKey, projectKeyValues);
					if(lastChange!=null && lastChange.compareTo(lastSynchronization)>0){
						projectConflicts.add((projectManager.getProjectEntry(data.elementAt(0), projectKeyValues, projectKey)).elementAt(0));
						projectConflicts.add(data.elementAt(0)[j]);
					}
				}
				if(projectManager.updateProjectData(data.elementAt(0)[0], data.elementAt(0)[j], projectKey)<1)
					projectManager.insertProjectData(data.elementAt(0)[0], data.elementAt(0)[j]);
			}
			conflict.add(projectConflicts);
		
			for(int j=1; j<data.elementAt(1).length; j++){
				//conflict management only for changes in global database
				if(lastSynchronization!=null){
					int[] unitKeyValues=this.getKeyValues(unitKey, data.elementAt(1)[j], data.elementAt(1)[0]);
					//conflict?
					String lastChange=inputUnit.getLastChange(unitKey, unitKeyValues);
					if(lastChange!=null && lastChange.compareTo(lastSynchronization)>0){
						unitConflicts.add((inputUnit.getUnitEntry(data.elementAt(1), unitKeyValues, unitKey)).elementAt(0));
						unitConflicts.add(data.elementAt(1)[j]);
					}
				}
				if(inputUnit.updateData(data.elementAt(1)[0], data.elementAt(1)[j], unitKey)<1)
					inputUnit.insertData(data.elementAt(1)[0], data.elementAt(1)[j]);
			}
			conflict.add(unitConflicts);
		
			for(int j=1; j<data.elementAt(2).length; j++){
				//conflict version only for changes in global database
				if(lastSynchronization!=null){
					int[] massKeyValues=this.getKeyValues(massKey, data.elementAt(2)[j], data.elementAt(2)[0]);
					//conflict?
					String lastChange=massManager.getLastChange(massKey, massKeyValues);
					if(lastChange!=null && lastChange.compareTo(lastSynchronization)>0){
						massConflicts.add((massManager.getMassEntry(data.elementAt(2), massKeyValues, massKey)).elementAt(0));
						massConflicts.add(data.elementAt(2)[j]);
					}
				}
				if(massManager.updateData(data.elementAt(2)[0], data.elementAt(2)[j], massKey)<1)
					massManager.insertData(data.elementAt(2)[0], data.elementAt(2)[j]);
			}
			conflict.add(massConflicts);
		
			for(int j=1; j<data.elementAt(3).length; j++){
				//conflict version only for changes in global database
				if(lastSynchronization!=null){
					int[] artefactKeyValues=this.getKeyValues(artefactKey, data.elementAt(3)[j], data.elementAt(3)[0]);
					//conflict?
					String lastChange=artefactManager.getLastChange(artefactKey, artefactKeyValues);
					if(lastChange!=null && lastChange.compareTo(lastSynchronization)>0){
						artefactConflicts.add((artefactManager.getArtefactEntry(data.elementAt(3), artefactKeyValues, artefactKey)).elementAt(0));
						artefactConflicts.add(data.elementAt(3)[j]);
					}
				}
				if(artefactManager.updateData(data.elementAt(3)[0], data.elementAt(3)[j], artefactKey)<1)
					artefactManager.insertData(data.elementAt(3)[0], data.elementAt(3)[j]);
			}
			conflict.add(artefactConflicts);
		}
		catch(SQLException s){
			s.printStackTrace();
		}
		return conflict;
	}
	
	/**
	 * get values for given primary key fields
	 * @param keys primary key fields
	 * @param data
	 * @param scheme
	 * @return
	 */
	private int[] getKeyValues(Vector<String> keys, String[] data, String[] scheme){
		Vector<String> keyValues = new Vector<String>();
		for(int i=0; i<keys.size(); i++){
			for(int j=0; j<scheme.length; j++){
				if(scheme[j].substring(scheme[j].indexOf(".")+1).equals(keys.elementAt(i))) keyValues.add(data[j]);
			}
		}
		int[] result = new int[keyValues.size()];
		for(int i=0; i<keyValues.size(); i++){
			result[i]=Integer.parseInt(keyValues.elementAt(i));
		}
		return result;
	}
	
	public void setLastSynchronizeTime(String time, int[] projectKey)
			throws StatementNotExecutedException {
		projectManager.setLastSynchronizeTime(time, projectKey);
	}

	/**
	 * 
	 * @return: projects the user can work with
	 * @throws StatementNotExecutedException
	 */
	public Vector<Projekt> getProjects() throws StatementNotExecutedException{
		return projectManager.getProjects();
	}
		
	/**
	 * change status for entries of tables projekt, eingeabeeinheit, masse and artefaktmaske 
	 * belonging to project
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 */
	public void setSynchronized(int[] projectKey)  throws StatementNotExecutedException{
		projectManager.setSynchronized(projectKey);
		inputUnit.setSynchronized(projectKey);
		massManager.setSynchronized(projectKey);
		artefactManager.setSynchronized(projectKey);
	}
	 
	public String getProjectName(int[] projectKey)  throws StatementNotExecutedException{
		return projectManager.getProjectName(projectKey);
	}
		
	public String getLastSynchronizationOfProject(int[] projectKey)
			throws StatementNotExecutedException {
		return projectManager.getLastSynchronization(projectKey);
	}
	
	public boolean projectExists(int[] projectKey) throws StatementNotExecutedException{
		return projectManager.projectExists(projectKey);
	}

	
	//using table benutzer
	public int getBenutzernummer(String user) throws StatementNotExecutedException{
		return userManager.getBenutzernummer(user);
	}
	
	public Vector<String> getUserNames() throws StatementNotExecutedException{
		return userManager.getUserNames();
	}
	
	public void changeRight(int[] projectKey, String user, int right)
	throws StatementNotExecutedException{
		userManager.changeRight(projectKey, user, right);
	}
	
	public boolean getIsAdmin() throws StatementNotExecutedException{
		return userManager.getIsAdmin();
	}
	
	public String getRight(int[] projectKey) throws StatementNotExecutedException{
		return userManager.getRight(projectKey);
	}
	
	public String getMail(int[] projectKey) throws StatementNotExecutedException{
		return userManager.getMail(projectKey);
	}
	
		
	//using table eingabeeinheit
	public int getCountFk(String text, int[] projectKey) throws StatementNotExecutedException{
		return inputUnit.getCountFk(text, projectKey);
	}
	
	public int getCountInventar(int number, int[] projectKey) throws StatementNotExecutedException{
		return inputUnit.getCountInventar(number, projectKey);
	}
	
	public int getCountSkelNr(int number, int[] projectKey) throws StatementNotExecutedException{
		return inputUnit.getCountSkelNr(number, projectKey);
	}
	
	public int getCountKnIndNr(int number, int[] projectKey) throws StatementNotExecutedException{
		return inputUnit.getCountKnIndNr(number, projectKey);
	}
		
	public String getPhaseValue(String phase, int[] recordKey) throws StatementNotExecutedException{
		return inputUnit.getPhaseValue(phase, recordKey);
	}
		
	public int getBonesNumber(int[] projectKey)throws StatementNotExecutedException{
		return inputUnit.getBonesNumber(projectKey);
	}
	
	public double getOverallWeight(int[] projectKey)throws StatementNotExecutedException{
		return inputUnit.getOverallWeight(projectKey);
	}
	
	public String[][] getDataRecord(int[] recordKey) throws StatementNotExecutedException{
		return inputUnit.getDataRecord(recordKey);
	}
		
	public String[][] getFK(String phasenlabel, int[] projectKey) throws StatementNotExecutedException{
		return inputUnit.getFK(phasenlabel, projectKey);
	}
	
	public void insertIntoEingabeeinheit(int[] projectKey,Vector<String> fields,
			int[] ArtefaktID, int[] massID, String phase1, String phase2,
			String phase3, String phase4, Vector<String> values) throws StatementNotExecutedException{
	
		inputUnit.insertIntoEingabeeinheit(projectKey,fields, 
				ArtefaktID, massID, phase1, phase2, phase3, phase4, values);
	}
		
	public void deleteFromEingabeeinheit(int[] recordID)throws StatementNotExecutedException{
		inputUnit.deleteFromEingabeeinheit(recordID);
	}
	
	public void update(String phasenlabel, String content, String label, int[] projectKey)
		throws StatementNotExecutedException{
		inputUnit.update(phasenlabel, content, label, projectKey);
	}
	
	public void updateEingabeeinheit(int[] recordKey,int[] projectKey,Vector<String> fields,
		int[] ArtefactKey, int[] massKey, String phase1, String phase2,
		String phase3, String phase4, Vector<String> values) throws StatementNotExecutedException{
		inputUnit.updateEingabeeinheit(recordKey,projectKey,fields, ArtefactKey, massKey, phase1, phase2, phase3, phase4, values);
	} 
	
	
	//using tables masse and massabhaengigkeit
	public int[] insertIntoMasse(Vector<String> fieldsMasse,
			Vector<String> valuesMasse) throws StatementNotExecutedException{	
		return massManager.insertIntoMasse(fieldsMasse, valuesMasse);
	}
	
	public String holeMassName(String s) throws StatementNotExecutedException{
		return massManager.holeMassName(s);
	}
	
	public String getMasse(int tierlabel, int skellabel) throws StatementNotExecutedException{
		return massManager.getMasse(tierlabel, skellabel);
	}
	
	public String holeMassWert(String mass, int[] massKey) throws StatementNotExecutedException{
		return massManager.holeMassWert(mass, massKey);
	}
	
	public void deleteFromMasse(int[] massID)throws StatementNotExecutedException{
		massManager.deleteFromMasse(massID);
	}
	
	public void updateMasse(Vector<String> fieldsMasse,int[] massKey, Vector<String> valuesMasse)	throws StatementNotExecutedException{
		massManager.updateMasse(fieldsMasse, massKey, valuesMasse);
	}
	
	public int[] getMassId(int[] recordKey) throws StatementNotExecutedException{
		return inputUnit.getMassId(recordKey);
	}
	 
	//using table artefaktmaske
	public int[] insertIntoArtefaktmaske(Vector<String> fieldsA,
			Vector<String> valuesA) throws StatementNotExecutedException{
		return artefactManager.insertIntoArtefaktmaske(fieldsA, valuesA);
	}
	
	public void deleteFromArtefaktmaske(int[] artefactID)throws StatementNotExecutedException{
		artefactManager.deleteFromArtefaktmaske(artefactID);
	}
	
	public String[][] getArtefaktmaske(int[] artefactKey) throws StatementNotExecutedException{
		return artefactManager.getArtefaktmaske(artefactKey);
	}
	
	public void updateArtefaktmaske(Vector<String> fieldsA, int[] artefactKey,
			Vector<String> valuesA) throws StatementNotExecutedException{
		artefactManager.updateArtefaktmaske(fieldsA, artefactKey, valuesA);
	}
	
	public int[] getArtefaktId(int[] recordKey) throws StatementNotExecutedException{
		return artefactManager.getArtefaktKey(recordKey);
	}
	
	
	//using table tierart
	public void updateTierart(String TierCode, String TierName, String DTier, 
			String TierFolgeAuswertung, String LB){
		speciesManager.updateTierart(TierCode, TierName, DTier, TierFolgeAuswertung, LB);
	}
			
	public int getTierlabel(int tiercode)throws StatementNotExecutedException{
		return speciesManager.getTierlabel(tiercode);
	}
	
	
	//using table skelteil
	public int getSkelLB(int skelcode)throws StatementNotExecutedException{
		return skeletonManager.getSkelLB(skelcode);
	}
			
	
	//working with database scheme
	public Vector<String> getTables() throws MetaStatementNotExecutedException{
		return schemeManager.getTables();
	}
	
	public Vector<String[]> getTableDescription(String tableName)  throws MetaStatementNotExecutedException{
		return schemeManager.getTableDescription(tableName);
	}
	
	public Vector<String> getPrimaryKeys(String tableName)
			throws MetaStatementNotExecutedException {
		return schemeManager.getPrimaryKeys(tableName);
	}
	
	public void createTable(String tableName, String[] columnNames,
			String[] types, String[] sizes, String[] nullable, String[] defaultValues,
			String[] autoincrement, String[] primaryKey) 
			throws StatementNotExecutedException{
		schemeManager.createTable(tableName, columnNames, types, sizes,
				nullable, defaultValues, autoincrement, primaryKey);
	}
	
	public void changeColumn(String tableName, String columnName, String type,
			String size, String nullable, String defaultValues,
			String autoincrement) throws StatementNotExecutedException {
		schemeManager.changeColumn(tableName, columnName, type, size, nullable,
				defaultValues, autoincrement);
	}

	public void addColumn(String tableName, String columnName, String type,
			String size, String nullable, String defaultValues,
			String autoincrement) throws StatementNotExecutedException {
		schemeManager.addColumn(tableName, columnName, type, size, nullable,
				defaultValues, autoincrement);
	}

	public void addPrimaryKey(String tableName, String[] columns)
			throws StatementNotExecutedException {
		schemeManager.addPrimaryKey(tableName, columns);
	}

	public void changePrimaryKey(String tableName, String[] columns)
			throws StatementNotExecutedException {
		schemeManager.changePrimaryKey(tableName, columns);
	}
	
	
	
	//definition tables
	/**
	 * get definition entries - all or only changes 
	 * @param tableName
	 * @param lastSynchronization
	 * @param event CHANGES_GLOBAL: just changes of definition table
	 * 				CHANGES_ALL: every definition table record
	 * @return Vector<String[]>
	 * 			'- data records; last entry contains system time
	 * 						 '- column content
	 * @throws StatementNotExecutedException
	 * @throws MetaStatementNotExecutedException
	 */
	public Vector<String[]> getDefinitionData(String tableName,
			String lastSynchronization, int event) throws StatementNotExecutedException, MetaStatementNotExecutedException {
		String[][] scheme=schemeManager.getColumnLabelsTypes(tableName);
		Vector<String[]> result=definitionManager.getDefinitionData(tableName, scheme, lastSynchronization, event);			
		String[] time = new String[1];
		time[0]=getSystemTime();
		result.add(time);
		return result;
	}
	
	public String getLastSynchronizationOfDefinitionTable(String tableName)
			throws StatementNotExecutedException {
		return definitionManager.getLastSynchronization(tableName);
	}
	
	/**
	 * insert changes of definition tables
	 * @precondition user may change definition tables
	 * @param tableName
	 * @param data
	 * @param scheme
	 * @throws MetaStatementNotExecutedException
	 * @throws StatementNotExecutedException
	 */
	public void changeDefinitions(String tableName, Vector<String[]> data, 
			String[] scheme) throws MetaStatementNotExecutedException, StatementNotExecutedException{
		Vector<String> key = this.getPrimaryKeys(tableName);
		for(int i=0; i<scheme.length; i++){
			scheme[i]=tableName+"."+scheme[i];
		}
		for(int i=0; i<data.size()-1; i++){
			int updated=definitionManager.updateDefinitionData(scheme, data.elementAt(i), key, tableName);
			if(updated<1) definitionManager.insertDefinitionData(scheme, data.elementAt(i), tableName);
		}
		definitionManager.setLastSynchronizationForDefinitionTable(tableName, data.lastElement()[0]);
	}
	
	public void deleteDefinitionPermanent(String tableName)
			throws StatementNotExecutedException {
		definitionManager.deleteDefinitionPermanent(tableName);
	}

	
	/**
	 * close connection to database
	 */
	public void closeConnection(){
		try{
			connection.close();
		}
		catch (SQLException sql){
			sql.printStackTrace();
		}
	}
	
	public void disableAutoCommit(){
		try{
			connection.setAutoCommit(false);
		}
		catch(SQLException s){
			s.printStackTrace();
		}
	}
	
	public void enableAutoCommit(){
		try{
			connection.commit();
			connection.setAutoCommit(true);
		}
		catch(SQLException s){
			s.printStackTrace();
		}
	}
	
	public Connection getConnection(){
		return connection;
	}
	
	public void rollback(){
		try{
			connection.rollback();
			connection.setAutoCommit(true);
		}
		catch(SQLException s){
			s.printStackTrace();
		}
	}
	
	public int setIsolationLevel(int isolation){
		try{
			int level=connection.getTransactionIsolation();
			connection.setTransactionIsolation(isolation);
			return level;
		}
		catch(SQLException s){
			s.printStackTrace();
		}
		return -1;
	}
	
	
	public void lockDatabase() throws MetaStatementNotExecutedException, StatementNotExecutedException{
		Vector<String> tables=getTables();
		databaseManager.lockDatabase(databaseName, tables);
	}
	
	public void unlockDatabase() throws MetaStatementNotExecutedException, StatementNotExecutedException{
		databaseManager.unlockTables();
	}
	
}
