package ossobook.sqlQueries;

import java.sql.*;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages version table and nachrichten table (makes inserts, deletes, updates and select statements
 * on the tables)
 * @author j.lamprecht
 *
 */

public class DatabaseManager extends TableManager{
	private static Log _log = LogFactory.getLog(DatabaseManager.class);
	public DatabaseManager(Connection con, String databaseName){
		super(con, databaseName);
	}
	
	//get DB-Version from DB
	public String getOssobookVersion() throws StatementNotExecutedException{
		String dbversion="";
		ResultSet rs=null;
		String query="SELECT ossobookversion, dbversion FROM " + databaseName+".version;";
		try {
			if (_log.isDebugEnabled()) {
				_log.debug("Query: " + query);
				}
			rs=createStatement(query);
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
		if(rs!=null){
			try {
				rs.next();
				dbversion=rs.getString(2);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return dbversion;	
	}
	
	/**
	 * get number of local database
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getDatabaseNumber() throws StatementNotExecutedException{
		String query = "SELECT Datenbanknummer FROM " + databaseName+".version;";
		int result = -1;
		try{
			ResultSet rs = createStatement(query);
			if(rs.next()){
				result= rs.getInt(1);
			}
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return result;
	}
	
	/**
	 * get next nachrichtennummer to assign
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getMessageNumber() throws StatementNotExecutedException{
		String query = "SELECT Nachrichtennummer FROM " + databaseName+".version;";
		int result = -1;
		try{
			ResultSet rs = createStatement(query);
			if(rs.next()){
				result= rs.getInt(1);
			}
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return result;
	}
	
	/**
	 * get next assigned nachrichtennummer for project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getAssignedMessageNumber(int[] projectKey) throws StatementNotExecutedException{
		String query="";
		try{
			query="SELECT Nachrichtennummer FROM " + databaseName+".projekt WHERE" +
				" ProjNr="+projectKey[0] +" AND Datenbanknummer=" + projectKey[1] +" AND Nachrichtennummer!=-1;";	
			ResultSet rs = createStatement(query);
			if(rs.next()){
				return rs.getInt(1);
			}
			query="SELECT Nachrichtennummer FROM " + databaseName+".eingabeeinheit WHERE" +
			" ProjNr="+projectKey[0] +" AND DBNummerProjekt=" + projectKey[1] +" AND Nachrichtennummer!=-1;";	
			rs = createStatement(query);
			if(rs.next()){
				return rs.getInt(1);
			}
			query="SELECT masse.Nachrichtennummer FROM " + databaseName+".eingabeeinheit, " + databaseName+".masse WHERE" +
			" ProjNr="+projectKey[0] +" AND DBNummerProjekt=" + projectKey[1] +
			" AND eingabeeinheit.massID=masse.massID AND DBNummerMasse=masse.Datenbanknummer AND masse.Nachrichtennummer!=-1;";	
			rs = createStatement(query);
			if(rs.next()){
				return rs.getInt(1);
			}
			query="SELECT artefaktmaske.Nachrichtennummer FROM " + databaseName+".eingabeeinheit, " + databaseName+".artefaktmaske WHERE" +
			" ProjNr="+projectKey[0] +" AND DBNummerProjekt=" + projectKey[1] +
			" AND eingabeeinheit.artefaktID=artefaktmaske.ID AND DBNummerArtefakt=artefaktmaske.Datenbanknummer AND artefaktmaske.Nachrichtennummer!=-1;";	
			rs = createStatement(query);
			if(rs.next()){
				return rs.getInt(1);
			}
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return -1;
	}
	
	/**
	 * check, if project changes have already been handled by server
	 * @param messageIdentifier  Nachrichtennummer and Datenbanknummer of local database
	 * @param projectIdentifier primary key of project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public boolean checkMessageNumber(int[] messageIdentifier, int[] projectIdentifier) throws StatementNotExecutedException{
		String query = "SELECT nachrichten.Nachrichtennummer<" + messageIdentifier[0] 
		    + " FROM " + databaseName+".nachrichten WHERE nachrichten.Datenbanknummer = " + messageIdentifier[1]
		    + " AND nachrichten.ProjNr=" + projectIdentifier[0] + " AND nachrichten.DBNummerProjekt=" + projectIdentifier[1]+";"; 
		try{
			ResultSet rs = createStatement(query);
			if(rs.next()){
				return rs.getBoolean(1);
			}
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
		//no entry for database number => message not handled yet
		return true;
	}
	
	/**
	 * memorize next nachrichtennummer
	 * @throws StatementNotExecutedException
	 */
	public void increaseMessageNumber() throws StatementNotExecutedException{
		String query="UPDATE " + databaseName+".version SET Nachrichtennummer=Nachrichtennummer+1;";
		try{
			executeStatement(query);
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
	/**
	 * part 1 of memorizing handled project changes of client 
	 * @param messageIdentifier
	 * @param projectKey
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int updateMessageNumber(int[] messageIdentifier, int[] projectKey) throws StatementNotExecutedException{
		String query="UPDATE " + databaseName+".nachrichten SET Nachrichtennummer=" +
			messageIdentifier[0] +" WHERE Datenbanknummer=" + messageIdentifier[1]+
			" AND nachrichten.ProjNr="+projectKey[0] + " AND nachrichten.DBNummerProjekt=" + projectKey[1] +";";
		try{
			return executeUpdate(query);
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return -1;
	}
	
	/**
	 * part 2 of memorizing handled project changes of client
	 * @param messageIdentifier
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 */
	public void insertMessageNumber(int[] messageIdentifier, int[] projectKey) throws StatementNotExecutedException{
		String query="INSERT INTO " + databaseName+".nachrichten (Datenbanknummer, ProjNr, DBNummerProjekt, Nachrichtennummer) VALUES (" +
			+ messageIdentifier[1] + "," + projectKey[0]+","+projectKey[1]+","+messageIdentifier[0]+");";
		try{
			executeUpdate(query);
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
	/**
	 * get current time
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getSystemTime() throws StatementNotExecutedException{
		String query = "SELECT Now()+0";
		String result=null;
		try{
			ResultSet rs = createStatement(query);
			if(rs.next()){
				result=rs.getString(1);
			}
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return result;
	}
	
	/**
	 * lock given tables of given database
	 * @param databaseName
	 * @param tables
	 * @throws StatementNotExecutedException
	 */
	public void lockDatabase(String databaseName, Vector<String> tables) throws StatementNotExecutedException{
		String query = "LOCK TABLES ";
		for(int i=0; i<tables.size(); i++){
			query = query + databaseName+"."+tables.elementAt(i) + " WRITE";
			if(i<tables.size()-1) query = query + ", ";
		}
		try{
			executeStatement(query);
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}		
	}
	
	/**
	 * unlock database
	 * @throws StatementNotExecutedException
	 */
	public void unlockTables() throws StatementNotExecutedException{
		String query = "UNLOCK TABLES;";
		try{
			executeStatement(query);
		}
		catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
}
