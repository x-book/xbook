package ossobook.sqlQueries;

import java.sql.*;
import java.util.*;

import ossobook.client.base.metainfo.Projekt;
import ossobook.exceptions.NoWriteRightException;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages projekt table (makes inserts, deletes, updates and select statements
 * on the table)
 * @author j.lamprecht
 *
 */
public class ProjectManager extends TableManager{
	
	UserManager user;
	int versionDatabaseNumber;
	
	public ProjectManager(Connection con, UserManager user, int databaseNumber, String databaseName){
		super(con, databaseName);
		this.user=user;
		this.versionDatabaseNumber=databaseNumber;
	}
	
	/**
	 * check project already exists in database
	 * @param projectKey
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public boolean projectExists(int[] projectKey) throws StatementNotExecutedException{
		String query="SELECT count(*) FROM " + databaseName+".projekt WHERE projNr=" + projectKey[0] 
		       + " AND Datenbanknummer=" + projectKey[1] + ";";
		ResultSet rs = null;
		try{
			rs = createStatement(query);
			if (rs.next()) {
				int count = rs.getInt(1);
				if(count<=0) return false;
			}
		}
		catch (SQLException s){
			printErrorMessage(s, query);
		}
		return true;
	}
		
	/**
	 * get all projects for which user has at least read right
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<Projekt> getProjects() throws StatementNotExecutedException{
		Vector<Projekt> plist = new Vector<Projekt>();
		ResultSet rs = null;
		String query="";
		if (user.getIsAdmin())
			query="SELECT ProjNr, ProjName, Datenbanknummer FROM " + databaseName+".projekt ORDER BY ProjName" +
					" WHERE geloescht='N';";
		else query = "SELECT ProjNr, ProjName, Datenbanknummer FROM projekt WHERE geloescht='N' AND EXISTS " + 
			"(SELECT * FROM projektrechte WHERE Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1) " +
			"AND projekt.ProjNr=projektrechte.ProjNr AND projekt.Datenbanknummer=projektrechte.DBNummerProjekt)" +
			" ORDER BY ProjName;";
		
		try{
			rs=createStatement(query);
			while(rs.next()){
				plist.add(new Projekt(rs.getString(2), rs.getInt(1), rs.getInt(3)));
			}
		}
		catch (SQLException s){
			printErrorMessage(s, query);
		}
			
		return plist;
	}
	
	/**
	 * get all projects for which user has at least read right
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Projekt[] holeProjekte() throws StatementNotExecutedException{		
		Projekt[] plist = new Projekt[0];	
		int counterinfo=getCounterinfo();
		plist = getProjects(counterinfo);			
		return plist;
	}
	
	/**
	 * count projects for which user has at least read right
	 * @return
	 * @throws StatementNotExecutedException
	 */
	private int getCounterinfo() throws StatementNotExecutedException{
		int i=0;
		String query="";
		try{
			//admin is allowed to work with every project 
			if(user.getIsAdmin()){
				query="SELECT COUNT(ProjNr)AS counterinfo FROM " + databaseName+".projekt WHERE geloescht='N';";
			}
			//user may at least read project
			else{
				query="SELECT COUNT(ProjNr)AS counterinfo FROM projekt WHERE geloescht='N' AND EXISTS" +
					"(SELECT * FROM projektrechte WHERE Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1)" +
					"AND projekt.ProjNr=projektrechte.ProjNr AND projekt.Datenbanknummer=projektrechte.DBNummerProjekt " +
					"AND geloescht='N');";
			}
		
			ResultSet rs = createStatement(query);
			rs.next();
			i = rs.getInt("counterinfo");
		}
		catch (SQLException s){
			printErrorMessage(s, query);
		}
		return i;	
	}
	
	/**
	 * get given amount of projects for which user has at least read right
	 * @param counterinfo
	 * @return
	 * @throws StatementNotExecutedException
	 */
	private Projekt[] getProjects(int counterinfo) throws StatementNotExecutedException{
		Projekt[]plist = new Projekt[counterinfo];
		String query="";
		try{
			//admin is allowed to work with every project
			if(user.getIsAdmin()){
				query="SELECT ProjNr, ProjName, Datenbanknummer FROM " + databaseName+".projekt WHERE geloescht='N' ORDER BY ProjName";
			}
			//user may at least read project
			else{
				query="SELECT ProjNr, ProjName, Datenbanknummer FROM " + databaseName+".projekt WHERE geloescht='N' AND EXISTS" + 
					"(SELECT * FROM projektrechte WHERE Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1) " +
					"AND projekt.ProjNr=projektrechte.ProjNr AND projekt.Datenbanknummer=projektrechte.DBNummerProjekt " +
					"AND geloescht='N') ORDER BY ProjName";
			}
			ResultSet rs = null;
			rs = createStatement(query);
			int i = 0;
			while (rs.next()) {
				String name = rs.getString("ProjName");
				int nr = rs.getInt("ProjNr");	
				int db = rs.getInt("Datenbanknummer");
				Projekt p = new Projekt(name, nr, db);
				plist[i] = p;
				i++;
			}
		}
		catch (SQLException s){
			printErrorMessage(s, query);
		}
		return plist;
	}
	
	/**
	 * @precondition user may at least read project with given projNr
	 * @param projNr
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String[][] getProjektInformations(int[] projectKey) throws StatementNotExecutedException{
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;
		String query = "SELECT * FROM " + databaseName+".projekt " 
			+ "WHERE ProjNr=" + projectKey[0] 
			+ " AND Datenbanknummer="+projectKey[1]
			+ " AND geloescht='N';";
		String[][] info = new String[0][0];
		
		try { 
			rs = createStatement(query);
			rsmd = rs.getMetaData();
			rs.next();
			int columnCount=rsmd.getColumnCount();
			info = new String[columnCount][2];
			
			for (int i = 1; i <= columnCount; i++) {
				info[i-1][0] = rsmd.getColumnName(i);
				info[i-1][1] = rs.getString(i);
			}

		} catch (SQLException s) {
			printErrorMessage(s, query);
		}
		return info;
	}
	
	/**
	 * @postcondition ProjEigentuemer is current user (trigger in database)
	 * @param ProjektName
	 * @throws StatementNotExecutedException
	 */
	public void newProject(String ProjektName) throws StatementNotExecutedException{
		String query = "INSERT INTO " + databaseName+".projekt (ProjEigentuemer, projName, Datenbanknummer, Zustand, zuletztSynchronisiert)" + 
			"VALUES " + "(SUBSTRING(USER(),1, LOCATE('@', USER())-1), '" + ProjektName +"', " + versionDatabaseNumber + ", 'ge\u00E4ndert', NOW()+0);";
		try {
			executeStatement(query);
		} catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
	/**
	 * @param projnr
	 * @throws StatementNotExecutedException
	 * @throws NoWriteRightException
	 */
	public void deleteProject(int[] projectKey) throws StatementNotExecutedException, NoWriteRightException{
		if(user.getIsAdmin() || user.getRight(projectKey).equals("schreiben")){
			String query = "UPDATE " + databaseName+".projekt SET geloescht='Y', Zustand='ge\u00E4ndert' WHERE ProjNr='" + projectKey[0]
				+ "' AND Datenbanknummer="+projectKey[1]+";";
			executeStatement(query);
			query = "UPDATE projektrechte SET geloescht='Y', Zustand='ge\u00E4ndert' WHERE ProjNr='" +projectKey[0]
				+ "' AND DBNummerProjekt="+projectKey[1]+";";
			executeStatement(query);
		}
		else throw new NoWriteRightException(getProjectName(projectKey), "l\u00F6schen");
	}
	
	/**  
	 * @param projnr
	 * @throws StatementNotExecutedException
	 */
	public void deleteProjectPermanent(int[] projectKey, int event) throws StatementNotExecutedException{
		String condition ="";
		if(event==QueryManager.DELETE_DELETED) condition= " AND geloescht='Y' AND Zustand='synchronisiert'";	
		String query = "DELETE FROM " + databaseName+".projekt WHERE ProjNr='" + projectKey[0]
			+ "' AND Datenbanknummer="+projectKey[1]+" " +condition+";";
		executeStatement(query);
		query = "DELETE FROM " + databaseName+".projektrechte WHERE ProjNr='" + projectKey[0]
			+ "' AND DBNummerProjekt="+projectKey[1]+" " +condition+";";
		executeStatement(query);
	}
	
	/**
	 * get the name of a project to a given project number
	 * @param projnr
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getProjectName(int[] projectKey) throws StatementNotExecutedException{
		String result="";
		String query="SELECT ProjName FROM " + databaseName+".projekt WHERE ProjNr="+projectKey[0]+" AND Datenbanknummer="+projectKey[1]+";";
		try{
			ResultSet rs = createStatement(query);
			rs.next();
			return rs.getString(1);
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return result;
	}
	
	/**
	 * @precondition: users has right to write on project
	 * @param label
	 * @param text
	 * @param projNr
	 * @throws StatementNotExecutedException
	 */
	public void updateProjekt(String label, String text, int[] projectKey) throws StatementNotExecutedException{
		String query = "UPDATE " + databaseName+".projekt SET " + label + "='"
			+ text +"', Zustand='ge\u00E4ndert'"+ " WHERE ProjNr=" + projectKey[0]
			   + " AND Datenbanknummer="+projectKey[1]+";";
		try {
			// FELD Eintragen
			executeUpdate(query);
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
	/**
	 * @precondition: current user may read the project
	 * @param projName
	 * @return the number to the given projectName
	 * @throws StatementNotExecutedException
	 */
	public int[] getProjectKey(String projName) throws StatementNotExecutedException{
		String query = "SELECT ProjNr, Datenbanknummer FROM " + databaseName+".projekt WHERE " +
				"ProjName='" + projName + "';";
		int[] key = {-1,-1};
		try{
			ResultSet rs = createStatement(query);
			if(rs.next()){
				key[0]= rs.getInt(1);
				key[1]=rs.getInt(2);
			}
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return key;
	}
	
	/**
	 * 
	 * @param projectKeyValues of global database
	 * -> needed to be independet from global database scheme changes
	 * @param columns of global database
	 * -> needed to be independet from order and global database scheme changes
	 * @param key
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getProjectEntry(String[][] columns, int[] projectKeyValues, Vector<String> key) throws StatementNotExecutedException{
		String condition =" WHERE ";
		for(int i=0; i<key.size(); i++){
			condition = condition + key.elementAt(i) + "=" + projectKeyValues[i];
			if(i<key.size()-1) condition = condition + " AND ";
		}
		String[] tableNames = {databaseName+".projekt"};
		return getEntries(columns, tableNames, condition);
	}
	
	/**
	 * 
	 * @param projnr
	 * @return whether the logged in user is owner of the project with the given number
	 * @throws StatementNotExecutedException
	 */
	public boolean isProjectOwner(int[] projectKey) throws StatementNotExecutedException{
		String query = "SELECT ProjEigentuemer=SUBSTRING(USER(),1, LOCATE('@', USER())-1) " +
				"FROM " + databaseName+".projekt WHERE ProjNr=" + projectKey[0] + " AND Datenbanknummer=" + projectKey[1]+" AND geloescht='N';";
		try{
			ResultSet rs = createStatement(query);
			rs.next();
			if (rs.getInt(1)==1) return true;
			else return false;
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return false;
	}
	
	/**
	 * check if registeres user at database is project owner of given project
	 * @param projectKey
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getProjectOwner(int[] projectKey) throws StatementNotExecutedException{
		String query = "SELECT ProjEigentuemer " +
			"FROM " + databaseName+".projekt WHERE ProjNr=" + projectKey[0] + " AND Datenbanknummer=" + projectKey[1]+" AND geloescht='N';";
		try{
			ResultSet rs = createStatement(query);
			if(rs.next())
				return rs.getString(1);
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return null;
	}
	 
	/**
	 * get all eingabeeinheit entries which have been changed
	 * @param projectKey 
	 * @param columntypes columns of global database 
	 * -> needed to be independet from order and global database scheme changes
	 * @param event - QueryManager.CHANGES_LOCAL, QueryManager.CHANGES_GLOBAL, QueryManager.ALL_GLOBAL
	 * @param lastSynchronisation
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getProjectEntries(int[] projectKey, String[][] columntypes, int event, String lastSynchronisation) throws StatementNotExecutedException{
		Vector<String[]> result;
		String[] tables = {databaseName+".projekt"};
		String condition =" WHERE projNr="+projectKey[0]+" AND projekt.Datenbanknummer=" + projectKey[1] + " ";
		if(event==QueryManager.CHANGES_LOCAL) condition = condition + " AND projekt.Zustand LIKE 'ge\u00E4ndert' AND projekt.Nachrichtennummer != -1";
		else if(event==QueryManager.CHANGES_GLOBAL)
			condition = condition+ " AND projekt.Zustand>'" + lastSynchronisation + "'";
		result = getEntries(columntypes, tables , condition);
		return result;
	}
	
	/**
	 * 
	 * @param projectKeyValues of global database
	 * -> needed to be independet from global database scheme changes
	 * @param columns of global database
	 * -> needed to be independet from order and global database scheme changes
	 * @param key
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getRightEntries(int[] projectKey, String[][] columntypes) throws StatementNotExecutedException{
		Vector<String[]> result;
		String condition = " WHERE projNr="+projectKey[0]+" AND projektrechte.DBNummerProjekt=" + projectKey[1];
		String[] tables = {databaseName+".projektrechte"};
		result = getEntries(columntypes, tables , condition);
		return result;
	}
	
	public void insertProjectData(String[] scheme, String[] data) throws StatementNotExecutedException{
		insertData(scheme, data, databaseName+".projekt");
	}
	
	public void insertRightData(String[] scheme, String[] data) throws StatementNotExecutedException{
		insertData(scheme, data,databaseName+".projektrechte");
	}
	
	/**
	 * set last time of synchronization for project after synchronization of project is finished
	 * @param time
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 */
	public void setLastSynchronizeTime(String time, int[] projectKey) throws StatementNotExecutedException{
		String query = "UPDATE " + databaseName+".projekt SET zuletztSynchronisiert='" + time + "' " +
				"WHERE projNr='" + projectKey[0] + "' AND Datenbanknummer="+projectKey[1]+";"; 
		try{
			executeUpdate(query);
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
	public int updateProjectData(String[] scheme, String[] data, Vector<String> key) throws StatementNotExecutedException{
		return updateData(scheme, data, databaseName+".projekt", key);
	}
	
	/**
	 * set Zustand to 'synchronisiert' after server affirmed changing global database
	 * @precondition executed on local database during synchronization
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 */
	public void setSynchronized(int[] projectKey) throws StatementNotExecutedException{
		String query = "UPDATE " + databaseName+".projekt SET Zustand='synchronisiert', projekt.Nachrichtennummer=-1 WHERE " +
				"ProjNr = '" + projectKey[0] + "' AND Datenbanknummer="+projectKey[1]+" AND projekt.Nachrichtennummer!=-1;";
		try{
			executeUpdate(query);
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
	/**
	 * get last time of synchronization of project
	 * @param projectKey
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getLastSynchronization(int[] projectKey) throws StatementNotExecutedException{
		String query = "SELECT zuletztSynchronisiert FROM " + databaseName+".projekt WHERE" +
				" ProjNr="+projectKey[0]+" AND Datenbanknummer="+projectKey[1] +";";
		try{
			ResultSet rs = createStatement(query);
			if(rs.next()) return rs.getString(1);
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
		return null;
	}

	public String getLastChange(Vector<String> key, int[] keyValues) throws StatementNotExecutedException{
		return getStatus(key, keyValues, databaseName+".projekt");
	}
	
	/**
	 * assign message number to changed entries in artefaktmaske of given project
	 * @param message
	 * @param projectKey
	 * @throws StatementNotExecutedException
	 */
	public void setMessageNumber(int message, int[] projectKey) throws StatementNotExecutedException{
		String query = "UPDATE " + databaseName + ".projekt SET Nachrichtennummer=" + message 
			+ " WHERE ProjNr="+projectKey[0]+" AND Datenbanknummer="+projectKey[1] +" AND Zustand='ge\u00E4ndert';";
		try{
			executeStatement(query);
		}catch (SQLException e) {
			printErrorMessage(e, query);
		}
	}
	
}
