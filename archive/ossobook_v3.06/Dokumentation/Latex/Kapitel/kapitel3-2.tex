\section{Problemklassen eines Intermittently Synchronized Database System}
\label{problemklassen}

In Kapitel \ref{datenbankmodell} wurde festgestellt, dass es sich beim zuk"unftigen Ossobook-System um ein Intermittently Synchronized Database System handelt. W"ah\-rend der Konzeption eines solchen m"ussen immer folgende drei grundlegende Fragen beantwortet werden:
~\\
\begin{compactitem}
\item Wie soll die \textbf{Datenverteilung und -replikation} erfolgen?
\item Wie sollen die Daten abgeglichen (\textbf{Synchronisation}) und wie kann (dabei) die \textbf{Konsistenz} gew"ahrleistet werden?
\item Wie kann die Konsistenz aller Datenbanken gesichert werden, auch wenn es w"ahrend der Synchronisation zum Kommunikationsabbruch kommt (\textbf{Fehlertoleranz})?
\end{compactitem}

~\\
Die Abschnitte \ref{replikation} bis \ref{quellen} besch"aftigen sich zun"achst nur mit der Theorie zu diesen Fragestellungen. Es werden verschiedene Antworten sowie deren Vor- und Nachteile vorgestellt.

Erst Kapitel \ref{konzept} legt dann die einzelnen Strategien f"ur das Ossobook-System fest.


\subsection{Replikation}
\label{replikation}

\cite{MS04} versteht unter \hypertarget{rep}{\textbf{Replikation}} "`die Einf"uhrung von Datenredundanz"'. Auf Datenbanken angewendet bedeutet dies, dass Datenelemente in mehreren Datenbanken gespeichert werden. Dabei gibt es mindestens eine Master-Datenbank, die \textbf{Replikationsquelle}, von der aus die Daten auf eine oder mehrere Slave-Datenbanken, die \textbf{Replikationssenken}, repliziert werden.

Die verschiedenen Datenbanken k"onnen alle Daten enthalten oder nur einen Teil. Im ersten Fall wird von \textbf{voll replizierten Datenbanken} gesprochen. 

Soll eine Datenbank nur eine Untermenge aller Daten enthalten, m"ussen die Daten so fragmentiert werden, dass die Originaldaten jederzeit aus den zerlegten Daten wiederhergestellt werden k"onnen. Bei einer \textbf{vertikalen Fragmentierung} beispielsweise wird nur eine Teilmenge aller Spalten einer Tabelle repliziert. Erfolgt diese Aufteilung absolut disjunkt, d.h. kommt keine Spalte eines Fragments in einem anderen Fragment vor, gibt es kein gemeinsames Attribut, das f"ur die Wiederherstellung der Relation sorgt. Es muss also in allen Fragmenten ein Prim"arschl"ussel bzw. ein Schl"usselkandidat eingef"uhrt werden, mithilfe dessen die Relation rekonstruiert werden kann.

Bei der \textbf{horizontalen Fragmentierung} hingegen wird die Menge aller Tupel einer Tabelle in Teilmengen unterteilt. Diese Teilmengen werden anschlie"send auf die verschiedenen Datenbanken kopiert. Dabei k"onnen einzelne Fragmente auch mehreren Datenbanken zugewiesen werden. Dennoch kann die Ursprungsrelation jederzeit problemlos wiederhergestellt werden, solange jeder Datensatz der Ursprungstabellen mindestens in einer Datenbank gespeichert ist und den systemweit eindeutigen Prim"arschl"ussel enth"alt, durch den Datensatzduplikate identifiziert werden k"onnen. Eine solche horizontale Fragmentierung erfolgt meist anhand eines einzelnen Attributs.

Werden bei der Replikation horizontale und vertikale Fragmentierung gemischt, so nennt sich dies \textbf{Hybridfragmentierung}, f"ur die es sinnvolle Anwendungen gibt: So k"onnte es in einer Firma eine gro"se Datenbank geben, die alle Informationen "uber die Mitarbeiter enth"alt: Vorname, Nachname, Einsatzort, Vorgesetzter, Aufgabe, Gehalt, \punkte An den einzelnen Standorten werden einzelnen Mitarbeitern verschiedenen Aufgaben zugeteilt. Bei Problemen muss gew"ahrleistet werden, dass ein verantwortlicher Mitarbeiter erreichbar ist. F"ur diese beiden Aufgaben sind Gehaltsinformationen irrelevant. Deswegen werden auf den einzelnen Datenbanken der Standorte alle Spalten au"ser der des Gehalts repliziert (vertikale Fragmentierung). Die Verteilung der Aufgaben erfolgt nur an Mitarbeiter, die am Standort ihren Arbeitsplatz haben. Deswegen werden nur deren Daten auf der Standortdatenbank gespeichert (horizontale Fragmentierung). 

Neben der bereits erw"ahnten vollst"andigen Replikation auf allen Datenbanken gibt es noch die \textbf{nicht redundante Allokation}. Hierbei wird jedes Fragment in genau einer Datenbank gespeichert. Es ist also keinerlei Redundanz vorhanden. Zwischen der vollst"andigen Replikation und der nicht redundanten Allokation gibt es noch alle m"oglichen Varianten, in denen verschiedene Fragmente unterschiedlich oft repliziert werden, w"ahrend andere keinerlei Redundanz erfahren. 

~\\
Eine ausf"uhrlichere Beschreibung mit Erkl"arungen anhand von Beispielen zum Thema "`Replikation"' finden sich u.a. in \cite{ES02}.


\subsection{Synchronisation}
\label{synchronisation-begriff}

Unter \textbf{Synchronisation} wird "`der Abgleich von redundant vorhandenen Daten, die verschiedene "Anderungen erfahren haben"' \cite{MS04} verstanden. Ist es in einem Datenbanksystem m"oglich, dass  
auf mindestens zwei verschiedenen Datenbanken sich widersprechende "Anderungen an denselben Datens"atzen vorgenommen werden k"onnen, m"ussen diese Daten miteinander verglichen und, falls ein Konflikt erkannt wird, mittels einer festgelegten Strategie behandelt werden. Ziel ist es so, die Konsistenz des Datenbanksystems beizubehalten bzw. wiederherzustellen. 

Verschiedene Konfliktbehandlungsstrategien werden im folgenden Unterkapitel \ref{konfliktstrategien} vorgestellt. Zuvor soll jedoch gekl"art werden, unter welchen Umst"anden eine solche Strategie eingesetzt werden muss, und wie erkannt werden kann, dass ein Konflikt aufgetreten ist. In Kapitel \ref{konfliktarten} werden deswegen zun"achst die einzelnen Konfliktkategorien und ihre Ursachen vorgestellt. Anschlie"send folgt in Abschnitt \ref{erkennung} eine Beschreibung von Methoden, die der Kon\-flikt\-er\-ken\-nung dienen.


\subsubsection{Konfliktarten}
\label{konfliktarten}

Grunds"atzlich kann der Synchronisationsprozess in zwei Schritte unterteilt werden:
~\\
\begin{compactitem}
\item Bei der \textbf{Reintegration} werden die auf den Replikationssenken durchgef"uhrten "Anderungen in die Replikationsquelle integriert.
\item Die \textbf{R"uck"ubertragung} beschreibt den umgekehrten Weg. Hierbei wird ein aktueller, konsistenter Datenbankzustand der Synchronisationsquelle an eine oder mehrere Replikationssenken "ubertragen.
\end{compactitem}

~\\
Je nach Datenbanksystemkonzept k"onnen nur die Reintegration, nur die R"uck\-"uber\-tra\-gung oder beide Schritte zum Einsatz kommen. Dabei werden jeweils auf der entsprechenden Datenbank entweder nochmals die "Anderungstransaktionen\footnote{"Anderung steht hier f"ur das Einf"ugen, "Andern oder Entfernen eines Datensatzes.}\saveFN\aend\ ausgef"uhrt oder die einzelnen, ge"anderten Datens"atze kopiert. Im ersten Fall wird von \textbf{anweisungsbasierter}, im zweiten von \textbf{datensatzbasierter Synchronisation} gesprochen \cite{ADbas}\footnote{\cite{ADbas} spricht zwar von Replikation, meint damit jedoch das Nachf"uhren der auf einer Datenbank ausgef"uhrten "Anderungen auf einer anderen Datenbank. Damit entspricht dies dem Prozess der Synchronisation.}. 

~\\
Falls zwei Nutzer quasi gleichzeitig dieselben Datens"atze, d.h. mit denselben Prim"arschl"usseln, jeweils auf ihrer mobilen Datenbank ge"andert\useFN\aend\ haben, kommt es bei der Reintegration zum Konflikt, da unklar ist, welche Aktualisierung "ubernommen werden soll. Bei der Reintegration durch den zweiten Nutzer k"onnen drei Konfliktsituationen unterschieden werden, je nachdem, welche Art von Konflikt vorliegt (vgl. \cite{MH07}):
~\\
\begin{compactitem}
\item Einf"ugen eines Schl"usselduplikats
\item Widerspr"uchliche Aktualsierungen
\item Aktualisieren oder L"oschen einer nicht mehr existierenden Zeile
\end{compactitem}

In den folgenden Abschnitten \ref{primärduplikate} bis \ref{delete-konflikt} werden diese Konfliktarten einzeln vorgestellt. Dabei werden die Umst"ande ihrer Entstehung erl"autert.


\paragraph{Schl"usselduplikate}~\\
\label{primärduplikate}
Duplizierte prim"are Schl"ussel entstehen, wenn zwei Benutzer unabh"angig voneinander auf verschiedenen Datenbanken jeweils einen neuen Datensatz ein\-f"u\-gen. Diesem k"onnen sie nun zuf"allig denselben prim"aren Schl"ussel zuordnen. Eine andere M"oglichkeit der Entstehung ist die, dass der Prim"arschl"ussel vom DBMS generiert wird. Dann k"onnen die automatischen Schl"usselgenerierungen ("`Autoincrement"') zweier Datenbanken unabh"angig voneinander denselben Prim"arschl"ussel erzeugen.

In beiden F"allen sind die Datens"atze im System nicht mehr eindeutig zu identifizieren. Bei der zweiten Reintegration f"uhrt der Versuch, den Datensatz erneut einzuf"ugen, zu einem schwerwiegenden Fehler seitens des DBMS'.

Tritt ein solcher Konflikt auf, gibt es zwei grunds"atzliche Wege, wie mit ihm verfahren werden kann: Zum Einen kann entweder der erste eingebrachte Datensatz durch den zweiten "uberschrieben oder der zweite, noch einzubringende Datensatz verworfen werden. 

Zum Anderen k"onnen beide Datens"atze in die Datenbank eingebracht werden. Dann m"ussen beide Prim"arschl"ussel systemweit eindeutig gemacht werden. An einen Character-Prim"arschl"ussel kann z.B. der Name oder die systemweit eindeutige Identifikationsnummer der einf"ugenden Datenbank angeh"angt werden. 

Problematisch ist jedoch, dass beide Methoden zu einem unbefriedigenden Ergebnis f"uhren k"onnen: Im ersten Fall gehen Daten ggf. verloren, im zweiten wird ein identischer Datensatz ggf. redundant gespeichert. Deswegen sollte m"oglichst von vornherein versucht werden, die Prim"arschl"usselgenerierung systemweit eindeutig zu gestalten.

~\\
Dasselbe Problem ergibt sich bei Unique-Index-Constraints\footnote{Ein Beispiel f"ur einen Unique-Index-Constraint findet sich in Kapitel \ref{primär}}. Hierbei darf ein Attributwert(tupel) innerhalb einer Tabelle h"ochstens einmal vorkommen. Dies bedeutet, wird w"ahrend des zweiten Reintegrationsschritts versucht, einen solchen Unique-Index-Constraint nochmals einzuf"ugen oder auf einen bereits existierenden abzu"andern, tritt ein Konflikt auf. Dieser kann entsprechend dem Umgang mit Pri\-m"ar\-schl"us\-sel\-du\-pli\-ka\-ten ebenfalls durch das Anh"angen eines eindeutigen Identifikators gel"ost werden. Allerdings besteht hier die Gefahr, dass damit der Sinn des (der) Attributs (Attribute) f"ur die Nutzer verloren geht.

\paragraph{Widerspr"uchliche Updates}~\\
Wenn zwei Nutzer auf unterschiedlichen Datenbanken denselben Datensatz "andern, f"uhrt dies zu zwei sich widersprechenden "Anderungen. Sollen diese "Anderungen nun in die Replikationsquelle integriert werden, ist unklar, welche Version des Datensatzes die richtige ist oder ob eine Mischung der beiden Aktualisierungen in die Replikationsquelle eingebracht werden m"usste.

Die Problematik widerspr"uchlicher Updates besteht zun"achst einmal darin, den Konflikt zu erkennen. Die anwendbaren Erkennungsmethoden werden im Abschnitt \ref{erkennung} beschrieben. Nachdem ein Konflikt festgestellt wurde, muss dieser bereinigt werden. Hierf"ur gibt es verschiedene Konfliktbehandlungsstrategien, die je nach Art der Anwendung und Bedeutung der Daten eingesetzt werden k"onnen. Sie werden in Abschnitt \ref{konfliktstrategien} einzeln vorgestellt.

\paragraph{Konflikte aufgrund eines gel"oschten Datensatzes}~\\
\label{delete-konflikt}
Ein Konflikt durch das L"oschen eines Datensatzes tritt immer dann auf, wenn ein Nutzer einen Datensatz aus seiner Datenbank entfernt und ein anderer denselben Datensatz in seiner Datenbank entweder ebenfalls l"oscht oder "andert. Synchronisiert der erste Nutzer seine Datenbank mit der Replikationsquelle, wird der entsprechende Datensatz w"ahrend des Reintegrationsschritts aus der globalen Datenbank entfernt. Will nun auch der zweite Nutzer seine "Anderungen synchronisieren, k"onnen diese nicht "ubernommen werden, da der Datensatz in der Quelle nicht mehr existiert.

Dies stellt f"ur das zweimalige Entfernen eines Datensatzes kein Problem dar, da die zwei L"osch-Anweisungen keinen Widerspruch darstellen. Anders verh"alt sich dies jedoch, wenn die zweite Anweisung den gel"oschten Datensatz "andern soll. Dann geht die entsprechende "Anderung verloren, da das zugeh"orige UPDATE den gel"oschten Datensatz nicht betrifft. Es stellt sich deswegen die Frage, wie ein solcher Konflikt erkannt und wie mit diesem umgegangen werden soll (vgl. Abschnitte \ref{erkennung} und \ref{konfliktstrategien}).


\subsubsection{Konflikterkennung}
\label{erkennung}

Um das Datenbanksystem konsistent zu halten, muss zun"achst festgestellt werden, dass es zu einem Konflikt gekommen ist. F"ur "`Schl"usselduplikate"' stellt dies kein Problem dar, da das DBMS beim "`erneuten"' Einf"ugen des Datensatzes einen schwerwiegenden Fehler ausl"ost.

Anders verh"alt sich dies bei "`widerspr"uchlichen Updates"' und "`Konflikten aufgrund eines gel"oschten Datensatzes"'. Hier muss eine explizite Konfliktpr"ufung stattfinden:

\paragraph*{Erkennung von Konflikten aufgrund eines gel"oschten Datensatzes}
Da es sich beim erneuten L"oschen eines Datensatzes um keinen Widerspruch zu den vorher ausgef"uhrten Anweisungen handelt, muss dieser Fall nicht extra behandelt werden. Eine Pr"ufung auf Konflikte ist nicht notwendig.

Soll jedoch ein nicht mehr existierende Datensatz ge"andert werden, gehen die Informationen des Datensatzes verloren, falls der Konflikt nicht erkannt und nicht entsprechend reagiert wird. Deswegen muss das Synchronisationsprogramm "Anderungen der Reintegration auf Konflikte mit vorher erfolgten L"oschanweisungen pr"ufen. 

Eine M"oglichkeit des Erkennens besteht z.B. darin zu pr"ufen, ob die UPDATE-Anweisungen eines Reintegrationsschritts mindestens einen Datensatz betrifft. So gibt ein UPDATE-Statement auf einer MySQL-Datenbank die Anzahl der betroffenen Zeilen zur"uck. 

Problematisch an dieser Methode ist jedoch, dass die "Anderung bei der anweisungsbasierten Replikation zu einem fr"uheren Zeitpunkt evtl. mehr Datens"atze betroffen h"atte, n"amlich auch die zwischenzeitlich gel"oschten. Diese Konflikte werden so nicht erkannt.

Ist ein Datensatz einmal gel"oscht, sind seine Informationen verloren. Es ist nur schwer m"oglich, auf die Werte des Datensatzes vor seiner L"oschung zur"uckzugreifen, um eine Entscheidung "uber den Umgang mit dem Konflikt zu treffen. Evtl. k"onnten zwar hierf"ur die vorhandenen Log-Files genutzt werden; dies ist jedoch mit hohem Aufwand verbunden. 

Deswegen wird in \cite{conf} geraten, Datens"atze nicht zu l"oschen, sondern sie stattdessen in der Datenbank zu markieren. Damit kann jederzeit auf den Vorzustand des Datensatzes zugegriffen, wodurch ein auftretender Konflikt wie ein widerspr"uchliches Update erkannt und behandelt werden kann.


\paragraph*{Erkennung widerspr"uchlicher Updates}~\\
Grunds"atzlich gibt es zwei M"oglichkeiten, widerspr"uchliche Updates zu erkennen: Die erste Methode verwendet \textbf{Versionen}, die einzelnen Datens"atzen oder Datensatzwerten zugeordnet werden. In der Replikationsquelle wird die Version jedes Mal um eins hochgez"ahlt, wenn sich der entsprechende Datensatz oder der betreffende Spaltenwert "andern. Die Replikationssenke merkt sich, welche Version sie zuletzt von der Quelle gelesen hat. Sollen anschlie"send bei der Synchronisation "Anderungen in die Quelle reintegriert werden, kann "uber den Vergleich der aktuellen Version der betroffenen Daten mit der entsprechenden von der Senke protokollierten letzten Datenversion ein Konflikt festgestellt werden. Dies ist der Fall, wenn die alte und die neue Version nicht "ubereinstimmen.

Die zweite Methode der Konflikterkennung arbeitet "ahnlich, allerdings werden hier \textbf{Timestamps} verwendet. D.h., auf der Replikationsquelle wird f"ur Datens"atze bzw. Attributwerte der "Anderungszeitpunkt protokolliert. Die Replikationssenke merkt sich den Zeitpunkt, zudem zuletzt die einzelnen Datens"atze bzw. Spaltenwerte aktualisiert wurden. Wird sp"ater bei der Synchronisation f"ur einen Wert fesgestellt, dass "`gemerktes Aktualisierungsdatum $<$ letztes "Anderungsdatum"' gilt, so handelt es sich bei der aktuellen und der letzten globalen "Anderung um einen Widerspruch. Ein Konflikt wurde erkannt. 

Protokolliert bei dieser Methode die Replikationssenke selbst den Zeitpunkt des letztes Aktualisierens und nutzt dazu nicht die Systemzeit der Replikationsquelle, so muss darauf geachtet werden, dass die Systemzeiten der beiden Datenbanken synchron laufen und "ubereinstimmen \cite{conf}. Andernfalls werden bei der n"achsten Synchronisation zu viele oder zu wenige Konflikte erkannt. Zu viele werden festgestellt, wenn die Senke einen zu fr"uhen Zeitpunkt protokolliert. Dann existieren evtl. Datens"atze in der Quelle, f"ur die gilt "`Synchronisationsdatum $<$ "Anderungsdatum"', obwohl die "Anderungen bereits vor der Synchronisation vorgenommen wurden. Bei der n"achsten Synchronisation wird f"ur "Anderungen, die diese Datens"atze betreffen, ein Konflikt erkannt.

Zu wenig Konflikte werden aufgedeckt, wenn die Senke ein zu sp"ates Datum protokolliert. Damit gilt f"ur neu ge"anderte Datens"atze der Quelle evtl.: "`Synchronisationsdatum $>$ "Anderungsdatum"'. Diese Konflikte werden nicht erkannt.

Umgehen l"asst sich dieses Problem, indem die Senke beim Aktualisieren die Quelle nach ihrer Systemzeit fragt und diese als Synchronisationszeitpunkt "ubernimmt.


\subsubsection{Konfliktbehandlungsstrategien}
\label{konfliktstrategien}

Aus Kapitel \ref{konfliktarten} geht hervor, dass es in einem Intermittently Synchronized Database System Daten"anderungen geben kann, die im Widerspruch zueinander stehen. Deswegen muss eine Strategie festgelegt werden, wie mit Konflikten umgegangen werden soll. Dabei gibt es eine ganze Menge von Methoden, die von "`der Anwendungsentwickler muss daf"ur sorgen, dass keinerlei Konflikte auftreten"' bis "`bei einem Konflikt werden beide Datensatzversionen in der Datenbank gespeichert"' reichen\footnote{Die meisten dieser Methoden finden Verwendung bei Anwendungen, die durch den Microsoft SQL Server 2005, den Oracle 10g oder den MySQL 5.0 unterst"utzt werden. N"ahere Informationen, welche Strategien von den jeweiligen Servern im Einzelnen zur Verf"ugung gestellt werden, lassen sich in Kapitel \ref{praxis} nachlesen. Weiterf"uhrende Literaturvorschl"age sind, soweit im Folgenden nicht anders angegeben, ebenfalls in diesem Kapitel zu finden.}: 
~\\
\begin{compactitem}
	\item Verhinderung
	\item Kapitulation
	\item Dokumentation
	\item Priorit"aten
	\item Timestamps
	\item Mathematische Funktionen
	\item Duplikate
	\item Interaktivit"at
\end{compactitem}
~\\
In den folgenden Paragraphen sollen diese Strategien einzeln vorgestellt werden.

\paragraph*{Verhinderung}~\\
Die Konsistenz des Datenbanksystems bleibt immer gewahrt, wenn im System grunds"atzlich keine Konflikte auftreten k"onnen. Deswegen sind bei dieser Strategie Sys\-tem\-ad\-minis\-tra\-to\-ren und Anwendungsentwickler daf"ur verantwortlich, dass die "Anderung derselben Datenbankobjekte durch mehrere Nutzer an unterschiedlichen Orten zur (nahezu) selben Zeit ausgeschlossen wird.

So k"onnte z.B. der Systemadministrator einen Datenbankserver, den Master, festlegen, auf dem Daten ge"andert werden d"urften. Alle anderen Datenbanken, die Slaves, dienten nur dem Abfragen des Datenbankinhalts.

Wird diese Strategie eingesetzt, unterliegt das Datenbanksystem einer gro"sen Restriktion: Sollen "Anderungen vorgenommen werden, muss auf den Master zur"uckgegriffen werden. Dies wiederum bedeutet, dass zwischen den mobilen Clients, auf denen die Anwendung l"auft, und dem Master dauerhaft eine Verbindung bestehen muss. In einem Intermittently Synchronized Database System ist dies jedoch i.d.R. nicht der Fall. Damit ist diese Strategie nur dann einsetzbar, wenn auf den mobilen Clients haupts"achlich Datenabfragen gemacht werden.

\paragraph*{Kapitulation}~\\
Bei der Kapitulation wird zun"achst davon ausgegangen, dass keine Konflikte auftreten. Dennoch findet eine Konfliktpr"ufung statt. Wird hierbei ein Widerspruch festgestellt, wird dieser als schwerwiegender Fehler behandelt und die Synchronisation abgebrochen. Die am Konflikt beteiligten Datenbanken sind solange nicht einsatzbereit, bis eine verantwortliche Person den Konflikt manuell bereinigt hat. 

In einem Intermittently Synchronized Database System macht es nur dann Sinn von einem schwerwiegenden Fehler bei einem Konflikt auszugehen, wenn Konflikte tats"achlich so gut wie ausgeschlossen werden k"onnen. Andernfalls muss immer daf"ur gesorgt sein, dass zu jeder Tages- und Nachtzeit ein Sys\-tem\-ver\-ant\-wort\-li\-cher verf"ugbar ist, der einen auftretenden Konflikt behebt, um die Funktionsweise der Datenbanken wiederherzustellen. 

Insbesondere in Datenbanksystemen, die nur von wenigen Personen genutzt werden, macht es keinen Sinn, solch einen Systemverantwortlichen zu benennen. Stattdessen sollten die Nutzer selbst Konflikte bereinigen k"onnen.

Erschwerend bei dieser Strategie kommt hinzu, dass der Systemverantwortliche sich sowohl mit dem Datenbankinhalt auskennen muss, um den Konflikt fachgerecht zu bereinigen, als auch mit dem Datenbankserver an sich, da es nach einem aufgetretenen Konflikt ggf. notwendig ist, die beteiligten Datenbanken neu zu starten.

\paragraph*{Dokumentation}~\\
Auch bei der Strategie der Dokumentation wird wie bei der Kapitulation auf die manuelle L"osung des Konflikts gesetzt. Hierbei wird ein Konflikt jedoch nicht als Fehler angesehen. Dennoch werden die den Konflikt ausl"osenden Transaktionsvorg"ange abgebrochen und dokumentiert. Ein Systemverantwortlicher wird informiert.

Im Gegensatz zur Kapitulation kann die Datenbank weiter genutzt werden, selbst wenn der Konflikt noch nicht vom Systemverantwortlichen gel"ost wurde. So kann es zu Folgekonflikten kommen, die ebenfalls wieder dokumentiert werden. Insgesamt kann damit eine sehr gro"se und un"ubersichtliche Konfliktdatei entstehen. Deswegen sollte der Systemverantwortliche immer m"oglichst zeitnah reagieren und die Konflikte beseitigen.

Analog zur Kapitulationsmethode muss der Systemverantwortliche mit dem Inhalt der Datenbank und der Funktionsweise einer Datenbank (insbesondere den Log-Dateien) vertraut sein. Dies kann sich v.a. bei kleinen, informatikfernen Projekten als problematisch erweisen.

\paragraph*{Priorit"aten}~\\
Wird die Priotit"aten-Strategie zur Konfliktl"osung eingesetzt, wird mit Pr"aferenzen gearbeitet. Zum Einen k"onnen den einzelnen \textbf{Datenbanken} unterschiedliche Priorit"aten zugeordnet werden. Tritt ein Konflikt zwischen den "Anderungen zweier Datenbanken auf, "`gewinnt"' die Datenbank mit der h"oheren Priorit"at, d.h. ihre "Anderung wird "ubernommen, w"ahrend die andere verworfen wird. Hierbei wird meist eine der folgenden beiden Varianten verwendet:

\begin{compactitem}
\item[]
\item Bei der Reintegration gewinnt immer die Replikationsquelle:\\
Bei einem Konflikt werden die "Anderungen in der Masterdatenbank beibehalten und die "Anderungen des Slaves verworfen.
\item Bei der Reintegration gewinnt immer die Replikationssenke.\\
Die konfliktausl"osenden "Anderungen auf dem Master werden durch die "Anderungen des Slaves "uberschrieben.
\end{compactitem} 

\newpage
Die andere Methode\footnote{Die Konfliktl"osung mittels "`Priorit"aten f"ur Operationen"' oder "`Duplikaten"' wird z.B. bei der Synchronization Markup Language (SyncML) vorgeschlagen. N"aheres hierzu findet sich in \cite{MS04}.}\saveFN\sft\ besteht darin, bestimmte \textbf{Operationen} anderen Operationen vorzuziehen; von zwei konflikt"aren Anweisungen wird immer die ausgef"uhrt, die die h"ohere Priorit"at hat. Bei zwei gleichen, gemeinsam einen Konflikt ausl"osenden Operation, wie zwei z.B. UPDATEs, kann diese Methode keine Priorit"aten setzen. F"ur solche F"alle muss eine eigene Strategie definiert werden.

~\\
Damit sind beide Strategien einfach anzuwenden, jedoch haben sie einen gravierenden Nachteil: Es kann nie mit hundertprozentiger Sicherheit die richtige Entscheidung getroffen werden. Im Fall des Einbringens der falschen "Anderung gehen bei beiden Varianten wichtige Information verloren und der Datenbankbestand wird verf"alscht. Deshalb sollten die auftretenden Konflikte immer protokolliert werden. So kann eine beteiligte Person sp"ater die Log-Dateien durchsehen und auf falsche Entscheidungen hin untersuchen, um diese - m"oglichst direkt in der Replikationsquelle - zu korrigieren.

\paragraph*{Timestamps}~\\
F"ur diese Methode werden vier Arten von Timestamps eingef"uhrt, im Folgenden bezeichnet als:
~\\
\begin{compactitem}
\item Einbringungszeitpunkt:\\
Hierbei handelt es sich um den Zeitpunkt, zu dem der Datensatz\footnote{Der Einfachheit halber wird von einer Konflikterkennung basierend auf Datens"atzen ausgegangen.} zuletzt auf der Quelle ge"andert wurde (direkt oder w"ahrend einer Synchronisation).
\item "Anderungszeitpunkt der Quelle:\\
Die Quelle protokolliert, wann die letzte "Anderung des Datensatzes stattfand. Im Fall einer direkten "Anderung auf der Quelle entspricht dieser Timestamp dem Einbringungszeitpunkt. Im Fall der Reintegration wird der Zeitpunkt von der Senke bestimmt.
\item Synchronisationszeitpunkt:\\
Die Senke merkt sich, wann sie den Datensatz das letzte Mal synchronisiert hat.
\item "Anderungszeitpunkt der Senke:\\
Dieser Timestamp entspricht dem Zeitpunkt, zu dem der Datensatz auf der Senke zuletzt ge"andert wurde.
\end{compactitem}

Bei der Reintegration wird zun"achst der Einbringungszeitpunkt mit dem Synchronisationszeitpunkt verglichen. Gilt "`Einbringungszeitpunkt $>$ Synchronisationszeitpunkt"', wurde der Datensatz zwischenzeitlich von einem anderen Nutzer ge"andert und ein Konflikt ist aufgetreten. Dann wird der bestehende Datensatz der Quelle nur dann durch den neuen Datensatz "uberschrieben, falls gilt "`"Anderungszeitpunkt der Quelle $<$ "Anderungszeitpunkt der Senke"'. Andernfalls wird die "Anderung der Senke verworfen. Damit enth"alt die Quelle immer die aktuellste "Anderung.

Wie bei der Konflikterkennung mittels Timestamps ist bei dieser Methode zu beachten, dass die Replikationsquellen und -senken ein einheitliches Zeitsystem nutzen.

Da immer Daten verloren gehen, sollten analog der Priorit"aten-Strategie zus"atzlich alle Konflikte dokumentiert werden, um Fehlentscheidungen r"uckg"angig machen zu k"onnen.


\paragraph*{Mathematische Funktionen}~\\
Werden mathematische Funktionen zur Konfliktl"osung eingesetzt, bedeutet dies, dass eine Entscheidung dar"uber, welche "Anderung in die Datenbank eingebracht werden soll, anhand von Vergleichen und Berechnungen bestimmter Felder getroffen wird. 
 
~\\
So ist z.B. denkbar, dass die "Anderung eingebracht wird, durch deren Anwendung der Inhalt eines Feldes gr"o"ser / kleiner wird als bei Anwendung der anderen "Anderung (\textbf{Maximum} / \textbf{Minimum}).

~\\
Eine weitere Strategie besteht darin, beim Auftreten eines Konflikts bzgl. eines numerischen Feldes keine Entweder-Oder-Wahl zu treffen, sondern die Differenz der beiden Felder zum bisherigen Wert zu addieren (\textbf{Addition}): 

\texttt{neuer Spaltenwert = alter Spaltenwert + (neuer Wert - alter Wert)}

"Ahnlich wird bei der Durchschnittsmethode gearbeitet. Hier wird f"ur das den Konflikt verursachende, numerische Feld der \textbf{Durchschnitt} des Spaltenwerts und des neuen Wertes gebildet:

\texttt{neuer Spaltenwert = (alter Spaltenwert + neuer Wert)/2}

~\\
Eine weitere Variante der Verwendung mathematischer Funktionen liegt in der Festlegung einer Funktion, wie die zwei konkurrierenden Werte kombiniert in das Feld einzutragen sind (\textbf{Kombination}). Sie findet damit haupts"achlich Anwendung f"ur String-Felder: Die beiden Werte werden durch ein spezielles Zeichen getrennt aneinander geh"angt und in das Datenbankfeld eingetragen. Meist wird den Stringteilen zus"atzlich ein Identifikator zugeordnet, der angibt, von welcher Datenbank der String urspr"unglich stammt.

Nachteilig an all diesen Funktionen ist, dass es kaum m"oglich ist, f"ur jede Spalte eine sinnvolle L"osung zu finden. Wird das Datenbankschema angepasst, muss auch die Konfliktl"osungsstrategie adaptiert werden. Insbesondere muss f"ur jede neue Spalten auch eine neue Strategie festgelegt werden. Konflikte, die durch "Anderung verschiedene Felder entstehen, werden mittels dieser Methode gar nicht gel"ost.

Somit muss die Strategie "`Konfliktl"osung mittels mathematischer Methode"' immer durch andere Ma"snahmen erg"anzt werden.

\paragraph*{Duplikat}~\\
Tritt bei der Duplikat-Strategie\useFN\sft\ ein Konflikt auf, werden beide Aktualisierungen in die Datenbank eingebracht. Das hei"st, die den Konflikt verursachenden Datens"atze werden neu eingef"ugt. 

Dabei kann es passieren, dass durch das Duplizieren der Informationen zwei Datens"atze mit demselben Prim"arschl"ussel entstehen. Dieser Prim"arschl"usselkonflikt muss gel"ost werden (vgl. Abschnitt  \ref{primärduplikate}).

Au"serdem ist die Methode f"ur Daten, die statistisch ausgewertet werden sollen, ungeeignet, da durch den Konflikt die Ergebnisse verf"alscht werden. Die duplizierten Datens"atze werden in den Berechnungen doppelt ber"ucksichtigt.


\paragraph*{Interaktivit"at}~\\
Bei der interaktiven Konfliktl"osung ist die Synchronisationsanwendung auf die Mitarbeit des Nutzers oder eines Systemverantwortlichen angewiesen. Stellt das Programm w"ahrend des Synchronisationprozesses einen Konflikt fest, so wird die anwesende Person direkt benachrichtigt. Diese kann dann aus einer Reihe von verschiedenen Konfliktl"osestrategien diejenige aussuchen, die f"ur diesen konkreten Konflikt am sinnvollsten ist. Als Strategie k"onnten u.a. eine mathematische Funktion, eine Priorit"aten-Methode oder die Duplikat-Technik eingesetzt werden.

Damit kann diese Methode individuell f"ur jeden Konflikt angewendet werden und ist deswegen die sicherste Methode, einen Konflikt richtig aufzul"osen. Problematisch ist allerdings, dass immer eine Person anwesend sein muss, die sich um den Konflikt k"ummert. Ist dies nicht der Fall, stoppt das Programm und wartet, bis der Konflikt manuell gel"ost wird. Dies kann gro"se Verz"ogerungszeiten bedeuten, insbesondere wenn Recherchen bzgl. der Aufl"osung angestellt werden m"ussen.


\subsection{Fehlertoleranz}
\label{kommunikation}

In einem Intermittently Synchronized Database System gibt es neben der zentralen Datenbank und den mobilen Clients einen Server, der die Synchronisationsanfragen der Clients annimmt und bearbeitet. Dabei kommunizieren Client und Server i.d.R. "uber das Internet miteinander.

Hierzu baut der Client zun"achst eine Verbindung zum Server auf, wobei das zugrunde liegende, unsichere Kommunikationsmedium meist durch ein zuverl"assiges Transportprotokoll wie TCP vor den beiden Kommunikationspartner verborgen bleibt. Bei kontinuierlich bestehender Verbindung werden durch dieses Duplikatnachrichten herausgefiltert und verloren gegangene Nachrichten wiederholt angefordert\footnote{Weitere Informationen zum Thema "`zuverl"assige Kommunikation"' und TCP finden sich in \cite{T03}.}.

St"urzen jedoch Server bzw. Client ab oder f"allt das Netz aus, bricht die Verbindung komplett ab. Sp"ater kann dann nicht mehr nachvollzogen werden, ob die Anfrage vom Server bereits (vollst"andig) bearbeitet wurde. 

Im folgenden Abschnitt \ref{quellen} soll untersucht werden, zu welchen Zeitpunkten solch ein Ausfall stattfinden kann, um anschlie"send in Abschnitt \ref{semantik} zu kl"aren, wie verschiedene Anwendungstypen mit einem Kommunikationsabbruch umgehen.


\subsubsection{Kommunikationsfehlerquellen}
\label{quellen}

\cite{TS07}\footnote{Zwar geht es hier um die "`RPC-Semantik bei Fehlern"', dennoch lassen sich die Fehlerarten auch auf entfernte Objektaufrufe "ubertragen. Bei der Kommunikation zwischen dem Client und dem Server macht es keinen Unterschied, ob die Kommunikation "uber echten Nachrichtenaustausch oder entfernte Objektaufrufe und zugeh"orige R"uckgabewerte erfolgt.} unterscheidet f"unf Arten von Kommunikationsfehlerquellen, je nachdem bei welchem Arbeitsschritt der Verbindungsabbruch auftritt:

\paragraph*{Der Client kann den Server nicht finden.}~\\
Der Client versucht zun"achst eine Verbindung zum Server aufzubauen. Dieser Versuch kann jedoch scheitern, wenn der Client im Besitz der falschen Serververbindungsdaten, wie IP-Adresse oder Port, ist, oder er von vornherein keine Netzverbindung hat. Damit wird keine Client-Server-Verbindung aufgebaut und der Client kann seine Anfrage nicht an den Server schicken. Der Server bearbeitet die Anfrage also nicht.

\paragraph*{Die Anforderungsnachricht vom Client an den Server geht verloren.}~\\
Der Client schickt eine Anfrage an den Server. Das Netz f"allt jedoch aus, bevor diese den Server erreicht. Somit bearbeitet der Server die Clientnachricht nicht.

\paragraph*{Der Server st"urzt ab, nachdem er eine Anforderung empfangen hat.}
In Abbildung \ref{fig:fehler}a ist der normale Ablauf einer Serveranfrageverarbeitung dargestellt: Der Server erh"alt eine Anforderung, f"uhrt diese aus und sendet eine Antwort an den Client zur"uck. Nach dem Erhalt kann der Server jedoch jederzeit abst"urzen. Unterteilt nach den Ausfallzeitpunkten ergeben sich damit drei Situationen:
~\\
\begin{compactitem}
\item[1.] Der Server f"allt direkt nach dem Empfang der Anforderung aus, noch bevor er mit Bearbeitung der Clientnachricht beginnt (vgl. Abbildung \ref{fig:fehler}c).
\item[2.] Der Server befindet sich mitten in der Ausf"uhrung, beendet diese jedoch nicht mehr.
\item[3.] Der Server hat die Anfragebearbeitung komplett beendet, st"urzt jedoch ab, bevor er dem Client eine Antwort schicken kann (vgl. Abbildung \ref{fig:fehler}b). 
\end{compactitem}

~\\
Die Bearbeitung der Clientanforderung erfolgt also ganz, teilweise oder gar nicht.

\begin{figure}[htb]
	\centering
	\includegraphics*[width=1\textwidth, viewport=-1 761 400 840] 		{../Diagramme/kommunikation/ausfallarten.pdf}
	\caption[Serverausfall]{Serverausfall: (a) der Normalfall, (b) Absturz nach der Ausf"uhrung, (c) Absturz vor der Ausf"uhrung \cite{TS07}}
	\label{fig:fehler}
\end{figure}

\paragraph*{Die Antwortnachricht vom Server an den Client geht verloren.}~\\
Hat der Server die Anfrage des Clients bearbeitet, sendet er i.d.R. eine Antwort an den Client zur"uck. F"allt gerade in diesem Moment das Netz dauerhaft aus, geht die Antwortnachricht des Servers verloren. 

\paragraph*{Der Client st"urzt ab, nachdem er eine Anforderung gesendet hat.}~\\
Nicht nur der Server kann ausfallen, sondern auch der Client. Tut er dies, nachdem er seine Anfrage an den Server gesendet hat, wei"s er, sobald er wieder einsatzbereit ist, entweder nicht mehr, ob er die Anfrage gesendet hat, oder kann nicht entscheiden, ob seine Anfrage erfolgreich bearbeitet wurde.

Ein weiteres Problem ist, dass der Client evtl. nach Wiederholung seiner Anfrage die Antwort auf seine letzte, unbeendete Anfrage erh"alt. Dies kann zu Interpretationsschwierigkeiten und Irritationen seitens des Clients f"uhren, da er die erhaltene Antwort ggf. falsch verarbeitet.


\subsubsection{Anwendungen und Kommunikationsfehler}
\label{semantik}

Wie schwerwiegend ein Kommunikationsfehler ist, h"angt von der Anwendung ab; darauf, welche Anforderung sie an die Client-Server-Kommunikation stellt. So gibt es die Anwendungen, die unbedingt eine Antwort auf ihre Anfrage ben"otigen bzw. bei denen der Server unbedingt jede Anfrage bearbeiten muss. Es hat jedoch keine negativen Konsquenzen, wenn die Anfrage mehr als einmal vom Server verarbeitet wird, wie dies z.B. bei reinen Leseoperationen der Fall ist. Die L"osung des Problems auftretender Kommunikationsfehler besteht f"ur die Anwendung darin, die Anfrage solange zu wiederholen, bis sie eine Antwort des Servers erh"alt. Bei dieser Art von Anwendungen wird davon gesprochen, eine "`\textbf{At-Least-Once-Semantik}"' garantieren zu m"ussen.

~\\
Bei Anwendungen, die eine "`\textbf{At-Most-Once-Semantik}"' als Kommunikationsgrundlage brauchen, darf eine Anfrage h"ochstens einmal vom Server bearbeitet werden. Es ist jedoch kein Problem, wenn keinerlei Anfragebearbeitung seitens des Servers erfolgt. Denkbar sind solche Semantiken in Systemen, in denen Informationen schnell veralten und bei einer versp"ateten Verarbeitung zu schweren Entscheidungsfehlern f"uhren w"urden. Ein solch schnelllebiges Anwendungsgebiet ist z.B. die Arbeit mit B"orsendaten. 

Tritt ein Kommunikationsfehler auf, so ignoriert solch eine "`At-Most-Once"'-Anwendung die fehlende Antwort und schickt die Anfrage nicht erneut. Evtl. stellt sie aber zu einem sp"ateren Zeitpunkt eine andere, an die neue Situation angepasste Anfrage.

~\\
Die dritte Art von Semantik ist die "`\textbf{Exactly-Once-Semantik}"'. Hierbei muss garantiert werden, dass die Anfrage eines Clients vom Server genau einmal bearbeitet wird. Anwendung findet diese Semantik z.B. beim Speichern von Dateien. Der Nutzer erwartet, dass seine Datei auf dem Server gespeichert wird, aber auch, dass seine eigentlich bereits gespeicherte Datei nicht eine zwischenzeitlich gemachte, neuere Version beim nochmaligen Speichern "uberschreibt.

F"ur diese Art von Anwendungen sind Kommunikationsfehler besonders schwierig zu behandeln. Zum Einen muss sie zwischen den drei Fehlern "`Server wurde nicht gefunden"', "`Anfrage ging verloren"' sowie "`Server fiel nach Erhalt der Anfrage aus"' auf der einen Seite und den vier Fehlerarten "`Server fiel nach Bearbeitung der Anfrage aus"', "`Server fiel nach Erstellung der Antwort aus"', "`Antwort ging verloren"' sowie "`Client fiel nach Bearbeitung aus"' auf der anderen unterscheiden k"onnen. Im ersten Fall muss die Anfrage wiederholt werden, im zweiten darf sie es nicht. 

Zum Anderen muss f"ur s"amtliche Bearbeitungsschritte des Servers festgelegt werden, wie mit einem Serverausfall verfahren werden soll. Es stellt sich die Frage, ab welchem Schritt die Anfrage als bearbeitet gilt und wie im Abbruchsfall die einzelnen Schritte wieder r"uckg"angig gemacht werden k"onnen. F"ur den letzten Fall muss es also so eine Art Undo-Funktion geben. Damit ist es oftmals unm"oglich, eine Exactly-Once-Semantik f"ur alle Arten von Kommunikationsfehlern zu garantieren.

Um zu unterscheiden, ob eine Anfrage bereits bearbeitet wurde oder nicht, k"onnen Nachrichtenidentifikatoren eingesetzt werden. Diese schickt der Client mit jeder Anforderungsnachricht zum Server, der diese nach Bearbeitung als "`erledigt"' abspeichert. Bekommt er nun eine Anfrage mit einem Identifikator, den er schon gespeichert hat, erkennt der Server die Nachricht als Duplikat und bearbeitet die Anfrage nicht erneut. Stattdessen schickt er direkt eine Antwort. Somit kann der Client seine Anfrage so oft wiederholen, bis er eine Antwort vom Server erh"alt.

Problematisch an diesem Ansatz ist, dass die Nachrichtenidentifikatoren dauerhaft gespeichert werden m"ussen, d.h. bei einem Serverabsturz darf der Server nicht vergessen, welche Nachrichten er bereits verarbeitet hat, und bei einem Clientabsturz darf der Client nicht vergessen, welche Nachrichtenidentifikatoren bereits vergeben wurden. Au"serdem m"ussen serverseitig der letzte Schritt der Nachrichtenverarbeitung und das Speichern des zugeh"origen Identifikators atomar erfolgen, was sich oftmals als sehr schwierig erweist. 

~\\
Diese Kommunikationssemantiken werden anhand eines Beispiels in \cite{TS07} unter Einbeziehung der einzelnen Kommunikationsfehler genauer erl"autert.
