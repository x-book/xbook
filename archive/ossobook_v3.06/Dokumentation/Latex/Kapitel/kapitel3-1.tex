\chapter{Replikation und Synchronisation in der Datenbank-Theorie}
\label{theorie}

Bevor ein System implementiert wird, sollten sich die Entwickler damit be\-sch"af\-ti\-gen, ob es bereits theoretische Betrachtungen zum Thema gibt, in denen z.B. Probleme beschrieben und L"osungen bzw. L"osungsans"atze vorgestellt werden. Dadurch werden weniger Probleme "ubersehen, die evtl. sp"ater zu schwerwiegenden Fehlern f"uhren. Durch Betrachtung der vorgestellten L"osungen k"onnen Vor- und Nachteile analysiert werden, um anschlie"send einen L"o\-sungs\-an\-satz, der f"ur das System passend ist, bei der Implementierung umzusetzen. Ziel ist es so, m"oglichst einfache, fehlerfreie Programme zu entwickeln, ohne das Rad jedesmal neu zu erfinden.

Deswegen werden in diesem Kapitel, in Abschnitt \ref{datenbankmodell}, zun"achst verschiedene theoretische Datenbankmodelle betrachtet, insbesondere ihre Definitionen, um das zuk"unftige Ossobook-System einzuordnen. Danach werden in Abschnitt \ref{problemklassen} Probleme betrachtet, die bei der Implementierung des Ossobook-Systems auftreten und sp"ater gel"ost werden m"ussen.


\section{Zuordnung des Ossobook-Systems zu einem theoretischen Datenbankmodell}
\label{datenbankmodell}

Ein "`normales"' Datenbanksystem besteht aus dem DBMS\footnote{Datenbankmanagementsystem} und dem physischen Speicherort der Daten. Dabei liegen die Daten zun"achst nicht repliziert vor und es gibt gut erforschte L"osungen f"ur auftretende Probleme wie z.B. "`Anomalien im Mehrbenutzerbetrieb"' \cite{HR01}. Sobald die Daten jedoch auf andere Datenbanken kopiert werden, sind diese L"osungen nicht mehr einsetzbar, um ein konsistentes Datenbanksystem zu erhalten. 

Deswegen m"ussen solche Systeme separat betrachtet, Probleme identifiziert und L"osungen gefunden werden. So entwickelten sich zwei weitere Datenbankmodelle: das "`verteilte Datenbanksystem"' und das "`mobile Datenbanksystem"'.

In diesem Kapitel werden in den Abschnitten \ref{verteilt} und \ref{mobil} diese beiden Datenbanksysteme unter dem Gesichtspunkt betrachtet, ob das Ossobook-System eine Auspr"agung eines dieser Modelle darstellt, wobei die Zuordnung anhand jeweils einer Definition erfolgt.

\subsection{Verteilte Datenbanksysteme}
\label{verteilt}

In \cite{org} wird eine \textbf{verteilte Datenbank} (DDB) folgenderma"sen definiert: \\
\hypertarget{ddbs}{"`logisch zusammengeh"orende Daten sind
in physisch verschiedenen,
durch ein Netz verbundenen Rechnern gespeichert,
werden aber gemeinsam verwaltet.}
Die Administration "ubernimmt ein DDBMS\footnote{verteiltes Datenbankmanagementsystem}, das ein 'Softwaresystem' ist, das eine verteilte Datenbank verwaltet und die transparente Verteilung f"ur den Benutzer "ubernimmt"' \cite{ES02}.

Bei den Ossobook-Datenbanken handelt es sich um mehrere Datenbanken, n"amlich um eine zentrale und viele lokale. Diese Datenbanken befinden sich an unterschiedlichen Orten, so dass die Daten des Systems auf "`physisch verschiedenen Rechnern"' gespeichert werden. Die Daten geh"oren logisch zusammen, da von Zeit zu Zeit immer wieder ein Datenabgleich zwischen einer der lokalen Datenbanken und der globalen erfolgt, wobei die Konsistenz des Systems gewahrt werden muss. Zum Datenabgleich verbinden sich die lokalen Datenbanken zeitweilig mit der globalen Datenbank; die Rechner sind also nicht immer "uber ein Netz miteinander verbunden. 

Um aber alle Datenbanken von einem DDBMS verwalten zu lassen, ist eine best"andige Kommunikation zwischen allen Rechnern notwendig, denn das DDBMS ist u.a. f"ur die Nebenl"aufigkeitskontrolle verantwortlich. In \cite{ES02} werden verschiedene Methoden\footnote{Weitere Techniken zur Koordination und Nebenl"aufigkeitskontrolle in verteilten Systemen, auch anwendbar auf DDB-Systeme, finden sich z.B. in \cite{DKC05}} zur Wahrung der Konsistenz bei Zugriffen auf verschiedene Kopien in einem verteilten Datenbanksystem vorgestellt. 

Z.B. gibt es die \textbf{Prim"arknotentechnik}, bei der ein Rechner als Koordinationsknoten f"ur alle Datenbankobjekte gew"ahlt wird. Dieser Knoten verwaltet alle Sperren: Alle anderen Rechner senden Sperr- und Entsperranfragen an ihn, wenn sie auf ein Datenobjekt zugreifen bzw. dieses wieder freigeben wollen. Der Koordinator bestimmt bei mehreren Sperranforderungen verschiedener Knoten quasi zur selben Zeit, in welcher Reihenfolge diese zugreifen d"urfen. Er sperrt das Datenobjekt sowie alle Kopien und informiert den Gewinnerknoten, der dann das Datenobjekt "andert.

Eine weitere Technik zur Konsistenzerhaltung ist die \textbf{verteilte Nebenl"aufigkeitskontrolle durch Abstimmung}. Hierbei sendet ein Knoten eine Sperranfrage an alle anderen Knoten, die eine Kopie des betreffenden Objekts besitzen. Die anderen Knoten antworten dem anfragenden Knoten mit einer Best"atigungsnachricht oder schweigen. Erh"alt der anfragende Knoten von der Mehrheit Best"atigungen, informiert er alle anderen Knoten dar"uber, dass er das Objekt jetzt benutzen wird. Dann setzen alle Knoten eine Sperre f"ur ihre Kopie, die sie selbst verwalten. Bekommt der anfragende Knoten nicht gen"ugend Best"atigungen, annuliert er seine Anfrage und bearbeitet das Datenobjekt nicht. 

Bei beiden Methoden ist eine best"andige Kommunikation zwischen den einzelnen Rechnern notwendig. Bricht die Verbindung ab - egal, ob gewollt oder ungewollt - wird dies als Fehlerfall behandelt. Dann wird die lokale Datenbank des ausgefallenen Knotens bei seiner Wiederherstellung auf den Stand der anderen Datenbanken gebracht. Alle zwischenzeitlich gemachten lokalen "Anderungen des Nutzers werden "uberschrieben.

Somit sind die Konzepte zur Fehlerbehandlung und Nebenl"aufigkeitskontrolle f"ur das Ossobook-System ungeeignet. Es kann hier kein DDBMS geben, das die Zugriffe auf alle Datenbanken koordiniert. Damit bilden die globale und die lokalen Datenbanken kein verteiltes Datenbanksystem.

\subsection{Mobile Datenbanksysteme}
\label{mobil}
 
\cite{MS04} definiert ein \textbf{mobiles Datenbanksystem} als  "`Small-Footprint-Daten\-bank\-system\footnote{"`Datenbanksystem mit kleinerem eigenen Speicherbedarf und angepasster Funktionalit"at"' \cite{MS04}}, das auf einem mobilen Client (beispielsweise einem PDA) installiert ist und auf diesem Daten persistent in einer strukturierten Weise speichert. Ein mobiles Datenbanksystem ist an die speziellen Bed"urfnisse und Anforderungen der leistungsschwachen mobilen Clients angepasst."'  

Demnach handelt es sich bei den Ossobook-Datenbanken auf den Laptops um mobile Datenbanksysteme. Hier werden nur die f"ur den entsprechenden Nutzer relevanten Projektdaten dauerhaft und strukturiert gespeichert. Es wird weniger Speicherplatz ben"otigt als bei der zentralen Ossobook-Datenbank.

"`In der Praxis werden oftmals Daten einer zentralen Datenbank auf einen mobilen Client repliziert, damit auch im Disconnected Mode\footnote{"`Ein mobiler Client befindet sich im Disconnected Mode, wenn er vom Festnetz getrennt ist."' \cite{MS04}} eine weitere Datenverarbeitung unabh"angig von einer bestehenden Netzverbindung m"oglich ist. Auf dem Client durchgef"uhrte "Anderungen m"ussen dann sp"ater mit den Originaldaten der zentralen Datenbank wieder abgeglichen werden."', hei"st es dann weiter. Genau dieser Effekt wird f"ur das neue Ossobook-System gew"unscht: Verschiedene Projekte werden auf die lokalen Datenbanken repliziert. Befindet sich ein Laptop, also ein mobiler Client, im Disconnected Mode, so k"onnen die Daten trotzdem weiter verarbeitet werden. Sp"ater, wenn wieder eine Internetverbindung besteht, spielt der Client seine Daten"anderungen in die zentrale Datenbank ein und "ubernimmt die zentralen "Anderungen in seine lokale Datenbank.

Ein solches Datenbanksystem wird "`\textbf{Intermittently Synchronized Database System"'} genannt \cite{ES02}. Es besteht aus einer zentralen Datenbank, mehreren mobilen Datenbanksystemen und einem Server. Der Server ist f"ur den korrekten Austausch der zentralen und lokalen "Anderungen verantwortlich. Hierzu nimmt er zun"achst die lokalen "Anderungen eines Clients entgegen und bringt diese - ggf. mit einer vorherigen Verarbeitung - in die globale Datenbank ein. Anschlie"send liest er eine clientspezifische Kopie der zentralen "Anderungen aus der globalen Datenbank aus und gibt diese an den Client zur"uck.

Zusammenfassend ergeben sich damit die folgenden, wesentlichen Merkmale eines Intermittently Snychronized Database System:
~\\
\begin{compactitem}
\item Ein Client kommuniziert mit dem Server, um an diesen Aktualsierungen zu senden, von ihm "Anderungen zu empfangen oder Transaktionen zu verarbeiten, f"ur die nicht lokale Daten notwendig sind.
\item Die Kommunikation erfolgt auf Wunsch des Clients. Der Server kann keine Verbindung initiieren, da der Client i.d.R. nicht online ist.
\item Im Gegensatz zu "`normalen"' mobilen Datenbanksystemen, bei denen Clients immer mit dem Netz verbunden sind, spielen Energierspar"uberlegungen keine Rolle. 
\item Der Client kann Daten und Transaktionen selbst verwalten, solange er nicht mit dem Server verbunden ist. Es gibt (zumindest in begrenztem Umfang) die M"oglichkeit einer eigenen Fehler-Recovery.
\end{compactitem}

~\\
Das neue Ossobook-System erf"ullt, dass die mobilen Clients meist offline sind und deswegen eine Kommunikation immer durch sie initiiert wird, dass Energiespar"uberlegungen keine Rolle spielen und dass die mobilen Clients lokal selbst f"ur die Daten- und Transaktionsverwaltung sowie die Fehler-Recovery zust"andig sind.

Um die Daten"anderungen zwischen zentraler und globaler Datenbank austauschen zu k"onnen, ist ein Server notwendig, der den Zugriff auf die zentrale Datenbank kontrolliert (vgl. Kapitel \ref{vorbereitend} und \ref{fundierung}) und das Konfliktmanagement "ubernimmt (vgl. Kapitel \ref{fundierung}).

Mit der Einf"uhrung des Servers ist auch der erste Punkt der Merkmale eines Intermittently Synchronized Database System erf"ullt, womit es sich beim neuen Ossobook-System um ein solches handelt.

F"ur das Ossobook-System ergibt sich die in Grafik \ref{fig:systemarchitektur} dargestellte \textbf{Systemarchitektur}. Neben der zentralen Datenbank und dem Server gibt es zwei Sorten von Clients: Die nicht mobilen Clients haben kein mobiles Datenbanksystem und k"onnen nur direkt, vom Intranet aus mit der Datenbank arbeiten. Die mobilen Clients haben eine lokale Datenbank, mit der sie arbeiten, wenn sie offline sind. Sp"ater tauschen sie die "Anderungen der eigenen lokalen Datenbank mit den "Anderungen der zentralen Datenbank "uber den Server aus. Hat ein mobiler Client Internetzugang, kann er direkt mit der zentralen Datenbank arbeiten. "Anderungen werden dann direkt auf dieser vorgenommen und nicht mit der lokalen abgestimmt.

\begin{figure}[htb]
		\centering
		\includegraphics[width=0.8\textwidth,clip,viewport=85 222 523 614]{../Diagramme/theorie/systemarchitektur.pdf}
	\caption{Systemarchitektur des Ossobook-Systems}
	\label{fig:systemarchitektur}
\end{figure} 
 




