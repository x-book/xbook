\chapter{Einleitung}
\label{einleitung}

"`Als ich einmal gefragt wurde, was ich mache, antwortete ich: \glq Ich arbeite mit M"ull. Ich bin der wohl best bezahlteste M"ullmann in M"unchen.\grq"'\footnote{sinngem"a"se, nicht w"ortliche Wiedergabe einer Aussage von Herrn Professor Peters}, erz"ahlte mir Herr Professor Dr. rer.nat. Dr.med.vet.habil. Joris Peters, Leiter des Instituts f"ur Pa\-l"ao\-ana\-to\-mie und Geschichte der Tiermedizin, mit einem Zwinkern im Auge. Dann entwickelte sich ein Gespr"ach "uber die Forschung am Institut und "uber den Begriff "`Pal"aoanatomie"'.

Der erste Abschnitt dieser Einleitung (Kapitel \ref{institut}) f"uhrt den Begriff "`Pa\-l"ao\-ana\-to\-mie"' und erl"autert, welche Forschungsziele diesbez"uglich am Institut verfolgt werden. Dadurch wird der Grundstein f"ur das Verst"andnis gelegt, welchem Zweck diese Diplomarbeit dient. Kapitel \ref{aufgabe} stellt diesen dann im Einzelnen dar, indem er die  w"ahrend dieser Diplomarbeit zu l"osenden Teilaufgaben beschreibt. Abschlie"send gibt Kapitel \ref{inhalt} noch einen kurzen "Uberblick "uber den Inhalt der Arbeit und somit, wie an die L"osung der Gesamtaufgabe herangegangen wurde.


\section{Pal"aoanatomie an der LMU}
\label{institut}
"`\textbf{Pal"aoanatomie}"' ist der Begriff, unter dem an der LMU die "`Bearbeitung von Tier"uberresten"' \cite{B94} verstanden wird. Im Rest Deutschlands und in Europa wird diese Wissenschaftsdiszilpin jedoch mit Arch"aozoologie\footnote{aus dem Amerikanischen kommend manchmal auch Zooarch"aologie} bezeichnet. Somit handelt es sich bei der Nomenklatur "`Pal"aoanatomie"' um eine lokale Besonderheit.

Die Arbeitsgrundlage der Pal"aoanatomie bilden bei arch"aologischen Ausgrabungen anfallende Tierfunde jeglicher Art. Hierzu geh"oren neben Knochen auch Muschelschalen, Schneckenh"auser, Fischschuppen, Leder, Haare, Fell- und Hornreste. 

Da die Pal"aoanatomie Hand in Hand mit der Arch"aologie geht, ist sie klar abzugrenzen von der \textbf{Pal"aontologie}, die sich "`mit der Evolution pflanzlicher und tierischer Organismen seit der Entstehung einfachsten Lebens vor etwa 3,8 Milliarden Jahren bis zum Homo sapiens"' \cite{Pal} besch"aftigt. Pa\-l"ao\-ana\-to\-mi\-sche Fragestellungen hingegen werden immer in Zusammenhang mit der \textbf{Entwicklung des Menschen} gesehen, den es erst seit c.a. 2,5 Millionen Jahren vor Christus gibt. Damit erstreckt sich der in der Pal"aoanatomie zu erforschende Zeitraum vom \textbf{Pal"aolithikum} (Altsteinzeit) bis in die \textbf{Neuzeit}. 

Mithilfe einer Analyse der Tier"uberreste k"onnen \textbf{wirtschafts-} und \textbf{kultur\-ge\-schicht\-li\-che Aussagen} "uber die Entwicklung des Menschen gemacht, aber auch (in begrenztem Ma"se) \textbf{zoologische}, \textbf{"okologische} und \textbf{tierz"uchterisch-haus\-tier\-kundliche Fragestellungen} beantwortet werden.

So l"asst sich z.B. aus der Menge an Wild- und Haustierknochen, die in den von Menschen hinterlassenen Abf"allen gefunden wurden, feststellen, welche wirtschaftliche Bedeutung die Haustierhaltung f"ur die Menschen hatte, ob sie haupts"achlich von der Jagd oder der Tierzucht lebten. Abbildung \ref{fig:fundzahlen} z.B. stellt die f"ur den Menschen wichtigsten Fleischtiere nach Anzahl gegen"uber. Es wurde untersucht, wie sich der Wildanteil in den Funden einer Aus\-gra\-bungs\-st"at\-te in Thessalien (Griechenland) von der Diminizeit (ca. 4. Jahrtausend v.Chr.) bis zur Mykenischen Zeit (ca. 11. Jahrhundert v.Chr.) ver"anderte.

\begin{figure}[htbp]
	\centering
	\includegraphics*[width=1\textwidth, viewport=41 208 488 792] {../Diagramme/pal�oanatomie/fundzahlen.pdf}
	\caption[Statistische Auswertung von Tiervorkommen]{Magula Pevkika, Thessalien. Prozentanteile der wichtigsten Fleischtiere nach Fundzahlen, Mindestindividuenzahlen und Knochengewichten in der Zeitstufenfolge Diminizeit (Di), Rachmanizeit (Ra), Fr"uhbronzezeit (FB), Mittelhelladische Zeit (MH) und Mykenische Zeit (My). \cite{H78}}
	\label{fig:fundzahlen}
\end{figure}

Zu den kulturgeschichtlichen Fragestellungen geh"ort u.a. das Thema, wie es dazu kam, dass sich die Menschen Haustiere halten. So l"asst sich z.B. durch viele verschiedene Ausgrabungen an unterschiedlichen Orten feststellen, seit wann es (mindestens) schon das Hausschwein gibt oder wo mit der Haltung von K"uhen (wahrscheinlich) begonnen wurde. So zeigt Abbildung \ref{fig:Haustiernachweise} ein Domestikations-Zeit-Diagramm. Hieraus l"asst sich ablesen, f"ur welche Gebiete der Nachweis eines bestimmten Haustiers erbracht wurde und von wann der fr"uheste Fund stammt.

\begin{figure}[htbp]
	\centering
	\includegraphics*[width=1\textwidth, viewport=45 348 539 816] 		{../Diagramme/pal�oanatomie/haustiernachweise.pdf}
	\caption{Die fr"uhesten Haustiernachweise \cite{H78}}
	\label{fig:Haustiernachweise}
\end{figure}

~\\
Voraussetzung f"ur die Beantwortung der oben genannten Fragestellungen ist die Bestimmung der Tierart. Es stellt sich die Frage, zu welcher Speizies der gefundene "Uberrest geh"ort. Hierbei bildet die Gegenwart den Ausgangspunkt: "Uber den Vergleich mit den Knochen (oder anderem tierischen Material) heute noch existierender Tiere kann festgestellt werden, um welche Tierart es sich vor Jahrhunderten oder sogar Jahrtausenden gehandelt hat oder zumindest zu welchem heute vorkommenden Tier eine Verwandtschaft besteht. Eine der wichtigen Aufgaben ist es deswegen, Vergleichsdatensammlungen anzulegen. Hier werden verschiedene Merkmale heute auftretender Tierarten gespeichert. Am Institut f"ur Pa\-l"ao\-ana\-to\-mie und Geschichte der Tiermedizin wird diese Sammlung, \textbf{Animal Diversity Collection} genannt, best"andig erweitert, da verschiedene Zoologische G"arten Tierleichen spenden und die Mitarbeiter des Instituts Expeditionen durchf"uhren, von denen sie weitere Tierskelette mitbringen.

Eine tierartliche Bestimmung ist nicht immer einfach. V.a. zu Beginn der Domestikation ist es schwierig zwischen Wild- und Haustieren zu unterscheiden. Aber auch sp"ater kann sich die Festlegung der Tierart diffizil gestalten. Das betrifft z.B. die Hausgefl"ugelarten Gans, Ente und Taube, da die Unterschiede zwischen Wild- und Haustier nicht sehr ausgepr"agt sind. Damit ist "`eine Trennung der Knochen nach Haus- und Wildform"' [\punkte] "`nicht sicher m"oglich"' \cite{B94}. In diesen F"allen k"onnen Aussagen zur Bestimmung allenfalls aufgrund der Umst"ande des Fundes getroffen werden. So kann z.B. eine gro"se Menge von Fundst"ucken derselben Tierart darauf hindeuten, dass die Menschen diese Tiere gehalten und gez"uchtet haben. 
~\\
\begin{table}[htb]
\begin{threeparttable}
	\centering
	\begin{tabular}{p{0.38\textwidth}|p{0.58\textwidth}}
		\hline
		\textbf{Ergebnisform} & \textbf{Hauptaussagerichtung}\\
		\hline
		Artbestimmung & genutzte Tierarten\\
		\hline
		H"aufigkeit der Arten (Fundzahl, Mindestanzahl der Individuen, Fundgewichte) & Verh"altnis von Haustierhaltung, Jagd, Fischfang und Sammelwirtschaft; wirtschalftliche Bedeutung der Haustierarten\\
		\hline
		H"aufigkeit der Elemente bei den einzelnen Arten & Nutzung und Verbleib von K"orperteilen (Produktion, Konsumtion), gewerbliche Nutzung von Tierprodukten (Knochen, Horn, Fell u.a.)\\
		\hline
		Bestimmung der Knochenfragmentierung sowie Analyse der Zerlegungsspuren & Schlachttechnik, Verbrauchsgepflogenheiten\\
		\hline
		Altersgliederung und Bestimmung des Geschlechterverh"altnisses & Haltungs- und Nutzungsziele, Herdenstruktur (Haltungsstrategie)\\
		\hline
		Osteometrische Daten (Knochenma"se) & Ph"anotyp\tnote{1} der Haustiere (Gr"o"se, Wuchsform usw.), Praxis der Tierz"uchtung\\
		\hline
		 Bestimmung von anatomisch-pathologischen Ver"anderungen & Krankheitsbelastung, Haltungsbedingungen\\
		\hline
	\end{tabular}	
	\begin{tablenotes}\footnotesize 
		\item[1]	"`Ph"anotyp:"'  [\punkte] "`Erscheinungsbild eines Lebewesens; es entsteht aus dem Zusammenwirken der Erbanlagen (Gene) und der Umweltbedingungen"'\cite{ph�} 
	\end{tablenotes}\footnotesize 	
	\caption{Ergebnisformen arch"aozoologischer Materialanalysen und daraus ableitbare Aussagen zur Haustierhaltung \cite{B94}}
	\label{tab:Haustierhaltung}
	\end{threeparttable}
\end{table}

War eine tierartliche Bestimmung am Institut f"ur Pal"aoanatomie und Geschichte der Tiermedizin erfolgreich, werden die Merkmale des Fundst"ucks in der extra f"ur diesen Zweck entwickelten Datenbank, \textbf{Ossobook} genannt, gespeichert. Tabelle \ref{tab:Haustierhaltung} fasst einen Teil der grunds"atzlich bestimmbaren Merkmale von Tierfunden und daraus ableitbarer Aussagen zusammen. Das Schema der am Institut verwendeten Datenbank und die speicherbaren Informationen werden in Kapitel \ref{ossobook-datenbank} genauer beschrieben.
 
~\\
Insgesamt entsteht so am Institut eine gro"se Datensammlung, mithilfe der verschiedene Fragestellungen zur Nutzung, zur Haltung und zur Bedeutung der Tiere in der menschlichen Gesellschaft zum Fundzeitpunkt gekl"art werden k"onnen. So beschreibt z.B. eine der neuesten wissenschafltichen Ver"offentlichungen, an der auch Herr Professor Joris Peters gearbeitet hat, neue Erkenntnisse "uber die Domestikationsgeschichte des Esels. \cite{PE08} fasst hierbei die wesentlichen Punkte zusammen: In "Agypten wurden alte Eselskelette gefunden, die vor 5000 Jahren in einer eigenen, an einen Grabkomplex eines Pharaos angrenzenden Grabkammer beigesetzt wurden. Anhand der Feststellung "`spezifisch pathologisch-anatomischer Ver"anderungen an bestimmten K"orperstellen"' \cite{PE08} konnte geschlussfolgert werden, dass das Tier dem Tragen von Lasten diente; dass die Esel beerdigt wurden, beschreibt den Stellenwert des Tieres f"ur den K"onig. "Agyptologen interpretieren die Beisetzung so, dass die Eselherde dem K"onig auch in seinem n"achsten Leben zu Diensten sein sollte. Im Alten "Agypten trug die Nutzung des Esels als Last- und Transporttier wahrscheinlich stark dazu bei, dass sich der Handel in Afrika und im westlichen Asien ausweitete. 

An diesem Projekt l"asst sich gut sehen, dass Wissenschaftler der Pal"aoanatomie oft eng mit Kollegen anderer Wissenschaftsdisziplinen zusammenarbeiten - hier waren es neben "Agyptologen und Arch"aologen auch Mathematiker und Tiermediziner.


\section{Aufgabenstellung und Zielsetzung}
\label{aufgabe}

Momentan befinden sich die Mitarbeiter des Insituts vor Ort an der LMU, wenn sie Daten der letzten Ausgrabungen in die Datenbank eintragen. Ein Datenbankzugriff "uber das Internet ist nicht m"oglich. Die Mitarbeiter m"ussen deswegen ihre Daten zun"achst anderweitig dokumentieren und k"onnen sie erst nach ihrer R"uckkehr von der Ausgrabungsst"atte in der Datenbank abspeichern. Dabei besteht neben einem Arbeitsmehraufwand auch die Gefahr, dass Daten verlorengehen. W"ahrend von der Ossobook-Datenbank regelm"a"sig ein Backup erstellt wird, m"ussen die Nutzer beim "`Zwischenspeichern"' auf Bl"ocken und in Laptop-Dateien ab und zu einen Verlust hinnehmen.

Deswegen soll die Datenbank zuk"unftig auch "uber das Internet erreichbar sein. Mitarbeiter k"onnen dann ihre Daten direkt an den Ausgrabunsst"atten eingeben, egal wo sich diese befinden. Dabei ist jedoch zu bedenken, dass nicht "uberall Internet zur Verf"ugung steht bzw. dieses kosteng"unstig genutzt werden kann.

Deswegen ist es \textbf{Ziel} dieser Diplomarbeit, einen Mechansimus bereitzustellen, der den automatischen Austausch von Daten"anderungen erm"oglicht. Jeder Mitarbeiter, der an Ausgrabungen beteiligt ist und mit der Ossobook-Datenbank arbeiten muss, soll einen Laptop bekommen, auf dem er eine eigene Datenbank hat mit dem Schema, das dem der globalen Datenbank entspricht. 

Hat der Mitarbeiter mangels Internetzugang nicht die M"oglichkeit, mit der globalen Datenbank zu arbeiten, kann er seine lokale nutzen und "Anderungen an den dort gespeicherten Daten vornehmen. Sobald er wieder eine Verbindung zur globalen Datenbank aufbauen kann, tut er dies und startet eine Funktion "`\textbf{synchronisieren}"'. Hierdurch werden seine lokalen Projektaktualisierungen in die globale Datenbank "ubernommen. 

Diese Synchronisationsfunktion kann der Nutzer auch verwenden, um die Kos\-ten f"ur das "Ubertragen der "Anderungen zu verringern. Speichert er alle "Anderungen zun"achst lokal, um sie anschlie"send zu synchronisieren, reduzieren sich Zeit und Datenmenge, da w"ahrend der Kommunikation weniger Anfragen und Antworten gesendet werden m"ussen.

~\\
Zus"atzlich soll die Funktion "`synchronisieren"' die "Anderungen der globalen an die lokale Datenbank schicken. Startet der Nutzer diese, bekommt er die Aktualisierungen seiner Kollegen. Damit stehen ihm am Ausgrabungsort immer relativ aktuelle Daten zur Verf"ugung, selbst wenn er Monate unterwegs ist.

~\\
Da die Datenbank "uber das Internet erreichbar sein muss, kann die Firewall nicht mehr genutzt werden, den Zugang zur Datenbank komplett zu untersagen, wie sie es momentan tut. Deswegen wird es keinen "`Default-User"' mehr geben, mithilfe dessen sich heute alle Mitarbeiter an der Datenbank anmelden. Stattdessen bekommt jeder User seinen eigenen Benutzernamen und sein eigenes Passwort. So wird es Unberechtigten erschwert, unerlaubt auf die Datenbank zuzugreifen, zumal die \textbf{Identifikation} gleichzeitig dazu genutzt werden wird, Usern \textbf{Schreib-} bzw. \textbf{Leseberechtigungen} f"ur bestimmte Projekte zu gew"ahren. 

Erzeugt ein Nutzer ein neues Projekt, so wird er zu dessen \textbf{Projektleiter}. Der Projektleiter hat das Recht, Schreib- und Leseberechtigungen f"ur sein Projekt an andere User zu vergeben bzw. diese wieder zu entziehen. Dabei beinhaltet die Schreibberechtigung neben dem Lesen und "Andern der Projektdaten auch das L"oschen und Einf"ugen von Daten. Zuk"unftig soll er dann einem, mehreren oder allen Usern den Zugriff auf sein Projekt erlauben oder auch wieder verbieten k"onnen. 

Ein Projekt besteht aus Informationen zum Projekt selbst und aus Informationen "uber gefundene Objekte. Wie in Abschnitt bereits \ref{institut} dargestellt, werden die Objekte durch Merkmale beschrieben, deren Definitionen ebenfalls in den Datenbanken gespeichert werden. Um eine einheitliche Verwendung der Merkmale und damit auch ihre Vergleichbarkeit zu gew"ahrleisten, sollen diese nur durch \textbf{Administratoren} definiert und zentral abgespeichert werden k"onnen. Deswegen sollen "Anderungen dieser Merkmale nur auf der zentralen Datenbank vorgenommen und w"ahrend der Synchronisation nur die projektbezogenen, lokalen "Anderungen auf die globale Datenbank "ubertragen werden.

Neben dem Recht, Merkmalsdefinitionen zu "andern, einzuf"ugen und zu l"oschen, haben nur die Administratoren das Recht, das globale Datenbankschema zu modifizieren. Auch diese globalen Datenbankschema"anderungen sollen w"ahrend der Synchronisation auf die lokalen Datenbanken "ubertragen werden. "Anderungen an den lokalen Schemata sollen hingegen nicht in die globale Datenbank "ubernommen werden.

Ein Administrator steht bzgl. der Rechteverwaltung "uber einem Projektleiter, d.h., er soll alle Projekte einsehen d"urfen und "Anderungen an ihnen vornehmen k"onnen. Au"serdem soll er Projektrechte an andere User vergeben oder ihnen diese wieder entziehen k"onnen. So kann garantiert werden, dass es immer m"oglich ist, auf ein Projekt zuzugreifen, selbst wenn der Projektleiter einmal nicht verf"ugbar ist.


\section{Kurzer "Uberblick "uber den Inhalt}
\label{inhalt}

Die Konzeption und Implementierung der in Abschnitt \ref{aufgabe} formulierten Aufgabenstellung\footnote{Die in Zusammenarbeit mit den Mitarbeitern des Instituts f"ur Pal"aoanatomie und Geschichte der Tiermedizin erfassten Aufgaben sind in Anhang \ref{anforderungsanalyse} nochmals kurz und b"undig niedergeschrieben.} erfordert zu\-n"achst die Betrachtung des vorhandenen Ossobook-Systems, also der vorhandenen Ossobook-Datenbank und der vorhandenen Ossobook-Clinent-Software. Hiermit besch"aftigt sich Kapitel \ref{system} "`\textbf{Das bestehende Ossobook-System}"'. 

Um ein sinnvolles Konzept f"ur die Erweiterung des Ossobook-Systems um den automatischen Datenaustausch erstellen zu k"onnen, wird in Kapitel \ref{theorie} "`\textbf{Replikation und Synchronisation in der Datenbank-Theorie}"' anschlie"send eine Zuordnung des neuen Ossobook-Systems zu einem theoretischen Datenbankkonzept vorgenommen. Hierbei werden dann zugleich sich aus der Art des Datenbanksystems ergebende Fragestellungen betrachtet, die sp"ater f"ur das Ossobook-System zufriedenstellend beantwortet werden m"ussen.

In Kapitel \ref{praxis} "`\textbf{Betrachtung von Synchronisations- und Replikationsmechanismen existierender Datenbanksysteme}"' werden beispielhaft mehrere in der Praxis eingesetzte Datenbankserver daraufhin untersucht, ob sie den automatischen Datenaustausch im Ossobook-System "ubernehmen k"onnten. Dies beinhaltet insbesondere den MySQL-Server 5.0, der im erweiterten Ossobook-System weiterhin f"ur die Ossobook-Datenbank verantwortlich sein wird.

Die Kapitel \ref{vorbereitend} "`\textbf{Vorbereitende "Anderungen der Ossobook-Software}"' und \ref{fundierung} "`\textbf{Implementierung der Synchronisation}"' beschreiben die Anpassungen des Ossobook-Datenbankschemas und der Ossobook-Software. Dabei widmet sich Kapitel \ref{vorbereitend} zun"achst den allgemeinen Aspekten der Software, insbesondere auch der Umsetzung der geforderten Projektrechte. Kapitel \ref{fundierung} handelt dann von der eigentlichen Implementierung des automatischen Datenaustausches. Dabei wird u.a. ausf"uhrlich auf die Beantwortung der im Theorieteil aufgeworfenen Fragestellungen eingegangen. 

Zum Abschluss dieser Diplomarbeit wird in Kapitel \ref{ausblick} "`\textbf{Ausblick}"' noch ein kurzer Blick darauf geworfen, wie zuk"unftig das Ossobook-System genutzt werden k"onnte. Dies beinhaltet zum Einen Anpassungen der Ossobook-Software an Nutzeranspr"uche und zum Anderen Einsatzm"oglichkeiten verschiedener Informatikmethoden zur Untersuchung und Bearbeitung gro"ser Datenmengen.
