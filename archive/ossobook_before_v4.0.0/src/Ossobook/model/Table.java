package ossobook.model;

import java.io.Serializable;
import java.util.Vector;

/**
 * defines scheme of a table in a database
 * 
 * @author j.lamprecht
 * 
 */
public class Table implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String name;
	private final Vector<Column> columns;
	private final Vector<String> primaryKey;

	/**
	 * defines table object of database
	 * 
	 * @param name
	 * @param columns
	 * @param primaryKey
	 */
	public Table(String name, Vector<Column> columns, Vector<String> primaryKey) {
		this.name = name;
		this.columns = columns;
		this.primaryKey = primaryKey;
	}

	public String getName() {
		return name;
	}

	public Vector<Column> getColumns() {
		return columns;
	}

	public Vector<String> getPrimaryKey() {
		return primaryKey;
	}

	/**
	 * compares primary keys of "this" to primary key of given table
	 * 
	 * @param table
	 * @return
	 */
	public boolean getPrimaryKeyIdentic(Table table) {
		boolean primaryKeyIdentic = true;
		if (primaryKey.size() == 0) {
			return false;
		}
		if (primaryKey.size() != table.getPrimaryKey().size()) {
			return false;
		}
		for (int i = 0; i < primaryKey.size(); i++) {
			if (!primaryKeyIdentic) {
				return false;
			}
			for (int j = 0; j < table.getPrimaryKey().size(); j++) {
				if (primaryKey.elementAt(i).equals(
						table.getPrimaryKey().elementAt(j))) {
					primaryKeyIdentic = true;
					break;
				}
				if (j == table.getPrimaryKey().size() - 1) {
					primaryKeyIdentic = false;
				}
			}
		}
		return primaryKeyIdentic;
	}
}
