package ossobook;

import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import ossobook.client.util.Configuration;

/**
 * Class used for localizing the GUI of the OssoBook client.
 * 
 * <p>
 * Generated and maintained using the "Externalize Strings" functionality of the
 * Eclipse IDE.
 * </p>
 * 
 * @author fnuecke
 */
public class Messages {
	private static final String		BUNDLE_NAME		= "ossobook.messages";	//$NON-NLS-1$
																			
	private static ResourceBundle	RESOURCE_BUNDLE	= null;
	
	/**
	 * Singleton.
	 */
	private Messages() {
	}
	
	/**
	 * Get a localized string of the given ID. If the string cannot be found in
	 * the current localization, an error String is returned (ID of the string
	 * encapsulated by '!'s).
	 * 
	 * @param key
	 *            the ID of the localized string to get.
	 * @return localized string or error string if not found.
	 */
	public static String getString(String key) {
		synchronized (BUNDLE_NAME) {
			if (RESOURCE_BUNDLE == null) {
				RESOURCE_BUNDLE = Utf8ResourceBundle.getBundle(BUNDLE_NAME,
						new Locale(Configuration.config.getProperty("ossobook.language", Locale.getDefault().getLanguage()))); //$NON-NLS-1$
			}
			try {
				return RESOURCE_BUNDLE.getString(key);
			} catch (MissingResourceException e) {
				return '!' + key + '!';
			}
		}
	}
	
	/**
	 * Get a formatted string.
	 * 
	 * <p>
	 * This is a convenience function for
	 * <code>String.format(Messages.getString(key), args)</code>.
	 * </p>
	 * 
	 * @param key
	 *            the ID of the localized string to get.
	 * @param args
	 *            arguments to use when formatting the string.
	 * @return localized string or error string if not found.
	 */
	public static String getString(String key, Object... args) {
		return String.format(getString(key), args);
	}

	/**
	 * Support for UTF-8 in localization property files.
	 *
	 * <p>
	 * Original implementation slightly modified from <a href="http://www.thoughtsabout.net/blog/archives/000044.html">here</a>.
	 * </p>
	 */
	private abstract static class Utf8ResourceBundle {

		public static final ResourceBundle getBundle(String baseName, Locale locale) {
			ResourceBundle bundle = ResourceBundle.getBundle(baseName, locale);
			return createUtf8PropertyResourceBundle(bundle);
		}

		private static ResourceBundle createUtf8PropertyResourceBundle(ResourceBundle bundle) {
			if (!(bundle instanceof PropertyResourceBundle)) {
				return bundle;
			}
			return new Utf8PropertyResourceBundle((PropertyResourceBundle) bundle);
		}

		private static class Utf8PropertyResourceBundle extends ResourceBundle {
			PropertyResourceBundle bundle;

			private Utf8PropertyResourceBundle(PropertyResourceBundle bundle) {
				this.bundle = bundle;
			}

			/* (non-Javadoc)
			 * @see java.util.ResourceBundle#getKeys()
			 */
			@SuppressWarnings("unchecked")
			public Enumeration getKeys() {
				return bundle.getKeys();
			}

			/* (non-Javadoc)
			 * @see java.util.ResourceBundle#handleGetObject(java.lang.String)
			 */
			protected Object handleGetObject(String key) {
				String value = bundle.getString(key);
				if (value == null) {
					return null;
				}
				try {
					return new String(value.getBytes("ISO-8859-1"), "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$
				} catch (UnsupportedEncodingException e) {
					// Shouldn't fail - but should we still add logging message?
					return null;
				}
			}
		}
	}
}
