/*
 * Created on 14.04.2005
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.JFrame;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.base.metainfo.Project;
import ossobook.client.gui.update.components.other.TabellenAnsicht;
import ossobook.client.gui.update.components.window.ProjektFenster;

/**
 * @author ali Preferences - Java - Code Style - Code Templates
 */
public class CSVExport {
	private static final Log _log = LogFactory.getLog(CSVExport.class);
	private final Connection con;

	public CSVExport(Connection con) {
		this.con = con;
	}

	/**
	 * @param s
	 * @return
	 */
	// Eliminiere die Zeilenumbr\u00FCche im String
	private static String elimCR(String s) {
		if (s.lastIndexOf("\n") > -1) { //$NON-NLS-1$
			s = s.replaceAll("\n", " "); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return s;
	}

	// Eliminiere die Kommata im String
	private static String escapeComma(String input) {
		if (input != null) {
			if (input.lastIndexOf(",") > -1) { //$NON-NLS-1$
				//input = input.replaceAll(",", " - ");
				input = "\"" + input + "\""; //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		return input;
	}

	// DER CSV Export
	public void export(String query, File f, JFrame displayframe) {
		String result = ""; //$NON-NLS-1$
		long now = System.currentTimeMillis();
		if (_log.isDebugEnabled()) {
			_log.debug("Starting CSV-Export:"); //$NON-NLS-1$
		}
		try {
			Statement stat = con.createStatement();
			ResultSet rs = stat.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int count = rsmd.getColumnCount();
			String s = ""; //$NON-NLS-1$
			String cname;
			// Ueberschriften
			for (int i = 1; i <= count; i++) {
				cname = rsmd.getColumnName(i);
				// finde mass\u00FCberschriften
				if (!(cname == null)) {
					if (cname.startsWith("mass")) { //$NON-NLS-1$
						try {
							cname = getMassName(cname);
						} catch (Exception e) {
							LogFactory.getLog(CSVExport.class).error(e, e);
						}
					}
				}

				// ID as first column name produces problems on excel csv import
				if (i == 1 && cname.equals("ID")) { //$NON-NLS-1$
					cname = "id"; //$NON-NLS-1$
				}

				s = s + "," + cname; //$NON-NLS-1$
			}
			result = result + s.substring(1, s.length()) + "\n"; //$NON-NLS-1$
			// Inhalte
			if (_log.isTraceEnabled()) {
				_log.trace("Fetched Titles: " //$NON-NLS-1$
						+ (System.currentTimeMillis() - now));
			}
			int rowcount = 0;

			StringBuffer theResult = new StringBuffer(""); //$NON-NLS-1$
			while (rs.next()) {
				s = "";// ","; //$NON-NLS-1$
				String neuerString;
				for (int i = 1; i <= count; i++) {
					neuerString = rs.getString(i);
					neuerString = escapeComma(neuerString);
					s = s + "," + neuerString; //$NON-NLS-1$
				}
				s = elimCR(s);
                theResult.append(s.substring(1, s.length())).append("\n"); //$NON-NLS-1$
				displayframe.setTitle(Messages.getString("CSVExport.0") + rowcount); //$NON-NLS-1$
				rowcount++;
			}
			if (_log.isTraceEnabled()) {
				_log.trace("Iterated " + rowcount + " rows."); //$NON-NLS-1$ //$NON-NLS-2$
			}
			result = result + theResult;
		} catch (Exception e) {
			e.printStackTrace();
		}
		// schreibe
		try {
			if (_log.isTraceEnabled()) {
				_log.trace("Writing file time. time: " //$NON-NLS-1$
						+ (System.currentTimeMillis() - now));
			}
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(result);
			bw.close();

			fw.close();
		} catch (Exception e) {
			LogFactory.getLog(CSVExport.class).error(e, e);
		}
		displayframe.setTitle(Messages.getString("CSVExport.1")); //$NON-NLS-1$
		if (_log.isTraceEnabled()) {
			_log.trace("End of CSV-Export. Time used: " //$NON-NLS-1$
					+ (System.currentTimeMillis() - now));
		}
	}

	/**
	 * @param cname
	 * @return
	 */
	private String getMassName(String cname) throws Exception {
		String query = "SELECT massBeschreibung FROM massdefinition WHERE massName LIKE('" //$NON-NLS-1$
				+ cname + "') AND geloescht='N';"; //$NON-NLS-1$
		Statement statMass = con.createStatement();
		ResultSet rsMassName = statMass.executeQuery(query);
		rsMassName.next();
		return rsMassName.getString("massBeschreibung"); //$NON-NLS-1$
	}

	// Funktion zum schreiben einer Skelettteilliste
	public void exportSkelettListe(File f, Project project,
			int gewaehlterTiercode, String groupby) {

		String query1 = "SELECT @nAbsolut:=SUM(anzahl)  FROM eingabeeinheit,tierart WHERE tierart.TierCode=" //$NON-NLS-1$
				+ gewaehlterTiercode
				+ " AND tierart.tierCode=eingabeeinheit.tierart AND projNr=" //$NON-NLS-1$
				+ project.getNumber()
				+ " AND DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND geloescht='N';"; //$NON-NLS-1$
		String query2 = " SELECT @gAbsolut:=SUM(gewicht) FROM eingabeeinheit,tierart WHERE tierart.TierCode=" //$NON-NLS-1$
				+ gewaehlterTiercode
				+ " AND tierart.tierCode=eingabeeinheit.tierart AND projNr=" //$NON-NLS-1$
				+ project.getNumber()
				+ " AND DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND geloescht='N';"; //$NON-NLS-1$

		String selectClause = ""; //$NON-NLS-1$

		// Wenn gew\u00E4hlt ->
		if (groupby.contains("fK")) { //$NON-NLS-1$
			String fk = "fK,"; //$NON-NLS-1$
			selectClause = selectClause + fk;
		}
		// Wenn gew\u00E4hlt ->
		if (groupby.contains("phase1")) { //$NON-NLS-1$
			String phase1 = "phase1 AS 'FK-Konkordanz 1',"; //$NON-NLS-1$
			selectClause = selectClause + phase1;
		}
		// Wenn gew\u00E4hlt ->
		if (groupby.contains("phase2")) { //$NON-NLS-1$
			String phase2 = "phase2 AS 'FK-Konkordanz 2',"; //$NON-NLS-1$
			selectClause = selectClause + phase2;
		}
		// Wenn gew\u00E4hlt ->
		if (groupby.contains("phase3")) { //$NON-NLS-1$
			String phase3 = "phase3 AS 'FK-Konkordanz 3',"; //$NON-NLS-1$
			selectClause = selectClause + phase3;
		}
		// Wenn gew\u00E4hlt ->
		if (groupby.contains("phase4")) { //$NON-NLS-1$
			String phase4 = "phase4 AS 'FK-Konkordanz 4',"; //$NON-NLS-1$
			selectClause = selectClause + phase4;
		}
		// immer ->
		String summeN = "SUM(anzahl) AS n,"; //$NON-NLS-1$
		selectClause = selectClause + summeN;
		// immer ->
		String proporzN = "SUM(anzahl)/(@nAbsolut/100 ) AS 'n%',"; //$NON-NLS-1$
		selectClause = selectClause + proporzN;
		// immer ->
		String summeG = "Sum(gewicht) As g,"; //$NON-NLS-1$
		selectClause = selectClause + summeG;
		// immer ->
		String proporzG = "SUM(gewicht)/(@gAbsolut/100 ) AS 'g%',"; //$NON-NLS-1$
		selectClause = selectClause + proporzG;
		// immer ->
		String tiername = "tiername,"; //$NON-NLS-1$
		selectClause = selectClause + tiername;
		// immer ->
		String skelname = "skelName"; //$NON-NLS-1$
		selectClause = selectClause + skelname;

		String query3 = "SELECT " //$NON-NLS-1$
				+ selectClause
				+ " FROM `eingabeeinheit`,tierart,skelteil " //$NON-NLS-1$
				+ " WHERE ProjNr=" //$NON-NLS-1$
				+ project.getNumber()
				+ " AND DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND eingabeeinheit.tierart=tierart.tiercode  AND eingabeeinheit.skelettteil = skelteil.skelcode AND tierart.TierCode=" //$NON-NLS-1$
				+ gewaehlterTiercode
				+ " AND eingabeeinheit.geloescht='N' AND tierart.geloescht='N' AND skelteil.geloescht='N' Group by " //$NON-NLS-1$
				+ groupby + ",skelCode Order by " + groupby //$NON-NLS-1$
				+ ",SkelFolgeAuswertung;"; //$NON-NLS-1$
		// Die
		// Folge wird gem\u00E4ss Auswertungs Reihenfolge
		// in der tabelle tierart sortiert.
		String result = ""; //$NON-NLS-1$
		try {
			Statement stat = con.createStatement();
			stat.executeQuery(query1);
			stat.executeQuery(query2);
			ResultSet rs = stat.executeQuery(query3);
			ResultSetMetaData rsmd = rs.getMetaData();
			int count = rsmd.getColumnCount();
			String s = ""; //$NON-NLS-1$

			// Ueberschriften
			for (int i = 1; i <= count; i++) {
				s = s + "," + rsmd.getColumnName(i); //$NON-NLS-1$
			}
			result = result + s.substring(1, s.length()) + "\n"; //$NON-NLS-1$
			// Inhalte
			while (rs.next()) {
				s = "";// ","; //$NON-NLS-1$
				for (int i = 1; i <= count; i++) {
					s = s + ", " + rs.getString(i); //$NON-NLS-1$
				}
				result = result + s.substring(1, s.length()) + "\n"; //$NON-NLS-1$
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// schreibe
		try {
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(result);
			bw.close();

			fw.close();
		} catch (Exception e) {
			LogFactory.getLog(CSVExport.class).error(e, e);
		}
	}

	// Funktion zum schreiben einer Tierartenliste f\u00FCr die
	// Sortierreihenfolge
	// ist eine TierReihenfolge in der DB vorgesehen (Tierfolge Auswertung).
	public void exportTierartenListe(File f, Project project) {

		String query1 = "SELECT @nAbsolut:=SUM(anzahl)  FROM eingabeeinheit WHERE projNr=" //$NON-NLS-1$
				+ project.getNumber()
				+ " AND DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND geloescht='N';"; //$NON-NLS-1$
		String query2 = " SELECT @gAbsolut:=SUM(gewicht) FROM eingabeeinheit WHERE projNr=" //$NON-NLS-1$
				+ project.getNumber()
				+ " AND DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND geloescht='N';"; //$NON-NLS-1$
		String query3 = " SELECT tierart,TierName,SUM(anzahl) AS n, SUM(anzahl)  / (@nAbsolut /100 ) AS 'n%', SUM(gewicht) AS g, SUM(gewicht)  / (@gAbsolut /100 ) AS 'g%'" //$NON-NLS-1$
				+ " FROM eingabeeinheit,tierart WHERE projNr=" //$NON-NLS-1$
				+ project.getNumber()
				+ " AND DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND tierart.TierCode=eingabeeinheit.tierart " //$NON-NLS-1$
				+ " AND eingabeeinheit.geloescht='N' AND tierart.geloescht='N' " //$NON-NLS-1$
				+ "GROUP BY eingabeeinheit.tierart ORDER BY tierart.TierFolgeAuswertung;"; //$NON-NLS-1$
		// Die
		// Folge
		// wird
		// gem\u00E4ss
		// Auswertungs
		// Reihenfolge
		// in
		// der
		// tabelle
		// tierart
		// sortiert.
		String result = ""; //$NON-NLS-1$
		try {
			Statement stat = con.createStatement();
			stat.executeQuery(query1);
			stat.executeQuery(query2);
			ResultSet rs = stat.executeQuery(query3);
			ResultSetMetaData rsmd = rs.getMetaData();
			int count = rsmd.getColumnCount();
			String s = ""; //$NON-NLS-1$

			// Ueberschriften
			for (int i = 1; i <= count; i++) {
				s = s + "," + rsmd.getColumnName(i); //$NON-NLS-1$
			}
			result = result + s.substring(1, s.length()) + "\n"; //$NON-NLS-1$
			// Inhalte
			while (rs.next()) {
				s = "";// ","; //$NON-NLS-1$
				for (int i = 1; i <= count; i++) {
					s = s + ", " + rs.getString(i); //$NON-NLS-1$
				}
				result = result + s.substring(1, s.length()) + "\n"; //$NON-NLS-1$
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// schreibe
		try {
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(result);
			bw.close();

			fw.close();
		} catch (Exception e) {
			LogFactory.getLog(CSVExport.class).error(e, e);
		}
	}

	// Gibt die CSV info als String zur\u00FCck

	// gibt die CSV Info als JTable zur\u00FCck
	@SuppressWarnings("unchecked")
	public TabellenAnsicht showResultTable(String query,
			ProjektFenster boneproject, int zweck) {
		TabellenAnsicht result = null;
		// TableModel model=new DefaultTableModel();
		// DefaultTableColumnModel column=new DefaultTableColumnModel();

		// result.setModel(model);
		String[] s;
		try {
			Statement stat = con.createStatement();
			ResultSet rs = stat.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int count = rsmd.getColumnCount();
			s = new String[count];

			// Ueberschriften
			for (int i = 1; i <= count; i++) {
				s[i - 1] = rsmd.getColumnName(i);
			}

			Vector rows = new Vector();
			while (rs.next()) {
				Vector v = new Vector();
				for (int i = 1; i <= count; i++) {
					v.add(rs.getString(i));
				}
				rows.add(v.toArray());
			}

			Object[][] Arr = new Object[rows.size()][count];
			for (int i = 0; i < rows.size(); i++) {
				Object[] o = (Object[]) rows.get(i);
				for (int j = 0; j < o.length; j++) {
					// why is there an if here? is this a bug?
					if (j == 0) {
						Arr[i][j] = o[j];
					} else {
						Arr[i][j] = o[j];
					}
				}
			}
			if (Arr.length == 0) {
				result = null;
			} else {
				result = new TabellenAnsicht(Arr, s, boneproject, zweck);
				// TableColumn ID=result.getColumnModel().getColumn(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}