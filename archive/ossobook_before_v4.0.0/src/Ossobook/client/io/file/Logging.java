package ossobook.client.io.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;

/**
 * represents log files for conflicts and database scheme changes during a
 * synchronization or initialization
 * 
 * @author j.lamprecht
 * 
 */
@SuppressWarnings("serial")
public class Logging implements Serializable {

	private File logging;

	/**
	 * creates log file file name: Logging/ProjectName/DDMMYYYY-HHMMSS-log
	 * 
	 * @param log
	 *            kind of logging (e.g. for database scheme changes or occuring
	 *            synchronization conflicts )
	 */
	public Logging(String log, String projectName) {
		if (projectName == null) {
			projectName = "Schema"; //$NON-NLS-1$
		}

		File directory = new File("log" + File.separator + projectName); //$NON-NLS-1$

		// get current date
		Calendar cal = Calendar.getInstance();

		String day;
		int dayInt = cal.get(Calendar.DAY_OF_MONTH);
		if (dayInt < 10) {
			day = "0" + Integer.toString(dayInt); //$NON-NLS-1$
		} else {
			day = Integer.toString(dayInt);
		}

		String month;
		int monthInt = cal.get(Calendar.MONTH) + 1;
		if (monthInt < 10) {
			month = "0" + Integer.toString(monthInt); //$NON-NLS-1$
		} else {
			month = Integer.toString(monthInt);
		}

		int year = cal.get(Calendar.YEAR);

		String minute;
		int minuteInt = cal.get(Calendar.MINUTE);
		if (minuteInt < 10) {
			minute = "0" + Integer.toString(minuteInt); //$NON-NLS-1$
		} else {
			minute = Integer.toString(minuteInt);
		}

		String hour;
		int hourInt = cal.get(Calendar.HOUR_OF_DAY);
		if (hourInt < 10) {
			hour = "0" + Integer.toString(hourInt); //$NON-NLS-1$
		} else {
			hour = Integer.toString(hourInt);
		}

		String second;
		int secondInt = cal.get(Calendar.SECOND);
		if (secondInt < 10) {
			second = "0" + Integer.toString(secondInt); //$NON-NLS-1$
		} else {
			second = Integer.toString(secondInt);
		}

		logging = new File("log" + File.separator + projectName //$NON-NLS-1$
				+ File.separator + day + month + year + "-" + hour + minute //$NON-NLS-1$
				+ second + "-" + log + ".txt"); //$NON-NLS-1$ //$NON-NLS-2$
		try {
			directory.mkdirs();
			logging.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * save global log file local
	 * 
	 * @param log
	 */
	public Logging(Logging log) {
		try {
			logging = log.getFile();
			new File(logging.getParent()).mkdirs();
			logging.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * writes an entry in the log file
	 * 
	 * @param log
	 */
	public void log(String log) {
		try {
			FileWriter fw = new FileWriter(logging, true);
			fw.write(log);
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * merge two logging files
	 * 
	 * @param log
	 */
	public void append(Logging log) {
		try {
			FileReader fr = new FileReader(log.getFile());
			BufferedReader br = new BufferedReader(fr);
			FileWriter fw = new FileWriter(logging, true);
			String line;

			for (int i = 0; i < 4; i++) {
				if (br.ready()) {
					br.readLine();
				}
			}

			while (br.ready()) {
				line = br.readLine();
				fw.write(line);
			}
			br.close();
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * proofs whether entries have been made to the log file
	 * 
	 * @return
	 */
	public int getRowsOfContent() {

		int result = 0;
		try {
			FileReader fr = new FileReader(logging);
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()) {
				br.readLine();
				result++;
			}
			br.close();
		} catch (FileNotFoundException f) {
			f.printStackTrace();
		} catch (IOException io) {
			io.printStackTrace();
		}
		return result;
	}

	public String getPath() {
		return logging.getAbsolutePath();
	}

	public File getFile() {
		return logging;
	}
}
