/*
 * Created on 30.05.2001
 */
package ossobook.client.io.database.dbinfos;

import java.sql.Connection;

import org.apache.commons.logging.LogFactory;

import ossobook.client.io.database.communication.DatabaseInfo;

/**
 * @author ali
 * 
 */
public class FKUebersicht extends DatabaseInfo {

	public FKUebersicht(Connection c, String query, String header) {
		super(c, query, header);
	}

	@Override
	protected String bearbeiteResultat() {
		// Output
		String resultat = ""; //$NON-NLS-1$
		try {
			while (rs.next()) {
				resultat = resultat + rs.getString("fK") + " / " //$NON-NLS-1$ //$NON-NLS-2$
						+ rs.getString("Summe") + "\n"; //$NON-NLS-1$ //$NON-NLS-2$
			}
		} catch (Exception e) {
			LogFactory.getLog(FKUebersicht.class).error(e, e);
		}
		return resultat;
	}

}
