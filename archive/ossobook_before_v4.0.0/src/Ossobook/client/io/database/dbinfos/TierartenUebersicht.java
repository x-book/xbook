/*
 * Created on 30.05.2001
 */
package ossobook.client.io.database.dbinfos;

import java.sql.Connection;

import org.apache.commons.logging.LogFactory;

import ossobook.client.io.database.communication.DatabaseInfo;

/**
 * @author ali
 * 
 */
public class TierartenUebersicht extends DatabaseInfo {

	public TierartenUebersicht(Connection c, String query, String header) {
		super(c, query, header);
	}

	@Override
	protected String bearbeiteResultat() {
		// Output
		String resultat = ""; //$NON-NLS-1$
		try {
			while (rs.next()) {
				resultat = resultat + rs.getString("TierName") + " / " //$NON-NLS-1$ //$NON-NLS-2$
						+ rs.getString("Summe") + "\n"; //$NON-NLS-1$ //$NON-NLS-2$
			}
		} catch (Exception e) {
			LogFactory.getLog(TierartenUebersicht.class).error(e, e);
		}
		return resultat;
	}

}
