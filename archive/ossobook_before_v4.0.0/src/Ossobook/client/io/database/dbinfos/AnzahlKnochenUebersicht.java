/*
 * Created on 30.05.2001
 */
package ossobook.client.io.database.dbinfos;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.logging.LogFactory;

import ossobook.client.io.database.communication.DatabaseInfo;

/**
 * @author ali
 * 
 */
public class AnzahlKnochenUebersicht extends DatabaseInfo {

	public AnzahlKnochenUebersicht(Connection c, String query, String header) {
		super(c, query, header);
	}

	@Override
	protected String bearbeiteResultat() {
		// Output
		StringBuilder result = new StringBuilder(""); //$NON-NLS-1$
		try {
			while (rs.next()) {
				result.append(rs.getString("SUMAnzahl")); //$NON-NLS-1$
			}
		} catch (SQLException e) {
			LogFactory.getLog(AnzahlKnochenUebersicht.class).error(e, e);
		}
		return result.toString();
	}

}
