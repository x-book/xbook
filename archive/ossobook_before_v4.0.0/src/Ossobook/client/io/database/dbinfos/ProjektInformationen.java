/*
 * Created on 30.05.2001
 */
package ossobook.client.io.database.dbinfos;

import java.sql.Connection;
import java.sql.ResultSetMetaData;

import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.io.database.communication.DatabaseInfo;

/**
 * @author ali
 * 
 */
public class ProjektInformationen extends DatabaseInfo {

	public ProjektInformationen(Connection c, String query, String header) {
		super(c, query, header);
	}

	@Override
	protected String bearbeiteResultat() {
		// Output
		String resultat = ""; //$NON-NLS-1$
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			rs.next();
			int i = rsmd.getColumnCount();
			for (int j = 1; j <= i; j++) {
				String name = rsmd.getColumnName(j);
				String value = rs.getString(j);
				if ((name.equals("projektNotiz")) || name.equals("befundNotiz")) { //$NON-NLS-1$ //$NON-NLS-2$
					if (!value.equals("")) { //$NON-NLS-1$
						value = Messages.getString("ProjektInformationen.4"); //$NON-NLS-1$
					} else {
						value = Messages.getString("ProjektInformationen.5"); //$NON-NLS-1$
					}
				}
				resultat = resultat + name + " : " + value + "\n"; //$NON-NLS-1$ //$NON-NLS-2$
			}
		} catch (Exception e) {
			LogFactory.getLog(ProjektInformationen.class).error(e, e);
		}
		return resultat;
	}

}
