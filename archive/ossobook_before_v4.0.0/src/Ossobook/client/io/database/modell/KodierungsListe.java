package ossobook.client.io.database.modell;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * The CodeList Abstract-Class is the Base for all OssoBook CodeLists. CodeLists
 * are ment to translate given codes into values to be displayes in the GUI. The
 * translation should prevent from Mistakes. e.g.: if an Animal code is given
 * the Codelist returns it's Name.
 * 
 * @see ossobook.client.gui.update.components.window.ProjektFenster
 * @author ali
 * 
 */

public class KodierungsListe {
	private static final Log _log = LogFactory.getLog(KodierungsListe.class);
	protected HashMap<String, Integer> codes;

	protected HashMap<Integer, String> teile;

	protected String tabellenname = ""; //$NON-NLS-1$

	protected String namenColumne = ""; //$NON-NLS-1$

	protected String codeColumne = ""; //$NON-NLS-1$

	protected static ResultSet getList(Connection connection, String tableName) {
		ResultSet result = null;
		try {
			Statement s = connection.createStatement();
			String query = "SELECT * FROM " + tableName //$NON-NLS-1$
					+ "  WHERE geloescht='N';"; //$NON-NLS-1$
			if (_log.isDebugEnabled()) {
				_log.debug("Query: " + query); //$NON-NLS-1$
			}
			result = s.executeQuery(query);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		return result;
	}

	public String getTeil(Object n) {
		return teile.get(n);
	}

	protected void fillHashmap(ResultSet rs) {
		try {
			while (rs.next()) {
				String s = rs.getString(namenColumne);
				Integer i = rs.getInt(codeColumne);
				teile.put(i, s);
				codes.put(s, i);
			}
		} catch (SQLException e) {
			LogFactory.getLog(KodierungsListe.class).error(e, e);
		}
	}
}