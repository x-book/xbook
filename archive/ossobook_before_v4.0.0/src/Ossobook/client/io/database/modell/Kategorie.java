package ossobook.client.io.database.modell;

import java.awt.Color;

/**
 * Category defines the subset to which the BasicField belongs.
 * 
 * @author ali
 * 
 */
public class Kategorie {
	public static final int BENUTZERDEFINIERT = 5;

	public static final int BESTIMMUNG = 2;

	public static final int ERHALTUNG = 3;

	public static final int IDENTIFIKATION = 1;

	public static final int PASSNUMMERN = 4;

	private final int dieseKategorie;

	/**
	 * constructs Category with one of the static Values:
	 * Category.IDENTIFIKATION, Category.BESTIMMUNG ...
	 */
	public Kategorie(int kategorie) {
		dieseKategorie = kategorie;
	}

	/**
	 * @return the Color belonging to the Category
	 */
	public Color getColor() {
		if (dieseKategorie == Kategorie.IDENTIFIKATION) {
			return Color.blue;
		} else if (dieseKategorie == Kategorie.BESTIMMUNG) {
			return Color.green;
		} else if (dieseKategorie == Kategorie.ERHALTUNG) {
			return Color.orange;
		} else if (dieseKategorie == Kategorie.PASSNUMMERN) {
			return Color.cyan;
		} else if (dieseKategorie == Kategorie.BENUTZERDEFINIERT) {
			return Color.red;
		} else {
			return Color.black;
		}
	}

}