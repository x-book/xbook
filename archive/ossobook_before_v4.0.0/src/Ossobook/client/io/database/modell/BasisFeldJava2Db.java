package ossobook.client.io.database.modell;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JCheckBox;
import javax.swing.JComponent;

/**
 * BasicField defines the minimum Attributes a Ossobook field must provide.
 * 
 * @author ali
 */
public class BasisFeldJava2Db {

	private final JCheckBox CheckBoxSwitch;

	private final Color coloring;

	private final String dbAequivalent;

	private final JComponent eingabeKomponente;

	private final boolean feldImmerAktiv;

	private final int sectionNr;

	public BasisFeldJava2Db(final JComponent eingabeKomponente,
							String bezeichnung, int sectionNr, String InfoText,
							String dbAequivalent, Color checkTextColor,
							boolean feldImmerAktiv,
							boolean defaultChecked) {
		this.eingabeKomponente = eingabeKomponente;

		this.eingabeKomponente.setToolTipText(InfoText);
		this.eingabeKomponente.addKeyListener(new KeyListener() {

			public void keyTyped(KeyEvent arg0) {

			}

			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					eingabeKomponente.transferFocus();
				}
			}

			public void keyReleased(KeyEvent arg0) {

			}

		});
		this.dbAequivalent = dbAequivalent;
		CheckBoxSwitch = new JCheckBox(bezeichnung, defaultChecked);
		CheckBoxSwitch.setToolTipText(InfoText);
		// this.CheckBoxSwitch.addFocusListener(new FocusListener() {
		// public void focusGained(FocusEvent arg0) {
		//            	
		// }
		// public void focusLost(FocusEvent arg0) {
		// }
		// });
		coloring = checkTextColor;
		this.sectionNr = sectionNr;

		this.eingabeKomponente.setBackground(Color.white);
		CheckBoxSwitch.setBackground(Color.white);
		this.feldImmerAktiv = feldImmerAktiv;
	}

	/**
	 * @return Returns the checkBoxSwitch of the Component.
	 */
	public JCheckBox getCheckBoxSwitch() {
		CheckBoxSwitch.setEnabled(!feldImmerAktiv);
		CheckBoxSwitch.setForeground(coloring);
		return CheckBoxSwitch;
	}

	/**
	 * @return Returns the dbAequivalent.
	 */
	public String getDbAequivalent() {
		return dbAequivalent;
	}

	/**
	 * @return Returns the Component.
	 */
	public JComponent getEingabeKomponente() {
		return eingabeKomponente;
	}

	/**
	 * @return Returns the sectionNr.
	 */
	public int getSectionNr() {
		return sectionNr;
	}

}