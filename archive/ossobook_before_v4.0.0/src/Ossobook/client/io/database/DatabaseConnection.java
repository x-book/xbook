package ossobook.client.io.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import ossobook.client.io.forwarder.Forwarder;
import ossobook.client.util.Configuration;

/**
 * This Class is the supporting Class for DBConnection. It Provides the default
 * settings for an ossobook Connection to the used MYSQL DB on Local or
 * MainHost.
 * 
 * @author ali
 */
public class DatabaseConnection {

	/**
	 * Initialize database driver once.
	 */
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance(); //$NON-NLS-1$
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private final ConnectionType connectionType;
	private Connection connection;

	/**
	 * Creates a new instance of mySqlConnection using 'ossobookDBConfig.txt'
	 * and login info from ossobook
	 * @throws SQLException 
	 */
	public DatabaseConnection(String username, String password,
			ConnectionType connectionType) throws SQLException
	{
		this.connectionType = connectionType;
		String host, database;
		switch (connectionType) {
		case CONNECTION_GLOBAL:
			host = Configuration.config.getProperty("global.host"); //$NON-NLS-1$
			database = Configuration.config.getProperty("global.database"); //$NON-NLS-1$
			break;
		case CONNECTION_LOCAL:
			host = Configuration.config.getProperty("local.host"); //$NON-NLS-1$
			database = Configuration.config.getProperty("local.database"); //$NON-NLS-1$
			break;
		case CONNECTION_SYNCHRONIZE:
			if (Forwarder.isInitialized()) {
				host = "localhost:" + Configuration.config.getProperty("forwarder.0.listenport"); //$NON-NLS-1$ //$NON-NLS-2$
				database = Configuration.config.getProperty("global.database"); //$NON-NLS-1$
			} else {
				host = Configuration.config.getProperty("global.host"); //$NON-NLS-1$
				database = Configuration.config.getProperty("global.database"); //$NON-NLS-1$
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown connection type."); //$NON-NLS-1$
		}
		connection = DriverManager.getConnection("jdbc:mysql://" + host + "/" + database, username, password); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Get the database name this connection is set up to connect to.
	 * 
	 * @return the database name for this connection.
	 */
	public String getDatabase() {
		try {
			return connection.getCatalog();
		} catch (SQLException e) {
			return null;
		}
	}
	
	/**
	 * Reference to the actual lowlevel connection used.
	 * 
	 * @return actual connection used.
	 */
	public Connection getUnderlyingConnection() {
		return connection;
	}

	/**
	 * The connection type of this connection.
	 * 
	 * @return the connection type.
	 */
	public ConnectionType getConnectionType() {
		return connectionType;
	}
}