package ossobook.client.io.database;

/**
 * Possible connection types for the {@link ossobook.client.io.database.DatabaseConnection} class.
 */
public enum ConnectionType {

	/**
	 * Global connection (using the global.* settings).
	 */
	CONNECTION_GLOBAL,

	/**
	 * Global connection (using the local.* settings). Also used for local synchronization connection.
	 */
	CONNECTION_LOCAL,

	/**
	 * Global synchronization connection (using the forwarder if set, else global.* settings).
	 */
	CONNECTION_SYNCHRONIZE
}
