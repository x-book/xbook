package ossobook.client.gui.synchronization;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import ossobook.Messages;
import ossobook.client.base.metainfo.Project;
import ossobook.client.synchronization.SynchronizationProgress;
import ossobook.client.synchronization.SynchronizationWorker;
import ossobook.client.util.Configuration;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.queries.QueryManager;
import ossobook.queries.QueryManagerFactory;

import com.jidesoft.swing.CheckBoxList;

/**
 * This frame is used for the synchronization.
 * 
 * <p>
 * It displays two lists, one of the locally available projects, one of the
 * globally available projects, from which the user may select which projects to
 * synchronize.
 * </p>
 * 
 * @author fnuecke
 */
public class SynchronizationFrame extends JInternalFrame implements PropertyChangeListener {
	
	private static final long	serialVersionUID	= 6475649652867916066L;
	
	/**
	 * Possible logging types that can be logged to the information field.
	 */
	private enum LogType {
		/**
		 * Default message (black).
		 */
		DEFAULT,

		/**
		 * Warning message (orange).
		 */
		WARNING,

		/**
		 * Error message (red).
		 */
		ERROR,

		/**
		 * System notice (grey).
		 */
		SYSTEM
	}
	
	/**
	 * Check if a sync frame is already open (only allow one at a time).
	 */
	private static boolean			isOpen						= false;
	
	/**
	 * A list of components to disable while the synchronization is in progress.
	 */
	private final Vector<Component>		disableWhileSynchronizing	= new Vector<Component>();
	
	/**
	 * List with locally available projects.
	 */
	private CheckBoxList			listLocal;
	
	/**
	 * List with globally available projects.
	 */
	private CheckBoxList			listGlobal;
	
	/**
	 * Helper variable to check if currently synchronizing the project lists
	 * after a selection change (to avoid infinite loops).
	 */
	private boolean					syncingLists;
	
	/**
	 * Progress bar for displaying overall progress.
	 */
	private JProgressBar			progressOverall;
	
	/**
	 * Text area used to output status messages regarding the synchronization.
	 */
	private JTextPane				output;
	
	/**
	 * Synchronizer used to synchronize databases.
	 */
	private SynchronizationWorker	synchronizer;

	/**
	 * Creates a new synchronization window which allows selecting the projects
	 * to synchronize and initialize the synchronization.
	 *
	 * @throws IllegalStateException
	 *             if another SynchronizationFrame is already opened.
	 */
	public SynchronizationFrame() throws IllegalStateException {
		synchronized (SynchronizationFrame.class) {
			if (isOpen) {
				throw new IllegalStateException(
						"Only one synchronization window at a time allowed."); //$NON-NLS-1$
			}
			isOpen = true;
		}
		
		// Some basic setup...
		setTitle(Messages.getString("SynchronizationFrame.0")); //$NON-NLS-1$
		setMaximizable(true);
		setFrameIcon(new ImageIcon(Configuration.IMAGES_DIR
				+ "mail-send-receive.png")); //$NON-NLS-1$
		setIconifiable(true);
		setResizable(true);
		
		// Sizes...
		setMinimumSize(new Dimension(300, 400));
		setPreferredSize(new Dimension(500, 500));
		
		// Layout...
		GridBagLayout layout = new GridBagLayout();
		setLayout(layout);
		
		// Create components that go inside this window.
		
		// Starting with the projects panel.
		JPanel projects = new JPanel();
		add(projects);
		buildConstraints(projects, layout, 0, 1);
		projects.setBorder(BorderFactory.createEmptyBorder());
		projects.setLayout(new BoxLayout(projects, BoxLayout.X_AXIS));
		
		// Create the two lists for local and global projects.
		listLocal = buildProjectList(projects, Messages.getString("SynchronizationFrame.2")); //$NON-NLS-1$
		listGlobal = buildProjectList(projects, Messages.getString("SynchronizationFrame.3")); //$NON-NLS-1$
		
		// Add listeners to allow synchronizing the project selection.
		// This is just a cosmetic, but improves usability quite a bit (it
		// would be enough if a project were selected in just one of the lists).
		listLocal.getCheckBoxListSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						synchronizeSelection(listLocal, listGlobal);
					}
				});
		listGlobal.getCheckBoxListSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						synchronizeSelection(listGlobal, listLocal);
					}
				});
		
		// Create the progress bars.
		JPanel progress = new JPanel();
		add(progress);
		buildConstraints(progress, layout, 0, 1, 1, 0);
		progress.setBorder(BorderFactory.createTitledBorder(Messages.getString("SynchronizationFrame.4"))); //$NON-NLS-1$
		progress.setLayout(new BoxLayout(progress, BoxLayout.Y_AXIS));
		
		progressOverall = new JProgressBar(0, 100);
		progress.add(progressOverall);
		
		// The information text pane used for displaying log messages.
		JPanel log = new JPanel();
		add(log);
		buildConstraints(log, layout, 2, 1);
		log.setBorder(BorderFactory.createTitledBorder(Messages.getString("SynchronizationFrame.5"))); //$NON-NLS-1$
		log.setLayout(new BoxLayout(log, BoxLayout.Y_AXIS));
		
		JScrollPane outputScroller = new JScrollPane();
		outputScroller.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		log.add(outputScroller);
		outputScroller.setPreferredSize(new Dimension());
		
		output = new JTextPane();
		outputScroller.setViewportView(output);
		output.setBorder(BorderFactory.createEmptyBorder());
		output.setEditable(false);
		
		// Buttons to start synchronization / close the window.
		JPanel buttons = new JPanel();
		add(buttons);
		buildConstraints(buttons, layout, 0, 3, 1, 0);
		buttons.setBorder(BorderFactory.createEmptyBorder());
		buttons.setLayout(new FlowLayout(FlowLayout.TRAILING));
		
		JButton synchronize=new JButton(Messages.getString("SynchronizationFrame.6"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "mail-send-receive.png")); //$NON-NLS-1$
		buttons.add(synchronize);
		disableWhileSynchronizing.add(synchronize);
		//synchronize.setEnabled(false);
		synchronize.setFocusable(false);
		final PropertyChangeListener observer = this;
		synchronize.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (areProjectsSelected() ||
						JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(getParent(),
								Messages.getString("SynchronizationFrame.1"), //$NON-NLS-1$
								Messages.getString("SynchronizationFrame.7"), //$NON-NLS-1$
								JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE))
				{
					for (Component c : disableWhileSynchronizing) {
						c.setEnabled(false);
					}
					setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					synchronizer = new SynchronizationWorker(
							QueryManagerFactory.getLocalSyncManager(),
							QueryManagerFactory.getGlobalSyncManager(),
							getSelectedProjects());
					synchronizer.addPropertyChangeListener(observer);
					synchronizer.execute();
				}
			}
		});
		
		JButton close=new JButton(Messages.getString("SynchronizationFrame.8"), new ImageIcon( //$NON-NLS-1$
				Configuration.IMAGES_DIR + "process-stop.png")); //$NON-NLS-1$
		buttons.add(close);
		disableWhileSynchronizing.add(close);
		close.setFocusable(false);
		close.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				QueryManager localConnection =
					QueryManagerFactory.getLocalSyncManager();
				if (localConnection != null) {
					localConnection.closeConnection();
				}
				QueryManager globalConnection =
					QueryManagerFactory.getGlobalSyncManager();
				if (globalConnection != null) {
					globalConnection.closeConnection();
				}
				try {
					setClosed(true);
					synchronized (SynchronizationFrame.class) {
						isOpen = false;
					}
					dispose();
				} catch (PropertyVetoException ignored) {
				}
			}
		});
		
		logMessage(Messages.getString("SynchronizationFrame.10"), LogType.SYSTEM); //$NON-NLS-1$
		
		// Create connection to global database
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		logMessage(Messages.getString("SynchronizationFrame.11"), LogType.SYSTEM); //$NON-NLS-1$
		new Thread(new Runnable() {
			@Override
			public void run() {
				if (QueryManagerFactory.getLocalSyncManager() != null &&
						QueryManagerFactory.getGlobalSyncManager() != null)
				{
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							updateProjectLists();
							setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
							logMessage(Messages.getString("SynchronizationFrame.12")); //$NON-NLS-1$
						}
					});
				} else {
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
							logMessage(Messages.getString("SynchronizationFrame.15"), LogType.ERROR); //$NON-NLS-1$
						}
					});
				}
			}
		}).start();
		
		// Finish up.
		pack();
	}
	
	/**
	 * Updates the project lists.
	 */
	private void updateProjectLists() {
		// Clear lists.
		listLocal.setModel(new DefaultListModel());
		listGlobal.setModel(new DefaultListModel());
		
		try {
			// Fill in available projects.
			final DefaultListModel localModel = new DefaultListModel();
			for (Project project : QueryManagerFactory.getLocalSyncManager().getProjects()) {
				localModel.addElement(project.getName());
			}
			final DefaultListModel globalModel = new DefaultListModel();
			for (Project project : QueryManagerFactory.getGlobalSyncManager().getProjects()) {
				globalModel.addElement(project.getName());
			}
			logMessage(Messages.getString("SynchronizationFrame.16"), //$NON-NLS-1$
					LogType.SYSTEM);
			listLocal.setModel(localModel);
			listLocal.selectNone();
			listGlobal.setModel(globalModel);
			listGlobal.selectNone();
		} catch (StatementNotExecutedException e) {
			logMessage(Messages.getString("SynchronizationFrame.17"), LogType.ERROR); //$NON-NLS-1$
		} catch (NullPointerException e) {
			logMessage(Messages.getString("SynchronizationFrame.9"), LogType.ERROR); //$NON-NLS-1$
		}
	}
	
	/**
	 * Checks if at least one project is selected.
	 * 
	 * @return <code>true</code> if at least one project is selected, else
	 *         <code>false</code>.
	 */
	private boolean areProjectsSelected() {
		return listLocal.getCheckBoxListSelectedIndices().length > 0
				|| listGlobal.getCheckBoxListSelectedIndices().length > 0;
	}
	
	/**
	 * Gets a list of all selected projects.
	 * 
	 * @return a list of all selected projects.
	 */
	private Set<String> getSelectedProjects() {
		HashSet<String> projects = new HashSet<String>();
		for (Object element : listLocal.getCheckBoxListSelectedValues()) {
			projects.add((String) element);
		}
		for (Object element : listGlobal.getCheckBoxListSelectedValues()) {
			projects.add((String) element);
		}
		return projects;
	}
	
	/**
	 * Utility function to create the project lists.
	 * 
	 * @param parent
	 *            the container into which to add the lists.
	 * @param title
	 *            the title of the list.
	 * @return the actual list component created.
	 */
	private CheckBoxList buildProjectList(JComponent parent, String title) {
		JPanel container = new JPanel();
		parent.add(container);
		container.setBorder(BorderFactory.createTitledBorder(title));
		GridBagLayout layout = new GridBagLayout();
		container.setLayout(layout);
		
		JScrollPane listScroller = new JScrollPane();
		container.add(listScroller);
		buildConstraints(listScroller, layout, 0, 2);
		listScroller
				.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		listScroller.setPreferredSize(new Dimension());
		
		final CheckBoxList list = new CheckBoxList();
		listScroller.setViewportView(list);
		disableWhileSynchronizing.add(list);
		list.setBorder(BorderFactory.createEmptyBorder());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JButton all = new JButton(Messages.getString("SynchronizationFrame.18"), new ImageIcon( //$NON-NLS-1$
				Configuration.IMAGES_DIR + "list-add.png")); //$NON-NLS-1$
		container.add(all);
		buildConstraints(all, layout, 0, 1, 1, 0);
		disableWhileSynchronizing.add(all);
		all.setFocusable(false);
		all.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				list.selectAll();
			}
		});
		
		JButton none = new JButton(Messages.getString("SynchronizationFrame.20"), new ImageIcon( //$NON-NLS-1$
				Configuration.IMAGES_DIR + "list-remove.png")); //$NON-NLS-1$
		container.add(none);
		buildConstraints(none, layout, 1, 1, 1, 0);
		disableWhileSynchronizing.add(none);
		none.setFocusable(false);
		none.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				list.selectNone();
			}
		});
		
		return list;
	}
	
	/**
	 * Utility method for setting up gridbaglayouts.
	 */
	private void buildConstraints(Component c, GridBagLayout layout, int y, int w) {
		buildConstraints(c, layout, 0, y, w, 1);
	}
	
	/**
	 * Utility method for setting up gridbaglayouts.
	 */
	private static void buildConstraints(Component c, GridBagLayout layout, int x, int y, int w, double wy) {
		layout.setConstraints(c, new GridBagConstraints(x, y, w, 1, (double) 1, wy,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						5, 5, 5, 5), 0, 0));
	}
	
	/**
	 * Synchronizes two checkbox lists based on the first given list.
	 * 
	 * <p>
	 * All elements selected in the first list will also be enabled in the
	 * second list, all elements selected in the second list which are
	 * <em>not</em> selected in the first list will be deselected.
	 * </p>
	 * 
	 * @param list1
	 *            the first list (base).
	 * @param list2
	 *            the second list (target).
	 */
	private void synchronizeSelection(CheckBoxList list1, CheckBoxList list2) {
		if (syncingLists) {
			return;
		}
		syncingLists = true;
		for (Object entry : list1.getCheckBoxListSelectedValues()) {
			list2.addCheckBoxListSelectedValue(entry, true);
		}
		
		for (Object entry : list2.getCheckBoxListSelectedValues()) {
			if (modelContains(list1.getModel(), entry) &&
					!arrayContains(list1.getCheckBoxListSelectedValues(), entry))
			{
				list2.removeCheckBoxListSelectedValue(entry, false);
			}
		}
		syncingLists = false;
	}
	
	/**
	 * Utility function to check if an element exists in an array. This function
	 * has a linear runtime behavior.
	 * 
	 * @param array
	 *            the array to search through.
	 * @param value
	 *            the value to search for.
	 * @return <code>true</code> if the array contains the element, else
	 *         <code>false</code>.
	 */
	private static boolean arrayContains(Object[] array, Object value) {
		for (Object entry : array) {
			if (entry.equals(value)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Utility function to check if an element exists in a list model. This
	 * function has a linear runtime behavior.
	 * 
	 * @param model
	 *            the list model to search through.
	 * @param value
	 *            the value to search for.
	 * @return <code>true</code> if the array contains the element, else
	 *         <code>false</code>.
	 */
	private static boolean modelContains(ListModel model, Object value) {
		for (int i = 0; i < model.getSize(); ++i) {
			if (model.getElementAt(i).equals(value)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Logs a default message to the information text area.
	 * 
	 * @param message
	 *            the message to log.
	 */
	private void logMessage(String message) {
		logMessage(message, LogType.DEFAULT);
	}
	
	/**
	 * Logs a message to the information text area.
	 * 
	 * @param message
	 *            the message to log.
	 * @param type
	 *            the message type, only results in a different color.
	 */
	private void logMessage(String message, LogType type) {
		if (message == null) {
			return;
		}
		// Set color depending on the type.
		SimpleAttributeSet style = new SimpleAttributeSet();
		switch (type) {
		case WARNING:
			StyleConstants.setForeground(style, Color.ORANGE);
			break;
		case ERROR:
			StyleConstants.setForeground(style, Color.RED);
			break;
		case SYSTEM:
			StyleConstants.setForeground(style, Color.LIGHT_GRAY);
			break;
		case DEFAULT:
		default:
			StyleConstants.setForeground(style, Color.BLACK);
			break;
		}
		
		// Append the text with timestamp.
		try {
			Document document = output.getDocument();
			document.insertString(document.getLength(),
					String.format("[%s] %s\n", //$NON-NLS-1$
							DateFormat.getTimeInstance().format(new Date()),
							message),
					style);
			output.setCaretPosition(document.getLength());
		} catch (BadLocationException ignored) {
		}
	}
	
	/**
	 * Used to listen to updates of the synchronization process.
	 * 
	 * <p>
	 * {@inheritDoc}
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (!evt.getPropertyName().equals(
				SynchronizationWorker.PROPERTY_PROGRESS)
				|| !(evt.getNewValue() instanceof SynchronizationProgress)) {
			return;
		}
		final SynchronizationProgress progress =
				(SynchronizationProgress) evt.getNewValue();
		switch (progress.getType()) {
		case STARTED:
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					logMessage(progress.getMessage(), LogType.SYSTEM);
					progressOverall.setValue(0);
				}
			});
			break;
		case WARNING:
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					logMessage(progress.getMessage(), LogType.WARNING);
				}
			});
			break;
		case ERROR:
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					logMessage(progress.getMessage(), LogType.ERROR);
					progressOverall.setValue(100);
					updateProjectLists();
					for (Component c : disableWhileSynchronizing) {
						c.setEnabled(true);
					}
					setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			});
			break;
		case COMPLETE:
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					logMessage(progress.getMessage(), LogType.SYSTEM);
					progressOverall.setValue(100);
					updateProjectLists();
					for (Component c : disableWhileSynchronizing) {
						c.setEnabled(true);
					}
					setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			});
			break;
		case UPDATE:
		default:
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					logMessage(progress.getMessage());
					progressOverall.setValue((int)(progress.getProgress() * progressOverall.getMaximum()));
				}
			});
			break;
		}
	}
}
