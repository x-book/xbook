package ossobook.client.gui.administration;

import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.base.metainfo.Project;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.queries.QueryManager;

/**
 * controls the right changing process
 * 
 * @author j.lamprecht
 * 
 */
public class UserAdministration implements Observer {

	private final QueryManager manager;
	private final AdminWindow window;
	private final String projName;

	/**
	 * initializes AdminWindow to get user for which rights shall be changed and
	 * to get the right to which shall be changed
	 * 
	 * @param manager
	 * @param projName
	 *            : name of the project for that rights shall be changed
	 */
	public UserAdministration(QueryManager manager, String projName) {
		this.manager = manager;
		this.projName = projName;
		Vector<String> users = new Vector<String>();
		try {
			users = manager.getUserNames();
		} catch (StatementNotExecutedException e) {
			LogFactory.getLog(UserAdministration.class).error(e, e);
		}
		window = new AdminWindow(users, projName);
		window.addObserver(this);
	}

	/**
	 * rights shall be changed
	 */
	public void update(Observable o, Object arg) {
		if (o == window) {
			startChangingRight();
		}
	}

	/**
	 * gets the users and the right: starts the changing of the given right
	 * owned by the single users to the selected right
	 */
	private void startChangingRight() {
		Vector<String> users = window.getUsers();
		int right = window.getRight();
		boolean missingInfo = false;

		// project owner has selected no user
		if (users.size() == 0) {
			JOptionPane.showMessageDialog(window.getPanel(),
					Messages.getString("UserAdministration.0"), //$NON-NLS-1$
					Messages.getString("UserAdministration.1"), //$NON-NLS-1$
					JOptionPane.WARNING_MESSAGE);
			missingInfo = true;
		}

		// project owner has selected no right
		if (right == AdminWindow.NOTHING) {
			JOptionPane.showMessageDialog(window.getPanel(),
					Messages.getString("UserAdministration.2"), //$NON-NLS-1$
					Messages.getString("UserAdministration.3"), //$NON-NLS-1$
					JOptionPane.WARNING_MESSAGE);
			missingInfo = true;
		}

		if (missingInfo) {
			return;
		}

		try {
			for (int i = 0; i < users.size(); i++) {
				Project project = manager.getProject(projName);
				manager.changeRight(project, users.elementAt(i), right);
			}

			JOptionPane.showMessageDialog(window.getPanel(),
					Messages.getString("UserAdministration.4"), //$NON-NLS-1$
					Messages.getString("UserAdministration.5"), //$NON-NLS-1$
					JOptionPane.WARNING_MESSAGE);
		} catch (StatementNotExecutedException e) {
			JOptionPane
					.showMessageDialog(
							window.getPanel(),
							Messages.getString("UserAdministration.6") //$NON-NLS-1$
									+ Messages.getString("UserAdministration.7"), //$NON-NLS-1$
							Messages.getString("UserAdministration.8"), //$NON-NLS-1$
							JOptionPane.WARNING_MESSAGE);
		}

	}

	public JPanel getPanel() {
		return window.getPanel();
	}
}
