package ossobook.client.gui.common;

import java.beans.PropertyVetoException;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.base.BaseFacade;
import ossobook.client.base.metainfo.Project;
import ossobook.client.base.scheme.SchemeArtefacts;
import ossobook.client.base.scheme.SchemeBones;
import ossobook.client.base.scheme.SchemeIndet;
import ossobook.client.gui.administration.UserAdministration;
import ossobook.client.gui.update.components.window.ProjektFenster;
import ossobook.client.io.database.ConnectionType;
import ossobook.client.util.Configuration;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.queries.QueryManager;
import ossobook.queries.QueryManagerFactory;

/**
 * builds up project manipulation window - composed if ProjektFenster,
 * UserAdministration window and ProjectConnectionInformation window
 * 
 * @author j.lamprecht
 * 
 */
public class ProjectManipulation extends JInternalFrame implements AncestorListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final ProjektFenster projectf;

	/**
	 * builds up ProjectManipulation window
	 * 
	 * @param projName
	 *            : name of the project which can be manipulated
	 * @param parentReference
	 * @param manager
	 * @param project
	 *            : number of the project which can be manipulated
	 */
	public ProjectManipulation(String projName,
			OssobookFrame parentReference, QueryManager manager,
			Project project) {

                addAncestorListener(this);
		JTabbedPane register = new JTabbedPane();

		// ProjektFenster
		projectf = new ProjektFenster(Messages.getString("ProjectManipulation.0") + QueryManagerFactory.getUsername() //$NON-NLS-1$
				+ Messages.getString("ProjectManipulation.1") + projName, manager, new SchemeIndet(), //$NON-NLS-1$
        		new SchemeBones(parentReference, new BaseFacade(manager), project),
				new SchemeArtefacts(parentReference), project, this, parentReference, parentReference.getDesktop());

		register.addTab(Messages.getString("ProjectManipulation.2"), projectf.getUpdateWindow()); //$NON-NLS-1$

		// UserAdministration
		UserAdministration admin = new UserAdministration(manager, projName);
		String title = Messages.getString("ProjectManipulation.3"); //$NON-NLS-1$
		register.addTab(title, admin.getPanel());
		Boolean enabled = false;
		try {
			enabled = (manager.getConnectionType() == ConnectionType.CONNECTION_GLOBAL) &&
					(manager.isProjectOwner(manager.getProject(projName)) || manager.getIsAdmin());
		} catch (StatementNotExecutedException e) {
			LogFactory.getLog(ProjectManipulation.class).error(e, e);
		}
		register.setEnabledAt(register.indexOfTab(title), enabled);

		// connection information
		ProjectConnectionInformation information = new ProjectConnectionInformation(
				manager);
		register.addTab(Messages.getString("ProjectManipulation.5"), information.getPanel()); //$NON-NLS-1$

		setTitle(project.getName());
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setResizable(true);
		setMaximizable(true);
		setIconifiable(true);
		setClosable(true);
		setContentPane(register);
		setVisible(true);
		parentReference.getDesktop().add(this);
		try {
			setSelected(true);
		} catch (PropertyVetoException ef) {
			ef.printStackTrace();
			JOptionPane.showMessageDialog(null, Messages.getString("ProjectManipulation.6") //$NON-NLS-1$
					+ ef.getStackTrace().toString() + ": " + toString()); //$NON-NLS-1$
		}
	}

	public ProjektFenster getProjectWindow() {
		return projectf;
	}
	
        @Override
        public void ancestorRemoved(AncestorEvent event) {
        }
        @Override
        public void ancestorMoved(AncestorEvent event) {
        }
        @Override
        public void ancestorAdded(AncestorEvent event) {
            if (event.getAncestor() == this) {
                int xSize = getParent().getWidth()
                                    - Configuration.projektbasisfensterBorderRight;
                this.setSize(xSize, Configuration.projektbasisfenster_y);
            }
        }
}
