package ossobook.client.gui.common;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import ossobook.Messages;
import ossobook.client.io.database.ConnectionType;
import ossobook.queries.QueryManager;
import ossobook.queries.QueryManagerFactory;

/**
 * informs the user which database he uses and which user name
 * 
 * @author j.lamprecht
 * 
 */
class ProjectConnectionInformation {

	private final JPanel panel;

	/**
	 * builds up ProjectConnectionInformation window
	 * 
	 * @param manager
	 */
	public ProjectConnectionInformation(QueryManager manager) {

		panel = new JPanel();
		JTextArea description = new JTextArea();
		description.setEditable(false);
		description.setBackground(new Color(236, 233, 216));

		if (manager.getConnectionType() == ConnectionType.CONNECTION_LOCAL) {
			description.setText(Messages.getString("ProjectConnectionInformation.0", QueryManagerFactory.getUsername())); //$NON-NLS-1$
		} else {
			description.setText(Messages.getString("ProjectConnectionInformation.1", QueryManagerFactory.getUsername())); //$NON-NLS-1$
		}

		panel.add(description);
		panel.setVisible(true);
	}

	public JPanel getPanel() {
		return panel;
	}

}
