package ossobook.client.gui.common;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.base.metainfo.Project;
import ossobook.client.base.scheme.SchemeArtefacts;
import ossobook.client.base.scheme.SchemeBones;
import ossobook.client.events.EventType;
import ossobook.client.events.ObservableSupport;
import ossobook.client.events.OssoBookEvent;
import ossobook.client.gui.update.components.other.TabellenAnsicht;
import ossobook.client.gui.update.components.window.ProjektFenster;
import ossobook.client.io.CSVExport;
import ossobook.client.util.Configuration;

/**
 * The main menu bar of the application.
 *
 * @author ali
 * @author fnuecke
 */
public class MainMenuBar extends JMenuBar {
	
	private static final long		serialVersionUID	= -121867104259180093L;
	
	/**
	 * Logging...
	 */
	private static final Log				log					= LogFactory.getLog(MainMenuBar.class);
	
	/**
	 * Observable support.
	 */
	private final ObservableSupport	observable			= new ObservableSupport();
	
	/**
	 * Components in this list will only be enabled while logged in.
	 */
	private final Collection<Component>	onlyWhenLoggedIn	= new ArrayList<Component>();
	
	/**
	 * Components in this list will only be enabled while logged in and having
	 * administrator rights.
	 */
	private final Collection<Component>	onlyWhenAdmin		= new ArrayList<Component>();
	
	/**
	 * Components in this list will only be enabled while logged out.
	 */
	private final Collection<Component>	onlyWhenLoggedOut	= new ArrayList<Component>();
	
	private final OssobookFrame			ossobookFrame;
	
	private JInternalFrame			durchsucheAlleFrame	= null;
	
	private String					bedingung;

	/**
	 * Creates the menu bar, instantiating all the menu entries and setting them
	 * up to notify the controller when clicked.
	 * 
	 * @param parent
	 * @param controller
	 *            the controller that will be notified when a menu item is
	 *            selected.
	 */
	public MainMenuBar(OssobookFrame parent, Observer controller) {
		ossobookFrame = parent;
		observable.addObserver(controller);
		
		// Reused temporary variables.
		JMenu menu;
		JMenuItem item;
		
		// FILE -------------------------------
		menu = new JMenu(Messages.getString("MainMenuBar.2"));// MENU //$NON-NLS-1$
		add(menu);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.14"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "system-users.png")); //$NON-NLS-1$
		onlyWhenLoggedOut.add(item);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L,
				ActionEvent.CTRL_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.LOGIN);
			}
		});
		menu.add(item);
		
		menu.addSeparator();
		
		item = new JMenuItem(Messages.getString("MainMenuBar.4"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "document-new.png")); //$NON-NLS-1$
		onlyWhenLoggedIn.add(item);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.NEW_PROJECT);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.15"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "document-open.png")); //$NON-NLS-1$
		onlyWhenLoggedIn.add(item);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.OPEN_PROJECT);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.5"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "edit-delete.png")); //$NON-NLS-1$
		onlyWhenLoggedIn.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.DELETE_PROJECT);
			}
		});
		menu.add(item);
		
		// disabled until action defined
//		item = new JMenuItem(Messages.getString("MainMenuBar.16")); //$NON-NLS-1$
//		onlyWhenLoggedIn.add(item);
//		item.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//			}
//		});
//		menu.add(item);
		
		menu.addSeparator();
		
		item = new JMenuItem(Messages.getString("MainMenuBar.18"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "document-save.png")); //$NON-NLS-1$
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		onlyWhenLoggedIn.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				csvexport();
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.19")); //$NON-NLS-1$
		onlyWhenLoggedIn.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				csvexportKeineMasse();

			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.17")); //$NON-NLS-1$
		onlyWhenLoggedIn.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String felder = JOptionPane.showInputDialog(
						Messages.getString("MainMenuBar.1"), "*"); //$NON-NLS-1$ //$NON-NLS-2$
				String where = JOptionPane.showInputDialog(
						Messages.getString("MainMenuBar.3"), "1"); //$NON-NLS-1$ //$NON-NLS-2$
				if (where.equals("")) { //$NON-NLS-1$
					where = "1"; //$NON-NLS-1$
				}
				csvexportAllgemein(felder, where);
			}
		});
		menu.add(item);
		
		menu.addSeparator();
		
		item = new JMenuItem(Messages.getString("MainMenuBar.20"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "edit-find.png")); //$NON-NLS-1$
		onlyWhenLoggedIn.add(item);
		item.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				suchenNach();

			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.21"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "system-search.png")); //$NON-NLS-1$
		onlyWhenLoggedIn.add(item);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F,
				ActionEvent.CTRL_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				suchenNach("1"); //$NON-NLS-1$
			}
		});
		menu.add(item);
		
		menu.addSeparator();
		
		item = new JMenuItem(Messages.getString("MainMenuBar.22"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "mail-send-receive.png")); //$NON-NLS-1$
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		onlyWhenLoggedIn.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.SYNCHRONIZE);
			}
		});
		menu.add(item);
		
		menu.addSeparator();
		
		item = new JMenuItem(Messages.getString("MainMenuBar.6"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "system-log-out.png")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.EXIT);
			}
		});
		menu.add(item);
		
		// TEMPLATE -------------------------------
		menu = new JMenu(Messages.getString("MainMenuBar.23"));// MENU //$NON-NLS-1$
		onlyWhenLoggedIn.add(menu);
		add(menu);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.24")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				zeigeTemplateIndet();

			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.25")); //$NON-NLS-1$
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K,
				ActionEvent.ALT_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				zeigeTemplateKnochen();

			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.26")); //$NON-NLS-1$
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,
				ActionEvent.ALT_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				zeigeTemplateArtefakt();

			}
		});
		menu.add(item);
		
		// ANALYSIS -------------------------------
		menu = new JMenu(Messages.getString("MainMenuBar.27"));// MENU //$NON-NLS-1$
		onlyWhenLoggedIn.add(menu);
		add(menu);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.28")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.SKELETON_ANALYSIS);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.29")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.ANIMAL_ANALYSIS);
			}
		});
		menu.add(item);
		
		// TOOLS -------------------------------
		menu = new JMenu(Messages.getString("MainMenuBar.30"));// MENU //$NON-NLS-1$
		onlyWhenLoggedIn.add(menu);
		add(menu);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.31")); //$NON-NLS-1$
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, ActionEvent.ALT_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.TIERARTSUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.32")); //$NON-NLS-1$
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F6, ActionEvent.ALT_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.SKELTEILSUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.35")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.ALTER1SUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.36")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.ALTER2SUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.37")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.ALTER3SUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.38")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.ALTER4SUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.39")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.ALTER5SUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.34")); //$NON-NLS-1$
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F7, ActionEvent.ALT_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.ERHALTUNGSUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.40")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.WURZELFRASS);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.41")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.VERSINTERUNG);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.42")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.FETTIGSUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.43")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.PATINASUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.44")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.BRANDSPURSUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.45")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.VERBISSSUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.46")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.SCHLACHTSPUR1);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.47")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.SCHLACHTSPUR2);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.48")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.GESCHLECHT);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.33")); //$NON-NLS-1$
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F8, ActionEvent.ALT_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.BRUCHKANTESUCHE);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.49")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.BRUCHKANTE2);
			}
		});
		menu.add(item);
		
		// CODES -------------------------------
		menu = new JMenu(Messages.getString("MainMenuBar.50")); //$NON-NLS-1$
		onlyWhenLoggedIn.add(menu);
		onlyWhenAdmin.add(menu);
		add(menu);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.51")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.ADD_ANIMAL_CODE);
			}
		});
		menu.add(item);
		
		// DATAMINING -------------------------------
		menu = buildDataMiningMenu(null, "datamining"); //$NON-NLS-1$
		if (menu != null) {
			add(menu);
		}
		
		// HELP -------------------------------
		menu = new JMenu(Messages.getString("MainMenuBar.52")); //$NON-NLS-1$
		add(menu);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.53"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "dialog-information.png")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.SHOW_ABOUT);
			}
		});
		menu.add(item);
		
		item = new JMenuItem(Messages.getString("MainMenuBar.55")); //$NON-NLS-1$
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.SHOW_LOG);
			}
		});
		menu.add(item);

		menu.addSeparator();
		
		item = new JMenuItem(Messages.getString("MainMenuBar.7"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "view-refresh.png")); //$NON-NLS-1$
		onlyWhenLoggedIn.add(item);
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0));
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fireEvent(EventType.CHECK_FOR_UPDATES);
			}
		});
		menu.add(item);
		
		// Disable components that only work when logged in.
		for (Component c : onlyWhenLoggedIn) {
			c.setEnabled(false);
		}
	}
	
	/**
	 * Builds the menu item for datamining by parsing the config file.
	 * 
	 * @param menu
	 *			the menu item to which to attach items that will be parsed,
	 *			i.e. the menu item corresponding to the current search path.
	 * @param path
	 *			the current "path" in the menu definition.
	 * @return the generated (sub)menu.
	 */
	private static JMenu buildDataMiningMenu(JMenu menu, String path) {
		// Get title for entry.
		String title = Configuration.config.getProperty(path + ".title", //$NON-NLS-1$
				path.substring(path.lastIndexOf(".") + 1)); //$NON-NLS-1$
		// Yes, try to parse it as children and get the title.
		String[] children = Configuration.config.getProperty(path, "").split(","); //$NON-NLS-1$ //$NON-NLS-2$
		// Check if it's a JAR file definition.
		if (children.length == 1 && children[0].endsWith(".jar")) { //$NON-NLS-1$
			// Path to a jar, this is a leaf.
			JMenuItem item = new JMenuItem(title);
			item.setActionCommand(children[0]);
			item.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						Runtime.getRuntime().exec(String.format("cmd /c java -jar %s", //$NON-NLS-1$
								System.getProperty("user.dir") //$NON-NLS-1$
								+ System.getProperty("file.separator") //$NON-NLS-1$
								+ e.getActionCommand()));
					} catch (IOException ex) {
						System.out.println(
								Messages.getString("MainMenuBar.8")); //$NON-NLS-1$
					}
				}
			});
			menu.add(item);
		} else if (children.length > 0 && !children[0].equals("")) { //$NON-NLS-1$
			// Submenu.
			JMenu submenu = new JMenu(title);
			for (String child : children) {
				buildDataMiningMenu(submenu, path + "." + child.trim()); //$NON-NLS-1$
			}
			// Check if the menu parameter is given, if not initialize it
			// to this sumenu (used for root node).
			if (menu == null) {
				menu = submenu;
			} else {
				menu.add(submenu);
			}
		}
		return menu;
	}
	
	/**
	 * Used for simplified firing of events.
	 * 
	 * @param type
	 *            the type of event to trigger in the controller.
	 */
	private void fireEvent(EventType type) {
		observable.fire(new OssoBookEvent(type));
	}
	
	/**
	 * Notify of successful login to enable menu entries accordingly.
	 * 
	 * @param isAdmin
	 *            whether the user is logged in as admin or not.
	 */
	public void loginCompleted(boolean isAdmin) {
		for (Component c : onlyWhenLoggedIn) {
			c.setEnabled(isAdmin || !onlyWhenAdmin.contains(c));
		}
		for (Component c : onlyWhenLoggedOut) {
			c.setEnabled(false);
		}
	}
	
	/**
	 *
	 */
	private void csvexport() {
		File f = null;
		String filePath = null;
		Project project;

		// choose file
		try {
			JFileChooser fc = new JFileChooser();
			ossobookFrame.getDesktop().add(fc);
			int returnVal = fc.showSaveDialog(ossobookFrame);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				filePath = fc.getSelectedFile().getAbsolutePath();
				f = new File(csvexportname(filePath));
			}
		}
		catch (Exception exception) {
			return;
		}

		ProjektFenster p;
		try {
			ProjectManipulation window = this.getSelectedProject();
			p = window.getProjectWindow();
			window.moveToFront();
			window.setSelected(true);

			project = p.getProject();
		}
		catch (Exception exception) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.9")); //$NON-NLS-1$
			return;
		}

		try {
			CSVExport export = new CSVExport(p.getManager().getConnection());

			String exportedFields =
				"fK,phase1,phase2,phase3,phase4,BenutzerName,inventarNr,xAchse,yAchse," + //$NON-NLS-1$
				"tierart,skelettteil,knochenteil,pathologie,alter1,alter2,alter3,alter4,alter5," + //$NON-NLS-1$
				"bruchkante,bruchkante2,erhaltung,wurzelfrass,kruste,fettig,patina,brandspur," + //$NON-NLS-1$
				"verbiss,verdaut,schlachtS1,schlachtS2,feuchtboden,geschlecht,koerperseite," + //$NON-NLS-1$
				"gewicht,massID,massNotiz,objektNotiz,skelNr,knIndNr,groesse1,groesse2,saison," + //$NON-NLS-1$
				"jahrring,zusatzFeld1,zusatzFeld2,zusatzFeld3,zusatzFeld4,anzahl,indivnrsicher," + //$NON-NLS-1$
				"TierName,DTier,SkelName"; //$NON-NLS-1$

			String query = "SELECT " //$NON-NLS-1$
					+ exportedFields
					+ " FROM eingabeeinheit,tierart,skelteil WHERE projNr=" //$NON-NLS-1$
					+ project.getNumber()
					+ " AND eingabeeinheit.DBNummerProjekt=" //$NON-NLS-1$
					+ project.getDatabaseNumber()
					+ " AND tierart.TierCode=eingabeeinheit.tierart AND skelteil.skelCode=eingabeeinheit.skelettteil " //$NON-NLS-1$
					+ "AND eingabeeinheit.geloescht='N' ORDER BY eingabeeinheit.ID;"; //$NON-NLS-1$

			if (log.isDebugEnabled()) {
				log.debug("Query: " + query); //$NON-NLS-1$
			}

			export.export(query, f, ossobookFrame);
		}
		catch (Exception exception) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.58")); //$NON-NLS-1$
			return;
		}

		try {
			f = new File(csvexportname(filePath + "_Masse")); //$NON-NLS-1$

			ProjectManipulation window = this.getSelectedProject();
			p = window.getProjectWindow();
			window.moveToFront();
			window.setSelected(true);

			project = p.getProject();
		}
		catch (Exception exception) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.9")); //$NON-NLS-1$
			return;
		}

		try {
			CSVExport export = new CSVExport(p.getManager().getConnection());

			String massFields =
				"eingabeeinheit.fK ,eingabeeinheit.phase1, eingabeeinheit.phase2, " + //$NON-NLS-1$
				"eingabeeinheit.phase3, eingabeeinheit.phase4, eingabeeinheit.tierart, " + //$NON-NLS-1$
				"tierart.TierName, eingabeeinheit.skelettteil, skelteil.SkelName, " + //$NON-NLS-1$
				"masse.MassID, masse.Datenbanknummer, " + //$NON-NLS-1$
				"masse.mass1, masse.mass2, masse.mass3, masse.mass4, masse.mass5, " + //$NON-NLS-1$
				"masse.mass6, masse.mass7, masse.mass8, masse.mass9, masse.mass10, " + //$NON-NLS-1$
				"masse.mass11, masse.mass12, masse.mass13, masse.mass14, masse.mass15, " + //$NON-NLS-1$
				"masse.mass16, masse.mass17, masse.mass18, masse.mass19, masse.mass20, " + //$NON-NLS-1$
				"masse.mass21, masse.mass22, masse.mass23, masse.mass24, masse.mass25, " + //$NON-NLS-1$
				"masse.mass26, masse.mass27, masse.mass28, masse.mass29, masse.mass30, " + //$NON-NLS-1$
				"masse.mass31, masse.mass32, masse.mass33, masse.mass34, masse.mass35, " + //$NON-NLS-1$
				"masse.mass36, masse.mass37, masse.mass38, masse.mass39, masse.mass40, " + //$NON-NLS-1$
				"masse.mass41, masse.mass42, masse.mass43, masse.mass44, masse.mass45, " + //$NON-NLS-1$
				"masse.mass46, masse.mass47, masse.mass48, masse.mass49, masse.mass50, " + //$NON-NLS-1$
				"masse.mass51, masse.mass52, masse.mass53, masse.mass54, masse.mass55, " + //$NON-NLS-1$
				"masse.mass56, masse.mass57, masse.mass58, masse.mass59, masse.mass60, " + //$NON-NLS-1$
				"masse.mass61, masse.mass62, masse.mass63, masse.mass64, masse.mass65, " + //$NON-NLS-1$
				"masse.mass66, masse.mass67, masse.mass68, masse.mass69, masse.mass70, " + //$NON-NLS-1$
				"masse.mass71, masse.mass72, masse.mass73, masse.mass74, masse.mass75, " + //$NON-NLS-1$
				"masse.mass76, masse.mass77, masse.mass78, masse.mass79, masse.mass80, " + //$NON-NLS-1$
				"masse.mass81, masse.mass82, masse.mass83, masse.mass84, masse.mass85, " + //$NON-NLS-1$
				"masse.mass86, masse.mass87, masse.mass88, masse.mass89, masse.mass90, " + //$NON-NLS-1$
				"masse.mass91, masse.mass92, masse.mass93, masse.mass94, masse.mass95, " + //$NON-NLS-1$
				"masse.mass96, masse.mass97, masse.mass98, masse.mass99, masse.mass100, " + //$NON-NLS-1$
				"masse.mass101, masse.mass102, masse.mass103, masse.mass104, masse.mass105, " + //$NON-NLS-1$
				"masse.mass106, masse.mass107, masse.mass108, masse.mass109, masse.mass110, " + //$NON-NLS-1$
				"masse.mass111, masse.mass112, masse.mass113, masse.mass114, masse.mass115, " + //$NON-NLS-1$
				"masse.mass116, masse.mass117, masse.mass118, masse.mass119, masse.mass120, " + //$NON-NLS-1$
				"masse.mass121, masse.mass122, masse.mass123, masse.mass124, masse.mass125, " + //$NON-NLS-1$
				"masse.mass126, masse.mass127, masse.mass128, masse.mass129, masse.mass130, " + //$NON-NLS-1$
				"masse.mass131, masse.mass132, masse.mass133, masse.mass134, masse.mass135, " + //$NON-NLS-1$
				"masse.mass136, masse.mass137, masse.mass138, masse.mass139, masse.mass140, " + //$NON-NLS-1$
				"masse.mass141, masse.mass142, masse.mass143, masse.mass144, masse.mass145, " + //$NON-NLS-1$
				"masse.mass146, masse.mass147, masse.mass148, masse.mass149, masse.mass150, " + //$NON-NLS-1$
				"masse.mass151, masse.mass152"; //$NON-NLS-1$

			String query = "SELECT " //$NON-NLS-1$
					+ massFields
					+ " FROM eingabeeinheit,masse,tierart,skelteil WHERE projNr=" //$NON-NLS-1$
					+ project.getNumber()
					+ " AND eingabeeinheit.DBNummerProjekt=" //$NON-NLS-1$
					+ project.getDatabaseNumber()
					+ " AND masse.MassID=eingabeeinheit.massID " //$NON-NLS-1$
					+ " AND masse.Datenbanknummer=eingabeeinheit.DBNummerMasse " //$NON-NLS-1$
					+ " AND eingabeeinheit.geloescht='N' " //$NON-NLS-1$
					+ " AND masse.geloescht='N' " //$NON-NLS-1$
					+ " AND eingabeeinheit.tierart=tierart.TierCode " //$NON-NLS-1$
					+ " AND eingabeeinheit.skelettteil=skelteil.SkelCode" //$NON-NLS-1$
					+ " ORDER BY eingabeeinheit.fK, eingabeeinheit.phase1, eingabeeinheit.phase2, " //$NON-NLS-1$
					+ "eingabeeinheit.phase3, eingabeeinheit.phase4, eingabeeinheit.ID;"; //$NON-NLS-1$

			if (log.isDebugEnabled()) {
				log.debug("Query: " + query); //$NON-NLS-1$
			}

			export.export(query, f, ossobookFrame);
		}
		catch (Exception exception) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.58")); //$NON-NLS-1$
        }

	}

	/**
	 *
	 */
	private void csvexportKeineMasse() {
		File f = null;
		Project project;

		// choose file
		try {
			JFileChooser fc = new JFileChooser();
			ossobookFrame.getDesktop().add(fc);
			int returnVal = fc.showSaveDialog(ossobookFrame);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				f = new File(csvexportname(fc.getSelectedFile().getAbsolutePath()));
			}
		}
		catch (Exception exception) {
			return;
		}

		ProjektFenster p;
		try {
			ProjectManipulation window = this.getSelectedProject();
			p = window.getProjectWindow();
			window.moveToFront();
			window.setSelected(true);

			project = p.getProject();
		}
		catch (Exception exception) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.9")); //$NON-NLS-1$
			return;
		}

		try {
			CSVExport export = new CSVExport(p.getManager().getConnection());

			String exportedFields = "*"; //$NON-NLS-1$

			String query = "SELECT " //$NON-NLS-1$
					+ exportedFields
					+ " FROM eingabeeinheit,tierart,skelteil WHERE projNr=" //$NON-NLS-1$
					+ project.getNumber()
					+ " AND eingabeeinheit.DBNummerProjekt=" //$NON-NLS-1$
					+ project.getDatabaseNumber()
					+ " AND tierart.TierCode=eingabeeinheit.tierart" //$NON-NLS-1$
					+ " AND skelteil.skelCode=eingabeeinheit.skelettteil" //$NON-NLS-1$
					+ " AND eingabeeinheit.geloescht='N' ORDER BY eingabeeinheit.ID;"; //$NON-NLS-1$

			if (log.isDebugEnabled()) {
				log.debug("Query: " + query); //$NON-NLS-1$
			}

			export.export(query, f, ossobookFrame);
		}
		catch (Exception exception) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.58")); //$NON-NLS-1$
        }
	}

	/**
	 *
	 */
	private void csvexportAllgemein(String felder, String whereclause) {
		File f = null;
		Project project;

		// choose file
		try {
			JFileChooser fc = new JFileChooser();
			ossobookFrame.getDesktop().add(fc);
			int returnVal = fc.showSaveDialog(ossobookFrame);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				f = new File(csvexportname(fc.getSelectedFile().getAbsolutePath()));
			}
		}
		catch (Exception exception) {
			return;
		}

		ProjektFenster p;
		try {
			ProjectManipulation window = this.getSelectedProject();
			p = window.getProjectWindow();
			window.moveToFront();
			window.setSelected(true);

			project = p.getProject();
		}
		catch (Exception exception) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.9")); //$NON-NLS-1$
			return;
		}

		try {
			CSVExport export = new CSVExport(p.getManager().getConnection());

			String query = "SELECT " //$NON-NLS-1$
					+ felder
					+ " FROM eingabeeinheit,tierart, skelteil WHERE projNr=" //$NON-NLS-1$
					+ project.getNumber()
					+ " AND eingabeeinheit.DBNummerProjekt=" //$NON-NLS-1$
					+ project.getDatabaseNumber()
					+ " AND tierart.TierCode=eingabeeinheit.tierart AND skelteil.skelCode=eingabeeinheit.skelettteil AND " //$NON-NLS-1$
					+ whereclause
					+ " AND eingabeeinheit.geloescht='N' ORDER BY eingabeeinheit.ID;"; //$NON-NLS-1$

			if (log.isDebugEnabled()) {
				log.debug("Query: " + query); //$NON-NLS-1$
			}

			export.export(query, f, ossobookFrame);
		}
		catch (Exception exception) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.58")); //$NON-NLS-1$
        }
	}

	/**
	 *
	 */
	private void suchenNach() {
		suchenNach(""); //$NON-NLS-1$
	}

	private void suchenNach(String bedingung) {
		System.out.println("==> suchenNach(String)");
		this.bedingung = bedingung;
		TabellenAnsicht result = produziereDurchsucheAlleTabelle(this.bedingung);

		if (result != null) {
			durchsucheAlleFrame = new JInternalFrame();
			result.setParentContainer(durchsucheAlleFrame);
			durchsucheAlleFrame.setSize(
					Configuration.durchsuchealleframe_x,
					Configuration.durchsuchealleframe_y);
			durchsucheAlleFrame.setClosable(true);
			durchsucheAlleFrame.setResizable(true);
			JScrollPane sp = new JScrollPane(result);
			durchsucheAlleFrame.getContentPane().add(sp);
			durchsucheAlleFrame.setVisible(true);
			ossobookFrame.getDesktop().add(durchsucheAlleFrame);
			try {
				durchsucheAlleFrame.setSelected(true);
			} catch (PropertyVetoException ignored) {
			}
		}
	}

	public void updateDurchsucheAlleFrame() {
		if (durchsucheAlleFrame.isVisible()) {
			TabellenAnsicht result = produziereDurchsucheAlleTabelle(bedingung);
			JScrollPane sp = new JScrollPane(result);
			durchsucheAlleFrame.getContentPane().removeAll();
			durchsucheAlleFrame.getContentPane().add(sp);

			try {
				durchsucheAlleFrame.setSelected(true);
			}
			catch (PropertyVetoException ignored) {
			}

			// if this property is not touched the display will fail. :-(
			durchsucheAlleFrame.setSize(new Dimension(
					(int) durchsucheAlleFrame.getSize().getWidth(),
					(int) durchsucheAlleFrame.getSize().getHeight()
				));
		}
	}

	private TabellenAnsicht produziereDurchsucheAlleTabelle(String bedingung) {
		this.bedingung = bedingung;
		TabellenAnsicht result;
		Project project;
		ProjektFenster p;

		if (this.bedingung.equals("")) { //$NON-NLS-1$
			this.bedingung = JOptionPane.showInputDialog(
					Messages.getString("MainMenuBar.96"), "1"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		try {
			ProjectManipulation window = this.getSelectedProject();
			p = window.getProjectWindow();
			window.moveToFront();
			window.setSelected(true);

			project = p.getProject();
		}
		catch (Exception exception) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.9")); //$NON-NLS-1$
			return null;
		}

		try {
			CSVExport export = new CSVExport(p.getManager().getConnection());

			String selectFields = 
				"eingabeeinheit.Datenbanknummer,eingabeeinheit.ID,artefaktID,DBNummerArtefakt," + //$NON-NLS-1$
				"massID,DBNummerMasse,fK,inventarNr,xAchse,yAchse,TierName,SkelName,knochenteil," + //$NON-NLS-1$
				"alter1,alter2,alter3,alter4,alter5,erhaltung,wurzelfrass,kruste,fettig,patina," + //$NON-NLS-1$
				"brandspur,verbiss,verdaut,schlachtS1,schlachtS2,feuchtboden,geschlecht," + //$NON-NLS-1$
				"koerperseite,gewicht,pathologie,bruchkante,bruchkante2,groesse1,groesse2,saison," + //$NON-NLS-1$
				"jahrring,skelNr,knIndNr,zusatzFeld1,zusatzFeld2,zusatzFeld3,zusatzFeld4,anzahl"; //$NON-NLS-1$

			String query = "SELECT " //$NON-NLS-1$
					+ selectFields
					+ " FROM eingabeeinheit,tierart,skelteil WHERE projNr=" //$NON-NLS-1$
					+ project.getNumber()
					+ " AND eingabeeinheit.DBNummerProjekt=" //$NON-NLS-1$
					+ project.getDatabaseNumber()
					+ " AND tierart.TierCode=eingabeeinheit.tierart" //$NON-NLS-1$
					+ " AND skelteil.skelCode=eingabeeinheit.skelettteil AND " //$NON-NLS-1$
					+ this.bedingung
					+ " AND eingabeeinheit.geloescht='N' ORDER BY eingabeeinheit.ID DESC;"; //$NON-NLS-1$

			if (log.isDebugEnabled()) {
				log.debug("Query: " + query); //$NON-NLS-1$
			}

			double time = System.currentTimeMillis();
			result = export.showResultTable(query, p, TabellenAnsicht.DATENSATZSUCHE);

			if (log.isTraceEnabled()) {
				log.trace("time for showResultTable: " //$NON-NLS-1$
						+ (System.currentTimeMillis() - time));
			}
		}
		catch (Exception f) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.58")); //$NON-NLS-1$
			return null;
		}

		return result;
	}
	
	/**
	 *
	 */
	private void zeigeTemplateArtefakt() {
		ProjektFenster p;

		try {
			ProjectManipulation window = this.getSelectedProject();
			p = window.getProjectWindow();
			window.moveToFront();
			window.setSelected(true);
		}
		catch (Exception exception) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.9")); //$NON-NLS-1$
			return;
		}

		try {
			SchemeArtefacts atemp = p.showArtefaktTemplate();
			ossobookFrame.getDesktop().add(atemp);
			atemp.moveToFront();
			atemp.setSelected(true);
		}
		catch (Exception exception) {
			exception.printStackTrace();
        }
	}

	/**
	 *
	 */
	private void zeigeTemplateIndet() {
		ProjektFenster p;

		try {
			ProjectManipulation window = this.getSelectedProject();
			p = window.getProjectWindow();
			window.moveToFront();
			window.setSelected(true);
		}
		catch (Exception exception) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.64")); //$NON-NLS-1$
			return;
		}

		try {
			ossobookFrame.getDesktop().add(p.showIndetTemplate());
		}
		catch (Exception exception) {
			exception.printStackTrace();
        }
	}

	/**
	 *
	 */
	private void zeigeTemplateKnochen() {
		ProjektFenster p;

		try {
			ProjectManipulation window = this.getSelectedProject();
			p = window.getProjectWindow();
			window.moveToFront();
			window.setSelected(true);
		}
		catch (Exception exception) {
			JOptionPane.showMessageDialog(ossobookFrame.getDesktop(),
					Messages.getString("MainMenuBar.9")); //$NON-NLS-1$
			return;
		}

		try {
			SchemeBones btemp = p.showKnochenTemplate();
			ossobookFrame.getDesktop().add(btemp);
			btemp.moveToFront();
			btemp.setSelected(true);
		}
		catch (Exception exception) {
			exception.printStackTrace();
        }
	}

	/**
	 *
	 */
	private static String csvexportname(String name) {
		// append csv to filename
		if (! name.endsWith(".txt")) { //$NON-NLS-1$
			return name + ".txt"; //$NON-NLS-1$
		}

		return name;
	}
	
	private ProjectManipulation getSelectedProject() {
		List<ProjectManipulation> projectList = this.getOpenProjectManipulationList();
		return (projectList.size() == 1)
			? projectList.get(0)
			: (ProjectManipulation) (ossobookFrame.getDesktop().getSelectedFrame());
	}
	
	/**
	 *
	 */
	private List<ProjectManipulation> getOpenProjectManipulationList() {
		JInternalFrame[] allFrames = ossobookFrame.getDesktop().getAllFrames();
		List<ProjectManipulation> result = new ArrayList<ProjectManipulation>();

        for (JInternalFrame allFrame : allFrames) {
            try {
                ProjectManipulation window = (ProjectManipulation) allFrame;
                //ProjektFenster p = window.getProjectWindow();

                // if last command did not throw an error, window is a project window
                result.add(window);

            } catch (Exception e) {
                // do nothing
            }
        }

		return result;
	}
}
