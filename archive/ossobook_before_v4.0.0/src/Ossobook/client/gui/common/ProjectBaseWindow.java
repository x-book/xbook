package ossobook.client.gui.common;

import javax.swing.JInternalFrame;
import javax.swing.WindowConstants;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import ossobook.client.util.Configuration;

/**
 * The Basic Class for all Ossobook Projects
 * 
 * @author ali
 * 
 */
@SuppressWarnings("serial")
public class ProjectBaseWindow extends JInternalFrame implements AncestorListener {
	private final String projektName;

	protected ProjectBaseWindow(String projektname) {
		projektName = projektname;
		addAncestorListener(this);
		settings();
	}

	/**
	 * sets up the frames Settings
	 * 
	 */
	private void settings() {
		setTitle(projektName);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setResizable(true);
		setMaximizable(true);
		setIconifiable(true);
		setClosable(true);
		setVisible(true);
	}

	@Override
        public void ancestorRemoved(AncestorEvent event) {
        }
        @Override
        public void ancestorMoved(AncestorEvent event) {
        }
        @Override
        public void ancestorAdded(AncestorEvent event) {
            if (event.getAncestor() == this) {
                int xSize = getParent().getWidth()
                                    - Configuration.projektbasisfensterBorderRight;
                    this.setSize(xSize, Configuration.projektbasisfenster_y);
            }
        }
}