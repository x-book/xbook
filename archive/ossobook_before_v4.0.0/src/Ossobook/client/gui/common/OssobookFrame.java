package ossobook.client.gui.common;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Arrays;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;

import ossobook.client.util.Configuration;

/**
 * The main window of the application, containing the main menu and desktop pane
 * for internal frames.
 * 
 * @author ali
 * @author fnuecke
 */
public class OssobookFrame extends JFrame {

	private static final long	serialVersionUID	= 913442356889935293L;
	
	/**
	 * Container for child windows.
	 */
	private final JDesktopPane		desktop;
	
	/**
	 * Application main menu (used to enable entries on successful login).
	 */
	private final MainMenuBar			mainMenu;
	
	/**
	 * Creates a new main frame for the application.
	 */
	public OssobookFrame(Observer controller) {
		super("OssoBook"); //$NON-NLS-1$
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Register icons for several resolutions.
		setIconImages(Arrays.asList(
                new ImageIcon(Configuration.IMAGES_DIR + "icon" + System.getProperty("file.separator") + "16.png").getImage(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				new ImageIcon(Configuration.IMAGES_DIR + "icon" + System.getProperty("file.separator") + "24.png").getImage(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				new ImageIcon(Configuration.IMAGES_DIR + "icon" + System.getProperty("file.separator") + "32.png").getImage(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				new ImageIcon(Configuration.IMAGES_DIR + "icon" + System.getProperty("file.separator") + "48.png").getImage(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				new ImageIcon(Configuration.IMAGES_DIR + "icon" + System.getProperty("file.separator") + "64.png").getImage(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				new ImageIcon(Configuration.IMAGES_DIR + "icon" + System.getProperty("file.separator") + "96.png").getImage(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				new ImageIcon(Configuration.IMAGES_DIR + "icon" + System.getProperty("file.separator") + "128.png").getImage())); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		
		// Set size.
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(Math.min(1024, screen.width), Math.min(768, screen.height - 100));
		
		// Center on screen/
		setLocationRelativeTo(null);
		
		// Create the desktop pane for MDI like child windows.
		desktop = new JDesktopPane();
		desktop.setBackground(Color.gray);
		setContentPane(desktop);
		
		// Create main menu.
		mainMenu = new MainMenuBar(this, controller);
		setJMenuBar(mainMenu);
		
		// Show.
		setVisible(true);
	}
	
	/**
	 * The internal desktop used to contain MDI like child windows (internal
	 * frames).
	 * 
	 * @return Returns the desktop.
	 */
	public JDesktopPane getDesktop() {
		return desktop;
	}
	
	/**
	 * The applications main menu.
	 * 
	 * @return the main menu bar.
	 */
	public MainMenuBar getMainMenu() {
		return mainMenu;
	}
}