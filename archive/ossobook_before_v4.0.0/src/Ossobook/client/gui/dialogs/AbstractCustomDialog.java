package ossobook.client.gui.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 * Superclass for simplified creation of dialogs that allow diversified
 * selection of custom data.
 * 
 * <p>
 * These dialogs will always have an "OK" and "Cancel" button, as well as some
 * added KeyListeners (e.g. for closing the dialog using the escape key).
 * </p>
 * 
 * @author fnuecke
 */
public abstract class AbstractCustomDialog extends JDialog {
	
	private static final long	serialVersionUID	= 8629079780411164930L;
	
	/**
	 * Whether the dialog was closed successfully or aborted.
	 */
	private boolean wasAborted;
	
	/**
	 * Listener used to determine "escape" key presses, to exit the dialog.
	 */
	private final KeyListener enterListener = new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				accept();
			}
		}
	};
	
	/**
	 * Listener used to determine "escape" key presses, to exit the dialog.
	 */
	private final KeyListener escapeListener = new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
				cancel();
			}
		}
	};
	
	/**
	 * Listener for components that trigger acceptance when their action event
	 * fires.
	 */
	private final ActionListener acceptListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			accept();
		}
	};
	
	/**
	 * Listener for components that trigger cancellation when their action event
	 * fires.
	 */
	private final ActionListener abortListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			cancel();
		}
	};
	
	/**
	 * New custom dialog, with the given parent and given title.
	 * 
	 * <p>
	 * Per default, the frame will not be resizable by the user.
	 * </p>
	 * 
	 * @param parent
	 *            the parent to use.
	 * @param title
	 *            the title to use.
	 */
    AbstractCustomDialog(JFrame parent, String title) {
		super(parent, title, ModalityType.APPLICATION_MODAL);
		setResizable(false);
		
		// Exit via escape key.
		addKeyListener(escapeListener);
		parent.addKeyListener(escapeListener);
		addKeyListener(enterListener);
		parent.addKeyListener(enterListener);
	}

	/**
	 * New custom dialog, with the given parent and given title.
	 * 
	 * <p>
	 * Per default, the frame will not be resizable by the user.
	 * </p>
	 * 
	 * @param parent
	 *            the parent to use.
	 */
    AbstractCustomDialog(JFrame parent) {
		super(parent, ModalityType.APPLICATION_MODAL);
		setResizable(false);
		
		// Exit via escape key.
		addKeyListener(escapeListener);
		parent.addKeyListener(escapeListener);
		addKeyListener(enterListener);
		parent.addKeyListener(enterListener);
	}

	/**
	 * Register a button that triggers an "ok".
	 * 
	 * @param button
	 *            the button to register.
	 */
    void registerAcceptComponent(JButton button) {
		button.addActionListener(acceptListener);
		button.addKeyListener(escapeListener);
		
	}

	/**
	 * Register a textfield that triggers an "ok".
	 * 
	 * @param textField
	 *            the textfield to register.
	 */
    void registerAcceptComponent(JTextField textField) {
		textField.addActionListener(acceptListener);
		textField.addKeyListener(escapeListener);
		
	}

	/**
	 * Register a button that triggers a "cancel".
	 * 
	 * @param button
	 *            the button to register.
	 */
    void registerCancelComponent(JButton button) {
		button.addActionListener(abortListener);
		button.addKeyListener(escapeListener);
	}
	
	/**
	 * Adds a key listener for escape and enter to the component, where enter
	 * accepts and escape aborts.
	 * 
	 * @param comp
	 *            the component to which to add the listener.
	 */
    void registerComponent(JComponent comp) {
		comp.addKeyListener(escapeListener);
		comp.addKeyListener(enterListener);
	}
	
	/**
	 * Trigger an OK.
	 */
    void accept() {
		wasAborted = false;
		setVisible(false);
	}
	
	/**
	 * Trigger a cancellation.
	 */
    void cancel() {
		setVisible(false);
	}
	
	/**
	 * Opens the dialog, allowing the user to input his data.
	 * 
	 * <p>
	 * Note that this function blocks until the user has either confirmed his
	 * input, or aborted the action. In case of a successful data entry (user
	 * clicked 'OK' or pressed enter) the function returns <code>true</code>,
	 * else (clicked on 'Cancel') it returns <code>false</code>.
	 * </p>
	 * 
	 * @return whether to continue with the given data (<code>true</code>) or
	 * not (canceled, <code>false</code>).
	 */
	public boolean open() {
		wasAborted = true;
		setLocationRelativeTo(getParent());
		setVisible(true);
		return !wasAborted;
	}
}
