package ossobook.client.gui.dialogs;

/**
 * Possible types for this dialog (determines used icons and title).
 */
public enum ProjectDialogType {
	/**
	 * Dialog used to open a project.
	 */
	OPEN,

	/**
	 * Dialog used to choose a project to delete.
	 */
	DELETE,

	/**
	 * Default, nondescript project chooser.
	 */
	DEFAULT
}
