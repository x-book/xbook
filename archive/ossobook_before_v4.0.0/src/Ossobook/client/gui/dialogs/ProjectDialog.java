package ossobook.client.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.MessageFormat;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;

import ossobook.Messages;
import ossobook.client.base.metainfo.Project;
import ossobook.client.io.database.ConnectionType;
import ossobook.client.util.Configuration;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.queries.QueryManager;

import com.jidesoft.swing.DefaultOverlayable;
import com.jidesoft.swing.Searchable;
import com.jidesoft.swing.SearchableBar;
import com.jidesoft.swing.SearchableUtils;
import com.jidesoft.swing.StyledLabelBuilder;

/**
 * Dialog used for managing and opening projects.
 * 
 * @author fnuecke
 */
public class ProjectDialog extends AbstractCustomDialog {

	private static final long		serialVersionUID	= -1722952582988161016L;

	/**
	 * The list with selectable projects.
	 */
	private final JList				projectList;
	
	/**
	 * Creates a new project management frame.
	 * 
	 * @param parent
	 *            the parent container for which to display the dialog.
     * @param manager
 *            QueryManager to use (this basically determines if this is a
 *            manager for local or for global projects).
     */
	public ProjectDialog(JFrame parent, QueryManager manager, ProjectDialogType type)
	{
		super(parent);
		
		switch (type) {
		case DELETE:
			setTitle(Messages.getString("ProjectDialog.0")); //$NON-NLS-1$
			setIconImage(new ImageIcon(Configuration.IMAGES_DIR + "edit-delete.png").getImage()); //$NON-NLS-1$
			break;
		case OPEN:
			setTitle(Messages.getString("ProjectDialog.1")); //$NON-NLS-1$
			setIconImage(new ImageIcon(Configuration.IMAGES_DIR + "document-open.png").getImage()); //$NON-NLS-1$
			break;
		default:
			setIconImage(new ImageIcon(Configuration.IMAGES_DIR + "folder.png").getImage()); //$NON-NLS-1$
			break;
		}
		
		setResizable(true);
		setPreferredSize(new Dimension(600, 600));
		setMinimumSize(new Dimension(500, 400));
		
		GridBagLayout layout = new GridBagLayout();
		
		setLayout(layout);
		
		final JLabel projectGroup = new JLabel();
		layout.setConstraints(projectGroup, makeConstraints(0, 0, GridBagConstraints.REMAINDER, 1, 1));
		if (manager.getConnectionType() == ConnectionType.CONNECTION_LOCAL) {
			projectGroup.setBorder(BorderFactory.createTitledBorder(Messages.getString("ProjectDialog.2"))); //$NON-NLS-1$
		} else {
			projectGroup.setBorder(BorderFactory.createTitledBorder(Messages.getString("ProjectDialog.3"))); //$NON-NLS-1$
		}
		projectGroup.setLayout(new BorderLayout());
		add(projectGroup);
		
		Vector<Project> projects;
		String overlayMessage;
		try {
			projects = manager.getProjects();
			overlayMessage = Messages.getString("ProjectDialog.4"); //$NON-NLS-1$
		} catch (StatementNotExecutedException e) {
			projects = new Vector<Project>();
			overlayMessage = Messages.getString("ProjectDialog.5"); //$NON-NLS-1$
		}
		projectList = new JList(projects);
		projectList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		projectList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		projectList.setVisibleRowCount(0);
		projectList.setCellRenderer(new ProjectCellRenderer());

		projectList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					accept();
				}
			}
		});
		
		DefaultOverlayable overlayList = new DefaultOverlayable(projectList,
				StyledLabelBuilder.createStyledLabel(MessageFormat.format("'{'{0}:f:gray'}'", overlayMessage)), //$NON-NLS-1$
				DefaultOverlayable.CENTER);
		overlayList.setOverlayVisible(projects.size() == 0);
		//projectGroup.add(overlayList);

		JScrollPane projectScrollPane = new JScrollPane();
		projectScrollPane.setBorder(BorderFactory.createEtchedBorder());
		projectScrollPane.setViewportView(overlayList);
		projectGroup.add(projectScrollPane);

		Searchable searchable = SearchableUtils.installSearchable(projectList);
		searchable.setRepeats(true);
		SearchableBar searchableBar = SearchableBar.install(searchable,
				KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.CTRL_DOWN_MASK),
				new SearchableBar.Installer()
		{
			@Override
			public void openSearchBar(SearchableBar searchBar) {
				projectGroup.add(searchBar, BorderLayout.AFTER_LAST_LINE);
				projectGroup.invalidate();
				projectGroup.revalidate();
			}
			@Override
			public void closeSearchBar(SearchableBar searchBar) {
				projectGroup.remove(searchBar);
				projectGroup.invalidate();
				projectGroup.revalidate();
			}
		});
		searchableBar.setVisibleButtons(searchableBar.getVisibleButtons() & ~SearchableBar.SHOW_HIGHLIGHTS);
		searchableBar.getInstaller().openSearchBar(searchableBar);
		
		JPanel spacer = new JPanel();
		layout.setConstraints(spacer, makeConstraints(0, 1, 1, 1, 0));
		add(spacer);
		
		JButton openProject = new JButton(Messages.getString("ProjectDialog.7"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "go-next.png")); //$NON-NLS-1$
		layout.setConstraints(openProject, makeConstraints(1));
		add(openProject);
		
		JButton cancel = new JButton(Messages.getString("ProjectDialog.8"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "process-stop.png")); //$NON-NLS-1$
		layout.setConstraints(cancel, makeConstraints(2));
		add(cancel);
		
		registerComponent(projectList);
		registerAcceptComponent(openProject);
		registerCancelComponent(cancel);
		
		pack();
	}
	
	/**
	 * The project selected when the "open" button was clicked.
	 * 
	 * @return the project to open.
	 */
	public Project getSelectedProject() {
		if (projectList.getSelectedIndex() < 0) {
			return null;
		} else {
			return (Project)projectList.getSelectedValue();
		}
	}

	private static GridBagConstraints makeConstraints(int x) {
		return makeConstraints(x, 1, 1, 0, 0);
	}
	
	private static GridBagConstraints makeConstraints(int x, int y, int width, float weightx, float weighty) {
		return new GridBagConstraints(x, y, width, 1, weightx, weighty,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						5, 5, 5, 5), 0, 0);
	}
}
