package ossobook.client.gui.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import ossobook.Messages;
import ossobook.client.util.Configuration;

import com.jidesoft.swing.DefaultOverlayable;
import com.jidesoft.swing.OverlayPasswordField;
import com.jidesoft.swing.OverlayTextField;
import com.jidesoft.swing.StyledLabelBuilder;

/**
 * This is the login form, used to get the username and password from the user
 * used to log in to the local and global database.
 * 
 * <p>
 * It behaves as a dialog and should be opened using the {@link #open()}
 * function. This function returns <code>true</code> if the login should be
 * attempted with the given data, or <code>false</code> if it should be aborted.
 * </p>
 * 
 * <p>
 * The given username and password can be retrieved using the two respective
 * getters of this class.
 * </p>
 * 
 * @author j.lamprecht
 * @author fnuecke
 */
public class LoginDialog extends AbstractCustomDialog {
	
	private static final long	serialVersionUID	= 1111450903899303702L;
	
	/**
	 * Text field for inputting the username.
	 */
	private final JTextField			username;
	
	/**
	 * Text field for inputting the password.
	 */
	private final JPasswordField		password;

	/**
	 * builds up Login window
	 * 
	 * @param parent the parent frame to the dialog.
	 */
	public LoginDialog(JFrame parent) {
		super(parent, Messages.getString("LoginDialog.0")); //$NON-NLS-1$
		setIconImage(new ImageIcon(Configuration.IMAGES_DIR + "system-users.png").getImage()); //$NON-NLS-1$
		
		GridBagLayout layout = new GridBagLayout();
		
		setLayout(layout);
		
		// Username input text box.
		username = new OverlayTextField(10);
		// Create tooltip and overlay.
		username.setToolTipText(Messages.getString("LoginDialog.1")); //$NON-NLS-1$
		final DefaultOverlayable overlayUsername = new DefaultOverlayable(username,
				StyledLabelBuilder.createStyledLabel(String.format("{%s:f:gray}", Messages.getString("LoginDialog.1"))), //$NON-NLS-1$ //$NON-NLS-2$
				DefaultOverlayable.CENTER);
		username.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				if (username.getDocument().getLength() == 0) {
					overlayUsername.setOverlayVisible(true);
				}
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				if (username.getDocument().getLength() > 0) {
					overlayUsername.setOverlayVisible(false);
				}
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
			}
		});
		username.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				overlayUsername.setOverlayVisible(username.getDocument().getLength() == 0);
			}
			@Override
			public void focusGained(FocusEvent e) {
				overlayUsername.setOverlayVisible(username.getDocument().getLength() == 0);
			}
		});
		// Set layout and add.
		layout.setConstraints(overlayUsername, makeConstraints(0, 0, 2));
		add(overlayUsername);
		
		// Password input textbox.
		password = new OverlayPasswordField(10);
		// Create tooltip and overlay.
		password.setToolTipText(Messages.getString("LoginDialog.2")); //$NON-NLS-1$
		final DefaultOverlayable overlayPassword = new DefaultOverlayable(password,
				StyledLabelBuilder.createStyledLabel(String.format("{%s:f:gray}", Messages.getString("LoginDialog.2"))), //$NON-NLS-1$ //$NON-NLS-2$
				DefaultOverlayable.CENTER);
		password.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				if (password.getDocument().getLength() == 0) {
					overlayPassword.setOverlayVisible(true);
				}
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				if (password.getDocument().getLength() > 0) {
					overlayPassword.setOverlayVisible(false);
				}
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
			}
		});
		password.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				overlayPassword.setOverlayVisible(password.getDocument().getLength() == 0);
			}
			@Override
			public void focusGained(FocusEvent e) {
				overlayPassword.setOverlayVisible(password.getDocument().getLength() == 0);
			}
		});
		// Set layout and add.
		layout.setConstraints(overlayPassword, makeConstraints(0, 1, 2));
		add(overlayPassword);
		
		// Buttons.
		JButton ok = new JButton(Messages.getString("LoginDialog.3"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "go-next.png")); //$NON-NLS-1$
		layout.setConstraints(ok, makeConstraints(0, 2, 1));
		add(ok);

		// login aborted
		JButton cancel = new JButton(Messages.getString("LoginDialog.4"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "process-stop.png")); //$NON-NLS-1$
		layout.setConstraints(cancel, makeConstraints(1, 2, 1));
		add(cancel);
		
		registerAcceptComponent(username);
		registerAcceptComponent(password);
		registerAcceptComponent(ok);
		registerCancelComponent(cancel);
		
		username.grabFocus();
		
		pack();
		
		// Mainly meant for debugging... should not be used on public computers
		// due to security issues (PW stored as plain text - never a good idea).
		username.setText(Configuration.config.getProperty("debug.username", "")); //$NON-NLS-1$ //$NON-NLS-2$
		password.setText(Configuration.config.getProperty("debug.password", "")); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	/**
	 * The username entered by the user.
	 * 
	 * @return the username.
	 */
	public String getUser() {
		return username.getText();
	}
	
	/**
	 * The password entered by the user.
	 * 
	 * @return the password.
	 */
	public String getPassword() {
		return String.valueOf(password.getPassword());
	}
	
	private static GridBagConstraints makeConstraints(int x, int y, int width) {
		return new GridBagConstraints(x, y, width, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(8, 8, 8, 8), 0, 0);
	}
}
