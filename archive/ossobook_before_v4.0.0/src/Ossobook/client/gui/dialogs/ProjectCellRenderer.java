package ossobook.client.gui.dialogs;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.SystemColor;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import ossobook.client.util.Configuration;

/**
 * Custom cell renderer for project selection, to make it look a little nicer...
 * 
 * @author fnuecke
 */
public class ProjectCellRenderer extends JLabel implements ListCellRenderer {
	
	private static final long	serialVersionUID	= 838887503417244611L;
	
	/**
	 * Label for the folder icon.
	 */
	private final JLabel icon = new JLabel(new ImageIcon(Configuration.IMAGES_DIR +
			"folder32.png"), JLabel.CENTER); //$NON-NLS-1$
	
	/**
	 * Label used for displaying the name of the project.
	 */
	private final JLabel text = new JLabel("", JLabel.CENTER); //$NON-NLS-1$
	
	public ProjectCellRenderer() {
		setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
		text.setOpaque(true);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		icon.setAlignmentX(Component.CENTER_ALIGNMENT);
		text.setAlignmentX(Component.CENTER_ALIGNMENT);
		text.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
		add(icon);
		add(text);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Component getListCellRendererComponent(JList list,
			Object value, int index, boolean isSelected,
			boolean cellHasFocus)
	{
		// Set new text and colorize depending on selected state.
		text.setText(value.toString());
		if (isSelected) {
			text.setBackground(SystemColor.textHighlight);
			text.setForeground(SystemColor.textHighlightText);
		} else {
			text.setBackground(SystemColor.text);
			text.setForeground(SystemColor.textText);
		}
		
		// Recalculate preferred sizes because layout doens't seem to.
		setPreferredSize(null);
		Dimension ps = getPreferredSize();
		Dimension is = icon.getPreferredSize();
		Dimension ts = text.getPreferredSize();
		setPreferredSize(new Dimension(ps.width + ts.width, ps.height + is.height + ts.height));
		
		return this;
	}
}
