package ossobook.client.gui.dialogs;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import ossobook.Messages;
import ossobook.client.io.database.ConnectionType;
import ossobook.client.util.Configuration;
import ossobook.queries.QueryManagerFactory;

/**
 * Dialog used to allow the user to select which database to use when opening a
 * project.
 * 
 * @author fnuecke
 */
public class DatabaseSelectionDialog extends AbstractCustomDialog {

	private static final long	serialVersionUID	= -3196321574742605805L;
	
	/**
	 * Radio button representing local database.
	 */
	private final JToggleButton		local;

	public DatabaseSelectionDialog(JFrame parent) {
		super(parent, Messages.getString("DatabaseSelectionDialog.0")); //$NON-NLS-1$
		setIconImage(new ImageIcon(Configuration.IMAGES_DIR + "network-server.png").getImage()); //$NON-NLS-1$

		GridBagLayout layout = new GridBagLayout();

		setLayout(layout);

		JPanel selection = new JPanel();
		selection.setBorder(BorderFactory.createTitledBorder(Messages.getString("DatabaseSelectionDialog.1"))); //$NON-NLS-1$
		layout.setConstraints(selection, makeConstraints(0, 0, 2));
		selection.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		// local select button
		local = new JToggleButton(
				Messages.getString("DatabaseSelectionDialog.2"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "folder.png")); //$NON-NLS-1$
		if (!QueryManagerFactory.isLocalAvailable()) {
			local.setEnabled(false);
		} else {
			local.setSelected(true);
		}
		selection.add(local);
		
		// global select button
		JToggleButton global = new JToggleButton(
				Messages.getString("DatabaseSelectionDialog.3"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "network-workgroup.png")); //$NON-NLS-1$
		if (!QueryManagerFactory.isGlobalAvailable()) {
			global.setEnabled(false);
		} else {
			global.setSelected(true);
		}
		selection.add(global);
		
		add(selection);
		
		// Add to group to make only one selectable.
		ButtonGroup group = new ButtonGroup();
		group.add(local);
		group.add(global);
		
		JButton ok = new JButton(Messages.getString("DatabaseSelectionDialog.4"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "go-next.png")); //$NON-NLS-1$
		layout.setConstraints(ok, makeConstraints(0, 1, 1));
		add(ok);

		JButton cancel = new JButton(Messages.getString("DatabaseSelectionDialog.5"), //$NON-NLS-1$
				new ImageIcon(Configuration.IMAGES_DIR + "process-stop.png")); //$NON-NLS-1$
		layout.setConstraints(cancel, makeConstraints(1, 1, 1));
		add(cancel);
		
		registerComponent(local);
		registerComponent(global);
		registerAcceptComponent(ok);
		registerCancelComponent(cancel);
		
		pack();
	}
	
	/**
	 * Get the type of database to connect to.
	 * 
	 * @return the type of database to connect to.
	 */
	public ConnectionType getDatabaseType() {
		if (local.isSelected()) {
			return ConnectionType.CONNECTION_LOCAL;
		} else {
			return ConnectionType.CONNECTION_GLOBAL;
		}
	}
	
	private static GridBagConstraints makeConstraints(int x, int y, int width) {
		return new GridBagConstraints(x, y, width, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						5, 5, 5, 5), 0, 0);
	}
}
