package ossobook.client.gui.update.components.window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.base.codelists.Alter1Liste;
import ossobook.client.base.codelists.BrandspurListe;
import ossobook.client.base.codelists.Bruchkanten2Liste;
import ossobook.client.base.codelists.BruchkantenListe;
import ossobook.client.base.codelists.ErhaltungListe;
import ossobook.client.base.codelists.FettListe;
import ossobook.client.base.codelists.GeschlechtsListe;
import ossobook.client.base.codelists.KnochenTeilListe;
import ossobook.client.base.codelists.PatinaListe;
import ossobook.client.base.codelists.Schlachtspur1Liste;
import ossobook.client.base.codelists.Schlachtspur2Liste;
import ossobook.client.base.codelists.SkelettteilListe;
import ossobook.client.base.codelists.TierartListe;
import ossobook.client.base.codelists.VerbissListe;
import ossobook.client.base.codelists.VersinterungListe;
import ossobook.client.base.codelists.WurzelfrassListe;
import ossobook.client.base.metainfo.Project;
import ossobook.client.base.scheme.SchemeArtefacts;
import ossobook.client.base.scheme.SchemeBones;
import ossobook.client.base.scheme.SchemeIndet;
import ossobook.client.gui.common.MainMenuBar;
import ossobook.client.gui.common.OssobookFrame;
import ossobook.client.gui.common.ProjectBaseWindow;
import ossobook.client.gui.common.ProjectManipulation;
import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturTextFeld;
import ossobook.client.gui.update.components.other.ArtefaktPanel;
import ossobook.client.gui.update.components.other.GewoehnlichesSelbstkorrekturTextFeld;
import ossobook.client.gui.update.components.other.KonkordanzPanel;
import ossobook.client.gui.update.components.other.ProjektPanel;
import ossobook.client.gui.update.components.other.UebersichtsPanel;
import ossobook.client.gui.update.elements.inputfields.Alter1KodeFeld;
import ossobook.client.gui.update.elements.inputfields.Alter2KodeFeld;
import ossobook.client.gui.update.elements.inputfields.Alter3KodeFeld;
import ossobook.client.gui.update.elements.inputfields.Alter4KodeFeld;
import ossobook.client.gui.update.elements.inputfields.Alter5KodeFeld;
import ossobook.client.gui.update.elements.inputfields.BrandspurFeld;
import ossobook.client.gui.update.elements.inputfields.Bruchkanten2Feld;
import ossobook.client.gui.update.elements.inputfields.BruchkantenFeld;
import ossobook.client.gui.update.elements.inputfields.ErhaltungFeld;
import ossobook.client.gui.update.elements.inputfields.FettFeld;
import ossobook.client.gui.update.elements.inputfields.GeschlechtsFeld;
import ossobook.client.gui.update.elements.inputfields.PatinaFeld;
import ossobook.client.gui.update.elements.inputfields.Schlachtspur1Feld;
import ossobook.client.gui.update.elements.inputfields.Schlachtspur2Feld;
import ossobook.client.gui.update.elements.inputfields.SkelKodeFeld;
import ossobook.client.gui.update.elements.inputfields.TierartKodeFeld;
import ossobook.client.gui.update.elements.inputfields.VerbissFeld;
import ossobook.client.gui.update.elements.inputfields.VersinterungFeld;
import ossobook.client.gui.update.elements.inputfields.WurzelfrassFeld;
import ossobook.client.gui.update.elements.other.MassKnopf;
import ossobook.client.gui.update.elements.other.NotizButton;
import ossobook.client.io.database.modell.BasisFeldJava2Db;
import ossobook.client.util.Configuration;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.queries.QueryManager;
import ossobook.queries.UserManager;

/**
 * The Specific Ossobook Bone Project.
 * 
 * @author ali
 */
@SuppressWarnings("serial")
public class ProjektFenster extends ProjectBaseWindow {
	private static final Log _log = LogFactory.getLog(ProjektFenster.class);
	
	private static ProjektFenster proFenster;

	// make these non-static!
	public static Alter1Liste alter1list;

	//private static Alter2Liste alter2list;

	//private static Alter3Liste alter3list;

	//private static Alter4Liste alter4list;

	//private static Alter5Liste alter5list;

	public static TierartListe animallist;

	public static BruchkantenListe bruchkantenlist;

	public static Bruchkanten2Liste bruchkanten2list;

	public static ErhaltungListe erhaltunglist;

	public static WurzelfrassListe wurzelfrasslist;

	public static SkelettteilListe skellist;

	public static KnochenTeilListe knochenteillist;

	private int[] artefactID;

	private int[] recordID;

	private int[] massID;

	private final SchemeArtefacts artefaktTemplate;

	// !
	// temporary set to private static...
	// set it back to private final if static is not necessary any more
	// !
	private static OssobookFrame mainframe;

	// ANFANG BENUTZER INFORMATIONEN
	// private int benutzerNummer;

    private final QueryManager manager;

    // ENDE BENUTZER INFORMATIONEN
	private final SchemeIndet indetTemplate;

	// Elemente von CommandPanel
	private JButton insertButton;

	private final SchemeBones knochenTemplate;

	private final JTabbedPane maintab;

	private FehlernachrichtenFenster messageFrame;

	private JPanel p1;
    private JPanel p;

    private final Project project;

	private final UebersichtsPanel ueberblickpanel;

	private final KonkordanzPanel konkordanzpanel1;

	private final KonkordanzPanel konkordanzpanel2;

	private final KonkordanzPanel konkordanzpanel3;

	private final KonkordanzPanel konkordanzpanel4;

	// Variable f\u00FCr Massinformationen
	private MassKnopf massComponente = null;

	public static VersinterungListe versinterunglist;

	public static FettListe fettlist;

	public static PatinaListe patinalist;

	public static BrandspurListe brandspurlist;

	public static VerbissListe verbisslist;

	public static Schlachtspur1Liste schlachtspur1list;

	public static Schlachtspur2Liste schlachtspur2list;

	public static GeschlechtsListe geschlechtslist;

	// ENDE ELEMENTE VON COMMANDPANEL

	public ProjektFenster(String projektname, QueryManager manager,
			SchemeIndet indetTemplate, SchemeBones knochenTemplate,
			SchemeArtefacts artefaktTemplate, Project project,
			ProjectManipulation parent, OssobookFrame mainframe,
			JDesktopPane desktop) {

		super(projektname);

		parent.addInternalFrameListener(new InternalFrameAdapter() {
			public void internalFrameClosing(InternalFrameEvent arg0) {
				messageFrame.doDefaultCloseAction();
			}
		});
		// Feldzuweisungen
		indetTemplate.setParent(this);
		knochenTemplate.setParent(this);
		artefaktTemplate.setParent(this);
		this.indetTemplate = indetTemplate;
		this.knochenTemplate = knochenTemplate;
		this.artefaktTemplate = artefaktTemplate;
		this.manager = manager;
		this.project = project;
		this.mainframe = mainframe;
		messageFrame = new FehlernachrichtenFenster(projektname,
				(int) mainframe.getSize().getWidth()
						- Configuration.projektbasisfensterBorderRight
		);
		// produziere CodeListen
		ProjektFenster.animallist = new TierartListe(manager.getConnection());
		ProjektFenster.skellist = new SkelettteilListe(manager.getConnection());
		ProjektFenster.knochenteillist = new KnochenTeilListe(manager
				.getConnection());
		ProjektFenster.alter1list = new Alter1Liste(manager.getConnection());
		//ProjektFenster.alter2list = new Alter2Liste(manager.getConnection());
		//ProjektFenster.alter3list = new Alter3Liste(manager.getConnection());
		//ProjektFenster.alter4list = new Alter4Liste(manager.getConnection());
		//ProjektFenster.alter5list = new Alter5Liste(manager.getConnection());
		ProjektFenster.bruchkantenlist = new BruchkantenListe(manager
				.getConnection());
		ProjektFenster.bruchkanten2list = new Bruchkanten2Liste(manager
				.getConnection());
		ProjektFenster.erhaltunglist = new ErhaltungListe(manager
				.getConnection());
		ProjektFenster.wurzelfrasslist = new WurzelfrassListe(manager
				.getConnection());
		ProjektFenster.versinterunglist = new VersinterungListe(manager
				.getConnection());
		ProjektFenster.fettlist = new FettListe(manager.getConnection());
		ProjektFenster.patinalist = new PatinaListe(manager.getConnection());
		ProjektFenster.brandspurlist = new BrandspurListe(manager
				.getConnection());
		ProjektFenster.verbisslist = new VerbissListe(manager.getConnection());
		ProjektFenster.schlachtspur1list = new Schlachtspur1Liste(manager
				.getConnection());
		ProjektFenster.schlachtspur2list = new Schlachtspur2Liste(manager
				.getConnection());
		ProjektFenster.geschlechtslist = new GeschlechtsListe(manager
				.getConnection());

		// Montage der Tabs
		maintab = new JTabbedPane();
		maintab.addTab(Messages.getString("ProjektFenster.0"), produziereFelderHauptmaske()); //$NON-NLS-1$
        JPanel artefaktpanel = produziereFelderArtefaktmaske();
		maintab.addTab(Messages.getString("ProjektFenster.1"), artefaktpanel); //$NON-NLS-1$
		ueberblickpanel = produziereUeberblick();
		maintab.addTab(Messages.getString("ProjektFenster.2"), ueberblickpanel); //$NON-NLS-1$
		maintab.addTab(Messages.getString("ProjektFenster.3"), new ProjektPanel(manager, project,  //$NON-NLS-1$
				mainframe));
		konkordanzpanel1 = new KonkordanzPanel(manager, project,
				"phase1"); //$NON-NLS-1$
		konkordanzpanel2 = new KonkordanzPanel(manager, project,
				"phase2"); //$NON-NLS-1$
		konkordanzpanel3 = new KonkordanzPanel(manager, project,
				"phase3"); //$NON-NLS-1$
		konkordanzpanel4 = new KonkordanzPanel(manager, project,
				"phase4"); //$NON-NLS-1$
		maintab.addTab(Messages.getString("ProjektFenster.8"), konkordanzpanel1); //$NON-NLS-1$
		maintab.addTab(Messages.getString("ProjektFenster.9"), konkordanzpanel2); //$NON-NLS-1$
		maintab.addTab(Messages.getString("ProjektFenster.10"), konkordanzpanel3); //$NON-NLS-1$
		maintab.addTab(Messages.getString("ProjektFenster.11"), konkordanzpanel4); //$NON-NLS-1$
		maintab.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JTabbedPane activePane = (JTabbedPane) e.getSource();
				int selectionIndex = activePane.getSelectedIndex();
				if (selectionIndex == 2) {
					if (_log.isDebugEnabled()) {
						_log.debug("ueberblick selected."); //$NON-NLS-1$
					}
					updateUeberblick();
				} else if (selectionIndex == 3) {
					if (_log.isDebugEnabled()) {
						_log.debug("projekt selected."); //$NON-NLS-1$
					}

				} else if (selectionIndex == 4) {
					if (_log.isDebugEnabled()) {
						_log.debug("Konkordanz 1 selected."); //$NON-NLS-1$
					}
					updateKonkordanz1();
				} else if (selectionIndex == 5) {
					if (_log.isDebugEnabled()) {
						_log.debug("Konkordanz 2  selected."); //$NON-NLS-1$
					}
					updateKonkordanz2();
				} else if (selectionIndex == 6) {
					if (_log.isDebugEnabled()) {
						_log.debug("Konkordanz 3  selected."); //$NON-NLS-1$
					}
					updateKonkordanz3();
				} else if (selectionIndex == 7) {
					if (_log.isDebugEnabled()) {
						_log.debug("Konkordanz 4  selected."); //$NON-NLS-1$
					}
					updateKonkordanz4();
				}
			}
		});
		setContentPane(maintab);
		desktop.add(messageFrame);
		// prob fokus
		fokussiereErstesFeld();
		proFenster = this;
	}

	/**
	 * @return JPanel which Insert, update or Delete Commands
	 */
	private JPanel createCommandPanel() {
		JPanel result = new JPanel();
		insertButton = new JButton(Messages.getString("ProjektFenster.18")); //$NON-NLS-1$
		insertButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (_log.isTraceEnabled()) {
					_log.trace("starting insert."); //$NON-NLS-1$
				}
				double time = System.currentTimeMillis();
				insertValues();
				fokussiereErstesFeld();
				time = System.currentTimeMillis() - time;
				if (_log.isTraceEnabled()) {
					_log.trace(this.getClass().getName()
							+ ": time used for insert: " + time); //$NON-NLS-1$
				}
			}

		});

        JButton deleteButton = new JButton(Messages.getString("ProjektFenster.21"));
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteValues(); //$NON-NLS-1$
				emptyMask();
				// prob fokus
				fokussiereErstesFeld();
			}

		});

        JButton emptyMaskButton = new JButton(Messages.getString("ProjektFenster.23"));
		emptyMaskButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				emptyMask();
				fokussiereErstesFeld();
			}

		});

		result.add(insertButton);
		result.add(deleteButton);
		result.add(emptyMaskButton);
		// result.add(searchButton);

		try {
			// user may not write to the project
			if (!manager.getIsAdmin()
					&& !(manager.getRight(project) == UserManager.WRITE)) { //$NON-NLS-1$
				deleteButton.setEnabled(false);
				insertButton.setEnabled(false);
			}
		} catch (StatementNotExecutedException e) {
			LogFactory.getLog(ProjektFenster.class).error(e, e);
		}

		return result;
	}

	/**
	 * 
	 */
	// ist delete
	private void deleteValues() {
		insertButton.setText(Messages.getString("ProjektFenster.18")); //$NON-NLS-1$
		try {
			manager.deleteFromEingabeeinheit(recordID);
			manager.deleteFromArtefaktmaske(artefactID);
			manager.deleteFromMasse(massID);
		} catch (StatementNotExecutedException e) {
			LogFactory.getLog(ProjektFenster.class).error(e, e);
		}

		knochenTemplate.loadActiveTemplate();
		artefaktTemplate.loadActiveTemplate();
		updateUeberblick();
	}

	private void emptyMask() {
		loescheHauptmaske();

		// Suche ArtefaktPanel
		JPanel jpa = getArtefaktPanel();

		// L\u00F6sche ArtefaktPanel
		loescheArtefaktPanel(jpa);
	}

	private void fokussiereErstesFeld() {
		JPanel tab = getEingabePanel();
		JPanel ueberPanel = (JPanel) tab.getComponent(0);
		JPanel firstPanel = (JPanel) ueberPanel.getComponent(0);

		int x = firstPanel.getComponentCount();
		for (int i = 0; i < x; i++) {
			JPanel ptemp = (JPanel) firstPanel.getComponent(i);
			try {
				AbstraktesSelbstkorrekturTextFeld ctemp0 = (AbstraktesSelbstkorrekturTextFeld) ptemp
						.getComponent(0);
				JCheckBox ctemp1 = (JCheckBox) ptemp.getComponent(1);
				if (!(ctemp1.isSelected())) {
					ctemp0.grabFocus();
					break;
				}
			} catch (Exception e) {
				LogFactory.getLog(ProjektFenster.class).error(e, e);
			}
		}
	}

	private void loescheHauptmaske() {
		for (int j = 0; j < p.getComponentCount(); j++) {
			p1 = (JPanel) p.getComponent(j);
			for (int i = 0; i < p1.getComponentCount(); i++) {

				JPanel p = (JPanel) p1.getComponent(i);

				JComponent c = (JComponent) p.getComponent(0);
				boolean loeschen = !((JCheckBox) p.getComponent(1))
						.isSelected();
				if (loeschen) {
					if (c instanceof MassKnopf) {
						((MassKnopf) c).reset();
					} else if (c instanceof AbstraktesSelbstkorrekturTextFeld) {
						((AbstraktesSelbstkorrekturTextFeld) c).reset();
					} else if (c instanceof NotizButton) {
						((NotizButton) c).reset();
					} else if (c instanceof JCheckBox) {
						((JCheckBox) c).setSelected(false);
//					} else if (c instanceof TierartKodeFeld) {
//						((TierartKodeFeld) c).clearTextField();
					} else if (c instanceof JComboBox) {
						((JComboBox) c).setSelectedIndex(0);
					}

				}

			}

		}
		messageFrame.updateMessageFrame(""); //$NON-NLS-1$

	}

	private static void loescheArtefaktPanel(JPanel jpa) {
		JPanel jp = (JPanel) jpa.getComponent(0);
		for (int i = 0; i < jp.getComponentCount(); i++) {
			JPanel p = (JPanel) jp.getComponent(i);
			JComponent c = (JComponent) p.getComponent(0);
			boolean loeschen = !((JCheckBox) p.getComponent(1)).isSelected();
			if ((c.getClass().getSuperclass().getName()
					.equals("ossobook.framework.elements.AbstractValidatedTextField")) //$NON-NLS-1$
					&& (loeschen)) {
				AbstraktesSelbstkorrekturTextFeld ctext = (AbstraktesSelbstkorrekturTextFeld) c;
				ctext.reset();
			}
		}
	}

	private JPanel getArtefaktPanel() {
		JPanel jpa = null;
		for (int i = 0; i < maintab.getComponentCount(); i++) {
			jpa = (JPanel) maintab.getComponent(i);
			try {
				if (jpa.getToolTipText().equals(Messages.getString("ProjektFenster.1"))) { //$NON-NLS-1$
					break;
				}
			} catch (Exception e) {
				LogFactory.getLog(ProjektFenster.class).error(e, e);
			}
		}
		return jpa;
	}

	private JPanel getEingabePanel() {
		JPanel jpa = null;
		for (int i = 0; i < maintab.getComponentCount(); i++) {
			jpa = (JPanel) maintab.getComponent(i);
			try {
				System.out.println(jpa.getToolTipText());
				if (jpa.getToolTipText().equals(Messages.getString("ProjektFenster.0"))) { //$NON-NLS-1$
					System.out.println("Test");
					break;
				}
			} catch (Exception e) {
				LogFactory.getLog(ProjektFenster.class).error(e, e);
			}
		}
		return jpa;
	}

	public int[] getMassID() {
		return massID;
	}

	/**
	 * @return Returns the connection.
	 */
	/*
	 * public Connection getConnection() { return connection; }
	 */

	public QueryManager getManager() {
		return manager;
	}

	/**
	 * 
	 */
	private JComponent getFieldByDbAequivalent(String a) {
		for (int j = 0; j < p.getComponentCount(); j++) {
			p1 = (JPanel) p.getComponent(j);
			for (int i = 0; i < p1.getComponentCount(); i++) {
				JPanel p = (JPanel) p1.getComponent(i);
				JComponent c = (JComponent) p.getComponent(0);
				if (c.getToolTipText().equals(a)) {
					return c;
				}
			}

		}
		return null;
	}

	/**
	 * @param name
	 * @return
	 */
	private JComponent getFieldByDbAequivalentArtefakt(String name) {
		JPanel jpa = null;
		for (int i = 0; i < maintab.getComponentCount(); i++) {
			jpa = (JPanel) maintab.getComponent(i);
			try {
				if (jpa.getToolTipText().equals(Messages.getString("ProjektFenster.1"))) { //$NON-NLS-1$
					break;
				}
			} catch (Exception e) {
				LogFactory.getLog(ProjektFenster.class).error(e, e);
			}
		}
		JPanel parte = (JPanel) jpa.getComponent(0);
		for (int i = 0; i < parte.getComponentCount(); i++) {
			JPanel p = (JPanel) parte.getComponent(i);
			JComponent c = (JComponent) p.getComponent(0);
			if (c.getToolTipText().equals(name)) {
				return c;
			}
		}
		return null;
	}

	/**
	 * @return
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * @return
	 */
	public int getSkelCode()  {
		AbstraktesSelbstkorrekturTextFeld c;// = new GewoehnlichesSelbstkorrekturTextFeld();
		c = (AbstraktesSelbstkorrekturTextFeld) getFieldByDbAequivalent("skelettteil"); //$NON-NLS-1$
		return Integer.parseInt(c.toString());
	}

	/**
	 * @return
	 */
	public int getTierCode()  {
		AbstraktesSelbstkorrekturTextFeld c = (AbstraktesSelbstkorrekturTextFeld) getFieldByDbAequivalent("tierart"); //$NON-NLS-1$
		return Integer.parseInt(c.toString());
	}

	/**
	 * insert the Values if the validate_Form() method returns true insert and
	 * update substituted by update
	 */
	private void insertValues() {

		// Check ob Artefaktinformationen vorhanden sind

		// zugriff auf Artefaktpanel
		JPanel jpa = getArtefaktPanel();

		// Zugriff auf contentPane Panel von Artefakt
		JPanel jp = (JPanel) jpa.getComponent(0);

		// UPDATE statement variabeln
		Vector<String> fields = new Vector<String>();
		Vector<String> fieldsA = new Vector<String>();
		Vector<String> fieldsMasse = new Vector<String>();
		Vector<String> values = new Vector<String>();
		Vector<String> valuesA = new Vector<String>();
		Vector<String> valuesMasse = new Vector<String>();

		String phase1 = "''"; //$NON-NLS-1$
		String phase2 = "''"; //$NON-NLS-1$
		String phase3 = "''"; //$NON-NLS-1$
		String phase4 = "''"; //$NON-NLS-1$

		// Alle Artefakt eingabekomponenten durchsuchen und fieldsA bzw. valuesA
		// auff\u00FCllen
		for (int i = 0; i < jp.getComponentCount(); i++) {
			JPanel p = (JPanel) jp.getComponent(i);
			JComponent c = (JComponent) p.getComponent(0);
			if (!c.toString().equals("")) { //$NON-NLS-1$
				fieldsA.add(c.getToolTipText());
				valuesA.add(c.toString());
			}
		}
		// Fehlerpr\u00FCfung Hauptmaske
		String fehler = ""; //$NON-NLS-1$
		boolean check = true;

		for (int j = 0; j < p.getComponentCount(); j++) {
			p1 = (JPanel) p.getComponent(j);
			for (int i = 0; i < p1.getComponentCount(); i++) {

				JPanel p = (JPanel) p1.getComponent(i);
				// Auslesen der Werte f\u00FCr Hauptmaske
				JComponent c = (JComponent) p.getComponent(0);
				// ausgeschlossen MassID -> Sonderbehandlung

				if (!c.getToolTipText().equals("massID")) { //$NON-NLS-1$
					fields.add(c.getToolTipText());
					values.add(c.toString());
				} else {
					// Hole Massinfo
					massComponente = (MassKnopf) c;
					JComponent[] cmasse = new JComponent[0];
					try {

						cmasse = massComponente.getMasse();
					} catch (Exception e) {
						LogFactory.getLog(ProjektFenster.class).error(e, e);
					}

					// generiere MassEingabe
					// try {
					for (JComponent element : cmasse) {
						GewoehnlichesSelbstkorrekturTextFeld mass = (GewoehnlichesSelbstkorrekturTextFeld) element;
						fieldsMasse.add(mass.getToolTipText());
						String inhalt = mass.toString();
						if (inhalt.equals("")) { //$NON-NLS-1$
							inhalt = "0"; //$NON-NLS-1$
						}
						valuesMasse.add(inhalt);
					}

					/*
					 * fieldsMasse = fieldsMasse.substring(0, fieldsMasse
					 * .lastIndexOf(",")); valuesMasse =
					 * valuesMasse.substring(0, valuesMasse .lastIndexOf(","));
					 */
					// } catch (Exception e) {
					// }
				}
				if (c instanceof AbstraktesSelbstkorrekturTextFeld) {
					AbstraktesSelbstkorrekturTextFeld ctext = (AbstraktesSelbstkorrekturTextFeld) c;
					if (!ctext.check()) {
						check = false;// globale variable
						// informieren
						fehler = fehler + "- " + c.getToolTipText() + "\n"; //$NON-NLS-1$ //$NON-NLS-2$

					}
				}
				if (c instanceof TierartKodeFeld) {
					TierartKodeFeld ctext = (TierartKodeFeld) c;
					if (!ctext.check()) {
						check = false;// globale variable
						// informieren
						fehler = fehler + "- " + c.getToolTipText() + "\n"; //$NON-NLS-1$ //$NON-NLS-2$

					}
				}
				// REMOVE }
			}
		}
		// Eintragen falls kein Fehler
		if (check) {
			// EINTRAGEN MASSE
			// String idvalueMasse = "";
			// if (this.insertButton.getText().equals("\u00E4ndern")) {
			// deleteValues();
			// idvorspannArte="ID,";
			// idvalueArte=this.aktuelleArtefaktDatensatzID+",";
			// }
			int[] massID = { -1, -1 };
			// Falls Masse masse vorhanden ABER nicht schon eingetragen (knopf:
			// rot)

			boolean zeroOnly = true;
            for (String aValuesMasse : valuesMasse) {
                float value = Float.parseFloat(aValuesMasse);
                if (value > 0) {
                    zeroOnly = false;
                }
            }

			if (!valuesMasse.equals("") //$NON-NLS-1$
					&& !(massComponente.getBackground() == Color.RED)
					&& !zeroOnly) {
				try {
					if (!insertButton.getText().equals(Messages.getString("ProjektFenster.18"))) { //$NON-NLS-1$
						massID = manager.getMassId(recordID);
						if (massID[0] != -1) {
							if (_log.isDebugEnabled()) {
								_log
										.debug("update Masse for massID: " //$NON-NLS-1$
												+ Arrays.toString(massID));
							}
							manager.updateMasse(fieldsMasse, massID,
									valuesMasse);
						} else if (fieldsMasse.size() > 0) {
							massID = manager.insertIntoMasse(fieldsMasse,
									valuesMasse);
							if (_log.isDebugEnabled()) {
								_log.debug("insert Masse: " + Arrays.toString(massID)); //$NON-NLS-1$
							}
						}
					} else if (fieldsMasse.size() > 0) {
						massID = manager.insertIntoMasse(fieldsMasse,
								valuesMasse);
					}
				} catch (StatementNotExecutedException e) {
					LogFactory.getLog(ProjektFenster.class).error(e, e);
				}
				// Falls Knopf Rot DatensatzID \u00FCbernehmen
			} else if ((massComponente.getBackground() == Color.RED)) {
				massID = this.massID;
			}
			// EINTRAGEN ARTEFAKTE
			// if (this.insertButton.getText().equals("\u00E4ndern")) {
			// deleteValues();
			// idvorspannArte="ID,";
			// idvalueArte=this.aktuelleArtefaktDatensatzID+",";
			// }

			// falls Artefaktinfos vorhanden Artefakteintrag erstellen
			int[] ArtefaktID = { -1, -1 };
			/*
			 * if (!valuesA.equals("")) { fieldsA = fieldsA.substring(0,
			 * fieldsA.lastIndexOf(",")); valuesA = valuesA.substring(0,
			 * valuesA.lastIndexOf(","));
			 */

			try {
				if (!insertButton.getText().equals(Messages.getString("ProjektFenster.18"))) { //$NON-NLS-1$
					ArtefaktID = manager.getArtefaktId(recordID);
					if (ArtefaktID[0] != -1) {
						manager.updateArtefaktmaske(fieldsA, ArtefaktID,
								valuesA);
					} else if (fieldsA.size() > 0) {
						ArtefaktID = manager.insertIntoArtefaktmaske(fieldsA,
								valuesA);
					}
				} else if (fieldsA.size() > 0) {
					ArtefaktID = manager.insertIntoArtefaktmaske(fieldsA,
							valuesA);
				}
			} catch (StatementNotExecutedException e) {
				LogFactory.getLog(ProjektFenster.class).error(e, e);
			}
			// Falls es sich um eine \u00C4nderung handelt wird die ID
			// mitgegeben
			// String idvorspann = "";
			// String idvalue = "";
			if (insertButton.getText().equals(Messages.getString("ProjektFenster.50"))) { //$NON-NLS-1$
				try {
					phase1 = "'" + manager.getPhaseValue("phase1", recordID) //$NON-NLS-1$ //$NON-NLS-2$
							+ "'"; //$NON-NLS-1$
					phase2 = "'" + manager.getPhaseValue("phase2", recordID) //$NON-NLS-1$ //$NON-NLS-2$
							+ "'"; //$NON-NLS-1$
					phase3 = "'" + manager.getPhaseValue("phase3", recordID) //$NON-NLS-1$ //$NON-NLS-2$
							+ "'"; //$NON-NLS-1$
					phase4 = "'" + manager.getPhaseValue("phase4", recordID) //$NON-NLS-1$ //$NON-NLS-2$
							+ "'"; //$NON-NLS-1$
				} catch (StatementNotExecutedException e) {
					LogFactory.getLog(ProjektFenster.class).error(e, e);
				}
				// idvorspann = "ID";
				// idvalue = this.recordID+"";

			}

			// EINGABE DER HAUPTMASKE
			try {
				if (!insertButton.getText().equals(Messages.getString("ProjektFenster.18"))) { //$NON-NLS-1$
					manager.updateEingabeeinheit(recordID, project, fields,
							ArtefaktID, massID, phase1, phase2, phase3, phase4,
							values);
				} else {
					manager.insertIntoEingabeeinheit(project, fields,
							ArtefaktID, massID, phase1, phase2, phase3, phase4,
							values);
				}
			} catch (StatementNotExecutedException e) {
				LogFactory.getLog(ProjektFenster.class).error(e, e);
			}

			messageFrame.updateMessageFrame(""); //$NON-NLS-1$
			try {
				MainMenuBar mbar = mainframe.getMainMenu();
				mbar.updateDurchsucheAlleFrame();
			} catch (Exception e) {
				LogFactory.getLog(ProjektFenster.class).error(e, e);
			}
			emptyMask();
			artefactID = new int[2];
			artefactID[0] = -1;
			artefactID[1] = -1;
			recordID = new int[2];
			recordID[0] = -1;
			recordID[1] = -1;
			this.massID = new int[2];
			massID[0] = -1;
			massID[1] = -1;
			if (insertButton.getText().equals(Messages.getString("ProjektFenster.50"))) { //$NON-NLS-1$
				knochenTemplate.loadActiveTemplate();
				artefaktTemplate.loadActiveTemplate();
			}

		} else {
			messageFrame.updateMessageFrame(Messages.getString("ProjektFenster.66") + fehler); //$NON-NLS-1$
		}
	}

	/**
	 * 
	 */
	private void updateKonkordanz1() {
		konkordanzpanel1.updateInfo();
	}

	private void updateKonkordanz2() {
		konkordanzpanel2.updateInfo();
	}

	private void updateKonkordanz3() {
		konkordanzpanel3.updateInfo();
	}

	private void updateKonkordanz4() {
		konkordanzpanel4.updateInfo();
	}

	/**
	 * @return
	 */
	private JPanel produziereFelderArtefaktmaske() {

		return new ArtefaktPanel(artefaktTemplate);
	}

	/**
	 * @return JPanel Main Mask for ProjectFrame. The Content of this Frame.
	 */
	private JPanel produziereFelderHauptmaske() {
        JPanel pcontent = new JPanel();
		pcontent.setLayout(new BorderLayout());
		p = new JPanel();
		p.setLayout(new GridLayout(2, 2));
		p1 = new JPanel();
		p1.setBorder(new TitledBorder(Messages.getString("ProjektFenster.67"))); //$NON-NLS-1$
        JPanel p2 = new JPanel();
		p2.setBorder(new TitledBorder(Messages.getString("ProjektFenster.68"))); //$NON-NLS-1$
        JPanel p3 = new JPanel();
		p3.setBorder(new TitledBorder(Messages.getString("ProjektFenster.69"))); //$NON-NLS-1$
        JPanel p4 = new JPanel();
		p4.setBorder(new TitledBorder(Messages.getString("ProjektFenster.70"))); //$NON-NLS-1$
		// Einf\u00FCgen der Felder auf die Panels nach Sektonsnummern sortiert
		BasisFeldJava2Db[] f = knochenTemplate.getFields();
		for (BasisFeldJava2Db element : f) {
			if (element.getCheckBoxSwitch().isSelected()) {
				String mem = element.getDbAequivalent();
				String s = element.getCheckBoxSwitch().getText();
				JComponent inputComponent = element.getEingabeKomponente();
				inputComponent.setToolTipText(mem);

				JPanel inputComponentUnit = new JPanel();

				inputComponentUnit.setLayout(new BoxLayout(inputComponentUnit,
						BoxLayout.X_AXIS));

				TitledBorder border = new TitledBorder(s);
				// NOTE Hier wird die Titelposition pro Feld ver\u00E4ndert!
				// //border.setTitlePosition(TitledBorder.LEFT);
				inputComponentUnit.setBorder(border);
				inputComponentUnit.add(inputComponent);
				JCheckBox b = new JCheckBox();// verantwortlich f\u00FCr das
				// Nichtl\u00F6schen der Werte
				b.setBorderPainted(false);
				b.setFocusable(false);
				inputComponentUnit.add(b);

				if (element.getSectionNr() == 1) {
					p1.add(inputComponentUnit);
				}
				if (element.getSectionNr() == 2) {
					p2.add(inputComponentUnit);
				}
				if (element.getSectionNr() == 3) {
					p3.add(inputComponentUnit);
				}
				if (element.getSectionNr() == 4) {
					p4.add(inputComponentUnit);
				}
			}
		}
		// Formatiere gem\u00E4ss Anzahl Felder
		int zeilen = 4;
		int count1 = p1.getComponentCount();
		p1.setLayout(new GridLayout(zeilen, count1 / zeilen));
		int count2 = p2.getComponentCount();
		p2.setLayout(new GridLayout(zeilen, count2 / zeilen));
		int count3 = p3.getComponentCount();
		p3.setLayout(new GridLayout(zeilen, count3 / zeilen));
		int count4 = p4.getComponentCount();
		p4.setLayout(new GridLayout(zeilen, count4 / zeilen));
		// Ende Formatieren
		p.add(p1);
		p.add(p2);
		p.add(p3);
		p.add(p4);
		pcontent.add(p, BorderLayout.CENTER);
        JPanel commandPanel = createCommandPanel();
		pcontent.add(commandPanel, BorderLayout.SOUTH);
		pcontent.setToolTipText(Messages.getString("ProjektFenster.0")); //$NON-NLS-1$
		return pcontent;
	}

	/**
	 * @return
	 */
	private UebersichtsPanel produziereUeberblick() {
		return new UebersichtsPanel(manager, project);
	}

	/**
	 * 
	 */
	public void searchValues(int[] recordKey) {
		emptyMask();
		knochenTemplate.loadMaxTemplate();
		artefaktTemplate.loadMaxTemplate();
		insertButton.setText(Messages.getString("ProjektFenster.50")); //$NON-NLS-1$
		recordID = recordKey;
		// Massbutton
		MassKnopf massbutton = null;

		try {
			// gew\u00FCnschtes Element Ermitteln
			// gew\u00FCnschtes Element in Maske einf\u00FCgen
			// Lese Informationen aus ROW
			String[][] rsEingabe = manager.getDataRecord(recordKey);
			artefactID = new int[2];
			artefactID[0] = -1;
			artefactID[1] = -1;
			massID = new int[2];
			massID[0] = -1;
			massID[1] = -1;

			for (String[] element : rsEingabe) {
				String cName = element[0];
				String cWert = element[1];

				// finde passendes Feld
				JComponent c = getFieldByDbAequivalent(cName);

				if (c instanceof AbstraktesSelbstkorrekturTextFeld) {
					((AbstraktesSelbstkorrekturTextFeld) c).setText(cWert);
				} else if (c instanceof NotizButton) {
					NotizButton nbt = (NotizButton) c;
					if (!cWert.equals("")) { //$NON-NLS-1$
						nbt.setBackground(Color.RED);
					} else {
						nbt.setBackground(SystemColor.control);
					}
					nbt.setNotizFeldText(cWert);
				} else if (c instanceof JCheckBox) {
					((JCheckBox) c).setSelected(cWert.equals(Messages.getString("ProjektFenster.74"))); //$NON-NLS-1$
				} else if (c instanceof JComboBox) {
					((JComboBox) c).setSelectedItem(cWert);
				} else if (c instanceof MassKnopf) {
					massbutton = (MassKnopf) c;
				}
				
				// Test ob Artefakt vorhanden
				if (cName.equals("artefaktID")) { //$NON-NLS-1$
					if (!cWert.equals("-1")) { //$NON-NLS-1$
						artefactID[0] = Integer.parseInt(cWert);
					}
				}
				if (cName.equals("DBNummerArtefakt")) { //$NON-NLS-1$
					if (!cWert.equals("-1")) { //$NON-NLS-1$
						artefactID[1] = Integer.parseInt(cWert);
					}
				}
				// Test ob Masse vorhanden
				if (cName.equals("massID")) { //$NON-NLS-1$
					if (!cWert.equals("-1")) { //$NON-NLS-1$
						massID[0] = Integer.parseInt(cWert);
					}
				}
				if (cName.equals("DBNummerMasse")) { //$NON-NLS-1$
					if (!cWert.equals("-1")) { //$NON-NLS-1$
						massID[1] = Integer.parseInt(cWert);
					}
				}
			}
			// Mass Prozedur
			if (massbutton != null) {
				if (massID[0] >= 0) {
					massbutton.setBackground(Color.RED);
				} else {
					massbutton.setBackground(SystemColor.control);
				}
			}
			// Artefakt Prozedur
			if (artefactID[0] >= 0) {

				// Lese Informationen aus ROW
				String result[][] = manager.getArtefaktmaske(artefactID);
				for (int i = 1; i <= result.length; i++) {
					String cName = result[i - 1][0];
					String cWert = result[i - 1][1];
					// finde passendes Feld
					JComponent c = getFieldByDbAequivalentArtefakt(cName);
					try {
						AbstraktesSelbstkorrekturTextFeld abs = (AbstraktesSelbstkorrekturTextFeld) c;
						abs.setText(cWert);
					} catch (Exception e) {
						LogFactory.getLog(ProjektFenster.class).error(e, e);
					}
					try {
						NotizButton nbt = (NotizButton) c;
						if (!cWert.equals("")) { //$NON-NLS-1$
							nbt.setBackground(Color.RED);
						} else {
							nbt.setBackground(SystemColor.control);
						}
						nbt.setNotizFeldText(cWert);
					} catch (Exception e) {
						LogFactory.getLog(ProjektFenster.class).error(e, e);
					}
					try {
						JCheckBox chb = (JCheckBox) c;
						if (cWert.equals(Messages.getString("ProjektFenster.74"))) { //$NON-NLS-1$
							chb.setSelected(true);
						}
					} catch (Exception e) {
						LogFactory.getLog(ProjektFenster.class).error(e, e);
					}
					try {
						JComboBox cbx = (JComboBox) c;
						cbx.setSelectedItem(cWert);
					} catch (Exception e) {
						LogFactory.getLog(ProjektFenster.class).error(e, e);
					}
				}
			}
			insertButton.setText(Messages.getString("ProjektFenster.50")); //$NON-NLS-1$
			// Focus auf erstes TAB
			maintab.setSelectedIndex(0);
		} catch (StatementNotExecutedException e) {
			/*
			 * JOptionPane.showMessageDialog(this, "Konnte Aktion nicht
			 * ausf\u00FChren!"); e.printStackTrace();
			 */
		}
	}

	/**
	 * @return display the ArtefactTemplate for this Project
	 */
	public SchemeArtefacts showArtefaktTemplate() {
		artefaktTemplate.setVisible(true);
		try {
			artefaktTemplate.setSelected(true);
		} catch (PropertyVetoException ignored) {

		}
		return artefaktTemplate;
	}

	/**
	 * @return display the indetTemplate for this Project
	 */
	public SchemeIndet showIndetTemplate() {
		indetTemplate.setVisible(true);
		return indetTemplate;
	}

	/**
	 * @return display the BoneTemplate for this Project
	 */
	public SchemeBones showKnochenTemplate() {
		knochenTemplate.setVisible(true);
		try {
			knochenTemplate.setSelected(true);
		} catch (PropertyVetoException ignored) {

		}
		return knochenTemplate;
	}

	public String toString() {
		return "ProjektFenster";
	}

	/**
	 * apply a Template
	 */
	public void updateInputScreen() {
		maintab.setComponentAt(0, produziereFelderHauptmaske());
		maintab.setComponentAt(1, produziereFelderArtefaktmaske());
	}

	/**
	 * 
	 */
	private void updateUeberblick() {
		double time = System.currentTimeMillis();
		ueberblickpanel.updateInfo();
		time = System.currentTimeMillis() - time;
		if (_log.isTraceEnabled()) {
			_log.trace("Time used for updating update-tab: " + time); //$NON-NLS-1$
		}
	}

	/**
	 * @param i
	 */
	// NOTE so werden methoden f\u00FCr nachschlagefelder eingebaut
	public void setTierArtField(int i) {
		TierartKodeFeld tf = (TierartKodeFeld) getFieldByDbAequivalent("tierart"); //$NON-NLS-1$
		tf.setText("" + i); //$NON-NLS-1$
	}

	public void setSkelCodeField(int i) {
		SkelKodeFeld sf = (SkelKodeFeld) getFieldByDbAequivalent("skelettteil"); //$NON-NLS-1$
		sf.setText("" + i); //$NON-NLS-1$
	}

	public void setBruchkanteCodeField(int i) {
		BruchkantenFeld bf = (BruchkantenFeld) getFieldByDbAequivalent("bruchkante"); //$NON-NLS-1$
		bf.setText("" + i); //$NON-NLS-1$
	}

	public void setErhaltungCodeField(int i) {
		ErhaltungFeld ef = (ErhaltungFeld) getFieldByDbAequivalent("erhaltung"); //$NON-NLS-1$
		ef.setText("" + i); //$NON-NLS-1$
	}

	/**
	 * @param i
	 */
	public void setAlter1CodeField(int i) {
		Alter1KodeFeld a1cf = (Alter1KodeFeld) getFieldByDbAequivalent("alter1"); //$NON-NLS-1$
		a1cf.setText("" + i); //$NON-NLS-1$
	}

	/**
	 * @param i
	 */
	public void setAlter2CodeField(int i) {
		Alter2KodeFeld a2cf = (Alter2KodeFeld) getFieldByDbAequivalent("alter2"); //$NON-NLS-1$
		a2cf.setText("" + i); //$NON-NLS-1$
	}

	/**
	 * @param i
	 */
	public void setAlter3CodeField(int i) {
		Alter3KodeFeld a3cf = (Alter3KodeFeld) getFieldByDbAequivalent("alter3"); //$NON-NLS-1$
		a3cf.setText("" + i); //$NON-NLS-1$
	}

	/**
	 * @param i
	 */
	public void setAlter4CodeField(int i) {
		Alter4KodeFeld a4cf = (Alter4KodeFeld) getFieldByDbAequivalent("alter4"); //$NON-NLS-1$
		a4cf.setText("" + i); //$NON-NLS-1$
	}

	/**
	 * @param i
	 */
	public void setAlter5CodeField(int i) {
		Alter5KodeFeld a5cf = (Alter5KodeFeld) getFieldByDbAequivalent("alter5"); //$NON-NLS-1$
		a5cf.setText("" + i); //$NON-NLS-1$
	}

	public void setWurzelfrassField(int i) {
		WurzelfrassFeld wf = (WurzelfrassFeld) getFieldByDbAequivalent("wurzelfrass"); //$NON-NLS-1$
		wf.setText("" + i); //$NON-NLS-1$
	}

	public void setVersinterungField(int i) {
		VersinterungFeld vf = (VersinterungFeld) getFieldByDbAequivalent("kruste"); //$NON-NLS-1$
		vf.setText("" + i); //$NON-NLS-1$
	}

	public void setFettField(int i) {
		FettFeld ff = (FettFeld) getFieldByDbAequivalent("fettig"); //$NON-NLS-1$
		ff.setText("" + i); //$NON-NLS-1$
	}

	public void setPatinaField(int i) {
		PatinaFeld pf = (PatinaFeld) getFieldByDbAequivalent("patina"); //$NON-NLS-1$
		pf.setText("" + i); //$NON-NLS-1$
	}

	public void setBrandspurField(int i) {
		BrandspurFeld bf = (BrandspurFeld) getFieldByDbAequivalent("brandspur"); //$NON-NLS-1$
		bf.setText("" + i); //$NON-NLS-1$
	}

	public void setVerbissField(int i) {
		VerbissFeld vf = (VerbissFeld) getFieldByDbAequivalent("verbiss"); //$NON-NLS-1$
		vf.setText("" + i); //$NON-NLS-1$
	}

	public void setSchlachtS1Field(int i) {
		Schlachtspur1Feld s1f = (Schlachtspur1Feld) getFieldByDbAequivalent("schlachtS1"); //$NON-NLS-1$
		s1f.setText("" + i); //$NON-NLS-1$
	}

	public void setSchlachtS2Field(int i) {
		Schlachtspur2Feld s2f = (Schlachtspur2Feld) getFieldByDbAequivalent("schlachtS2"); //$NON-NLS-1$
		s2f.setText("" + i); //$NON-NLS-1$
	}

	public void setGeschlechtsField(int i) {
		GeschlechtsFeld gf = (GeschlechtsFeld) getFieldByDbAequivalent("geschlecht"); //$NON-NLS-1$
		gf.setText("" + i); //$NON-NLS-1$
	}

	public void setBruchkanten2Field(int i) {
		Bruchkanten2Feld bf = (Bruchkanten2Feld) getFieldByDbAequivalent("bruchkante2"); //$NON-NLS-1$
		bf.setText("" + i); //$NON-NLS-1$
	}

	public JTabbedPane getUpdateWindow() {
		return maintab;
	}
	
	public static OssobookFrame getMainFrame() {
		return mainframe;
	}
	
	public static ProjektFenster getProjectFenster() {
		return proFenster;
	}
	
}