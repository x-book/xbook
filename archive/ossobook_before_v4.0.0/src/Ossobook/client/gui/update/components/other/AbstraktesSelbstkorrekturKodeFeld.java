/*
 * SkelCodeField.java
 *
 * Created on 24. M\u00E4rz 2003, 16:42
 */
package ossobook.client.gui.update.components.other;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.border.TitledBorder;

import ossobook.client.util.Configuration;

/**
 * @author ali
 */
public abstract class AbstraktesSelbstkorrekturKodeFeld extends AbstraktesSelbstkorrekturTextFeld {
	
	private static final long serialVersionUID = 1L;
	protected TitledBorder titledBorder = new TitledBorder(""); //$NON-NLS-1$;

	protected AbstraktesSelbstkorrekturKodeFeld() {
		super();
		settings();
	}

	private void settings() {
		Font f = new Font("CodeTitelFont", Font.PLAIN, Configuration.codeTitelFont); //$NON-NLS-1$
		Font cf = new Font("CodeFont", Font.PLAIN, Configuration.codeFont); //$NON-NLS-1$
		// NOTE hier wird Titelposition f\u00FCr Codefelder festgelegt
		// titledBorder.setTitlePosition(TitledBorder.LEFT);
		// NOTE hier wird TitelSchrift f\u00FCr Codefelder festgelegt //
		titledBorder.setTitleFont(f);
		// NOTE hier wird die Feldgr\u00F6sse festgelegt
		setMargin(new Insets(0, 0, 0, 0));
		setFont(cf);
		setPreferredSize(new Dimension(Configuration.selbstkorrekturfeld_x, Configuration.selbstkorrekturfeld_y));
		setBorder(titledBorder);
	}

	@Override
	public void reset() {
		setText(""); //$NON-NLS-1$
		titledBorder.setTitle(""); //$NON-NLS-1$
	}
}
