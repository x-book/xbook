/*
 * Created on 23.07.2004
 */
package ossobook.client.gui.update.components.other;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import ossobook.Messages;
import ossobook.client.base.scheme.SchemeArtefacts;
import ossobook.client.io.database.modell.BasisFeldJava2Db;

/**
 * @author ali
 */
@SuppressWarnings("serial")
public class ArtefaktPanel extends JPanel {
	private final SchemeArtefacts template;

	public ArtefaktPanel(SchemeArtefacts template) {
		this.template = template;
		setToolTipText(Messages.getString("ArtefaktPanel.0")); //$NON-NLS-1$
		createFields();
	}

	private void createFields() {
		setLayout(new BorderLayout());

		// Einf\u00FCgen der Felder auf die Panels nach Sektonsnummern
		// sortiert
		JPanel p1=new JPanel();
		BasisFeldJava2Db[] f = template.getFields();
		for (BasisFeldJava2Db element : f) {
			if (element.getCheckBoxSwitch().isSelected()) {

				String mem = element.getDbAequivalent();
				String s = element.getCheckBoxSwitch().getText();
				JComponent t = element.getEingabeKomponente();
				t.setToolTipText(mem);
				JPanel pint = new JPanel();

				pint.setBorder(new TitledBorder(s));
				pint.add(t);

				JCheckBox b = new JCheckBox();// verantwortlich f\u00FCr das
				// Nichtl\u00F6schen der Werte
				b.setFocusable(false);
				pint.add(b);
				p1.add(pint);

			}
		}
		// formatiere
		int zeilen = 9; // konstante zeilenzahl: vorsicht Einschr\u00E4nkung!
		int count1 = p1.getComponentCount();
		p1.setLayout(new GridLayout(zeilen, count1 / zeilen));
		add(p1);
	}

}