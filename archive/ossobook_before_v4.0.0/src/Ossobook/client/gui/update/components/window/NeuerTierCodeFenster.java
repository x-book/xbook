/*
 * Created on 09.06.2001
 */
package ossobook.client.gui.update.components.window;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import ossobook.Messages;
import ossobook.client.gui.update.components.other.GewoehnlichesSelbstkorrekturTextFeld;
import ossobook.client.util.Configuration;
import ossobook.queries.QueryManager;

/**
 * @author ali
 * 
 */
@SuppressWarnings("serial")
public class NeuerTierCodeFenster extends JInternalFrame {

	private final QueryManager manager;
    private GewoehnlichesSelbstkorrekturTextFeld TierCode, TierName, DTier,
			TierFolgeAuswertung, LB;
	private JLabel message;

	public NeuerTierCodeFenster(QueryManager manager) {
		super(Messages.getString("NeuerTierCodeFenster.0")); //$NON-NLS-1$
		this.manager = manager;
		settings();
        JPanel maske = generiereMaske();
		setContentPane(maske);
	}

	private void settings() {
		this.setSize(Configuration.projektveraenderungsfenster_x,
				Configuration.projektveraenderungsfenster_y);
		setVisible(true);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setResizable(true);
		setMaximizable(true);
		setIconifiable(true);
		setClosable(true);
		setVisible(true);
	}

	/**
	 * 
	 */
	private JPanel generiereMaske() {
		JPanel result = new JPanel();

		// formatierung
		result.setLayout(new GridLayout(6, 2));
		result.add(new JLabel(Messages.getString("NeuerTierCodeFenster.1"))); //$NON-NLS-1$
		TierCode = new GewoehnlichesSelbstkorrekturTextFeld();
		result.add(TierCode);

		result.add(new JLabel(Messages.getString("NeuerTierCodeFenster.2"))); //$NON-NLS-1$
		TierName = new GewoehnlichesSelbstkorrekturTextFeld();
		result.add(TierName);

		result.add(new JLabel(Messages.getString("NeuerTierCodeFenster.3"))); //$NON-NLS-1$
		DTier = new GewoehnlichesSelbstkorrekturTextFeld();
		result.add(DTier);

		result.add(new JLabel(Messages.getString("NeuerTierCodeFenster.4"))); //$NON-NLS-1$
		TierFolgeAuswertung = new GewoehnlichesSelbstkorrekturTextFeld("1"); //$NON-NLS-1$
		result.add(TierFolgeAuswertung);

		result.add(new JLabel(Messages.getString("NeuerTierCodeFenster.5"))); //$NON-NLS-1$
		LB = new GewoehnlichesSelbstkorrekturTextFeld("0"); //$NON-NLS-1$
		result.add(LB);

		JButton UpdateButton = new JButton(Messages.getString("NeuerTierCodeFenster.8")); //$NON-NLS-1$
		UpdateButton.setBackground(Color.YELLOW);
		UpdateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InsertAction();
			}
		});
		result.add(UpdateButton);
		message = new JLabel(Messages.getString("NeuerTierCodeFenster.9")); //$NON-NLS-1$
		result.add(message);
		return result;
	}

	/**
	 * 
	 */
	private void InsertAction() {
		// FELD Eintragen
		try {
			manager.updateTierart(TierCode.getText(), TierName.getText(), DTier
					.getText(), TierFolgeAuswertung.getText(), LB.getText());
			dispose();
		} catch (Exception e) {
			message.setText(e.getMessage());
		}

	}
}
