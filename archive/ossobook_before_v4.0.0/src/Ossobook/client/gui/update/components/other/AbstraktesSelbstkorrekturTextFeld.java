/*
 * validatedTextField.java
 *
 * Created on 21. Februar 2003, 15:30
 */
package ossobook.client.gui.update.components.other;

import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 * 
 * @author ali
 */
public abstract class AbstraktesSelbstkorrekturTextFeld extends
		GewoehnlichesTextFeld {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean correct = false; // state

	/** Creates a new instance of validatedTextField */
    protected AbstraktesSelbstkorrekturTextFeld() {
		super();
		addFocusListener(new FocusAdapter() { // on lostFocus : check
			@Override
			public void focusLost(FocusEvent evt) {
				setBackground(SystemColor.text);
				checkValue();

			}

		});
		addFocusListener(new FocusAdapter() { // on lostFocus : check
			@Override
			public void focusGained(FocusEvent evt) {
				setBackground(Color.cyan);
				// Wenn Feld Aktiviert wird .. automatisch Text Markieren
				setSelectionStart(0);
				setSelectionEnd(getText().length());
			}
		});

	}

	/**
	 * @param s
	 */
    protected AbstraktesSelbstkorrekturTextFeld(String s) {
		super(s);
		addFocusListener(new FocusAdapter() { // on lostFocus : check
			@Override
			public void focusLost(FocusEvent evt) {
				setBackground(SystemColor.text);
				checkValue();

			}

		});
		addFocusListener(new FocusAdapter() { // on lostFocus : check
			@Override
			public void focusGained(FocusEvent evt) {
				setBackground(Color.cyan);

			}
		});
	}

	// to be used by other classes
	public boolean check() {
		checkValue();
		return correct;
	}

	@Override
	public void setText(String s) {
		super.setText(s);
		check();
	}

	// use condition to determine if true or false
	private void checkValue() {
		if (condition()) {
			correct();
		} else {
			notCorrect();
		}
	}

	// Conditions to be implemented
	protected abstract boolean condition();

	// proceede if correct
	private void correct() {
		correct = true;
		setForeground(Color.black);
	}

	// procede if false
	private void notCorrect() { // If check fails
		correct = false;
		setForeground(Color.red);
	}

	/**
	 * 
	 */
	public void reset() {
		setText(""); //$NON-NLS-1$
		setBackground(SystemColor.text);
	}

	// Use rollback or not

}