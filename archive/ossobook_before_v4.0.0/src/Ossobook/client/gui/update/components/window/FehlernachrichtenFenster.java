package ossobook.client.gui.update.components.window;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;

public class FehlernachrichtenFenster extends JInternalFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextPane p;

	public FehlernachrichtenFenster(String title, int posx) {
		super(title);
		settings(posx, 0);
		setContentPane(constructContent());
	}

	private void settings(int x, int y) {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setFocusable(false);
		setEnabled(false);
		this.setSize(200, 200);
		this.setLocation(x, y);
		setResizable(true);
		setVisible(true);
	}

	private JScrollPane constructContent() {
		p = new JTextPane();
		p.setEditable(false);
		p.setFocusable(false);
		return new JScrollPane(p);

	}

	public void updateMessageFrame(String message) {
		p.setText(message);
	}

}
