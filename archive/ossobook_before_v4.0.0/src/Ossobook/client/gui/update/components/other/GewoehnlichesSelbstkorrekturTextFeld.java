/*
 * Created on 19.07.2004
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.gui.update.components.other;

/**
 * @author ali Window - Preferences - Java - Code Style - Code Templates
 */
public class GewoehnlichesSelbstkorrekturTextFeld extends
		AbstraktesSelbstkorrekturTextFeld {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param string
	 */
	public GewoehnlichesSelbstkorrekturTextFeld(String string) {
		setText(string);
	}

	/**
     * 
     */
	public GewoehnlichesSelbstkorrekturTextFeld() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ossobook.framework.elements.AbstractValidatedTextField#condition()
	 */
	@Override
	public boolean condition() {
		return true;
	}

}
