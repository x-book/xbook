/*
 * Created on 15.06.2001
 */
package ossobook.client.gui.update.components.other;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.base.metainfo.Project;
import ossobook.client.gui.common.OssobookFrame;
import ossobook.client.gui.update.components.window.ProjektVeraenderungsFenster;
import ossobook.client.io.database.dbinfos.ProjektInformationen;
import ossobook.queries.QueryManager;

/**
 * @author ali
 * 
 */
public class ProjektPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Log _log = LogFactory.getLog(ProjektPanel.class);
    private final QueryManager manager;
	private final Project project;
	private final ProjektPanel thispanel;
	private final OssobookFrame mainframe;

	// private JPanel thispanel;

	public ProjektPanel(QueryManager manager, Project project, OssobookFrame mainframe) {
		this.manager = manager;
		this.project = project;
		thispanel = this;
		this.mainframe = mainframe;
		layouteinstellung();
		updateInfo();

	}

	/**
     * 
     */
	private void layouteinstellung() {
		setLayout(new GridLayout(1, 1));
	}

	public void updateInfo() {
		try {
			removeAll();
		} catch (Exception ignored) {
		}
		// Projektinformationen
        JTextArea projektInfo = new JTextArea();
		String query = "SELECT * FROM projekt WHERE ProjNr=" + project.getNumber() //$NON-NLS-1$
				+ " " + "AND Datenbanknummer=" + project.getDatabaseNumber() //$NON-NLS-1$ //$NON-NLS-2$
				+ " AND geloescht='N';"; //$NON-NLS-1$
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query); //$NON-NLS-1$
		}
		ProjektInformationen pinfo = new ProjektInformationen(manager
				.getConnection(), query,
				Messages.getString("ProjektPanel.5")); //$NON-NLS-1$
		projektInfo.setText(pinfo.getProjektInfo());
		projektInfo.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				ProjektVeraenderungsFenster f = new ProjektVeraenderungsFenster(
						manager, project, mainframe, thispanel);
				mainframe.add(f);
				try {
					f.setSelected(true);
				} catch (PropertyVetoException ignored) {

				}
			}
		});
		JScrollPane sonstigeInfosSP = new JScrollPane(projektInfo);
		add(sonstigeInfosSP);
	}

}
