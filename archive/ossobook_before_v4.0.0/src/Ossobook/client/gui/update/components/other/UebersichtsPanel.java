/*
 * Created on 23.07.2004
 */
package ossobook.client.gui.update.components.other;

import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.base.metainfo.Project;
import ossobook.client.io.database.dbinfos.AnzahlArtefakteUebersicht;
import ossobook.client.io.database.dbinfos.AnzahlKnochenUebersicht;
import ossobook.client.io.database.dbinfos.FKUebersicht;
import ossobook.client.io.database.dbinfos.TierartenUebersicht;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.queries.QueryManager;

/**
 * @author ali
 */
public class UebersichtsPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Log _log = LogFactory.getLog(UebersichtsPanel.class);
    private final Project project;
	private final QueryManager manager;

	public UebersichtsPanel(QueryManager manager, Project project) {
		setToolTipText(Messages.getString("UebersichtsPanel.0")); //$NON-NLS-1$
		this.manager = manager;
		this.project = project;
		setLayout(new GridLayout(2, 3));
		createFields();
	}

	private void createFields() {
		updateInfo();
	}

	/**
	 * only if this tab is selected.
	 */
	public void updateInfo() {
		if (_log.isInfoEnabled()) {
			_log.info("updating stats"); //$NON-NLS-1$
		}
		try {
			removeAll();
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Problem in function updateInfo()"); //$NON-NLS-1$
				e.printStackTrace();
			}
		}
        JTextArea fundkomplexe = new JTextArea();
        JTextArea anzahlKnochen = new JTextArea();
        JTextArea tierartenAnzahl = new JTextArea();

		String s = ""; //$NON-NLS-1$
		String query; //$NON-NLS-1$
		String header; //$NON-NLS-1$

		// Welche Fundkomplexe sind schon angefangen
		header = Messages.getString("UebersichtsPanel.6"); //$NON-NLS-1$
		query = "SELECT SUM(anzahl) AS Summe, fK FROM eingabeeinheit WHERE projNr='" //$NON-NLS-1$
				+ project.getNumber()
				+ "' AND DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND geloescht='N' GROUP BY fK;"; //$NON-NLS-1$
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query); //$NON-NLS-1$
		}
		FKUebersicht fkuebersicht = new FKUebersicht(manager.getConnection(),
				query, header);
		fundkomplexe.setText(fkuebersicht.getProjektInfo());
		JScrollPane fundkomplexeSP = new JScrollPane(fundkomplexe);

		// Anzahl Aufgenommene Knochen
		header = Messages.getString("UebersichtsPanel.11"); //$NON-NLS-1$
		query = "SELECT SUM(Anzahl) as SUMAnzahl FROM eingabeeinheit WHERE projNr='" //$NON-NLS-1$
				+ project.getNumber()
				+ "' AND DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND geloescht='N';"; //$NON-NLS-1$
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query); //$NON-NLS-1$
		}

		AnzahlKnochenUebersicht aKnochen = new AnzahlKnochenUebersicht(manager
				.getConnection(), query, header);
		anzahlKnochen.setText(aKnochen.getProjektInfo());
		JScrollPane anzahlKnochenSP = new JScrollPane(anzahlKnochen);

		// Tierarten Anzahl
		header = Messages.getString("UebersichtsPanel.16"); //$NON-NLS-1$
		query = "SELECT TierName, SUM(anzahl) AS Summe FROM eingabeeinheit, tierart WHERE projNr='" //$NON-NLS-1$
				+ project.getNumber()
				+ "' AND DBNummerProjekt='" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ "' AND tierart.TierCode=eingabeeinheit.tierart AND eingabeeinheit.geloescht='N' " //$NON-NLS-1$
				+ "GROUP BY TierName ORDER BY Summe DESC;"; //$NON-NLS-1$
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query); //$NON-NLS-1$
		}

		TierartenUebersicht tuebersicht = new TierartenUebersicht(manager
				.getConnection(), query, header);
		tierartenAnzahl.setText(tuebersicht.getProjektInfo());
		JScrollPane tierartenSP = new JScrollPane(tierartenAnzahl);

		// Anzahl Aufgenommene Artefakte
		header = Messages.getString("UebersichtsPanel.22"); //$NON-NLS-1$
		query = "SELECT COUNT(artefaktmaske.ID) as IDAnzahl FROM artefaktmaske,eingabeeinheit WHERE eingabeeinheit.projNr='" //$NON-NLS-1$
				+ project.getNumber()
				+ "' AND eingabeeinheit.DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND artefaktmaske.ID=eingabeeinheit.artefaktID" //$NON-NLS-1$
				+ " AND artefaktmaske.Datenbanknummer=eingabeeinheit.Datenbanknummer" //$NON-NLS-1$
				+ " AND artefaktmaske.geloescht='N' AND eingabeeinheit.geloescht='N';"; //$NON-NLS-1$
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query); //$NON-NLS-1$
		}

		AnzahlArtefakteUebersicht anzahlArtefakte = new AnzahlArtefakteUebersicht(
				manager.getConnection(), query, header);
		anzahlKnochen.setText(anzahlKnochen.getText() + "\n" //$NON-NLS-1$
				+ anzahlArtefakte.getProjektInfo());

		try {
			// Sonstige informationen: gewichtsdurchschnitt
			// Wieviele Knochen?
			int anzahl = manager.getBonesNumber(project);
			// gesamtgewicht?
			double gewicht = manager.getOverallWeight(project);

			s = s + Messages.getString("UebersichtsPanel.30"); //$NON-NLS-1$
			s = s + (gewicht / anzahl);
			anzahlKnochen.setText(anzahlKnochen.getText() + "\n" + s); //$NON-NLS-1$
		} catch (StatementNotExecutedException e) {
			LogFactory.getLog(UebersichtsPanel.class).error(e, e);
		}
		// Montage
		this.add(fundkomplexeSP);
		this.add(tierartenSP);
		this.add(anzahlKnochenSP);
	}
}