/*
 * CommonTextField.java
 * 
 * Created on 24. M\u00E4rz 2003, 16:40
 */
package ossobook.client.gui.update.components.other;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;

import ossobook.client.util.Configuration;

/**
 * @author ali
 */
public class GewoehnlichesTextFeld extends javax.swing.JTextField {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates a new instance of CommonTextField */
    GewoehnlichesTextFeld() {
		super();
		settings();
	}

	private void settings() {
		setMargin(new Insets(0, 0, 0, 0));
		setPreferredSize(new Dimension(
				Configuration.gewoehnlichestextfeld_x,
				Configuration.gewoehnlichestextfeld_y));
		Font cf = new Font("CodeFont", Font.PLAIN, //$NON-NLS-1$
				Configuration.codeFont);
		setFont(cf);
	}

	/**
	 * @param s
	 */
    GewoehnlichesTextFeld(String s) {
		super(s);
		settings();
	}

	@Override
	public String toString() {
		return getText();
	}
}