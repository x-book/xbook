/*
 * Created on 22.07.2004
 *

 */
package ossobook.client.gui.update.components.window;

import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import ossobook.Messages;
import ossobook.client.gui.common.ProjectBaseWindow;
import ossobook.client.gui.update.components.other.GewoehnlichesSelbstkorrekturTextFeld;
import ossobook.client.util.Configuration;

/**
 * @author ali
 *
 */
public class MassFenster extends ProjectBaseWindow {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private JComponent[] felder;

	public MassFenster() {
		super(Messages.getString("MassFenster.0")); //$NON-NLS-1$
		setVisible(false);
		setClosable(false);
		setSize(Configuration.massfenster_x,
				Configuration.massfenster_y);
	}

	public JComponent[] getMassFields() {
		return felder;
	}

	public void konstruiereMassEingabe(
			GewoehnlichesSelbstkorrekturTextFeld[] felder, String[] bezeichner,
			String[] werte) {
        JPanel p = new JPanel();
		int x = felder.length / 2;
		p.setLayout(new GridLayout(x, 2));
		JPanel temppanel;
		this.felder = felder;
		for (int i = 0; i < felder.length; i++) {
			temppanel = new JPanel();
			GewoehnlichesSelbstkorrekturTextFeld feld = felder[i];
			feld = addListener(feld);
			if (werte.length > 0) {
				feld.setText(werte[i]);
			}
			temppanel.add(feld);
			temppanel.setBorder(new TitledBorder(bezeichner[i]));
			p.add(temppanel);
		}

		JScrollPane sp = new JScrollPane(p);
		setContentPane(sp);
	}

	private static GewoehnlichesSelbstkorrekturTextFeld addListener(
			final GewoehnlichesSelbstkorrekturTextFeld feld) {
		feld.addKeyListener(new KeyListener() {

			public void keyTyped(KeyEvent arg0) {

			}

			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					feld.transferFocus();
				}
			}

			public void keyReleased(KeyEvent arg0) {

			}

		});
		return feld;
	}

}