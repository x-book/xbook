package ossobook.client.gui.update.elements.other;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComponent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.gui.common.OssobookFrame;
import ossobook.client.gui.common.ProjectManipulation;
import ossobook.client.gui.update.components.other.GewoehnlichesSelbstkorrekturTextFeld;
import ossobook.client.gui.update.components.window.MassFenster;
import ossobook.client.gui.update.components.window.ProjektFenster;
import ossobook.client.gui.update.elements.inputfields.SkelKodeFeld;
import ossobook.client.gui.update.elements.inputfields.TierartKodeFeld;
import ossobook.exceptions.NoSuchMeasureCombinationException;
import ossobook.exceptions.StatementNotExecutedException;

//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
/**
 * This Button opens the MassScreen
 * 
 * @author ali
 */
@SuppressWarnings("serial")
public class MassKnopf extends JButton {
	private static final Log _log = LogFactory.getLog(MassKnopf.class);
	private final OssobookFrame mainframe;

	private MassFenster massscreen;

	public MassKnopf(OssobookFrame mainFrame, SkelKodeFeld skel,
			TierartKodeFeld tier) {
		super(Messages.getString("MassKnopf.0")); //$NON-NLS-1$
		mainframe = mainFrame;
		massscreen = null;
		skel.setMassKnopf(this);
		tier.setMassKnopf(this);
		addListeners();
	}

	/**
	 * The actions performed after event are defined here.
	 */
	private void addListeners() {
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					massButtonAction();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}

		});
	}

	public JComponent[] getMasse() {
		if (massscreen != null) {
			return massscreen.getMassFields();
		} else {
			return new JComponent[]{};
		}
	}

	private void resetMassScreen() {
		if (massscreen != null) {
			massscreen.setVisible(false);
			massscreen = null;
		}
	}

	@SuppressWarnings("unchecked")
	private void massButtonAction() throws Exception {
		//setBackground(null);
		ProjectManipulation window = (ProjectManipulation) mainframe.getDesktop().getSelectedFrame();
		ProjektFenster boneframe = window.getProjectWindow();
		try {
			if (massscreen == null) {
				massscreen = new MassFenster();
				// Connection c = boneframe.getConnection();
				int tierlabel;
				int skellabel;
				// HOLE die Eingabe
				int skelcode = -1;
				try {
					skelcode = boneframe.getSkelCode();
				} catch (Exception e2) {
					if (_log.isErrorEnabled()) {
						_log.error("Error in Function skelcode = boneframe.getSkelCode()"); //$NON-NLS-1$
						e2.printStackTrace();
					}
				}
				int tiercode = -1;
				try {
					tiercode = boneframe.getTierCode();
				} catch (Exception e3) {
					if (_log.isErrorEnabled()) {
						_log.error("Error in Function tiercode = boneframe.getTierCode()"); //$NON-NLS-1$
						e3.printStackTrace();
					}
				}
				// hole tierlabel
				tierlabel = boneframe.getManager().getTierlabel(tiercode);
				// hole skellabel
				skellabel = boneframe.getManager().getSkelLB(skelcode);

				// Hole Masse
				String result; //$NON-NLS-1$

				try {
					result = boneframe.getManager().getMasse(tierlabel,
							skellabel);
				} catch (NoSuchMeasureCombinationException e) {
					throw new Exception();
				}
				//  exception for no entry for tierlabel, skellabel
				// combination

				StringTokenizer st = new StringTokenizer(result);
				Vector vmasse = new Vector();
				Vector vmassname = new Vector();
				Vector vgemessen = new Vector();
				while (st.hasMoreTokens()) {
					GewoehnlichesSelbstkorrekturTextFeld c1 = new GewoehnlichesSelbstkorrekturTextFeld();
					String s = st.nextToken(";"); //$NON-NLS-1$
					c1.setToolTipText(s);
					vmassname.add(boneframe.getManager().holeMassName(s));
					int[] massID = boneframe.getMassID();
					if (massID != null) {
						vgemessen.add(boneframe.getManager().holeMassWert(s, massID));
					}
					vmasse.add(c1);
				}
				// masse Vector -> Array
				GewoehnlichesSelbstkorrekturTextFeld[] carr = new GewoehnlichesSelbstkorrekturTextFeld[vmasse
						.size()];
				for (int i = 0; i < vmasse.size(); i++) {
					carr[i] = (GewoehnlichesSelbstkorrekturTextFeld) vmasse.get(i);
				}
				// massNamen Vector -> Array
				String[] namenarr = new String[vmassname.size()];
				for (int i = 0; i < vmassname.size(); i++) {
					namenarr[i] = (String) vmassname.get(i);
				}
				// massNamen Vector -> Array
				String[] wertearr = new String[vgemessen.size()];
				for (int i = 0; i < vgemessen.size(); i++) {
					wertearr[i] = (String) vgemessen.get(i);
				}

				massscreen.konstruiereMassEingabe(carr, namenarr, wertearr);
				mainframe.getDesktop().add(massscreen);
			}// END IF !Massscreen is visible
			massscreen.setVisible(!massscreen.isVisible());

		} catch (StatementNotExecutedException e1) {
			// ex.printStackTrace();
		}
	}

	public void reset() {
		//setBackground(null);
		resetMassScreen();
		// this.massscreen.reset();
	}
}