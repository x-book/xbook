package ossobook.client.gui.update.elements.inputfields.boneparts;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import ossobook.Messages;
import ossobook.client.util.Configuration;

public class LongBonePanel extends JPanel implements MouseListener, AncestorListener {
	
	private static final long	serialVersionUID	= -6908131364898272761L;
	
	private final JTextField display;
    private BufferedImage image;
    
    private int cellX = -1;
    private int cellY = -1;
    private Rectangle rectangle;
    
    public LongBonePanel(JTextField display) {
        addAncestorListener(this);
        this.display = display;
        try {
            image = ImageIO.read(new File(Configuration.IMAGES_DIR + "ui.bonepartwindow.longbone.png")); //$NON-NLS-1$
            setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
            String[] offsets = Configuration.config.getProperty("ui.bonepartwindow.longbone.offsets", "").split(","); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            try {
                rectangle = new Rectangle(Integer.parseInt(offsets[0]),
                        Integer.parseInt(offsets[1]),
                        Integer.parseInt(offsets[2]),
                        Integer.parseInt(offsets[3]));
                addMouseListener(this);
            } catch (Exception e) {
                System.out.println(Messages.getString("LongBonePanel.4")); //$NON-NLS-1$
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);
        if (cellX >= 0 && cellY >= 0 && cellX < 17 && cellY < 4) {
            g.setColor(new Color(0, 0, 1, 0.25f));
            g.fillRect(rectangle.x + cellX * rectangle.width / 17,
                    rectangle.y + cellY * rectangle.height / 4,
                    rectangle.width / 17,
                    rectangle.height / 4);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int x = e.getX() - rectangle.x;
        int y = e.getY() - rectangle.y;
        if (x > 0 && y > 0 && x < rectangle.width && y < rectangle.height) {
            cellX = x * 17 / rectangle.width;
            cellY = y * 4 / rectangle.height;
            display.setText(String.valueOf(cellX + 1 + 20 * cellY));
        }
        repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        try {
            int currVal = Integer.parseInt(display.getText());
            if (currVal > 0) {
                cellX = Math.min(16, currVal % 20 - 1);
                cellY = Math.min(3, currVal / 20);
            }
        } catch (Exception ignored) {
        }
        repaint();
    }

    @Override
    public void ancestorMoved(AncestorEvent event) {
    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {
    }
}
