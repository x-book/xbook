package ossobook.client.gui.update.elements.inputfields;

import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.gui.update.elements.inputfields.boneparts.LongBonePanel;
import ossobook.client.util.Configuration;

public class KnochenTeilKodeWindow extends JFrame {

	private static final long	serialVersionUID	= -3299163142356040204L;

	/**
     * Possible modes.
     */
    private enum Mode {
        LONG_BONE
    }

    /**
     * Mapping skeleton codes to the modes they result in.
     */
    private final HashMap<Integer, Mode> modeMapping = new HashMap<Integer, Mode>();
    
    /**
     * Mapping modes to their used panels.
     */
    private final HashMap<Mode, JPanel> panelMapping = new HashMap<Mode, JPanel>();
    
    /**
     * Currently displayed panel (to hide again).
     */
    private JPanel currentPanel;

    public KnochenTeilKodeWindow(JTextField field) {
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);

        // Parse IDs for which to show what input mask / image.
        parse(Mode.LONG_BONE, "ui.bonepartwindow.longbone"); //$NON-NLS-1$

        // Create panels for the different types in Mode.
        panelMapping.put(Mode.LONG_BONE, new LongBonePanel(field));
    }

    /**
     * Parses a setting from the program configuration as a comma separated list
     * to retrieve the codes used to trigger the display of the given mode.
     * 
     * @param config
     *            the name of the setting in the configuration file.
     * @param mode
     *            the mode which to display for the IDs in this setting.
     */
    private void parse(Mode mode, String config) {
        for (String code : Configuration.config.getProperty(config, "").split(",")) { //$NON-NLS-1$ //$NON-NLS-2$
            try {
                int codeNumeric = Integer.parseInt(code);
                modeMapping.put(codeNumeric, mode);
            } catch (Exception e) {
				LogFactory.getLog(KnochenTeilKodeWindow.class).warn(e, e);
            }
        }
    }

    /**
     * Displays this window and shows the appropriate input mask based on the id
     * given. The given id must be the one currently set in the skeleton code
     * field.
     * 
     * <p>
     * This window will only open if the code is known. If it isn't, an
     * exception will be thrown.
     * </p>
     * 
     * @param code
     *            the current code set in the skeleton code field.
     * @throws Exception
     *             if the code is unknown.
     */
    public void display(int code) throws Exception {
        Mode mode = modeMapping.get(code);
        if (mode == null) {
            setVisible(false);
            throw new Exception(Messages.getString("KnochenTeilKodeWindow.3")); //$NON-NLS-1$
        }
        if (currentPanel != null) {
            remove(currentPanel);
        }
        currentPanel = panelMapping.get(mode);
        add(currentPanel);
        pack();
        setVisible(true);
    }
}
