/*
 * Created on 19.07.2004
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.gui.update.elements.inputfields;

import java.awt.Color;
import java.awt.SystemColor;

import org.apache.commons.logging.LogFactory;

import ossobook.client.base.metainfo.Project;
import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturTextFeld;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.queries.QueryManager;

/**
 * @author ali Window - Preferences - Java - Code Style - Code Templates
 */
public class FundKomplexFeld extends AbstraktesSelbstkorrekturTextFeld {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final QueryManager manager;
	private final Project project;

	public FundKomplexFeld(QueryManager manager, Project project) {
		this.project = project;
		this.manager = manager;
	}

	@Override
	public boolean condition() {
		if (!getText().equals("")) {
			try {
				if (manager.getCountFk(getText(), project) > 0) {
					setBackground(Color.YELLOW);
				} else {
					setBackground(SystemColor.text);
				}
				return true;
			} catch (StatementNotExecutedException e) {
				LogFactory.getLog(FundKomplexFeld.class).error(e, e);
				setBackground(SystemColor.text);
			}
		}
		return false;
	}
}
