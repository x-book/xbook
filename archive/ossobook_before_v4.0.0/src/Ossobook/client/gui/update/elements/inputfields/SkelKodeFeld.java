/*
 * SkelCodeField.java
 *
 * Created on 24. M\u00E4rz 2003, 16:42
 */
package ossobook.client.gui.update.elements.inputfields;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import org.apache.commons.logging.LogFactory;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;
import ossobook.client.gui.update.components.window.ProjektFenster;
import ossobook.client.gui.update.elements.other.MassKnopf;

/**
 * @author ali
 */
public class SkelKodeFeld extends AbstraktesSelbstkorrekturKodeFeld {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private KnochenTeilKodeFeld knochenteilfield;

	private MassKnopf massknopf;
	
	/** Creates a new instance of SkelCodeField */
	public SkelKodeFeld() {
		super();
		addFocusListener(new FocusAdapter() { // on lostFocus : check
			@Override
			public void focusLost(FocusEvent evt) {
				try {
					massknopf.reset();
				} catch (Exception e) {
					LogFactory.getLog(SkelKodeFeld.class).error(e, e);
				}
			}
		});
	}

	@Override
	public boolean condition() {
		boolean result;
		try {
			Integer i = Integer.parseInt(this.getText());
			String s = ProjektFenster.skellist.getTeil(i);
			if (!s.equals("")) { //$NON-NLS-1$
				titledBorder.setTitle(s);
				result = true;
				// bei \u00C4nderung knochenteilfield auf "" setzen
				// this.knochenteilfield.setText("");
				knochenteilfield.setForeground(Color.red);

			} else {
				titledBorder.setTitle(""); //$NON-NLS-1$
				result = false;
			}

		} catch (Exception e) {
			titledBorder.setTitle(""); //$NON-NLS-1$
			result = false;
		}

		return result;
	} /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
	 */

	/**
	 * @param field
	 */
	public void setKnochenTeilField(KnochenTeilKodeFeld field) {
		knochenteilfield = field;
	}

	public void setMassKnopf(MassKnopf masse) {
		massknopf = masse;
	}
}
