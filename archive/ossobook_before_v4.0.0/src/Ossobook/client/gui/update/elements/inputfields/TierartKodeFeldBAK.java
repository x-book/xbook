/*
 * TierartKodeFeld.java
 */
package ossobook.client.gui.update.elements.inputfields;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JTextField;

import ossobook.client.gui.common.ProjectManipulation;
import ossobook.client.gui.update.components.window.ProjektFenster;
import ossobook.client.gui.update.elements.other.MassKnopf;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * Holds the full functionality of the animal code field.
 * 
 * @author Daniel Kaltenthaler
 */
public class TierartKodeFeldBAK extends JComboBox implements KeyListener, FocusListener, ItemListener {

	/** Hold the minimum number of chars when start searching for
	 *  available animals for the input substring */
	private final int MINIMUM_CHARS_STARTING_TO_SEARCH = 3;
	
	/** Hold the serial version user ID for the serializable class */
	private static final long serialVersionUID = 1L;
	/** ? */
	private MassKnopf massknopf;
	/** Hold the JTextField (of the JComboBox) where to enter
	 * and select the searched animal */
	private JTextField searchField;
	
	/** A ProjectManipulation object holding the subwindow for the content */
	private ProjectManipulation window;
	/** A ProjektFenster object holding the frame for the bones */
	private ProjektFenster boneframe;
	
	/** Holds the text string of the JTextField */
	private String stringInput = "";
	
	/**
	 * Constructor of the TierartKodeFeld class.
	 * 
	 * Creates a new instance of TierartKodeFeld set up the default settings
	 */
	public TierartKodeFeldBAK() {
		super();
		
		setEditable(true);

        searchField = (JTextField) getEditor().getEditorComponent();
        searchField.addKeyListener(this);
        searchField.addFocusListener(this);
        
        addItemListener(this);
	}

	/**
	 * The key released listener.
	 * 
	 * As soon as any key is pressed while the JComboBox is on focus the listener
	 * saves the updated input string in the class variable <code>stringInput</code>.
	 * 
	 * Then it checks if it is an Integer values or not. If it is it makes it possible
	 * to convert the animal Id to the animal Name by pressing 'Enter'.
	 * 
	 * Finaly this listener updates the items of the JComboBox and checks if the current
	 * input string is available in the database or not and mark it by using colors.
	 * 
	 * @param event The KeyEvent which is executed.
	 * 
	 * @see java.awt.event.KeyListener
	 */
	public void keyReleased(KeyEvent event) {
		char source = event.getKeyChar();

		// Save the input text
		stringInput = searchField.getText();

		try {
			// check if the input is an Integer. If not a
			// NumberFormatException is thrown and the code in the
			// try block is not executed.
			Integer.parseInt(stringInput);
			if (source == KeyEvent.VK_ENTER) {
				// if the input is an Integer and the key 'Enter' is pressed,
				// convert the number to the animal name.
				convertAnimalIdToAnimalName();
			} else {
				// if the input is an Integer, keep the font color black and exit the method.
				checkAndDisplayTierlabelAvailable(stringInput, Color.BLACK, Color.BLACK);
				return;
			}
		} catch (NumberFormatException e) {
			// do nothing
		}

		// update the content of the combo box
		updateComboboxItems(stringInput);
		// set the Font color to red if the current input is no animal in the database,
		// or to green if it is.
		checkAndDisplayTierlabelAvailable(stringInput, Color.RED, Color.GREEN);
	}

	
	/**
	 * Updates the items of the JComboBox.
	 * 
	 * Checks which animals are available containing the substring commited by the parameter
	 * and add them to the item list of the JComboBox.
	 * 
	 * The search is only executed if there are typed in at least
	 * <code>MINIMUM_CHARS_STARTING_TO_SEARCH</code> characters.
	 * 
	 * @param subString A String holding the substring of the animal name which is searched.
	 */
	private void updateComboboxItems(String subString) {
		removeAllItems();
		addItem(subString);

//		window = (ProjectManipulation) ProjektFenster.getMainFrame().getDesktop().getSelectedFrame();
//		boneframe = window.getProjectWindow();
		boneframe = ProjektFenster.getProjectFenster();

		try {
			if (subString.length() >= MINIMUM_CHARS_STARTING_TO_SEARCH) {
				Vector<String> searchResult = new Vector<String>();
	
				// get an Vector object including the names of the animals which should be added to the combobox listing.
				try {
					searchResult = boneframe.getManager().getTierlabelListingForSearch(subString);
				} catch (StatementNotExecutedException ex) {
					ex.printStackTrace();
				}
	
				// add all items from the Vector file to the combobox
				for (int akk=0; akk<searchResult.size(); akk++) {
					addItem(searchResult.elementAt(akk));
				}

			}
		} catch (NullPointerException e) {
			// do nothing
		}

		// set the cursor to the end. Necessary because the update of the
		// JComboBox items marks als characters.
		try {
			searchField.setSelectionStart(subString.length());
		} catch (NullPointerException e) {
			// do nothing
		}
	}
	
	/**
	 * Check if the name of an animal is available in the database and
	 * display the status with font colors in the JComboBox.
	 * 
	 * @param stringInput The String which should be checked if it is a valid animal.
	 * @param colorAnimalNotAvailable A Color object holding the font color if the
	 *                                animal is not available in the database.
	 * @param colorAnimalAvailable A Color object holding the font color if the animal
	 *                             is available in the database.
	 * 
	 * @return <code>true</code> if the animal is available in the database,
	 *         <code>false</code> if not.
	 */
	private boolean checkAndDisplayTierlabelAvailable(String stringInput,
			Color colorAnimalNotAvailable, Color colorAnimalAvailable) {
		try {
			if (boneframe.getManager().isTierlabelAvailable(stringInput)) {
			    searchField.setForeground(colorAnimalAvailable);
			    return true;
			} else {
			    searchField.setForeground(colorAnimalNotAvailable);
			    return false;
			}
		} catch (StatementNotExecutedException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			// do nothing
		}
		return false;
	}
	
	/**
	 * Converts an animal Id in the JComboBox to the animal name.
	 */
	public void convertAnimalIdToAnimalName() {
		try {
			stringInput = ProjektFenster.animallist.getTeil(Integer.parseInt(stringInput));
		} catch (Exception ex) {
			// do nothing
		}
	}
	
	/**
	 * Returns the animal ID of the selected value of the JComboBox.
	 * 
	 * @return A String value holding the animal Id of the selected value of the JComboBox.
	 */
	public String toString() {
		try {
			return boneframe.getManager().getTierCode(stringInput);
		} catch (StatementNotExecutedException ex) {
			ex.printStackTrace();
		}
		return "";
	}
	
	/**
	 * ?
	 * 
	 * @see ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
	 */
	public void setMassKnopf(MassKnopf masse) {
		massknopf = masse;
	}

	/**
	 * Clear the textfield and set the text back to an emtpy string.
	 */
	public void clearTextField() {
		updateComboboxItems("");
	}
	
	/**
	 * Check if the selected animal name is available in the database or not.
	 * 
	 * @return <code>true</code> if the selected animal name is available in the
	 *         the database, <code>false</code> if not
	 */
	public boolean check() {
		return checkAndDisplayTierlabelAvailable(stringInput, Color.RED, Color.BLACK);
	}
	
	/**
	 * Get a tooltip for the JComboBox.
	 * 
	 * @return A String holding the tooltip of the JComboBox.
	 */
	public String getToolTipText() {
		return "Tierart";
	}
	
	/**
	 * Set an animal by committing an animal id.
	 * This method is used by the reference work.
	 * 
	 * @param id The ID of the animal which should be set in the combo box.
	 *           IMPORTANT: The String must have an Integer value!!!
	 */
	public void setText(String id) {
		removeAllItems();
		addItem(ProjektFenster.animallist.getTeil(Integer.parseInt(id)));
	}

	/**
	 * The item state changed listener.
	 * 
	 * Saves the input text in the class variable and check if it is a valid animal.
	 * 
	 * @param event The ItemEvent which is executed.
	 */
	public void itemStateChanged(ItemEvent event) {
		stringInput = searchField.getText();
		checkAndDisplayTierlabelAvailable(stringInput, Color.RED, Color.GREEN);
	}

	/**
	 * The focus lost listener.
	 * 
	 * If the focus of the JComboBox is lost (e.g. by pressing 'Tab') convert
	 * animal Ids to animal names, update the items of the JComboBox and
	 * check if the current input text is a valid animal.
	 * 
	 * @param event The FocusEvent which is executed.
	 */
	public void focusLost(FocusEvent event) {
		// try to convert Integer from an animal id to an animal name
		convertAnimalIdToAnimalName();
		// update the content of the combo box
		updateComboboxItems(stringInput);
		// set the Font color to red if the current input is no animal in the database.
		checkAndDisplayTierlabelAvailable(stringInput, Color.RED, Color.BLACK);
	}

	/** Method not in use, but necessary because of implementation. Method does nothing. */
	public void keyTyped(KeyEvent event) { }
	/** Method not in use, but necessary because of implementation. Method does nothing. */
	public void keyPressed(KeyEvent event) { }
	/** Method not in use, but necessary because of implementation. Method does nothing. */
	public void focusGained(FocusEvent event) {}

}