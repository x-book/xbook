package ossobook.client.gui.update.elements.inputfields;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturTextFeld;

/*
 * ItemNrField.java
 *
 * Created on 21. M\u00E4rz 2003, 15:34
 */

/**
 * 
 * @author ali
 */
public class KoordinatenFeld extends AbstraktesSelbstkorrekturTextFeld {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates a new instance of ItemNrField */
	public KoordinatenFeld() {
		super();
	}

	@Override
	public boolean condition() {
		boolean result;
		try {
			Float.parseFloat(getText());
			result = true;
		} catch (NumberFormatException e) {
			result = false;
		}
		if (this.getText().equals("")) { //$NON-NLS-1$
			result = true;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
	 */

}
