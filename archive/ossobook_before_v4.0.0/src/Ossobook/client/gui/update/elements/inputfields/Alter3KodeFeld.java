/*
 * AnimalCodeField.java
 *
 * Created on 19. M\u00E4rz 2003, 18:22
 */
package ossobook.client.gui.update.elements.inputfields;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;

/**
 * @author ali
 */
public class Alter3KodeFeld extends AbstraktesSelbstkorrekturKodeFeld {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new instance of AnimalCodeField
	 */
	public Alter3KodeFeld() {
		super();
	}

	@Override
	public boolean condition() {
		return true;
	}
}