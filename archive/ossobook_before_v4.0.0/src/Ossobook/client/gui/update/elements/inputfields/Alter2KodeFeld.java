/*
 * AnimalCodeField.java
 *
 * Created on 19. M\u00E4rz 2003, 18:22
 */
package ossobook.client.gui.update.elements.inputfields;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;

/**
 * @author ali
 */
public class Alter2KodeFeld extends AbstraktesSelbstkorrekturKodeFeld {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new instance of AnimalCodeField
	 */
	public Alter2KodeFeld() {
		super();
	}

	@Override
	public boolean condition() {
		boolean result;
		try {
			new Integer(Integer.parseInt(this.getText()));
			// Falls vergleich mit Liste gew\u00FCnscht: Auskommentieren
			// String s = JIntFrameBoneProject.alter2list.getTeil(i);
			// if (!(s==null)) {
			// titledBorder.setTitle(s);
			// result = true;
			// } else {
			// titledBorder.setTitle("");
			// result = true;
			// }
			result = true; // entfernen, falls vergleich mit Liste
			// gew\u00FCnscht
		} catch (Exception e) {
			titledBorder.setTitle(""); //$NON-NLS-1$
			result = false;
		}
		if (getText().equals("")) { //$NON-NLS-1$
			result = true;
		}
		return result;
	}
}