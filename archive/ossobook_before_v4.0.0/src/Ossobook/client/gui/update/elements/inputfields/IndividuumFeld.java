package ossobook.client.gui.update.elements.inputfields;

import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import org.apache.commons.logging.LogFactory;

import ossobook.client.base.metainfo.Project;
import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturTextFeld;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.queries.QueryManager;

/*
 * ItemNrField.java
 *
 * Created on 21. M\u00E4rz 2003, 15:34
 */

/**
 * 
 * @author ali
 */
public class IndividuumFeld extends AbstraktesSelbstkorrekturTextFeld {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final QueryManager manager;
	private final Project project;

	/** Creates a new instance of ItemNrField */
	public IndividuumFeld(QueryManager manager, Project project) {
		super();
		this.project = project;
		this.manager = manager;
		addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent keyEvt) {
			}

			public void keyReleased(KeyEvent keyEvt) {
				int pressedCharIntValue = Character.getNumericValue(keyEvt
						.getKeyChar());
				int actionValue = Character.getNumericValue('n');
				if (pressedCharIntValue == actionValue) {
					setText(getNextFreeIndividualNr() + ""); //$NON-NLS-1$
				}
			}

			public void keyTyped(KeyEvent keyEvt) {
			}
		});
	}

	private int getNextFreeIndividualNr() {
		try {
			return manager.getNextFreeIndividualNr(project) + 1;
		} catch (StatementNotExecutedException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public boolean condition() {
		if (!getText().equals("")) { //$NON-NLS-1$
			try {
				int parsed = Integer.parseInt(getText());
				try {
					if ( manager.getCountKnIndNr(parsed, project) > 0) {
						setBackground(Color.YELLOW);
					} else {
						setBackground(SystemColor.text);
					}
					return true;
				} catch (StatementNotExecutedException e) {
					LogFactory.getLog(IndividuumFeld.class).error(e, e);
					setBackground(SystemColor.text);
				}
			} catch (NumberFormatException ignored) {
			}
		}
		return true;
	}
}
