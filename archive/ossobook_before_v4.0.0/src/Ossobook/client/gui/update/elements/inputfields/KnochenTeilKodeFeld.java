package ossobook.client.gui.update.elements.inputfields;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import ossobook.Messages;
import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;
import ossobook.client.gui.update.components.window.ProjektFenster;

/**
 * @author ali
 */
public class KnochenTeilKodeFeld extends AbstraktesSelbstkorrekturKodeFeld {
	private static final long serialVersionUID = 1L;
	private final SkelKodeFeld skelCodeField;
	private final KnochenTeilKodeWindow window;

	/**
	 * Creates a new instance of SkelCodeField
	 */
	public KnochenTeilKodeFeld(SkelKodeFeld field) {
		super();
		this.skelCodeField = field;
		window = new KnochenTeilKodeWindow(this);
		field.setKnochenTeilField(this);

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					// Double click, open mask.
					try {
						window.display(Integer.parseInt(skelCodeField.getText()));
					} catch (NumberFormatException ex) {
						System.out.println(Messages.getString("KnochenTeilKodeFeld.0")); //$NON-NLS-1$
					} catch (Exception ex) {
						System.out.println(ex.getMessage());
					}
				}
			}
		});
	}

	@Override
	public boolean condition() {
		boolean result;
		setForeground(null);
		try {
			Integer.parseInt(this.getText());
			result = true;
		} catch (NumberFormatException e) {
			result = false;
		}
		if (result) {
			// falls keine Aufl\u00F6sung m\u00F6glich
			try {
				Integer i = new Integer(this.getText());
				Integer j = new Integer(skelCodeField.getText());
				titledBorder.setTitle(ProjektFenster.knochenteillist.getTeil(i + "," + j)); //$NON-NLS-1$
			} catch (Exception e) {
				// NoSuchPartException
			}
		}
		if (this.getText().equals("")) { //$NON-NLS-1$
			result = true;
		}
		return result;
	}
}
