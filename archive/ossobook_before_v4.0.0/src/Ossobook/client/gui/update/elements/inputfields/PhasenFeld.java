/*
 * Created on 19.07.2004
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.gui.update.elements.inputfields;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturTextFeld;

/**
 * @author ali Window - Preferences - Java - Code Style - Code Templates
 */
public class PhasenFeld extends AbstraktesSelbstkorrekturTextFeld {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PhasenFeld(String s) {
		super(s);
	}

	@Override
	public boolean condition() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
	 */

}
