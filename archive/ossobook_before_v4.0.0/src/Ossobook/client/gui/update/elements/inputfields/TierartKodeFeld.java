/*
 * AnimalCodeField.java
 *
 * Created on 19. M\u00E4rz 2003, 18:22
 */
package ossobook.client.gui.update.elements.inputfields;

import org.apache.commons.logging.LogFactory;
import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;
import ossobook.client.gui.update.components.window.ProjektFenster;
import ossobook.client.gui.update.elements.other.MassKnopf;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 * @author ali
 */
@SuppressWarnings("serial")
public class TierartKodeFeld extends AbstraktesSelbstkorrekturKodeFeld {
	private MassKnopf massknopf;

	/** Creates a new instance of AnimalCodeField */
	public TierartKodeFeld() {
		super();
		addFocusListener(new FocusAdapter() { // on lostFocus : check
			@Override
			public void focusLost(FocusEvent evt) {
				try {
					massknopf.reset();
				} catch (Exception e) {
					LogFactory.getLog(TierartKodeFeld.class).error(e, e);
				}
			}
		});
	}

	@Override
	public boolean condition() {
		boolean result;
		try {
			Integer i = Integer.parseInt(this.getText());
			String s = ProjektFenster.animallist.getTeil(i);
			if (!s.equals("")) { //$NON-NLS-1$
				titledBorder.setTitle(s);
				result = true;

			} else {
				titledBorder.setTitle(""); //$NON-NLS-1$
				result = false;
			}

		} catch (Exception e) {
			titledBorder.setTitle(""); //$NON-NLS-1$
			result = false;
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
	 */
	public void setMassKnopf(MassKnopf masse) {
		massknopf = masse;
	}

}