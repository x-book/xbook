/*
 * Created on 19.07.2004
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.gui.update.elements.inputfields;

import javax.swing.JCheckBox;

import ossobook.Messages;

/**
 * @author ali Preferences - Java - Code Style - Code Templates
 */
public class VerdautFeld extends JCheckBox {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		if (isSelected()) {
			return Messages.getString("ProjektFenster.74"); //$NON-NLS-1$
		} else {
			return ""; //$NON-NLS-1$
		}
	}
}