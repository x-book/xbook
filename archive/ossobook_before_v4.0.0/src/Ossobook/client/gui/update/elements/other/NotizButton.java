/*
 * Created on 12.04.2005
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ossobook.client.gui.update.elements.other;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import ossobook.client.gui.common.OssobookFrame;
import ossobook.client.gui.update.elements.inputfields.NotizFeld;
import ossobook.client.util.Configuration;

/**
 * @author ali Preferences - Java - Code Style - Code Templates
 */
public class NotizButton extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final JInternalFrame intFrame;

	private final NotizFeld notizFeld;

	public NotizButton(String Beschriftung, final OssobookFrame mainframe) {
		super(Beschriftung);
		intFrame = new JInternalFrame();
		notizFeld = new NotizFeld();
		JScrollPane sp = new JScrollPane(notizFeld);
		intFrame.getContentPane().add(sp);
		intFrame.setSize(Configuration.notizfeld_x,
				Configuration.notizfeld_y);
		intFrame.setResizable(true);
		intFrame.setClosable(true);
		intFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		mainframe.getDesktop().add(intFrame);
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				intFrame.setVisible(!intFrame.isVisible());
			}
		});
	}

	/**
	 * 
	 */
	public void reset() {
		notizFeld.reset();

	}

	public void setNotizFeldText(String text) {
		notizFeld.setText(text);
	}

	@Override
	public String toString() {
		return notizFeld.toString();
	}
}