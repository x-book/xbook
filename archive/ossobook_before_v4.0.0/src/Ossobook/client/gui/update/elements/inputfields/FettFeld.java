/*
 * SkelCodeField.java
 *
 * Created on 24. M\u00E4rz 2003, 16:42
 */
package ossobook.client.gui.update.elements.inputfields;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;
import ossobook.client.gui.update.components.window.ProjektFenster;

/**
 * @author ali
 */
public class FettFeld extends AbstraktesSelbstkorrekturKodeFeld {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates a new instance of SkelCodeField */
	public FettFeld() {
		super();

	}

	@Override
	public boolean condition() {
		boolean result;
		try {
			Integer i = Integer.parseInt(this.getText());
			String s = ProjektFenster.fettlist.getTeil(i);
			if (!s.equals("")) { //$NON-NLS-1$
				titledBorder.setTitle(s);
				result = true;
			} else {
				titledBorder.setTitle(""); //$NON-NLS-1$
				result = false;
			}

		} catch (Exception e) {
			titledBorder.setTitle(""); //$NON-NLS-1$
			result = false;
		}
		if (getText().equals("")) { //$NON-NLS-1$
			result = true;
		}
		if (getText().equals("")) { //$NON-NLS-1$
			result = true;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
	 */
}
