/*
 * SkelCodeField.java
 *
 * Created on 24. M\u00E4rz 2003, 16:42
 */
package ossobook.client.gui.update.elements.inputfields;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;
import ossobook.client.gui.update.components.window.ProjektFenster;

/**
 * @author ali
 */
public class Schlachtspur2Feld extends AbstraktesSelbstkorrekturKodeFeld {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates a new instance of SkelCodeField */
	public Schlachtspur2Feld() {
		super();
	}

	@Override
	public boolean condition() {
		boolean result;
		try {
			Integer.parseInt(this.getText());
			result = true;
		} catch (NumberFormatException e) {
			result = false;
		}
		if (result) {
			Integer i = new Integer(this.getText());
			titledBorder.setTitle(ProjektFenster.schlachtspur2list.getTeil(i));
		}
		if (this.getText().equals("")) { //$NON-NLS-1$
			result = true;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
	 */
}
