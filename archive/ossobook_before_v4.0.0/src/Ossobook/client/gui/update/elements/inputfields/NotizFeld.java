/*
 * NotizField.java
 *
 * Created on 19. M\u00E4rz 2003, 17:01
 */
package ossobook.client.gui.update.elements.inputfields;

import javax.swing.JTextArea;

/**
 * 
 * @author ali
 */
public class NotizFeld extends JTextArea {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates a new instance of NotizField */
	public NotizFeld() {
		super();
		setVisible(true);

	}

	public void reset() {
		setText(""); //$NON-NLS-1$
	}

	@Override
	public String toString() {
		return this.getText();
	}
}