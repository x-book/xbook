package ossobook.client.gui.update.elements.inputfields;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturTextFeld;

/*
 * ItemNrField.java
 *
 * Created on 21. M\u00E4rz 2003, 15:34
 */

/**
 * 
 * @author ali
 */
public class AnzahlFeld extends AbstraktesSelbstkorrekturTextFeld {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates a new instance of ItemNrField */
	public AnzahlFeld() {
		super();
		setText("1"); //$NON-NLS-1$
	}

	@Override
	public boolean condition() {
		boolean result;
		try {
			Integer.parseInt(getText());
			result = true;
		} catch (NumberFormatException e) {
			result = false;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ossobook.framework.elements.AbstractValidatedTextField#reset()
	 */
	@Override
	public void reset() {
		setText("1"); //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
	 */

}
