/*
 * AnimalCodeField.java
 *
 * Created on 19. M\u00E4rz 2003, 18:22
 */
package ossobook.client.gui.update.elements.inputfields;

import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturKodeFeld;
import ossobook.client.gui.update.components.window.ProjektFenster;

/**
 * @author ali
 */
public class Alter1KodeFeld extends AbstraktesSelbstkorrekturKodeFeld {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new instance of AnimalCodeField
	 */
	public Alter1KodeFeld() {
		super();
	}

	@Override
	public boolean condition() {
		boolean result;
		try {
			Integer i = Integer.parseInt(this.getText());
			String s = ProjektFenster.alter1list.getTeil(i);
			if (!s.equals("")) { //$NON-NLS-1$
				titledBorder.setTitle(s);
				result = true;
			} else {
				titledBorder.setTitle(""); //$NON-NLS-1$
				result = false;
			}

		} catch (Exception e) {
			titledBorder.setTitle(""); //$NON-NLS-1$
			result = false;
		}
		if (getText().equals("")) { //$NON-NLS-1$
			result = true;
		}
		return result;
	}
}