/*
 * SexField.java
 *
 * Created on 19. M\u00E4rz 2003, 17:44
 */
package ossobook.client.gui.update.elements.inputfields;

import javax.swing.JComboBox;

/**
 * 
 * @author ali
 */
public class KoerperSeiteFeld extends JComboBox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates a new instance of SexField */
	public KoerperSeiteFeld() {
		super();
		addItem("indet");
		addItem("r"); // = r
		addItem("l"); // = l
	}

	@Override
	public String toString() {
		return getSelectedItem().toString();
	}
}