package ossobook.client.gui.update.elements.inputfields;

import java.awt.Color;
import java.awt.SystemColor;

import org.apache.commons.logging.LogFactory;

import ossobook.client.base.metainfo.Project;
import ossobook.client.gui.update.components.other.AbstraktesSelbstkorrekturTextFeld;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.queries.QueryManager;

/*
 * ItemNrField.java
 *
 * Created on 21. M\u00E4rz 2003, 15:34
 */
/**
 * 
 * @author ali
 */
public class SketeillNummerFeld extends AbstraktesSelbstkorrekturTextFeld {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final QueryManager manager;
	private final Project project;

	/** Creates a new instance of ItemNrField */
	public SketeillNummerFeld(QueryManager manager, Project project) {
		super();
		this.project = project;
		this.manager = manager;
	}

	@Override
	public boolean condition() {
		if (!getText().equals("")) { //$NON-NLS-1$
			try {
				int parsed = Integer.parseInt(getText());
				try {
					if (manager.getCountSkelNr(parsed, project) > 0) {
						setBackground(Color.yellow);
					} else {
						setBackground(SystemColor.text);
					}
					return true;
				} catch (StatementNotExecutedException e) {
					LogFactory.getLog(SketeillNummerFeld.class).error(e, e);
					setBackground(SystemColor.text);
				}
			} catch (NumberFormatException ignored) {
			}
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ossobook.framework.elements.AbstractValidatedTextField#useDefaultValue()
	 */

}
