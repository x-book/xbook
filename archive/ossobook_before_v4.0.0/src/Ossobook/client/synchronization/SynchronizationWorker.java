package ossobook.client.synchronization;

import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.Collection;
import java.util.Vector;

import javax.swing.SwingWorker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.base.metainfo.Project;
import ossobook.client.io.file.Logging;
import ossobook.client.io.mail.Mail;
import ossobook.client.synchronization.SynchronizationProgress.Type;
import ossobook.exceptions.MetaStatementNotExecutedException;
import ossobook.exceptions.NoReadRightExceptionForSynchronization;
import ossobook.exceptions.NoWriteRightExceptionForSynchronization;
import ossobook.exceptions.StatementNotExecutedException;
import ossobook.queries.QueryManager;
import ossobook.queries.QueryManagerFactory;
import ossobook.queries.UserManager;

/**
 * This class extends a {@link SwingWorker} to allow parallel processing of the
 * synchronization of a given list of projects.
 * 
 * <p>
 * 
 * </p>
 * 
 * @author j.lamprecht
 * @author fnuecke
 */
public class SynchronizationWorker extends SwingWorker<Void, Void> {
	
	/**
	 * Logging.
	 */
	private static final Log log = LogFactory.getLog(SynchronizationWorker.class);
	
	/**
	 * Property name of the progress property which is updated whenever the
	 * synchronizations progress changes.
	 */
	public static final String	PROPERTY_PROGRESS	= "progress";	//$NON-NLS-1$

	private final QueryManager		localManager;
	private final QueryManager		globalManager;
	
	private final Collection<String>	projects;
	
	private Vector<String>		schemaLogs;
	private Vector<String>		conflictLogs;
	
	private SynchronizationProgress oldProgress;
	
	public SynchronizationWorker(QueryManager localManager,
			QueryManager globalManager, Collection<String> projects) {
		this.localManager = localManager;
		this.globalManager = globalManager;
		this.projects = projects;
	}
	
	/**
	 * Used to update the progress property of the synchronization task.
	 * 
	 * <p>
	 * This updates the progress value and dispatches a property change event,
	 * with a new instance of {@link SynchronizationProgress} holding the two values specified.
	 * </p>
	 * 
	 * <p>
	 * If set, uses the old percentual progress, else uses 0.
	 * </p>
	 * 
	 * @param type
	 *            the type of the progress update.
	 * @param message
	 *            the message to display.
	 */
	private void update(SynchronizationProgress.Type type, String message) {
		if (oldProgress != null) {
			update(type, message, oldProgress.getProgress());
		} else {
			update(type, message, 0);
		}
	}
	
	/**
	 * Used to update the progress property of the synchronization task.
	 * 
	 * <p>
	 * This updates the progress value and dispatches a property change event,
	 * with a new instance of {@link SynchronizationProgress} holding the two values specified.
	 * </p>
	 * 
	 * @param type
	 *            the type of the progress update.
	 * @param message
	 *            the message to display.
	 * @param progress
	 *            the percentual progress of the synchronization.
	 */
	private void update(SynchronizationProgress.Type type, String message, double progress) {
		SynchronizationProgress newProgress = new SynchronizationProgress(type, message, progress);
		firePropertyChange(PROPERTY_PROGRESS, oldProgress, newProgress);
		oldProgress = newProgress;
	}
	
	/**
	 * Utility function for reformatting a stacktrace to a string, prepended
	 * with tabs and adding a newline after each trace step.
	 * 
	 * @param trace
	 *            the stacktrace to format.
	 * @return the formatted stacktrace as a string.
	 */
	private static String stringifyStackTrace(StackTraceElement[] trace) {
		StringBuilder sb = new StringBuilder();
		for (StackTraceElement element : trace) {
            sb.append("\t").append(element.toString()).append("\n"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return sb.toString();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Void doInBackground() {
		// Disable query managers for other access.
		QueryManagerFactory.setSynchronizationInProgress(true);
		
		update(Type.STARTED, Messages.getString("SynchronizationWorker.2")); //$NON-NLS-1$
		
		// Check if the connections are valid.
		if (localManager == null || globalManager == null) {
			update(Type.ERROR, Messages.getString("SynchronizationWorker.0")); //$NON-NLS-1$
			return null;
		}
		
		try {
			// Lock the local database to prevent manipulation during the
			// synchronization process.
			localManager.lockDatabase();
			
			// Set transaction isolation and disable auto commits (begin
			// transaction)
			int localLevel = localManager.setIsolationLevel(QueryManager.ISOLATION_LEVEL);
			localManager.disableAutoCommit();
			Savepoint localSavepoint = localManager.getConnection().setSavepoint();
			
			int syncLevel = globalManager.setIsolationLevel(QueryManager.ISOLATION_LEVEL);
			globalManager.disableAutoCommit();
			Savepoint globalSavepoint = globalManager.getConnection().setSavepoint();
			
			// Try the actual synchronization.
			try {
				doSynchronization();
			} catch (SQLException ex) {
				// Failed... roll back if the connections still stand, and
				// revert to old mode.
				try {
					globalManager.getConnection().rollback(globalSavepoint);
					globalManager.enableAutoCommit();
					globalManager.setIsolationLevel(syncLevel);
				} catch (SQLException ex2) {
					log.error(ex2, ex2);
				}
				try {
					localManager.getConnection().rollback(localSavepoint);
					localManager.enableAutoCommit();
					localManager.setIsolationLevel(localLevel);
				} catch (SQLException ex2) {
					log.error(ex2, ex2);
				}

				// And finally, unlock the local database.
				try {
					localManager.unlockDatabase();
				} catch (SQLException ex2) {
					log.error(ex2, ex2);
				}
				
				update(Type.ERROR, Messages.getString("SynchronizationWorker.3", stringifyStackTrace(ex.getStackTrace()))); //$NON-NLS-1$
				return null;
			}
			
			// Successful so far, commit all changes and end the transaction.
			try {
				globalManager.commit();
				localManager.commit();
			} catch (SQLException ex) {
				// Failed committing, again, roll back whats possible and
				// restore the connection which is still alive.
				try {
					globalManager.getConnection().rollback(globalSavepoint);
					globalManager.enableAutoCommit();
					globalManager.setIsolationLevel(syncLevel);
				} catch (SQLException ex2) {
					log.error(ex2, ex2);
				}
				try {
					localManager.getConnection().rollback(localSavepoint);
					localManager.enableAutoCommit();
					localManager.setIsolationLevel(localLevel);
				} catch (SQLException ex2) {
					log.error(ex2, ex2);
				}
				
				// And finally, unlock the local database.
				try {
					localManager.unlockDatabase();
				} catch (SQLException ex2) {
					log.error(ex2, ex2);
				}
				
				update(Type.ERROR, Messages.getString("SynchronizationWorker.3", stringifyStackTrace(ex.getStackTrace()))); //$NON-NLS-1$
				return null;
			}
			
			// Print changes to database schema and path to conflict file.
			if (schemaLogs != null && schemaLogs.size() > 0) {
				StringBuilder schemaPaths = new StringBuilder();
				for (String path : schemaLogs) {
                    schemaPaths.append(path).append("\n"); //$NON-NLS-1$
				}
				update(Type.WARNING, Messages.getString("SynchronizationWorker.6", schemaPaths.toString())); //$NON-NLS-1$
			}
			if (conflictLogs != null && conflictLogs.size() > 0) {
				StringBuilder conflictPaths = new StringBuilder();
				for (String path : conflictLogs) {
                    conflictPaths.append(path).append("\n"); //$NON-NLS-1$
				}
				update(Type.WARNING, Messages.getString("SynchronizationWorker.8", conflictPaths.toString())); //$NON-NLS-1$
			}
			
			// Release save points and revert old state.
			try {
				globalManager.getConnection().releaseSavepoint(globalSavepoint);
				globalManager.enableAutoCommit();
				globalManager.setIsolationLevel(syncLevel);
			} catch (SQLException ex) {
				log.error(ex, ex);
			}
			try {
				localManager.getConnection().releaseSavepoint(localSavepoint);
				localManager.enableAutoCommit();
				localManager.setIsolationLevel(localLevel);
			} catch (SQLException ex) {
				log.error(ex, ex);
			}
			
			// And finally, unlock the local database.
			try {
				localManager.unlockDatabase();
			} catch (SQLException ex) {
				log.error(ex, ex);
			}
			
			// We did it!
			update(Type.COMPLETE, Messages.getString("SynchronizationWorker.9")); //$NON-NLS-1$
		} catch (SQLException ex) {
			update(Type.ERROR, Messages.getString("SynchronizationWorker.3", stringifyStackTrace(ex.getStackTrace()))); //$NON-NLS-1$
		}
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void done() {
		// Enable query managers for other access.
		QueryManagerFactory.setSynchronizationInProgress(false);
	}
	
	/**
	 * Perform the actual synchronization of the local with the global database.
	 * 
	 * @throws SQLException if anything goes wrong.
	 */
	private void doSynchronization() throws SQLException {
		// Get the database scheme.
		DatabaseScheme scheme = new DatabaseScheme(localManager, globalManager);
		
		// Get any changes to the database scheme in the global database and
		// apply them to the local database.
		update(Type.UPDATE, Messages.getString("SynchronizationWorker.11")); //$NON-NLS-1$
		String path = scheme.synchronizeScheme();
		if (path != null) {
			schemaLogs = new Vector<String>();
			schemaLogs.add(path);
		}
		update(Type.UPDATE, Messages.getString("SynchronizationWorker.12"), 0.1); //$NON-NLS-1$
		
		// Perform the actual synchronization. Check all given projects and see
		// if they need initializing or synchronizing (of project data).
		int doneCounter = 0;
		double totalCount = projects.size();
		for (String projectName : projects) {
			Project project = localManager.getProject(projectName);
			if (project != null) {
				// Exists locally, synchronize.
				synchronize(project, scheme);
			} else {
				project = globalManager.getProject(projectName);
				if (project != null) {
					// Exists globally, initialize.
					initialize(project);
				} else {
					// Wait... what?
					update(Type.WARNING, Messages.getString("SynchronizationWorker.13", projectName)); //$NON-NLS-1$
				}
			}
			update(Type.UPDATE, null, 0.1 + 0.4 * doneCounter++/totalCount);
		}
		
		// Synchronize code tables. This is the same for initialization and
		// synchronization and represents the actual synchronizing of the data.
		update(Type.UPDATE, Messages.getString("SynchronizationWorker.14"), 0.5); //$NON-NLS-1$
		
		// Loop through all known tables. This copies all the data from the
		// global database into the local one.
		doneCounter = 0;
		totalCount = scheme.getDefinitionTableNames().length;
		for (String table : scheme.getDefinitionTableNames()) {
			// Get changes from the global database and introduce them into the
			// local database.
			try {
				localManager.changeDefinitions(table,
						globalManager.getDefinitionData(table, localManager.getLastSynchronizationOfDefinitionTable(table)),
						scheme.getDefinitionTableScheme(table));
				
				// Short wait, otherwise this tends to hang... dunno why.
				try {
					Thread.sleep(200);
				} catch (InterruptedException ignored) {
				}
				
				// Clean up: delete rows marked as deleted.
				localManager.deleteDefinitionPermanent(table);
				
				// Notify world.
				update(Type.UPDATE, Messages.getString("SynchronizationWorker.15", table), 0.5 + 0.45 * doneCounter++/totalCount); //$NON-NLS-1$
			} catch (SQLException e) {
				update(Type.WARNING, Messages.getString("SynchronizationWorker.16", table)); //$NON-NLS-1$
				throw e;
			}
		}
		
		// Done synchronizing, notify world.
		update(Type.UPDATE, Messages.getString("SynchronizationWorker.17"), 0.95); //$NON-NLS-1$
	}
	
	/**
	 * Initialize a project, i.e. create it locally and fetch the data from the
	 * global database.
	 * 
	 * @param project
	 *            the project to initialize to the local database.
	 */
	private void initialize(Project project) throws SQLException {
		String projectName = project.getName();
		
		update(Type.UPDATE, Messages.getString("SynchronizationWorker.18", projectName)); //$NON-NLS-1$
		
		try {
			// Check whether user has read right.
			if (!globalManager.getIsAdmin() &&
					globalManager.getRight(project) == UserManager.NORIGHTS)
			{
				update(Type.WARNING, Messages.getString("SynchronizationWorker.19", projectName)); //$NON-NLS-1$
				return;
			}
			
			// Get all data on the project.
			Vector<String[][]> projectData = globalManager.
				getGlobalProjectData(project, QueryManager.ALL_GLOBAL, null);
			
			// Anything found?
			if (projectData.size() > 0) {
				// Check if the project exists locally. If it does, delete all
				// traces before continuing.
				if (localManager.projectExists(project)) {
					localManager.deleteProjectPermanent(project, QueryManager.DELETE_ALL);
				}
				// Insert record into local database.
				localManager.insertData(projectData, project, true);
				localManager.setLastSynchronizeTime(projectData.elementAt(5)[0][0], project);
				localManager.setSynchronized(project);
				// Clean up by deleting rows marked as deleted.
				localManager.deleteProjectPermanent(project, QueryManager.DELETE_DELETED);
			}
			
			update(Type.UPDATE, Messages.getString("SynchronizationWorker.20", projectName)); //$NON-NLS-1$
		} catch (SQLException e) {
			update(Type.WARNING, Messages.getString("SynchronizationWorker.21", projectName)); //$NON-NLS-1$
			throw e;
		}
	}
	
	/**
	 * Synchonizes a project that already exists locally with the global
	 * database (and inserts it into the global database if it's not known
	 * there, yet).
	 * 
	 * @param project
	 *            the project to synchronize.
	 * @throws SQLException if something goes wrong on a global level.
	 */
	private void synchronize(Project project, DatabaseScheme scheme) throws SQLException {
		update(Type.UPDATE, Messages.getString("SynchronizationWorker.22", project.getName())); //$NON-NLS-1$
		
		conflictLogs = new Vector<String>();
		
		try {
			Logging logging = null;
			
			boolean projectExists = globalManager.projectExists(project);
			
			// no answer for last synchronization of local project changes
			String lastSynchronization = localManager.getLastSynchronizationOfProject(project);
			
			logging = commitChanges(localManager.getChangesOfProject(project, scheme.getProjectScheme()),
					lastSynchronization, project, project.getName(), logging,
					localManager.getAssignedMessageNumber(project),
					localManager.getDatabaseNumber());
			
			// exchange new changes of project
			localManager.setMessageNumber(project);
			logging = commitChanges(localManager.getChangesOfProject(project, scheme.getProjectScheme()),
					lastSynchronization, project, project.getName(), logging,
					localManager.getAssignedMessageNumber(project),
					localManager.getDatabaseNumber());
			
			// inform about occurred conflicts
			if (logging != null && logging.getRowsOfContent() > 4) {
				conflictLogs.add(logging.getPath());
			}
			
			// retransmit project: contains logic of retransmission
			// get global changes from server
			// prove whether user has read right
			if (!globalManager.getIsAdmin() &&
					globalManager.getRight(project) == UserManager.NORIGHTS &&
					projectExists)
			{
				throw new NoReadRightExceptionForSynchronization();
			}
			
			// project exists on local system
			Vector<String[][]> changes;
			if (lastSynchronization != null) {
				changes = globalManager.getGlobalProjectData(project,
						QueryManager.CHANGES_GLOBAL, lastSynchronization);
			} else {
				changes = globalManager.getGlobalProjectData(project,
						QueryManager.ALL_GLOBAL, lastSynchronization);
			}
			
			// Short wait, otherwise this tends to hang... dunno why.
			try {
				Thread.sleep(200);
			} catch (InterruptedException ignored) {
			}
			
			// insert changes into local database
			localManager.changeData(changes, null);
			localManager.setLastSynchronizeTime(changes.elementAt(5)[0][0], project);
			localManager.setSynchronized(project);
			localManager.deleteProjectPermanent(project, QueryManager.DELETE_DELETED);

			update(Type.UPDATE, Messages.getString("SynchronizationWorker.23", project.getName())); //$NON-NLS-1$
        } catch (SQLException e) {
			update(Type.WARNING, Messages.getString("SynchronizationWorker.1", project.getName())); //$NON-NLS-1$
			throw e;
		} catch (NoWriteRightExceptionForSynchronization e) {
			update(Type.WARNING, Messages.getString("SynchronizationWorker.24", project.getName())); //$NON-NLS-1$
			throw new SQLException(e.getMessage(), e);
		} catch (NoReadRightExceptionForSynchronization e) {
			update(Type.WARNING, Messages.getString("SynchronizationWorker.25", project.getName())); //$NON-NLS-1$
			throw new SQLException(e.getMessage(), e);
		}
	}

	/**
	 * transmit changes and log occurred conflicts and save them on client
	 */
	private Logging commitChanges(Vector<String[][]> changes,
			String lastSynchronization, Project project, String projectName,
			Logging logging, int messageNumber, int databaseNumber)
			throws StatementNotExecutedException,
			MetaStatementNotExecutedException,
			NoWriteRightExceptionForSynchronization {
		
		if (messageNumber != -1) {
			// send project changes to server
			Logging conflictFile;
			Vector<String[]> scheme = new Vector<String[]>();
			scheme.add(changes.elementAt(0)[0]);
			scheme.add(changes.elementAt(1)[0]);
			scheme.add(changes.elementAt(2)[0]);
			scheme.add(changes.elementAt(3)[0]);
			
			// prove whether user has write right
			int right = globalManager.getRight(project);
			if (!globalManager.getIsAdmin() && globalManager.projectExists(project) && (right == UserManager.NORIGHTS || right != UserManager.WRITE)) {
				throw new NoWriteRightExceptionForSynchronization();
			}
			
			if (globalManager.checkMessageNumber(messageNumber, databaseNumber, project)) {
				Vector<Vector<String[]>> conflicts = globalManager.changeData(changes, lastSynchronization);
				
				// send conflict mail to project owner?
				if ((conflictFile = logConflicts(conflicts, globalManager.getProjectName(project), scheme)) != null) {
					Mail conflictMail = new Mail(globalManager.getMail(project),
							globalManager.getProjectName(project));
					try {
						conflictMail.sendConflictMail(conflictFile.getFile());
					} catch (Exception e) {
						log.warn(Messages.getString("SynchronizationWorker.4"), e); //$NON-NLS-1$
						update(Type.WARNING, Messages.getString("SynchronizationWorker.26")); //$NON-NLS-1$
					}
				}
				globalManager.updateMessageNumber(messageNumber, databaseNumber, project);
			} else {
				conflictFile = logConflicts(new Vector<Vector<String[]>>(), globalManager.getProjectName(project), scheme);
			}
			
			if (logging == null || logging.getRowsOfContent() <= 4) {
				if (conflictFile != null) {
					logging = new Logging(conflictFile);
				} else {
					logging = null;
				}
			} else {
				logging.append(conflictFile);
			}
			
			localManager.setSynchronized(project);
		} else if (logging == null) {
			logging = new Logging("Konflikte-" + projectName, projectName); //$NON-NLS-1$
			logging.log(Messages.getString("Synchronize.17", projectName)); //$NON-NLS-1$
		}
		return logging;
	}

	private Logging logConflicts(Vector<Vector<String[]>> conflicts,
			String projectName, Vector<String[]> schemeVector) {
		Logging conflictFile = new Logging("Konflikte-" + projectName, projectName); //$NON-NLS-1$
		conflictFile.log(Messages.getString("LocalChangeManager.4") + projectName + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		conflictFile.log("-------------------------------------------------------------\n\n"); //$NON-NLS-1$
		if (conflicts.size() < 1
				|| (conflicts.elementAt(0).size() < 1
						&& conflicts.elementAt(1).size() < 1
						&& conflicts.elementAt(2).size() < 1 && conflicts
						.elementAt(3).size() < 1)) {
			conflictFile.log(Messages.getString("LocalChangeManager.7")); //$NON-NLS-1$
			conflictFile = null;
		} else {
			if (conflicts.elementAt(0).size() > 0) {
				String[] columnsNotToLog = { "Zustand", "zuletztSynchronisiert" }; //$NON-NLS-1$ //$NON-NLS-2$
				logTableConflicts("projekt", conflicts.elementAt(0), //$NON-NLS-1$
						schemeVector.elementAt(0), columnsNotToLog, conflictFile);
			}
			if (conflicts.elementAt(1).size() > 0) {
				String[] columnsNotToLog = { "Zustand" }; //$NON-NLS-1$
				logTableConflicts("eingabeeinheit", conflicts.elementAt(1), //$NON-NLS-1$
						schemeVector.elementAt(1), columnsNotToLog, conflictFile);
			}
			if (conflicts.elementAt(2).size() > 0) {
				String[] columnsNotToLog = { "Zustand" }; //$NON-NLS-1$
				logTableConflicts("masse", conflicts.elementAt(2), schemeVector //$NON-NLS-1$
						.elementAt(2), columnsNotToLog, conflictFile);
			}
			if (conflicts.elementAt(3).size() > 0) {
				String[] columnsNotToLog = { "Zustand" }; //$NON-NLS-1$
				logTableConflicts("artefaktmaske", conflicts.elementAt(3), //$NON-NLS-1$
						schemeVector.elementAt(3), columnsNotToLog, conflictFile);
			}
		}
		return conflictFile;
	}

	private static void logTableConflicts(String tableName,
			Vector<String[]> conflicts, String[] scheme,
			String[] columnsNotToLog, Logging conflictFile) {
		
		conflictFile.log(Messages.getString("LocalChangeManager.0") + tableName + ": \n\n"); //$NON-NLS-1$ //$NON-NLS-2$
		Vector<Integer> notToLog = new Vector<Integer>();
		outer: for (int i = 0; i < scheme.length; i++) {
			String column = scheme[i].substring(scheme[i].indexOf(".") + 1); //$NON-NLS-1$
			for (String element : columnsNotToLog) {
				if (column.equals(element)) {
					notToLog.add(i);
					continue outer;
				}
			}
			conflictFile.log(column + " "); //$NON-NLS-1$
			if (i < scheme.length - 1) {
				conflictFile.log(","); //$NON-NLS-1$
			}
		}
		conflictFile.log("\n"); //$NON-NLS-1$
		for (int j = 0; j < conflicts.size(); j++) {
			if (j % 2 == 0) {
				conflictFile.log(Messages.getString("LocalChangeManager.25")); //$NON-NLS-1$
			} else {
				conflictFile.log(Messages.getString("LocalChangeManager.26")); //$NON-NLS-1$
			}
			for (int i = 0; i < conflicts.elementAt(j).length; i++) {
				if (!notToLog.contains(Integer.valueOf(i))) {
					String value = conflicts.elementAt(j)[i];
					if (value == null) {
						conflictFile.log("-  "); //$NON-NLS-1$
					} else if (value.equals("")) { //$NON-NLS-1$
						conflictFile.log("-  "); //$NON-NLS-1$
					} else {
						conflictFile.log(value + " "); //$NON-NLS-1$
					}
					if (i < conflicts.elementAt(j).length) {
						conflictFile.log(","); //$NON-NLS-1$
					}
				}
			}
			conflictFile.log("\n"); //$NON-NLS-1$
		}
		conflictFile.log("\n------------------------------"); //$NON-NLS-1$
	}

}
