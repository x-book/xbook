package ossobook.client.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;

import ossobook.Messages;

/**
 * Global options for OssoBook.
 * 
 * @author ali
 * @author fnuecke
 */
public class Configuration {
	
	/**
	 * Main OssoBook client settings.
	 * 
	 * <p>
	 * This includes all settings read from the <code>ossobook.config</code>
	 * file, e.g. data regarding the global and local database servers
	 * (hostname, database name), as well as data on the mail server to use when
	 * sending conflict mails (during synchronization) and some other
	 * synchronization related settings.
	 */
	public static final Properties config = new Properties();
	
	/**
	 * Directory containing further configuration files.
	 */
	public static String CONFIG_DIR;
	
	/**
	 * Directory containing graphics.
	 */
	public static String IMAGES_DIR;
	
	// those "configurable window sizes" are definitely NOT cool. remove them without breaking anything.
	
	/** Windowsizes */
	public static int notizfeld_x = 300;
	public static int notizfeld_y = 300;
	public static int durchsuchealleframe_x = 800;
	public static int durchsuchealleframe_y = 300;
	public static int massfenster_x = 500;
	public static int massfenster_y = 400;
	public static int projektauswahlfenster_x = 400;
	public static int projektauswahlfenster_y = 400;
	public static int projektbasisfenster_x = 900;
	public static int projektbasisfenster_y = 650;
	public static int projektveraenderungsfenster_x = 400;
	public static int projektveraenderungsfenster_y = 400;
	public static int ossobookgrundschablone_x = 400;
	public static int ossobookgrundschablone_y = 400;
	/** Fieldsizes */
	public static int gewoehnlichestextfeld_x = 50;
	public static int gewoehnlichestextfeld_y = 30;
	public static int selbstkorrekturfeld_x = 50;
	public static int selbstkorrekturfeld_y = 35;
	/** Fontsizes */
	public static int codeTitelFont = 9;
	public static int codeFont = 9;
	public static final int projektbasisfensterBorderRight = 200;

	/**
	 * Initializes the configuration by loading the configuration files.
	 */
	public static void init() {
		
		System.out.println("Reading 'ossobook.default.config' and 'ossobook.config' file."); //$NON-NLS-1$
		
		// Try reading in the configuration file. If it fails, log that fact.
		File f = new File("ossobook.default.config"); //$NON-NLS-1$
		try {
			FileInputStream inputStream = new FileInputStream(f);
			try {
				config.load(inputStream);
			} catch (IOException e) {
				failConfig();
			} finally {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e) {
			failConfig();
		}

		f = new File("ossobook.config"); //$NON-NLS-1$
		try {
			if (f.exists()) {
				FileInputStream inputStream = new FileInputStream(f);
				try {
					config.load(inputStream);
				} catch (IOException e) {
					System.out.println(Messages.getString("Configuration.4")); //$NON-NLS-1$
				} finally {
					try {
						inputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(String.format(Messages.getString("Configuration.3"), "ossobook.config")); //$NON-NLS-1$ //$NON-NLS-2$
		}

		f = new File("ossobook.user.config"); //$NON-NLS-1$
		try {
			if (f.exists()) {
				FileInputStream inputStream = new FileInputStream(f);
				try {
					config.load(inputStream);
				} catch (IOException e) {
					System.out.println("Problem reading file: ossobook.user.config, failed reading user configuration. Only using values from 'ossobook.default.config' and 'ossobook.config'."); //$NON-NLS-1$
				} finally {
					try {
						inputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(String.format(Messages.getString("Configuration.3"), "ossobook.user.config")); //$NON-NLS-1$ //$NON-NLS-2$
		}
		
		// Try reading the configuration and images directory from the
		// loaded settings.
		CONFIG_DIR = config.getProperty("ossobook.config.dir"); //$NON-NLS-1$
        IMAGES_DIR = config.getProperty("ossobook.images.dir"); //$NON-NLS-1$
        
        // Set proper logging configuration.
		PropertyConfigurator.configure(CONFIG_DIR + "logging.config"); //$NON-NLS-1$
		// Then get the logger.
		Log log = LogFactory.getLog(Configuration.class);
        
		// OK, final sysout.
		System.out.println("Successfully parsed settings, switching to configured logging."); //$NON-NLS-1$
		
		// Read display configuration
		try {
			log.info(Messages.getString("Configuration.5", CONFIG_DIR + "display.config"));  //$NON-NLS-1$//$NON-NLS-2$
			Properties displayConfig = new Properties();
			displayConfig.load(new FileInputStream(new File(CONFIG_DIR + "display.config"))); //$NON-NLS-1$
			
			notizfeld_x = Integer.parseInt(displayConfig.getProperty("notefield.x", String.valueOf(notizfeld_x))); //$NON-NLS-1$
			notizfeld_y = Integer.parseInt(displayConfig.getProperty("notefield.y", String.valueOf(notizfeld_y))); //$NON-NLS-1$
			durchsuchealleframe_x = Integer.parseInt(displayConfig.getProperty("search.all.frame.x", String.valueOf(durchsuchealleframe_x))); //$NON-NLS-1$
			durchsuchealleframe_y = Integer.parseInt(displayConfig.getProperty("search.all.frame.y", String.valueOf(durchsuchealleframe_y))); //$NON-NLS-1$
			massfenster_x = Integer.parseInt(displayConfig.getProperty("measure.window.x", String.valueOf(massfenster_x))); //$NON-NLS-1$
			massfenster_y = Integer.parseInt(displayConfig.getProperty("measure.window.y", String.valueOf(massfenster_y))); //$NON-NLS-1$
			projektauswahlfenster_x = Integer.parseInt(displayConfig.getProperty("project.selection.window.x", String.valueOf(projektauswahlfenster_x))); //$NON-NLS-1$
			projektauswahlfenster_y = Integer.parseInt(displayConfig.getProperty("project.selection.window.y", String.valueOf(projektauswahlfenster_y))); //$NON-NLS-1$
			projektbasisfenster_x = Integer.parseInt(displayConfig.getProperty("project.base.window.x", String.valueOf(projektbasisfenster_x))); //$NON-NLS-1$
			projektbasisfenster_y = Integer.parseInt(displayConfig.getProperty("project.base.window.y", String.valueOf(projektbasisfenster_y))); //$NON-NLS-1$
			projektveraenderungsfenster_x = Integer.parseInt(displayConfig.getProperty("project.details.window.x", String.valueOf(projektveraenderungsfenster_x))); //$NON-NLS-1$
			projektveraenderungsfenster_y = Integer.parseInt(displayConfig.getProperty("project.details.window.y", String.valueOf(projektveraenderungsfenster_y))); //$NON-NLS-1$
			ossobookgrundschablone_x = Integer.parseInt(displayConfig.getProperty("ossobook.base.scheme.window.x", String.valueOf(ossobookgrundschablone_x))); //$NON-NLS-1$
			ossobookgrundschablone_y = Integer.parseInt(displayConfig.getProperty("ossobook.base.scheme.window.y", String.valueOf(ossobookgrundschablone_y))); //$NON-NLS-1$
			gewoehnlichestextfeld_x = Integer.parseInt(displayConfig.getProperty("simple.textfield.x", String.valueOf(gewoehnlichestextfeld_x))); //$NON-NLS-1$
			gewoehnlichestextfeld_y = Integer.parseInt(displayConfig.getProperty("simple.textfield.y", String.valueOf(gewoehnlichestextfeld_y))); //$NON-NLS-1$
			selbstkorrekturfeld_x = Integer.parseInt(displayConfig.getProperty("selfcorrection.textfield.x", String.valueOf(selbstkorrekturfeld_x))); //$NON-NLS-1$
			selbstkorrekturfeld_y = Integer.parseInt(displayConfig.getProperty("selfcorrection.textfield.y", String.valueOf(selbstkorrekturfeld_y))); //$NON-NLS-1$
			codeTitelFont = Integer.parseInt(displayConfig.getProperty("code.title.font", String.valueOf(codeTitelFont))); //$NON-NLS-1$
			codeFont = Integer.parseInt(displayConfig.getProperty("code.font", String.valueOf(codeFont))); //$NON-NLS-1$
		} catch (FileNotFoundException e) {
			log.debug(Messages.getString("Configuration.6")); //$NON-NLS-1$
		} catch (IOException e) {
			log.debug(Messages.getString("Configuration.7")); //$NON-NLS-1$
		}
		
		// Add shutdown hook that will save the settings.
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					config.store(new BufferedOutputStream(new FileOutputStream("ossobook.user.config")), "Do not change this file, it will be overwritten each time the client is exited. Use the 'ossobook.config' file, instead."); //$NON-NLS-1$ //$NON-NLS-2$
				} catch (FileNotFoundException ignored) {
				} catch (IOException ignored) {
				}
			}
		}));
	}

	/**
	 * Failed loading default settings, recommend user to abort program start.
	 */
	private static void failConfig() {
		if (JOptionPane.NO_OPTION == JOptionPane.showConfirmDialog(null,
				Messages.getString("Configuration.0"), //$NON-NLS-1$
				Messages.getString("Configuration.1"), //$NON-NLS-1$
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE))
		{
			System.exit(0);
		}
	}
}