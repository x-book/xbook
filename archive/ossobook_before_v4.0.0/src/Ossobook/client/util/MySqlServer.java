package ossobook.client.util;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.Messages;

/**
 * Utility class used to start and stop the standalone MySQL server that comes
 * with the client.
 * 
 * @author fnuecke
 */
public final class MySqlServer {

	/**
	 * Logging...
	 */
	private static final Log log = LogFactory.getLog(MySqlServer.class);
	
	/**
	 * No need for an instance.
	 */
	private MySqlServer() {
	}
	
	/**
	 * Start the MySQL server if possible.
	 */
	public static void init() {
		try {
			String startTmp = "mysqld.startcmd"; //$NON-NLS-1$
			String stopTmp = "mysqld.stopcmd"; //$NON-NLS-1$
			String osName = System.getProperty("os.name").toLowerCase(); //$NON-NLS-1$
			if (osName.contains("windows")) { //$NON-NLS-1$
				startTmp += ".win"; //$NON-NLS-1$
				stopTmp += ".win"; //$NON-NLS-1$
			} else if (osName.contains("mac")) { //$NON-NLS-1$
				startTmp += ".mac"; //$NON-NLS-1$
				stopTmp += ".mac"; //$NON-NLS-1$
			} else if (osName.contains("linux")) { //$NON-NLS-1$
				startTmp += ".unix"; //$NON-NLS-1$
				stopTmp += ".unix"; //$NON-NLS-1$
			} else {
				log.warn(Messages.getString("MySqlServer.5")); //$NON-NLS-1$
				return;
			}
			final String startCmd = Configuration.config.getProperty(startTmp);
			final String stopCmd = Configuration.config.getProperty(stopTmp);
			
			if (startCmd == null || startCmd.length() == 0 ||
					stopCmd == null || stopCmd.length() == 0)
			{
				log.warn(Messages.getString("MySqlServer.6")); //$NON-NLS-1$
				return;
			}
			
			log.info(Messages.getString("MySqlServer.0", startCmd)); //$NON-NLS-1$
			Runtime.getRuntime().exec(startCmd);
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						log.info(Messages.getString("MySqlServer.1", stopCmd)); //$NON-NLS-1$
						// Run the stop command with a timeout of 5 seconds.
						Timeout.timeout(Runtime.getRuntime().exec(stopCmd), 5000);
					} catch (IOException e) {
						log.error(Messages.getString("MySqlServer.2", e.getMessage())); //$NON-NLS-1$
					}
				}
			}));
		} catch (IOException e) {
			log.error(Messages.getString("MySqlServer.3", e.getMessage())); //$NON-NLS-1$
			
			// Set local connections to disabled.
			log.info(Messages.getString("MySqlServer.4")); //$NON-NLS-1$
			Configuration.config.setProperty("local.enabled", "false"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}
	
	/**
	 * Utility class for enforcing a timeout on a process.
	 */
	private final static class Timeout {
		
		/**
		 * Waits for the given time to wait for the process to finish, else
		 * aborts it forcibly.
		 * 
		 * <p>
		 * This method blocks until the process finishes one way or another.
		 * </p>
		 * 
		 * @param process
		 * @param time
		 */
		public static void timeout(final Process process, long time) {
			// Prepare 'timer' thread.
			Thread timeoutThread = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						process.waitFor();
					} catch (InterruptedException ignored) {
					}
				}
			});
			timeoutThread.start();
			try {
				// Wait for the thread to finish for the given amount of time.
				// If the thread does finish soon, the process is already done,
				// else it'll be killed forcibly (in the finally block).
				timeoutThread.join(time);
			} catch (InterruptedException e) {
				// To kill the stopper thread.
				timeoutThread.interrupt();
				// Because of the join.
				Thread.currentThread().interrupt();
			} finally {
				// Kill the child process if it's still running.
				process.destroy();
			}
		}
	}
}
