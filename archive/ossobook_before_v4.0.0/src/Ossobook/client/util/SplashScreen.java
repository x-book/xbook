/**
 * Splash screen showing the OssoBook logo while the actual GUI loads.
 * 
 * @author ali
 * @author fnuecke
 */
package ossobook.client.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;


public class SplashScreen {
	private SplashScreen() {
	}
	
	/**
	 * Opens a new splashscreen.
	 */
	public static void show() {
		// Get the image.
		final ImageIcon image = new ImageIcon(Configuration.IMAGES_DIR + "ossobook.jpg"); //$NON-NLS-1$
		// Create an extra thread to create a frame and display the image.
		new Thread(new Runnable() {
			@Override
			public void run() {
				final JFrame frame = new JFrame();
				frame.setAlwaysOnTop(true);
				frame.setUndecorated(true);
				frame.setSize(image.getIconWidth(), image.getIconHeight());
				frame.setContentPane(new JLabel(image));
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				
				final Timer timer = new Timer(1500, null);
				ActionListener listener = new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						timer.stop();
						frame.dispose();
					}
				};
				timer.addActionListener(listener);
				timer.start();
			}
		}).start();
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException ignored) {
		}
	}
}