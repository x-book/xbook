package ossobook.client.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.io.database.ConnectionType;
import ossobook.client.io.database.DatabaseConnection;

/**
 * Utility class that checks if the client has been "installed" on the local
 * machine by connecting to the local database (if the default one is used,
 * otherwise the user is responsible for the database setup herself).
 * 
 * <p>
 * If the connection succeeds, we look for a row in the .version table. If no
 * such row exists, we ask the user to the path of the "setup SQL file",
 * containing the queries necessary to personalize a template database.
 * </p>
 * 
 * @author fnuecke
 */
public final class DatabaseInitializer {
	
	/**
	 * Logging...
	 */
	private static final Log log = LogFactory.getLog(DatabaseInitializer.class);
	
	/**
	 * No instantiation needed.
	 */
	private DatabaseInitializer() {
	}
	
	/**
	 * Initializes and executes the installer if necessary.
	 * 
	 * <p>
	 * Creates a connection to the local database and checks if there is an
	 * entry in the <code>version</code> table. If there isn't the database is
	 * assumed to be not initialized and the installing process is started by
	 * calling the {@link #install(Connection, String)} function.
	 * </p>
	 */
	public static void init() {
		if (!"true".equals(Configuration.config.getProperty("installer.enabled"))) { //$NON-NLS-1$ //$NON-NLS-2$
			return;
		}
		// Wait a bit to allow startup of MySQL server.
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ignore) {
		}
		
		Connection connection = null;
		try {
			// Make connection to local database.
			DatabaseConnection databaseConnection = 
				new DatabaseConnection("root", null, //$NON-NLS-1$
						ConnectionType.CONNECTION_LOCAL);
			// Get actual connection to perform query.
			connection = databaseConnection.getUnderlyingConnection();
			// Check if there's an entry in the version table.
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM " + //$NON-NLS-1$
					databaseConnection.getDatabase() + ".version"); //$NON-NLS-1$
			if (rs.next()) {
				int count = rs.getInt(1);
				try {
					stmt.close();
				} catch (SQLException e) {
					log.warn(Messages.getString("DatabaseInitializer.3")); //$NON-NLS-1$
				}
				if (count == 0) {
					// Not yet installed.
					log.info(Messages.getString("DatabaseInitializer.4")); //$NON-NLS-1$
					install(connection, databaseConnection.getDatabase());
				}
			}
		} catch (SQLException e) {
			log.warn(Messages.getString("DatabaseInitializer.5")); //$NON-NLS-1$
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e2) {
					log.warn(Messages.getString("DatabaseInitializer.6")); //$NON-NLS-1$
				}
			}
		}
		
		// Check if the forwarder key is set.
		String currentKey = Configuration.config.getProperty("forwarder.0.authkey"); //$NON-NLS-1$
		if (currentKey == null || currentKey.isEmpty()) {
			// Forwarder key not set.
			String result = JOptionPane.showInputDialog(null, Messages.getString("DatabaseInitializer.2"), //$NON-NLS-1$
					Messages.getString("DatabaseInitializer.9"), //$NON-NLS-1$
					JOptionPane.QUESTION_MESSAGE);
			if (result != null && !result.isEmpty()) {
				Configuration.config.setProperty("forwarder.0.authkey", result); //$NON-NLS-1$
				Configuration.config.setProperty("forwarder.1.authkey", result); //$NON-NLS-1$
			}
		}
	}
	
	/**
	 * Perform "installation".
	 * 
	 * <p>
	 * Asks the user if she wishes to initialize the database now, and if so
	 * asks for the setup file to use. The file is assumed to contain one SQL
	 * query per line. If any one of the queries fails, the setup fails.
	 * </p>
	 * 
	 * <p>
	 * The queries will be taken as is, except that the string
	 * <code>{{DATABASE}}</code> will be replaced with the locally configured
	 * database name (from the config file).
	 * </p>
	 * 
	 * @param connection
	 *            the connection to the local database to use for executing the
	 *            setup queries.
	 * @param database
	 */
	private static void install(Connection connection, String database) {
		if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
				Messages.getString("DatabaseInitializer.7"), Messages.getString("DatabaseInitializer.8"), JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE)) //$NON-NLS-1$ //$NON-NLS-2$
		{
			JFileChooser fc = new JFileChooser();
			fc.setFileFilter(new FileNameExtensionFilter(Messages.getString("DatabaseInitializer.0"), "sql"));  //$NON-NLS-1$//$NON-NLS-2$
			
			if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				Savepoint savepoint = null;
				try {
					// Begin transaction and set savepoint.
					connection.setAutoCommit(false);
					savepoint = connection.setSavepoint();
					
					// Read every line from the file and treat it as a query.
					try {
						BufferedReader reader = new BufferedReader(new FileReader(fc.getSelectedFile()));
						String line;
						while ((line = reader.readLine()) != null) {
							Statement stmt = connection.createStatement();
							stmt.execute(line.replace("{{{DATABASE}}}", database). //$NON-NLS-1$
									replace("{{{CLIENTVERSION}}}", Configuration.config.getProperty("ossobook.version"))); //$NON-NLS-1$ //$NON-NLS-2$
							stmt.close();
						}
						
						// Done, commit changes.
						connection.commit();
						connection.releaseSavepoint(savepoint);
						savepoint = null;
						
						log.info(Messages.getString("DatabaseInitializer.12")); //$NON-NLS-1$
						
						JOptionPane.showMessageDialog(null, Messages.getString("DatabaseInitializer.13"), Messages.getString("DatabaseInitializer.8"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
						
					} catch (FileNotFoundException e) {
						String message = Messages.getString("DatabaseInitializer.15"); //$NON-NLS-1$
						log.error(message);
						JOptionPane.showMessageDialog(null, message, Messages.getString("DatabaseInitializer.16"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
					} catch (IOException e) {
						String message = Messages.getString("DatabaseInitializer.17"); //$NON-NLS-1$
						log.error(message);
						JOptionPane.showMessageDialog(null, message, Messages.getString("DatabaseInitializer.16"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
					} finally {
						if (savepoint != null) {
							// Savepoint was not released and nulled -> error.
							connection.rollback(savepoint);
							savepoint = null;
						}
						connection.setAutoCommit(true);
					}
				} catch (SQLException e1) {
					String message = Messages.getString("DatabaseInitializer.19", e1.getMessage()); //$NON-NLS-1$
					log.error(message);
					JOptionPane.showMessageDialog(null, message, Messages.getString("DatabaseInitializer.16"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
					if (savepoint != null) {
						// Savepoint was not released and nulled -> error.
						try {
							connection.rollback(savepoint);
						} catch (SQLException e) {
							log.error(Messages.getString("DatabaseInitializer.21")); //$NON-NLS-1$
						}
					}
				}
			}
		}
	}
}
