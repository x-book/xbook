package ossobook.client.base.scheme;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;

import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.base.fields.FieldSetArtefacts;
import ossobook.client.gui.common.OssobookFrame;
import ossobook.client.util.Configuration;

/**
 * Template for artefact Screen
 * 
 * @author ali
 */
@SuppressWarnings("serial")
public class SchemeArtefacts extends SchemeBase {

    /**
	 * Constructs Object
	 */
	public SchemeArtefacts(OssobookFrame mainframe) {
		super(Messages.getString("SchemeArtefacts.0")); //$NON-NLS-1$
		setFields(new FieldSetArtefacts(mainframe).getNewBasicFields());
		frameSettings();
		addFieldsToPanel();
		getMainPanel().add(createDefaultButton());
		try {
			loadDefault();
		} catch (Exception e) {
			LogFactory.getLog(SchemeArtefacts.class).error(e, e);
		}
	}

	private JButton createDefaultButton() {
        JButton setDefaultBtn = new JButton(Messages.getString("SchemeArtefacts.1"));
		setDefaultBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveTemplate(new File(Configuration.CONFIG_DIR
						+ "defaultArtefaktTemplate.txt")); //$NON-NLS-1$
			}
		});
		return setDefaultBtn;
	}

	private void loadDefault() {
		loadTemplate(new File(Configuration.CONFIG_DIR
				+ "defaultArtefaktTemplate.txt")); //$NON-NLS-1$
	}

	/**
     * 
     */
	public void loadMaxTemplate() {
		// verhindere \u00FCberschreiben des aktuellen templates f\u00FCr update
		File preserve = getAktuellesTemplate();
		File f = new File(Configuration.CONFIG_DIR
				+ "maximumArtefaktTemp.txt"); //$NON-NLS-1$
		loadTemplate(f);
		updateAction();
		setAktuellesTemplate(preserve);
	}

	/**
     * 
     */
	public void loadActiveTemplate() {
		loadTemplate(getAktuellesTemplate());
		updateAction();
	}
}