package ossobook.client.base.scheme;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;

import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.base.BaseFacade;
import ossobook.client.base.fields.FieldSetBones;
import ossobook.client.base.metainfo.Project;
import ossobook.client.gui.common.OssobookFrame;
import ossobook.client.util.Configuration;

/**
 * Template for MainScreen.
 * 
 * @author ali
 */
@SuppressWarnings("serial")
public class SchemeBones extends SchemeBase {

    /**
	 * Constructs BoneTemplate
	 */
	public SchemeBones(OssobookFrame mainframe, BaseFacade fasade,
			Project project) {
		super(Messages.getString("SchemeBones.0")); //$NON-NLS-1$
		setFields(new FieldSetBones(mainframe, fasade, project)
				.getNewBasicFields());
		frameSettings();
		addFieldsToPanel();
		getMainPanel().add(createDefaultButton());
		try {
			loadDefault();
		} catch (Exception e) {
			LogFactory.getLog(SchemeBones.class).error(e, e);
		}
	}

	/**
     *  
     */
	private JButton createDefaultButton() {
        JButton setDefaultBtn = new JButton(Messages.getString("SchemeBones.1"));
		setDefaultBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveTemplate(new File(Configuration.CONFIG_DIR
						+ "defaultKnochenTemplate.txt")); //$NON-NLS-1$
			}
		});
		return setDefaultBtn;
	}

	/**
     *  
     */
	private void loadDefault() {
		loadTemplate(new File(Configuration.CONFIG_DIR
				+ "defaultKnochenTemplate.txt")); //$NON-NLS-1$
	}

	/**
     * 
     */
	public void loadMaxTemplate() {
		// verhindere \u00FCberschreiben des aktuellen templates f\u00FCr update
		File preserve = getAktuellesTemplate();
		File f = new File(Configuration.CONFIG_DIR
				+ "maximumKnochenTemp.txt"); //$NON-NLS-1$
		loadTemplate(f);
		updateAction();
		setAktuellesTemplate(preserve);
	}

	/**
     * 
     */
	public void loadActiveTemplate() {
		loadTemplate(getAktuellesTemplate());
		updateAction();
	}

}
