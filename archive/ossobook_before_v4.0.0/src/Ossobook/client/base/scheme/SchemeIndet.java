package ossobook.client.base.scheme;

import ossobook.Messages;

/**
 * Template for Indet Case. If a Bone is indeterminable these Fields should be
 * filled in. Because indet fields are often a reduction of the hole fielset,
 * work should be saved by this template.
 * 
 * @author ali
 */
@SuppressWarnings("serial")
public class SchemeIndet extends SchemeBase {

	/**
     */
	public SchemeIndet() {
		super(Messages.getString("SchemeIndet.0")); //$NON-NLS-1$
	}
}