package ossobook.client.base.scheme;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.gui.update.components.window.ProjektFenster;
import ossobook.client.io.database.modell.BasisFeldJava2Db;
import ossobook.client.util.Configuration;

/**
 * Basic Class for all the Templates.
 * 
 * @author ali
 * 
 */
@SuppressWarnings("serial")
public class SchemeBase extends JInternalFrame {
	private File aktuellesTemplate;

	private BasisFeldJava2Db[] fields;

	private JPanel mainPanel;

	private ProjektFenster parentRef;

	private String templateName = "Default"; //$NON-NLS-1$

	SchemeBase(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * 
	 */
	void addFieldsToPanel() {
		for (BasisFeldJava2Db field : fields) {
			mainPanel.add(field.getCheckBoxSwitch());
		}
		JButton confirm = new JButton(Messages.getString("SchemeBase.4"));
		confirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeAction();
			}
		});
		JButton saveButton = new JButton(Messages.getString("SchemeBase.5"));
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveTemplate();
			}
		});
		JButton loadButton = new JButton(Messages.getString("SchemeBase.6"));
		loadButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadTemplate();
			}
		});

		mainPanel.add(confirm);
		mainPanel.add(saveButton);
		mainPanel.add(loadButton);
	}

	private void closeAction() {
		updateAction();
		try {
			parentRef.setSelected(true);
			parentRef.toFront();
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		dispose();
	}

	/**
	 * 
	 * Display properties of the Frame.
	 */
	void frameSettings() {
		this.setSize(Configuration.ossobookgrundschablone_x,
				Configuration.ossobookgrundschablone_y);
		setTitle(templateName);
		mainPanel = new JPanel();
		mainPanel.setBackground(Color.white);
		mainPanel.setLayout(new GridLayout((fields.length / 2), 2));
		setContentPane(mainPanel);
		setVisible(true);
	}

	File getAktuellesTemplate() {
		return aktuellesTemplate;
	}

	/**
	 * @return Returns the fields.
	 */
	public BasisFeldJava2Db[] getFields() {
		return fields;
	}

	JPanel getMainPanel() {
		return mainPanel;
	}

	/**
	 * The Mask is returning the isSelected(), !isSelected() values of the field
	 * as boolean[].
	 */
	boolean[] getMask() {
		boolean[] b = new boolean[fields.length];
		for (int i = 0; i < b.length; i++) {
			b[i] = fields[i].getCheckBoxSwitch().isSelected();
		}
		return b;
	}

	/**
	 * 
	 */
	void loadTemplate() {
		File templateFile = null;
		try {
			JFileChooser fileChooser = new JFileChooser();
			parentRef.add(fileChooser);
			int returnVal = fileChooser.showOpenDialog(parentRef
					.getDesktopPane());
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				templateFile = new File(fileChooser.getSelectedFile()
						.getAbsolutePath());
			}
			loadTemplate(templateFile);
		} catch (Exception e) {
			LogFactory.getLog(SchemeArtefacts.class).error("Problem in Function loadTemplate()."); //$NON-NLS-1$
		}
	}

	/**
	 * if closing button is pressed, the fields will be updated in
	 * ossobook.framework.JIntFrameBoneProject.
	 * 
	 * @see ossobook.client.gui.update.components.window.ProjektFenster
	 * 
	 */
	void loadTemplate(File templateFile) {
		LogFactory.getLog(SchemeArtefacts.class).info("loading template-file: " + templateFile.getPath()); //$NON-NLS-1$
		
		// Lese Information aus File
		String s = ""; //$NON-NLS-1$
		try {
			FileReader fread = new FileReader(templateFile);
			BufferedReader bread = new BufferedReader(fread);
			while (bread.ready()) {
				s = s + bread.readLine();
			}
			aktuellesTemplate = templateFile;
		} catch (Exception e) {
			LogFactory.getLog(SchemeArtefacts.class).error(e, e);
		}
		// Setzte Information in boolean[] Maske um
		boolean[] barr = new boolean[s.length()];
		for (int i = 0; i < s.length(); i++) {
            barr[i] = s.charAt(i) != '0';
		}
		setMask(barr);
	}

	void saveTemplate() {
		File templateFile = null;
		try {
			JFileChooser fc = new JFileChooser();
			parentRef.add(fc);
			int returnVal = fc.showSaveDialog(parentRef);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				templateFile = new File(fc.getSelectedFile().getAbsolutePath());
			}
			saveTemplate(templateFile);
		} catch (Exception e) {
			LogFactory.getLog(SchemeArtefacts.class).error("Error locating file for saveTemplate() function."); //$NON-NLS-1$
		}
	}

	void saveTemplate(File templateFile) {
		try {
			FileWriter fw = new FileWriter(templateFile);
			BufferedWriter bw = new BufferedWriter(fw);

			boolean[] barr = getMask();
			for (boolean element : barr) {
				if (element) {
					bw.write("1"); //$NON-NLS-1$
				} else {
					bw.write("0"); //$NON-NLS-1$
				}
			}
			bw.close();
			fw.close();
			LogFactory.getLog(SchemeArtefacts.class).info("Template-file: " + templateFile.getPath() //$NON-NLS-1$
					+ " saved."); //$NON-NLS-1$
		} catch (Exception e) {
			LogFactory.getLog(SchemeArtefacts.class).error("Problem saving file."); //$NON-NLS-1$
		}
	}

	void setAktuellesTemplate(File aktuellesTemplate) {
		this.aktuellesTemplate = aktuellesTemplate;
	}

	/**
	 * @param fields
	 *            The fields to set.
	 */
	void setFields(BasisFeldJava2Db[] fields) {
		this.fields = fields;
	}

	/**
	 * Sets a bitmask for checkbox properties.
	 */
	void setMask(boolean[] mask) {
		for (int i = 0; i < fields.length; i++) {
			fields[i].getCheckBoxSwitch().setSelected(mask[i]);
		}
	}

	public void setParent(ProjektFenster parent) {
		parentRef = parent;
	}

	void updateAction() {
		parentRef.updateInputScreen();
	}
}