package ossobook.client.base;

import ossobook.queries.QueryManager;

public class BaseFacade {

	private final QueryManager manager;

	public BaseFacade(QueryManager manager) {
		this.manager = manager;
	}

	public QueryManager getManager() {
		return manager;
	}

}
