package ossobook.client.base.fields;

import java.util.Vector;

import ossobook.Messages;
import ossobook.client.base.BaseFacade;
import ossobook.client.base.metainfo.Project;
import ossobook.client.gui.common.OssobookFrame;
import ossobook.client.gui.update.components.other.GewoehnlichesSelbstkorrekturTextFeld;
import ossobook.client.gui.update.elements.inputfields.Alter1KodeFeld;
import ossobook.client.gui.update.elements.inputfields.Alter2KodeFeld;
import ossobook.client.gui.update.elements.inputfields.Alter3KodeFeld;
import ossobook.client.gui.update.elements.inputfields.Alter4KodeFeld;
import ossobook.client.gui.update.elements.inputfields.Alter5KodeFeld;
import ossobook.client.gui.update.elements.inputfields.AnzahlFeld;
import ossobook.client.gui.update.elements.inputfields.BrandspurFeld;
import ossobook.client.gui.update.elements.inputfields.Bruchkanten2Feld;
import ossobook.client.gui.update.elements.inputfields.BruchkantenFeld;
import ossobook.client.gui.update.elements.inputfields.ErhaltungFeld;
import ossobook.client.gui.update.elements.inputfields.FettFeld;
import ossobook.client.gui.update.elements.inputfields.FundKomplexFeld;
import ossobook.client.gui.update.elements.inputfields.GeschlechtsFeld;
import ossobook.client.gui.update.elements.inputfields.GewichtFeld;
import ossobook.client.gui.update.elements.inputfields.IndividuumFeld;
import ossobook.client.gui.update.elements.inputfields.InventarNummerFeld;
import ossobook.client.gui.update.elements.inputfields.KnochenTeilKodeFeld;
import ossobook.client.gui.update.elements.inputfields.KoerperSeiteFeld;
import ossobook.client.gui.update.elements.inputfields.KoordinatenFeld;
import ossobook.client.gui.update.elements.inputfields.PatinaFeld;
import ossobook.client.gui.update.elements.inputfields.Schlachtspur1Feld;
import ossobook.client.gui.update.elements.inputfields.Schlachtspur2Feld;
import ossobook.client.gui.update.elements.inputfields.SkelKodeFeld;
import ossobook.client.gui.update.elements.inputfields.SketeillNummerFeld;
import ossobook.client.gui.update.elements.inputfields.TierartKodeFeld;
import ossobook.client.gui.update.elements.inputfields.VerbissFeld;
import ossobook.client.gui.update.elements.inputfields.VerdautFeld;
import ossobook.client.gui.update.elements.inputfields.VersinterungFeld;
import ossobook.client.gui.update.elements.inputfields.WurzelfrassFeld;
import ossobook.client.gui.update.elements.other.MassKnopf;
import ossobook.client.gui.update.elements.other.NotizButton;
import ossobook.client.io.database.modell.BasisFeldJava2Db;
import ossobook.client.io.database.modell.Kategorie;

/**
 * The Set of Fields designed for the Main-Screen.
 * 
 * @author ali
 */
public class FieldSetBones {

	private final BaseFacade fasade;

	private final OssobookFrame mainframe;

	private Vector<BasisFeldJava2Db> v; // beinhaltet alle BasicFields

	private final Project project;

	public FieldSetBones(OssobookFrame mainframe, BaseFacade fasade,
			Project project) {
		this.project = project;
		this.fasade = fasade;
		this.mainframe = mainframe;
		produceFields();
	}

	/**
	 * @return the defined Array of BasicField[]
	 */
	public BasisFeldJava2Db[] getNewBasicFields() {
		BasisFeldJava2Db[] bf = new BasisFeldJava2Db[v.size()];
		for (int i = 0; i < v.size(); i++) {
			bf[i] = v.get(i);
		}
		return bf;
	}

	/**
	 * In this Method the Fields to be hold by the Set are defined.
	 * 
	 * @see ossobook.client.io.database.modell.BasisFeldJava2Db
	 */
	private void produceFields() {
		v = new Vector<BasisFeldJava2Db>();
		// KNOCHENLOKALISIERUNG u. IDENTIFIKATION
		Kategorie kat = new Kategorie(Kategorie.IDENTIFIKATION);
		v.add(new BasisFeldJava2Db(new InventarNummerFeld(
        ), Messages.getString("FieldSetBones.0"), 1, null, "inventarNr", kat //$NON-NLS-1$ //$NON-NLS-2$
				.getColor(), false, false));
		v.add(new BasisFeldJava2Db(new FundKomplexFeld(fasade.getManager(),
				project), Messages.getString("FieldSetBones.2"), 1, null, "fK", kat.getColor(), //$NON-NLS-1$ //$NON-NLS-2$
								   true, true));
		v
				.add(new BasisFeldJava2Db(new KoordinatenFeld(),
						Messages.getString("FieldSetBones.4"), 1, null, "xAchse",  //$NON-NLS-1$ //$NON-NLS-2$
						kat.getColor(), false, false));
		v
				.add(new BasisFeldJava2Db(new KoordinatenFeld(),
						Messages.getString("FieldSetBones.6"), 1, null, "yAchse",  //$NON-NLS-1$ //$NON-NLS-2$
						kat.getColor(), false, false));
		// KNOCHENBESTIMMUNG
		kat = new Kategorie(Kategorie.BESTIMMUNG);
		TierartKodeFeld tiercodefield = new TierartKodeFeld();
		v.add(new BasisFeldJava2Db(tiercodefield, Messages.getString("FieldSetBones.8"), 1, null, //$NON-NLS-1$
				"tierart", kat.getColor(), true, true)); //$NON-NLS-1$
		SkelKodeFeld skelcodefield = new SkelKodeFeld();
		v.add(new BasisFeldJava2Db(skelcodefield, Messages.getString("FieldSetBones.10"), 1, null, //$NON-NLS-1$
				"skelettteil", kat.getColor(), true, true)); //$NON-NLS-1$
		v.add(new BasisFeldJava2Db(new KnochenTeilKodeFeld(skelcodefield),
				Messages.getString("FieldSetBones.12"), 1, null, "knochenteil", kat.getColor(), //$NON-NLS-1$ //$NON-NLS-2$
				true, true));
		v.add(new BasisFeldJava2Db(new Alter1KodeFeld(), Messages.getString("FieldSetBones.14"), 1, //$NON-NLS-1$
				null, "alter1", kat.getColor(), true, true)); //$NON-NLS-1$
		v.add(new BasisFeldJava2Db(new Alter2KodeFeld(), Messages.getString("FieldSetBones.16"), 1, //$NON-NLS-1$
				null, "alter2", kat.getColor(), true, true)); //$NON-NLS-1$
		v
				.add(new BasisFeldJava2Db(new Alter3KodeFeld(), Messages.getString("FieldSetBones.1"), //$NON-NLS-1$
						1, null, "alter3", kat.getColor(),  //$NON-NLS-1$
						false, false));
		v
				.add(new BasisFeldJava2Db(new Alter4KodeFeld(), Messages.getString("FieldSetBones.20"), //$NON-NLS-1$
						1, null, "alter4", kat.getColor(),  //$NON-NLS-1$
						false, false));
		v.add(new BasisFeldJava2Db(new Alter5KodeFeld(), Messages.getString("FieldSetBones.22"), 1, null, //$NON-NLS-1$
				"alter5", kat.getColor(), false, false)); //$NON-NLS-1$
		v.add(new BasisFeldJava2Db(new GeschlechtsFeld(), Messages.getString("FieldSetBones.24"), 2, //$NON-NLS-1$
				null, "geschlecht", kat.getColor(), false, //$NON-NLS-1$
				true));
		v.add(new BasisFeldJava2Db(new KoerperSeiteFeld(), Messages.getString("FieldSetBones.26"), //$NON-NLS-1$
				2, null, "koerperseite", kat.getColor(),  //$NON-NLS-1$
				false, true));
		v.add(new BasisFeldJava2Db(new GewichtFeld(), Messages.getString("FieldSetBones.28"), 2, null, //$NON-NLS-1$
				"gewicht", kat.getColor(), true, true)); //$NON-NLS-1$
		v.add(new BasisFeldJava2Db(new MassKnopf(mainframe, skelcodefield,
				tiercodefield), Messages.getString("FieldSetBones.30"), 2, null, "massID",  //$NON-NLS-1$ //$NON-NLS-2$
				kat.getColor(), true, true));
		v.add(new BasisFeldJava2Db(new NotizButton(Messages.getString("FieldSetBones.32"), mainframe), //$NON-NLS-1$
				Messages.getString("FieldSetBones.33"), 2, null, "massNotiz", kat.getColor(), //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetBones.35"), 2, null, "pathologie", kat.getColor(),  //$NON-NLS-1$ //$NON-NLS-2$
				false, true));

		v.add(new BasisFeldJava2Db(new BruchkantenFeld(), Messages.getString("FieldSetBones.37"), 2, //$NON-NLS-1$
				null, "bruchkante", kat.getColor(), false, //$NON-NLS-1$
				true));

		v.add(new BasisFeldJava2Db(new Bruchkanten2Feld(), Messages.getString("FieldSetBones.39"), 2, //$NON-NLS-1$
				null, "bruchkante2", kat.getColor(), false, //$NON-NLS-1$
				true));
		// KNOCHENERHALTUNG
		kat = new Kategorie(Kategorie.ERHALTUNG);

		v.add(new BasisFeldJava2Db(new ErhaltungFeld(),
				Messages.getString("FieldSetBones.41"), 3, null, "erhaltung", kat //$NON-NLS-1$ //$NON-NLS-2$
						.getColor(), false, true));
		v.add(new BasisFeldJava2Db(new WurzelfrassFeld(), Messages.getString("FieldSetBones.43"), 3, //$NON-NLS-1$
				null, "wurzelfrass", kat.getColor(), false, //$NON-NLS-1$
				true));
		v.add(new BasisFeldJava2Db(new VersinterungFeld(),
				Messages.getString("FieldSetBones.45"), 3, null, "kruste", kat.getColor(), //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new FettFeld(), Messages.getString("FieldSetBones.47"), 3, //$NON-NLS-1$
				null, "fettig", kat.getColor(), false, true)); //$NON-NLS-1$
		v.add(new BasisFeldJava2Db(new PatinaFeld(),
				Messages.getString("FieldSetBones.49"), 3, null, "patina", kat //$NON-NLS-1$ //$NON-NLS-2$
						.getColor(), false, true));
		v.add(new BasisFeldJava2Db(new BrandspurFeld(), Messages.getString("FieldSetBones.51"), 3, null, //$NON-NLS-1$
				"brandspur", kat.getColor(), false, true)); //$NON-NLS-1$
		v.add(new BasisFeldJava2Db(new VerbissFeld(), Messages.getString("FieldSetBones.53"), 3, null, //$NON-NLS-1$
				"verbiss", kat.getColor(), false, true)); //$NON-NLS-1$
		v.add(new BasisFeldJava2Db(new VerdautFeld(), Messages.getString("FieldSetBones.55"), 3, null, //$NON-NLS-1$
				"verdaut", kat.getColor(), false, true)); //$NON-NLS-1$
		v.add(new BasisFeldJava2Db(new Schlachtspur1Feld(), Messages.getString("FieldSetBones.57"), //$NON-NLS-1$
				3, null, "schlachtS1", kat.getColor(), false, //$NON-NLS-1$
				true));
		v.add(new BasisFeldJava2Db(new Schlachtspur2Feld(), Messages.getString("FieldSetBones.59"), //$NON-NLS-1$
				3, null, "schlachtS2", kat.getColor(), false, //$NON-NLS-1$
				true));
		v.add(new BasisFeldJava2Db(new VerdautFeld(), Messages.getString("FieldSetBones.61"), 3, null, //$NON-NLS-1$
				"feuchtboden", kat.getColor(), false, true)); //$NON-NLS-1$
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetBones.63"), 4, null, "groesse1", kat //$NON-NLS-1$ //$NON-NLS-2$
						.getColor(), false, false));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetBones.65"), 4, null, "groesse2", kat //$NON-NLS-1$ //$NON-NLS-2$
						.getColor(), false, false));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetBones.67"), 4, null, "saison", kat.getColor(),  //$NON-NLS-1$ //$NON-NLS-2$
				false, false));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetBones.69"), 4, null, "jahrring", kat.getColor(), //$NON-NLS-1$ //$NON-NLS-2$
				false, false));
		// PASSNUMMERN
		kat = new Kategorie(Kategorie.PASSNUMMERN);
		v
				.add(new BasisFeldJava2Db(
						new SketeillNummerFeld(fasade.getManager(), project),
						Messages.getString("FieldSetBones.71"), //$NON-NLS-1$
						4,
						Messages.getString("FieldSetBones.72"), //$NON-NLS-1$
						"skelNr", kat.getColor(), false, //$NON-NLS-1$
						false));
		v
				.add(new BasisFeldJava2Db(
						new IndividuumFeld(fasade.getManager(), project),
						Messages.getString("FieldSetBones.74"), //$NON-NLS-1$
						4,
						Messages.getString("FieldSetBones.75"), //$NON-NLS-1$
						"knIndNr", kat.getColor(), false, //$NON-NLS-1$
						false));
		v
				.add(new BasisFeldJava2Db(new VerdautFeld(), Messages.getString("FieldSetBones.77"), //$NON-NLS-1$
						4, null, "indivnrsicher", kat.getColor(),  //$NON-NLS-1$
						false, false));

		// BENUTZERDEFINIERTE FELDER
		kat = new Kategorie(Kategorie.BENUTZERDEFINIERT);
		v.add(new BasisFeldJava2Db(new NotizButton(Messages.getString("FieldSetBones.79"), mainframe), //$NON-NLS-1$
				Messages.getString("FieldSetBones.80"), 4, null, "objektNotiz",  //$NON-NLS-1$ //$NON-NLS-2$
				kat.getColor(), false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetBones.82"), 4, null, "zusatzFeld1", kat.getColor(),  //$NON-NLS-1$ //$NON-NLS-2$
				false, false));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetBones.84"), 4, null, "zusatzFeld2", kat.getColor(),  //$NON-NLS-1$ //$NON-NLS-2$
				false, false));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetBones.86"), 4, null, "zusatzFeld3", kat.getColor(),  //$NON-NLS-1$ //$NON-NLS-2$
				false, false));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetBones.88"), 4, null, "zusatzFeld4", kat.getColor(),  //$NON-NLS-1$ //$NON-NLS-2$
				false, false));
		v.add(new BasisFeldJava2Db(new AnzahlFeld(), Messages.getString("FieldSetBones.90"), 4, null, //$NON-NLS-1$
				"anzahl", kat.getColor(), true, true)); //$NON-NLS-1$
	}
}