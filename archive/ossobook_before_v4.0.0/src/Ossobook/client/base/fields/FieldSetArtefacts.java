package ossobook.client.base.fields;

import java.awt.Color;
import java.util.Vector;

import ossobook.Messages;
import ossobook.client.gui.common.OssobookFrame;
import ossobook.client.gui.update.components.other.GewoehnlichesSelbstkorrekturTextFeld;
import ossobook.client.gui.update.elements.inputfields.AfBronzeteilFeld;
import ossobook.client.gui.update.elements.inputfields.AfEinlagenFeld;
import ossobook.client.gui.update.elements.inputfields.AfEisenteilFeld;
import ossobook.client.gui.update.elements.inputfields.AfKompositobjektFeld;
import ossobook.client.gui.update.elements.inputfields.AfUeberschliffenerBruchFeld;
import ossobook.client.gui.update.elements.other.NotizButton;
import ossobook.client.io.database.modell.BasisFeldJava2Db;

/**
 * The Set of Fields designed for the Artefakt-Screen.
 * 
 * @author ali
 */
public class FieldSetArtefacts {

	private final OssobookFrame mainframe;

	private Vector<BasisFeldJava2Db> v;

	public FieldSetArtefacts(OssobookFrame mainframe) {
		this.mainframe = mainframe;
		produceFields();
	}

	/**
	 * @return the defined Array of BasicField[]
	 */
	public BasisFeldJava2Db[] getNewBasicFields() {
		BasisFeldJava2Db[] bf = new BasisFeldJava2Db[v.size()];
		for (int i = 0; i < v.size(); i++) {
			bf[i] = v.get(i);
		}
		return bf;
	}

	/**
	 * In this Method the Fields to be hold by the Set are defined.
	 * 
	 * @see ossobook.client.io.database.modell.BasisFeldJava2Db
	 */
	private void produceFields() {
		v = new Vector<BasisFeldJava2Db>();
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.0"), 1, null, "typ", Color.red, false, //$NON-NLS-1$ //$NON-NLS-2$
				true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.2"), 1, null, "untertyp", Color.red,  //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.4"), 1, null, "verzierung", Color.blue,  //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.6"), 1, null, "herstellungsspur",  //$NON-NLS-1$ //$NON-NLS-2$
				Color.blue, false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.8"), 1, null, "gebrauchsspur", Color.blue, //$NON-NLS-1$ //$NON-NLS-2$
				false, true));

		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.10"), 1, null, "basis", Color.blue,  //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.12"), 1, null, "spitze", Color.blue,  //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.14"), 1, null, "schaeftung", Color.blue, //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.16"), 1, null, "spitzenform", Color.blue,  //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.18"), 1, null, "spitzenquerschnitt",  //$NON-NLS-1$ //$NON-NLS-2$
				Color.blue, false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.20"), 1, null, "meisselform", Color.blue,  //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.22"), 1, null, "meissellaengsschnitt", //$NON-NLS-1$ //$NON-NLS-2$
				Color.blue, false, true));

		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.24"), 1, null, "fragmentierung", Color.blue, //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.26"), 1, null, "faerbung", Color.blue,  //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				Messages.getString("FieldSetArtefacts.28"), 1, null, "inschrift", Color.blue,  //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"M1", 1, Messages.getString("FieldSetArtefacts.31"), "m1", Color.blue, //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"M2", 1, Messages.getString("FieldSetArtefacts.34"), "m2", Color.blue,  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"M3", 1, Messages.getString("FieldSetArtefacts.37"), "m3", Color.blue, //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"M4", 1, Messages.getString("FieldSetArtefacts.40"), "m4", Color.blue, //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				false, true));
		v.add(new BasisFeldJava2Db(new GewoehnlichesSelbstkorrekturTextFeld(),
				"M5", 1, Messages.getString("FieldSetArtefacts.43"), "m5", Color.blue, //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				false, true));
		v.add(new BasisFeldJava2Db(new NotizButton(Messages.getString("FieldSetArtefacts.45"), //$NON-NLS-1$
				mainframe), Messages.getString("FieldSetArtefacts.46"), 1, null, "diverseNotizen",  //$NON-NLS-1$ //$NON-NLS-2$
				Color.blue, false, true));
		v.add(new BasisFeldJava2Db(new AfEisenteilFeld(), Messages.getString("FieldSetArtefacts.48"), //$NON-NLS-1$
				1, null, "eisenteile", Color.blue,  //$NON-NLS-1$
				false, true));
		v.add(new BasisFeldJava2Db(new AfBronzeteilFeld(),
				Messages.getString("FieldSetArtefacts.50"), 1, null, "bronzeteile",  //$NON-NLS-1$ //$NON-NLS-2$
				Color.blue, false, true));
		v.add(new BasisFeldJava2Db(new AfEinlagenFeld(), Messages.getString("FieldSetArtefacts.52"), 1, null, //$NON-NLS-1$
				"einlagen", Color.blue, false, true)); //$NON-NLS-1$
		v.add(new BasisFeldJava2Db(new AfKompositobjektFeld(),
				Messages.getString("FieldSetArtefacts.54"), 1, null, "kompositObjekt", Color.blue, //$NON-NLS-1$ //$NON-NLS-2$
				false, true));
		v.add(new BasisFeldJava2Db(new AfUeberschliffenerBruchFeld(),
				Messages.getString("FieldSetArtefacts.56"), 1, null, "ueberschliffenerBruch", //$NON-NLS-1$ //$NON-NLS-2$
				Color.blue, false, true));
	}
}