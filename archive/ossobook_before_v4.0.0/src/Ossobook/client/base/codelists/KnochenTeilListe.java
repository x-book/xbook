package ossobook.client.base.codelists;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * The CodeList Abstract-Class is the Base for all OssoBook CodeLists. CodeLists
 * are ment to translate given codes into values to be displayes in the GUI. The
 * translation should prevent from Mistakes. e.g.: if an Animal code is given
 * the Codelist returns it's Name.
 * 
 * @see ossobook.client.gui.update.components.window.ProjektFenster
 * @author ali
 * 
 */

public class KnochenTeilListe {
	private static final Log _log = LogFactory.getLog(KnochenTeilListe.class);
	//private final HashMap<String, String> codes;
	private final HashMap<String, String> teile;
	private final String namenColumne = "KnochenTeilName"; //$NON-NLS-1$
	private final String skelCodeColumen = "SkelCode"; //$NON-NLS-1$
	private final String codeColumne = "KnochenTeilCode"; //$NON-NLS-1$

	public KnochenTeilListe(Connection connection) {
		teile = new HashMap<String, String>();
		//codes = new HashMap<String, String>();
        fillHashmap(getList(connection)); //$NON-NLS-1$
	}

	static ResultSet getList(Connection connection) {
		ResultSet result = null;
		try {
			Statement s = connection.createStatement();
			String query = "SELECT * FROM knochenteil WHERE geloescht='N';"; //$NON-NLS-1$
			if (_log.isDebugEnabled()) {
				_log.debug("Query: " + query); //$NON-NLS-1$
			}
			result = s.executeQuery(query);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		return result;
	}

	public String getTeil(Object n) {
		return teile.get(n);
	}

	void fillHashmap(ResultSet rs) {
		try {
			while (rs.next()) {
				String s = rs.getString(namenColumne);
				Integer i = rs.getInt(codeColumne);
				Integer j = rs.getInt(skelCodeColumen);
				teile.put(i + "," + j, s); //$NON-NLS-1$
				//codes.put(s, i + "," + j); //$NON-NLS-1$

			}
		} catch (SQLException e) {
			LogFactory.getLog(KnochenTeilListe.class).error(e, e);
		}
	}
}