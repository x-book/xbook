package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;

/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class Alter3Liste extends KodierungsListe {

	public Alter3Liste(Connection connection) {
		tabellenname = "alter3"; //$NON-NLS-1$
		namenColumne = "Alter3Name"; //$NON-NLS-1$
		codeColumne = "Alter3Code"; //$NON-NLS-1$
		teile = new HashMap<Integer, String>();
		codes = new HashMap<String, Integer>();
		fillHashmap(getList(connection, tabellenname));
	}

}