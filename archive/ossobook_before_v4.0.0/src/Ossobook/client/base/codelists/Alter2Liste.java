package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;

/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class Alter2Liste extends KodierungsListe {

	public Alter2Liste(Connection connection) {
		tabellenname = "alter2"; //$NON-NLS-1$
		namenColumne = "Alter2Name"; //$NON-NLS-1$
		codeColumne = "Alter2Code"; //$NON-NLS-1$
		teile = new HashMap<Integer, String>();
		codes = new HashMap<String, Integer>();
		fillHashmap(getList(connection, tabellenname));
	}

}