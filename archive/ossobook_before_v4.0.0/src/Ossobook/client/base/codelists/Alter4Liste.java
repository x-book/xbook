package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;

public class Alter4Liste extends KodierungsListe {

	public Alter4Liste(Connection connection) {
		tabellenname = "alter4"; //$NON-NLS-1$
		namenColumne = "Alter4Name"; //$NON-NLS-1$
		codeColumne = "Alter4Code"; //$NON-NLS-1$
		teile = new HashMap<Integer, String>();
		codes = new HashMap<String, Integer>();
		fillHashmap(getList(connection, tabellenname));
	}

}