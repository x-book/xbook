package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;

/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class Schlachtspur2Liste extends KodierungsListe {

	public Schlachtspur2Liste(Connection connection) {
		tabellenname = "schlachtspur2"; //$NON-NLS-1$
		namenColumne = "Schlachtspur2Name"; //$NON-NLS-1$
		codeColumne = "Schlachtspur2Code"; //$NON-NLS-1$
		teile = new HashMap<Integer, String>();
		codes = new HashMap<String, Integer>();
		fillHashmap(getList(connection, tabellenname));
	}

}