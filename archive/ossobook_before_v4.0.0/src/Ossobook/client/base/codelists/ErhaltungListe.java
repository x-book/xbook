package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;

/**
 * SkelCodeList from MainServer.
 * 
 * @author ali
 */
public class ErhaltungListe extends KodierungsListe {

	public ErhaltungListe(Connection connection) {
		tabellenname = "erhaltung"; //$NON-NLS-1$
		namenColumne = "ErhaltungName"; //$NON-NLS-1$
		codeColumne = "ErhaltungCode"; //$NON-NLS-1$
		teile = new HashMap<Integer, String>();
		codes = new HashMap<String, Integer>();
		fillHashmap(getList(connection, tabellenname));
	}

}
