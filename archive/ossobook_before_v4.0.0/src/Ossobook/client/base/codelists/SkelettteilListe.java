package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;

/**
 * SkelCodeList from MainServer.
 * 
 * @author ali
 */
public class SkelettteilListe extends KodierungsListe {
	public SkelettteilListe(Connection connection) {
		tabellenname = "skelteil"; //$NON-NLS-1$
		namenColumne = "SkelName"; //$NON-NLS-1$
		codeColumne = "SkelCode"; //$NON-NLS-1$
		teile = new HashMap<Integer, String>();
		codes = new HashMap<String, Integer>();
		fillHashmap(getList(connection, tabellenname));

	}
}
