package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;

/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class Alter1Liste extends KodierungsListe {
	/** Creates a new instance of AnimalList */
	public Alter1Liste(Connection connection) {
		tabellenname = "alter1"; //$NON-NLS-1$
		namenColumne = "Alter1Name"; //$NON-NLS-1$
		codeColumne = "Alter1Code"; //$NON-NLS-1$
		teile = new HashMap<Integer, String>();
		codes = new HashMap<String, Integer>();
		fillHashmap(getList(connection, tabellenname));
	}
}