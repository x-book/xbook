package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;

/**
 * Animal list from Server. Class gets Animal table from Main Server
 * 
 * @author ali
 */
public class GeschlechtsListe extends KodierungsListe {

	public GeschlechtsListe(Connection connection) {
		tabellenname = "geschlecht"; //$NON-NLS-1$
		namenColumne = "GeschlechtName"; //$NON-NLS-1$
		codeColumne = "GeschlechtCode"; //$NON-NLS-1$
		teile = new HashMap<Integer, String>();
		codes = new HashMap<String, Integer>();
		fillHashmap(getList(connection, tabellenname));
	}

}