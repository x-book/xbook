package ossobook.client.base.codelists;

import java.sql.Connection;
import java.util.HashMap;

import ossobook.client.io.database.modell.KodierungsListe;

public class Alter5Liste extends KodierungsListe {

	public Alter5Liste(Connection connection) {
		tabellenname = "alter5"; //$NON-NLS-1$
		namenColumne = "Alter5Name"; //$NON-NLS-1$
		codeColumne = "Alter5Code"; //$NON-NLS-1$
		teile = new HashMap<Integer, String>();
		codes = new HashMap<String, Integer>();
		fillHashmap(getList(connection, tabellenname));
	}

}