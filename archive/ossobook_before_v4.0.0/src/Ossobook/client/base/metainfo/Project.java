package ossobook.client.base.metainfo;

/**
 * This class represents a project stored in the OssoBook database.
 * 
 * <p>
 * It can be used to access the project name, id and database id. It can be used
 * as a reference to a project and passed to most functions handling project
 * manipulation to indicate which project to manipulate.
 * </p>
 * 
 * <p>
 * This class is immutable.
 * </p>
 * 
 * @author fnuecke
 */
public class Project {
	
	/**
	 * Project name.
	 */
	private final String	name;
	
	/**
	 * Project id.
	 */
	private final int		number;
	
	/**
	 * Database id project belongs to.
	 */
	private final int		databaseNumber;
	
	/**
	 * Creates a new project representative.
	 * 
	 * @param name
	 *            the name of the project to represent.
	 * @param number
	 *            the id of the project to represent.
	 * @param databaseNumber
	 *            the id of the database the project to represent belongs to.
	 */
	public Project(String name, int number, int databaseNumber) {
		this.name = name;
		this.number = number;
		this.databaseNumber = databaseNumber;
	}
	
	/**
	 * The name of the project.
	 * 
	 * @return the name of the project.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * The id (number) of the project.
	 * 
	 * @return the id of the project.
	 */
	public int getNumber() {
		return number;
	}
	
	/**
	 * The id (number) of the database the project belongs to.
	 * 
	 * @return the id of the database containing the project.
	 */
	public int getDatabaseNumber() {
		return databaseNumber;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
//		return "Project[name=" + name + ", number=" + number
//				+ ", databaseNumber=" + databaseNumber + "]";
		// When used in lists this is nicer.
		return name;
	}
}