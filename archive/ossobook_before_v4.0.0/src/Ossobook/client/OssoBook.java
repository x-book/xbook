package ossobook.client;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import ossobook.client.controllers.GuiController;
import ossobook.client.io.forwarder.Forwarder;
import ossobook.client.util.Configuration;
import ossobook.client.util.DatabaseInitializer;
import ossobook.client.util.MySqlServer;
import ossobook.client.util.SingleProgramInstance;
import ossobook.client.util.SplashScreen;

import com.jidesoft.plaf.LookAndFeelFactory;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;

/**
 * This is the Projects Main Class. Start OssoBook here.
 * 
 * <p>
 * This class takes care of reading in the main configuration file from the
 * current working directory (the file is named <code>ossobook.config</code>).
 * 
 * <p>
 * After that it initializes the GUI.
 * 
 * @author ali
 * @author fnuecke
 */
public class OssoBook {
	
	/**
	 * Program entry.
	 * 
	 * @param args
	 *            unused.
	 */
	public static void main(String[] args) {
		Configuration.init();
		
		// Make sure there's only one instance of OssoBook running at a time.
		SingleProgramInstance.check(Configuration.config.getProperty("ossobook.lockport")); //$NON-NLS-1$
		
		// Set look and feel.
		try {
			UIManager.setLookAndFeel(WindowsLookAndFeel.class.getName());
			LookAndFeelFactory.installJideExtension();
		} catch (Exception ignored) {
		}
		
		// Show splash screen.
		SplashScreen.show();
		
		// Start MySQL server if set, and prepare shutdown.
		if ("true".equals(Configuration.config.getProperty("mysqld.enabled"))) { //$NON-NLS-1$ //$NON-NLS-2$
			MySqlServer.init();
			
			// Check if the database is our standalone one, and if it has been
			// initialized / personalized.
			DatabaseInitializer.init();
		}
		
		// If it has not been disabled, start up the tunnel.
		Forwarder.init();
		
		// Start up the GUI.
		final GuiController guiController = new GuiController();
		
		// Show login window right away.
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				guiController.login();
			}
		});
	}
}
