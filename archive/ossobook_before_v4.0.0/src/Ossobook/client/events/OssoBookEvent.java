package ossobook.client.events;

public class OssoBookEvent {
	/**
	 * The event type.
	 */
	private final EventType type;
	
	/**
	 * Optional additional arguments.
	 */
	private final Object argument;
	
	/**
	 * Creates a new event of the given type.
	 * 
	 * @param type the type of the event.
	 */
	public OssoBookEvent(EventType type) {
		this(type, null);
	}
	
	/**
	 * Creates a new event of the given type, with additional arguments.
	 * 
	 * @param type
	 *            the type of the event.
	 * @param argument
	 *            additional arguments to store in the event.
	 */
	public OssoBookEvent(EventType type, Object argument) {
		this.type = type;
		this.argument = argument;
	}
	
	/**
	 * The type of this event.
	 * 
	 * @return event type.
	 */
	public EventType getType() {
		return type;
	}
	
	/**
	 * Additional arguments stored in the event. May be <code>null</code>.
	 * 
	 * @return additional arguments.
	 */
	public Object getArgument() {
		return argument;
	}
}
