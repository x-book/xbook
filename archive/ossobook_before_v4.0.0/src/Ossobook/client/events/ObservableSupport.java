package ossobook.client.events;

import java.util.Observable;

/**
 * To allow observable behavior for elements without extending the
 * {@link Observable} class.
 * 
 * @author fnuecke
 */
public class ObservableSupport extends Observable {
	
	/**
	 * Fire an event with the given data.
	 * 
	 * @param arg
	 *            the data to send as the update argument.
	 */
	public void fire(Object arg) {
		setChanged();
		notifyObservers(arg);
	}
}