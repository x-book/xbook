package ossobook.client.events;

/**
 * Actions that can be handled by a controller.
 */
public enum EventType {
	/**
	 * Open login dialog to let the user enter login information and
	 * perform actual login.
	 */
	LOGIN,
	
	/**
	 * Open project chooser and open selected project.
	 */
	OPEN_PROJECT,
	
	/**
	 * Open synchronization window.
	 */
	SYNCHRONIZE,
	
	/**
	 * Check for software updates.
	 */
	CHECK_FOR_UPDATES,
	
	/**
	 * Exit the program.
	 */
	EXIT,
	
	/**
	 * Show application log.
	 */
	SHOW_LOG,
	
	/**
	 * Show the 'about' dialog box.
	 */
	SHOW_ABOUT,
	
	/**
	 * Add new animal code.
	 */
	ADD_ANIMAL_CODE,
	
	/**
	 * Perform skelton analysis.
	 */
	SKELETON_ANALYSIS,
	
	/**
	 * Perform animal analysis.
	 */
	ANIMAL_ANALYSIS,
	
	/**
	 * Lookup tables.
	 */
	TIERARTSUCHE, SKELTEILSUCHE, BRUCHKANTESUCHE, ERHALTUNGSUCHE, ALTER1SUCHE,
	ALTER2SUCHE, ALTER3SUCHE, ALTER4SUCHE, ALTER5SUCHE, WURZELFRASS,
	VERSINTERUNG, FETTIGSUCHE, PATINASUCHE, BRANDSPURSUCHE, VERBISSSUCHE,
	SCHLACHTSPUR1, SCHLACHTSPUR2, GESCHLECHT, BRUCHKANTE2,
	
	/**
	 * Create a new project.
	 */
	NEW_PROJECT,
	
	/**
	 * Delete an existing project.
	 */
	DELETE_PROJECT
}