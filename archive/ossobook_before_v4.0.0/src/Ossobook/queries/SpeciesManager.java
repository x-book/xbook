package ossobook.queries;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.apache.commons.logging.LogFactory;

import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages tierart table (makes inserts, updates and select statements on the
 * table)
 * 
 * @author j.lamprecht
 * @author Daniel Kaltenthaler
 * 
 */
class SpeciesManager extends TableManager {

	public SpeciesManager(Connection con, String databaseName) {
		super(con, databaseName);
	}

	/**
	 * @precondition logged in user is admin
	 * @param TierCode
	 * @param TierName
	 * @param DTier
	 * @param TierFolgeAuswertung
	 * @param LB
	 */
	public void updateTierart(String TierCode, String TierName, String DTier,
			String TierFolgeAuswertung, String LB) {
		String query = String.format("INSERT INTO %s.tierart VALUES('%s','%s','%s','%s','%s', Now()+0, 'N');", databaseName, TierCode, TierName, DTier, TierFolgeAuswertung, LB); //$NON-NLS-1$
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			LogFactory.getLog(SpeciesManager.class).error(e, e);
		}
	}

	/**
	 * Converts an animal ID to the correspondenting animal label.
	 * 
	 * @param animalId The animal ID which should be converted to a label.
	 * @return The correspondenting animal label.
	 * 
	 * @throws StatementNotExecutedException Thrown if a error with the SQL query appears.
	 * 
	 * @see ossobook.queries.QueryManager#getTierlabel(int)
	 */
	public int getTierlabel(int animalId) throws StatementNotExecutedException {
		String query = String.format("SELECT LB FROM %s.tierart WHERE TierCode=%d AND geloescht='N';", databaseName, animalId); //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getInt("LB"); //$NON-NLS-1$
			}
			return 0;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}
	
	/**
	 * Converts an animal name to the correspondenting animal ID.
	 * 
	 * @param animalName The animal name which should be converted to an ID.
	 * @return The correspondenting animal ID.
	 * 
	 * @throws StatementNotExecutedException Thrown if a error with the SQL query appears.
	 * 
	 * @see ossobook.queries.QueryManager#getTierCode(String)
	 */
	public String getTierCode(String animalName) throws StatementNotExecutedException {
		String query = "SELECT TierCode " +
					   "FROM " + databaseName + ".tierart " +
					   "WHERE TierName = '" + animalName + "' " +
					   		"AND geloescht='N';";
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getString("TierCode");
			}
			return "-999";
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}
	
	/**
	 * Commit an String object including a (sub)string of an animal.
	 * The method searches in the database for all animal names which have 
	 * this string. The result is returned as a sorted Vector object.
	 * 
	 * @param substring The substring of the searched animal names.
	 * @return An Vector<String> object holding a sorted animal listing.
	 * @throws StatementNotExecutedException Thrown if a error with the SQL query appears.
	 * 
	 * @see ossobook.queries.QueryManager#getTierlabelListingForSearch(String)
	 */
	public Vector<String> getTierNameListingForSearch(String substring) throws StatementNotExecutedException {
		String query = "SELECT TierName " +
					   "FROM " + databaseName + ".tierart " +
					   "WHERE TierName LIKE '%" + substring + "%' " +
					   		"AND geloescht='N' " +
					   "ORDER BY TierName;";
		Vector<String> v = new Vector<String>();
		try {
			ResultSet rs = executeSelect(query);
			while (rs.next()) {
				v.add(rs.getString("TierName"));
			}
			return v;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}
	
	/**
	 * Check if a specific animal name is available in the database or not.
	 * 
	 * @param animalName The animal name to check
	 * @return <code>true</code> if the animal name is available in the database, 
	 *         <code>false</code> if not.
	 * @throws StatementNotExecutedException Thrown if a error with the SQL query appears.
	 * 
	 * @see ossobook.queries.QueryManager#isTierlabelAvailable(String)
	 */
	public boolean isTierlabelAvailable(String animalName) throws StatementNotExecutedException {
		String query = "SELECT TierName " +
				       "FROM " + databaseName + ".tierart " +
				       "WHERE TierName = '" + animalName +"' " +
				   			"AND geloescht='N';";
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
		
	}

}
