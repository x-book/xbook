package ossobook.queries;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages skelteil table (makes inserts, updates and select statements on the
 * table)
 * 
 * @author j.lamprecht
 * 
 */
class SkeletonManager extends TableManager {

	public SkeletonManager(Connection con, String databaseName) {
		super(con, databaseName);
	}

	public int getSkelLB(int skelcode) throws StatementNotExecutedException {
		String query = String.format("SELECT skelLB FROM %s.skelteil WHERE SkelCode=%d AND geloescht='N';", databaseName, skelcode); //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return  rs.getInt("skelLB"); //$NON-NLS-1$
			}
			return 0;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}
}
