package ossobook.queries;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.client.base.metainfo.Project;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages artefaktmaske table (makes inserts, deletes, updates and select
 * statements on the table)
 * 
 * @author j.lamprecht
 * 
 */
class ArtefactManager extends TableManager {
	private static final Log _log = LogFactory.getLog(ArtefactManager.class);
	// number of the local or global database of table version
	private final int versionDatabaseNumber;

	public ArtefactManager(Connection con, int databaseNumber,
			String databaseName) {
		super(con, databaseName);
		versionDatabaseNumber = databaseNumber;
	}

	/**
	 * artefaktmaske records deleted from database by changing "geloescht" field
	 * for given project
	 * 
	 * @precondition user has right to write on project
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void deleteArtefaktMaske(Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " //$NON-NLS-1$
				+ databaseName
				+ ".artefaktMaske, " //$NON-NLS-1$
				+ databaseName
				+ ".eingabeeinheit " //$NON-NLS-1$
				+ "SET artefaktMaske.geloescht='Y', artefaktMaske.Zustand='ge\u00E4ndert' " //$NON-NLS-1$
				+ "WHERE eingabeeinheit.artefaktID=artefaktMaske.ID AND " //$NON-NLS-1$
				+ "eingabeeinheit.DBNummerArtefakt=artefaktmaske.Datenbanknummer AND " //$NON-NLS-1$
				+ "eingabeeinheit.projNr='" + project.getNumber() //$NON-NLS-1$
				+ "' AND eingabeeinheit.DBNummerProjekt=" + project.getDatabaseNumber() + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * artefaktmaske records definitely deleted from database for given project
	 * 
	 * @precondition method is executed on local database during synchronization
	 * @param project
	 * @param event
	 *            - QueryManager.DELETE_DELETED, QueryManager.DELETE_ALL
	 * @throws StatementNotExecutedException
	 */
	public void deleteArtefaktPermanent(Project project, int event)
			throws StatementNotExecutedException {
		String condition = ""; //$NON-NLS-1$
		if (event == QueryManager.DELETE_DELETED) {
			condition = " AND artefaktMaske.geloescht='Y' AND artefaktmaske.Zustand='synchronisiert'"; //$NON-NLS-1$
		}
		String query = "DELETE artefaktMaske FROM " //$NON-NLS-1$
				+ databaseName
				+ ".artefaktMaske, " //$NON-NLS-1$
				+ databaseName
				+ ".eingabeeinheit " //$NON-NLS-1$
				+ "WHERE eingabeeinheit.artefaktID=artefaktMaske.ID " //$NON-NLS-1$
				+ "AND eingabeeinheit.DBNummerArtefakt=artefaktMaske.Datenbanknummer " //$NON-NLS-1$
				+ "AND eingabeeinheit.projNr='" + project.getNumber() + "' " //$NON-NLS-1$ //$NON-NLS-2$
				+ "AND eingabeeinheit.DBNummerProjekt=" + project.getDatabaseNumber() + " " //$NON-NLS-1$ //$NON-NLS-2$
				+ condition + ";"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * inserts arbitrary record into artefaktmaske
	 * 
	 * @param fieldsA
	 * @param valuesA
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int[] insertIntoArtefaktmaske(Vector<String> fieldsA,
			Vector<String> valuesA) throws StatementNotExecutedException {
		String table = databaseName + ".artefaktmaske"; //$NON-NLS-1$
		String query = "INSERT into " + table + " ("; //$NON-NLS-1$ //$NON-NLS-2$
		int[] artefact = new int[2];
		for (int i = 0; i < fieldsA.size(); i++) {
			query = query + fieldsA.elementAt(i) + ", "; //$NON-NLS-1$
		}
		query = query + "Datenbanknummer, Zustand) VALUES ("; //$NON-NLS-1$
		for (int i = 0; i < valuesA.size(); i++) {
			query = query + "'" + valuesA.elementAt(i) + "', "; //$NON-NLS-1$ //$NON-NLS-2$
		}
		query = query + versionDatabaseNumber + ", 'ge\u00E4ndert');"; //$NON-NLS-1$
		try {
			artefact[0] = executeInsert(query);
			artefact[1] = versionDatabaseNumber;
			/*
			 * query = "SELECT MAX(ID) as mid FROM artefaktmaske;"; ResultSet rs
			 * = createStatement(query); rs.next(); ArtefaktID =
			 * rs.getInt("mid");
			 */
		} catch (SQLException e) {
			if (e.getErrorCode() == 1364) {
				if (_log.isInfoEnabled()) {
					_log.info("Angabe fehlt"); //$NON-NLS-1$
				}
			} else {
				log.error("MySQL Error occurred.", e);
				throw new StatementNotExecutedException(query);
			}
		}
		return artefact;
	}

	/**
	 * gets key for the given eingabeeinheit key
	 * 
	 * @precondition user has right to read record
	 * @param recordKey
	 *            : Id of a data record in the table eingabeeinheit
	 * @return the ID of the data record in the artefaktmaske table which
	 *         belongs to the data set with the given datasetId in the table
	 *         eingabeeinheit
	 * @throws StatementNotExecutedException
	 */
	public int[] getArtefaktKey(int[] recordKey) throws StatementNotExecutedException {
		String query = "SELECT artefaktID, DBNummerArtefakt FROM " //$NON-NLS-1$
				+ databaseName + ".eingabeeinheit WHERE ID=" + recordKey[0] //$NON-NLS-1$
				+ " AND Datenbanknummer=" + recordKey[1] //$NON-NLS-1$
				+ " AND geloescht='N';"; //$NON-NLS-1$
		int[] id = new int[2];
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				id[0] = rs.getInt("artefaktID"); //$NON-NLS-1$
				id[1] = rs.getInt("DBNummerArtefakt"); //$NON-NLS-1$
			}
			return id;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * updates a given data record in the table artefaktmaske
	 * 
	 * @precondition user has right to update record
	 * @param fieldsA
	 *            : fields in artefaktmaske that shall be updated
	 * @param artifactKey
	 *            : identifier for the artefaktmaske record
	 * @param valuesA
	 *            : values to which the fields shall be changed
	 * @throws StatementNotExecutedException
	 */
	public void updateArtefaktmaske(Vector<String> fieldsA, int[] artifactKey,
			Vector<String> valuesA) throws StatementNotExecutedException {
		String query = "UPDATE " + databaseName + ".artefaktmaske SET "; //$NON-NLS-1$ //$NON-NLS-2$
		for (int i = 0; i < fieldsA.size(); i++) {
			query = query + fieldsA.elementAt(i) + "='" + valuesA.elementAt(i) //$NON-NLS-1$
					+ "',"; //$NON-NLS-1$
		}
		query = query + " Zustand='ge\u00E4ndert'"; //$NON-NLS-1$
		query = query + " WHERE ID=" + artifactKey[0] + " AND Datenbanknummer=" //$NON-NLS-1$ //$NON-NLS-2$
				+ artifactKey[1] + ";"; //$NON-NLS-1$
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}

	}

	/**
	 * gets the artefaktmaske entry for given key
	 * 
	 * @precondition user has right to read record
	 * @param artefactKey
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String[][] getArtefaktmaske(int[] artefactKey)
			throws StatementNotExecutedException {
		String query = "SELECT * FROM " + databaseName //$NON-NLS-1$
				+ ".artefaktmaske WHERE ID='" + artefactKey[0] + "'" //$NON-NLS-1$ //$NON-NLS-2$
				+ " AND geloescht='N' AND Datenbanknummer=" + artefactKey[1] //$NON-NLS-1$
				+ ";"; //$NON-NLS-1$
		String[][] result;
		try {
			ResultSet rs = executeSelect(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			boolean valid = rs.next();
			int columnCount = rsmd.getColumnCount();
			result = new String[columnCount][2];
			for (int i = 0; i < columnCount; i++) {
				result[i][0] = rsmd.getColumnName(i + 1);
				if (valid) {
				    result[i][1] = rs.getString(i + 1);
				} else {
				    result[i][1] = ""; //$NON-NLS-1$
				}
			}
			return result;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * artefaktmaske record deleted from database by changing "geloescht" field
	 * 
	 * @precondition user has right to "delete" record
	 * @param artefactID
	 * @throws StatementNotExecutedException
	 */
	public void deleteFromArtefaktmaske(int[] artefactID)
			throws StatementNotExecutedException {
		String query = "UPDATE " //$NON-NLS-1$
				+ databaseName
				+ ".artefaktmaske SET geloescht='Y', Zustand='ge\u00E4ndert' WHERE ID='" //$NON-NLS-1$
				+ artefactID[0] + "' AND Datenbanknummer=" + artefactID[1] //$NON-NLS-1$
				+ ";"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get all artefaktmaske entries which have been changed
	 * 
	 * @param project
	 * @param columntypes
	 *            columns of global database -> needed to be independet from
	 *            order and global database scheme changes
	 * @param event
	 *            - QueryManager.CHANGES_LOCAL, QueryManager.CHANGES_GLOBAL,
	 *            QueryManager.ALL_GLOBAL
	 * @param lastSynchronisation
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getEntries(Project project,
			String[][] columntypes, int event, String lastSynchronisation)
			throws StatementNotExecutedException {
		Vector<String[]> result;
		String condition = " WHERE projNr=" //$NON-NLS-1$
				+ project.getNumber()
				+ " AND eingabeeinheit.artefaktID=artefaktmaske.ID AND eingabeeinheit.DBNummerArtefakt=artefaktmaske.Datenbanknummer AND eingabeeinheit.DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber();
		if (event == QueryManager.CHANGES_LOCAL) {
			condition = condition
					+ " AND artefaktmaske.Zustand='ge\u00E4ndert' AND artefaktmaske.Nachrichtennummer != -1"; //$NON-NLS-1$
		} else if (event == QueryManager.CHANGES_GLOBAL) {
			condition = condition + " AND artefaktmaske.Zustand>'" //$NON-NLS-1$
					+ lastSynchronisation + "'"; //$NON-NLS-1$
		}
		String[] tables = { databaseName + ".artefaktmaske", //$NON-NLS-1$
				databaseName + ".eingabeeinheit" }; //$NON-NLS-1$
		result = getEntries(columntypes, tables, condition);
		return result;
	}

	/**
	 * 
	 * @param projectValues
	 *            of global database -> needed to be independet from global
	 *            database scheme changes
	 * @param columns
	 *            of global database -> needed to be independet from order and
	 *            global database scheme changes
	 * @param key
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getArtefactEntry(String[][] columns,
			int[] projectValues, Vector<String> key)
			throws StatementNotExecutedException {
		String condition = " WHERE "; //$NON-NLS-1$
		for (int i = 0; i < key.size(); i++) {
			condition = condition + key.elementAt(i) + "=" //$NON-NLS-1$
					+ projectValues[i];
			if (i < key.size() - 1) {
				condition = condition + " AND "; //$NON-NLS-1$
			}
		}
		String[] tableNames = { databaseName + ".artefaktmaske" }; //$NON-NLS-1$
		return getEntries(columns, tableNames, condition);
	}

	public void insertData(String[] scheme, String[] data)
			throws StatementNotExecutedException {
		insertData(scheme, data, false);
	}
	public void insertData(String[] scheme, String[] data, boolean ignore)
			throws StatementNotExecutedException {
		insertData(scheme, data, databaseName + ".artefaktmaske", ignore); //$NON-NLS-1$
	}

	public int updateData(String[] scheme, String[] data, Vector<String> key)
			throws StatementNotExecutedException {
		return updateData(scheme, data, databaseName + ".artefaktmaske", key); //$NON-NLS-1$
	}

	/**
	 * set Zustand to 'synchronisiert' after server affirmed changing global
	 * database
	 * 
	 * @precondition executed on local database during synchronization
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void setSynchronized(Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " //$NON-NLS-1$
				+ databaseName
				+ ".artefaktmaske, " //$NON-NLS-1$
				+ databaseName
				+ ".eingabeeinheit SET artefaktmaske.Zustand='synchronisiert', artefaktmaske.Nachrichtennummer=-1 WHERE " //$NON-NLS-1$
				+ "ProjNr = '" //$NON-NLS-1$
				+ project.getNumber()
				+ "' AND eingabeeinheit.DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND " //$NON-NLS-1$
				+ " eingabeeinheit.artefaktID=artefaktmaske.ID AND eingabeeinheit.DBNummerArtefakt=artefaktmaske.Datenbanknummer AND artefaktmaske.Nachrichtennummer!=-1;"; //$NON-NLS-1$
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	public String getLastChange(Vector<String> key, int[] keyValues)
			throws StatementNotExecutedException {
		return getStatus(key, keyValues, databaseName + ".artefaktmaske"); //$NON-NLS-1$
	}

	/**
	 * assign message number to changed entries in artefaktmaske of given
	 * project
	 * 
	 * @param message
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void setMessageNumber(int message, Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " //$NON-NLS-1$
				+ databaseName
				+ ".artefaktmaske, " //$NON-NLS-1$
				+ databaseName
				+ ".eingabeeinheit SET artefaktmaske.Nachrichtennummer=" //$NON-NLS-1$
				+ message
				+ " WHERE ProjNr=" //$NON-NLS-1$
				+ project.getNumber()
				+ " AND eingabeeinheit.DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND eingabeeinheit.artefaktID=artefaktmaske.ID AND eingabeeinheit.DBNummerArtefakt=artefaktmaske.Datenbanknummer AND artefaktmaske.Zustand='ge\u00E4ndert';"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}
}
