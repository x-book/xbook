package ossobook.queries;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import ossobook.client.base.metainfo.Project;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages benutzer table (makes inserts, deletes, updates and select statements
 * on the table) manages rights of the users for the different projects
 * 
 * @author j.lamprecht
 * 
 */
public class UserManager extends TableManager {

	// constants for changing rights
	public static final int NORIGHTS = 0;
	public static final int READ = 1;
	public static final int WRITE = 2;

	public UserManager(Connection con, String databaseName) {
		super(con, databaseName);
	}

	/**
	 * 
	 * @return the users of the database except the admins and the logged in
	 *         user
	 * @throws StatementNotExecutedException
	 */
	public Vector<String> getUserNames() throws StatementNotExecutedException {
		Vector<String> users = new Vector<String>();
		String query = "SELECT BenutzerName FROM " //$NON-NLS-1$
				+ databaseName
				+ ".benutzer WHERE " //$NON-NLS-1$
				+ "BenutzerName!=SUBSTRING(USER(),1, LOCATE('@', USER())-1) AND" //$NON-NLS-1$
				+ " Admin!='Y' ORDER BY BenutzerName;"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			while (rs.next()) {
				users.add(rs.getString(1));
			}
			return users;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * proofs whether user may do everything
	 * 
	 * @return true if User is administrator, else false
	 * @throws StatementNotExecutedException
	 */
	public boolean getIsAdmin() throws StatementNotExecutedException {
		String query = "SELECT Admin FROM " //$NON-NLS-1$
				+ databaseName
				+ ".benutzer WHERE BenutzerName=SUBSTRING(USER(),1, LOCATE('@', USER())-1)" //$NON-NLS-1$
				+ " AND geloescht='N';"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
            return rs.next() && rs.getString(1).equals("Y");
			} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * 
	 * @param project
	 *            project number
	 * @return right which the User has for the project - read or read and write
	 *         or nothing
	 * @throws StatementNotExecutedException
	 */
	public int getRight(Project project)
			throws StatementNotExecutedException {
		String query = "SELECT Recht FROM " + databaseName //$NON-NLS-1$
				+ ".projektrechte WHERE " //$NON-NLS-1$
				+ "Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1)" //$NON-NLS-1$
				+ " and ProjNr=" + project.getNumber() + " AND DBNummerProjekt=" //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getDatabaseNumber() + " AND geloescht='N';"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				if (rs.getString(1).equals("schreiben")) { //$NON-NLS-1$
					return WRITE; //$NON-NLS-1$
				} else if (rs.getString(1).equals("lesen")) { //$NON-NLS-1$
					return READ; //$NON-NLS-1$
				}
			}
			return NORIGHTS;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition: "changer" is project owner or administrator gives /
	 *                changes /revokes right to/ of / from given user for given
	 *                project
	 * @param project
	 * @param user
	 *            whose right shall be changed
	 * @param right
	 * @throws StatementNotExecutedException
	 */
	public void changeRight(Project project, String user, int right)
			throws StatementNotExecutedException {
		String query = ""; //$NON-NLS-1$
		if (user == null) {
			user = "SUBSTRING(USER(),1, LOCATE('@', USER())-1)"; //$NON-NLS-1$
		} else {
			user = "'" + user + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}
		try {
			if (right == NORIGHTS) {
				query = "UPDATE " + databaseName //$NON-NLS-1$
						+ ".projektrechte SET geloescht='Y' WHERE " + "ProjNr=" //$NON-NLS-1$ //$NON-NLS-2$
						+ project.getNumber() + " AND Benutzer=" + user //$NON-NLS-1$
						+ " AND DBNummerProjekt=" + project.getDatabaseNumber() + ";"; //$NON-NLS-1$ //$NON-NLS-2$
				executeUpdate(query);
			} else {
				query = "UPDATE " + databaseName + ".projektrechte SET Recht=" //$NON-NLS-1$ //$NON-NLS-2$
						+ right + " WHERE " + "ProjNr=" + project.getNumber() //$NON-NLS-1$ //$NON-NLS-2$
						+ " AND Benutzer=" + user + " AND DBNummerProjekt=" //$NON-NLS-1$ //$NON-NLS-2$
						+ project.getDatabaseNumber() + ";"; //$NON-NLS-1$
				int alreadyexists = executeUpdate(query);
				if (alreadyexists < 1) {
					query = "INSERT INTO " //$NON-NLS-1$
							+ databaseName
							+ ".projektrechte (Benutzer,ProjNr, DBNummerProjekt, Recht) " //$NON-NLS-1$
							+ "VALUES (" + user + ", " + project.getNumber() + ", " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							+ project.getDatabaseNumber() + ", " + right + ");"; //$NON-NLS-1$ //$NON-NLS-2$
					executeUpdate(query);
				}
			}
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get e-mail address of project owner
	 * 
	 * @param project
	 *            primary key of project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String[] getMail(Project project)
			throws StatementNotExecutedException {
		String query = "SELECT Mail FROM " + databaseName + ".projekt, " //$NON-NLS-1$ //$NON-NLS-2$
				+ databaseName + ".benutzer WHERE " + "projNr=" + project.getNumber() //$NON-NLS-1$ //$NON-NLS-2$
				+ " AND Datenbanknummer=" + project.getDatabaseNumber() //$NON-NLS-1$
				+ " AND (ProjEigentuemer=BenutzerName OR BenutzerName='" //$NON-NLS-1$
				+ QueryManagerFactory.getUsername() + "') AND benutzer.geloescht='N'"; //$NON-NLS-1$
		Vector<String> mail = new Vector<String>();
		try {
			ResultSet rs = executeSelect(query);
			while (rs.next()) {
				mail.add(rs.getString(1));
			}
			return mail.toArray(new String[mail.size()]);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}
}
