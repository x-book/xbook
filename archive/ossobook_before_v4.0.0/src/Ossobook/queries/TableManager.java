package ossobook.queries;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages database queries
 * 
 * @author j.lamprecht
 * @author fnuecke
 */
public abstract class TableManager {
	static final Log	log	= LogFactory.getLog(TableManager.class);
	protected final Connection		connection;
	protected final String			databaseName;
	
	/**
	 * 
	 * @param connection
	 *            that has been established
	 */
    TableManager(Connection connection, String databaseName) {
		this.connection = connection;
		this.databaseName = databaseName;
	}
	
	/**
	 * select, insert, update or delete statement on database shall be done
	 * 
	 * @param query
	 *            : select, insert, update or delete statement
	 * @throws StatementNotExecutedException
	 */
    void executeQuery(String query) throws SQLException {
		Statement statement = connection.createStatement();
		statement.execute(query);
		statement.close();
	}
	
	/**
	 * insert, update or delete statement on database shall be done
	 * 
	 * @param query
	 *            : insert, update or delete statement
	 * @throws SQLException
	 */
    int executeUpdate(String query) throws SQLException {
		Statement statement = connection.createStatement();
		int result = statement.executeUpdate(query);
		statement.close();
		return result;
	}
	
	/**
	 * select statement on database shall be done
	 * 
	 * @param query
	 *            : select Statement
	 * @return
	 * @throws SQLException
	 */
    ResultSet executeSelect(String query) throws SQLException {
		Statement s = connection.createStatement();
		return s.executeQuery(query);
	}

	/**
	 * executes an insert for the given table
	 * 
	 * @param query
	 * @return primary key values of the inserted record
	 * @throws StatementNotExecutedException
	 */
    int executeInsert(String query) throws SQLException {
		Statement statement = connection.createStatement();
		statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
		ResultSet resultSet = statement.getGeneratedKeys();
		if (resultSet.next()) {
			return resultSet.getInt(1);
		}
		return -1;
	}

	/**
	 * updates the record for the given table
	 * 
	 * @param scheme
	 *            - scheme of the table
	 * @param data
	 *            - values of the record
	 * @param tableName
	 * @param key
	 *            - primary key of table
	 * @return
	 * @throws StatementNotExecutedException
	 */
    int updateData(String[] scheme, String[] data, String tableName,
			Vector<String> key)
			throws StatementNotExecutedException {
		String query = "UPDATE " + tableName + " SET "; //$NON-NLS-1$ //$NON-NLS-2$
		String[] keyValues = new String[key.size()];
		for (int i = 0; i < scheme.length; i++) {
			for (int j = 0; j < key.size(); j++) {
				if ((databaseName + "." + scheme[i]).equals(tableName + "." //$NON-NLS-1$ //$NON-NLS-2$
						+ key.elementAt(j))) {
					keyValues[j] = data[i];
					break;
				}
			}
			if ((databaseName + "." + scheme[i]).equals(tableName + ".Zustand")) { //$NON-NLS-1$ //$NON-NLS-2$
				query = query + "Zustand='synchronisiert'"; //$NON-NLS-1$
			} else {
				query = query + scheme[i] + "='" + data[i] + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			}
			if (i < scheme.length - 1) {
				query = query + ", "; //$NON-NLS-1$
			} else {
				query = query + " WHERE "; //$NON-NLS-1$
			}
		}
		for (int i = 0; i < key.size(); i++) {
			query = query + key.elementAt(i) + "='" + keyValues[i] + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			if (i < key.size() - 1) {
				query = query + " AND "; //$NON-NLS-1$
			} else {
				query = query + ";"; //$NON-NLS-1$
			}
		}
		try {
			return executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}
	
	/**
	 * inserts a new record into the given table
	 * 
	 * @param scheme
	 *            - scheme of the table
	 * @param data
	 *            - values of the record
	 * @param tableName
	 * @throws StatementNotExecutedException
	 */
    void insertData(String[] scheme, String[] data, String tableName)
			throws StatementNotExecutedException {
		insertData(scheme, data, tableName, false);
	}
	
	/**
	 * inserts a new record into the given table
	 * 
	 * @param scheme
	 *            - scheme of the table
	 * @param data
	 *            - values of the record
	 * @param tableName
	 * @throws StatementNotExecutedException
	 */
    void insertData(String[] scheme, String[] data, String tableName, boolean ignore)
			throws StatementNotExecutedException {
		String query = "INSERT" + (ignore ? " IGNORE" : "") + " INTO " + tableName + " ("; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		int statusPlace = -1;
		for (int i = 0; i < scheme.length; i++) {
			if (scheme[i].equals(tableName + ".Zustand")) { //$NON-NLS-1$
				statusPlace = i;
			}
			query = query + scheme[i];
			if (i == scheme.length - 1) {
				query = query + ") VALUES ("; //$NON-NLS-1$
			} else {
				query = query + ", "; //$NON-NLS-1$
			}
		}
		for (int i = 0; i < data.length; i++) {
			if (!tableName.equals(databaseName + ".projektrechte") //$NON-NLS-1$
					&& i == statusPlace) {
				query = query + "'synchronisiert'"; //$NON-NLS-1$
			} else {
				query = query + "'" + data[i] + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			}
			if (i == scheme.length - 1) {
				query = query + ");"; //$NON-NLS-1$
			} else {
				query = query + ", "; //$NON-NLS-1$
			}
		}
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * gets changed records of given table
	 * 
	 * @param columns
	 *            - scheme of the table
	 * @param tableNames
	 * @param condition
	 *            - where condition
	 * @return
	 * @throws StatementNotExecutedException
	 */
    Vector<String[]> getEntries(String[][] columns, String[] tableNames,
			String condition) throws StatementNotExecutedException {

		String query = "SELECT "; //$NON-NLS-1$
		for (int i = 0; i < columns[0].length; i++) {
			query = query + columns[0][i];
			if (i < columns[0].length - 1) {
				query = query + ", "; //$NON-NLS-1$
			} else {
				query = query + " FROM "; //$NON-NLS-1$
			}
		}
		for (int i = 0; i < tableNames.length; i++) {
			query = query + tableNames[i];
			if (i < tableNames.length - 1) {
				query = query + ", "; //$NON-NLS-1$
			} else {
				query = query + condition;
			}
		}
		Vector<String[]> result = new Vector<String[]>();
		try {
			ResultSet rs = executeSelect(query);
			while (rs.next()) {
				String[] row = new String[columns[1].length];
				for (int i = 0; i < columns[0].length; i++) {
					if ("int".equals(columns[1][i])) { //$NON-NLS-1$
						row[i] = Integer.toString(rs.getInt(i + 1));
					} else if ("float".equals(columns[1][i])) { //$NON-NLS-1$
						row[i] = Float.toString(rs.getFloat(i + 1));
					} else {
						row[i] = rs.getString(i + 1);
					}
				}
				result.add(row);
			}
			return result;
		} catch (SQLException e) {
			log.error("MySQL Error occurred. Query was: " + query, e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * gets status of the given record
	 * 
	 * @param key
	 *            - primary key fields
	 * @param keyValues
	 *            - primary key values
	 * @param tableName
	 * @return
	 * @throws StatementNotExecutedException
	 */
    String getStatus(Vector<String> key, int[] keyValues,
			String tableName) throws StatementNotExecutedException {
		String query = String.format("SELECT Zustand FROM %s WHERE ", tableName); //$NON-NLS-1$
		for (int i = 0; i < key.size(); i++) {
			query = query + key.elementAt(i) + "=" + keyValues[i]; //$NON-NLS-1$
			if (i < key.size() - 1) {
				query = query + " AND "; //$NON-NLS-1$
			} else {
				query = query + ";"; //$NON-NLS-1$
			}
		}
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getString(1);
			}
			return null;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}
}
