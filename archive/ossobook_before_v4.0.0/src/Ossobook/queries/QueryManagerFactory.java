package ossobook.queries;

import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.io.database.ConnectionType;
import ossobook.client.io.database.DatabaseConnection;
import ossobook.client.synchronization.SynchronizationWorker;
import ossobook.client.util.Configuration;
import ossobook.exceptions.StatementNotExecutedException;

import com.mysql.jdbc.MysqlErrorNumbers;

/**
 * This class is used to store connections to the local and global database over
 * the course of a session (i.e. as long as the client is open).
 * 
 * <p>
 * There are three accessors, for the three connection types:<br/>
 * <dl>
 * <dt>{@link #getLocalManager()}</dt>
 * <dd>for a connection to the local database.</dd>
 * <dt>{@link #getGlobalManager()}</dt>
 * <dd>for a connection to the global database.</dd>
 * <dt>{@link #getLocalSyncManager()}</dt>
 * <dd>for a connection to the local database to be used while synchronizing.
 * This extra local connection is necessary to avoid interference by users
 * trying to work on local projects while the synchronization runs.</dd>
 * <dt>{@link #getGlobalSyncManager()}</dt>
 * <dd>for a connection to the global database to be used while synchronizing.</dd>
 * </dl>
 * </p>
 * 
 * @author fnuecke
 */
public class QueryManagerFactory {
	
	/**
	 * Logging.
	 */
	private static final Log log = LogFactory.getLog(QueryManagerFactory.class);
	
	/**
	 * Local connection enabled / available?
	 */
	private static boolean		localEnabled	= false;
	
	/**
	 * Global connection enabled / available?
	 */
	private static boolean		globalEnabled	= false;
	
	/**
	 * Currently set username.
	 */
	private static String		username;
	
	/**
	 * Currently set password.
	 */
	private static String		password;
	
	/**
	 * Current local query manager in use.
	 */
	private static QueryManager	localManager;
	
	/**
	 * Current global query manager in use.
	 */
	private static QueryManager	globalManager;
	
	/**
	 * Current local query manager for synchronization in use.
	 */
	private static QueryManager	localSyncManager;
	
	/**
	 * Current global query manager for synchronization in use.
	 */
	private static QueryManager	globalSyncManager;
	
	/**
	 * Flag whether currently synchronizing, ergo blocking the connection used
	 * for synchronizing to all other access.
	 */
	private static boolean		isSynchronizationInProgress;
	
	/**
	 * Reinitialize using the current login information.
	 * 
	 * @param username
	 *            the new username to use.
	 * @param password
	 *            the new password to use.
	 */
	public static void initialize(String username, String password) {
		synchronized (QueryManagerFactory.class) {
			// Store username and password.
			QueryManagerFactory.username = username;
			QueryManagerFactory.password = password;
			
			// Close previous connections.
			if (localManager != null) {
				localManager.closeConnection();
			}
			if (globalManager != null) {
				globalManager.closeConnection();
			}
			
			// Check which connections are allowed, for the allowed ones check if
			// the login data works / a connection is possible.
			localEnabled = "true".equals( //$NON-NLS-1$
					Configuration.config.getProperty("local.enabled")); //$NON-NLS-1$
			if (localEnabled) {
				// Check if login data is correct.
				try {
					// Connect, then disconnect again.
					DatabaseConnection connection = new DatabaseConnection(
							username, password,
							ConnectionType.CONNECTION_LOCAL);
					localEnabled = checkVersion(new QueryManager(connection));
					connection.getUnderlyingConnection().close();
				} catch (SQLException e) {
					switch (e.getErrorCode()) {
					case MysqlErrorNumbers.ER_ACCESS_DENIED_ERROR:
						log.warn(Messages.getString("QueryManagerFactory.0")); //$NON-NLS-1$
						break;
					default:
						log.warn(Messages.getString("QueryManagerFactory.1")); //$NON-NLS-1$
						break;
					}
					// Login failed, disable local database use.
					localEnabled = false;
				}
			}
			globalEnabled = "true".equals( //$NON-NLS-1$
					Configuration.config.getProperty("global.enabled")); //$NON-NLS-1$
			if (globalEnabled) {
				// Check if login data is correct.
				try {
					// Connect, then disconnect again.
					DatabaseConnection connection = new DatabaseConnection(
							username, password,
							ConnectionType.CONNECTION_GLOBAL);
					globalEnabled = checkVersion(new QueryManager(connection));
					connection.getUnderlyingConnection().close();
				} catch (SQLException e) {
					// Login failed, disable local database use.
					globalEnabled = false;
				}
			}
		}
	}
	
	/**
	 * Check the database version against the client version (entry in config
	 * versus entry in database .version table).
	 * 
	 * @param manager
	 *            querymanager wrapping the connection to check.
	 * @return <code>true</code> if the versions are equal.
	 */
	private static boolean checkVersion(QueryManager manager) {
		String clientVersion = Configuration.config.getProperty("ossobook.dbversion"); //$NON-NLS-1$
		String databaseVersion = "?"; //$NON-NLS-1$
		try {
			databaseVersion = manager.getDatabaseVersion();
		} catch (StatementNotExecutedException e) {
			LogFactory.getLog(QueryManagerFactory.class).warn(e, e);
		}
		if (databaseVersion.equals(clientVersion)) {
			return true;
		}
		log.warn(Messages.getString("QueryManagerFactory.2", clientVersion, databaseVersion)); //$NON-NLS-1$
		return false;
	}
	
	/**
	 * Whether a connection to the local database is available.
	 * 
	 * @return <code>true</code> if a connection to the local database is
	 * possible.
	 */
	public static boolean isLocalAvailable() {
		return localEnabled;
	}
	
	/**
	 * Whether a connection to the global database is available.
	 * 
	 * @return <code>true</code> if a connection to the global database is
	 * possible.
	 */
	public static boolean isGlobalAvailable() {
		return globalEnabled;
	}

	/**
	 * Whether a connection managers used during synchronization are available.
	 * 
	 * @return <code>true</code> if a no synchronization is currently in
	 * progress.
	 */
	public static boolean isSyncAvailable() {
		synchronized (QueryManagerFactory.class) {
			return !isSynchronizationInProgress;
		}
	}
	
	/**
	 * The username currently used for creating connections to the databases.
	 * 
	 * @return the currently set username.
	 */
	public static String getUsername() {
		synchronized (QueryManagerFactory.class) {
			return username;
		}
	}
	
	/**
	 * Sets the current synchronization state, i.e. whether the program is
	 * currently performing a synchronization or not.
	 * 
	 * <p>
	 * <b>Important</b>: this should <em>only</em> be set to <code>false</code>
	 * again by the same process that set it to <code>true</code>, to guarantee
	 * synchronization safety. In the current implementation (2010/02/26) the
	 * only class that should actively change this value is the
	 * {@link SynchronizationWorker}.
	 * </p>
	 * 
	 * <p>
	 * Note that instances retrieved using the {@link #getLocalSyncManager()}
	 * and {@link #getGlobalSyncManager()} functions prior to changing this
	 * value remain valid and could still cause problems. Due to this it is not
	 * recommended to store long term references to these query managers, but
	 * instead re-fetch them when needed.
	 * </p>
	 * 
	 * @param value the new value of the synchronization state.
	 */
	public static void setSynchronizationInProgress(boolean value) {
		synchronized (QueryManagerFactory.class) {
			isSynchronizationInProgress = value;
		}
	}
	
	/**
	 * Returns whether the user is an admin (has admin rights) in a connectable
	 * database. The user must be administrator in the local <b>and</b> the
	 * global database, if both are connectable.
	 * 
	 * @return <code>true</code> if the user is admin in all connectable
	 * databases.
	 */
	public static boolean isUserAdmin() {
		synchronized (QueryManagerFactory.class) {
			QueryManager manager = getGlobalManager();
			if (manager != null) {
				try {
					if (!manager.getIsAdmin()) {
						return false;
					}
				} catch (StatementNotExecutedException e) {
					return false;
				}
			}
			manager = getLocalManager();
			if (manager != null) {
				try {
					if (!manager.getIsAdmin()) {
						return false;
					}
				} catch (StatementNotExecutedException e) {
					return false;
				}
			}
			return true;
		}
	}
	
	/**
	 * Returns a manager for a connection to the local database, or
	 * <code>null</code> if the local database is disabled or not available.
	 * 
	 * <p>
	 * If no connection is currently active, a new manager is created and
	 * returned. This manager will then be stored and returned in future calls
	 * to this function, until the connection the manager uses dies.
	 * </p>
	 * 
	 * @return query manager for the local database or <code>null</code>.
	 */
	public static QueryManager getLocalManager() {
		synchronized (QueryManagerFactory.class) {
			if (!localEnabled) {
				return null;
			}
			try {
				if (localManager == null || localManager.isClosed()) {
					localManager = new QueryManager(new DatabaseConnection(
							username, password,
							ConnectionType.CONNECTION_LOCAL));
				}
			} catch (SQLException e) {
				localManager = null;
			}
		}
		return localManager;
	}
	
	/**
	 * Returns a manager for a connection to the global database, or
	 * <code>null</code> if the global database is disabled or not available.
	 * 
	 * <p>
	 * If no connection is currently active, a new manager is created and
	 * returned. This manager will then be stored and returned in future calls
	 * to this function, until the connection the manager uses dies.
	 * </p>
	 * 
	 * @return query manager for the global database or <code>null</code>.
	 */
	public static QueryManager getGlobalManager() {
		synchronized (QueryManagerFactory.class) {
			if (!globalEnabled) {
				return null;
			}
			try {
				if (globalManager == null || globalManager.isClosed()) {
					globalManager = new QueryManager(new DatabaseConnection(
							username, password,
							ConnectionType.CONNECTION_GLOBAL));
				}
			} catch (SQLException e) {
				globalManager = null;
			}
		}
		return globalManager;
	}
	
	/**
	 * Returns a manager for a connection to the local database for
	 * synchronization, or <code>null</code> if the connection fails.
	 * 
	 * <p>
	 * If no connection is currently active, a new manager is created and
	 * returned. This manager will then be stored and returned in future calls
	 * to this function, until the connection the manager uses dies.
	 * </p>
	 * 
	 * @return query manager for the local database or <code>null</code>.
	 */
	public static QueryManager getLocalSyncManager() {
		synchronized (QueryManagerFactory.class) {
			if (isSynchronizationInProgress) {
				return null;
			}
			try {
				if (localSyncManager == null ||
						localSyncManager.isClosed())
				{
					localSyncManager = new QueryManager(new DatabaseConnection(
							username, password,
							ConnectionType.CONNECTION_LOCAL));
				}
			} catch (SQLException e) {
				localSyncManager = null;
			}
		}
		return localSyncManager;
	}
	
	/**
	 * Returns a manager for a connection to the global database for
	 * synchronization, or <code>null</code> if the connection fails.
	 * 
	 * <p>
	 * If no connection is currently active, a new manager is created and
	 * returned. This manager will then be stored and returned in future calls
	 * to this function, until the connection the manager uses dies.
	 * </p>
	 * 
	 * @return query manager for the global database or <code>null</code>.
	 */
	public static QueryManager getGlobalSyncManager() {
		synchronized (QueryManagerFactory.class) {
			if (isSynchronizationInProgress) {
				return null;
			}
			try {
				if (globalSyncManager == null ||
						globalSyncManager.isClosed())
				{
					globalSyncManager = new QueryManager(new DatabaseConnection(
							username, password,
							ConnectionType.CONNECTION_SYNCHRONIZE));
				}
			} catch (SQLException e) {
				globalSyncManager = null;
			}
		}
		return globalSyncManager;
	}
}
