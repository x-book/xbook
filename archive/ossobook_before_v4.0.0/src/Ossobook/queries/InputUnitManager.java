package ossobook.queries;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.client.base.metainfo.Project;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages eingabeeinheit table (makes inserts, deletes, updates and select
 * statements on the table)
 * 
 * @author j.lamprecht
 * 
 */
class InputUnitManager extends TableManager {
	private static final Log _log = LogFactory.getLog(InputUnitManager.class);
	private final int versionDatabaseNumber;

	public InputUnitManager(Connection con, int databaseNumber,
			String databaseName) {
		super(con, databaseName);
		versionDatabaseNumber = databaseNumber;
	}

	/**
	 * @precondition user has the right to read project
	 * @param text
	 * @param project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getCountFk(String text, Project project)
			throws StatementNotExecutedException {
		String query = "SELECT COUNT(fK) AS fk FROM " + databaseName //$NON-NLS-1$
				+ ".eingabeeinheit WHERE fK LIKE('" + text + "')AND projNr=" //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getNumber() + " " + " AND DBNummerProjekt=" + project.getDatabaseNumber() //$NON-NLS-1$ //$NON-NLS-2$
				+ " AND geloescht='N';"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getInt("fk"); //$NON-NLS-1$
			}
			return -1;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to read project
	 * @param number
	 * @param project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getCountSkelNr(int number, Project project)
			throws StatementNotExecutedException {
		String query = "SELECT COUNT(skelNr) AS sknr FROM " + databaseName //$NON-NLS-1$
				+ ".eingabeeinheit WHERE skelNr=" + number + " AND projNr=" //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getNumber() + "AND DBNummerProjekt=" + project.getDatabaseNumber() //$NON-NLS-1$
				+ " AND geloescht='N';"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getInt("sknr"); //$NON-NLS-1$
			}
			return -1;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to read project
	 * @param number
	 * @param project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getCountKnIndNr(int number, Project project)
			throws StatementNotExecutedException {
		String query = "SELECT COUNT(knIndNr) AS knnr FROM " + databaseName //$NON-NLS-1$
				+ ".eingabeeinheit WHERE knIndNr=" + number + " AND projNr=" //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getNumber() + " AND DBNummerProjekt=" + project.getDatabaseNumber() //$NON-NLS-1$
				+ " AND geloescht='N';"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getInt("knnr"); //$NON-NLS-1$
			}
			return -1;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to read project
	 * @param project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getBonesNumber(Project project)
			throws StatementNotExecutedException {
		String query = "SELECT SUM(Anzahl) as SUMAnzahl FROM " + databaseName //$NON-NLS-1$
				+ ".eingabeeinheit WHERE projNr='" + project.getDatabaseNumber() //$NON-NLS-1$
				+ "' AND DBNummerProjekt=" + project.getDatabaseNumber() //$NON-NLS-1$
				+ " AND geloescht='N';"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getInt("SUMAnzahl"); //$NON-NLS-1$
			}
			return 0;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to read project
	 * @param project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public double getOverallWeight(Project project)
			throws StatementNotExecutedException {
		String query = "SELECT SUM(gewicht) as gewicht FROM " + databaseName //$NON-NLS-1$
				+ ".eingabeeinheit WHERE projNr='" + project.getNumber() //$NON-NLS-1$
				+ "' AND DBNummerProjekt=" + project.getDatabaseNumber() //$NON-NLS-1$
				+ " AND geloescht='N';"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			double gewicht = 0;
			while (rs.next()) {
				gewicht = 0.0;
				try {
					gewicht = Double.parseDouble(rs.getString("gewicht")); //$NON-NLS-1$
				} catch (Exception e) {
					LogFactory.getLog(InputUnitManager.class).warn(e, e);
				}
			}
			return gewicht;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to delete from project
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void deleteUnit(Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " //$NON-NLS-1$
				+ databaseName
				+ ".eingabeeinheit SET geloescht='Y', Zustand='ge\u00E4ndert' WHERE eingabeeinheit.projNr='" //$NON-NLS-1$
				+ project.getNumber() + "' AND DBNummerProjekt=" + project.getDatabaseNumber() //$NON-NLS-1$
				+ ";"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * eingabeeinheit records definitely deleted from database for given project
	 * 
	 * @precondition method is executed on local database during synchronization
	 * @param project
	 * @param event
	 *            - QueryManager.DELETE_DELETED, QueryManager.DELETE_ALL
	 * @throws StatementNotExecutedException
	 */
	public void deleteUnitPermanent(Project project, int event)
			throws StatementNotExecutedException {
		String condition = ""; //$NON-NLS-1$
		if (event == QueryManager.DELETE_DELETED) {
			condition = " AND geloescht='Y' AND Zustand='synchronisiert'"; //$NON-NLS-1$
		}
		String query = "DELETE FROM " + databaseName //$NON-NLS-1$
				+ ".eingabeeinheit WHERE eingabeeinheit.projNr='" //$NON-NLS-1$
				+ project.getNumber() + "' AND DBNummerProjekt=" + project.getDatabaseNumber() //$NON-NLS-1$
				+ " " + condition + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to write in project
	 * @param project
	 * @param fields
	 * @param ArtefaktID
	 * @param massID
	 * @param phase1
	 * @param phase2
	 * @param phase3
	 * @param phase4
	 * @param values
	 * @throws StatementNotExecutedException
	 */
	public void insertIntoEingabeeinheit(Project project,
			Vector<String> fields, int[] ArtefaktID, int[] massID,
			String phase1, String phase2, String phase3, String phase4,
			Vector<String> values) throws StatementNotExecutedException {

		String query = "INSERT into " + databaseName + ".eingabeeinheit (" //$NON-NLS-1$ //$NON-NLS-2$
				+ "ProjNr,DBNummerProjekt," + "BenutzerName," //$NON-NLS-1$ //$NON-NLS-2$
				+ "ArtefaktID,DBNummerArtefakt" + ",massID,DBNummerMasse" //$NON-NLS-1$ //$NON-NLS-2$
				+ ",phase1,phase2,phase3,phase4, "; //$NON-NLS-1$
		for (int i = 0; i < fields.size(); i++) {
			query = query + fields.elementAt(i) + ", "; //$NON-NLS-1$
		}
		query = query + "Datenbanknummer, Zustand) Values (" + project.getNumber() //$NON-NLS-1$
				+ "," + project.getDatabaseNumber() + ", " //$NON-NLS-1$ //$NON-NLS-2$
				+ "SUBSTRING(USER(),1, LOCATE('@', USER())-1)" + "," //$NON-NLS-1$ //$NON-NLS-2$
				+ ArtefaktID[0] + ", " + ArtefaktID[1] + "," + massID[0] + ", " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ massID[1] + "," + phase1 + ", " + phase2 + ", " + phase3 //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ ", " + phase4 + ", "; //$NON-NLS-1$ //$NON-NLS-2$
		for (int i = 0; i < values.size(); i++) {
			query = query + "'" + values.elementAt(i) + "', "; //$NON-NLS-1$ //$NON-NLS-2$
		}
		query = query + versionDatabaseNumber + ", 'ge\u00E4ndert');"; //$NON-NLS-1$
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query); //$NON-NLS-1$
		}
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to write in project
	 * @param project
	 * @param fields
	 * @param ArtefaktKey
	 * @param massKey
	 * @param phase1
	 * @param phase2
	 * @param phase3
	 * @param phase4
	 * @param values
	 * @throws StatementNotExecutedException
	 */
	public void updateEingabeeinheit(int[] recordKey, Project project,
			Vector<String> fields, int[] ArtefaktKey, int[] massKey,
			String phase1, String phase2, String phase3, String phase4,
			Vector<String> values) throws StatementNotExecutedException {

		String query = "UPDATE " + databaseName + ".eingabeeinheit SET " //$NON-NLS-1$ //$NON-NLS-2$
				+ "ProjNr=" + project.getNumber() + ",DBNummerProjekt=" //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getDatabaseNumber()
				+ ",BenutzerName=SUBSTRING(USER(),1, LOCATE('@', USER())-1)" //$NON-NLS-1$
				+ ",ArtefaktID=" + ArtefaktKey[0] + ",DBNummerArtefakt=" //$NON-NLS-1$ //$NON-NLS-2$
				+ ArtefaktKey[1] + ",massID=" + massKey[0] + ",DBNummerMasse=" //$NON-NLS-1$ //$NON-NLS-2$
				+ massKey[1] + ",phase1=" + phase1 + ",phase2=" + phase2 //$NON-NLS-1$ //$NON-NLS-2$
				+ ",phase3=" + phase3 + ",phase4=" + phase4 //$NON-NLS-1$ //$NON-NLS-2$
				+ ",Zustand='ge\u00E4ndert'"; //$NON-NLS-1$

		for (int i = 0; i < fields.size(); i++) {
			query = query + "," + fields.elementAt(i) + "='" //$NON-NLS-1$ //$NON-NLS-2$
					+ values.elementAt(i) + "'"; //$NON-NLS-1$
		}

		query = query + " WHERE ID =" + recordKey[0] + " AND Datenbanknummer=" //$NON-NLS-1$ //$NON-NLS-2$
				+ recordKey[1] + ";"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to read project
	 * @param phase
	 * @param recordKey
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getPhaseValue(String phase, int[] recordKey)
			throws StatementNotExecutedException {
		String query = "SELECT " + phase + " FROM " + databaseName //$NON-NLS-1$ //$NON-NLS-2$
				+ ".eingabeeinheit WHERE ID=" + "'" + recordKey[0] //$NON-NLS-1$ //$NON-NLS-2$
				+ "' AND Datenbanknummer=" + recordKey[1] //$NON-NLS-1$
				+ " AND geloescht='N';"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getString(1);
			}
			return "";
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to read project
	 * @param recordKey
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String[][] getDataRecord(int[] recordKey)
			throws StatementNotExecutedException {
		String query = "SELECT * FROM " + databaseName //$NON-NLS-1$
				+ ".eingabeeinheit WHERE ID='" + recordKey[0] //$NON-NLS-1$
				+ "' AND Datenbanknummer=" + recordKey[1] //$NON-NLS-1$
				+ " AND geloescht='N';"; //$NON-NLS-1$
		String[][] result = new String[0][0];
		try {
			ResultSet rs = executeSelect(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			if (rs.next()) {
				int columnCount = rsmd.getColumnCount();
				result = new String[columnCount][2];
				for (int i = 0; i < columnCount; i++) {
					result[i][0] = rsmd.getColumnName(i + 1);
					result[i][1] = rs.getString(i + 1);
				}
			}
			return result;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to read project
	 * @param phasenlabel
	 * @param project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String[][] getFK(String phasenlabel, Project project)
			throws StatementNotExecutedException {
		String query = "SELECT fK," + phasenlabel + " FROM " + databaseName //$NON-NLS-1$ //$NON-NLS-2$
				+ ".eingabeeinheit WHERE projNr=" + project.getNumber() //$NON-NLS-1$
				+ " AND DBNummerProjekt=" + project.getDatabaseNumber() //$NON-NLS-1$
				+ " AND geloescht='N' GROUP BY fK ORDER BY " + phasenlabel //$NON-NLS-1$
				+ ",fK;"; //$NON-NLS-1$
		String[][] phase;
		try {
			int i = 0;
			Vector<String> fk = new Vector<String>();
			Vector<String> label = new Vector<String>();
			ResultSet rs = executeSelect(query);
			while (rs.next()) {
				fk.add(rs.getString("fK")); //$NON-NLS-1$
				label.add(rs.getString(phasenlabel));
				i++;
			}
			phase = new String[i][2];
			for (int j = 0; j < i; j++) {
				phase[j][0] = fk.get(j);
				phase[j][1] = label.get(j);
			}
			return phase;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to write in project
	 * @param recordID
	 * @throws StatementNotExecutedException
	 */
	public void deleteFromEingabeeinheit(int[] recordID)
			throws StatementNotExecutedException {
		String query = "UPDATE " //$NON-NLS-1$
				+ databaseName
				+ ".eingabeeinheit SET geloescht='Y', Zustand='ge\u00E4ndert' WHERE ID='" //$NON-NLS-1$
				+ recordID[0] + "' AND Datenbanknummer=" + recordID[1] + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to write in project
	 * @param phasenlabel
	 * @param content
	 * @param label
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void update(String phasenlabel, String content, String label,
			Project project) throws StatementNotExecutedException {
		String query = "UPDATE " + databaseName + ".eingabeeinheit SET " //$NON-NLS-1$ //$NON-NLS-2$
				+ phasenlabel + "='" + content + "', Zustand='ge\u00E4ndert'" //$NON-NLS-1$ //$NON-NLS-2$
				+ " WHERE fK LIKE('" + label + "') AND ProjNr=" + project.getNumber() //$NON-NLS-1$ //$NON-NLS-2$
				+ " AND DBNummerProjekt=" + project.getDatabaseNumber() + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to read project
	 * @param recordKey
	 *            : Id of a data record in the table eingabeeinheit
	 * @return the ID of the data record in the Masse table which belongs to the
	 *         data set with the given datasetId in the table eingabeeinheit
	 * @throws StatementNotExecutedException
	 */
	public int[] getMassId(int[] recordKey)
			throws StatementNotExecutedException {
		String query = "SELECT massID, DBNummerMasse FROM " + databaseName //$NON-NLS-1$
				+ ".eingabeeinheit WHERE ID=" + recordKey[0] //$NON-NLS-1$
				+ " AND Datenbanknummer=" + recordKey[1] //$NON-NLS-1$
				+ " AND geloescht='N';"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			int[] id = new int[2];
			if (rs.next()) {
				id[0] = rs.getInt("massID"); //$NON-NLS-1$
				id[1] = rs.getInt("DBNummerMasse"); //$NON-NLS-1$
			}
			return id;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get all eingabeeinheit entries which have been changed
	 * 
	 * @param project
	 * @param columntypes
	 *            columns of global database -> needed to be independet from
	 *            order and global database scheme changes
	 * @param event
	 *            - QueryManager.CHANGES_LOCAL, QueryManager.CHANGES_GLOBAL,
	 *            QueryManager.ALL_GLOBAL
	 * @param lastSynchronisation
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getEntries(Project project,
			String[][] columntypes, int event, String lastSynchronisation)
			throws StatementNotExecutedException {
		Vector<String[]> result;
		String condition = " WHERE eingabeeinheit.DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber() + " AND projNr=" + project.getNumber(); //$NON-NLS-1$
		if (event == QueryManager.CHANGES_LOCAL) {
			condition = condition
					+ " AND eingabeeinheit.Zustand='ge\u00E4ndert' AND eingabeeinheit.Nachrichtennummer != -1"; //$NON-NLS-1$
		} else if (event == QueryManager.CHANGES_GLOBAL) {
			condition = condition + " AND eingabeeinheit.Zustand>'" //$NON-NLS-1$
					+ lastSynchronisation + "'"; //$NON-NLS-1$
		}
		String[] tables = { databaseName + ".eingabeeinheit" }; //$NON-NLS-1$
		result = getEntries(columntypes, tables, condition);
		return result;
	}

	/**
	 *
	 * @param columns
	 *            of global database -> needed to be independet from order and
	 *            global database scheme changes
	 * @param projectValues
	 *            of global database -> needed to be independet from global
	 *            database scheme changes
	 * @param key
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getUnitEntry(String[][] columns,
			int[] projectValues, Vector<String> key)
			throws StatementNotExecutedException {
		String condition = " WHERE "; //$NON-NLS-1$
		for (int i = 0; i < key.size(); i++) {
			condition = condition + key.elementAt(i) + "=" //$NON-NLS-1$
					+ projectValues[i];
			if (i < key.size() - 1) {
				condition = condition + " AND "; //$NON-NLS-1$
			}
		}
		String[] tableNames = { databaseName + ".eingabeeinheit" }; //$NON-NLS-1$
		return getEntries(columns, tableNames, condition);
	}

	public void insertData(String[] scheme, String[] data)
			throws StatementNotExecutedException {
		insertData(scheme, data, false);
	}
	
	public void insertData(String[] scheme, String[] data, boolean ignore)
			throws StatementNotExecutedException {
		insertData(scheme, data, databaseName + ".eingabeeinheit", ignore); //$NON-NLS-1$
	}

	public int updateData(String[] scheme, String[] data, Vector<String> key)
			throws StatementNotExecutedException {
		return updateData(scheme, data, databaseName + ".eingabeeinheit", key); //$NON-NLS-1$
	}

	/**
	 * set Zustand to 'synchronisiert' after server affirmed changing global
	 * database
	 * 
	 * @precondition executed on local database during synchronization
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void setSynchronized(Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " //$NON-NLS-1$
				+ databaseName
				+ ".eingabeeinheit SET Zustand='synchronisiert', eingabeeinheit.Nachrichtennummer=-1 WHERE " //$NON-NLS-1$
				+ "ProjNr = '" + project.getNumber() + "' AND DBNummerProjekt=" //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getDatabaseNumber() + " AND eingabeeinheit.Nachrichtennummer!=-1;"; //$NON-NLS-1$
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	public String getLastChange(Vector<String> key, int[] keyValues)
			throws StatementNotExecutedException {
		return getStatus(key, keyValues, databaseName + ".eingabeeinheit"); //$NON-NLS-1$
	}

	public void setMessageNumber(int message, Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " + databaseName //$NON-NLS-1$
				+ ".eingabeeinheit SET Nachrichtennummer=" + message //$NON-NLS-1$
				+ " WHERE ProjNr=" + project.getNumber() + " AND DBNummerProjekt=" //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getDatabaseNumber() + " AND Zustand='ge\u00E4ndert';"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	public int getNextFreeIndividualNr(Project project)
			throws StatementNotExecutedException {
		String query = "SELECT MAX(knIndNr) AS knnr FROM " + databaseName //$NON-NLS-1$
				+ ".eingabeeinheit WHERE projNr=" + project.getNumber() //$NON-NLS-1$
				+ " AND DBNummerProjekt=" + project.getDatabaseNumber() //$NON-NLS-1$
				+ " AND geloescht='N';"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getInt("knnr"); //$NON-NLS-1$
			}
			return -1;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

}
