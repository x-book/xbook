package ossobook.queries;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages version code tables except of "tierart" and "skelteil" (makes
 * inserts, deletes, updates and select statements on the tables)
 * 
 * @author j.lamprecht
 * 
 */
class DefinitionManager extends TableManager {

	public DefinitionManager(Connection con, String databaseName) {
		super(con, databaseName);
	}

	/**
	 * gets changed definition entries which have been changed
	 * 
	 * @param tableName
	 * @param scheme
	 *            columns of global database -> needed to be independent from
	 *            order and global database scheme changes
	 * @param lastSynchronization
	 * @param event
	 *            - QueryManager.CHANGES_LOCAL, QueryManager.CHANGES_GLOBAL,
	 *            QueryManager.ALL_GLOBAL
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getDefinitionData(String tableName,
			String[][] scheme, String lastSynchronization, int event)
			throws StatementNotExecutedException {
		String[] tableNames = { tableName };
		String condition = ""; //$NON-NLS-1$
		if (event == QueryManager.CHANGES_GLOBAL) {
			condition = " WHERE Zustand>" + lastSynchronization; //$NON-NLS-1$
		}
		return getEntries(scheme, tableNames, condition);
	}

	/**
	 * get last time when table has been changed
	 * 
	 * @param tableName
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getLastSynchronization(String tableName) throws StatementNotExecutedException {
		String query = String.format("SELECT Zustand FROM %s LIMIT 1", tableName); //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getString(1);
			}
			return "0";
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	public int updateDefinitionData(String[] scheme, String[] data, Vector<String> key, String tableName)
			throws StatementNotExecutedException
	{
		return updateData(scheme, data, databaseName + "." + tableName, key);
	}

	public void insertDefinitionData(String[] scheme, String[] data,
			String tableName) throws StatementNotExecutedException {
		insertData(scheme, data, tableName);
	}

	/**
	 * set Zustand to 'synchronisiert' after adapting local definition table
	 * 
	 * @precondition executed on local database during synchronization
	 * @param tableName
	 * @param lastSynchronization
	 * @throws StatementNotExecutedException
	 */
	public void setLastSynchronizationForDefinitionTable(String tableName,
			String lastSynchronization) throws StatementNotExecutedException {
		String query = String.format("UPDATE %s SET Zustand=%s;", tableName, lastSynchronization); //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * definition table records definitely deleted from database
	 * 
	 * @precondition method is executed on local database during synchronization
	 * @param tableName
	 * @throws StatementNotExecutedException
	 */
	public void deleteDefinitionPermanent(String tableName)
			throws StatementNotExecutedException {
		String query = "DELETE FROM " + tableName + " WHERE geloescht='Y'"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}
}
