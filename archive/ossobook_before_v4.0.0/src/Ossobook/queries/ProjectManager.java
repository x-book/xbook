package ossobook.queries;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import ossobook.client.base.metainfo.Project;
import ossobook.exceptions.NoWriteRightException;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages projekt table (makes inserts, deletes, updates and select statements
 * on the table)
 * 
 * @author j.lamprecht
 * 
 */
class ProjectManager extends TableManager {

	private final UserManager user;
	private final int versionDatabaseNumber;

	public ProjectManager(Connection con, UserManager user, int databaseNumber,
			String databaseName) {
		super(con, databaseName);
		this.user = user;
		versionDatabaseNumber = databaseNumber;
	}

	/**
	 * check project already exists in database
	 * 
	 * @param project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public boolean projectExists(Project project)
			throws StatementNotExecutedException {
		String query = "SELECT COUNT(*) FROM " + databaseName //$NON-NLS-1$
				+ ".projekt WHERE projNr=" + project.getNumber() //$NON-NLS-1$
				+ " AND Datenbanknummer=" + project.getDatabaseNumber() + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		ResultSet rs;
		try {
			rs = executeSelect(query);
			if (rs.next()) {
				int count = rs.getInt(1);
				return count > 0;
			}
			return true;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get all projects for which user has at least read right
	 * 
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<Project> getProjects() throws StatementNotExecutedException {
		Vector<Project> plist = new Vector<Project>();
		ResultSet rs;
		String query; //$NON-NLS-1$
		if (user.getIsAdmin()) {
			query = "SELECT ProjNr, ProjName, Datenbanknummer FROM " //$NON-NLS-1$
					+ databaseName
					+ ".projekt WHERE geloescht='N' ORDER BY ProjName;"; //$NON-NLS-1$
		} else {
			query = "SELECT ProjNr, ProjName, Datenbanknummer FROM projekt WHERE geloescht='N' AND EXISTS " //$NON-NLS-1$
					+ "(SELECT * FROM projektrechte WHERE Benutzer=SUBSTRING(USER(),1, LOCATE('@', USER())-1) " //$NON-NLS-1$
					+ "AND projekt.ProjNr=projektrechte.ProjNr AND projekt.Datenbanknummer=projektrechte.DBNummerProjekt)" //$NON-NLS-1$
					+ " ORDER BY ProjName;"; //$NON-NLS-1$
		}

		try {
			rs = executeSelect(query);
			while (rs.next()) {
				plist.add(new Project(rs.getString(2), rs.getInt(1), rs
						.getInt(3)));
			}
			return plist;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user may at least read project with given projNr
	 * @param project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String[][] getProjektInformations(Project project)
			throws StatementNotExecutedException {
		ResultSet rs;
		ResultSetMetaData rsmd;
		String query = "SELECT * FROM " + databaseName + ".projekt " //$NON-NLS-1$ //$NON-NLS-2$
				+ "WHERE ProjNr=" + project.getNumber() + " AND Datenbanknummer=" //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getDatabaseNumber() + " AND geloescht='N';"; //$NON-NLS-1$
		String[][] info = new String[0][0];

		try {
			rs = executeSelect(query);
			rsmd = rs.getMetaData();
			if (rs.next()) {
				int columnCount = rsmd.getColumnCount();
				info = new String[columnCount][2];
	
				for (int i = 1; i <= columnCount; i++) {
					info[i - 1][0] = rsmd.getColumnName(i);
					info[i - 1][1] = rs.getString(i);
				}
			}
			return info;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @postcondition ProjEigentuemer is current user (trigger in database)
	 * @param ProjektName
	 * @throws StatementNotExecutedException
	 */
	public void newProject(String ProjektName)
			throws StatementNotExecutedException {
		String query = "INSERT INTO " //$NON-NLS-1$
				+ databaseName
				+ ".projekt (ProjEigentuemer, projName, Datenbanknummer, Zustand, zuletztSynchronisiert)" //$NON-NLS-1$
				+ "VALUES " + "(SUBSTRING(USER(),1, LOCATE('@', USER())-1), '" //$NON-NLS-1$ //$NON-NLS-2$
				+ ProjektName + "', " + versionDatabaseNumber //$NON-NLS-1$
				+ ", 'ge\u00E4ndert', NOW()+0);"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @param project
	 * @throws StatementNotExecutedException
	 * @throws NoWriteRightException
	 */
	public void deleteProject(Project project)
			throws StatementNotExecutedException, NoWriteRightException {
		if (user.getIsAdmin() || user.getRight(project) == UserManager.WRITE) { //$NON-NLS-1$
			String query = "UPDATE " //$NON-NLS-1$
					+ databaseName
					+ ".projekt SET geloescht='Y', Zustand='ge\u00E4ndert' WHERE ProjNr='" //$NON-NLS-1$
					+ project.getNumber() + "' AND Datenbanknummer=" + project.getDatabaseNumber() //$NON-NLS-1$
					+ ";"; //$NON-NLS-1$
			try {
				executeQuery(query);
			} catch (SQLException e) {
				log.error("MySQL Error occurred.", e);
				throw new StatementNotExecutedException(query);
			}
			
			query = "UPDATE projektrechte SET geloescht='Y', Zustand='ge\u00E4ndert' WHERE ProjNr='" //$NON-NLS-1$
					+ project.getNumber()
					+ "' AND DBNummerProjekt=" //$NON-NLS-1$
					+ project.getDatabaseNumber()
					+ ";"; //$NON-NLS-1$
			try {
				executeQuery(query);
			} catch (SQLException e) {
				log.error("MySQL Error occurred.", e);
				throw new StatementNotExecutedException(query);
			}
		} else {
			throw new NoWriteRightException(getProjectName(project)); //$NON-NLS-1$
		}
	}

	/**
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void deleteProjectPermanent(Project project, int event)
			throws StatementNotExecutedException {
		String condition = ""; //$NON-NLS-1$
		if (event == QueryManager.DELETE_DELETED) {
			condition = " AND geloescht='Y' AND Zustand='synchronisiert'"; //$NON-NLS-1$
		}
		String query = "DELETE FROM " + databaseName //$NON-NLS-1$
				+ ".projekt WHERE ProjNr='" + project.getNumber() //$NON-NLS-1$
				+ "' AND Datenbanknummer=" + project.getDatabaseNumber() + " " + condition //$NON-NLS-1$ //$NON-NLS-2$
				+ ";"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
		
		query = "DELETE FROM " + databaseName + ".projektrechte WHERE ProjNr='" //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getNumber() + "' AND DBNummerProjekt=" + project.getDatabaseNumber() //$NON-NLS-1$
				+ " " + condition + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get the name of a project to a given project number
	 * 
	 * @param project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getProjectName(Project project)
			throws StatementNotExecutedException {
		String query = "SELECT ProjName FROM " + databaseName //$NON-NLS-1$
				+ ".projekt WHERE ProjNr=" + project.getNumber() //$NON-NLS-1$
				+ " AND Datenbanknummer=" + project.getDatabaseNumber() + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			ResultSet rs = executeSelect(query);
			rs.next();
			return rs.getString(1);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition: users has right to write on project
	 * @param label
	 * @param text
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void updateProjekt(String label, String text, Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " + databaseName + ".projekt SET " + label //$NON-NLS-1$ //$NON-NLS-2$
				+ "='" + text + "', Zustand='ge\u00E4ndert'" + " WHERE ProjNr=" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ project.getNumber() + " AND Datenbanknummer=" + project.getDatabaseNumber() + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			// FELD Eintragen
			executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition: current user may read the project
	 * @param projName
	 * @return the number to the given projectName
	 * @throws StatementNotExecutedException
	 */
	public Project getProject(String projName)
			throws StatementNotExecutedException {
		String query = "SELECT ProjNr, Datenbanknummer FROM " + databaseName //$NON-NLS-1$
				+ ".projekt WHERE " + "ProjName='" + projName + "' AND geloescht='N';"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return new Project(projName, rs.getInt(1), rs.getInt(2));
			}
			return null;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 *
	 * @param columns
	 *            of global database -> needed to be independet from order and
	 *            global database scheme changes
	 * @param projectValues
	 *            of global database -> needed to be independet from global
	 *            database scheme changes
	 * @param key
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getProjectEntry(String[][] columns,
			int[] projectValues, Vector<String> key)
			throws StatementNotExecutedException {
		String condition = " WHERE "; //$NON-NLS-1$
		for (int i = 0; i < key.size(); i++) {
			condition = condition + key.elementAt(i) + "=" //$NON-NLS-1$
					+ projectValues[i];
			if (i < key.size() - 1) {
				condition = condition + " AND "; //$NON-NLS-1$
			}
		}
		String[] tableNames = { databaseName + ".projekt" }; //$NON-NLS-1$
		return getEntries(columns, tableNames, condition);
	}

	/**
	 * 
	 * @param project
	 * @return whether the logged in user is owner of the project with the given
	 *         number
	 * @throws StatementNotExecutedException
	 */
	public boolean isProjectOwner(Project project)
			throws StatementNotExecutedException {
		String query = "SELECT ProjEigentuemer=SUBSTRING(USER(),1, LOCATE('@', USER())-1) " //$NON-NLS-1$
				+ "FROM " //$NON-NLS-1$
				+ databaseName
				+ ".projekt WHERE ProjNr=" //$NON-NLS-1$
				+ project.getNumber()
				+ " AND Datenbanknummer=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND geloescht='N';"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
            return rs.next() && rs.getInt(1) == 1;
			} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get all eingabeeinheit entries which have been changed
	 * 
	 * @param project
	 * @param columntypes
	 *            columns of global database -> needed to be independet from
	 *            order and global database scheme changes
	 * @param event
	 *            - QueryManager.CHANGES_LOCAL, QueryManager.CHANGES_GLOBAL,
	 *            QueryManager.ALL_GLOBAL
	 * @param lastSynchronisation
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getProjectEntries(Project project,
			String[][] columntypes, int event, String lastSynchronisation)
			throws StatementNotExecutedException {
		Vector<String[]> result;
		String[] tables = { databaseName + ".projekt" }; //$NON-NLS-1$
		String condition = " WHERE projNr=" + project.getNumber() //$NON-NLS-1$
				+ " AND projekt.Datenbanknummer=" + project.getDatabaseNumber() + " "; //$NON-NLS-1$ //$NON-NLS-2$
		if (event == QueryManager.CHANGES_LOCAL) {
			condition = condition
					+ " AND projekt.Zustand LIKE 'ge\u00E4ndert' AND projekt.Nachrichtennummer != -1"; //$NON-NLS-1$
		} else if (event == QueryManager.CHANGES_GLOBAL) {
			condition = condition + " AND projekt.Zustand>'" //$NON-NLS-1$
					+ lastSynchronisation + "'"; //$NON-NLS-1$
		}
		result = getEntries(columntypes, tables, condition);
		return result;
	}

	/**
	 * 
	 * @param project
	 *            of global database -> needed to be independet from global
	 *            database scheme changes
	 * @param columntypes
	 *            of global database -> needed to be independet from order and
	 *            global database scheme changes
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getRightEntries(Project project,
			String[][] columntypes) throws StatementNotExecutedException {
		Vector<String[]> result;
		String condition = " WHERE projNr=" + project.getNumber() //$NON-NLS-1$
				+ " AND projektrechte.DBNummerProjekt=" + project.getDatabaseNumber(); //$NON-NLS-1$
		String[] tables = { databaseName + ".projektrechte" }; //$NON-NLS-1$
		result = getEntries(columntypes, tables, condition);
		return result;
	}

	public void insertProjectData(String[] scheme, String[] data)
			throws StatementNotExecutedException {
		insertProjectData(scheme, data, false);
	}
	
	public void insertProjectData(String[] scheme, String[] data, boolean ignore)
			throws StatementNotExecutedException {
		insertData(scheme, data, databaseName + ".projekt", ignore); //$NON-NLS-1$
	}

	public void insertRightData(String[] scheme, String[] data, boolean ignore)
			throws StatementNotExecutedException {
		insertData(scheme, data, databaseName + ".projektrechte", ignore); //$NON-NLS-1$
	}

	/**
	 * set last time of synchronization for project after synchronization of
	 * project is finished
	 * 
	 * @param time
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void setLastSynchronizeTime(String time, Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " + databaseName //$NON-NLS-1$
				+ ".projekt SET zuletztSynchronisiert='" + time + "' " //$NON-NLS-1$ //$NON-NLS-2$
				+ "WHERE projNr='" + project.getNumber() + "' AND Datenbanknummer=" //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getDatabaseNumber() + ";"; //$NON-NLS-1$
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	public int updateProjectData(String[] scheme, String[] data,
			Vector<String> key) throws StatementNotExecutedException {
		return updateData(scheme, data, databaseName + ".projekt", key); //$NON-NLS-1$
	}

	/**
	 * set Zustand to 'synchronisiert' after server affirmed changing global
	 * database
	 * 
	 * @precondition executed on local database during synchronization
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void setSynchronized(Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " //$NON-NLS-1$
				+ databaseName
				+ ".projekt SET Zustand='synchronisiert', projekt.Nachrichtennummer=-1 WHERE " //$NON-NLS-1$
				+ "ProjNr = '" + project.getNumber() + "' AND Datenbanknummer=" //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getDatabaseNumber() + " AND projekt.Nachrichtennummer!=-1;"; //$NON-NLS-1$
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get last time of synchronization of project
	 * 
	 * @param project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getLastSynchronization(Project project)
			throws StatementNotExecutedException {
		String query = "SELECT zuletztSynchronisiert FROM " + databaseName //$NON-NLS-1$
				+ ".projekt WHERE" + " ProjNr=" + project.getNumber() //$NON-NLS-1$ //$NON-NLS-2$
				+ " AND Datenbanknummer=" + project.getDatabaseNumber() + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getString(1);
			}
			return null;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	public String getLastChange(Vector<String> key, int[] keyValues)
			throws StatementNotExecutedException {
		return getStatus(key, keyValues, databaseName + ".projekt"); //$NON-NLS-1$
	}

	/**
	 * assign message number to changed entries in artefaktmaske of given
	 * project
	 * 
	 * @param message
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void setMessageNumber(int message, Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " + databaseName //$NON-NLS-1$
				+ ".projekt SET Nachrichtennummer=" + message //$NON-NLS-1$
				+ " WHERE ProjNr=" + project.getNumber() + " AND Datenbanknummer=" //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getDatabaseNumber() + " AND Zustand='ge\u00E4ndert';"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

}
