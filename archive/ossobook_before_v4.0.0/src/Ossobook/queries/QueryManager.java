package ossobook.queries;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Vector;

import org.apache.commons.logging.LogFactory;

import ossobook.client.base.metainfo.Project;
import ossobook.client.io.database.ConnectionType;
import ossobook.client.io.database.DatabaseConnection;
import ossobook.exceptions.MetaStatementNotExecutedException;
import ossobook.exceptions.NoSuchMeasureCombinationException;
import ossobook.exceptions.NoWriteRightException;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * QueryManager is the interface between the gui and the database. It takes
 * requests of the gui and delegates them to the adequate TableManager, which
 * makes the database query.
 * 
 * @author j.lamprecht
 * 
 */

public class QueryManager {
	
	private final Connection						connection;
	private ProjectManager					projectManager;
	private DatabaseManager					databaseManager;
	private UserManager						userManager;
	private InputUnitManager					inputUnit;
	private MassManager						massManager;
	private ArtefactManager					artefactManager;
	private SpeciesManager					speciesManager;
	private SkeletonManager					skeletonManager;
	private SchemeManager						schemeManager;
	private DefinitionManager					definitionManager;
	
	public static final int							ALL_GLOBAL		= 1;
	public static final int							CHANGES_LOCAL	= 2;
	public static final int							CHANGES_GLOBAL	= 3;
	public static final int							DELETE_ALL		= 1;
	public static final int							DELETE_DELETED	= 2;
	
	public static final int							ISOLATION_LEVEL	= Connection.TRANSACTION_SERIALIZABLE;
	
	private final String								databaseName;
	private final ConnectionType connectionType;

	QueryManager(DatabaseConnection databaseConnection) {
		this.connection = databaseConnection.getUnderlyingConnection();
		this.connectionType = databaseConnection.getConnectionType();
		this.databaseName = databaseConnection.getDatabase();
		try {
			databaseManager = new DatabaseManager(connection, databaseName);
			int databaseNumber = databaseManager.getDatabaseNumber();
			userManager = new UserManager(connection, databaseName);
			inputUnit = new InputUnitManager(connection, databaseNumber,
					databaseName);
			massManager = new MassManager(connection, databaseNumber,
					databaseName);
			artefactManager = new ArtefactManager(connection, databaseNumber,
					databaseName);
			speciesManager = new SpeciesManager(connection, databaseName);
			skeletonManager = new SkeletonManager(connection, databaseName);
			projectManager = new ProjectManager(connection, userManager,
					databaseNumber, databaseName);
			schemeManager = new SchemeManager(connection, databaseName);
			definitionManager = new DefinitionManager(connection, databaseName);
		} catch (StatementNotExecutedException e) {
			LogFactory.getLog(QueryManager.class).error(e, e);
		}
	}
	
	public ConnectionType getConnectionType() {
		return connectionType;
	}

	public String getOssobookVersion() throws StatementNotExecutedException {
		return databaseManager.getOssobookVersion();
	}

	// using table version and nachrichten
	public String getDatabaseVersion() throws StatementNotExecutedException {
		return databaseManager.getDatabaseVersion();
	}

	String getSystemTime() throws StatementNotExecutedException {
		return databaseManager.getSystemTime();
	}

	public int getAssignedMessageNumber(Project project)
			throws StatementNotExecutedException {
		return databaseManager.getAssignedMessageNumber(project);
	}

	/**
	 * assign next nachrichtennummer for project changes
	 * 
	 * @param project
	 *            primary key of project
	 * @throws StatementNotExecutedException
	 */
	public void setMessageNumber(Project project)
			throws StatementNotExecutedException {
		int number = databaseManager.getMessageNumber();
		projectManager.setMessageNumber(number, project);
		inputUnit.setMessageNumber(number, project);
		massManager.setMessageNumber(number, project);
		artefactManager.setMessageNumber(number, project);
		databaseManager.increaseMessageNumber();
	}

	public boolean checkMessageNumber(int messageNumber, int databaseNumber, Project project)
			throws StatementNotExecutedException {
		return databaseManager
				.checkMessageNumber(messageNumber, databaseNumber, project);
	}

	/**
	 * memorize handled project changes
	 * 
	 * @param messageNumber
	 *            Nachrichtennummer of local database
	 * @param databaseNumber
	 *            Datenbanknummer of local database
	 * @param project
	 *            primary Key of project
	 * @throws StatementNotExecutedException
	 */
	public void updateMessageNumber(int messageNumber, int databaseNumber, Project project)
			throws StatementNotExecutedException {
		if (databaseManager.updateMessageNumber(messageNumber, databaseNumber, project) < 1) {
			databaseManager.insertMessageNumber(messageNumber, databaseNumber, project);
		}
	}

	public int getDatabaseNumber() throws StatementNotExecutedException {
		return databaseManager.getDatabaseNumber();
	}

	// using table projekt

	public String[][] getProjektInformations(Project project)
			throws StatementNotExecutedException {
		return projectManager.getProjektInformations(project);
	}

	/**
	 * create new project; on local database set right for project owner
	 * 
	 * @param ProjektName
	 * @throws StatementNotExecutedException
	 */
	public void newProject(String ProjektName)
			throws StatementNotExecutedException {
        projectManager.newProject(ProjektName);
        if (connectionType ==
            ConnectionType.CONNECTION_LOCAL)
        {
            userManager.changeRight(getProject(ProjektName), null,
                    UserManager.WRITE);
        }
	}

	/**
	 * delete all entries of project by changing "geloescht" field
	 * 
	 * @param project
	 * @throws StatementNotExecutedException
	 * @throws NoWriteRightException
	 */
	public void deleteProject(Project project)
			throws StatementNotExecutedException, NoWriteRightException {
		// L\u00F6sche Projekt aus Projekttabelle und Rechte zu Projekt
		projectManager.deleteProject(project);
		// L\u00F6sche Masse zu Projekt
		massManager.deleteMasse(project);
		// L\u00F6sche Artefakte zu Projekt
		artefactManager.deleteArtefaktMaske(project);
		// L\u00F6sche Knochen zu Projekt
		inputUnit.deleteUnit(project);
	}

	/**
	 * delete all project entries definitely
	 * 
	 * @precondition method is executed on local database during synchronization
	 * @param project
	 * @param event
	 * @throws StatementNotExecutedException
	 */
	public void deleteProjectPermanent(Project project, int event)
			throws StatementNotExecutedException {
		// L\u00F6sche Projekt aus Projekttabelle und Rechte zu Projekt
		projectManager.deleteProjectPermanent(project, event);
		// L\u00F6sche Masse zu Projekt
		massManager.deleteMassePermanent(project, event);
		// L\u00F6sche Artefakte zu Projekt
		artefactManager.deleteArtefaktPermanent(project, event);
		// L\u00F6sche Knochen zu Projekt
		inputUnit.deleteUnitPermanent(project, event);
	}

	public void updateProjekt(String label, String text, Project project)
			throws StatementNotExecutedException {
		projectManager.updateProjekt(label, text, project);
	}

	public Project getProject(String projName)
			throws StatementNotExecutedException {
		return projectManager.getProject(projName);
	}

	public boolean isProjectOwner(Project project)
			throws StatementNotExecutedException {
		return projectManager.isProjectOwner(project);
	}

	/**
	 * get global project entries for all project tables - including column
	 * names
	 * 
	 * @param project
	 * @param event
	 * @param lastSynchronisation
	 * @return Vector<String[][]> '- tables in order projekt, eingabeeinheit,
	 *         masse, artefaktmaske, projektrechte (and system time) '- single
	 *         data records '- single column content
	 * @throws StatementNotExecutedException
	 * @throws MetaStatementNotExecutedException
	 */
	public Vector<String[][]> getGlobalProjectData(Project project, int event,
			String lastSynchronisation) throws StatementNotExecutedException,
			MetaStatementNotExecutedException {
		Vector<String[][]> result;
        
        // get project information
        String[][] projectScheme = schemeManager
                .getColumnLabelsTypes("projekt"); //$NON-NLS-1$
        // get entries in eingabeeinheit to project
        String[][] inputScheme = schemeManager
                .getColumnLabelsTypes("eingabeeinheit"); //$NON-NLS-1$
        // get entries in masse to project
        String[][] massScheme = schemeManager.getColumnLabelsTypes("masse"); //$NON-NLS-1$
        // get entries in artefaktmaske to project
        String[][] artefactScheme = schemeManager
                .getColumnLabelsTypes("artefaktmaske"); //$NON-NLS-1$

        result = getProjectEntries(project, projectScheme, inputScheme,
                massScheme, artefactScheme, event, lastSynchronisation);

        // get entries in benutzer to project
        String[][] rightScheme = schemeManager
                .getColumnLabelsTypes("projektrechte"); //$NON-NLS-1$
        Vector<String[]> rightData = projectManager.getRightEntries(
                project, rightScheme);
        String[][] right = format(rightScheme, rightData);

        String databaseTime = getSystemTime();
        String[][] time = new String[1][1];
        time[0][0] = databaseTime;

        result.add(right);
        result.add(time);
		return result;
	}

	/**
	 * get project entries for all project tables - including column names
	 * 
	 * @param project
	 * @param projectScheme
	 *            scheme of table projekt (global)
	 * @param inputScheme
	 *            scheme of table eingabeeinheit (global)
	 * @param massScheme
	 *            scheme of table masse (global)
	 * @param artefactScheme
	 *            scheme of table artefaktmaske (global)
	 * @param event
	 *            : CHANGES_GLOBAL only changes of global project CHANGES_LOCAL
	 *            only changes of local project ALL_GLOBAL all entries of global
	 *            project
	 * 
	 * @param lastSynchronisation
	 * @return Vector<String[][]> '- different tables projekt, eingabeeinheit,
	 *         masse, artefaktmaske '- different data records '- diefferent
	 *         column content
	 * @throws StatementNotExecutedException
	 */
	private Vector<String[][]> getProjectEntries(Project project,
			String[][] projectScheme, String[][] inputScheme,
			String[][] massScheme, String[][] artefactScheme, int event,
			String lastSynchronisation) throws StatementNotExecutedException {
		Vector<String[][]> result = new Vector<String[][]>();

		Vector<String[]> projectData = projectManager.getProjectEntries(
				project, projectScheme, event, lastSynchronisation);
		String[][] projectdata = format(projectScheme, projectData);
		Vector<String[]> inputData = inputUnit.getEntries(project,
				inputScheme, event, lastSynchronisation);
		String[][] input = format(inputScheme, inputData);
		Vector<String[]> massData = massManager.getEntries(project,
				massScheme, event, lastSynchronisation);
		String[][] masse = format(massScheme, massData);
		Vector<String[]> artefactData = artefactManager.getEntries(project,
				artefactScheme, event, lastSynchronisation);
		String[][] artefact = format(artefactScheme, artefactData);

		result.add(projectdata);
		result.add(input);
		result.add(masse);
		result.add(artefact);

		return result;
	}

	/**
	 * merge scheme and data
	 * 
	 * @param scheme
	 * @param data
	 * @return
	 */
	private static String[][] format(String[][] scheme, Vector<String[]> data) {
		String[][] result = new String[data.size() + 1][scheme[0].length];
        System.arraycopy(scheme[0], 0, result[0], 0, scheme[0].length);
		for (int i = 0; i < data.size(); i++) {
			for (int j = 0; j < scheme[0].length; j++) {
				result[i + 1][j] = data.elementAt(i)[j];
			}
		}
		return result;
	}

	/**
	 * get all changes of local database used on client
	 * 
	 * @param project
	 * @param scheme
	 * @return
	 */
	public Vector<String[][]> getChangesOfProject(Project project,
			Vector<String[][]> scheme) {
		Vector<String[][]> result = new Vector<String[][]>();
		try {
			result = getProjectEntries(project, scheme.elementAt(0), scheme
					.elementAt(1), scheme.elementAt(2), scheme.elementAt(3),
					CHANGES_LOCAL, null);
		} catch (SQLException s) {
			s.printStackTrace();
		}
		return result;
	}

	/**
	 * insert changes
	 * 
	 * @precondition no conflicts can occur
	 * @param data
	 * @param project
	 * @param ignore ignore errors due to rows already existing.
	 */
	public void insertData(Vector<String[][]> data, Project project, boolean ignore) {
		try {
			for (int j = 1; j < data.elementAt(0).length; j++) {
				projectManager.insertProjectData(data.elementAt(0)[0], data
						.elementAt(0)[j], ignore);
			}
			for (int j = 1; j < data.elementAt(1).length; j++) {
				inputUnit
						.insertData(data.elementAt(1)[0], data.elementAt(1)[j], ignore);
			}
			for (int j = 1; j < data.elementAt(2).length; j++) {
				massManager.insertData(data.elementAt(2)[0],
						data.elementAt(2)[j], ignore);
			}
			for (int j = 1; j < data.elementAt(3).length; j++) {
				artefactManager.insertData(data.elementAt(3)[0], data
						.elementAt(3)[j], ignore);
			}

			for (int j = 1; j < data.elementAt(4).length; j++) {
				projectManager.insertRightData(data.elementAt(4)[0], data
						.elementAt(4)[j], ignore);
			}

			projectManager.setLastSynchronizeTime(data.elementAt(5)[0][0],
					project);
		} catch (SQLException s) {
			s.printStackTrace();
		}
	}

	/**
	 * first tries to update existing record, then (at fault) inserts as new
	 * record assumes checking for conflicts
	 * 
	 * @param data
	 * @param lastSynchronization
	 *            - null for changing local database
	 * @return Vector<Vector<String[]>>: '- different tables: projekt,
	 *         eingabeeinheit, masse, artefaktmaske '- different conflict
	 *         records in single table '- single data records (always even
	 *         number: first old record, second new record)
	 * @throws StatementNotExecutedException
	 * @throws MetaStatementNotExecutedException
	 */
	public Vector<Vector<String[]>> changeData(Vector<String[][]> data,
			String lastSynchronization) throws StatementNotExecutedException,
			MetaStatementNotExecutedException {

		Vector<String> projectKey = getPrimaryKeys("projekt"); //$NON-NLS-1$
		Vector<String> unitKey = getPrimaryKeys("eingabeeinheit"); //$NON-NLS-1$
		Vector<String> artefactKey = getPrimaryKeys("artefaktmaske"); //$NON-NLS-1$
		Vector<String> massKey = getPrimaryKeys("masse"); //$NON-NLS-1$

		Vector<Vector<String[]>> conflict = new Vector<Vector<String[]>>();
		Vector<String[]> projectConflicts = new Vector<String[]>();
		Vector<String[]> unitConflicts = new Vector<String[]>();
		Vector<String[]> massConflicts = new Vector<String[]>();
		Vector<String[]> artefactConflicts = new Vector<String[]>();

        for (int j = 1; j < data.elementAt(0).length; j++) {
            // conflict version only for changes in global database
            if (lastSynchronization != null) {
                int[] projectValues = getKeyValues(projectKey, data
                        .elementAt(0)[j], data.elementAt(0)[0]);
                // conflict?
                String lastChange = projectManager.getLastChange(
                        projectKey, projectValues);
                if (lastChange != null
                        && lastChange.compareTo(lastSynchronization) > 0) {
                    projectConflicts.add((projectManager
                            .getProjectEntry(data.elementAt(0),
                                    projectValues, projectKey))
                            .elementAt(0));
                    projectConflicts.add(data.elementAt(0)[j]);
                }
            }
            if (projectManager.updateProjectData(data.elementAt(0)[0], data
                    .elementAt(0)[j], projectKey) < 1) {
                projectManager.insertProjectData(data.elementAt(0)[0], data
                        .elementAt(0)[j]);
            }
        }
        conflict.add(projectConflicts);

        for (int j = 1; j < data.elementAt(1).length; j++) {
            // conflict management only for changes in global database
            if (lastSynchronization != null) {
                int[] unitKeyValues = getKeyValues(unitKey, data
                        .elementAt(1)[j], data.elementAt(1)[0]);
                // conflict?
                String lastChange = inputUnit.getLastChange(unitKey,
                        unitKeyValues);
                if (lastChange != null
                        && lastChange.compareTo(lastSynchronization) > 0) {
                    unitConflicts.add((inputUnit.getUnitEntry(data
                            .elementAt(1), unitKeyValues, unitKey))
                            .elementAt(0));
                    unitConflicts.add(data.elementAt(1)[j]);
                }
            }
            if (inputUnit.updateData(data.elementAt(1)[0], data
                    .elementAt(1)[j], unitKey) < 1) {
                inputUnit.insertData(data.elementAt(1)[0], data
                        .elementAt(1)[j]);
            }
        }
        conflict.add(unitConflicts);

        for (int j = 1; j < data.elementAt(2).length; j++) {
            // conflict version only for changes in global database
            if (lastSynchronization != null) {
                int[] massKeyValues = getKeyValues(massKey, data
                        .elementAt(2)[j], data.elementAt(2)[0]);
                // conflict?
                String lastChange = massManager.getLastChange(massKey,
                        massKeyValues);
                if (lastChange != null
                        && lastChange.compareTo(lastSynchronization) > 0) {
                    massConflicts.add((massManager.getMassEntry(data
                            .elementAt(2), massKeyValues, massKey))
                            .elementAt(0));
                    massConflicts.add(data.elementAt(2)[j]);
                }
            }
            if (massManager.updateData(data.elementAt(2)[0], data
                    .elementAt(2)[j], massKey) < 1) {
                massManager.insertData(data.elementAt(2)[0], data
                        .elementAt(2)[j]);
            }
        }
        conflict.add(massConflicts);

        for (int j = 1; j < data.elementAt(3).length; j++) {
            // conflict version only for changes in global database
            if (lastSynchronization != null) {
                int[] artefactKeyValues = getKeyValues(artefactKey, data
                        .elementAt(3)[j], data.elementAt(3)[0]);
                // conflict?
                String lastChange = artefactManager.getLastChange(
                        artefactKey, artefactKeyValues);
                if (lastChange != null
                        && lastChange.compareTo(lastSynchronization) > 0) {
                    artefactConflicts.add((artefactManager
                            .getArtefactEntry(data.elementAt(3),
                                    artefactKeyValues, artefactKey))
                            .elementAt(0));
                    artefactConflicts.add(data.elementAt(3)[j]);
                }
            }
            if (artefactManager.updateData(data.elementAt(3)[0], data
                    .elementAt(3)[j], artefactKey) < 1) {
                artefactManager.insertData(data.elementAt(3)[0], data
                        .elementAt(3)[j]);
            }
        }
        conflict.add(artefactConflicts);
		return conflict;
	}

	/**
	 * get values for given primary key fields
	 * 
	 * @param keys
	 *            primary key fields
	 * @param data
	 * @param scheme
	 * @return
	 */
	private static int[] getKeyValues(Vector<String> keys, String[] data,
			String[] scheme) {
		Vector<String> keyValues = new Vector<String>();
		for (int i = 0; i < keys.size(); i++) {
			for (int j = 0; j < scheme.length; j++) {
				if (scheme[j].substring(scheme[j].indexOf(".") + 1).equals( //$NON-NLS-1$
						keys.elementAt(i))) {
					keyValues.add(data[j]);
				}
			}
		}
		int[] result = new int[keyValues.size()];
		for (int i = 0; i < keyValues.size(); i++) {
			result[i] = Integer.parseInt(keyValues.elementAt(i));
		}
		return result;
	}

	public void setLastSynchronizeTime(String time, Project project)
			throws StatementNotExecutedException {
		projectManager.setLastSynchronizeTime(time, project);
	}

	/**
	 * 
	 * @return: projects the user can work with
	 * @throws StatementNotExecutedException
	 */
	public Vector<Project> getProjects() throws StatementNotExecutedException {
		return projectManager.getProjects();
	}

	/**
	 * change status for entries of tables projekt, eingeabeeinheit, masse and
	 * artefaktmaske belonging to project
	 * 
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void setSynchronized(Project project)
			throws StatementNotExecutedException {
		projectManager.setSynchronized(project);
		inputUnit.setSynchronized(project);
		massManager.setSynchronized(project);
		artefactManager.setSynchronized(project);
	}

	public String getProjectName(Project project)
			throws StatementNotExecutedException {
		return projectManager.getProjectName(project);
	}

	public String getLastSynchronizationOfProject(Project project)
			throws StatementNotExecutedException {
		return projectManager.getLastSynchronization(project);
	}

	public boolean projectExists(Project project)
			throws StatementNotExecutedException {
		return projectManager.projectExists(project);
	}

	public Vector<String> getUserNames() throws StatementNotExecutedException {
		return userManager.getUserNames();
	}

	public void changeRight(Project project, String user, int right)
			throws StatementNotExecutedException {
		userManager.changeRight(project, user, right);
	}

	public boolean getIsAdmin() throws StatementNotExecutedException {
		return userManager.getIsAdmin();
	}

	public int getRight(Project project)
			throws StatementNotExecutedException {
		return userManager.getRight(project);
	}

	public String[] getMail(Project project)
			throws StatementNotExecutedException {
		return userManager.getMail(project);
	}

	// using table eingabeeinheit
	public int getCountFk(String text, Project project)
			throws StatementNotExecutedException {
		return inputUnit.getCountFk(text, project);
	}

	public int getCountSkelNr(int number, Project project)
			throws StatementNotExecutedException {
		return inputUnit.getCountSkelNr(number, project);
	}

	public int getCountKnIndNr(int number, Project project)
			throws StatementNotExecutedException {
		return inputUnit.getCountKnIndNr(number, project);
	}

	public String getPhaseValue(String phase, int[] recordKey)
			throws StatementNotExecutedException {
		return inputUnit.getPhaseValue(phase, recordKey);
	}

	public int getBonesNumber(Project project)
			throws StatementNotExecutedException {
		return inputUnit.getBonesNumber(project);
	}

	public double getOverallWeight(Project project)
			throws StatementNotExecutedException {
		return inputUnit.getOverallWeight(project);
	}

	public String[][] getDataRecord(int[] recordKey)
			throws StatementNotExecutedException {
		return inputUnit.getDataRecord(recordKey);
	}

	public String[][] getFK(String phasenlabel, Project project)
			throws StatementNotExecutedException {
		return inputUnit.getFK(phasenlabel, project);
	}

	public void insertIntoEingabeeinheit(Project project,
			Vector<String> fields, int[] ArtefaktID, int[] massID,
			String phase1, String phase2, String phase3, String phase4,
			Vector<String> values) throws StatementNotExecutedException {

		inputUnit.insertIntoEingabeeinheit(project, fields, ArtefaktID,
				massID, phase1, phase2, phase3, phase4, values);
	}

	public void deleteFromEingabeeinheit(int[] recordID)
			throws StatementNotExecutedException {
		inputUnit.deleteFromEingabeeinheit(recordID);
	}

	public void update(String phasenlabel, String content, String label,
			Project project) throws StatementNotExecutedException {
		inputUnit.update(phasenlabel, content, label, project);
	}

	public void updateEingabeeinheit(int[] recordKey, Project project,
			Vector<String> fields, int[] ArtefactKey, int[] massKey,
			String phase1, String phase2, String phase3, String phase4,
			Vector<String> values) throws StatementNotExecutedException {
		inputUnit.updateEingabeeinheit(recordKey, project, fields,
				ArtefactKey, massKey, phase1, phase2, phase3, phase4, values);
	}

	// using tables masse and massabhaengigkeit
	public int[] insertIntoMasse(Vector<String> fieldsMasse,
			Vector<String> valuesMasse) throws StatementNotExecutedException {
		return massManager.insertIntoMasse(fieldsMasse, valuesMasse);
	}

	public String holeMassName(String s) throws StatementNotExecutedException {
		return massManager.holeMassName(s);
	}

	public String getMasse(int tierlabel, int skellabel)
			throws StatementNotExecutedException,
			NoSuchMeasureCombinationException {
		return massManager.getMasse(tierlabel, skellabel);
	}

	public String holeMassWert(String mass, int[] massKey)
			throws StatementNotExecutedException {
		return massManager.holeMassWert(mass, massKey);
	}

	public void deleteFromMasse(int[] massID)
			throws StatementNotExecutedException {
		massManager.deleteFromMasse(massID);
	}

	public void updateMasse(Vector<String> fieldsMasse, int[] massKey,
			Vector<String> valuesMasse) throws StatementNotExecutedException {
		massManager.updateMasse(fieldsMasse, massKey, valuesMasse);
	}

	public int[] getMassId(int[] recordKey)
			throws StatementNotExecutedException {
		return inputUnit.getMassId(recordKey);
	}

	// using table artefaktmaske
	public int[] insertIntoArtefaktmaske(Vector<String> fieldsA,
			Vector<String> valuesA) throws StatementNotExecutedException {
		return artefactManager.insertIntoArtefaktmaske(fieldsA, valuesA);
	}

	public void deleteFromArtefaktmaske(int[] artefactID)
			throws StatementNotExecutedException {
		artefactManager.deleteFromArtefaktmaske(artefactID);
	}

	public String[][] getArtefaktmaske(int[] artefactKey)
			throws StatementNotExecutedException {
		return artefactManager.getArtefaktmaske(artefactKey);
	}

	public void updateArtefaktmaske(Vector<String> fieldsA, int[] artefactKey,
			Vector<String> valuesA) throws StatementNotExecutedException {
		artefactManager.updateArtefaktmaske(fieldsA, artefactKey, valuesA);
	}

	public int[] getArtefaktId(int[] recordKey)
			throws StatementNotExecutedException {
		return artefactManager.getArtefaktKey(recordKey);
	}

	// using table tierart
	public void updateTierart(String TierCode, String TierName, String DTier,
			String TierFolgeAuswertung, String LB) {
		speciesManager.updateTierart(TierCode, TierName, DTier,
				TierFolgeAuswertung, LB);
	}

	/**
	 * Converts an animal ID to the correspondenting animal label.
	 * 
	 * @param animalId The animal ID which should be converted to a label.
	 * @return The correspondenting animal label.
	 * 
	 * @throws StatementNotExecutedException Thrown if a error with the SQL query appears.
	 * 
	 * @see ossobook.queries.SpeciesManager#getTierlabel(int)
	 */
	public int getTierlabel(int animalId) throws StatementNotExecutedException {
		return speciesManager.getTierlabel(animalId);
	}

	/**
	 * Converts an animal name to the correspondenting animal ID.
	 * 
	 * @param animalName The animal name which should be converted to an ID.
	 * @return The correspondenting animal ID.
	 * 
	 * @throws StatementNotExecutedException Thrown if a error with the SQL query appears.
	 * 
	 * @see ossobook.queries.SpeciesManager#getTierCode(String)
	 */
	public String getTierCode(String animalName) throws StatementNotExecutedException {
		return speciesManager.getTierCode(animalName);
	}
	
	/**
	 * Commit an String object including a (sub)string of an animal.
	 * The method searches in the database for all animal names which have 
	 * this string. The result is returned as a sorted Vector object.
	 * 
	 * @param substring The substring of the searched animal names.
	 * @return An Vector<String> object holding a sorted animal listing.
	 * @throws StatementNotExecutedException Thrown if a error with the SQL query appears.
	 * 
	 * @see ossobook.queries.SpeciesManager#getTierNameListingForSearch(String)
	 */
	public Vector<String> getTierlabelListingForSearch(String substring) throws StatementNotExecutedException {
		return speciesManager.getTierNameListingForSearch(substring);
	}
	
	/**
	 * Check if a specific animal name is available in the database or not.
	 * 
	 * @param animalName The animal name to check
	 * @return <code>true</code> if the animal name is available in the database, 
	 *         <code>false</code> if not.
	 * @throws StatementNotExecutedException Thrown if a error with the SQL query appears.
	 * 
	 * @see ossobook.queries.SpeciesManager#isTierlabelAvailable(String)
	 */
	public boolean isTierlabelAvailable(String animalName) throws StatementNotExecutedException {
		return speciesManager.isTierlabelAvailable(animalName);
	}

	// using table skelteil
	public int getSkelLB(int skelcode) throws StatementNotExecutedException {
		return skeletonManager.getSkelLB(skelcode);
	}

	// working with database scheme
	public Vector<String> getTables() throws MetaStatementNotExecutedException {
		return schemeManager.getTables();
	}

	public Vector<String[]> getTableDescription(String tableName)
			throws MetaStatementNotExecutedException {
		return schemeManager.getTableDescription(tableName);
	}

	public Vector<String> getPrimaryKeys(String tableName)
			throws MetaStatementNotExecutedException {
		return schemeManager.getPrimaryKeys(tableName);
	}

	public void createTable(String tableName, String[] columnNames,
			String[] types, String[] sizes, String[] nullable,
			String[] defaultValues, String[] autoincrement, String[] primaryKey)
			throws StatementNotExecutedException {
		schemeManager.createTable(tableName, columnNames, types, sizes,
				nullable, defaultValues, autoincrement, primaryKey);
	}

	public void changeColumn(String tableName, String columnName, String type,
			String size, String nullable, String defaultValues,
			String autoincrement) throws StatementNotExecutedException {
		schemeManager.changeColumn(tableName, columnName, type, size, nullable,
				defaultValues, autoincrement);
	}

	public void addColumn(String tableName, String columnName, String type,
			String size, String nullable, String defaultValues,
			String autoincrement) throws StatementNotExecutedException {
		schemeManager.addColumn(tableName, columnName, type, size, nullable,
				defaultValues, autoincrement);
	}

	public void addPrimaryKey(String tableName, String[] columns)
			throws StatementNotExecutedException {
		schemeManager.addPrimaryKey(tableName, columns);
	}

	public void changePrimaryKey(String tableName, String[] columns)
			throws StatementNotExecutedException {
		schemeManager.changePrimaryKey(tableName, columns);
	}
	
	// definition tables
	/**
	 * get definition entries - all or only changes
	 * 
	 * @param tableName
	 * @param lastSynchronization
	 * @return Vector<String[]> '- data records; last entry contains system time
	 *         '- column content
	 * @throws StatementNotExecutedException
	 * @throws MetaStatementNotExecutedException
	 */
	public Vector<String[]> getDefinitionData(String tableName,
			String lastSynchronization) throws StatementNotExecutedException,
			MetaStatementNotExecutedException {
		String[][] scheme = schemeManager.getColumnLabelsTypes(tableName);
		Vector<String[]> result;
		if (lastSynchronization != null) {
			result = definitionManager.getDefinitionData(
					tableName, scheme, lastSynchronization,
					QueryManager.CHANGES_GLOBAL);
		} else {
			result = definitionManager.getDefinitionData(
					tableName, scheme, lastSynchronization,
					QueryManager.ALL_GLOBAL);
		}
		String[] time = new String[1];
		time[0] = getSystemTime();
		result.add(time);
		return result;
	}
	
	public String getLastSynchronizationOfDefinitionTable(String tableName)
			throws StatementNotExecutedException {
		return definitionManager.getLastSynchronization(tableName);
	}

	/**
	 * insert changes of definition tables
	 * 
	 * @precondition user may change definition tables
	 * @param tableName
	 * @param data
	 * @param scheme
	 * @throws MetaStatementNotExecutedException
	 * @throws StatementNotExecutedException
	 */
	public void changeDefinitions(String tableName, Vector<String[]> data,
			String[] scheme) throws MetaStatementNotExecutedException,
			StatementNotExecutedException {
		Vector<String> key = getPrimaryKeys(tableName);
		for (int i = 0; i < scheme.length; i++) {
			scheme[i] = tableName + "." + scheme[i]; //$NON-NLS-1$
		}
		for (int i = 0; i < data.size() - 1; i++) {
			try {
				int updated = definitionManager.updateDefinitionData(scheme,
						data.elementAt(i), key, tableName);
				if (updated < 1) {
					definitionManager.insertDefinitionData(scheme, data
							.elementAt(i), tableName);
				}
			} catch (StatementNotExecutedException e) {
				definitionManager.insertDefinitionData(scheme, data
						.elementAt(i), tableName);
			}
		}
		definitionManager.setLastSynchronizationForDefinitionTable(tableName,
				data.lastElement()[0]);
	}

	public void deleteDefinitionPermanent(String tableName)
			throws StatementNotExecutedException {
		definitionManager.deleteDefinitionPermanent(tableName);
	}

	/**
	 * close connection to database
	 */
	public void closeConnection() {
		try {
			connection.close();
		} catch (SQLException sql) {
			sql.printStackTrace();
		}
	}

	public boolean isClosed() {
		try {
			return connection.isClosed();
		} catch (SQLException e) {
			e.printStackTrace();
			return true;
		}
	}

	public void commit() throws SQLException {
		connection.commit();
	}
	
	public void disableAutoCommit() {
		try {
			connection.setAutoCommit(false);
		} catch (SQLException s) {
			s.printStackTrace();
		}
	}

	public void enableAutoCommit() {
		try {
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException s) {
			s.printStackTrace();
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public int setIsolationLevel(int isolation) {
		try {
			int level = connection.getTransactionIsolation();
			connection.setTransactionIsolation(isolation);
			return level;
		} catch (SQLException s) {
			s.printStackTrace();
		}
		return -1;
	}

	public void lockDatabase() throws MetaStatementNotExecutedException,
			StatementNotExecutedException {
		Vector<String> tables = getTables();
		databaseManager.lockDatabase(databaseName, tables);
	}

	public void unlockDatabase() throws StatementNotExecutedException {
		databaseManager.unlockTables();
	}

	public int getNextFreeIndividualNr(Project project)
			throws StatementNotExecutedException {
		return inputUnit.getNextFreeIndividualNr(project);
	}

}
