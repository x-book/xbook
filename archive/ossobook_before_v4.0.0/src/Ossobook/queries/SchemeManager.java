package ossobook.queries;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.exceptions.MetaStatementNotExecutedException;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages database scheme
 * 
 * @author j.lamprecht
 * 
 */
class SchemeManager extends TableManager {
	private static final Log _log = LogFactory.getLog(SchemeManager.class);

	public SchemeManager(Connection con, String databaseName) {
		super(con, databaseName);
	}

	/**
	 * gets all table names of database
	 * 
	 * @return
	 * @throws MetaStatementNotExecutedException
	 */
	public Vector<String> getTables() throws MetaStatementNotExecutedException {
		Vector<String> tableNames = new Vector<String>();
		try {
			DatabaseMetaData database = connection.getMetaData();
			String[] tblTypes = { "TABLE" }; //$NON-NLS-1$
			ResultSet rs = database.getTables(null, connection.getCatalog(),
					"%", tblTypes); //$NON-NLS-1$
			while (rs.next()) {
				tableNames.add(rs.getString("TABLE_NAME")); //$NON-NLS-1$
			}
		} catch (SQLException sql) {
			printMetaErrorMessage(sql, "getTables"); //$NON-NLS-1$
		}

		return tableNames;
	}

	/**
	 * gets descriptions (name and type) of the columns of the given table
	 * 
	 * @param tableName
	 * @return
	 * @throws MetaStatementNotExecutedException
	 */
	public String[][] getColumnLabelsTypes(String tableName)
			throws MetaStatementNotExecutedException {
		String[][] result = null;
		try {
			Vector<String> columns = new Vector<String>();
			Vector<String> types = new Vector<String>();
			DatabaseMetaData database = connection.getMetaData();
			ResultSet rs = database.getColumns(null, null, tableName, "%"); //$NON-NLS-1$

			while (rs.next()) {
				columns.add(tableName + "." + rs.getString("COLUMN_NAME")); //$NON-NLS-1$ //$NON-NLS-2$
				types.add(rs.getString("TYPE_NAME")); //$NON-NLS-1$
			}

			result = new String[2][columns.size()];
			columns.toArray(result[0]);
			types.toArray(result[1]);

		} catch (SQLException sql) {
			printMetaErrorMessage(sql, "getColumnLabelsTypes"); //$NON-NLS-1$
		}
		return result;
	}

	/**
	 * gets the description of the given table - consisting of: columns: name,
	 * type, default value, is nullable and autoincrement
	 * 
	 * @param tableName
	 * @return
	 * @throws MetaStatementNotExecutedException
	 */
	public Vector<String[]> getTableDescription(String tableName)
			throws MetaStatementNotExecutedException {

		Vector<String[]> result = new Vector<String[]>();
		try {
			DatabaseMetaData database = connection.getMetaData();
			ResultSet rs = database.getColumns(null, null, tableName, "%"); //$NON-NLS-1$
			String[] elements;
			// rs.getString("IS_AUTOINCREMENT"); just works since younger
			// database version
			// or with extra patch
			String query = "Explain " + tableName + ";"; //$NON-NLS-1$ //$NON-NLS-2$
			ResultSet rsHelp = executeSelect(query);
			String extra;
			while (rs.next()) {
				rsHelp.next();
				elements = new String[6];
				elements[0] = rs.getString("COLUMN_NAME"); //$NON-NLS-1$
				elements[1] = rs.getString("TYPE_NAME"); //$NON-NLS-1$

				if (elements[1].equals("int")) { //$NON-NLS-1$
					extra = rsHelp.getString("Type"); //$NON-NLS-1$
					elements[2] = extra.substring(4, extra.length() - 1);
				} else if (elements[1].equals("enum")) { //$NON-NLS-1$
					extra = rsHelp.getString("Type"); //$NON-NLS-1$
					elements[2] = extra.substring(5, extra.length() - 1);
				} else {
					elements[2] = String.valueOf(rs.getInt("COLUMN_SIZE")); //$NON-NLS-1$
				}

				elements[3] = rs.getString("COLUMN_DEF"); //$NON-NLS-1$
				elements[4] = rs.getString("IS_NULLABLE"); //$NON-NLS-1$

				extra = rsHelp.getString("Extra"); //$NON-NLS-1$
				if (extra.contains("auto_increment")) { //$NON-NLS-1$
					elements[5] = "YES"; //$NON-NLS-1$
				} else {
					elements[5] = "NO"; //$NON-NLS-1$
				}
				// elements[5]=rs.getString("IS_AUTOINCREMENT");
				result.add(elements);
			}
		} catch (SQLException sql) {
			printMetaErrorMessage(sql, "getTableDescription"); //$NON-NLS-1$
		}

		return result;
	}

	/**
	 * gets the primary key fields of the given table
	 * 
	 * @param tableName
	 * @return
	 * @throws MetaStatementNotExecutedException
	 */
	public Vector<String> getPrimaryKeys(String tableName)
			throws MetaStatementNotExecutedException {
		Vector<String> result = new Vector<String>();
		try {
			DatabaseMetaData database = connection.getMetaData();
			ResultSet rs = database.getPrimaryKeys(null, connection
					.getCatalog(), tableName);
			while (rs.next()) {
				result.add(rs.getString("COLUMN_NAME")); //$NON-NLS-1$
			}
		} catch (SQLException sql) {
			printMetaErrorMessage(sql, "getPrimaryKeys"); //$NON-NLS-1$
		}
		return result;
	}

	/**
	 * creates a new table with the given column attributes and primary key
	 * 
	 * @param tableName
	 * @param columnNames
	 * @param types
	 * @param sizes
	 * @param nullable
	 *            - column can be null
	 * @param defaultValues
	 *            - column has a default value
	 * @param autoincrement
	 *            - column is autoincrement
	 * @param primaryKey
	 * @throws StatementNotExecutedException
	 */
	public void createTable(String tableName, String[] columnNames,
			String[] types, String[] sizes, String[] nullable,
			String[] defaultValues, String[] autoincrement, String[] primaryKey)
			throws StatementNotExecutedException {
		String query = "CREATE TABLE " + tableName + " ("; //$NON-NLS-1$ //$NON-NLS-2$
		for (int i = 0; i < columnNames.length; i++) {
			query = query
					+ columnOptions(columnNames[i], types[i], sizes[i],
							nullable[i], defaultValues[i], autoincrement[i]);
			query = query + ", "; //$NON-NLS-1$
		}
		query = query + "PRIMARY KEY ("; //$NON-NLS-1$
		for (int i = 0; i < primaryKey.length; i++) {
			query = query + primaryKey[i];
			if (!(i == primaryKey.length - 1)) {
				query = query + ", "; //$NON-NLS-1$
			} else {
				query = query + ") "; //$NON-NLS-1$
			}
		}

		query = query + ");"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * gets the description of the default value
	 * 
	 * @param defaultValue
	 * @param type
	 * @return
	 */
	private static String getDefaultValueString(String defaultValue, String type) {
		if (type.equals("text") || type.equals("enum")) { //$NON-NLS-1$ //$NON-NLS-2$
			return ""; //$NON-NLS-1$
		}
		if (defaultValue == null) {
			return ""; //$NON-NLS-1$
		} else if (defaultValue.equals("") && //$NON-NLS-1$
		// Using contains in the following to also cover unsigned
				(type.contains("int") || type.contains("tinyint") //$NON-NLS-1$ //$NON-NLS-2$
						|| type.contains("smallint") //$NON-NLS-1$
						|| type.contains("mediumint") //$NON-NLS-1$
						|| type.contains("bigint") || type.contains("float") //$NON-NLS-1$ //$NON-NLS-2$
						|| type.contains("decimal") || type.contains("double"))) { //$NON-NLS-1$ //$NON-NLS-2$
			return ""; //$NON-NLS-1$
		} else if (!defaultValue.equals("")) { //$NON-NLS-1$
			return "DEFAULT '" + defaultValue + "' "; //$NON-NLS-1$ //$NON-NLS-2$
		} else {
			return "DEFAULT '' "; //$NON-NLS-1$
		}
	}

	/**
	 * builds the query part for the column definition
	 * 
	 * @param columnName
	 * @param type
	 * @param size
	 * @param nullable
	 * @param defaultValues
	 * @param autoincrement
	 * @return
	 */
	private String columnOptions(String columnName, String type, String size,
			String nullable, String defaultValues, String autoincrement) {
		String[] parts = type.split(" "); //$NON-NLS-1$
		String query = columnName + " "; //$NON-NLS-1$
		if (type.contains("unsigned") && parts.length == 2 //$NON-NLS-1$
				&& parts[1].equals("unsigned")) { //$NON-NLS-1$
			query += parts[0].toUpperCase() + " (" + size + ") " //$NON-NLS-1$ //$NON-NLS-2$
					+ parts[1].toUpperCase() + " "; //$NON-NLS-1$
		} else {
			query += type.toUpperCase() + " (" + size + ") "; //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (nullable.equals("NO")) { //$NON-NLS-1$
			query = query + "NOT NULL "; //$NON-NLS-1$
		}
		query = query + getDefaultValueString(defaultValues, type);
		if (autoincrement.equals("YES")) { //$NON-NLS-1$
			query = query + "AUTO_INCREMENT "; //$NON-NLS-1$
		}
		return query;
	}

	/**
	 * changes the definition of the given column
	 * 
	 * @param tableName
	 * @param columnName
	 * @param type
	 * @param size
	 * @param nullable
	 *            - can be null
	 * @param defaultValues
	 *            - has a default value
	 * @param autoincrement
	 *            - is autoincrement
	 * @throws StatementNotExecutedException
	 */
	public void changeColumn(String tableName, String columnName, String type,
			String size, String nullable, String defaultValues,
			String autoincrement) throws StatementNotExecutedException {
		String query = "ALTER TABLE " //$NON-NLS-1$
				+ tableName
				+ " CHANGE " //$NON-NLS-1$
				+ columnName
				+ " " //$NON-NLS-1$
				+ columnOptions(columnName, type, size, nullable,
						defaultValues, autoincrement) + ";"; //$NON-NLS-1$
		
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}

	}

	/**
	 * adds a column to the given table
	 * 
	 * @param tableName
	 * @param columnName
	 * @param type
	 * @param size
	 * @param nullable
	 *            - can be null
	 * @param defaultValues
	 *            - has a default value
	 * @param autoincrement
	 *            - is autoincrement
	 * @throws StatementNotExecutedException
	 */
	public void addColumn(String tableName, String columnName, String type,
			String size, String nullable, String defaultValues,
			String autoincrement) throws StatementNotExecutedException {
		String query = "ALTER TABLE " //$NON-NLS-1$
				+ tableName
				+ " ADD " //$NON-NLS-1$
				+ columnOptions(columnName, type, size, nullable,
						defaultValues, autoincrement);
		
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * adds a primary key to the given table
	 * 
	 * @param tableName
	 * @param columns
	 *            - new primary key
	 * @throws StatementNotExecutedException
	 */
	public void addPrimaryKey(String tableName, String[] columns)
			throws StatementNotExecutedException {
		String query = "ALTER TABLE " + tableName + " ADD PRIMARY KEY ("; //$NON-NLS-1$ //$NON-NLS-2$
		for (int i = 0; i < columns.length - 1; i++) {
			query = query + columns[i] + ", "; //$NON-NLS-1$
		}
		query = query + columns[columns.length - 1] + ");"; //$NON-NLS-1$
		
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * adapts primary key of given table
	 * 
	 * @param tableName
	 * @param columns
	 *            - new primary key
	 * @throws StatementNotExecutedException
	 */
	public void changePrimaryKey(String tableName, String[] columns)
			throws StatementNotExecutedException {
		String query = "ALTER TABLE " + tableName + " DROP PRIMARY KEY, " //$NON-NLS-1$ //$NON-NLS-2$
				+ "ADD PRIMARY KEY ("; //$NON-NLS-1$
		for (int i = 0; i < columns.length - 1; i++) {
			query = query + columns[i] + ", "; //$NON-NLS-1$
		}
		query = query + columns[columns.length - 1] + ");"; //$NON-NLS-1$
		
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	private static void printMetaErrorMessage(SQLException sql, String function)
			throws MetaStatementNotExecutedException {
		if (_log.isErrorEnabled()) {
			_log.error("ErrorCode " + sql.getErrorCode()); //$NON-NLS-1$
		}
		throw new MetaStatementNotExecutedException(function);
	}
}
