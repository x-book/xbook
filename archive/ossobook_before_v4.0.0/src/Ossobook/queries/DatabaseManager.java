package ossobook.queries;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import ossobook.client.base.metainfo.Project;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages version table and nachrichten table (makes inserts, deletes, updates
 * and select statements on the tables)
 * 
 * @author j.lamprecht
 * 
 */

class DatabaseManager extends TableManager {
	public DatabaseManager(Connection con, String databaseName) {
		super(con, databaseName);
	}

	/**
	 * Get the current client version from the database.
	 * 
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getOssobookVersion() throws StatementNotExecutedException {
		String query = String.format("SELECT ossobookversion FROM %s.version", databaseName); //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getString(1);
			}
			return "";
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}
	
	// get DB-Version from DB
	public String getDatabaseVersion() throws StatementNotExecutedException {
		String query = String.format("SELECT dbversion FROM %s.version", databaseName); //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getString(1);
			}
			return "";
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}
	
	/**
	 * get number of local database
	 * 
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getDatabaseNumber() throws StatementNotExecutedException {
		String query = "SELECT Datenbanknummer FROM " + databaseName //$NON-NLS-1$
				+ ".version;"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getInt(1);
			}
			return -1;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get next nachrichtennummer to assign
	 * 
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getMessageNumber() throws StatementNotExecutedException {
		String query = "SELECT Nachrichtennummer FROM " + databaseName //$NON-NLS-1$
				+ ".version;"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getInt(1);
			}
			return -1;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get next assigned nachrichtennummer for project
	 * 
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int getAssignedMessageNumber(Project project)
			throws StatementNotExecutedException {
		String query = ""; //$NON-NLS-1$
		try {
			query = "SELECT Nachrichtennummer FROM " + databaseName //$NON-NLS-1$
					+ ".projekt WHERE" + " ProjNr=" + project.getNumber() //$NON-NLS-1$ //$NON-NLS-2$
					+ " AND Datenbanknummer=" + project.getDatabaseNumber() //$NON-NLS-1$
					+ " AND Nachrichtennummer!=-1;"; //$NON-NLS-1$
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getInt(1);
			}
			query = "SELECT Nachrichtennummer FROM " + databaseName //$NON-NLS-1$
					+ ".eingabeeinheit WHERE" + " ProjNr=" + project.getNumber() //$NON-NLS-1$ //$NON-NLS-2$
					+ " AND DBNummerProjekt=" + project.getDatabaseNumber() //$NON-NLS-1$
					+ " AND Nachrichtennummer!=-1;"; //$NON-NLS-1$
			rs = executeSelect(query);
			if (rs.next()) {
				return rs.getInt(1);
			}
			query = "SELECT masse.Nachrichtennummer FROM " //$NON-NLS-1$
					+ databaseName
					+ ".eingabeeinheit, " //$NON-NLS-1$
					+ databaseName
					+ ".masse WHERE" //$NON-NLS-1$
					+ " ProjNr=" //$NON-NLS-1$
					+ project.getNumber()
					+ " AND DBNummerProjekt=" //$NON-NLS-1$
					+ project.getDatabaseNumber()
					+ " AND eingabeeinheit.massID=masse.massID AND DBNummerMasse=masse.Datenbanknummer AND masse.Nachrichtennummer!=-1;"; //$NON-NLS-1$
			rs = executeSelect(query);
			if (rs.next()) {
				return rs.getInt(1);
			}
			query = "SELECT artefaktmaske.Nachrichtennummer FROM " //$NON-NLS-1$
					+ databaseName
					+ ".eingabeeinheit, " //$NON-NLS-1$
					+ databaseName
					+ ".artefaktmaske WHERE" //$NON-NLS-1$
					+ " ProjNr=" //$NON-NLS-1$
					+ project.getNumber()
					+ " AND DBNummerProjekt=" //$NON-NLS-1$
					+ project.getDatabaseNumber()
					+ " AND eingabeeinheit.artefaktID=artefaktmaske.ID AND DBNummerArtefakt=artefaktmaske.Datenbanknummer AND artefaktmaske.Nachrichtennummer!=-1;"; //$NON-NLS-1$
			rs = executeSelect(query);
			if (rs.next()) {
				return rs.getInt(1);
			}
			return -1;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * check, if project changes have already been handled by server
	 * 
	 * @param messageNumber
	 *            Nachrichtennummer of local database
	 * @param databaseNumber
	 *            Datenbanknummer of local database
	 * @param project
	 *            primary key of project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public boolean checkMessageNumber(int messageNumber, int databaseNumber,
			Project project) throws StatementNotExecutedException {
		String query = "SELECT nachrichten.Nachrichtennummer<" //$NON-NLS-1$
				+ messageNumber + " FROM " + databaseName //$NON-NLS-1$
				+ ".nachrichten WHERE nachrichten.Datenbanknummer = " //$NON-NLS-1$
				+ databaseNumber + " AND nachrichten.ProjNr=" //$NON-NLS-1$
				+ project.getNumber() + " AND nachrichten.DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber() + ";"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getBoolean(1);
			}
			// no entry for database number => message not handled yet
			return true;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * memorize next nachrichtennummer
	 * 
	 * @throws StatementNotExecutedException
	 */
	public void increaseMessageNumber() throws StatementNotExecutedException {
		String query = "UPDATE " + databaseName //$NON-NLS-1$
				+ ".version SET Nachrichtennummer=Nachrichtennummer+1;"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * part 1 of memorizing handled project changes of client
	 * 
	 * @param messageNumber
	 * @param databaseNumber
	 * @param project
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int updateMessageNumber(int messageNumber, int databaseNumber, Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " + databaseName //$NON-NLS-1$
				+ ".nachrichten SET Nachrichtennummer=" + messageNumber //$NON-NLS-1$
				+ " WHERE Datenbanknummer=" + databaseNumber //$NON-NLS-1$
				+ " AND nachrichten.ProjNr=" + project.getNumber() //$NON-NLS-1$
				+ " AND nachrichten.DBNummerProjekt=" + project.getDatabaseNumber() + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			return executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * part 2 of memorizing handled project changes of client
	 * 
	 * @param messageNumber
	 * @param databaseNumber
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void insertMessageNumber(int messageNumber, int databaseNumber, Project project)
			throws StatementNotExecutedException {
		String query = "INSERT INTO " //$NON-NLS-1$
				+ databaseName
				+ ".nachrichten (Datenbanknummer, ProjNr, DBNummerProjekt, Nachrichtennummer) VALUES (" //$NON-NLS-1$
				+ databaseNumber + "," + project.getNumber() + "," //$NON-NLS-1$ //$NON-NLS-2$
				+ project.getDatabaseNumber() + "," + messageNumber + ");"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get current time
	 * 
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getSystemTime() throws StatementNotExecutedException {
		String query = "SELECT Now()+0"; //$NON-NLS-1$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getString(1);
			}
			return "";
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * lock given tables of given database
	 * 
	 * @param databaseName
	 * @param tables
	 * @throws StatementNotExecutedException
	 */
	public void lockDatabase(String databaseName, Vector<String> tables)
			throws StatementNotExecutedException {
		String query = "LOCK TABLES "; //$NON-NLS-1$
		for (int i = 0; i < tables.size(); i++) {
			query = query + databaseName + "." + tables.elementAt(i) + " WRITE"; //$NON-NLS-1$ //$NON-NLS-2$
			if (i < tables.size() - 1) {
				query = query + ", "; //$NON-NLS-1$
			}
		}
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * unlock database
	 * 
	 * @throws StatementNotExecutedException
	 */
	public void unlockTables() throws StatementNotExecutedException {
		String query = "UNLOCK TABLES;"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}
}
