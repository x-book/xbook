package ossobook.queries;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ossobook.Messages;
import ossobook.client.base.metainfo.Project;
import ossobook.exceptions.NoSuchMeasureCombinationException;
import ossobook.exceptions.StatementNotExecutedException;

/**
 * manages masse and massabhaengigkeit table (makes inserts, deletes, updates
 * and select statements on these tables)
 * 
 * @author j.lamprecht
 * 
 */
class MassManager extends TableManager {
	private static final Log _log = LogFactory.getLog(MassManager.class);
	private final int versionDatabaseNumber;

	public MassManager(Connection con, int databaseNumber, String databaseName) {
		super(con, databaseName);
		versionDatabaseNumber = databaseNumber;
	}

	/**
	 * @precondition user has the right to change project (via record in
	 *               eingabeeinheit)
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void deleteMasse(Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " //$NON-NLS-1$
				+ databaseName
				+ ".masse, " //$NON-NLS-1$
				+ databaseName
				+ ".eingabeeinheit " //$NON-NLS-1$
				+ "SET masse.geloescht='Y', masse.Zustand='ge\u00E4ndert' " //$NON-NLS-1$
				+ "WHERE eingabeeinheit.massID=masse.MassID AND eingabeeinheit.DBNummerMasse=masse.Datenbanknummer" //$NON-NLS-1$
				+ " AND eingabeeinheit.projNr='" + project.getNumber() //$NON-NLS-1$
				+ "' AND eingabeeinheit.DBNummerProjekt=" + project.getDatabaseNumber() + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * masse records definitely deleted from database for given project
	 * 
	 * @precondition method is executed on local database during synchronization
	 * @param project
	 * @param event
	 *            - QueryManager.DELETE_DELETED, QueryManager.DELETE_ALL
	 * @throws StatementNotExecutedException
	 */
	public void deleteMassePermanent(Project project, int event)
			throws StatementNotExecutedException {
		String condition = ""; //$NON-NLS-1$
		if (event == QueryManager.DELETE_DELETED) {
			condition = " AND masse.geloescht='Y' AND masse.Zustand='synchronisiert'"; //$NON-NLS-1$
		}
		String query = "DELETE masse FROM " + databaseName + ".masse, " //$NON-NLS-1$ //$NON-NLS-2$
				+ databaseName + ".eingabeeinheit " //$NON-NLS-1$
				+ "WHERE eingabeeinheit.massID=masse.MassID " //$NON-NLS-1$
				+ "AND eingabeeinheit.DBNummerMasse=masse.Datenbanknummer " //$NON-NLS-1$
				+ "AND eingabeeinheit.projNr=" + project.getNumber() //$NON-NLS-1$
				+ " AND eingabeeinheit.DBNummerProjekt=" + project.getDatabaseNumber() + " " //$NON-NLS-1$ //$NON-NLS-2$
				+ condition + ";"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to change project (via record in
	 *               eingabeeinheit)
	 * @param fieldsMasse
	 * @param valuesMasse
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public int[] insertIntoMasse(Vector<String> fieldsMasse,
			Vector<String> valuesMasse) throws StatementNotExecutedException {
		int[] massID = new int[2];

		String table = databaseName + ".masse"; //$NON-NLS-1$

		String query = "INSERT into " + table + " ("; //$NON-NLS-1$ //$NON-NLS-2$
		for (int i = 0; i < fieldsMasse.size(); i++) {
			query = query + fieldsMasse.elementAt(i) + ", "; //$NON-NLS-1$
		}
		query = query + "Datenbanknummer, Zustand) VALUES ("; //$NON-NLS-1$
		for (int i = 0; i < valuesMasse.size(); i++) {
			query = query + valuesMasse.elementAt(i) + ", "; //$NON-NLS-1$
		}
		query = query + versionDatabaseNumber + ", 'ge\u00E4ndert');"; //$NON-NLS-1$
		try {
			massID[0] = executeInsert(query);
			massID[1] = versionDatabaseNumber;
			/*
			 * query = "SELECT MAX(MassID) as mid FROM masse;"; ResultSet rs =
			 * createStatement(query); rs.next(); massID = rs.getInt("mid");
			 */
			return massID;
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get names of masses from table massabhaengigkeit for given labels
	 * 
	 * @param tierlabel
	 * @param skellabel
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String getMasse(int tierlabel, int skellabel)
			throws StatementNotExecutedException,
			NoSuchMeasureCombinationException {
		String query = "SELECT Masse FROM " + databaseName //$NON-NLS-1$
				+ ".massabhaengigkeit WHERE TierLB=" + tierlabel //$NON-NLS-1$
				+ " AND SkelLB=" + skellabel + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			ResultSet rs = executeSelect(query);
			rs.last();
			int count = rs.getRow();
			if (count == 0) {
				throw new NoSuchMeasureCombinationException(
						Messages.getString("MassManager.0")); //$NON-NLS-1$
			}
			rs.beforeFirst();
			if (rs.next()) {
				return rs.getString("Masse"); //$NON-NLS-1$
			}
			return "";
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get description for given name from massdefinition
	 * 
	 * @param s
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String holeMassName(String s) throws StatementNotExecutedException {
		String query = "SELECT massBeschreibung FROM " + databaseName //$NON-NLS-1$
				+ ".massdefinition WHERE massName LIKE('" + s + "');"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getString("massBeschreibung"); //$NON-NLS-1$
			}
			return "";
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has at least the right to read project (via record in
	 *               eingabeeinheit)
	 * @param mass
	 * @param massKey
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public String holeMassWert(String mass, int[] massKey)
			throws StatementNotExecutedException {
		String query = "SELECT " + mass + " FROM " + databaseName //$NON-NLS-1$ //$NON-NLS-2$
				+ ".masse WHERE massID =" + massKey[0] //$NON-NLS-1$
				+ " AND Datenbanknummer=" + massKey[1] + " AND geloescht='N';"; //$NON-NLS-1$ //$NON-NLS-2$
		if (_log.isDebugEnabled()) {
			_log.debug("Query: " + query); //$NON-NLS-1$
		}
		try {
			ResultSet rs = executeSelect(query);
			if (rs.next()) {
				return rs.getString(mass);
			}
			return "";
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * @precondition user has the right to change project (via record in
	 *               eingabeeinheit)
	 * @param massID
	 * @throws StatementNotExecutedException
	 */
	public void deleteFromMasse(int[] massID)
			throws StatementNotExecutedException {
		String query = "UPDATE " //$NON-NLS-1$
				+ databaseName
				+ ".masse SET geloescht='Y', Zustand='ge\u00E4ndert' WHERE MassID='" //$NON-NLS-1$
				+ massID[0] + "' AND Datenbanknummer=" + massID[1] + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * updates a given data record in the table Masse
	 * 
	 * @precondition user has the right to change project (via record in
	 *               eingabeeinheit)
	 * @param fieldsMasse
	 *            : fields in Masse that shall be updated
	 * @param massKey
	 *            : identifier for the Masse record
	 * @param valuesMasse
	 *            : values to which the fields shall be changed
	 * @throws StatementNotExecutedException
	 */
	public void updateMasse(Vector<String> fieldsMasse, int[] massKey,
			Vector<String> valuesMasse) throws StatementNotExecutedException {
		String query = "UPDATE " + databaseName + ".masse SET "; //$NON-NLS-1$ //$NON-NLS-2$
		for (int i = 0; i < fieldsMasse.size(); i++) {
			query = query + fieldsMasse.elementAt(i) + "='" //$NON-NLS-1$
					+ valuesMasse.elementAt(i) + "', "; //$NON-NLS-1$
		}
		query = query + "Zustand='ge\u00E4ndert'"; //$NON-NLS-1$
		query = query + " WHERE massID=" + massKey[0] //$NON-NLS-1$
				+ " AND masse.Datenbanknummer=" + massKey[1] + ";"; //$NON-NLS-1$ //$NON-NLS-2$
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	/**
	 * get all masse entries which have been changed
	 * 
	 * @param project
	 * @param columntypes
	 *            columns of global database -> needed to be independet from
	 *            order and global database scheme changes
	 * @param event
	 *            - QueryManager.CHANGES_LOCAL, QueryManager.CHANGES_GLOBAL,
	 *            QueryManager.ALL_GLOBAL
	 * @param lastSynchronisation
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getEntries(Project project,
			String[][] columntypes, int event, String lastSynchronisation)
			throws StatementNotExecutedException {
		Vector<String[]> result;
		String condition = " WHERE DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND ProjNr=" //$NON-NLS-1$
				+ project.getNumber()
				+ " AND eingabeeinheit.massID=masse.massID AND eingabeeinheit.DBNummerMasse=masse.Datenbanknummer"; //$NON-NLS-1$
		if (event == QueryManager.CHANGES_LOCAL) {
			condition = condition
					+ " AND masse.Zustand='ge\u00E4ndert' AND masse.Nachrichtennummer != -1"; //$NON-NLS-1$
		} else if (event == QueryManager.CHANGES_GLOBAL) {
			condition = condition + " AND masse.Zustand>'" //$NON-NLS-1$
					+ lastSynchronisation + "'"; //$NON-NLS-1$
		}
		String[] tables = { databaseName + ".masse", //$NON-NLS-1$
				databaseName + ".eingabeeinheit" }; //$NON-NLS-1$
		result = getEntries(columntypes, tables, condition);
		return result;
	}

	/**
	 * 
	 * @param projectValues
	 *            of global database -> needed to be independet from global
	 *            database scheme changes
	 * @param columns
	 *            of global database -> needed to be independet from order and
	 *            global database scheme changes
	 * @param key
	 * @return
	 * @throws StatementNotExecutedException
	 */
	public Vector<String[]> getMassEntry(String[][] columns,
			int[] projectValues, Vector<String> key)
			throws StatementNotExecutedException {
		String condition = " WHERE "; //$NON-NLS-1$
		for (int i = 0; i < key.size(); i++) {
			condition = condition + key.elementAt(i) + "=" //$NON-NLS-1$
					+ projectValues[i];
			if (i < key.size() - 1) {
				condition = condition + " AND "; //$NON-NLS-1$
			}
		}
		String[] tableNames = { databaseName + ".masse" }; //$NON-NLS-1$
		return getEntries(columns, tableNames, condition);
	}

	public void insertData(String[] scheme, String[] data)
			throws StatementNotExecutedException {
		insertData(scheme, data, false);
	}
	
	public void insertData(String[] scheme, String[] data, boolean ignore)
			throws StatementNotExecutedException {
		insertData(scheme, data, databaseName + ".masse", ignore); //$NON-NLS-1$
	}

	public int updateData(String[] scheme, String[] data, Vector<String> key)
			throws StatementNotExecutedException {
		return updateData(scheme, data, databaseName + ".masse", key); //$NON-NLS-1$
	}

	/**
	 * set Zustand to 'synchronisiert' after server affirmed changing global
	 * database
	 * 
	 * @precondition executed on local database during synchronization
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void setSynchronized(Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " //$NON-NLS-1$
				+ databaseName
				+ ".masse, " //$NON-NLS-1$
				+ databaseName
				+ ".eingabeeinheit SET masse.Zustand='synchronisiert', masse.Nachrichtennummer=-1 WHERE " //$NON-NLS-1$
				+ "ProjNr = '" //$NON-NLS-1$
				+ project.getNumber()
				+ "' AND eingabeeinheit.DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND " //$NON-NLS-1$
				+ " eingabeeinheit.massID=masse.MassID AND eingabeeinheit.DBNummerMasse=masse.Datenbanknummer AND masse.Nachrichtennummer!=-1;"; //$NON-NLS-1$
		try {
			executeUpdate(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

	public String getLastChange(Vector<String> key, int[] keyValues)
			throws StatementNotExecutedException {
		return getStatus(key, keyValues, databaseName + ".masse"); //$NON-NLS-1$
	}

	/**
	 * assign message number to changed entries in masse of given project
	 * 
	 * @param message
	 * @param project
	 * @throws StatementNotExecutedException
	 */
	public void setMessageNumber(int message, Project project)
			throws StatementNotExecutedException {
		String query = "UPDATE " //$NON-NLS-1$
				+ databaseName
				+ ".masse, " //$NON-NLS-1$
				+ databaseName
				+ ".eingabeeinheit SET masse.Nachrichtennummer=" //$NON-NLS-1$
				+ message
				+ " WHERE ProjNr=" //$NON-NLS-1$
				+ project.getNumber()
				+ " AND eingabeeinheit.DBNummerProjekt=" //$NON-NLS-1$
				+ project.getDatabaseNumber()
				+ " AND eingabeeinheit.massID=masse.MassID AND eingabeeinheit.DBNummerMasse=masse.Datenbanknummer AND masse.Zustand='ge\u00E4ndert';"; //$NON-NLS-1$
		try {
			executeQuery(query);
		} catch (SQLException e) {
			log.error("MySQL Error occurred.", e);
			throw new StatementNotExecutedException(query);
		}
	}

}
