package ossobook.exceptions;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import ossobook.Messages;

public class NoSuchMeasureCombinationException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoSuchMeasureCombinationException(String info) {
		JFrame frame = new JFrame();
		JOptionPane.showMessageDialog(frame, info, Messages.getString("NoSuchMeasureCombinationException.0"), //$NON-NLS-1$
				JOptionPane.WARNING_MESSAGE);
		this.printStackTrace();
	}
}
