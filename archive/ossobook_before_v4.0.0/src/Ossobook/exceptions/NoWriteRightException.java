package ossobook.exceptions;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import ossobook.Messages;

/**
 * exception in case user has no right to change project especially needed for
 * deletion of project
 * 
 * @author j.lamprecht
 * 
 */
public class NoWriteRightException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * inform user about missing write right
	 * 
	 * @param projname
	 */
	public NoWriteRightException(String projname) {
		JFrame frame = new JFrame();
		JOptionPane.showMessageDialog(frame,
			Messages.getString("NoWriteRightException.0", projname), //$NON-NLS-1$
			Messages.getString("NoWriteRightException.4"), //$NON-NLS-1$
			JOptionPane.WARNING_MESSAGE);
	}

}
