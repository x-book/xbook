package ossobook.exceptions;

/**
 * exception in case user has no right to write global project used during
 * initialization and synchronization
 * 
 * @author j.lamprecht
 * 
 */
public class NoWriteRightExceptionForSynchronization extends NoRightExceptionForSynchronization {

	private static final long serialVersionUID = 1L;

	/**
	 * remember projects with missing write right
	 *
	 */
	public NoWriteRightExceptionForSynchronization() {
		super();
	}
}
