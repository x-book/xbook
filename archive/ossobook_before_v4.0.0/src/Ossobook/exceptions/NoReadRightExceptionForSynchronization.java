package ossobook.exceptions;

/**
 * exception in case user has no right to read global project used during
 * initialization and synchronization
 * 
 * @author j.lamprecht
 * 
 */
public class NoReadRightExceptionForSynchronization extends NoRightExceptionForSynchronization {

	private static final long serialVersionUID = 1L;
	
	/**
	 * remember projects with missing read right
	 *
	 */
	public NoReadRightExceptionForSynchronization() {
		super();
	}
}
