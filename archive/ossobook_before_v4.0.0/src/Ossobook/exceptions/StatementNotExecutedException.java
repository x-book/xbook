package ossobook.exceptions;

import java.sql.SQLException;


/**
 * exception for the case that a DML statement could not be executed
 * 
 * @author j.lamprecht
 * 
 */
public class StatementNotExecutedException extends SQLException {

	private static final long	serialVersionUID	= 2076681914417747674L;

	private final String query;

	/**
	 * inform user about DML fault
	 * 
	 * @param query
	 *            - statement that could not be executed
	 */
	public StatementNotExecutedException(String query) {
//		JFrame frame = new JFrame();
//		JOptionPane.showMessageDialog(frame, query
//				+ Messages.getString("StatementNotExecutedException.0"), //$NON-NLS-1$
//				Messages.getString("StatementNotExecutedException.1"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
//		this.printStackTrace();
		this.query = query;
	}

	@Override
	public String toString() {
		return query;
	}
}
