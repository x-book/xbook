package ossobook.exceptions;


/**
 * exception in case user has no right to read / write global project used
 * during initialization and synchronization
 * 
 * @author j.lamprecht
 * 
 */
public abstract class NoRightExceptionForSynchronization extends Exception {

	private static final long serialVersionUID = 1L;
	
	NoRightExceptionForSynchronization() {
	}

}
