package ossobook.exceptions;

import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import ossobook.Messages;

/**
 * exception for the case that a DDL statement could not be executed
 * 
 * @author j.lamprecht
 * 
 */
public class MetaStatementNotExecutedException extends SQLException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * inform user about DDL-fault
	 * 
	 * @param function
	 *            - statement that could not be executed
	 */
	public MetaStatementNotExecutedException(String function) {
		JFrame frame = new JFrame();
		JOptionPane.showMessageDialog(frame, 
				Messages.getString("MetaStatementNotExecutedException.0", function), //$NON-NLS-1$
				Messages.getString("MetaStatementNotExecutedException.1"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
		this.printStackTrace();
	}
}
